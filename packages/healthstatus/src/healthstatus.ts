import {
  GrpcObject,
  handleUnaryCall,
  loadPackageDefinition,
  Server,
  ServerCredentials,
  ServiceDefinition,
} from '@grpc/grpc-js';
import {loadSync} from '@grpc/proto-loader';
import path from 'path';

export const PROTO_PATH = path.join(__dirname, '../protos/healthstatus.proto');

export function loadHealthStatusService() {
  const packageDefinition = loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  });

  const pkg = loadPackageDefinition(packageDefinition);
  const healthStatus = (pkg.healthStatus as GrpcObject)
    .HealthStatusService as GrpcObject;
  return healthStatus;
}

/**
 * Get a new server with the handler functions in this file bound to the methods
 * it serves.
 * @return The new server object
 */
export function createHealthStatusServer(port = '0.0.0.0:9090') {
  const server = new Server();
  const healthStatus = loadHealthStatusService();

  const healthstats: handleUnaryCall<{userRole: string}, unknown> = (
    call,
    callback,
  ) => {
    //let userRole = call.request?.userRole ?? 'user';
    //    if (!(userId in recommendations)) {
    //      userId = "user001";
    //    }
    callback(null, {
      healthStatuses: [
        {
          name: 'status1',
          report: 0,
        },
        {
          name: 'status2',
          report: 0,
        },
        {
          name: 'status3',
          report: 0,
        },
      ],
    });
  };

  server.addService(healthStatus.service as ServiceDefinition<unknown>, {
    healthstats,
  });

  return new Promise<{server: Server; port: number}>((resolve, reject) => {
    server.bindAsync(port, ServerCredentials.createInsecure(), (err, p) => {
      if (err) reject(err);
      else return resolve({server, port: p});
    });
  });
}

export async function main(port = '0.0.0.0:9090') {
  const healthStatusServer = await createHealthStatusServer(port);
  healthStatusServer.server.start();
  console.log(`HealthStatus server is running at ${port}.`);
  return healthStatusServer;
}

if (require.main === module) {
  main().catch(err => {
    console.error(err);
    process.exit(1);
  });
}
