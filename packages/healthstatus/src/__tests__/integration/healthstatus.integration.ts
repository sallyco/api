import {Client, credentials, Server} from '@grpc/grpc-js';
import {expect} from '@loopback/testlab';
import {createHealthStatusServer} from '../..';
import {loadHealthStatusService} from '../../healthstatus';

describe('healthstatus', () => {
  let server: Server;
  let port: number;

  before('starting server', async () => {
    const result = await createHealthStatusServer();
    server = result.server;
    port = result.port;
    server.start();
  });

  after('stopping server', async () => {
    if (server) {
      server.forceShutdown();
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let client: any;
  before(() => {
    const HealthStatusService =
      loadHealthStatusService() as unknown as typeof Client;
    client = new HealthStatusService(
      `localhost:${port}`,
      credentials.createInsecure(),
    );
  });

  it('returns health status for user002', done => {
    client.healthstats({userId: 'user002'}, (err: Error, result: unknown) => {
      if (err) return done(err);
      expect(result).to.eql({
        healthStatuses: [
          {
            name: 'status1',
            report: 0,
          },
          {
            name: 'status2',
            report: 0,
          },
          {
            name: 'status3',
            report: 0,
          },
        ],
      });
      done();
    });
  });
});
