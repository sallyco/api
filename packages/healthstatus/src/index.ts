export * from '@loopback/http-server';
export * from './healthstatus';

import {main as runmain} from './healthstatus';

export async function main(
  config: {
    grpc?: {port?: string};
  } = {grpc: {}},
) {
  if (config.grpc) {
    runmain(config.grpc.port).catch(err => {
      console.error(err);
      process.exit(1);
    });
  }
}

if (require.main === module) {
  main().catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
