import { addons } from '@storybook/addons';
import gbtTheme from './GbtTheme';

addons.setConfig({
  theme: gbtTheme,
});
