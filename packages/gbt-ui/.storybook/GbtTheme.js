import { create } from '@storybook/theming';
import logo from './logo.png';

export default create({
  base: 'light',
  brandTitle: 'Glassboard Tech UI Components',
  brandUrl: 'https://developers.glassboardtech.com',
  brandImage: logo,
});
