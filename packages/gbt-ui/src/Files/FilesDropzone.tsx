import * as React from 'react';
import { styled } from '@mui/material/styles';
import clsx from 'clsx';
import { useDropzone } from 'react-dropzone';
import {
  Box,
  Button,
  IconButton,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Typography,
} from '@mui/material';
import bytesToSize from '../tools/bytesToSize';
import { FileUploadIcon, CopyIcon, EllipsisVIcon } from 'react-line-awesome';

const PREFIX = 'FilesDropzone';

const classes = {
  root: `${PREFIX}-root`,
  dropZone: `${PREFIX}-dropZone`,
  dragActive: `${PREFIX}-dragActive`,
  image: `${PREFIX}-image`,
  info: `${PREFIX}-info`,
  list: `${PREFIX}-list`,
  actions: `${PREFIX}-actions`
};

const Root = styled('div')((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {},

  [`& .${classes.dropZone}`]: {
    border: `3px dashed ${theme.palette.divider}`,
    padding: theme.spacing(6),
    outline: 'none',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      opacity: 0.5,
      cursor: 'pointer',
    },
  },

  [`& .${classes.dragActive}`]: {
    backgroundColor: theme.palette.action.active,
    opacity: 0.5,
  },

  [`& .${classes.image}`]: {
    width: 130,
    fontSize: '96px',
  },

  [`& .${classes.info}`]: {
    marginTop: theme.spacing(1),
  },

  [`& .${classes.list}`]: {
    maxHeight: 320,
  },

  [`& .${classes.actions}`]: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'flex-end',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  }
}));

export interface FilesDropzoneProps {
  handleUpload: (arg: any) => void;
  className?: string;
}

const FilesDropzone: React.FC<FilesDropzoneProps> = ({ handleUpload, className, ...rest }) => {

  const [files, setFiles] = React.useState<any[]>([]);

  const handleDrop = React.useCallback((acceptedFiles) => {
    setFiles((prevFiles) => [...prevFiles].concat(acceptedFiles));
  }, []);

  const handleRemoveAll = () => {
    setFiles([]);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop,
  });

  return (
    <Root className={clsx(classes.root, className)} {...rest}>
      <div
        className={clsx({
          [classes.dropZone]: true,
          [classes.dragActive]: isDragActive,
        })}
        {...getRootProps()}
      >
        <input {...getInputProps()} />
        <div>
          <FileUploadIcon className={classes.image} />
        </div>
        <div>
          <Typography gutterBottom variant="h3">
            Select files
          </Typography>
          <Box mt={2}>
            <Typography color="textPrimary" variant="body1">
              Drop files here or <Link underline="always">browse</Link> your computer
            </Typography>
          </Box>
        </div>
      </div>
      {files.length > 0 && (
        <React.Fragment>
          <List className={classes.list}>
            {files.map((file, i) => (
              <ListItem divider={i < files.length - 1} key={i}>
                <ListItemIcon>
                  <CopyIcon />
                </ListItemIcon>
                <ListItemText
                  primary={file.name}
                  primaryTypographyProps={{ variant: 'h5' }}
                  secondary={bytesToSize(file.size)}
                />
                <Tooltip title="More options">
                  <IconButton edge="end" size="large">
                    <EllipsisVIcon />
                  </IconButton>
                </Tooltip>
              </ListItem>
            ))}
          </List>
          <div className={classes.actions}>
            <Button onClick={handleRemoveAll} size="small">
              Remove all
            </Button>
            <Button
              color="secondary"
              size="small"
              variant="contained"
              onClick={() => handleUpload(files)}
            >
              Upload files
            </Button>
          </div>
        </React.Fragment>
      )}
    </Root>
  );
};

export default FilesDropzone;
