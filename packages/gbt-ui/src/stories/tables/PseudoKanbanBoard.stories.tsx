import { Story, Meta } from '@storybook/react';

import PseudoKanbanBoard, { PseudoKanbanBoardProps } from '../../Tables/PseudoKanbanBoard';
import mockKanban from './mock_kanban';

export default {
  title: 'Tables / Pseudo Kanban Board',
  component: PseudoKanbanBoard,
} as Meta;

const Template: Story<PseudoKanbanBoardProps> = (args) => <PseudoKanbanBoard {...args} />;

export const MockExample = Template.bind({});
MockExample.args = { ...mockKanban };
