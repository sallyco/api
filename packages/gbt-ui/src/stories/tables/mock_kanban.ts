import moment from 'moment';

//import { v4 as uuidv4 } from 'uuid';
//import _ from 'lodash';

export default {
  lists: [
    {
      id: '5e849c39325dc5ef58e5a5db',
      title: 'Incoming',
    },
    {
      id: '5e849c2b38d238c33e516755',
      title: 'In progress',
    },
  ],
  cards: [
    {
      id: '5e849c8708bd72683b454747',
      cover: '/static/images/projects/project_3.png',
      description:
        'Duis condimentum lacus finibus felis pellentesque, ac auctor nibh fermentum. Duis sed dui ante. Phasellus id eros tincidunt, dictum lorem vitae, pellentesque sem. Aenean eu enim sit amet mauris rhoncus mollis. Sed enim turpis, porta a felis et, luctus faucibus nisi. Phasellus et metus fermentum, ultrices arcu aliquam, facilisis justo. Cras nunc nunc, elementum sed euismod ut, maximus eget nibh. Phasellus condimentum lorem neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce sagittis pharetra eleifend. Suspendisse potenti.',
      footer: `${moment.utc().local().format('lll')} (${moment.utc().local().fromNow()})`,
      listId: '5e849c39325dc5ef58e5a5db',
      title: 'Call with sales of HubSpot',
    },
    {
      id: '5e849c90fabe1f1f4b3557f6',
      cover: null,
      description: 'We are looking for vue experience and of course node js strong knowledge',
      footer: `${moment().add(6, 'days').toDate().getTime()}`,
      listId: '5e849c39325dc5ef58e5a5db',
      title: 'Interview for the Asis. Sales Manager',
    },
    {
      id: '5e849c977ef6265938bfd90b',
      cover: null,
      description:
        'We nede to make it aggresive with pricing because it’s in their interest to acquire us',
      footer: null,
      listId: '5e849c39325dc5ef58e5a5db',
      title: 'Change the height of the top bar because it looks too chunky',
    },
    {
      id: '5e849c9e34ee93bc7255c599',
      cover: null,
      description:
        'We nede to make it aggresive with pricing because it’s in their interest to acquire us',
      footer: null,
      listId: '5e849c39325dc5ef58e5a5db',
      title: 'Integrate Stripe API',
    },
    {
      id: '5e849ca7d063dc3830d4b49c',
      cover: null,
      description:
        'We need to make it aggresive with pricing because it’s in their interest to acquire us',
      footer: null,
      listId: '5e849c2b38d238c33e516755',
      title: 'Update the customer API for payments',
    },
    {
      id: '5e849cb5d0c6e8894451fdfa',
      cover: null,
      description:
        'We need to make it aggresive with pricing because it’s in their interest to acquire us',
      footer: null,
      listId: '5e849c2b38d238c33e516755',
      title: 'Redesign the landing page',
    },
  ],
  title: 'test',
};
