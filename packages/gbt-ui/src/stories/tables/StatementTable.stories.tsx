import { Story, Meta } from '@storybook/react';

import StatementTable, { StatementTableProps } from '../../Tables/StatementTable';

const sch = Object({
  title: 'Statement Title',
  subTitle: 'Statement Subtitle',
  sections: [
    {
      title: 'Section Title',
      // subTitle: "Section Subtitle",
      entries: [
        {
          description: 'Entry1 proceeds description',
          proceeds: '$500,000',
        },
        {
          description: 'Entry1 expenses description',
          expenses: '$123,456',
        },
        {
          description: 'Bolder Entry',
          expenses: 'displayed subtotal',
          bold: true,
        },
      ],
      // subTotal: 490000,
    },
    {
      title: 'Section2 Title',
      subTitle: 'Section2 Subtitle',
      entries: [
        {
          description: 'Entry12 expenses description',
          expenses: '$100',
        },
        {
          description: 'Entry13 expenses description',
          expenses: '$200',
        },
      ],
      subTotal: 300,
    },
  ],
  createdAt: 'Thu, 27 May 2021 18:48:57 GMT',
});

export default {
  title: 'Tables / StatementTable',
  component: StatementTable,
} as Meta;

const Template: Story<StatementTableProps> = (args) => <StatementTable {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  statementData: sch,
  editHandler: (r, m) => {
    console.log(r);
    console.log(m);
  },
};
