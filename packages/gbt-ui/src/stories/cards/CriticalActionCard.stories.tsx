import { Story, Meta } from '@storybook/react';
import { Typography } from '@mui/material';

import CriticalActionCard, { CriticalActionCardProps } from '../../Cards/CriticalActionCard';

export default {
  title: 'Cards/Critical Action Card',
  component: CriticalActionCard,
} as Meta;

const Template: Story<CriticalActionCardProps> = (args) => <CriticalActionCard {...args} />;

export const NoChildren = Template.bind({});
NoChildren.args = {};

export const WithChildren = Template.bind({});
WithChildren.args = {
  title: 'do a test',
  actionWord: 'test',
  children: (
    <Typography variant="body2" color="textSecondary">
      {`This will delete from database.`}
    </Typography>
  ),
};
