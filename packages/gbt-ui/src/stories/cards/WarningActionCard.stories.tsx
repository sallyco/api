import { Story, Meta } from '@storybook/react';
import { Typography } from '@mui/material';

import WarningActionCard, { WarningActionCardProps } from '../../Cards/WarningActionCard';

export default {
  title: 'Cards/Warning Action Card',
  component: WarningActionCard,
} as Meta;

const Template: Story<WarningActionCardProps> = (args) => <WarningActionCard {...args} />;

export const NoChildren = Template.bind({});
NoChildren.args = {};

export const WithChildren = Template.bind({});
WithChildren.args = {
  title: 'do a test',
  buttonText: 'test',
  children: (
    <Typography variant="body2" color="textSecondary">
      {`This will delete from database.`}
    </Typography>
  ),
};
