import { Story, Meta } from '@storybook/react';

import PerformanceSummaryWidget from '../../Widgets/PerformanceSummaryWidget';

export default {
  title: 'Widgets / PerformanceSummaryWidget',
  component: PerformanceSummaryWidget,
} as Meta;

const Template: Story = (args) => <PerformanceSummaryWidget {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  primaryNumber: 1000,
  secondaryNumber: 7000,
  targetNumber: 10000,
};
