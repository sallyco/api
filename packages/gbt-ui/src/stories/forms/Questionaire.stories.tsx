import { Story, Meta } from '@storybook/react';

import Questionaire, { QuestionaireProps } from '../../Forms/Questionaire';
import { JSONSchema7 } from 'json-schema';

const sch2 = {
  title: 'Application Questionaire',
  type: 'object',
  properties: {
    question1: {
      type: 'string',
      title: 'What is your company name?',
    },
    question1_05: {
      type: 'string',
      title: 'How did you hear about our accelerator program?',
    },
    question1_1: {
      type: 'string',
      title: 'Please describe to us what your company does in 140 characters or less!',
    },
    question1_2: {
      type: 'string',
      title:
        'Please describe to us what your company does in further detail up to 2500 characters.',
    },
    question1_5: {
      type: 'string',
      title: 'What are the names of the Founders / Co-founders?',
    },
    question1_7: {
      type: 'string',
      title: 'Where are you located?',
    },
    question2: {
      type: 'string',
      title: 'What is your website address?',
    },
    question2_1: {
      type: 'string',
      title: 'Which industry segment would you best fit in? Select all that apply.',
    },
    question4: {
      type: 'string',
      title: 'Why did you start this company?',
    },
    question4_5: {
      type: 'string',
      title: 'How did you meet your co-founders (if applicable)?',
    },
    question4_9: {
      type: 'string',
      title: 'How long have you worked together?',
    },
    question5_1: {
      type: 'string',
      title: 'Describe the problem your company is solving.',
    },
    question5_5: {
      type: 'string',
      title: 'How are your customers solving that problem today?',
    },
    question6: {
      type: 'string',
      title:
        'Please provide a 1 minute video demo of your product. Please include the URL so we can view it with any usercode / passwords that may be required.',
    },
    question7: {
      type: 'string',
      title:
        'Please share with us which functional areas around LEGAL you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question7_3: {
      type: 'string',
      title:
        'Please share with us which functional areas around PRODUCT you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question7_5: {
      type: 'string',
      title:
        'Please share with us which functional areas around ORGANIZATION you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question7_7: {
      type: 'string',
      title:
        'Please share with us which functional areas around INVESTMENT you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question7_9: {
      type: 'string',
      title:
        'Please share with us which functional areas around MARKETING you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question8_1: {
      type: 'string',
      title:
        'Please share with us which functional areas around OPERATIONS you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question8_3: {
      type: 'string',
      title:
        'Please share with us which functional areas around SALES you would like further mentorship on.  Please select all that apply.',
      enum: [],
    },
    question8_4: {
      type: 'string',
      title:
        'Please share with us any other functional areas not listed above that you would like help on.',
    },
    question999: {
      type: 'string',
      title: 'Why do you want to join our accelerator?',
    },
  },
  //required: ['key', 'category'],
  additionalProperties: false,
} as JSONSchema7;

const sch = {
  title: 'Score Card Questionaire',
  type: 'object',
  properties: {
    question1: {
      type: 'string',
      title: 'How many startups have you founded BEFORE your current Startup?',
      enum: ['0', '1', '2', '3', '> 3'],
    },
    question2: {
      type: 'string',
      title: 'How many profitable exits have you had?',
      enum: ['0', '1', '2', '3', '> 3'],
    },
    question3: {
      type: 'string',
      title: 'How many domain/industries would you classify yourself as an expert?',
      enum: ['0', '1', '2', '3', '>3'],
    },
    question4: {
      type: 'string',
      title: 'How many years of management experience do you have? (ie. leading a team)',
      enum: ['0', '1-3', '4-7', '8-15', '> 15'],
    },
    question4_2: {
      type: 'string',
      title: 'Do you have both a Business Co-Founder along with a Technical Co-Founder?',
      enum: ['Yes', 'No', '', '', ''],
    },
    question5: {
      type: 'string',
      title:
        'What is your Service Obtainable Market (SOM) market size? Please upload evidence if available.',
      enum: ['I dont know', '<$250M', '$251M - $1B', '$1B - $3B', '> $3B'],
    },
    question6: {
      type: 'string',
      title: 'What is your primary method of getting your current product to market',
      enum: [
        'Enterprise Sales: > 6 months',
        'Direct Sales: 3 - 6 months',
        'Direct Sales: 0 - 2 months or E-marketing/Social with moderate costs to acquire',
        'Extensive Partner/Channel network in place and working',
        'E-Marketing / Viral approach, no touch, no cost to acquire',
      ],
    },
    question7: {
      type: 'string',
      title: 'What is the maturity level of your product?',
      enum: [
        'Only a concept/design at this stage',
        'Inital prototype thats working',
        'Minimal Viable Product (MVP) with > 50 engaged Users',
        'Multiple Products of which several are being monetized',
        'Multiple solutions that have achieved Product-Market Fit (PMF) and scaling',
      ],
    },
    question8: {
      type: 'string',
      title: 'How impactful is the value-proposition of your solution/product?',
      enum: [
        'Unknown at this time',
        '1 - 15% cost savings or increased revenue  to your company',
        '16% - 30% cost savings or increased revenue to your company',
        '31-45% cost savings or increased revenue  to your company',
        '> 45% cost savings or increased revenue  to your company',
      ],
    },
    question9: {
      type: 'string',
      title: 'What is your company’s market differentiator?',
      enum: [
        'Cost Leadership',
        'Better Function and Features / Ease of Use',
        'Large barriers to Entry',
        'Better Distribution Strategy',
        'Higher Value Proposition',
      ],
    },
    question10: {
      type: 'string',
      title:
        'How many competitors do you have? Please upload your Competitive Matrix or other Supporting documents.',
      enum: ['Unknown at this time', '>15', '11 - 15', '6 - 10', '< 5'],
    },
    question11: {
      type: 'string',
      title: 'How well funded are your Competitors?',
      enum: [
        'Unknown at this time',
        'Self-funded by Founders',
        'Seed Round < $1M',
        'A-Series round $1 - $5M',
        'A-Series or later round > $5M',
      ],
    },
    question12: {
      type: 'string',
      title: 'How many customers does your largest competitor have?',
      enum: ['Unknown at this time', '1 - 10', '11 - 25', '26 -100', '>100'],
    },
    question13: {
      type: 'string',
      title: 'How many months does it take to recover your customer acquisition costs (CAC)?',
      enum: ['Unknown at this time', '> 24 months', '12 - 24 months', '6-12 months', '< 6 months'],
    },
    question14: {
      type: 'string',
      title:
        'How complete is your business model canvas? Please upload your latest Canvas and any experiments',
      enum: [
        'We have not started',
        'Business model canvas completed - no experiments completed',
        'Canvas completed along with at least 10 validated experiments',
        'Canvas completed with 25 experiments validated with evidence',
        'Canvas completed with more than 50+ experiments validated with evidence',
      ],
    },
    question14_2: {
      type: 'string',
      title:
        'How many current distribution channels do you have in place and working at this time?',
      enum: ['0', '1 - 5', '6 - 10', '11 - 15', '> 15'],
    },
    question15: {
      type: 'string',
      title: 'What is your Monthly Recurring Revenue (MRR)?',
      enum: ['0.00  $', '< $1K', '$1K - $5K', '$5K - $25K', '> $25K'],
    },
    question16: {
      type: 'string',
      title: 'How many paying customers do you have?',
      enum: ['0', '1 - 10', '11 - 25', '26 - 100', '> 100'],
    },
    question17: {
      type: 'string',
      title: 'How many months of runway do you have? (ie. how long until you run out of money?)',
      enum: [
        '0 - we are out of money now!',
        '1 - 3 months',
        '4 - 12 months',
        '13 - 18 months',
        '> 18 months',
      ],
    },
    question18: {
      type: 'string',
      title: 'How many Monthly Active Users (MAU) do you have?',
      enum: ['Unknown at this time', '< 25', '25 - 100', '101-250', '> 250'],
    },
    question19: {
      type: 'string',
      title: 'What is your month-over-month (MoM) growth rate of your MRR in the past 6 months?',
      enum: ['0', '1 - 10%', '11 - 20%', '21 - 25%', '> 25%'],
    },
    question20: {
      type: 'string',
      title: 'How much money have you raised in the last 12 months?',
      enum: ['0', '< $100K', '$101 - $250K', '$251K - $750K', '> $750K'],
    },
    question21: {
      type: 'string',
      title: 'How much investment do you have soft circled for your next round?',
      enum: [
        '$0 or I am not raising a round in the next 12 months',
        '< $100K',
        '$101K - $250K',
        '$251K - $500K',
        '> $500K',
      ],
    },
    question22: {
      type: 'string',
      title: 'What is your attrition rate over the trailing twelve months (TTM)?',
      enum: ['We do not have Customers at this time.', '> 12%', '8% - 12%', '3% - 7%', '< 3%'],
    },
  },
  //required: ['key', 'category'],
  additionalProperties: false,
} as JSONSchema7;

export default {
  title: 'Forms / Questionaire Response',
  component: Questionaire,
  parameters: { actions: { argTypesRegex: '^Action.*' } },
} as Meta;

/* eslint-disable @typescript-eslint/no-explicit-any */
const Template: Story<QuestionaireProps> = (args) => <Questionaire{...args} />;

const sleep = (milliseconds: number) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

export const ScoreCard = Template.bind({});
ScoreCard.args = {
  schema: sch,
  submitAction: async (data) => {
    await sleep(2000);
    console.log(data);
  },
};
export const Application = Template.bind({});
Application.args = {
  schema: sch2,
  submitAction: async (data) => {
    await sleep(2000);
    console.log(data);
  },
};
