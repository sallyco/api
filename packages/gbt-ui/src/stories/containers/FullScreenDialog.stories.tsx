import * as React from 'react';
import { Story, Meta } from '@storybook/react';

import FullScreenDialog, { FullScreenDialogProps } from '../../Containers/FullScreenDialog';

export default {
  title: 'Containers/Full Screen Dialog',
  component: FullScreenDialog,
} as Meta;

const Template: Story<FullScreenDialogProps> = (args) => <FullScreenDialog {...args} />;

const [open, setOpen] = React.useState<boolean>(true);
const [loading, setLoading] = React.useState<boolean>(true);

export const Simple = Template.bind({});
Simple.args = {
  title: 'Dialog Test',
  loading: loading,
  setLoading: setLoading,
  open: open,
  setOpen: setOpen,
  children: (
    <React.Fragment>
      <p>`I\'m content!`</p>
    </React.Fragment>
  ),
};
