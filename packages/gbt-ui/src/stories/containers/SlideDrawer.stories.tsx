import * as React from 'react';
import { Story, Meta } from '@storybook/react';

import SlideDrawer, { SlideDrawerProps } from '../../Containers/SlideDrawer';

export default {
  title: 'Containers/Slide Drawer',
  component: SlideDrawer,
} as Meta;

const Template: Story<SlideDrawerProps> = (args) => <SlideDrawer {...args} />;

export const Simple = Template.bind({});
Simple.args = {
  title: 'Drawer Test',
  children: (
    <React.Fragment>
      <p>`I'm content!`</p>
    </React.Fragment>
  ),
};
