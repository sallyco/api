import { Story, Meta } from '@storybook/react';

import BasicTabs, { BasicTabsProps } from '../../Containers/BasicTabs';

export default {
  title: 'Containers/Basic Tabs',
  component: BasicTabs,
} as Meta;

const Template: Story<BasicTabsProps> = (args) => <BasicTabs {...args} />;

export const Simple = Template.bind({});
Simple.args = {
  headerColor: 'primary',
  tabs: [
    {
      tabName: 'Investors',
      tabContent: <></>,
    },
    {
      tabName: 'Documents',
      tabContent: <></>,
    },
    {
      tabName: 'Audit Trail',
      tabContent: <></>,
    },
    {
      tabName: 'Advanced',
      tabContent: <></>,
    },
  ],
};
