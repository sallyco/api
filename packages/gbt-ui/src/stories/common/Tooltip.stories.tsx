import { Button } from '@mui/material';
import { Story, Meta } from '@storybook/react';

import Tooltip, { TooltipProps } from '../../Common/Tooltip';

export default {
  title: 'Components/Tooltip',
  component: Tooltip,
} as Meta;

const Template: Story<TooltipProps> = (args) => <Tooltip {...args} />;

export const Simple = Template.bind({});
Simple.args = {
  title: 'This tooltip is a test',
  children: <Button size="large">A Button</Button>,
};

export const Slow = Template.bind({});
Slow.args = {
  title: 'This tooltip is a test',
  transitionTime: 2600,
  children: <Button size="large">A Button</Button>,
};
