import { Story, Meta } from '@storybook/react';

import TitleHeader, { TitleHeaderProps } from '../../Header/TitleHeader';

export default {
  title: 'Components/Title Header',
  component: TitleHeader,
} as Meta;

const Template: Story<TitleHeaderProps> = (args) => <TitleHeader {...args} />;

export const NoChildren = Template.bind({});
NoChildren.args = {};

export const WithChildren = Template.bind({});
WithChildren.args = {
  title: 'test',
};
