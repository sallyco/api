import { Story, Meta } from '@storybook/react';


import ContentLoader, { ContentLoaderProps } from '../../Common/ContentLoader';

export default {
  title: 'Components / Content Loader',
  component: ContentLoader,
} as Meta;

const Template: Story<ContentLoaderProps> = (args) => <ContentLoader {...args} />;

export const Basic = Template.bind({});
Basic.args = {};

export const CustomText = Template.bind({});
CustomText.args = { message: 'Please wait...' };
