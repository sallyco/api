import { Story, Meta } from '@storybook/react';

import Notifications from '../../Notifications/Notifications';

export default {
  title: 'Components / Notifications',
  component: Notifications,
} as Meta;

const Template: Story = () => <Notifications />;

export const Standard = Template.bind({});
