import { Story, Meta } from '@storybook/react';

import CopyrightFooter from '../../Footer/CopyrightFooter';

export default {
  title: 'Components/Copyright Footer',
  component: CopyrightFooter,
} as Meta;

const Template: Story = () => <CopyrightFooter />;

export const Standard = Template.bind({});
