import { Story, Meta } from '@storybook/react';

import FilesDropzone, { FilesDropzoneProps } from '../../Files/FilesDropzone';

export default {
  title: 'Components / Files Dropzone',
  component: FilesDropzone,
  argTypes: { handleUpload: { action: 'clicked' } },
} as Meta;

const Template: Story<FilesDropzoneProps> = (args) => <FilesDropzone {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  className: 'test',
};
