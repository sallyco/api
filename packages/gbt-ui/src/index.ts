//import { Theme } from '@mui/material/styles';

//declare module '@mui/styles/defaultTheme' {
//  // eslint-disable-next-line @typescript-eslint/no-empty-interface
//  interface DefaultTheme extends Theme {}
//}

//COMMON
export { default as DeletePopup } from './Common/DeletePopup';
export * from './Common/DeletePopup';
export { default as ContentLoader } from './Common/ContentLoader';
export * from './Common/ContentLoader';
export { default as SignaturePad } from './Common/SignaturePad';
export * from './Common/SignaturePad';
export { default as Timestamp } from './Common/Timestamp';
export * from './Common/Timestamp';

//CHART
//export { default as ScoreCard } from './Charts/ScoreCard';
//export * from './Charts/ScoreCard';

//FORMS
export { default as Questionaire } from './Forms/Questionaire';
export * from './Forms/Questionaire';

import { withTheme } from '@rjsf/core';
import { Theme as MaterialUITheme } from './forms';
export const Form = withTheme(MaterialUITheme);

//HEADER
export { default as TitleHeader } from './Header/TitleHeader';
export * from './Header/TitleHeader';

//FILES
export { default as FilesDropzone } from './Files/FilesDropzone';
export * from './Files/FilesDropzone';

//TABS
export { default as BasicTabs } from './Containers/BasicTabs';
export * from './Containers/BasicTabs';

//NOTIFICATIONS
export { default as Notifications } from './Notifications/Notifications';
export * from './Notifications/Notifications';

//TABLES
export { default as TableWithTools } from './Tables/TableWithTools';
export * from './Tables/TableWithTools';
export { default as StatementTable } from './Tables/StatementTable';
export * from './Tables/PseudoKanbanBoard';
export { default as PseudoKanbanBoard } from './Tables/PseudoKanbanBoard';
export { default as InvestmentsTable } from './Tables/InvestmentsTable';
export * from './Tables/InvestmentsTable';

//FOOTER
export { default as CopyrightFooter } from './Footer/CopyrightFooter';
export * from './Footer/CopyrightFooter';

//Containers
export { default as SlideDrawer } from './Containers/SlideDrawer';
export * from './Containers/SlideDrawer';
export { default as FullScreenDialog } from './Containers/FullScreenDialog';
export * from './Containers/FullScreenDialog';
export { default as ConfirmationDialog } from './Containers/ConfirmationDialog';
export * from './Containers/ConfirmationDialog';
export { default as LargeDialog } from './Containers/LargeDialog';
export * from './Containers/LargeDialog';

//Widgets
export { default as PerformanceSummaryWidget } from './Widgets/PerformanceSummaryWidget';

//LISTS
export { default as AuditList } from './Lists/AuditList';
export { default as AuditListBackend } from './Lists/AuditListBackend';
export * from './Lists/AuditList';
export * from './Lists/AuditListBackend';

//CARDS
export { default as InfoCard } from './Cards/InfoCard';
export * from './Cards/InfoCard';
export { default as MiniCard } from './Cards/MiniCard';
export * from './Cards/MiniCard';
