import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Divider,
  Avatar,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
} from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import { CheckIcon, TimesIcon, ExternalLinkSquareAltIcon, EnvelopeIcon } from 'react-line-awesome';

import _ from 'lodash';
import numeral from 'numeral';

const PREFIX = 'AuditList';

const classes = {
  root: `${PREFIX}-root`,
  success: `${PREFIX}-success`,
  fail: `${PREFIX}-fail`,
  inline: `${PREFIX}-inline`
};

const StyledList = styled(List)((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {
    backgroundColor: theme.palette.background.paper,
  },

  [`& .${classes.success}`]: {
    backgroundColor: 'green',
  },

  [`& .${classes.fail}`]: {
    backgroundColor: 'red',
  },

  [`& .${classes.inline}`]: {
    display: 'inline',
  }
}));

export interface AuditListProps {
  subscriptions: Array<any> | undefined;
  subscriptionsLoading: boolean;
  accountBalance: number | undefined;
  acouuntBalanceLoading: boolean;
  setCanApprove: React.Dispatch<React.SetStateAction<boolean>>;
  useCheck5?: boolean;
}



const AuditList = ({
  subscriptions = undefined,
  subscriptionsLoading,
  accountBalance = undefined,
  acouuntBalanceLoading,
  setCanApprove,
  useCheck5 = false,
}: AuditListProps) => {


  const check1 =
    !subscriptionsLoading &&
    typeof subscriptions !== 'undefined' &&
    !_.find(subscriptions, { isDocsSigned: false });
  const check2 =
    !acouuntBalanceLoading && typeof accountBalance !== 'undefined' && accountBalance > 0;
  const check3 =
    !subscriptionsLoading && typeof subscriptions !== 'undefined' && subscriptions.length < 100;
  const check4 =
    !subscriptionsLoading &&
    typeof subscriptions !== 'undefined' &&
    !_.find(subscriptions, (sub) => {
      return sub.profile?.kycAml?.result !== 'CLEAR';
    });
  const check5 =
    !subscriptionsLoading &&
    typeof subscriptions !== 'undefined' &&
    !acouuntBalanceLoading &&
    typeof accountBalance !== 'undefined' &&
    accountBalance ===
      _.sumBy(subscriptions, function (o) {
        return o.amount;
      });
  const check6 =
    !subscriptionsLoading &&
    typeof subscriptions !== 'undefined' &&
    !_.find(subscriptions, (sub) => {
      return sub.hasOwnProperty('isAmountMatched') && sub.isAmountMatched === false;
    });

  setCanApprove(check1 && check2 && check3 && (!useCheck5 || check5) && check6);

  return (
    <StyledList className={classes.root}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar
            className={!subscriptionsLoading ? (check1 ? classes.success : classes.fail) : ''}
          >
            {subscriptionsLoading ? <CircularProgress /> : check1 ? <CheckIcon /> : <TimesIcon />}
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="1: All Subscription Docs Have Been Signed"
          secondary={
            subscriptionsLoading
              ? 'The system is auditing Signatures…'
              : check1
              ? 'Passed'
              : 'Failed doc signing'
          }
        />
      </ListItem>
      {!check1 && subscriptions && (
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>E-mail</TableCell>
              <TableCell>Amount</TableCell>
              <TableCell>Nudge</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {subscriptions
              .filter((entry) => !entry.isDocsSigned)
              .map((investor: any) => (
                <TableRow>
                  <TableCell>{investor.name}</TableCell>
                  <TableCell>{investor.email}</TableCell>
                  <TableCell>{numeral(investor.amount).format('$0,0')}</TableCell>
                  <TableCell>
                    <EnvelopeIcon />
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      )}
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar
            className={!acouuntBalanceLoading ? (check2 ? classes.success : classes.fail) : ''}
          >
            {acouuntBalanceLoading ? <CircularProgress /> : check2 ? <CheckIcon /> : <TimesIcon />}
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="2: Closing Balance is Positive"
          secondary={
            acouuntBalanceLoading
              ? 'The system is checking that funds are available to close'
              : check2
              ? 'Passed'
              : 'Failed bank balance'
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar
            className={!subscriptionsLoading ? (check3 ? classes.success : classes.fail) : ''}
          >
            {subscriptionsLoading ? <CircularProgress /> : check3 ? <CheckIcon /> : <TimesIcon />}
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="3: Investor Count is Under 100 "
          secondary={
            subscriptionsLoading
              ? 'The system is validating deal limitations…'
              : check3
              ? 'Passed'
              : 'Deal is over subscribed'
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar
            className={!subscriptionsLoading ? (check4 ? classes.success : classes.fail) : ''}
          >
            {subscriptionsLoading ? <CircularProgress /> : check4 ? <CheckIcon /> : <TimesIcon />}
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="4: All Investors Have Passed KYC/AML Check"
          secondary={
            subscriptionsLoading
              ? 'The system is validating KYC/AML status…'
              : check4
              ? 'Passed'
              : "Failed KYC/AML'"
          }
        />
      </ListItem>
      {!check4 && subscriptions && (
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>E-mail</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Link</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {subscriptions
              .filter((entry) => entry.profile?.kycAml?.result !== 'CLEAR')
              .map((investor: any) => (
                <TableRow>
                  <TableCell>{investor.name}</TableCell>
                  <TableCell>{investor.email}</TableCell>
                  <TableCell>{investor?.profile?.kycAml?.result}</TableCell>
                  {investor?.profile?.kycAml?.checkId && (
                    <TableCell>
                      <ExternalLinkSquareAltIcon
                        onClick={() => {
                          window.open(
                            `https://dashboard.onfido.com/checks/${investor?.profile?.kycAml?.checkId}`,
                            '_blank'
                          );
                        }}
                      />
                    </TableCell>
                  )}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      )}
      {useCheck5 && (
        <React.Fragment>
          <Divider variant="inset" component="li" />
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar
                className={!subscriptionsLoading ? (check5 ? classes.success : classes.fail) : ''}
              >
                {subscriptionsLoading ? (
                  <CircularProgress />
                ) : check5 ? (
                  <CheckIcon />
                ) : (
                  <TimesIcon />
                )}
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary="5: Deal Balance must equal $0"
              secondary={
                subscriptionsLoading
                  ? 'The system is calculating deal balance…'
                  : check5
                  ? 'Passed'
                  : 'Deal balance is not $0'
              }
            />
          </ListItem>
        </React.Fragment>
      )}
      <React.Fragment>
        <Divider variant="inset" component="li" />
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar
              className={!subscriptionsLoading ? (check6 ? classes.success : classes.fail) : ''}
            >
              {subscriptionsLoading ? <CircularProgress /> : check6 ? <CheckIcon /> : <TimesIcon />}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={`${useCheck5 ? '6' : '5'}: Committed Amounts Match Subscription Amounts`}
            secondary={
              subscriptionsLoading
                ? 'The system is matching amounts…'
                : check6
                ? 'Passed'
                : 'Amounts do not match'
            }
          />
        </ListItem>
      </React.Fragment>
    </StyledList>
  );
};

export default AuditList;
