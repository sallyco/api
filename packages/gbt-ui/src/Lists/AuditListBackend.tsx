import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Avatar,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Table,
  Divider,
} from '@mui/material';
import { CheckIcon, TimesIcon } from 'react-line-awesome';
import numeral from 'numeral';

const PREFIX = 'AuditListBackend';

const classes = {
  root: `${PREFIX}-root`,
  success: `${PREFIX}-success`,
  fail: `${PREFIX}-fail`,
  inline: `${PREFIX}-inline`,
};

const StyledList = styled(List)(({ theme }) => ({
  [`&.${classes.root}`]: {
    backgroundColor: theme.palette.background.paper,
  },

  [`& .${classes.success}`]: {
    backgroundColor: 'green',
  },

  [`& .${classes.fail}`]: {
    backgroundColor: 'red',
  },

  [`& .${classes.inline}`]: {
    display: 'inline',
  },
}));

enum AuditChecks {
  ALL_SUBSCRIPTION_DOCS_SIGNED = 'ALL_SUBSCRIPTION_DOCS_SIGNED',
  POSITIVE_CLOSING_BALANCE = 'POSITIVE_CLOSING_BALANCE',
  INVESTOR_COUNT_LIMIT = 'INVESTOR_COUNT_LIMIT',
  SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH = 'SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH',
  ALL_INVESTORS_PASSED_KYC = 'ALL_INVESTORS_PASSED_KYC',
  ALL_INVESTORS_ACCREDITED = 'ALL_INVESTORS_ACCREDITED',
}

type AuditCheckType = `${AuditChecks}`;

const AuditChecksDescriptions = {
  [AuditChecks.ALL_SUBSCRIPTION_DOCS_SIGNED]: 'All Subscription Docs Have Been Signed',
  [AuditChecks.POSITIVE_CLOSING_BALANCE]: 'Closing Balance is Positive',
  [AuditChecks.INVESTOR_COUNT_LIMIT]: 'Investor Count is Under Limit',
  [AuditChecks.SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH]:
    'Committed Amounts Match Subscription Amounts',
  [AuditChecks.ALL_INVESTORS_PASSED_KYC]: 'All Investors Have Passed KYC/AML Check',
  [AuditChecks.ALL_INVESTORS_ACCREDITED]: 'All Investors are Accredited',
};

export type CheckResultMap = {
  [key in AuditChecks]?: CheckResult;
};

export interface AuditListBackendProps {
  auditData: CheckResultMap;
  setCanApprove: React.Dispatch<React.SetStateAction<boolean>>;
  useCheck5?: boolean;
}

interface CheckResult {
  status: 'PASS' | 'FAIL';
  failDetail?: string;
  data?: any;
}

const AuditListBackend = ({ auditData, setCanApprove }: AuditListBackendProps) => {
  setCanApprove(!Object.values(auditData).find((item) => item.status !== 'PASS'));

  return (
    <StyledList className={classes.root}>
      {Object.keys(auditData).map((key, index) => {
        const check = auditData[key as AuditCheckType];
        if (check) {
          return (
            <>
              <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar className={check.status === 'PASS' ? classes.success : classes.fail}>
                    {check.status === 'PASS' ? <CheckIcon /> : <TimesIcon />}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={`${index + 1}: ${AuditChecksDescriptions[key as AuditCheckType]}`}
                  secondary={
                    check.status === 'PASS' ? 'Passed' : check?.failDetail ?? check?.status
                  }
                />
              </ListItem>
              {check?.data?.subscriptions && (
                <>
                  {' '}
                  <ListItem>
                    <Table size="small">
                      <TableHead>
                        <TableRow>
                          <TableCell>Name</TableCell>
                          <TableCell>E-mail</TableCell>
                          <TableCell>Amount</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {check?.data?.subscriptions.map((investor: any) => (
                          <TableRow>
                            <TableCell>{investor.name}</TableCell>
                            <TableCell>{investor.email}</TableCell>
                            <TableCell>{numeral(investor.amount).format('$0,0')}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </ListItem>
                </>
              )}
              <Divider variant="inset" component="li" />
            </>
          );
        }
        return <></>;
      })}

      {/*<ListItem alignItems="flex-start">*/}
      {/*  <ListItemAvatar>*/}
      {/*    <Avatar*/}
      {/*      className={!subscriptionsLoading ? (check1 ? classes.success : classes.fail) : ''}*/}
      {/*    >*/}
      {/*      {subscriptionsLoading ? <CircularProgress /> : check1 ? <CheckIcon /> : <TimesIcon />}*/}
      {/*    </Avatar>*/}
      {/*  </ListItemAvatar>*/}
      {/*  <ListItemText*/}
      {/*    primary="1: All Subscription Docs Have Been Signed"*/}
      {/*    secondary={*/}
      {/*      subscriptionsLoading*/}
      {/*        ? 'The system is auditing Signatures…'*/}
      {/*        : check1*/}
      {/*        ? 'Passed'*/}
      {/*        : 'Failed doc signing'*/}
      {/*    }*/}
      {/*  />*/}
      {/*</ListItem>*/}
      {/*{!check1 && subscriptions && (*/}
      {/*  <Table size="small">*/}
      {/*    <TableHead>*/}
      {/*      <TableRow>*/}
      {/*        <TableCell>Name</TableCell>*/}
      {/*        <TableCell>E-mail</TableCell>*/}
      {/*        <TableCell>Amount</TableCell>*/}
      {/*        <TableCell>Nudge</TableCell>*/}
      {/*      </TableRow>*/}
      {/*    </TableHead>*/}
      {/*    <TableBody>*/}
      {/*      {subscriptions*/}
      {/*        .filter((entry) => !entry.isDocsSigned)*/}
      {/*        .map((investor: any) => (*/}
      {/*          <TableRow>*/}
      {/*            <TableCell>{investor.name}</TableCell>*/}
      {/*            <TableCell>{investor.email}</TableCell>*/}
      {/*            <TableCell>{numeral(investor.amount).format('$0,0')}</TableCell>*/}
      {/*            <TableCell>*/}
      {/*              <EnvelopeIcon />*/}
      {/*            </TableCell>*/}
      {/*          </TableRow>*/}
      {/*        ))}*/}
      {/*    </TableBody>*/}
      {/*  </Table>*/}
      {/*)}*/}
      {/*<Divider variant="inset" component="li" />*/}
      {/*<ListItem alignItems="flex-start">*/}
      {/*  <ListItemAvatar>*/}
      {/*    <Avatar*/}
      {/*      className={!acouuntBalanceLoading ? (check2 ? classes.success : classes.fail) : ''}*/}
      {/*    >*/}
      {/*      {acouuntBalanceLoading ? <CircularProgress /> : check2 ? <CheckIcon /> : <TimesIcon />}*/}
      {/*    </Avatar>*/}
      {/*  </ListItemAvatar>*/}
      {/*  <ListItemText*/}
      {/*    primary="2: Closing Balance is Positive"*/}
      {/*    secondary={*/}
      {/*      acouuntBalanceLoading*/}
      {/*        ? 'The system is checking that funds are available to close'*/}
      {/*        : check2*/}
      {/*        ? 'Passed'*/}
      {/*        : 'Failed bank balance'*/}
      {/*    }*/}
      {/*  />*/}
      {/*</ListItem>*/}
      {/*<Divider variant="inset" component="li" />*/}
      {/*<ListItem alignItems="flex-start">*/}
      {/*  <ListItemAvatar>*/}
      {/*    <Avatar*/}
      {/*      className={!subscriptionsLoading ? (check3 ? classes.success : classes.fail) : ''}*/}
      {/*    >*/}
      {/*      {subscriptionsLoading ? <CircularProgress /> : check3 ? <CheckIcon /> : <TimesIcon />}*/}
      {/*    </Avatar>*/}
      {/*  </ListItemAvatar>*/}
      {/*  <ListItemText*/}
      {/*    primary="3: Investor Count is Under 100 "*/}
      {/*    secondary={*/}
      {/*      subscriptionsLoading*/}
      {/*        ? 'The system is validating deal limitations…'*/}
      {/*        : check3*/}
      {/*        ? 'Passed'*/}
      {/*        : 'Deal is over subscribed'*/}
      {/*    }*/}
      {/*  />*/}
      {/*</ListItem>*/}
      {/*<Divider variant="inset" component="li" />*/}
      {/*<ListItem alignItems="flex-start">*/}
      {/*  <ListItemAvatar>*/}
      {/*    <Avatar*/}
      {/*      className={!subscriptionsLoading ? (check4 ? classes.success : classes.fail) : ''}*/}
      {/*    >*/}
      {/*      {subscriptionsLoading ? <CircularProgress /> : check4 ? <CheckIcon /> : <TimesIcon />}*/}
      {/*    </Avatar>*/}
      {/*  </ListItemAvatar>*/}
      {/*  <ListItemText*/}
      {/*    primary="4: All Investors Have Passed KYC/AML Check"*/}
      {/*    secondary={*/}
      {/*      subscriptionsLoading*/}
      {/*        ? 'The system is validating KYC/AML status…'*/}
      {/*        : check4*/}
      {/*        ? 'Passed'*/}
      {/*        : "Failed KYC/AML'"*/}
      {/*    }*/}
      {/*  />*/}
      {/*</ListItem>*/}
      {/*{!check4 && subscriptions && (*/}
      {/*  <Table size="small">*/}
      {/*    <TableHead>*/}
      {/*      <TableRow>*/}
      {/*        <TableCell>Name</TableCell>*/}
      {/*        <TableCell>E-mail</TableCell>*/}
      {/*        <TableCell>Status</TableCell>*/}
      {/*        <TableCell>Link</TableCell>*/}
      {/*      </TableRow>*/}
      {/*    </TableHead>*/}
      {/*    <TableBody>*/}
      {/*      {subscriptions*/}
      {/*        .filter((entry) => entry.profile?.kycAml?.result !== 'CLEAR')*/}
      {/*        .map((investor: any) => (*/}
      {/*          <TableRow>*/}
      {/*            <TableCell>{investor.name}</TableCell>*/}
      {/*            <TableCell>{investor.email}</TableCell>*/}
      {/*            <TableCell>{investor?.profile?.kycAml?.result}</TableCell>*/}
      {/*            {investor?.profile?.kycAml?.checkId && (*/}
      {/*              <TableCell>*/}
      {/*                <ExternalLinkSquareAltIcon*/}
      {/*                  onClick={() => {*/}
      {/*                    window.open(*/}
      {/*                      `https://dashboard.onfido.com/checks/${investor?.profile?.kycAml?.checkId}`,*/}
      {/*                      '_blank'*/}
      {/*                    );*/}
      {/*                  }}*/}
      {/*                />*/}
      {/*              </TableCell>*/}
      {/*            )}*/}
      {/*          </TableRow>*/}
      {/*        ))}*/}
      {/*    </TableBody>*/}
      {/*  </Table>*/}
      {/*)}*/}
      {/*{useCheck5 && (*/}
      {/*  <React.Fragment>*/}
      {/*    <Divider variant="inset" component="li" />*/}
      {/*    <ListItem alignItems="flex-start">*/}
      {/*      <ListItemAvatar>*/}
      {/*        <Avatar*/}
      {/*          className={!subscriptionsLoading ? (check5 ? classes.success : classes.fail) : ''}*/}
      {/*        >*/}
      {/*          {subscriptionsLoading ? (*/}
      {/*            <CircularProgress />*/}
      {/*          ) : check5 ? (*/}
      {/*            <CheckIcon />*/}
      {/*          ) : (*/}
      {/*            <TimesIcon />*/}
      {/*          )}*/}
      {/*        </Avatar>*/}
      {/*      </ListItemAvatar>*/}
      {/*      <ListItemText*/}
      {/*        primary="5: Deal Balance must equal $0"*/}
      {/*        secondary={*/}
      {/*          subscriptionsLoading*/}
      {/*            ? 'The system is calculating deal balance…'*/}
      {/*            : check5*/}
      {/*            ? 'Passed'*/}
      {/*            : 'Deal balance is not $0'*/}
      {/*        }*/}
      {/*      />*/}
      {/*    </ListItem>*/}
      {/*  </React.Fragment>*/}
      {/*)}*/}
      {/*<React.Fragment>*/}
      {/*  <Divider variant="inset" component="li" />*/}
      {/*  <ListItem alignItems="flex-start">*/}
      {/*    <ListItemAvatar>*/}
      {/*      <Avatar*/}
      {/*        className={!subscriptionsLoading ? (check6 ? classes.success : classes.fail) : ''}*/}
      {/*      >*/}
      {/*        {subscriptionsLoading ? <CircularProgress /> : check6 ? <CheckIcon /> : <TimesIcon />}*/}
      {/*      </Avatar>*/}
      {/*    </ListItemAvatar>*/}
      {/*    <ListItemText*/}
      {/*      primary={`${useCheck5 ? '6' : '5'}: Committed Amounts Match Subscription Amounts`}*/}
      {/*      secondary={*/}
      {/*        subscriptionsLoading*/}
      {/*          ? 'The system is matching amounts…'*/}
      {/*          : check6*/}
      {/*          ? 'Passed'*/}
      {/*          : 'Amounts do not match'*/}
      {/*      }*/}
      {/*    />*/}
      {/*  </ListItem>*/}
      {/*</React.Fragment>*/}
    </StyledList>
  );
};

export default AuditListBackend;
