import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

const PREFIX = 'CopyrightFooter';

const classes = {
  copyright: `${PREFIX}-copyright`,
};

const StyledTypography = styled(Typography)({
  [`& .${classes.copyright}`]: {
    fontSize: '0.6rem',
  },
});

const CopyrightFooter = () => {
  return (
    <StyledTypography variant={'caption'} className={classes.copyright}>
      {`© ${new Date().getFullYear()} Assure Services, Inc. All rights reserved. Glassboard is a registered trademark of MMAC, Inc. DBA Glassboard Technology`}
    </StyledTypography>
  );
};

export default CopyrightFooter;
