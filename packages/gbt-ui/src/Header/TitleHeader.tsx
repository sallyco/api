import * as React from 'react';
import { styled } from '@mui/material/styles';
import type { FC } from 'react';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

const PREFIX = 'TitleHeader';

const classes = {
  fullWidthTitleHeader: `${PREFIX}-fullWidthTitleHeader`,
  title: `${PREFIX}-title`,
  titleImage: `${PREFIX}-titleImage`,
};

const StyledBox = styled(Box)(({ theme }) => ({
  [`&.${classes.fullWidthTitleHeader}`]: {
    maxWidth: '100%',
  },

  [`& .${classes.title}`]: {
    marginTop: 'auto',
    marginBottom: 'auto',
    display: 'flex',
  },

  [`& .${classes.titleImage}`]: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

export interface TitleHeaderProps {
  image?: string | undefined;
  title: JSX.Element | string;
  children?: React.ReactNode;
}

const TitleHeader: FC<TitleHeaderProps> = ({ image = undefined, title, children }) => {
  return (
    <StyledBox pt={2} className={classes.fullWidthTitleHeader}>
      <Grid container justifyContent="space-between" wrap="nowrap" spacing={2}>
        <Grid item zeroMinWidth className={classes.title}>
          {image && <Avatar variant="square" className={classes.titleImage} src={image} />}
          <Typography variant="h1" component="h1" noWrap>
            {title}
          </Typography>
        </Grid>
        <Grid item style={{ marginTop: 'auto', marginBottom: 'auto' }}>
          {children}
        </Grid>
      </Grid>
      <Divider />
    </StyledBox>
  );
};

export default TitleHeader;
