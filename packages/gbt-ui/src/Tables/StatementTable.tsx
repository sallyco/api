import React, { useState } from 'react';
import Papa from 'papaparse';

import {
  Box,
  Card,
  LinearProgress,
  CardContent,
  CardHeader,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  Button,
} from '@mui/material';

// @ts-ignore
import EasyEdit from 'react-easy-edit';

import { DownloadIcon, AngleUpIcon, AngleDownIcon } from 'react-line-awesome';
import { ContentLoader } from '../';

const statementStrings = {
  description: 'Description',
  proceeds: 'Proceeds',
  expenses: 'Expenses',
  download: 'Download',
  subTotal: 'Sub Total',
};

const downloadLinkId = 'statement-download-link';

function convertRowArraysToCSV(data: string[][]) {
  return Papa.unparse(data);
}

function downloadCSVString(data: string) {
  const dataBlob = new Blob([data], {
    type: 'text/csv;charset=utf-8;',
  });
  const blobUrl = URL.createObjectURL(dataBlob);

  const downloadLink = document.getElementById(downloadLinkId);
  downloadLink?.setAttribute('href', blobUrl);
  downloadLink?.click();

  URL.revokeObjectURL(blobUrl);
}

function buildTitleRows(title: string, subTitle?: string) {
  const rows: string[][] = [];
  rows.push([title]);

  if (subTitle) {
    rows.push([subTitle]);
  }

  return rows;
}

function getSpacerRow() {
  return [''];
}

function getTimeStamp(createdAt: string) {
  let createdAtDate = new Date();
  if (createdAt) {
    createdAtDate = new Date(createdAt);
  }
  return createdAt
    ? `${createdAtDate.toDateString()} ${createdAtDate.getHours()}:${createdAtDate.getMinutes()}`
    : '';
}

function getStatementRows(statementData: StatementTableProps['statementData']) {
  const csvRows: string[][] = [];

  // Build header row of data
  // title row
  // subTitle row
  let titleRows = buildTitleRows(statementData.title, statementData.subTitle ?? '');
  titleRows.forEach((row) => {
    csvRows.push(row);
  });
  csvRows.push([statementData.createdAt]);
  csvRows.push(getSpacerRow());

  // Column Headers
  csvRows.push([
    statementStrings.description,
    statementStrings.proceeds,
    statementStrings.expenses,
  ]);

  // Build Sections
  for (const section of statementData.sections) {
    // Section titles
    csvRows.push(getSpacerRow());
    titleRows = buildTitleRows(section.title, section.subTitle ?? '');
    titleRows.forEach((titleRow) => {
      csvRows.push(titleRow);
    });

    // Each entry
    section.entries.forEach((entry) => {
      csvRows.push([entry.description, entry?.proceeds ?? '', entry?.expenses ?? '']);
    });

    // Optional subtotal entry
    if (section?.subTotal) {
      csvRows.push([statementStrings.subTotal, '', section.subTotal]);
    }
  }

  return csvRows;
}

export interface StatementTableProps {
  statementData: {
    title: string;
    subTitle?: string;
    sections: {
      title: string;
      subTitle?: string;
      entries: {
        description: string;
        proceeds?: string;
        expenses?: string;
        bold?: boolean;
        /**
         * isInvestorRow is really a known hack, and is messy.
         * If you provide it in multiple entries you will get
         * multiple rows rendering the inve data.
         *
         * If we make any other collapsing rows we'll need to revisit
         * */
        isInvestorRow?: boolean;
      }[];
      subTotal?: string;
    }[];
    // We are using 'state', this could be another country, and a region... etc
    // TODO: We need more clarification on what this will be in those cases
    investors: { name: string; state: string; amount: string }[];
    createdAt: string;
  };
  editHandler?: ((r: any, m: any) => void) | null;
  loading?: boolean;
  hideLoading?: boolean;
}
export default function StatementTable({
  statementData,
  editHandler = null,
  loading = false,
  hideLoading = false,
}: StatementTableProps) {
  const [investorsOpen, setInvestorsOpen] = useState(false);
  const toggleInvestors = () => setInvestorsOpen(!investorsOpen);
  const tableSpanLength = 3;
  const createdAtText = getTimeStamp(statementData?.createdAt ?? Date.now());

  // Build the rows that should show in the markup
  const tableRows: any[] = [];
  statementData?.sections?.forEach((section) => {
    tableRows.push(
      <TableRow key={section.title}>
        <TableCell
          style={{
            paddingTop: '24px',
            borderBottom: section?.subTitle ? 'none' : '',
          }}
          colSpan={tableSpanLength}
        >
          <strong>{section.title}</strong>
        </TableCell>
      </TableRow>
    );

    if (section?.subTitle) {
      tableRows.push(
        <TableRow key={section.subTitle}>
          <TableCell colSpan={tableSpanLength}>{section.subTitle}</TableCell>
        </TableRow>
      );
    }

    if (
      section?.entries?.map((entry) => {
        // Set if the row should be a clickable row to expand hidden content
        const toggleInvestorsObject: { onClick?: () => void } = {};
        if (entry?.isInvestorRow) {
          toggleInvestorsObject.onClick = toggleInvestors;
        }

        tableRows.push(
          <TableRow
            key={entry.description}
            {...toggleInvestorsObject}
            style={{
              cursor: entry?.isInvestorRow ? 'pointer' : '',
            }}
          >
            <TableCell style={{ paddingLeft: '32px', fontWeight: entry?.bold ? 'bold' : 'normal' }}>
              {entry.description}
              {!entry.isInvestorRow ? '' : investorsOpen ? <AngleUpIcon /> : <AngleDownIcon />}
            </TableCell>
            <TableCell style={{ fontWeight: entry?.bold ? 'bold' : 'normal' }} align="right">
              {entry?.proceeds ?? ''}
            </TableCell>
            <TableCell style={{ fontWeight: entry?.bold ? 'bold' : 'normal' }} align="right">
              {editHandler &&
              entry.expenses &&
              entry.description &&
              entry.description !== 'Total Amount Witheld From Investor Proceeds' ? (
                <EasyEdit
                  type="text"
                  onSave={(value: any) =>
                    editHandler(Number.parseFloat(value), {
                      field: entry.description,
                      category: section.title,
                    })
                  }
                  saveButtonLabel="Save"
                  cancelButtonLabel="Cancel"
                  value={entry?.expenses ?? ' '}
                  viewAttributes={{ style: { border: '1px dotted darkslategray' } }}
                  onValidate={(value: string) => {
                    // Allow a float number
                    const number = Number.parseFloat(value);
                    return !Number.isNaN(number);
                  }}
                  validationMessage="Enter a whole number for the expense"
                />
              ) : (
                entry?.expenses ?? ''
              )}
            </TableCell>
          </TableRow>
        );

        if (entry.isInvestorRow) {
          tableRows.push(
            <TableRow key={'investorrow'} style={{ display: investorsOpen ? '' : 'none' }}>
              <TableCell colSpan={tableSpanLength}>
                {investorsOpen && statementData?.investors?.length > 0 && (
                  <CardContent>
                    <TableContainer>
                      <Table size="small">
                        <TableHead>
                          <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>State</TableCell>
                            <TableCell>Amount</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {statementData.investors.map((investor) => (
                            <TableRow>
                              <TableCell>{investor.name}</TableCell>
                              <TableCell>{investor.state}</TableCell>
                              <TableCell>{investor.amount}</TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </CardContent>
                )}
              </TableCell>
            </TableRow>
          );
        }
      })
    )
      if (section?.subTotal) {
        tableRows.push(
          <TableRow key={section.subTotal + statementStrings.subTotal}>
            <TableCell style={{ paddingLeft: '32px' }}>{statementStrings.subTotal}</TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right">{section?.subTotal}</TableCell>
          </TableRow>
        );
      }
  });

  return (
    <>
      <Card>
        {!hideLoading && (
          <React.Fragment>
            {statementStrings && loading ? (
              <LinearProgress />
            ) : (
              <LinearProgress variant="determinate" value={100} />
            )}
          </React.Fragment>
        )}
        <div
          style={{ display: 'flex', justifyContent: 'space-between', textAlign: 'left' }}
          id="download header container"
        >
          <CardHeader
            title={statementData?.title}
            subheader={
              (statementData?.subTitle ?? '') +
              (statementData?.subTitle ? ` - Created ${createdAtText}` : createdAtText)
            }
          />
          <div style={{ padding: '16px' }}>
            <Button
              onClick={() => {
                const rows = getStatementRows(statementData);
                const csvString = convertRowArraysToCSV(rows);
                downloadCSVString(csvString);
              }}
              variant="outlined"
            >
              <DownloadIcon />
            </Button>
            <a download="closing-statement.csv" href="" id={downloadLinkId}></a>
          </div>
        </div>
        {!statementStrings && loading ? (
          <Box mb={4}>
            <ContentLoader size={24} message={'Getting statement...'} />
          </Box>
        ) : (
          <CardContent>
            <TableContainer>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>{statementStrings.description}</TableCell>
                    <TableCell align="right">{statementStrings.proceeds}</TableCell>
                    <TableCell align="right">{statementStrings.expenses}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tableRows.map((row) => {
                    return row;
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
        )}
      </Card>
    </>
  );
}
