import type { FC } from 'react';
import { styled } from '@mui/material/styles';
import { Typography } from '@mui/material';

const PREFIX = 'Header';

const classes = {
  root: `${PREFIX}-root`,
};

const Root = styled('div')(() => ({
  [`&.${classes.root}`]: {},
}));

export interface HeaderProps {
  title: string;
}

const Header: FC<HeaderProps> = ({ title = 'Kanban Board' }) => {
  return (
    <Root className={classes.root}>
      <Typography variant="h3" color="textPrimary">
        {title}
      </Typography>
    </Root>
  );
};

export default Header;
