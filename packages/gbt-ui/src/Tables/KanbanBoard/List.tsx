import React from 'react';
import { styled } from '@mui/material/styles';

//import { useSnackbar } from 'notistack';
import { Box, Divider, IconButton, Paper, SvgIcon, Typography } from '@mui/material';

import { EllipsisVIcon } from 'react-line-awesome';

import Card, { CardProps } from './Card';

const PREFIX = 'List';

const classes = {
  root: `${PREFIX}-root`,
  inner: `${PREFIX}-inner`,
  title: `${PREFIX}-title`,
  droppableArea: `${PREFIX}-droppableArea`,
  menu: `${PREFIX}-menu`,
};

const Root = styled('div')(({ theme }) => ({
  [`&.${classes.root}`]: {},

  [`& .${classes.inner}`]: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '100%',
    overflowY: 'hidden',
    overflowX: 'hidden',
    width: 380,
    [theme.breakpoints.down('sm')]: {
      width: 300,
    },
  },

  [`& .${classes.title}`]: {
    cursor: 'pointer',
  },

  [`& .${classes.droppableArea}`]: {
    minHeight: 80,
    flexGrow: 1,
    overflowY: 'auto',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },

  [`& .${classes.menu}`]: {
    width: 240,
  },
}));

export interface ListProps {
  id: string;
  title: string;
  cards?: CardProps[];
}

const List: React.FC<ListProps> = ({ id, title, cards = [] as CardProps[] }: ListProps) => {
  //  const { enqueueSnackbar } = useSnackbar();
  //const [name, setName] = React.useState<string>(title);
  //const [isRenaming, setRenaming] = React.useState<boolean>(false);

  //const handleMenuOpen = (): void => {
  //  setMenuOpen(true);
  //};

  ////  const handleMenuClose = (): void => {
  ////    setMenuOpen(false);
  ////  };

  //const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
  //  event.persist();
  //  setName(event.target.value);
  //};

  //const handleRenameInit = (): void => {
  //  setRenaming(true);
  //  setMenuOpen(false);
  //};

  //const handleRename = async (): Promise<void> => {
  //  try {
  //    if (!name) {
  //      setName(name);
  //      setRenaming(false);
  //      return;
  //    }

  //    //const update = { name };

  //    setRenaming(false);
  //    //await dispatch(updateList(list.id, update));
  //    //enqueueSnackbar('List updated', {
  //    //  variant: 'success'
  //    //});
  //  } catch (err) {
  //    console.error(err);
  //    //enqueueSnackbar('Something went wrong', {
  //    //  variant: 'error'
  //    //});
  //  }
  //};

  //const handleDelete = async (): Promise<void> => {
  //  try {
  //    setMenuOpen(false);
  //    //await dispatch(deleteList(list.id));
  //    //enqueueSnackbar('List deleted', {
  //    //  variant: 'success'
  //    //});
  //  } catch (err) {
  //    console.error(err);
  //    //enqueueSnackbar('Something went wrong', {
  //    //  variant: 'error'
  //    //});
  //  }
  //};

  //const handleClear = async (): Promise<void> => {
  //  try {
  //    setMenuOpen(false);
  //    //await dispatch(clearList(list.id));
  //    //enqueueSnackbar('List cleared', {
  //    //  variant: 'success'
  //    //});
  //  } catch (err) {
  //    console.error(err);
  //    //enqueueSnackbar('Something went wrong', {
  //    //  variant: 'error'
  //    //});
  //  }
  //};

  return (
    <Root className={classes.root}>
      <Paper className={classes.inner}>
        <Box py={1} px={2} display="flex" alignItems="center">
          <Typography color="inherit" variant="h5">
            {title}
          </Typography>
          <Box flexGrow={1} />
          <IconButton color="inherit" edge="end" size="large">
            <SvgIcon fontSize="small">
              <EllipsisVIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Divider />
        <div className={classes.droppableArea}>
          {cards.length > 0 ? (
            <React.Fragment>
              {cards.map((card: CardProps) => (
                <Card {...card} key={card.id} />
              ))}
            </React.Fragment>
          ) : (
            <Box m={1}>
              <Typography color="inherit" variant="h6" align="center">
                Empty
              </Typography>
            </Box>
          )}
        </div>
        <Divider />
      </Paper>
    </Root>
  );
};

export default List;
