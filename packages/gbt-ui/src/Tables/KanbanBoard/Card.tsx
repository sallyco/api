import * as React from 'react';
import { styled } from '@mui/material/styles';
import type { FC } from 'react';
import clsx from 'clsx';

import {
  Box,
  Button,
  Card as MuiCard,
  CardContent,
  CardMedia,
  CardActionArea,
  CardActions,
  Typography,
} from '@mui/material';

const PREFIX = 'Card';

const classes = {
  root: `${PREFIX}-root`,
  card: `${PREFIX}-card`,
  warning: `${PREFIX}-warning`,
  error: `${PREFIX}-error`,
  cover: `${PREFIX}-cover`,
  badge: `${PREFIX}-badge`
};

const Root = styled('div')((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {
    outline: 'none',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },

  [`& .${classes.card}`]: {
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
  },

  [`& .${classes.warning}`]: {
    backgroundColor: theme.palette.warning.light,
  },

  [`& .${classes.error}`]: {
    backgroundColor: theme.palette.error.light,
  },

  [`& .${classes.cover}`]: {
    height: 200,
  },

  [`& .${classes.badge}`]: {
    '& + &': {
      marginLeft: theme.spacing(2),
    },
  }
}));

export interface CardProps {
  id: string;
  listId: string;
  title: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  cover?: string | null;
  description?: string | null;
  footer?: string | null;
  className?: string | null;
}

const Card: FC<CardProps> = ({
  id,
  listId,
  title,
  onClick = undefined,
  cover,
  description,
  footer,
  className,
}: CardProps) => {


  let ccs = classes.card;

  if (className === 'error') {
    ccs = clsx(classes.card, classes.error);
  } else if (className === 'warning') {
    ccs = clsx(classes.card, classes.warning);
  }

  return (
    <Root className={classes.root} key={id} onClick={onClick}>
      <MuiCard className={ccs} variant={'outlined'}>
        <CardActionArea>
          {cover && <CardMedia className={classes.cover} image={cover} />}
          <CardContent>
            <Typography variant="h5" color="textPrimary">
              {title}
            </Typography>
            {description && (
              <Typography variant="body2" color="textSecondary" component="p">
                {description}
              </Typography>
            )}
            <Box mt={1} display="flex" alignItems="center" />
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small">{footer}</Button>
        </CardActions>
      </MuiCard>
    </Root>
  );
};

export default Card;
