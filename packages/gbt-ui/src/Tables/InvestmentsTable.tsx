import numeral from 'numeral';
import { Chip } from '@mui/material';
import TableWithTools from './TableWithTools';
import { useEffect, useState } from 'react';

import create from 'zustand';
import { persist } from 'zustand/middleware';

type ViewColumnsState = {
  viewColumns: string[];
  setViewColumns: (v: string, a: string) => void;
};

const useStore = create<ViewColumnsState>(
  // @ts-ignore
  persist(
    (set) => ({
      viewColumns: [] as Array<string>,
      setViewColumns: (v, a) =>
        set((state) => {
          if (a === 'add') {
            if (state.viewColumns.includes(v)) {
              return { viewColumns: [...state.viewColumns] };
            }
            return { viewColumns: [...state.viewColumns, v] };
          } else if (a === 'remove') {
            return { viewColumns: state.viewColumns.filter((c) => c !== v) };
          }
          return { viewColumns: [...state.viewColumns] };
        }),
    }),
    {
      name: 'investments-table-columns',
    }
  )
);

export interface InvestmentsTableProps {
  data: any;
  hiddenColumns?: string[];
  rowsPerPage?: number;
  rowsSelected?: number[];
  elevation?: number;
  title?: JSX.Element | string;
  selectable?: boolean;
  clickable?: boolean;
  columnIndexMapping?: Map<string, number> | undefined;
  receivedFundsValue?: number;
  statusFunctions?: {
    statusText: (s: any) => string;
    statusColor: (
      s: string
    ) =>
      | 'teal'
      | 'red'
      | 'orange'
      | 'yellow'
      | 'olive'
      | 'green'
      | 'blue'
      | 'violet'
      | 'purple'
      | 'pink'
      | 'brown'
      | 'grey'
      | 'black'
      | undefined;
    statusOrder: (s: string) => number;
  };
  onRowSelectionChange?: ((c: any[], a: any[], r: any[]) => void) | null;
  customToolbarSelect?: ((s: any) => void) | null;
  onRowClick?: ((r: any, m: any) => void) | null;
  checkQualifiedPurchaser?: ((p: any) => boolean) | null;
  actionMenu?: ((r: any) => JSX.Element) | undefined;
  formatKycStatus?: ((r: any) => JSX.Element) | undefined;
  formatIdentityStatus?: ((r: any) => JSX.Element) | undefined;
  isReceivedStatus?: ((s: any) => boolean) | undefined;
  additionalColumnDefinitions?: any[];
  showDefaultDownload?: boolean;
  showDefaultFilter?: boolean;
  overrideDefaultDownload?:
    | ((buildHead: any, buildBody: any, columns: any, data: any) => void)
    | null;
  checkAccreditationStatus?: ((p: any) => any) | null;
  onSideLetterClick?: ((colData: any, cellMeta: any) => void) | null;
}

const relativeLuminance = (R8bit: any, G8bit: any, B8bit: any) => {
  const RsRGB = R8bit / 255.0;
  const GsRGB = G8bit / 255.0;
  const BsRGB = B8bit / 255.0;

  const R = RsRGB <= 0.03928 ? RsRGB / 12.92 : Math.pow((RsRGB + 0.055) / 1.055, 2.4);
  const G = GsRGB <= 0.03928 ? GsRGB / 12.92 : Math.pow((GsRGB + 0.055) / 1.055, 2.4);
  const B = BsRGB <= 0.03928 ? BsRGB / 12.92 : Math.pow((BsRGB + 0.055) / 1.055, 2.4);

  return 0.2126 * R + 0.7152 * G + 0.0722 * B;
};

const blackContrast = (r: any, g: any, b: any) => {
  const L = relativeLuminance(r, g, b);
  return (L + 0.05) / 0.05;
};

const whiteContrast = (r: any, g: any, b: any) => {
  const L = relativeLuminance(r, g, b);
  return 1.05 / (L + 0.05);
};

const chooseFGcolor = (color: any, prefer = 'white') => {
  const hex = color.replace(/#/, '');
  const r = parseInt(hex.substr(0, 2), 16);
  const g = parseInt(hex.substr(2, 2), 16);
  const b = parseInt(hex.substr(4, 2), 16);

  const Cb = blackContrast(r, g, b);
  const Cw = whiteContrast(r, g, b);
  if (Cb >= 7.0 && Cw >= 7.0) return prefer;
  else return Cb > Cw ? 'black' : 'white';
};

const hexCodes: any = {
  teal: '#008080',
  red: '#FF0000',
  orange: '#FFA500',
  yellow: '#FFFF00',
  olive: '#808000',
  green: '#008000',
  blue: '#0000FF',
  violet: '#EE82EE',
  purple: '#800080',
  pink: '#FFC0CB',
  brown: '#A52A2A',
  grey: '#808080',
  black: '#000000',
};

const getHexCodeForColor = (name: string | undefined) => {
  if (name && name.indexOf('#') > -1) return name;
  return !name || !Object.keys(hexCodes).includes(name) ? '#FFFFFF' : hexCodes[name];
};

const InvestmentsTable: React.FC<InvestmentsTableProps> = ({
  data,
  hiddenColumns = [],
  rowsSelected = [],
  elevation = undefined,
  title = '',
  selectable = false,
  clickable = false,
  columnIndexMapping = undefined,
  receivedFundsValue = undefined,
  statusFunctions = undefined,
  onRowSelectionChange = null,
  customToolbarSelect = null,
  onRowClick = null,
  checkQualifiedPurchaser = null,
  actionMenu = undefined,
  formatKycStatus = undefined,
  formatIdentityStatus = undefined,
  isReceivedStatus = undefined,
  additionalColumnDefinitions = [],
  showDefaultDownload = true,
  showDefaultFilter = true,
  overrideDefaultDownload = null,
  checkAccreditationStatus = undefined,
  onSideLetterClick = null,
}: InvestmentsTableProps) => {
  const idColumnIndex = columnIndexMapping && columnIndexMapping!.get('id');
  const profileColumnIndex = columnIndexMapping && columnIndexMapping!.get('profile');
  const sideLetterColumnIndex = columnIndexMapping && columnIndexMapping!.get('sideLetter');

  const viewColumns = useStore((state) => state.viewColumns);
  const setViewColumns = useStore((state) => state.setViewColumns);

  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    ...(idColumnIndex !== undefined && !hiddenColumns.includes('#')
      ? [
          {
            name: '#',
            options: {
              display: true,
              filter: false,
              download: false,
              customBodyRender: (value: any, tableMeta: any) => {
                const rowNumber = data.findIndex(
                  (x: any) => x.id === tableMeta.rowData[idColumnIndex]
                );
                return rowNumber > -1 ? rowNumber + 1 : '-';
              },
              sortCompare: (order: any) => {
                return (obj1: any, obj2: any) => {
                  const obj1Index = data.findIndex(
                    (x: any) => x.id === obj1.rowData[idColumnIndex]
                  );
                  const obj2Index = data.findIndex(
                    (x: any) => x.id === obj2.rowData[idColumnIndex]
                  );

                  return obj1Index > obj2Index && order === 'asc' ? 1 : -1;
                };
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('id')
      ? [
          {
            name: 'id',
            options: {
              display: false,
              filter: false,
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('name')
      ? [
          {
            name: 'name',
            label: 'Name',
            options: {
              filter: true,
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('tenantId')
      ? [
          {
            name: 'tenantId',
            label: 'Tenant',
            options: {
              filter: true,
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('entityName')
      ? [
          {
            name: 'profile.name',
            label: 'Entity',
            options: {
              filter: true,
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => value ?? `---`,
            },
          },
        ]
      : []),
    ...(profileColumnIndex && !hiddenColumns.includes('accreditationType')
      ? [
          {
            name: 'accreditationType',
            label: 'Accreditation',
            options: {
              hint: 'The Investor Accreditation Type',
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                if (checkAccreditationStatus) {
                  const profile = tableMeta.rowData[profileColumnIndex];
                  return checkAccreditationStatus(profile);
                }
                return '';
              },
            },
          },
        ]
      : []),
    ...(checkQualifiedPurchaser && profileColumnIndex && !hiddenColumns.includes('profileType')
      ? [
          {
            name: 'profileType',
            label: 'Type',
            options: {
              hint: 'QP: The investor is using a Qualified Purchaser profile',
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const profile = tableMeta.rowData[profileColumnIndex];
                if (checkQualifiedPurchaser(profile)) {
                  return 'QP';
                }
                return '---';
              },
              sortCompare: (order: any) => {
                return (obj1: any, obj2: any) => {
                  // Search the large list once
                  const profile1 = obj1.rowData[profileColumnIndex];
                  const profile2 = obj2.rowData[profileColumnIndex];

                  const prof1IsQP = checkQualifiedPurchaser(profile1);
                  const prof2IsQP = checkQualifiedPurchaser(profile2);

                  return prof1IsQP > prof2IsQP && order === 'asc' ? 1 : -1;
                };
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('email')
      ? [
          {
            name: 'email',
            label: 'E-mail',
            options: {
              filter: true,
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('phone')
      ? [
          {
            name: 'profile',
            label: 'Phone',
            options: {
              filter: true,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                return value?.phone ?? `---`;
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('amount')
      ? [
          {
            name: 'amount',
            label: 'Amount',
            options: {
              filter: true,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                return value ? (
                  <div style={{ color: 'green' }}>{numeral(value).format('$0,0')}</div>
                ) : (
                  `---`
                );
              },
            },
          },
        ]
      : []),
    ...(idColumnIndex !== undefined &&
    receivedFundsValue &&
    isReceivedStatus &&
    !hiddenColumns.includes('ownership')
      ? [
          {
            name: 'ownership',
            label: '% Ownership',
            options: {
              filter: true,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const rowData = data.find((x: any) => x.id === tableMeta.rowData[idColumnIndex]);
                const amount = isReceivedStatus(rowData) && rowData?.amount ? rowData.amount : 0;

                return amount && receivedFundsValue > 0 ? (
                  <div style={{ color: 'green' }}>
                    {numeral(amount / receivedFundsValue).format('0,0.0000%')}
                  </div>
                ) : (
                  `---`
                );
              },
            },
          },
        ]
      : []),
    ...(formatIdentityStatus && !hiddenColumns.includes('identityStatus')
      ? [
          {
            name: 'profile.kycAml',
            label: 'Identity Check',
            options: {
              filter: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const record = data[tableMeta.currentTableData[tableMeta.rowIndex].index].profile;
                return formatIdentityStatus(record);
              },
            },
          },
        ]
      : []),
    ...(formatKycStatus && !hiddenColumns.includes('kycStatus')
      ? [
          {
            name: 'profile.kycAml',
            label: 'KYC Check',
            options: {
              filter: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const record = data[tableMeta.currentTableData[tableMeta.rowIndex].index].profile;
                return formatKycStatus(record);
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('investorType')
      ? [
          {
            name: 'profile.taxDetails',
            label: 'Investor Type',
            options: {
              filter: false,
              display: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                return value?.registrationType ?? `---`;
              },
            },
          },
        ]
      : []),
    ...(idColumnIndex !== undefined && statusFunctions && !hiddenColumns.includes('status')
      ? [
          {
            name: 'status',
            label: 'Status',
            options: {
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const rowData = data.find((x: any) => x.id === tableMeta.rowData[idColumnIndex]);
                const statusText = statusFunctions.statusText(rowData);
                const bgColor = statusFunctions.statusColor(statusText);
                const foreColor = chooseFGcolor(getHexCodeForColor(bgColor));
                return (
                  <Chip style={{ backgroundColor: bgColor, color: foreColor }} label={statusText} />
                );
              },
              sortCompare: (order: any) => {
                return (obj1: any, obj2: any) => {
                  // Search the large list once
                  const wantedSubscriptions = data.filter(
                    (x: any) =>
                      x.id === obj1.rowData[idColumnIndex] || x.id === obj2.rowData[idColumnIndex]
                  );
                  const sub1 = wantedSubscriptions.find(
                    (x: any) => x.id === obj1.rowData[idColumnIndex]
                  );
                  const sub2 = wantedSubscriptions.find(
                    (x: any) => x.id === obj2.rowData[idColumnIndex]
                  );
                  const compared =
                    statusFunctions.statusOrder(statusFunctions.statusText(sub1)) >
                      statusFunctions.statusOrder(statusFunctions.statusText(sub2)) &&
                    order === 'asc';

                  return compared ? 1 : -1;
                };
              },
              customFilterListOptions: {
                render: (v: any) => `Status: ${v}`,
              },
            },
          },
        ]
      : []),
    ...(idColumnIndex !== undefined && !hiddenColumns.includes('signingStatus')
      ? [
          {
            name: 'signing',
            label: 'Signing Status',
            options: {
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const rowData = data.find((x: any) => x.id === tableMeta.rowData[idColumnIndex]);
                if (
                  rowData?.signers &&
                  rowData.signers.length > 0 &&
                  rowData.status !== 'CANCELLED'
                ) {
                  const notSigned = (rowData.signers ?? []).filter(
                    (signer: any) => !signer.dateSigned
                  );
                  return (
                    <Chip
                      style={{
                        backgroundColor: notSigned.length > 0 ? 'blue' : 'green',
                        color: 'white',
                      }}
                      label={
                        notSigned.length > 0
                          ? `Needs Co-Signer ${notSigned.length} of ${
                             rowData?.signers?.length
                            }`
                          : `${rowData?.signers?.length} of ${rowData?.signers?.length} Signed`
                      }
                    />
                  );
                }
                return <></>;
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('note')
      ? [
          {
            name: 'additionalProperties',
            label: 'Note',
            options: {
              filter: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                return value?.organizersNote;
              },
            },
          },
        ]
      : []),
    ...(!hiddenColumns.includes('sideLetter')
      ? [
          {
            name: 'sideLetter',
            label: 'Side Letter',
            options: {
              filter: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (sideLetter: any, tableMeta: any, updateValue: any) => {
                if (!sideLetter) {
                  return '';
                }
                return (
                  <>
                    {sideLetter.managementFee && (
                      <>
                        <span>
                          <b>Management Fee:</b>
                          {sideLetter.managementFee.type === 'flat' ? '$' : ''}
                          {sideLetter.managementFee.amount}
                          {sideLetter.managementFee.type === 'percent' ? '%' : ''}
                        </span>
                        <br />
                      </>
                    )}
                    {sideLetter.carryPercent &&
                      Object.keys(sideLetter.carryPercent).map((k) => {
                        return (
                          <>
                            <span>
                              <b>{k === 'organizerCarryPercentage' ? 'Organizer Carry' : k}:</b>
                              {sideLetter.carryPercent?.[k] ?? 0}%
                            </span>
                            <br />
                          </>
                        );
                      })}
                  </>
                );
              },
            },
          },
        ]
      : []),
    ...(idColumnIndex !== undefined && actionMenu && !hiddenColumns.includes('actions')
      ? [
          {
            name: 'actions',
            label: 'Actions',
            // eslint-disable-next-line react/display-name
            options: {
              filter: false,
              viewColumns: false,
              // eslint-disable-next-line react/display-name
              customBodyRender: (value: any, tableMeta: any, updateValue: any) => {
                const rowData = data.find((x: any) => x.id === tableMeta.rowData[idColumnIndex]);
                return actionMenu(rowData);
              },
            },
          },
        ]
      : []),
    ...additionalColumnDefinitions,
  ];

  const actionFieldLabels = [{ name: 'actions', label: 'Actions' }];

  const getColumnDefinition = (): any[] => {
    const colDef = initialColumnDefinitions;
    for (const def of colDef) {
      if (
        actionFieldLabels.filter((field) => field.name === def.name && field.label === def.label)
          .length > 0
      )
        continue;

      const tempViewColumns = [...viewColumns];
      if (
        tempViewColumns &&
        tempViewColumns.length === 0 &&
        (def.options?.display === undefined || def.options?.display === true)
      ) {
        setViewColumns(def.name, 'add');
      }

      if (tempViewColumns && tempViewColumns.length > 0) {
        if (tempViewColumns.includes(def.name)) {
          def.options = {
            ...(def.options ?? {}),
            display: true,
          };
        } else {
          def.options = {
            ...(def.options ?? {}),
            display: false,
          };
        }
      }
    }

    return colDef;
  };

  const onViewColumnsChange = (v: string, a: string): any[] => {
    setViewColumns(v, a);

    const colDef = columnDefinitions;
    const index = colDef.findIndex(
      (c: any) =>
        c.name === v &&
        actionFieldLabels.filter((field) => field.name === c.name && field.label === c.label)
          .length === 0
    );

    colDef[index]['options'] = {
      ...(colDef[index]['options'] ?? {}),
      display: a === 'add' ? true : false,
    };

    return colDef;
  };

  useEffect(() => {
    const colDef = getColumnDefinition();
    setColumnDefinitions(colDef);
  }, [data]);

  const viewColumnsChangeHandler = (v: string, a: string) => {
    const colDef = onViewColumnsChange(v, a);
    setColumnDefinitions(colDef);
    return;
  };

  const onCellClick = (
    colData: any,
    cellMeta: { colIndex: number; rowIndex: number; dataIndex: number }
  ) => {
    if (cellMeta.colIndex === sideLetterColumnIndex) {
      if (onSideLetterClick) {
        onSideLetterClick(colData, cellMeta);
      }
    }
  };

  return (
    <>
      <TableWithTools
        data-testid={'investments-table'}
        data={data}
        columnDefinitions={columnDefinitions}
        rowsSelected={rowsSelected}
        elevation={elevation}
        title={title}
        selectable={selectable}
        clickable={clickable}
        onRowClick={onRowClick}
        onRowSelectionChange={onRowSelectionChange}
        customToolbarSelect={customToolbarSelect}
        onViewColumnsChange={viewColumnsChangeHandler}
        enableNestedDataAccess={'.'}
        {...{ download: showDefaultDownload }}
        {...{ filter: showDefaultFilter }}
        {...(overrideDefaultDownload ? { onDownload: overrideDefaultDownload } : {})}
        onCellClick={onCellClick}
      />
    </>
  );
};

export default InvestmentsTable;
