import * as React from 'react';
import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import _ from 'lodash';

import { Header, CardProps, List, ListProps } from './KanbanBoard';

const PREFIX = 'PseudoKanbanBoard';

const classes = {
  root: `${PREFIX}-root`,
  content: `${PREFIX}-content`,
  inner: `${PREFIX}-inner`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    height: '100%',
    display: 'flex',
    overflow: 'hidden',
    flexDirection: 'column',
  },

  [`& .${classes.content}`]: {
    flexGrow: 1,
    flexShrink: 1,
    display: 'flex',
    overflowY: 'hidden',
    overflowX: 'auto',
  },

  [`& .${classes.inner}`]: {
    display: 'flex',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
}));

export interface PseudoKanbanBoardProps {
  title?: string | undefined;
  cards: CardProps[];
  lists: ListProps[];
}

const PseudoKanbanBoard: React.FC<PseudoKanbanBoardProps> = ({
  title = undefined,
  cards,
  lists,
}) => {
  return (
    <Root>
      {title && (
        <Box p={3}>
          <Header title={title} />
        </Box>
      )}
      <div className={classes.content}>
        <div className={classes.inner}>
          {lists.map((list: ListProps) => (
            <List
              id={list.id}
              key={list.id}
              title={list.title}
              cards={_.filter(cards, { listId: list.id })}
            />
          ))}
        </div>
      </div>
    </Root>
  );
};

export default PseudoKanbanBoard;
