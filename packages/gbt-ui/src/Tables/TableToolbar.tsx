import React from 'react';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import { SyncIcon } from 'react-line-awesome';

const PREFIX = 'TableToolbar';

const classes = {
  iconButton: `${PREFIX}-iconButton`,
  rotateIcon: `${PREFIX}-rotateIcon`,
};

const Root = styled('span')(({ theme }) => ({
  [`& .${classes.iconButton}`]: {},

  [`& .${classes.rotateIcon}`]: {
    animation: 'spin 1s linear infinite',
    color: theme.palette.secondary.main,
  },
}));

export interface TableToolbarProps {
  isLoading: boolean;
}

const TableToolbar: React.FC<TableToolbarProps> = ({ isLoading = false }) => {
  return (
    <Root>
      <Tooltip title={'Refresh Data'}>
        <IconButton disabled className={classes.iconButton} size="large">
          <SyncIcon className={isLoading ? classes.rotateIcon : ''} />
        </IconButton>
      </Tooltip>
      <style>{`
            @keyframes spin {
                 0% { transform: rotate(0deg); }
                 100% { transform: rotate(360deg); }
            }
        `}</style>
    </Root>
  );
};

export default TableToolbar;
