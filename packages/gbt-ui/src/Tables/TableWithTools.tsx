import React from 'react';

import MUIDataTable from 'mui-datatables';
//import {
//  createTheme,
//  Theme,
//  useTheme,
//  ThemeProvider,
//  StyledEngineProvider,
//} from '@mui/material/styles';

import TableSearch from './TableSearch';
import TableToolbar from './TableToolbar';

import create from 'zustand';
import { persist, combine } from 'zustand/middleware';

import { SearchIcon, PrintIcon, DownloadIcon, ColumnsIcon, FilterIcon } from 'react-line-awesome';

export interface TableProps {
  data: any;
  columnDefinitions: any;
  elevation?: number;
  title?: JSX.Element | string;
  selectable?: boolean;
  clickable?: boolean;
  onRowClick?: ((r: any, m: any) => void) | null;
  showLoadking?: boolean;
  isLoading?: boolean;

  [x: string]: any;
}

//const getMuiTheme = (clickable: boolean) => {
//  const theme = useTheme<Theme>();
//  return createTheme({
//    ...theme,
//    //components: {
//    //  MUIDataTable: {
//    //    styleOverrides: {
//    //      paper: {
//    //        borderRadius: 0,
//    //        boxShadow: 'none',
//    //      },
//    //    },
//    //  },
//    //  MUIDataTableBodyRow: {
//    //    styleOverrides: {
//    //      root: {
//    //        cursor: (clickable: boolean) => (clickable ? 'pointer' : ''),
//    //      },
//    //    },
//    //  },
//    //  MUIDataTableHeadCell: {
//    //    styleOverrides: {
//    //      root: {
//    //        paddingTop: '8px !important',
//    //        paddingBottom: '4px !important',
//    //        fontWeight: 700,
//    //      },
//    //    },
//    //  },
//    //  MUIDataTableBodyCell: {
//    //    styleOverrides: {
//    //      root: {
//    //        paddingTop: '8px !important',
//    //        paddingBottom: '8px !important',
//    //      },
//    //    },
//    //  },
//    //},
//  });
//};

const useStore = create(
  persist(
    combine({ rowsPerPage: 50 }, (set) => ({
      setRowsPerPage: (v: number) => set(() => ({ rowsPerPage: v })),
    })),
    {
      name: 'table-with-tools',
    }
  )
);

const TableWithTools: React.FC<TableProps> = ({
  data,
  title = '',
  elevation = 3,
  columnDefinitions,
  selectable = true,
  clickable = false,
  selectToolbar = null,
  onRowClick = null,
  showLoading = false,
  isLoading = false,
  ...rest
}: TableProps) => {
  //useStyles(clickable);
  const rowsPerPage = useStore((state) => state.rowsPerPage);
  const setRowsPerPage = useStore((state) => state.setRowsPerPage);

  return (
    <MUIDataTable
      columns={columnDefinitions}
      data={data}
      title={title}
      options={{
        ...(clickable && onRowClick && { onRowClick }),
        textLabels: {
          body: {
            noMatch: 'No matching records found',
          },
        },
        selectableRows: selectable ? 'multiple' : 'none',
        selectableRowsHideCheckboxes: !selectable,
        elevation: elevation,
        fixedHeader: true,
        print: false,
        responsive: 'standard',
        filterType: 'multiselect',
        rowsPerPage: rowsPerPage,
        rowsPerPageOptions: [10, 50, 100],
        onChangeRowsPerPage: (v) => setRowsPerPage(v),
        //        onFilterChange: (c, filterList) => console.log(filterList),
        customToolbar: () => {
          return showLoading && <TableToolbar isLoading={isLoading} />;
        },
        customSearchRender: function Fn(searchText, handleSearch, hideSearch, options) {
          return (
            <TableSearch
              searchText={searchText}
              onSearch={handleSearch}
              onHide={hideSearch}
              options={options}
            />
          );
        },
        ...rest,
      }}
      components={{
        icons: {
          SearchIcon,
          PrintIcon,
          DownloadIcon,
          ViewColumnIcon: ColumnsIcon,
          FilterIcon,
        },
      }}
    />
  );
};

export default TableWithTools;
