import React from 'react';

import { styled } from '@mui/material/styles';
import Grow from '@mui/material/Grow';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import { TimesCircleIcon } from 'react-line-awesome';

const PREFIX = 'TableSearch';

const classes = {
  main: `${PREFIX}-main`,
  searchText: `${PREFIX}-searchText`,
  clearIcon: `${PREFIX}-clearIcon`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.main}`]: {
    display: 'flex',
    flex: '1 0 auto',
  },

  [`& .${classes.searchText}`]: {
    flex: '0.8 0',
  },

  [`& .${classes.clearIcon}`]: {
    '&:hover': {
      color: theme.palette.error.main,
    },
  },
}));

export interface TableSearchProps {
  searchText: string;
  onSearch: any;
  onHide: any;
  options: any;
}

const TableSearch: React.FC<TableSearchProps> = ({ searchText, onSearch, onHide, options }) => {
  const handleTextChange = (e: any) => {
    onSearch(e.target.value);
  };

  return (
    <Root>
      <Grow appear in={true} timeout={300}>
        <div className={classes.main}>
          <TextField
            placeholder={'Search'}
            className={classes.searchText}
            InputProps={{
              'aria-label': options.textLabels.toolbar.search,
            }}
            value={searchText || ''}
            onChange={handleTextChange}
            fullWidth={true}
            margin={'dense'}
            size={'small'}
            autoFocus
          />
          <IconButton className={classes.clearIcon} onClick={onHide} size="large">
            <TimesCircleIcon />
          </IconButton>
        </div>
      </Grow>
    </Root>
  );
};

export default TableSearch;
