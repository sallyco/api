import * as React from 'react';

import { styled } from '@mui/material/styles';

import {
  Card,
  Divider,
  Checkbox,
  CardContent,
  IconButton,
  CardHeader,
  Link,
  List,
  ListItem,
  Typography,
} from '@mui/material';
import TreeView from '@mui/lab/TreeView';
import TreeItem from '@mui/lab/TreeItem';
import { PencilSquareIcon, AngleRightIcon, AngleDownIcon } from 'react-line-awesome';

import { alpha } from '@mui/material/styles';
import { toCapitalizedWords } from '../tools/textFormatters';

import moment from 'moment';

const PREFIX = 'InfoCard';

const classes = {
  root: `${PREFIX}-root`,
  header: `${PREFIX}-header`,
  content: `${PREFIX}-content`,
  listItem: `${PREFIX}-listItem`,
  treeItem: `${PREFIX}-treeItem`,
  sublistItem: `${PREFIX}-sublistItem`,
  sublistDivider: `${PREFIX}-sublistDivider`,
  checkbox: `${PREFIX}-checkbox`
};

const StyledCard = styled(Card)((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {},

  [`& .${classes.header}`]: {
    paddingBottom: theme.spacing(1),
    paddingTop: theme.spacing(1.4),
  },

  [`& .${classes.content}`]: {
    paddingTop: 0,
  },

  [`& .${classes.listItem}`]: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },

  [`& .${classes.treeItem}`]: {
    paddingLeft: theme.spacing(1),
    borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`,
  },

  [`& .${classes.sublistItem}`]: {
    paddingLeft: theme.spacing(1),
    justifyContent: 'space-between',
  },

  [`& .${classes.sublistDivider}`]: {
    marginTop: theme.spacing(1),
  },

  [`& .${classes.checkbox}`]: {
    padding: 0,
  }
}));

export interface InfoCardProps {
  item: any;
  fields?: string[] | undefined;
  omitFields?: string[] | undefined;
  linkfields?: Map<string, { value_field: string; url_field: string }> | undefined;
  title?: string | undefined;
  icon?: JSX.Element | undefined;
  editFields?: Map<string, { editFunc: () => {} }> | undefined;
  titleChanges?: Map<string, { title: string; subTitle?: string }> | undefined;
}

const LinkField = ({ item, title, data }: { item: any; title: string; data: any }) => {
  let name = item;
  if (data?.name) {
    name = data.name;
  } else if (data?.firstName || data?.lastName) {
    name = `${data?.firstName} ${data?.lastName}`.trim();
  }
  return <Link href={`/${title}/${item}`}>{name}</Link>;
};
const checkAndRenderImage = (value: string) => {
  if (value.match(/^(http|\/|.\/|..\/).*?\.(jpeg|jpg|gif|png)$/) !== null)
    return <img src={value} style={{ maxWidth: '220px', maxHeight: '100px' }} />;
  else if (value.match(/data:image\/([a-zA-Z]*);base64,([^\"]*)/g) !== null)
    return (
      <img
        src={value}
        style={{
          maxWidth: '220px',
          maxHeight: '100px',
          background: 'repeating-linear-gradient(45deg,#ffffff00, #00000007 2%,#ffffff00 1%)',
        }}
      />
    );
  else if (
    value.match(/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/) !== null
  )
    return `${moment.utc(value).local().format('lll')} (${moment.utc(value).local().fromNow()})`;
  else return value;
};

const renderObject = (item: any, level: number) =>
  Object.keys(item).map((key) => (
    <div key={key + level}>
      {typeof item[key] !== 'object' || item[key] === null ? (
        <ListItem className={classes.sublistItem} disableGutters>
          <Typography variant="caption" color="textPrimary">
            {toCapitalizedWords(key)}
          </Typography>
          <Typography
            variant={item[key] === null ? 'overline' : 'caption'}
            color="textSecondary"
            style={{
              padding: '0px 0px 0px 30px',
              textAlign: 'right',
              overflowWrap: 'anywhere',
            }}
          >
            {render(key, item[key] ?? 'not set')}
          </Typography>
        </ListItem>
      ) : (
        <React.Fragment>
          <TreeItem
            className={classes.treeItem}
            nodeId={key + level}
            label={
              <Typography variant="caption" color="textPrimary">
                {toCapitalizedWords(key)}
              </Typography>
            }
          >
            {renderObject(item[key], level + 1)}
          </TreeItem>
        </React.Fragment>
      )}
    </div>
  ));

const render = (key: any, value: any) => {


  switch (typeof value) {
    case 'string':
      return checkAndRenderImage(value);
    case 'boolean':
      return <Checkbox checked={value} disabled name="key" className={classes.checkbox} />;
    case 'number':
      return value;
    default:
      return value;
  }
};

const InfoCard: React.FC<InfoCardProps> = ({
  item,
  fields = undefined,
  omitFields = undefined,
  linkfields = undefined,
  title = 'Details',
  icon = undefined,
  editFields = undefined,
  titleChanges = undefined,
}) => {


  return (
    <StyledCard className={classes.root}>
      {title && (
        <React.Fragment>
          <CardHeader
            avatar={icon}
            className={classes.header}
            title={
              <Typography display="block" variant="h5" color="textPrimary">
                {title}
              </Typography>
            }
          />
          <Divider />
        </React.Fragment>
      )}
      <CardContent className={classes.content}>
        <List>
          {item &&
            Object.keys(item).map((key) => {
              if (fields && !fields.includes(key)) {
                return;
              } else if (omitFields && omitFields.includes(key)) {
                return;
              }
              return (
                <div key={key}>
                  {item[key] === null || typeof item[key] !== 'object' ? (
                    <ListItem className={classes.listItem} disableGutters divider>
                      <Typography variant="subtitle2" color="textPrimary">
                        {toCapitalizedWords(
                          titleChanges && titleChanges.has(key)
                            ? titleChanges.get(key)!.title ?? key
                            : key
                        )}
                        {editFields && editFields.has(key) && (
                          <IconButton
                            color="primary"
                            onClick={editFields.get(key)!.editFunc}
                            size="large"
                          >
                            <PencilSquareIcon />
                          </IconButton>
                        )}
                      </Typography>
                      <Typography
                        variant="h6"
                        color="textSecondary"
                        style={{
                          padding: '0px 0px 0px 30px',
                          textAlign: 'right',
                          overflowWrap: 'anywhere',
                        }}
                      >
                        {linkfields && linkfields.has(key) && linkfields.get(key) ? (
                          <LinkField
                            item={item[key]}
                            title={linkfields.get(key)!.url_field}
                            data={item[linkfields.get(key)!.value_field]}
                          />
                        ) : (
                          render(key, item[key])
                        )}
                      </Typography>
                    </ListItem>
                  ) : (
                    <React.Fragment>
                      <ListItem className={classes.listItem} disableGutters>
                        <Typography variant="subtitle2" color="textPrimary">
                          {toCapitalizedWords(
                            titleChanges && titleChanges.has(key)
                              ? titleChanges.get(key)!.title ?? key
                              : key
                          )}
                        </Typography>
                      </ListItem>
                      <TreeView
                        className="treeview-primary"
                        defaultExpanded={[key]}
                        defaultCollapseIcon={<AngleDownIcon />}
                        defaultExpandIcon={<AngleRightIcon />}
                      >
                        {renderObject(item[key], 1)}
                        <Divider className={classes.sublistDivider} />
                      </TreeView>
                    </React.Fragment>
                  )}
                </div>
              );
            })}
        </List>
      </CardContent>
    </StyledCard>
  );
};

export default InfoCard;
