import * as React from 'react';
import { styled } from '@mui/material/styles';
import type { FC } from 'react';
import {
  Grid,
  Card,
  Button,
  Divider,
  Typography,
  CardHeader,
  CardContent,
  CardActions,
  Link,
} from '@mui/material';
import { AngleRightIcon } from 'react-line-awesome';
import { ContentLoader } from '../';

const PREFIX = 'MiniCard';

const classes = {
  root: `${PREFIX}-root`,
  header: `${PREFIX}-header`,
  content: `${PREFIX}-content`,
  listItem: `${PREFIX}-listItem`,
};

const StyledCard = styled(Card)(({ theme }) => ({
  [`&.${classes.root}`]: {},

  [`& .${classes.header}`]: {
    paddingBottom: 0,
    paddingTop: theme.spacing(1),
  },

  [`& .${classes.content}`]: {
    paddingTop: 0,
  },

  [`& .${classes.listItem}`]: {
    padding: theme.spacing(2, 0),
    justifyContent: 'space-between',
  },
}));

export interface MiniCardProps {
  item: any;
  url?: any | undefined;
  title?: string | undefined;
  icon?: JSX.Element | undefined;
}

const LinkField = ({ item, url }: { item: any; url: string }) => {
  return <Link href={`/${url}/${item?.id}`}>View</Link>;
};

const MiniCard: FC<MiniCardProps> = ({ item, url, title = 'Details', icon }) => {
  return (
    <StyledCard className={classes.root}>
      {' '}
      {!item && typeof item !== 'object' ? (
        <ContentLoader message="" size={10} />
      ) : (
        <React.Fragment>
          {title && (
            <React.Fragment>
              <CardHeader
                avatar={icon}
                className={classes.header}
                title={
                  <Typography display="block" variant="overline" color="textPrimary">
                    {title}
                  </Typography>
                }
              />
              <Divider />
            </React.Fragment>
          )}
          <CardContent className={classes.content}>
            <Grid container spacing={0}>
              <Grid item md={1}></Grid>
              <Grid item md={9} className={classes.header}>
                <Typography display="block" variant="body1" color="textPrimary">
                  {item?.name}
                </Typography>
                <Typography display="block" variant="caption" color="textSecondary">
                  {item?.id}
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
          <CardActions disableSpacing={true}>
            <Button fullWidth color="secondary">
              <LinkField item={item} url={url} />
              <AngleRightIcon color="secondary" />
            </Button>
          </CardActions>
        </React.Fragment>
      )}
    </StyledCard>
  );
};

export default MiniCard;
