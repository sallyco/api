import React, { useState } from 'react';

import { styled } from '@mui/material/styles';

import {
  Dialog,
  DialogContentText,
  Paper,
  Typography,
  Button,
  IconButton,
  CircularProgress,
} from '@mui/material';
import { Grid } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import { TimesIcon } from 'react-line-awesome';

const PREFIX = 'WarningActionCard';

const classes = {
  root: `${PREFIX}-root`,
  main: `${PREFIX}-main`,
  header: `${PREFIX}-header`,
  buttongrid: `${PREFIX}-buttongrid`,
  deletebutton: `${PREFIX}-deletebutton`,
  closeButton: `${PREFIX}-closeButton`,
  inputText: `${PREFIX}-inputText`,
  deleteTitle: `${PREFIX}-deleteTitle`
};

const Root = styled('div')((
  {
    theme
  }
) => ({
  [`& .${classes.root}`]: {
    margin: 0,
    padding: theme.spacing(2),
  },

  [`& .${classes.main}`]: {
    padding: theme.spacing(2, 2),
    marginLeft: 10,
    maxWidth: 600,
    borderColor: 'grey',
  },

  [`& .${classes.header}`]: {
    textAlign: 'left',
    color: theme.palette.warning.main,
    variant: 'subtitle1',
    fontWeight: 1000,
  },

  [`& .${classes.buttongrid}`]: {
    marginTop: 20,
  },

  [`& .${classes.deletebutton}`]: {
    backgroundColor: theme.palette.warning.main,
    color: 'white',
  },

  [`& .${classes.closeButton}`]: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

  [`& .${classes.inputText}`]: {
    border: 0,
  },

  [`& .${classes.deleteTitle}`]: {
    fontWeight: 1000,
  }
}));

export interface WarningActionCardProps {
  title: string;
  buttonText: string;
  processing?: boolean;
  children?: React.ReactNode;
  onClickOk?: () => void;
}

const WarningActionCard = ({
  title,
  buttonText,
  processing = false,
  children,
  onClickOk,
}: WarningActionCardProps) => {
  const [modalOpen, setOpen] = useState(false);




  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOkClose = () => {
    setOpen(false);
    if (onClickOk) onClickOk();
  };

  return (
    <Root>
      <div>
        {!processing && (
          <React.Fragment>
            <Dialog
              fullWidth={true}
              onClose={handleClose}
              aria-labelledby="customized-dialog-title"
              open={modalOpen}
            >
              <DialogTitle className={classes.root}>
                <Typography className={classes.deleteTitle}>{`Are you sure ?`}</Typography>
                {modalOpen ? (
                  <IconButton
                    aria-label="close"
                    className={classes.closeButton}
                    onClick={handleClose}
                    size="large"
                  >
                    <TimesIcon />
                  </IconButton>
                ) : null}
              </DialogTitle>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button onClick={handleOkClose} color="primary">
                  Yes, Confirm
                </Button>
              </DialogActions>
            </Dialog>
          </React.Fragment>
        )}
        {processing && (
          <Dialog open={true}>
            <DialogContent>
              <Typography align={'center'}>
                <DialogContentText>Please wait..</DialogContentText>
                <CircularProgress />
              </Typography>
            </DialogContent>
          </Dialog>
        )}
      </div>

      <React.Fragment>
        <div className={classes.root}>
          <Paper className={classes.main}>
            <Grid container spacing={2}>
              <Grid xs={12} container>
                <Grid direction="column" spacing={2}>
                  <Grid>
                    <Typography gutterBottom className={classes.header}>
                      {title}
                    </Typography>
                    {children}
                  </Grid>
                  <Grid className={classes.buttongrid}>
                    <Button
                      variant="contained"
                      onClick={handleClickOpen}
                      size="small"
                      className={classes.deletebutton}
                    >
                      {buttonText}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </React.Fragment>
    </Root>
  );
};

export default WarningActionCard;
