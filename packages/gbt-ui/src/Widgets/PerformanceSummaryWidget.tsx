import { styled } from '@mui/material/styles';
import LinearProgress from '@mui/material/LinearProgress';
import { Box, Divider, Typography } from '@mui/material';
import { SquareIcon } from 'react-line-awesome';
import numeral from 'numeral';

const PREFIX = 'PerformanceSummaryWidget';

const classes = {
  fill: `${PREFIX}-fill`,
  root: `${PREFIX}-root`,
  divider: `${PREFIX}-divider`,
  raisedColor: `${PREFIX}-raisedColor`,
  raisedFill: `${PREFIX}-raisedFill`,
  targetFill: `${PREFIX}-targetFill`,
  committedColor: `${PREFIX}-committedColor`,
  committedFill: `${PREFIX}-committedFill`,
  disableAnimation: `${PREFIX}-disableAnimation`
};

const Root = styled('div')((
  {
    theme: Theme
  }
) => ({
  [`&.${classes.fill}`]: {
    width: '100%',
  },

  [`& .${classes.root}`]: {
    height: 12,
  },

  // Divider styling
  [`& .${classes.divider}`]: {
    marginTop: '.5em',
    marginBottom: '.5em',
  },

  // Raised
  [`& .${classes.raisedColor}`]: {
    backgroundColor: '#71b73c',
  },

  [`& .${classes.raisedFill}`]: {
    fill: '#71b73c',
  },

  [`& .${classes.targetFill}`]: {
    fill: 'black',
  },

  // Committed
  [`& .${classes.committedColor}`]: {
    backgroundColor: 'gold',
  },

  [`& .${classes.committedFill}`]: {
    fill: 'gold',
  },

  // Disable dot animation
  [`& .${classes.disableAnimation}`]: {
    transition: 'none',
    animation: '',
    backgroundColor: 'black',
    backgroundImage: 'none',
    backgroundPosition: '',
  }
}));

export interface PerformanceSummaryWidgetProps {
  primaryNumber?: number;
  secondaryNumber?: number;
  targetNumber?: number;
}

function numberToCurrency(number: number) {
  return `$${numeral(number).format('0,0')}`;
}

export default function PerformanceSummaryWidget({
  primaryNumber = 0,
  secondaryNumber = 0,
  targetNumber = 0,
  ...props
}: PerformanceSummaryWidgetProps) {


  const primaryPercent =
    primaryNumber > targetNumber ? 100 : Math.floor((primaryNumber / targetNumber) * 100);
  const secondaryPercent =
    secondaryNumber > targetNumber ? 100 : Math.floor((secondaryNumber / targetNumber) * 100);

  return (
    <Root className={classes.fill}>
      <LinearProgress
        variant="buffer"
        value={primaryPercent}
        valueBuffer={secondaryPercent}
        classes={{
          root: classes.root,
          barColorPrimary: classes.raisedColor,
          bar2Buffer: classes.committedColor,
          dashed: classes.disableAnimation,
        }}
        {...props}
      />
      <Divider classes={{ root: classes.divider }} />
      <Box display="flex" flexWrap="wrap" justifyContent="space-between">
        <Box>
          <Typography component="h5" gutterBottom variant="overline" color="textSecondary">
            {'Raised'}
          </Typography>
          <Box display="flex" alignItems="center" flexWrap="wrap">
            <Typography variant="h5" color="textPrimary">
              {numberToCurrency(primaryNumber)}
            </Typography>
            <SquareIcon className={classes.raisedFill}>+</SquareIcon>
          </Box>
        </Box>

        <Box>
          <Typography component="h5" gutterBottom variant="overline" color="textSecondary">
            {'Committed'}
          </Typography>
          <Box display="flex" alignItems="center" flexWrap="wrap">
            <Typography variant="h5" color="textPrimary">
              {numberToCurrency(secondaryNumber)}
            </Typography>
            <SquareIcon className={classes.committedFill}>+</SquareIcon>
          </Box>
        </Box>

        <Box>
          <Typography component="h5" gutterBottom variant="overline" color="textSecondary">
            {'Target'}
          </Typography>
          <Box display="flex" alignItems="center" flexWrap="wrap">
            <Typography variant="h5" color="textPrimary">
              {numberToCurrency(targetNumber)}
            </Typography>
            <SquareIcon className={classes.targetFill}>+</SquareIcon>
          </Box>
        </Box>
      </Box>
    </Root>
  );
}
