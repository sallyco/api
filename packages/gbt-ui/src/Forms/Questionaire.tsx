/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';
import { styled } from '@mui/material/styles';
//import { CheckIcon } from 'react-line-awesome';

import { Form } from '../';
import { JSONSchema7 } from 'json-schema';

import { Backdrop, Container, Box, Button, CircularProgress, Grid } from '@mui/material';
import { ContentLoader } from '../';

const PREFIX = 'Questionaire';

const classes = {
  root: `${PREFIX}-root`,
  formLayoutLabel: `${PREFIX}-formLayoutLabel`,
  formLayout: `${PREFIX}-formLayout`,
  buttonSubmit: `${PREFIX}-buttonSubmit`,
  backdrop: `${PREFIX}-backdrop`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    fontSize: 'larger',
  },

  [`& .${classes.formLayoutLabel}`]: {
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right',
    },
  },

  [`& .${classes.formLayout}`]: {
    '& .MuiFormControl-root': {
      width: '100%',
    },
    '& .MuiFormLabel-root': {
      display: 'none',
    },
  },

  [`& .${classes.buttonSubmit}`]: {
    float: 'right',
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
    marginRight: 0,
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    backgroundColor: theme.palette.success.main,
    color: theme.palette.common.white,
  },

  [`& .${classes.backdrop}`]: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export interface QuestionaireProps {
  schema: JSONSchema7;
  submitAction: (arg?: any) => void;
  loading?: boolean;
  setLoading?: React.Dispatch<React.SetStateAction<boolean>> | undefined;
}

const Questionaire: React.FC<QuestionaireProps> = ({
  schema,
  submitAction,
  loading,
  setLoading,
}) => {
  const [submitting, setSubmitting] = React.useState(false);
  //const [formdata, setFormdata] = React.useState(undefined);

  const CustomFieldTemplate = (props: any) => {
    const { id, label, help, required, description, errors, children } = props;
    if (id === 'root') {
      return (
        <div className={classes.formLayout}>
          {description}
          {children}
          {errors}
          {help}
        </div>
      );
    }
    return (
      <Grid container alignItems="center" spacing={3}>
        <Grid item xs={12} sm={6} className={classes.formLayoutLabel}>
          <label htmlFor={id}>
            {label}
            {required ? '*' : null}
          </label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <div className={classes.formLayout}>
            {description}
            {children}
            {errors}
            {help}
          </div>
        </Grid>
      </Grid>
    );
  };

  const onSubmit = async ({ formData }: { formData: any }) => {
    setSubmitting(true);
    await submitAction(formData);
    //setFormdata(undefined);
    setSubmitting(false);
  };

  return (
    <Root>
      {!schema || loading ? (
        <Box>
          <ContentLoader size={24} message={'Loading...'} />
        </Box>
      ) : (
        <Container className={classes.root}>
          <Form
            schema={schema}
            uiSchema={{
              question7_3: {
                'ui:widget': 'checkboxes',
              },
              question10: {
                'ui:widget': 'checkboxes',
              },
              question11: {
                'ui:widget': 'checkboxes',
              },
              question2_1: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_2: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_3: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_4: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_5: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_6: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_7: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_8: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_9: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_10: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_11: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_12: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_13: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_14: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
              question2_15: {
                'ui:widget': 'textarea',
                'ui:options': {
                  rows: 15,
                },
              },
            }}
            onSubmit={onSubmit}
            showErrorList={false}
            omitExtraData={true}
            //formData={formdata}
            //onChange={(e) => setFormdata(e.formData)}
            FieldTemplate={CustomFieldTemplate}
          >
            <Button variant="contained" type="submit" className={classes.buttonSubmit}>
              Next
            </Button>
          </Form>
        </Container>
      )}
      <Backdrop className={classes.backdrop} open={submitting}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Root>
  );
};

export default Questionaire;
