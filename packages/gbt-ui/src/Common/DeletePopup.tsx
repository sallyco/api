import React, { useState } from 'react';

import { styled } from '@mui/material/styles';

import {
  Dialog,
  DialogContentText,
  Paper,
  Typography,
  Button,
  IconButton,
  TextField,
  Chip,
  CircularProgress,
} from '@mui/material';
import { Grid } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import { TimesIcon } from 'react-line-awesome';

const PREFIX = 'DeletePopup';

const classes = {
  root: `${PREFIX}-root`,
  main: `${PREFIX}-main`,
  header: `${PREFIX}-header`,
  buttongrid: `${PREFIX}-buttongrid`,
  deletebutton: `${PREFIX}-deletebutton`,
  closeButton: `${PREFIX}-closeButton`,
  inputText: `${PREFIX}-inputText`,
  deleteTitle: `${PREFIX}-deleteTitle`
};

const Root = styled('div')((
  {
    theme
  }
) => ({
  [`& .${classes.root}`]: {
    margin: 0,
    padding: theme.spacing(2),
  },

  [`& .${classes.main}`]: {
    padding: theme.spacing(2, 2),
    marginLeft: 10,
    maxWidth: 600,
    borderColor: 'grey',
  },

  [`& .${classes.header}`]: {
    textAlign: 'left',
    color: theme.palette.text.primary,
    variant: 'subtitle1',
    fontWeight: 1000,
  },

  [`& .${classes.buttongrid}`]: {
    marginTop: 20,
  },

  [`& .${classes.deletebutton}`]: {
    backgroundColor: '#d41313',
    color: 'white',
  },

  [`& .${classes.closeButton}`]: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

  [`& .${classes.inputText}`]: {
    border: 0,
  },

  [`& .${classes.deleteTitle}`]: {
    fontWeight: 1000,
  }
}));

export interface Props {
  objectId: string;
  objectType: string;
  name: string;
  processing: boolean;
  onClickOk?: () => void;
}

const DeletePopup = ({ objectId, objectType, name, processing, onClickOk = () => {} }: Props) => {
  const [modalOpen, setOpen] = useState(false);
  const [isDisabled, setDisabled] = useState(true);
  const [inputValue, setinputValue] = useState('');



  const handleClickOpen = () => {
    setinputValue('');
    setOpen(true);
    setDisabled(true);
  };

  const handleClose = () => {
    setinputValue('');
    setOpen(false);
  };

  const handleOkClose = () => {
    setOpen(false);
    onClickOk();
  };

  const onValueChange= (event: React.ChangeEvent<HTMLInputElement>) => {
    setinputValue(event.target.value);
    event.target.value === 'DELETE' ? setDisabled(false) : setDisabled(true);
  };

  return (
    <Root>
      <div>
        {!processing && (
          <React.Fragment>
            <Dialog
              fullWidth={true}
              onClose={handleClose}
              aria-labelledby="customized-dialog-title"
              open={modalOpen}
            >
              <DialogTitle  className={classes.root}>
                <Typography
                  className={classes.deleteTitle}
                >{`Delete ${objectType}, Are you sure ?`}</Typography>
                {modalOpen ? (
                  <IconButton
                    aria-label="close"
                    className={classes.closeButton}
                    onClick={handleClose}
                    size="large">
                    <TimesIcon />
                  </IconButton>
                ) : null}
              </DialogTitle>
              <DialogContent dividers>
                <DialogContentText>
                  Please type <Chip label="DELETE" /> the text field below:
                </DialogContentText>
                <TextField
                  className={classes.inputText}
                  margin="dense"
                  id="name"
                  type="text"
                  fullWidth
                  autoFocus
                  value={inputValue}
                  onChange={onValueChange}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel, Keep {objectType}
                </Button>
                <Button onClick={handleOkClose} color="primary" disabled={isDisabled}>
                  Yes ,Delete {objectType}
                </Button>
              </DialogActions>
            </Dialog>
          </React.Fragment>
        )}
        {processing && (
          <Dialog open={true}>
            <DialogContent>
              <Typography align={'center'}>
                <DialogContentText>
                  Please wait while the deleting is being processed..
                </DialogContentText>
                <CircularProgress />
              </Typography>
            </DialogContent>
          </Dialog>
        )}
      </div>

      <React.Fragment>
        <div className={classes.root}>
          <Paper className={classes.main}>
            <Grid container spacing={2}>
              <Grid xs={12} container>
                <Grid direction="column" spacing={2}>
                  <Grid>
                    <Typography gutterBottom className={classes.header}>
                      Delete {objectType}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {`This will delete this ${objectType} (${name}) from database.`}
                    </Typography>
                  </Grid>
                  <Grid className={classes.buttongrid}>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={handleClickOpen}
                      size="small"
                      className={classes.deletebutton}
                    >
                      Delete
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </React.Fragment>
    </Root>
  );
};

export default DeletePopup;
