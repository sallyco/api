import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import { ExclamationTriangleIcon } from 'react-line-awesome';

export interface ContentLoaderProps {
  size?: number;
  message?: string;
  timeoutMilliseconds?: number;
  errorMessage?: string | undefined;
}

const ContentLoader = ({
  size = 64,
  message = 'Loading...',
  timeoutMilliseconds = 60000,
  errorMessage = undefined,
}: ContentLoaderProps) => {
  const [expired, setExpired] = React.useState(false);

  React.useEffect(() => {
    const timer = setTimeout(() => {
      setExpired(true);
    }, timeoutMilliseconds);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  const refreshPage = () => {
    window.location.reload();
  };

  return (
    <React.Fragment>
      <Box pt={6}>
        <Grid container alignItems="center" direction="column">
          <Grid item xs={12}>
            {!expired && !errorMessage ? (
              <CircularProgress size={size} />
            ) : (
              <ExclamationTriangleIcon style={{ fontSize: '86px' }} />
            )}
          </Grid>
          <Grid item xs={12}>
            {!expired && !errorMessage ? (
              <Typography variant={'body1'}>{message}</Typography>
            ) : (
              <React.Fragment>
                <Typography variant={'body1'} style={{ textAlign: 'center' }}>
                  {`Something went wrong, `}
                  <Link href="javascript:void(0)" onClick={refreshPage}>
                    try again.
                  </Link>
                </Typography>
                {errorMessage && (
                  <Box mt={2}>
                    <Card raised={false}>
                      <CardContent>
                        <Typography color="textSecondary" gutterBottom>
                          Error
                        </Typography>
                        <Typography variant="body2">{errorMessage}</Typography>
                      </CardContent>
                    </Card>
                  </Box>
                )}
              </React.Fragment>
            )}
          </Grid>
        </Grid>
      </Box>
    </React.Fragment>
  );
};

export default ContentLoader;
