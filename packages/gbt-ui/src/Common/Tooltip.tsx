import type { FC } from 'react';
//import { makeStyles, Theme, createStyles } from '@mui/material/styles';

import { Tooltip } from '@mui/material';

import Fade from '@mui/material/Fade';

export interface TooltipProps {
  title: string;
  transitionTime?: number;
  children?: any;
}

//const useStyles = makeStyles((theme: Theme) => {
//  return createStyles({
//    root: {},
//  });
//});

const TooltipComponent: FC<TooltipProps> = ({ title, transitionTime = 600, children }) => {
//  const classes = useStyles();

  return (
    <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: transitionTime }} title={title}>
      <span>{children}</span>
    </Tooltip>
  );
};

export default TooltipComponent;
