import * as React from 'react';
import { styled } from '@mui/material/styles';
import classNames from 'classnames';

import Grid from '@mui/material/Grid';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import TextField from '@mui/material/TextField';

import SignatureCanvas from 'react-signature-canvas';

const PREFIX = 'SignaturePad';

const classes = {
  label: `${PREFIX}-label`,
  signatureCanvasControl: `${PREFIX}-signatureCanvasControl`,
  signatureCanvas: `${PREFIX}-signatureCanvas`,
  signatureCanvasAction: `${PREFIX}-signatureCanvasAction`,
  signatureCanvasType: `${PREFIX}-signatureCanvasType`,
  formActions: `${PREFIX}-formActions`
};

const StyledFormControl = styled(FormControl)((
  {
    theme
  }
) => ({
  [`& .${classes.label}`]: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(0, 1),
  },

  [`&.${classes.signatureCanvasControl}`]: {
    position: 'relative',
  },

  [`& .${classes.signatureCanvas}`]: {
    borderRadius: '4px',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.grey[300],

    '&.hover': {
      borderColor: theme.palette.grey[500],
    },
    '&.focus': {
      borderColor: theme.palette.secondary.main,
    },
  },

  [`& .${classes.signatureCanvasAction}`]: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
  },

  [`& .${classes.signatureCanvasType}`]: {
    position: 'absolute',
    top: theme.spacing(8),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },

  [`& .${classes.formActions}`]: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  }
}));

export interface SignaturePadProps {
  id: string;
  value?: any;
  onSubmit?: (a: any) => void | undefined;
}

const SignaturePad = ({ id, onSubmit = undefined, value = null }: SignaturePadProps) => {

  const signRef = React.useRef() as React.MutableRefObject<SignatureCanvas>;
  const [hover, setHover] = React.useState(false);
  const [focused, setFocused] = React.useState(false);
  const [hasData, setHasData] = React.useState(false);
  const [drawMode, setDrawMode] = React.useState(true);
  const [typedName, setTypedName] = React.useState('');

  React.useEffect(() => {
    const data = JSON.parse(value);

    if (data) {
      signRef.current.fromData(data);
      setHasData(true);
    } else {
      signRef.current.clear();
      setHasData(false);
    }
  }, [value]);

  React.useEffect(() => {
    if (!drawMode) {
      signRef.current.clear();
      signRef.current.off();
    } else {
      signRef.current.on();
      setTypedName('');
    }
  }, [drawMode]);

  const handleMouseEnter = () => setHover(true);
  const handleMouseLeave = () => setHover(false);

  const handleLockUpdate = () => {
    setFocused(true);
    setHasData(true);
  };
  const handleUpdateData = () => {
    setFocused(false);
  };

  const handleClear = () => {
    signRef.current.clear();
    setHasData(false);
    setTypedName('');
  };

  const handleTextInput = (event: any) => {
    signRef.current.clear();
    setTypedName(event.target.value);
  };

  const handleSubmit = () => {
    const result = signRef.current.getTrimmedCanvas().toDataURL('image/png');
    if (onSubmit) {
      onSubmit(result);
    }
  };

  React.useEffect(() => {
    if (typedName === '') {
      signRef.current.clear();
      setHasData(false);
    } else {
      const ctx = signRef.current.getCanvas().getContext('2d');
      if (ctx) {
        ctx.font = '40px Yellowtail';
        ctx.fillText(typedName, 20, 160);
      }
      setHasData(true);
    }
  }, [typedName]);

  return (
    <StyledFormControl
      variant="outlined"
      color="secondary"
      focused={focused}
      className={classes.signatureCanvasControl}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <InputLabel
        variant="outlined"
        htmlFor="canvas-signature"
        className={classes.label}
        shrink={false}
      >
        Signature
      </InputLabel>

      <SignatureCanvas
        ref={signRef}
        penColor="black"
        backgroundColor="white"
        canvasProps={{
          width: 600,
          height: 200,
          className: classNames(classes.signatureCanvas, focused ? 'focus' : hover ? 'hover' : ''),
        }}
        onBegin={handleLockUpdate}
        onEnd={handleUpdateData}
      />
      {!drawMode && (
        <TextField
          variant="outlined"
          size="small"
          className={classes.signatureCanvasType}
          fullWidth
          onChange={handleTextInput}
          placeholder="Type Name"
          value={typedName}
        />
      )}
      <ButtonGroup className={classes.signatureCanvasAction} disableElevation>
        <Button variant={drawMode ? 'contained' : 'outlined'} onClick={() => setDrawMode(true)}>
          Draw
        </Button>
        <Button variant={!drawMode ? 'contained' : 'outlined'} onClick={() => setDrawMode(false)}>
          Type
        </Button>
      </ButtonGroup>
      <Grid justifyContent="space-between" container className={classes.formActions}>
        <Grid item>
          <Button disabled={!hasData} onClick={handleClear} variant="contained">
            Clear
          </Button>
        </Grid>
        <Grid item>
          <Button disabled={!hasData} onClick={handleSubmit} color="primary" variant="contained">
            Accept and Sign
          </Button>
        </Grid>
      </Grid>
    </StyledFormControl>
  );
};

export default SignaturePad;
