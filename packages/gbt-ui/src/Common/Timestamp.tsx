import type { FC } from 'react';
import moment from 'moment';

import { Typography } from '@mui/material';

export interface TimestampProps {
  value: string;
  multiLine?: boolean;
}

const Timestamp: FC<TimestampProps> = ({ value, multiLine = false }) => {

  return (
    <div>
      <Typography variant="body1">{`${moment.utc(value).local().format('lll')}`}</Typography>
      <Typography variant="caption">{`(${moment.utc(value).local().fromNow()})`}</Typography>
    </div>
  );
};

export default Timestamp;
