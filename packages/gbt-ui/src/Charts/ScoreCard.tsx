import * as React from 'react';

import { styled } from '@mui/material/styles';

import { Card, CardContent } from '@mui/material';
//import { ChevronDownIcon } from 'react-line-awesome';

import Chart from 'react-apexcharts';

import _ from 'lodash';

const PREFIX = 'ScoreCard';

const classes = {
  root: `${PREFIX}-root`,
  header: `${PREFIX}-header`,
  content: `${PREFIX}-content`,
  current: `${PREFIX}-current`,
  button: `${PREFIX}-button`,
  gridItem: `${PREFIX}-gridItem`,
  gridItemLabel: `${PREFIX}-gridItemLabel`,
  gridItemValue: `${PREFIX}-gridItemValue`,
  gridItemProgressValue: `${PREFIX}-gridItemProgressValue`,
  gridItemProgressBar: `${PREFIX}-gridItemProgressBar`,
  gridItemProgressBarValue: `${PREFIX}-gridItemProgressBarValue`
};

const StyledCard = styled(Card)((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {},

  [`& .${classes.header}`]: {
    paddingBottom: 0,
  },

  [`& .${classes.content}`]: {
    paddingTop: 0,
  },

  [`& .${classes.current}`]: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },

  [`& .${classes.button}`]: {
    margin: theme.spacing(1),
  },

  [`& .${classes.gridItem}`]: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },

  [`& .${classes.gridItemLabel}`]: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },

  [`& .${classes.gridItemValue}`]: {
    display: 'block',
    fontSize: '1.15rem',
  },

  [`& .${classes.gridItemProgressValue}`]: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },

  [`& .${classes.gridItemProgressBar}`]: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },

  [`& .${classes.gridItemProgressBarValue}`]: {
    paddingLeft: '1rem',
  }
}));

export interface ScoreCardProps {
  data: any;
}

const ScoreCard: React.FC<ScoreCardProps> = ({ data }: ScoreCardProps) => {


  const series = [
    {
      name: 'assessment',
      data: [3, 5, 3, 1, 4, 3, 2],
    },
  ];
  const options = {
    chart: {
      height: 350,
    },
    title: {
      text: 'Score Card',
    },
    xaxis: {
      categories: [
        'Managment',
        'Market',
        'Product',
        'Competition',
        'Business Model',
        'Traction / Financials',
        'Client Success',
      ],
    },
  };

  return (
    <StyledCard className={classes.root}>
      <CardContent className={classes.content}>
        <Chart options={options} series={series} type="radar" height={317} />
      </CardContent>
    </StyledCard>
  );
};

export default ScoreCard;
