import * as React from 'react';
import { styled } from '@mui/material/styles';

import {
  Backdrop,
  CircularProgress,
  Dialog,
  DialogContent,
  Slide,
  Typography,
  IconButton,
} from '@mui/material';
import MuiDialogTitle from '@mui/material/DialogTitle';
import { TimesIcon } from 'react-line-awesome';

import { TransitionProps } from '@mui/material/transitions';

const PREFIX = 'LargeDialog';

const classes = {
  root: `${PREFIX}-root`,
  closeButton: `${PREFIX}-closeButton`,
  backdrop: `${PREFIX}-backdrop`
};

const StyledDialog = styled(Dialog)((
  {
    theme
  }
) => ({
  [`&.${classes.root}`]: {
    margin: 0,
    padding: theme.spacing(2),
  },

  [`& .${classes.closeButton}`]: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

  [`& .${classes.backdrop}`]: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
}));

export interface LargeDialogProps {
  title: string;
  loading?: boolean;
  setLoading?: React.Dispatch<React.SetStateAction<boolean>> | undefined;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  slideDirection?: 'down' | 'up';
  children?: any;
}

const TransitionUp = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionDown = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const LargeDialog: React.FC<LargeDialogProps> = ({
  title,
  loading = false,
  setLoading = undefined,
  open,
  setOpen,
  slideDirection = 'up',
  children,
}) => {

  const maxWidth = 'lg';

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <StyledDialog
      fullWidth={true}
      maxWidth={maxWidth}
      open={open}
      onClose={handleClose}
      TransitionComponent={slideDirection === 'down' ? TransitionDown : TransitionUp}
      className={classes.root}
    >
      <MuiDialogTitle className={classes.root}>
        <Typography variant="h5">{title}</Typography>
        <IconButton className={classes.closeButton} onClick={handleClose} size="large">
          <TimesIcon />
        </IconButton>
      </MuiDialogTitle>
      <DialogContent>{children}</DialogContent>
      <Backdrop
        className={classes.backdrop}
        open={loading}
        onClick={() => setLoading && setLoading(false)}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </StyledDialog>
  );
};

export default LargeDialog;
