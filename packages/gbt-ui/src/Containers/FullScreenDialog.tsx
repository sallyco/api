import * as React from 'react';
import { styled } from '@mui/material/styles';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { TimesIcon } from 'react-line-awesome';

import {
  Backdrop,
  CircularProgress,
  Typography,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
} from '@mui/material';

const StyledBackdrop = styled(Backdrop)(({ theme }) => ({
  zIndex: theme.zIndex.drawer + 1,
  color: '#fff',
}));

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export interface FullScreenDialogProps {
  title: string;
  loading?: boolean;
  setLoading?: React.Dispatch<React.SetStateAction<boolean>> | undefined;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  closeAction?: (arg?: any) => void;
  titleBarAction?: JSX.Element | undefined;
  children?: any;
}

const FullScreenDialog: React.FC<FullScreenDialogProps> = ({
  title,
  loading = false,
  setLoading = undefined,
  open,
  closeAction,
  setOpen,
  titleBarAction = undefined,
  children,
}) => {
  const handleClose = () => {
    if (setLoading) {
      setLoading(false);
    }
    if (closeAction) {
      closeAction();
    }
    setOpen(false);
  };

  return (
    <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
      <AppBar sx={{ position: 'relative' }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
            size="large"
          >
            <TimesIcon />
          </IconButton>
          <Typography variant="h6" sx={{ ml: 2, flex: 1 }}>
            {title}
          </Typography>
          {titleBarAction}
        </Toolbar>
      </AppBar>
      {children}
      <StyledBackdrop open={loading} onClick={() => setLoading && setLoading(false)}>
        <CircularProgress color="inherit" />
      </StyledBackdrop>
    </Dialog>
  );
};

export default FullScreenDialog;
