import * as React from 'react';
import { styled } from '@mui/material/styles';
import type { FC } from 'react';
import { Box, Drawer, IconButton, Tooltip, Typography } from '@mui/material';
import { SearchIcon, TimesCircleIcon } from 'react-line-awesome';

const PREFIX = 'SlideDrawer';

const classes = {
  drawer: `${PREFIX}-drawer`,
};

const Root = styled('div')(() => ({
  [`& .${classes.drawer}`]: {
    width: 500,
    maxWidth: '100%',
  },
}));

export interface SlideDrawerProps {
  title: string;
  openIcon?: React.ReactNode;
  closeIcon?: React.ReactNode;
  children?: any;
}

const SlideDrawer: FC<SlideDrawerProps> = ({
  title = 'Search',
  openIcon = <SearchIcon />,
  closeIcon = <TimesCircleIcon />,
  children,
}) => {
  const [isOpen, setOpen] = React.useState<boolean>(false);

  const handleOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  return (
    <Root>
      <Tooltip title={title}>
        <IconButton color="inherit" onClick={handleOpen} edge="end">
          {openIcon}
        </IconButton>
      </Tooltip>
      <Drawer
        anchor="right"
        classes={{ paper: classes.drawer }}
        ModalProps={{ BackdropProps: { invisible: true } }}
        onClose={handleClose}
        open={isOpen}
        variant="temporary"
      >
        <Box p={4}>
          <Box display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h4" color="textPrimary">
              {title}
            </Typography>
            <IconButton onClick={handleClose} size="large">
              {closeIcon}
            </IconButton>
          </Box>
          {children}
        </Box>
      </Drawer>
    </Root>
  );
};

export default SlideDrawer;
