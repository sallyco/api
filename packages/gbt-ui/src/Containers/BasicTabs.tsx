import * as React from 'react';
import { styled } from '@mui/material/styles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';

import create from 'zustand';
import { persist } from 'zustand/middleware';

const PREFIX = 'BasicTabs';

const classes = {
  tabsContainer: `${PREFIX}-tabsContainer`,
  basicTabsRoot: `${PREFIX}-basicTabsRoot`,
  basicTabSelected: `${PREFIX}-basicTabSelected`,
  basicTabRoot: `${PREFIX}-basicTabRoot`,
  basicTabWrapper: `${PREFIX}-basicTabWrapper`,
};

const Root = styled('div')({
  [`& .${classes.basicTabsRoot}`]: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
  [`& .${classes.basicTabSelected}`]: {
    transition: 'background-color .4s',
  },
  [`& .${classes.basicTabRoot}`]: {
    width: 'auto',
    minWidth: '70px',
    borderRadius: '3px',
    opacity: 1,
    height: 'auto',
    padding: '10px 15px',
    minHeight: 'unset',
    lineHeight: '24px',
  },
});

type TabSelectionState = {
  tabSelection: { tabName: string; selectedTab: number }[];
  setTabSelection: (tabName: string, newlySelectedTab: number) => void;
};

const useTabSelectionStore = create<TabSelectionState>(
  // @ts-ignore
  persist(
    (set) => ({
      tabSelection: [] as Array<string>,
      setTabSelection: (t, v) =>
        set((state) => {
          const filteredTabSelection = state.tabSelection.filter((tab) => tab.tabName !== t);
          return {
            tabSelection: [
              ...filteredTabSelection,
              {
                tabName: t,
                selectedTab: v,
              },
            ],
          };
        }),
    }),
    {
      name: 'ui-tab-selection',
      getStorage: () => sessionStorage,
    }
  )
);

export interface BasicTabsProps {
  headerColor?: 'primary' | 'secondary';
  tabs: {
    tabName: string;
    tabIcon?:
      | string
      | React.ReactElement<any, string | React.JSXElementConstructor<any>>
      | undefined;
    tabContent?: React.ReactNode | undefined;
  }[];
  handleTabChange?: ((r: number) => void) | null;
  tabName?: string;
}

const BasicTabs: React.FC<BasicTabsProps> = ({
  headerColor = 'primary',
  tabs,
  handleTabChange,
  tabName = 'common-ui-tab',
}) => {
  const tabSelection = useTabSelectionStore(
    (state) =>
      state.tabSelection?.filter((tab) => tab.tabName === tabName)[0] ?? { tabName, selectedTab: 0 }
  );
  const setTabSelection = useTabSelectionStore((state) => state.setTabSelection);

  const [value, setValue] = React.useState(
    tabName === 'common-ui-tab' && tabSelection.selectedTab >= tabs.length
      ? 0
      : tabSelection.selectedTab
  );
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
    setTabSelection(tabName, newValue);
    if (handleTabChange) {
      handleTabChange(newValue);
    }
  };

  return (
    <Root>
      <Tabs
        classes={{
          root: classes.basicTabsRoot,
        }}
        value={value}
        onChange={handleChange}
        indicatorColor={headerColor}
        textColor={headerColor}
        variant="scrollable"
        scrollButtons="auto"
      >
        {tabs.map((prop, key) => (
          <Tab
            key={key}
            classes={{
              root: classes.basicTabRoot,
              selected: classes.basicTabSelected,
            }}
            label={prop.tabName}
            icon={prop.tabIcon}
          />
        ))}
      </Tabs>
      <React.Fragment>
        {tabs.map((prop, key) => (
          <div role="tabpanel" hidden={value !== key} key={key}>
            <Box mt={2}>{prop.tabContent}</Box>
          </div>
        ))}
      </React.Fragment>
    </Root>
  );
};

export default BasicTabs;
