import * as React from 'react';
import { styled } from '@mui/material/styles';
import type { FC } from 'react';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';

const PREFIX = 'ConfirmationDialog';

const classes = {
  root: `${PREFIX}-root`,
  approve: `${PREFIX}-approve`
};

const StyledDialog = styled(Dialog)((
  {
    theme
  }
) => ({
  [`& .${classes.root}`]: {},

  [`& .${classes.approve}`]: {
    backgroundColor: 'green',
    color: 'white',
  }
}));

export interface ConfirmDialogProps {
  title?: string;
  message?: string;
  cancelText?: string;
  confirmText?: string;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  onConfirm: () => void;
}

const ConfirmationDialog: FC<ConfirmDialogProps> = ({
  title = 'Confirm?',
  message = 'Are you Sure?',
  cancelText = 'Cancel',
  confirmText = 'Confirm',
  open = false,
  setOpen,
  onConfirm,
}: ConfirmDialogProps) => {


  const handleClose = () => {
    setOpen(false);
  };

  const handleConfirm = () => {
    onConfirm();
    setOpen(false);
  };

  return (
    <StyledDialog open={open} onClose={handleClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>{cancelText}</Button>
        <Button onClick={handleConfirm} className={classes.approve} autoFocus>
          {confirmText}
        </Button>
      </DialogActions>
    </StyledDialog>
  );
};

export default ConfirmationDialog;
