const webpack = require("webpack");
const path = require("path");
const { withSentryConfig } = require("@sentry/nextjs");
const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

const SentryWebpackPluginOptions = {
  silent: true,
};

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      webpack(config, options) {
        config.resolve.modules.push(path.resolve("./"));
        return config;
      },
      experimental: {
        optimizeFonts: false,
        esmExternals: "loose",
      },
      poweredByHeader: false,
      images: {
        domains: ["marketing-images-gbt.s3.us-east-2.amazonaws.com"],
      },
    };
  }

  return withBundleAnalyzer(
    withSentryConfig({
      webpack(config, options) {
        config.resolve.modules.push(path.resolve("./"));
        return config;
      },
      experimental: {
        optimizeFonts: false,
        esmExternals: "loose",
      },
      poweredByHeader: false,
      images: {
        domains: ["marketing-images-gbt.s3.us-east-2.amazonaws.com"],
      },
    }),
    SentryWebpackPluginOptions
  );
};
