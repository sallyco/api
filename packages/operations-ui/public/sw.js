importScripts("localforage.min.js");

const whitelistedOrigins = [
  "http://localhost",
  "http://localhost:3001",
  "http://localhost:3101",
  "https://api.local.glassboardtech.com",
  "https://operations.glassboardtech.com",
  "https://operations.dev.glassboardtech.com",
  "https://operations.sandbox.glassboardtech.com",
  "https://operations.qa.glassboardtech.com",
  "https://operations-api.dev.glassboardtech.com",
  "https://api.dev.glassboardtech.com",
  "https://operations-api.qa.glassboardtech.com",
  "https://api.qa.glassboardtech.com",
  "https://operations-api.sandbox.glassboardtech.com",
  "https://api.sandbox.glassboardtech.com",
  "https://operations-api.glassboardtech.com",
  "https://api.glassboardtech.com",
];

//const whitelistedPathRegex = /\/api\/[^.]*$/ // anything under /api
const whitelistedPathRegex = /\/[^.]*$/; // anything under /api

const addAuthHeader = async (event) => {
  destURL = new URL(event.request.url);
  if (
    whitelistedOrigins.includes(destURL.origin) &&
    whitelistedPathRegex.test(destURL.pathname) &&
    !destURL.pathname.includes("openid-connect")
  ) {
    const modifiedHeaders = new Headers(event.request.headers);

    const newReq = localforage.getItem("headers").then((value) => {
      if (value?.token)
        modifiedHeaders.append("Authorization", `Bearer ${value.token}`);
      if (value?.tenantId)
        modifiedHeaders.append("x-tenant-id", value.tenantId);

      const authReq = new Request(event.request, {
        headers: modifiedHeaders,
        mode: "cors",
      });
      return (async () => fetch(authReq))();
    });

    event.respondWith(newReq);
  }
};

self.addEventListener("install", function (event) {
  event.waitUntil(self.skipWaiting());
});

self.addEventListener("activate", function (event) {
  return self.clients.claim();
});

self.addEventListener("message", function (event) {
  if (event.data && event.data.type === "SET_TOKEN") {
    const token = event.data.token;
    const tenantId = event.data.tenantId;
    localforage.setItem("headers", {
      token: token,
      tenantId: tenantId,
    });
    event.ports[0].postMessage({ SET_TOKEN: "ack" });
  }
});

self.addEventListener("fetch", addAuthHeader);
