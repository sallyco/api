# Getting Started
_Operations UI is the front end to the operations app_

## Keycloak Setup
_This is required to allow authentication in operations-ui_

You need an operations-ui client created under the Master realm
1. Open Keycloak
1. Select _Master_ from the realms list
1. Under the _Configure_ section select _Clients_
1. Create a new client named `operations-ui`

Configure the client so you can get the Secret Key
1. Select the new client, and open the _Settings_ tab
1. The Following should be turned ON
    - Enabled
    - Standard Flow Enabled
    - Direct Access Grants Enabled
1. Access Type - should be set to _confidential_
1. Valid Redirect URIs - create one called `tmp`
1. Save

Get the Secret Key from the client and place in in the Tiltfile for _operations-ui_
1. Open the Credentials Tab
1. Client Authenticator - should be _Client id and Secret_
1. Copy the _Secret_
1. Update/Create your `.env.local`
```dotenv
NEXT_PUBLIC_CLIENT_SECRET=ccee52ec-5c6c-430a-9d5e-8aa0582dd6de
```
For Impersonate add environment varibale in .env.local
1. Update/Create your `.env.local`
```dotenv
NEXT_PUBLIC_APP_URL=localhost
```

You should now be able to log in to operations-ui with the standard account/password for development environments.  
