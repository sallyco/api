export interface Signer {
  dateOfBirth?: string;
  address?: object;
  taxDetails?: {
    type: string;
    value: string;
  };
}

export interface ProfileBankingUser {
  id?: string;
  accounts: ProfileBankAccount[];
}
export interface ProfileBankAccount {
  counterpartyId: string;
  bankName: string;
  accountName: string;
  accountId: string;
  subAccountId: string;
  providerMeta?: {
    typeId?: string;
    accountStatus?: string;
    accountApplicationId?: string;
    accountId?: string;
    [prop: string]: any;
  };
}

export interface Profile {
  id: string;
  name?: string;
  ownerId: string;
  tenantId: string;
  profileType?: string;
  stateOfFormation?: string;
  countryOfFormation?: string;
  legalName?: string;
  typeOfEntity?:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION'
    | 'S_CORPORATION'
    | 'GENERAL_PARTNERSHIP'
    | 'FOREIGN_ENTITY';
  firstName?: string;
  lastName?: string;
  address?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };
  phone?: string;
  dateOfBirth?: string;
  dateOfFormation?: string;
  isUSPerson?: boolean;
  isSingleMemberLLC?: boolean;
  createdAt: string;
  updatedAt: string;
  kycAml?: {
    applicantId?: string;
    checkId: string;
    lastDateChecked?: Date;
    status: string;
    result: string;
    reason: string;
    breakDown: object;
    overrideStatus?: string;
    [prop: string]: any;
  };
  email?: string;
  primarySignatory?: Signer;
  secondarySignatory?: Signer;
  beneficialOwner?: Signer;
  taxDetails?: {
    registrationType?: 'INDIVIDUAL' | 'ENTITY' | 'TRUST' | 'JOINT';
    taxIdentification: {
      type: string;
      value: string;
    };
  };
  bankingUser?: ProfileBankingUser;
}

export interface ProfilesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Profile[];
}
