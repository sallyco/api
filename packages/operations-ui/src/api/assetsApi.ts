export interface TeamMember {
  name?: string;
  role?: string;
  linkedInUrl?: string;
  image?: string;
}

export interface Asset {
  id: string;
  ownerId: string;
  tenantId: string;
  dealId: string;
  assetType: string; //enum: ['COMPANY',]
  name: string;
  details?: [
    {
      heading: string;
      body: string;
    }
  ];
  properties?: [
    {
      key: string;
      value: string;
    }
  ];
  contact?: {
    name: string;
    email: string;
    phone: string;
  };
  advisors: string[];
  description: string;
  assetUrl: string;
  team: TeamMember[];

  funding?: {
    round: string;
    targetRaiseAmount: string;
    securityType: string;
    preValuation: string;
  };
  revenueHistory: string[];
  costsHistory: string[];

  invitedOrganizers: string[];

  images: string[];
  artist?: {
    Name?: string;
    Location?: string;
    Images?: string;
  };

  videoURL: string;
  title: string;
  isEntity: boolean;
  entityInfo: {
    name: string;
    stateOfFormation: string;
    countryOfFormation: string;
  };
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  isPublic: boolean;
  isDeleted: boolean;
  files: string[];
  video: string;
  pithcDoc: string;
  logo?: string;
  banner?: string;
  appraisedValue?: number;
  fractionalOwnershipAmount?: number;
  category?: number;
  size?: string;
  additionalProperties: any;
}

export interface AssetsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Asset[];
}
