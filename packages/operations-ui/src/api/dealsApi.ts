export interface Deal {
  id: string;
  ownerId: string;
  tenantId: string;
  entityId: string;
  profileId: string;
  name: string;
  description: string;
  assetUrl: string;
  marketingLogo: string;
  marketingSlogan: string;
  portfolioCompanyName: string;
  portfolioCompanyState: string;
  portfolioCompanyEntity: string;
  companyMarketSector: string;
  raiseAmount: number;
  previouslyRaisedAmount: number;
  securityType: string;
  files: string[];
  estimatedCloseDate: string;
  minInvestmentAmount: number;
  carryPercentage: number;
  portfolioCompanyContact: {
    firstName: string;
    lastName: string;
    email: string;
  };
  status: string;
  createdAt: string;
  updatedAt: string;
  organizerFiles?: string[];
}

export interface DealsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Deal[];
}
