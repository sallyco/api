export interface SchemaDescription {
  title: string;
  type: string;
  properties: any;
  required: any;
  additionalProperties: boolean;
}
