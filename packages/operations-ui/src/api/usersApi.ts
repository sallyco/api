export interface User {
  id: string;
  username: string;
  enabled: boolean;
  totp: boolean;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  notBefore: number;
  createdTimestamp: string;
  contacts: object[];
  attributes: {
    phone: string;
    tenantId: string;
    termsOfService: string;
  };
  type: string;
  tenantId: string;
}
