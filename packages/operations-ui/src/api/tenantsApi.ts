export interface Tenant {
  id: string;
  inverted: boolean;
  name: string;
  url: string;
  assets: TenantAssetsState;
  settings: {
    emails: {
      fromAddress: string;
    };
  };
  owner: {
    name: string;
    email: string;
    phone: string;
  };
  dealLimit: number;
  dealLimits?: {
    maxDealCount?: number;
    maxInvestorCount?: number;
    maxTargetRaiseAmount?: number;
    perDealMaxInvestorCount?: number;
    perDealMaxTargetRaiseAmount?: number;
    costPerSPV?: number;
  };
  omdUrl?: string;
  feesPaymentsAccount?: {
    bankName: string;
    accountNumber: string;
    routingNumber: string;
    accountName: string;
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
    };
  };
  blueSkyPaymentsAccount?: {
    bankName: string;
    accountNumber: string;
    routingNumber: string;
    accountName: string;
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
    };
  };
}
interface TenantAssetsState {
  images: TenantAssetsImages;
  colors: TenantAssetsColors;
}

interface TenantAssetsImages {
  logo: string;
  logoInverted?: string;
  favicon?: string;
}

interface TenantAssetsColors {
  primaryColor: string;
  secondaryColor: string;
}
