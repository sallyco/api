export interface Invitation {
  id: string;
  acceptableBy: string;
  invitationdTo: [
    {
      id: string;
      type: string;
      relation: string;
    }
  ];
  invitationdRole: string;
  redirectUrl?: string;
  acceptedAt: string;
  createdAt: string;
  updatedAt: string;
  isDeleted: boolean;
}

export interface InvitationsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Invitation[];

  dealId?: string;
}
