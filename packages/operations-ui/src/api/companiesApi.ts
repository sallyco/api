export interface Company {
  bankAccount?: {
    counterpartyId: string;
    name: string;
    accountId: string;
    subAccountId: string;
  };
  id: string;
  ownerId: string;
  tenantId: string;
  dealId: string;
  profileId: string;
  companyId: string;
  name: string;
  email: string;
  files: string[];
  status: string;
  isDocsSigned: boolean;
  isKycAmlPassed: boolean;
  amount: number;
  ein: string;
  createdAt: string;
  updatedAt: string;
}

export interface CompaniesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Company[];

  dealId?: string;
}
