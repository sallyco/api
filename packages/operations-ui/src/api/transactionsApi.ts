import { Deal } from './dealsApi';
import { Subscription } from './subscriptionsApi';

export interface Transaction {
  id: string;
  accountID: string;
  amount: number;
  counterpartyId: string;
  dealId: string;
  direction: string;
  status: string;
  subscriptionId: string;
  tenantId: string;
  transactionId: null;
  type: string;
  createdAt: string;
  addedToNetwork?: string;
  updatedAt: string;
  deal: Partial<Deal>;
  subscription: Partial<Subscription>;
}

export interface TransactionReconsilation {
  amount: number;
  balance: number;
  date: string;
  desc: string;
  id: string;
  summary?: string;
  type?: string;
  wire?: string | null;
  providerMeta?: {
    check_id?: string | null;
    type_source?: string | null;
    amount?: string | number | null;
    date?: string | null;
    wire_id?: string | null;
    desc?: string | null;
    wire?: string | null;
    book_id?: string | null;
    type?: string | null;
    summary?: string | null;
    balance?: string | number | null;
    billpay_payment_id?: string | null;
    id?: string | null;
    trace_id?: string | null;
    ach_id?: string | null;
    fingerprint?: string | null;
    check_number?: string | null;
  };
}
