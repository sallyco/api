export interface Entity {
  id: string;
  dealId: string;
  ownerId: string;
  tenantId: string;
  name: string;
  entityType: string;
  countryOfFormation: string;
  stateOfFormation: string;
  regulationType: string;
  entityDocuments: {
    operatingAgreement: string;
    privatePlacementMemorandum: string;
    subscriptionAgreement: string;
  };
  ein: string;
  bankAccount: {
    bankName: string;
    accountNumber: string;
    routingNumber: string;
    accountName: string;
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
    };
  };
  formD: {
    cik?: string;
    cikPassword?: string;
    ccc?: string;
    accessionNumber?: string;
    fileNumber?: string;
    filedDate?: string;
  };
  createdAt: string;
  updatedAt: string;
  additionalProperties: any;
  bankingNodeId?: string;
  transactions?: object[];
  files?: string[];
}

export interface EntitiesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Entity[];
}
