export interface WorkflowProcess {
  id: string; //The id of the process definition.
  key: string; //The key of the process definition, i.e., the id of the BPMN 2.0 XML process definition.
  category: string; //The category of the process definition.
  description: string; //The description of the process definition.
  name: string; //The name of the process definition.
  version: number; //The version of the process definition that the engine assigned to it.
  resource: string; //The file name of the process definition.
  deploymentId: string; //The deployment id of the process definition.
  diagram: string; //The file name of the process definition diagram, if it exists.
  suspended: boolean; //A flag indicating whether the definition is suspended or not.
  tenantId: string; //The tenant id of the process definition.
  versionTag: string; //The version tag of the process or null when no version tag is set
  historyTimeToLive: number; //History time to live value of the process definition. Is used within History cleanup.
  startableInTasklist: boolean; //A flag indicating whether the process definition is startable in Tasklist or not.

  xml: string | 'undefined';
}

export interface WorkflowInstance {
  id: string; //The id of the process instance.
  definitionId: string; //The id of the process definition this instance belongs to.
  businessKey: string; //The business key of the process instance.
  caseInstanceId: string; //The id of the case instance associated with the process instance.
  ended: boolean; //A flag indicating whether the process instance has ended or not. Deprecated: will always be false!
  suspended: boolean; //A flag indicating whether the process instance is suspended or not.
  tenantId: string; //The tenant id of the process instance.

  variables: any;
}

export interface WorkflowTask {
  id: string; // The id of the task.
  name: string; // The tasks name.
  assignee: string; // The user assigned to this task.
  created: string; // The time the task was created. Default format* yyyy-MM-dd'T'HH:mm:ss.SSSZ.
  due: string; // The due date for the task. Default format* yyyy-MM-dd'T'HH:mm:ss.SSSZ.
  followUp: string; // The follow-up date for the task. Default format* yyyy-MM-dd'T'HH:mm:ss.SSSZ.
  delegationState: string; // The delegation state of the task. Corresponds to the DelegationState enum in the engine. Possible values are RESOLVED and PENDING.
  description: string; // The task description.
  executionId: string; // The id of the execution the task belongs to.
  owner: string; // The owner of the task.
  parentTaskId: string; // The id of the parent task, if this task is a subtask.
  priority: number; //The priority of the task.
  processDefinitionId: string; // The id of the process definition this task belongs to.
  processInstanceId: string; // The id of the process instance this task belongs to.
  caseExecutionId: string; // The id of the case execution the task belongs to.
  caseDefinitionId: string; // The id of the case definition the task belongs to.
  caseInstanceId: string; // The id of the case instance the task belongs to.
  taskDefinitionKey: string; // The task definition key.
  suspended: boolean; // Whether the task belongs to a process instance that is suspended.
  formKey: string; // If not null, the form key for the task.
  tenantId: string; // If not null, the tenantId for the task.

  variables: any;
  formVariables: object;
}

export interface WorkflowIncident {
  id: string; //The id of the incident.
  processDefinitionId: string; //The id of the process definition this incident is associated with.
  processInstanceId: string; //The id of the process instance this incident is associated with.
  executionId: string; //The id of the execution this incident is associated with.
  incidentTimestamp: string; //The time this incident happened. Default format* yyyy-MM-dd'T'HH:mm:ss.SSSZ.
  incidentType: string; //The type of incident, for example: failedJobs will be returned in case of an incident which identified a failed job during the execution of a process instance. See the User Guide for a list of incident types.
  activityId: string; //The id of the activity this incident is associated with.
  failedActivityId: string; //The id of the activity on which the last exception occurred.
  causeIncidentId: string; //The id of the associated cause incident which has been triggered.
  rootCauseIncidentId: string; //The id of the associated root cause incident which has been triggered.
  configuration: string; //The payload of this incident.
  tenantId: string; //The id of the tenant this incident is associated with.
  incidentMessage: string; //The message of this incident.
  jobDefinitionId: string; //The job definition id the incident is associated with.
}
