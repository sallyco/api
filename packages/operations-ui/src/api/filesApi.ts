export interface File {
  id: string;
  name: string;
  type: string;
  key: string;
  lastModified: number;
  createdAt: string;
  updatedAt: string;
  generating?: boolean;
  isPublic?: boolean;
  tenantId: string;
}

export interface FilesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: File[];
}
/**
export async function uploadFile(file: any, tenantId: string = "master") {
  const url = `${baseUrl}/files/upload`;
  const { data } = await axios.post<any>(url, file, {
    headers: {
      "Content-Type": "multipart/form-data",
      "x-file-upload-tenant-id": tenantId,
    },
  });

  return data;
}

export async function uploadImportFile(
  tenantId: string,
  ownerId: string,
  file: any
) {
  const url = `${baseUrl}/import/${tenantId}/${ownerId}`;
  const { data } = await axios.post<any>(url, file, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });

  return data;
}

export async function uploadDocCompany(file: any, companyId: string) {
  const url = `${baseUrl}/companies/documents-upload/${companyId}`;
  const { data } = await axios.put<any>(url, file, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });

  return data;
}
export async function downloadFileById(
  fileId: string,
  tenantId: string = "master"
) {
  const url = `${baseUrl}/files/${fileId}/download`;

  const { data } = await axios.get(url, {
    headers: {
      "x-file-download-tenant-id": tenantId,
    },
  });

  return data;
}
**/
