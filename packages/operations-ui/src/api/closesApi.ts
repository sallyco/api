export interface Close {
  id: string;
  dealId: string;
  entityId: string;
  files: string[];
  amount: number;
  status: string;
  fundManagerSigned?: boolean;
  needsApproval?: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface ClosesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Close[];
  dealId?: string;
}
