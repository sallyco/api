export interface Subscription {
  bankAccount?: {
    counterpartyId: string;
    name: string;
    accountId: string;
    subAccountId: string;
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
    };
  };
  id: string;
  ownerId: string;
  tenantId: string;
  dealId: string;
  profileId: string;
  subscriptionId: string;
  name: string;
  email: string;
  files: string[];
  status: string;
  isAmountMatched: boolean;
  isDocsSigned: boolean;
  isKycAmlPassed: boolean;
  amount: number;
  createdAt: string;
  updatedAt: string;
}

export interface SubscriptionsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Subscription[];

  dealId?: string;
}

export interface FundDocument {
  subscriptionId?: string;
  tenantId?: string;
  investorName?: string;
  investorEmail?: string;
  dealId?: string;
  dealName?: string;
  organizerId?: string;
  organizerName?: string;
  organizerFirstName?: string;
  organizerLastName?: string;
  entityId?: string;
  entityDocs?: object;
  closeId?: string;
  organizerSigned?: boolean;
  managerSigned?: boolean;
  investorSigned?: boolean;
  subscriptionAmount?: number;
  subscriptionCreatedAt?: string;
  targetRaiseAmount?: number;
}
