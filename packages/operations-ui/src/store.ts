import create from 'zustand';
import { persist } from 'zustand/middleware';

const isClient = typeof window !== 'undefined';

interface UiState {
  collapsed: boolean;
  toggleCollapsed: () => void;
  darkTheme: boolean;
  toggleDarkTheme: () => void;
}
export const useUiStore = create<UiState>(
  persist(
    (set) => ({
      collapsed: false,
      toggleCollapsed: () => set((state) => ({ collapsed: !state.collapsed })),
      darkTheme: false,
      toggleDarkTheme: () => set((state) => ({ darkTheme: !state.darkTheme })),
    }),
    { name: 'ui-state' }
  )
);
