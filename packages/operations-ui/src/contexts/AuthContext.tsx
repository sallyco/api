import React, { createContext, useContext } from 'react';
import { styled } from '@mui/material/styles';
import { SnackbarUtils } from '@packages/wired-gbt-ui';
import { useRouter } from 'next/router';
import { LinearProgress } from '@mui/material';
import { TenantsContext } from './TenantsContext';
import { getAuthUrl, getRedirectUrl } from '../tools/urlFormatters';

import Log from '../tools/Log';

const AuthContext = createContext<any>({});
export { AuthContext };

import { signIn, getSession } from 'next-auth/client';

const ThemedLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 8,
  borderRadius: 0,
}));

export const Authorization = ({ children }: { children: any }) => {
  const [authReady, setAuthReady] = React.useState(false);
  const tenant: any = useContext(TenantsContext);
  const tokenUrl = getAuthUrl(tenant.id);

  const router = useRouter();

  const parseJwt = (token) => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join('')
    );

    return JSON.parse(jsonPayload);
  };

  React.useEffect(() => {
    async function getToken() {
      const session = await getSession();
      const isUser = !!session?.user;
      if (!isUser) {
        const url = getRedirectUrl(tenant.id);
        signIn('oauth2', { callbackUrl: `${url}/dashboard` });
        return;
      }

      const fullurl = `${tokenUrl}/token`;
      const response = await fetch(fullurl, {
        body: new URLSearchParams({
          client_id: process.env.NEXT_PUBLIC_CLIENT_ID,
          client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
          grant_type: 'refresh_token',
          refresh_token: String(session.refreshToken),
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        mode: 'cors',
        method: 'POST',
        cache: 'no-store',
      });

      const tokens = await response.json();
      if (!response.ok) {
        if (tokens?.error === 'invalid_grant') {
          try {
            SnackbarUtils.warning('Session expired, logging out');
          } catch {
            Log.info('Session expired, logging out', tokens);
          }
          router.push('/logout');
          return;
        }
        SnackbarUtils.error(tokens?.error_description);
      }

      const { exp } = parseJwt(tokens.access_token);
      if (Date.now() >= exp * 1000) {
        router.push('/logout');
        return;
      }

      function setAuth(token, tenantId) {
        return new Promise(function (resolve, reject) {
          const messageChannel = new MessageChannel();
          messageChannel.port1.onmessage = function (event) {
            if (event.data.error) {
              reject(event.data.error);
            } else {
              resolve(event.data);
            }
          };
          navigator.serviceWorker.controller.postMessage(
            {
              type: 'SET_TOKEN',
              token: tokens.access_token,
              tenantId: tenantId,
            },
            [messageChannel.port2]
          );
        });
      }
      try {
        await setAuth(tokens.access_token, tenant.id);
        setAuthReady(true);
      } catch {
        setAuthReady(false);
      }
    }

    getToken();
  }, [tenant.id, tokenUrl, router]);

  if (authReady) {
    return children;
  }

  return <ThemedLinearProgress />;
};
