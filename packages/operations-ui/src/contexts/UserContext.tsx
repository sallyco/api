import React, { createContext, useContext } from 'react';
import * as Sentry from '@sentry/browser';

import { User } from '../api/usersApi';
import { TenantsContext } from './';
import { useRequest } from '@packages/wired-gbt-ui';

const UserContext = createContext<Record<string, User>>({});
export { UserContext };

export const UserProvider = ({ children }: { children: any }) => {
  const tenant: any = useContext(TenantsContext);
  const { data } = useRequest('/users/me', {
    swrOptions: {
      shouldRetryOnError: false,
    },
  });

  React.useEffect(() => {
    if (data?.username && tenant?.id) {
      Sentry.setUser({
        id: data.id,
        username: data.username,
        tenant: tenant.id,
      });
    }
  }, [data, tenant]);

  return <UserContext.Provider value={data}>{children}</UserContext.Provider>;
};
