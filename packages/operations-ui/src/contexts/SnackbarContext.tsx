import React from 'react';
import { SnackbarProvider } from 'notistack';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import Fade from '@mui/material/Fade';

const useStyles = makeStyles((theme: Theme) => ({
  // default variant
  contentRoot: {
    backgroundColor: 'aqua',
  },
  variantSuccess: {
    backgroundColor: theme.palette.success.main,
  },
  variantError: {
    backgroundColor: theme.palette.error.main,
  },
  variantInfo: {
    backgroundColor: theme.palette.info.main,
  },
  variantWarning: {
    backgroundColor: theme.palette.warning.main,
  },
}));

export const SnacksProvider: React.FC = ({ children }) => {
  const classes = useStyles();
  return (
    <SnackbarProvider
      classes={classes}
      maxSnack={3}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      TransitionComponent={Fade}
    >
      {children}
    </SnackbarProvider>
  );
};
