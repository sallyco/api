import React, { createContext } from 'react';
import { LinearProgress } from '@mui/material';
import { getTenantIdFromWindow } from '../tools/urlFormatters';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Theme } from '@mui/material/styles';
import { Tenant } from '../api/tenantsApi';
import { theme } from '../theme.styles';
import { useUiStore } from '../store';

import { useRequest } from '@packages/wired-gbt-ui';

import _ from 'lodash';

declare module '@mui/styles/defaultTheme' {
  type DefaultTheme = Theme;
}

const TenantsContext = createContext<Record<string, Tenant>>({});

export const TenantsProvider = ({ children }: { children: any }) => {
  let tenantId = 'master';
  if (typeof window !== 'undefined') {
    tenantId = getTenantIdFromWindow(window);
  }

  const { data } = useRequest(`/tenants/${tenantId}?filter[include][]=whiteLabel`);
  const darkTheme = useUiStore((state) => state.darkTheme);

  return !data ? (
    <LinearProgress
      sx={{
        height: 8,
        borderRadius: 0,
      }}
    />
  ) : (
    <ThemeProvider
      theme={createTheme(
        _.merge({ palette: { mode: darkTheme ? 'dark' : 'light' } }, theme, data.theme)
      )}
    >
      <TenantsContext.Provider value={{ ...data, isMaster: data.id === 'master' }}>
        {children}
      </TenantsContext.Provider>
    </ThemeProvider>
  );
};

export { TenantsContext };
