// UI imports
import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Button,
  Dialog,
  DialogContentText,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import KycAmlExceptionsChart from 'src/components/kyc-aml/KycAmlExceptionsChart';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import { CloseIcon } from 'react-line-awesome';
import LaunchIcon from '@mui/icons-material/Launch';
import FinancialTransactionsChart from 'src/components/financial-transactions/FinancialTransactionsChart';
import FundDocumentsChart from 'src/components/fund-documents/FundDocumentsChart';
import DealChart from 'src/components/deals/DealChart';
import useSWR from 'swr';
import { ContentLoader } from '@packages/gbt-ui';

const PREFIX = 'ChartDialog';

const classes = {
  closeButton: `${PREFIX}-closeButton`,
  icon: `${PREFIX}-icon`,
  typography: `${PREFIX}-typography`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.closeButton}`]: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

  [`& .${classes.icon}`]: {
    size: 'small',
    padding: 2,
  },

  [`& .${classes.typography}`]: {
    size: 8,
    padding: 2,
    color: '#FFFFFF',
  },
}));

const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

function ChartDialog({ chart_Name, chart_Data, title }) {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const [loading, setLoading] = React.useState(false);

  const metadatakycmlUrl = baseUrl + `/profiles`;
  const { data: profiles } = useSWR(chart_Name === 'kycaml' ? metadatakycmlUrl : null, fetcher);

  const metadatatransUrl = baseUrl + `/transactions-all`;
  const { data: transactions } = useSWR(
    chart_Name === 'financial-trans' ? metadatatransUrl : null,
    fetcher
  );

  function Chart({ chartName }) {
    switch (chartName) {
      case 'kycaml':
        return (
          <>
            {!profiles ? (
              <ContentLoader />
            ) : (
              <>
                <KycAmlExceptionsChart profiles={profiles.data} />
              </>
            )}
          </>
        );
      case 'fund-docs':
        return <FundDocumentsChart fundDocuments={chart_Data} />;
      case 'financial-trans':
        return (
          <>
            {!transactions ? (
              <ContentLoader />
            ) : (
              <>
                <FinancialTransactionsChart transactions={transactions} />;
              </>
            )}
          </>
        );

      case 'deal':
        return <DealChart fundDocuments={chart_Data} />;
      default:
        return <></>;
    }
  }
  return (
    <Root>
      <IconButton size="small" color="primary" aria-label="add an alarm" onClick={handleClickOpen}>
        <LaunchIcon className={classes.icon} />
        <Typography className={classes.icon}> </Typography>
      </IconButton>
      <Dialog
        fullWidth={true}
        maxWidth={'lg'}
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle>
          <Typography>{title}</Typography>
          {open ? (
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
              size="large"
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText>
            <Chart chartName={chart_Name} />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </Root>
  );
}

export default ChartDialog;
