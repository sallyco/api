import React from 'react';
//import { Form } from "semantic-ui-react";

interface MaskedFormFieldProps {
  name: string;
  showErrorsInline?: boolean;
  value?: any;
  [x: string]: any;
  // Additional properties: https://github.com/s-yadav/react-number-format
}

export const MaskedFormField = ({ ...fieldProps }: MaskedFormFieldProps) => {
  const { showErrorsInline, ...rest } = fieldProps;

  return <></>;
};
/**
    <Field {...rest}>
      {({
        field: { onBlur, ...field },
        form: { submitCount, touched, errors, handleBlur },
        meta,
        ...props
      }) => {
        return React.createElement(NumberFormat, {
          ...rest,
          ...field,
          ...props,
          ...((submitCount >= 1 || touched[field.name]) && errors[field.name]
            ? {
                error:
                  showErrorsInline === false
                    ? true
                    : {
                        content: errors[field.name],
                      },
              }
            : {}),
          customInput: Form.Input,
          onBlur: handleBlur,
        });
      }}
    </Field>
  );
};
**/

interface FormFieldProps {
  name: string;
  component?: any;
  showErrorsInline?: boolean;
  value?: any;
  [x: string]: any;
}

export const FormField = ({ component = '', ...fieldProps }: FormFieldProps) => {
  const { showErrorsInline, ...rest } = fieldProps;

  return <></>;
};
/**
    <Field {...rest}>
      {({
        field: { value, onBlur, ...field },
        form: { setFieldValue, submitCount, touched, errors, handleBlur },
        meta,
        ...props
      }) => {
        return React.createElement(component, {
          ...rest,
          ...field,
          ...props,
          ...(component === Form.Radio || component === Form.Checkbox
            ? {
                checked:
                  component === Form.Radio ? fieldProps.value === value : value,
              }
            : {
                value: value || "",
              }),

          ...((submitCount >= 1 || touched[field.name]) && errors[field.name]
            ? {
                error:
                  showErrorsInline === false
                    ? true
                    : {
                        content: errors[field.name],
                      },
              }
            : {}),
          onChange: (e, { value: newValue, checked }) =>
            setFieldValue(fieldProps.name, newValue || checked),
          onBlur: handleBlur,
        });
      }}
    </Field>
  );
};
**/
