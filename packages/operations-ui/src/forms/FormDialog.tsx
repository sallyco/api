// UI imports
import React from 'react';
import { Form } from '@packages/gbt-ui';
import { Container } from '@mui/material';
import { FullScreenDialog } from '@packages/gbt-ui';

import { toCapitalizedWords } from '../tools/textFormatters';

function FormDialog({
  open = false,
  closeHandler,
  schema,
  onSubmit,
  title,
  formData = null,
  uiSchema = {},
}) {
  schema.title = title;
  Object.keys(schema.properties).forEach(
    (i) => (schema.properties[i].title = toCapitalizedWords(i))
  );
  const [loading, setLoading] = React.useState(false);
  return (
    <FullScreenDialog
      title={formData?.name}
      open={open}
      setOpen={closeHandler}
      loading={loading}
      setLoading={setLoading}
    >
      <Container>
        <Form schema={schema} uiSchema={uiSchema} onSubmit={onSubmit} formData={formData} />
      </Container>
    </FullScreenDialog>
  );
}

export default FormDialog;
