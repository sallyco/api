export const formatDate = (date: string) => {
  const dateObj = new Date(date);
  const year = dateObj.getFullYear();
  const month = (1 + dateObj.getMonth()).toString().padStart(2, '0');
  const day = dateObj.getDate().toString().padStart(2, '0');
  return month + '/' + day + '/' + year;
};
