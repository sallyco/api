import React from 'react';
import { FileIcon, FilePdfIcon, FilePowerpointIcon, FileWordIcon } from 'react-line-awesome';
import { File } from '../api/filesApi';

export const fileIcons = (file: File) => {
  let type = file.type;
  const nameTyping = file.name.split('.').pop();
  if (!type || type === 'application/octet-stream') {
    type = nameTyping;
  }
  switch (type) {
    case 'pdf':
    case 'application/pdf':
      return <FilePdfIcon style={{ color: 'red' }} className="la-2x" />;
    case 'docx':
    case 'pages':
    case 'application/docx':
      return <FileWordIcon style={{ color: 'blue' }} />;
    case 'pptx':
    case 'application/pptx':
      return <FilePowerpointIcon style={{ color: 'orange' }} />;
    default:
      return <FileIcon />;
  }
};
