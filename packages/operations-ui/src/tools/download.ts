import { SnackbarUtils, useRequest } from '@packages/wired-gbt-ui';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

export const DownloadImportSchema = async () => {
  //try {
  //  const response = await useRequest("/import/schema");
  //  if (response.status !== 200) {
  //    SnackbarUtils.error(
  //      "Download is unavailable at this time for this document"
  //    );
  //    return;
  //  }
  //  const url = window.URL.createObjectURL(new Blob([response.data]));
  //  const link = document.createElement("a");
  //  link.href = url;
  //  link.setAttribute("download", "schema.xlsx");
  //  document.body.appendChild(link);
  //  link.click();
  //} catch {
  //  SnackbarUtils.error(
  //    "Download is unavailable at this time for this document"
  //  );
  //}
};

export const DownloadFile = async (fileId: string, fileName?: string, tenantId = 'master') => {
  try {
    if (!fileName) {
      const fileResponse = await fetch(`${baseUrl}/files/${fileId}`);
      const fileData = await fileResponse.json();
      fileName = fileData.name;
    }

    const response = await fetch(`${baseUrl}/files/${fileId}/download`, {
      headers: {
        'x-file-download-tenant-id': tenantId,
      },
      cache: 'force-cache',
    });
    if (response.status !== 200) {
      SnackbarUtils.error('Download is unavailable at this time for this file');
      return;
    }
    const object = await response.blob();
    const url = window.URL.createObjectURL(new Blob([object]));
    const link = document.createElement('a');
    link.href = url;
    if (fileName) {
      link.setAttribute('download', fileName);
    }
    document.body.appendChild(link);
    link.click();
  } catch (e) {
    SnackbarUtils.error('Download is unavailable at this time for this document');
  }
};

export const DownloadMassFiles = async (files) => {
  return Promise.all(files.map((file) => DownloadFile(file)));
};
