import { Profile } from '../api/profilesApi';
import { Theme } from '@mui/material';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

const useKycAmlChipStyles = makeStyles((theme: Theme) =>
  createStyles({
    redChip: {
      backgroundColor: 'red',
      color: 'white',
    },
    orangeChip: {
      backgroundColor: 'orange',
      color: 'white',
    },
    greenChip: {
      backgroundColor: 'green',
      color: 'white',
    },
    blueChip: {
      backgroundColor: 'blue',
      color: 'white',
    },
    grayChip: {
      backgroundColor: '#808080',
      color: 'white',
    },
    yellowChip: {
      backgroundColor: 'yellow',
      color: 'black',
    },
    kycChip: {
      flexGrow: 1,
      maxWidth: '160px',
    },
    firstChip: {
      paddingRight: '14px',
      borderRadius: '16px 0 0 16px',
      marginRight: '-5px',
      clipPath: 'polygon(0 0, 100% 0%, calc(100% - 16px) 100%, 0% 100%)',
      backfaceVisibility: 'hidden',
    },
    middleChip: {
      borderRadius: '0px',
    },
    lastChip: {
      paddingLeft: '14px',
      marginLeft: '-5px',
      borderRadius: '0 16px 16px 0',
      clipPath: 'polygon(16px 0, 100% 0%, 100% 100%, 0% 100%)',
      backfaceVisibility: 'hidden',
    },
    kycChipContainer: {
      display: 'flex',
      flexWrap: 'nowrap',
      justifyContent: 'center',
    },
  })
);

export type ReportLabel = 'IN PROGRESS' | 'EXCEPTION' | 'NOT RUN' | 'CLEAR';
const formatReportLabel = (profile: Profile, report_name: string) => {
  const report = profile?.kycAml?.providerMeta?.reports?.find(
    (report) => report.name === report_name
  );
  let label = (report?.result ?? 'IN PROGRESS').toUpperCase();

  if (profile?.kycAml?.status === 'EXCEPTION') {
    label = 'EXCEPTION';
  }
  if (profile?.kycAml?.status === 'IN_PROGRESS') {
    label = 'IN PROGRESS';
  }

  if (!report) {
    label = 'NOT RUN';
  }
  return label as ReportLabel;
};

export { formatReportLabel, useKycAmlChipStyles };
