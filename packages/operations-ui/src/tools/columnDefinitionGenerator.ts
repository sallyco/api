import moment from 'moment';
import { toCapitalizedWords } from './textFormatters';

const SUPPORTED_DATE_FORMATS = [moment.ISO_8601];

export const getColumnDefinition = (fieldMap: any[], initialColumnDef: any[]): any[] => {
  const colDef = [...initialColumnDef];
  Object.keys(fieldMap)
    .filter((key) => !['object', 'array'].includes(fieldMap[key]))
    .forEach((key) => {
      if (colDef.filter((col) => col.name === key).length <= 0) {
        colDef.push({
          name: key,
          label: toCapitalizedWords(key),
          options: {
            display: false,
            filter: true,
            sort: true,
            ...(fieldMap[key] === 'boolean'
              ? {
                  customBodyRender: (value, tableMeta, updateValue) =>
                    value === undefined ? 'Not Set' : value ? 'Yes' : 'No',
                }
              : {}),
            ...(fieldMap[key] === 'date'
              ? {
                  customBodyRender: (value, tableMeta, updateValue) =>
                    !value
                      ? 'Not Set'
                      : moment(value, SUPPORTED_DATE_FORMATS, true).isValid()
                      ? moment(value).format('LL')
                      : 'Invalid Date',
                }
              : {}),
          },
        });
      }
    });
  return colDef;
};
