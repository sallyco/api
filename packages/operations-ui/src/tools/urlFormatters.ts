import absoluteUrl from 'next-absolute-url';
import { Tenant } from '../api/tenantsApi';

const getTenantIdFromWindow = (window) => {
  const domain = /:\/\/([^\/]+)/.exec(window.location.href)[1];
  const subdomain = domain.split('.')[0];
  if (
    !subdomain.includes('localhost') &&
    subdomain !== 'operations' &&
    subdomain !== 'stage' &&
    subdomain !== 'dev' &&
    subdomain !== 'qa'
  ) {
    return subdomain;
  }
  return 'master';
};

const getTenantIdFromReq = (req: any) => {
  const { host } = absoluteUrl(req);
  const subdomain = host.split('.')[0];
  if (
    !subdomain.includes('localhost') &&
    subdomain !== 'operations' &&
    subdomain !== 'stage' &&
    subdomain !== 'dev' &&
    subdomain !== 'qa'
  ) {
    return subdomain;
  }
  return 'master';
};

const getAuthUrl = (tenantId: string) => {
  return `${process.env.NEXT_PUBLIC_AUTH_URL}/${tenantId}/protocol/openid-connect`;
};

const getRedirectUrl = (tenantId: string) => {
  const protocol = 'https'; //process.env.NODE_ENV === "development" ? "http" : "https";
  let host = process.env.NEXTAUTH_URL_BASE;
  if (tenantId !== 'master') {
    host = `${tenantId}.${host}`;
  }

  return `${protocol}://${host}`;
};

const getOmdUrl = (tenant: Tenant) => {
  let environment: string | false = process.env.NEXT_PUBLIC_ENVIRONMENT;
  const tenantId = tenant.id === 'gbt' ? 'app' : tenant.id;
  if (process.env.NEXT_PUBLIC_ENVIRONMENT === 'production') {
    environment = false;
  }
  if (process.env.NEXT_PUBLIC_ENVIRONMENT === 'sandbox') {
    environment = 'staging';
  }
  if (process.env.NEXT_PUBLIC_ENVIRONMENT === 'localhost') {
    environment = 'local';
  }

  return (
    tenant.omdUrl ??
    `https://${tenantId}${environment ? '.' + environment : ''}.glassboardtech.com/`
  );
};

export { getTenantIdFromWindow, getTenantIdFromReq, getAuthUrl, getRedirectUrl, getOmdUrl };
