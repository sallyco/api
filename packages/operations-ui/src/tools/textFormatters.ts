const toCapitalizedWords = (name: string) => {
  const words = name.match(/[A-Za-z][a-z]*/g) || [];
  return words.map(capitalize).join(' ');
};
const capitalize = (word: string) => word.charAt(0).toUpperCase() + word.substring(1);

export { toCapitalizedWords, capitalize };
