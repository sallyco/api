const dealStatusColors: {
  [key: string]:
    | 'teal'
    | 'red'
    | 'orange'
    | 'yellow'
    | 'olive'
    | 'green'
    | 'blue'
    | 'violet'
    | 'purple'
    | 'pink'
    | 'brown'
    | 'grey'
    | 'black'
    | undefined;
} = {
  PENDING: 'olive',
  DRAFT: 'olive',
  ONBOARDING: 'orange',
  OPEN: 'green',
  CLOSING: 'blue',
  'IN CLOSING': 'blue',
  CLOSED: 'grey',
};

export const DealStatusColors = (status: string) => dealStatusColors[status];

const subscriptionStatusColors: {
  [key: string]:
    | 'teal'
    | 'red'
    | 'orange'
    | 'yellow'
    | 'olive'
    | 'green'
    | 'blue'
    | 'violet'
    | 'purple'
    | 'pink'
    | 'brown'
    | 'grey'
    | 'black'
    | undefined;
} = {
  INVITED: 'green',
  PENDING: 'olive',
  COMMITTED: 'orange',
  'FUNDS_IN_TRANSIT-MANUAL': 'teal',
  COMPLETED: 'blue',
  CLOSED: 'grey',
  CANCELLED: 'grey',
  TRANSFERRED: 'grey',
};

export const SubscriptionStatusColors = (status: string) => subscriptionStatusColors[status];
