import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import StateAmountsTable from '../../../components/files/StateAmountsTable';

let amountData;

beforeEach(() => {
  amountData = {
    firstInvestmentDate: '2021-09-30T06:04:30.766Z',
    totalNASAAFee: 160,
    stateSummaries: [
      {
        amount: 1,
        count: 1,
        method: 'Electronic',
        state: 'California',
        fee: 300,
      },
      {
        amount: 2,
        count: 1,
        method: 'Electronic',
        state: 'Utah',
        fee: 0,
      },
    ],
  };
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

afterEach(async () => {});

const renderComponent = (props?) => {
  return render(StateAmountsTable({ amountData }));
};

xdescribe('State Amounts Table Tests', () => {
  it('Happy render', () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it('Shows fee in table on successful load', async () => {
    renderComponent();

    await waitFor(() => expect(screen.getAllByText('Fee')).toBeTruthy());

    await waitFor(() => expect(screen.getByTestId('MuiDataTableBodyCell-4-0')).toBeInTheDocument());
    await waitFor(() =>
      expect(screen.getByTestId('MuiDataTableBodyCell-4-0').firstElementChild.innerHTML).toBe(
        `$${amountData.stateSummaries[0].fee}`
      )
    );

    await waitFor(() => expect(screen.getByTestId('footer-fee')).toBeInTheDocument());
    await waitFor(() =>
      expect(screen.getByTestId('footer-fee').innerHTML).toBe(
        `Total Fee: $${
          amountData.stateSummaries.reduce((a, i) => a + i.fee, 0) + amountData.totalNASAAFee
        }`
      )
    );
  });
});
