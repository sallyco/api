import React, { FC, ReactElement } from 'react';
import { render, RenderOptions, screen, waitFor } from '@testing-library/react';
import FilingsTable from '../../../components/filings/FilingsTable';
import { SWRConfig, cache } from 'swr';

const filingData = [
  {
    id: 'test-id',
    listId: 'needsblue',
    onClick: () => Promise.resolve(true),
    footer: 'test-footer',
    title: 'test-title',
    description: 'test-description',
    className: 'error',
  },
];

beforeEach(async () => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

afterAll(() => {
  cache.clear();
});

const CommonComponentWrapper: FC = ({ children }) => {
  return <SWRConfig value={{ dedupingInterval: 0 }}>{children}</SWRConfig>;
};

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, { wrapper: CommonComponentWrapper, ...options });

describe('Filings Table Tests', () => {
  it('Shows data in table on successful load', async () => {
    customRender(<FilingsTable filingData={filingData} />);

    await waitFor(() => expect(screen.getByTestId('pagination-rows')).toBeInTheDocument());

    await waitFor(() =>
      expect(screen.getByTestId('pagination-rows').parentElement.nextSibling.textContent).toBe(
        '1-1 of 1'
      )
    );

    await waitFor(() => expect(screen.getByTestId('MuiDataTableBodyCell-3-0')).toBeInTheDocument());

    await waitFor(() =>
      expect(screen.getByTestId('MuiDataTableBodyCell-3-0').children[0].innerHTML).toBe(
        'Needs Blue Sky'
      )
    );

    await waitFor(() =>
      expect(screen.getByTestId('MUIDataTableBodyRow-0').className).toBe('makeStyles-error-2')
    );
  });
});
