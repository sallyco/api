import React, { FC, ReactElement } from 'react';
import { render, RenderOptions, screen, waitFor } from '@testing-library/react';
import ClosedDealsTable from '../../../components/filings/ClosedDealsTable';
import { SWRConfig, cache } from 'swr';

beforeEach(async () => {
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

afterAll(() => {
  cache.clear();
});

const CommonComponentWrapper: FC = ({ children }) => {
  return <SWRConfig value={{ dedupingInterval: 0 }}>{children}</SWRConfig>;
};

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, { wrapper: CommonComponentWrapper, ...options });

describe('Closed Deals Table Tests', () => {
  it('Shows data in table on successful load', async () => {
    customRender(<ClosedDealsTable />);

    await waitFor(() => expect(screen.getByTestId('pagination-rows')).toBeInTheDocument());

    await waitFor(() =>
      expect(screen.getByTestId('pagination-rows').parentElement.nextSibling.textContent).toBe(
        '0-0 of 0'
      )
    );
  });
});
