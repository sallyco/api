import React from 'react';
import mockAxios from 'jest-mock-axios';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import AddressForm from '../../../components/profiles/AddressForm';

let address;
const onSuccess = (data) => Promise.resolve();

beforeEach(() => {
  address = {
    address1: '32589 Ruecker Trail',
    city: 'Hollisberg',
    state: 'California',
    postalCode: '81273',
    country: 'United States of America',
    address2: 'Paradise Street',
  };
  jest.spyOn(console, 'error').mockImplementation(() => {});
});

afterEach(async () => {
  await mockAxios.reset();
});

const renderComponent = (props?) => {
  const defaultProps = {
    address,
    onSuccess,
    ...props,
  };
  return render(<AddressForm {...defaultProps} />);
};

describe('State Amounts Table Tests', () => {
  it('Happy render', () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it('Shows fee in table on successful load', async () => {
    const result = renderComponent();

    await waitFor(() => expect(screen.getByTestId('address-change-submit')).toBeInTheDocument());

    const submit = await screen.getByTestId('address-change-submit');
    await waitFor(() => expect(submit).not.toHaveAttribute('disabled'));

    const someElement = result.container.querySelector('#address1');
    await userEvent.clear(someElement);
    await userEvent.tab();
    await waitFor(() => expect(someElement).toHaveValue(''));
    await waitFor(() => expect(submit).toHaveAttribute('disabled'));
  });
});
