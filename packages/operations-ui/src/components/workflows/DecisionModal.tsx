import React, { useEffect } from 'react';
import { useRequest } from '@packages/wired-gbt-ui';
import makeStyles from '@mui/styles/makeStyles';
import dynamic from 'next/dynamic';
import Head from 'next/head';

import { FullScreenDialog } from '@packages/gbt-ui';

type ReactDmn = {
  diagramXML: string;
  onLoading: () => void;
  onShown: () => void;
  onError: (err: any) => void;
};

const DmnViewerWithNoSSR = dynamic(() => import('./ReactDmn'), { ssr: false });

import { Box } from '@mui/material';

interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  workflowDecision: {
    id: string; //The id of the decision definition.
    key: string; //The key of the decision definition, i.e., the id of the DMN 2.0 XML decision definition.
    category: string; //The category of the decision definition.
    name: string; //The name of the decision definition.
    version: number; //The version of the decision definition that the engine assigned to it.
    resource: string; //The file name of the decision definition.
    deploymentId: string; //The deployment id of the decision definition.
    tenantId: string; //The tenant id of the decision definition.
    versionTag: string; //The version tag of the decision or null when no version tag is set
    historyTimeToLive: number; //History time to live value of the decision definition. Is used within History cleanup.
    decisionRequirementsDefinitionId: string;
    decisionRequirementsDefinitionKey: string;

    xml: string | 'undefined';
  };
}

const useStyles = makeStyles({
  '@global': {
    '.powered-by': {
      display: 'none !important',
    },
    '.bjs-powered-by': {
      display: 'none !important',
    },
    '.react-dmn-diagram-container': {
      height: '100%',
    },
  },
  root: {
    maxWidth: '100%',
  },
});

const DecisionModal = ({ open, setOpen, workflowDecision }: Props) => {
  const classes = useStyles();
  const { data, isValidating } = useRequest(
    workflowDecision ? `/decisions/${workflowDecision.key}/xml` : null,
    { swrOptions: { revalidateOnFocus: false } }
  );
  const [loading, setLoading] = React.useState(false);

  useEffect(() => setLoading(isValidating), [isValidating]);

  function onError(err) {
    console.error('failed to render diagram', err);
  }

  function onLoading() {
    console.log('loading diagram');
  }

  function onShown() {
    console.log('diagram shown');
  }

  return (
    <FullScreenDialog
      title={workflowDecision?.name}
      open={open}
      setOpen={setOpen}
      loading={loading}
      setLoading={setLoading}
    >
      <Head>
        <link rel="stylesheet" href="https://unpkg.com/dmn-js@11.0.1/dist/assets/dmn-js-drd.css" />
        <link
          rel="stylesheet"
          href="https://unpkg.com/dmn-js@11.0.1/dist/assets/dmn-js-decision-table.css"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/dmn-js@11.0.1/dist/assets/dmn-js-literal-expression.css"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/dmn-js@11.0.1/dist/assets/dmn-js-shared.css"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/dmn-js@11.0.1/dist/assets/dmn-font/css/dmn.css"
        />
      </Head>
      <Box my={2} mx={'auto'} height="100%" width="90%">
        {data?.dmnXml && (
          <DmnViewerWithNoSSR
            diagramXML={data.dmnXml}
            onLoading={onLoading}
            onShown={onShown}
            onError={onError}
          />
        )}
      </Box>
    </FullScreenDialog>
  );
};

export default DecisionModal;
