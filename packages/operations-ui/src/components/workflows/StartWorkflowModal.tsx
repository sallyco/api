/* eslint-disable react/no-children-prop */
import React from 'react';
import { Form } from '@packages/gbt-ui';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { Button } from '@mui/material';
import { ContentLoader, LargeDialog } from '@packages/gbt-ui';
import { useRequest, doChange, objectProps } from '@packages/wired-gbt-ui';
import { AngleDoubleRightIcon } from 'react-line-awesome';

interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  slideDirection?: 'down' | 'up';
  mutate?: () => void | undefined;
  workflowProcess: {
    id: string;
    name: string;
  };
}
const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  buttonSubmit: {
    float: 'right',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    backgroundColor: theme.palette.success.main,
    color: theme.palette.common.white,
  },
}));

const StartWorkflowModal = ({
  open,
  setOpen,
  slideDirection = 'up',
  mutate = undefined,
  workflowProcess,
}: Props) => {
  const classes = useStyles();
  const op = objectProps.workflows;
  const { data, isValidating, error } = useRequest(
    workflowProcess ? `${op.route}/forms/start/describe` : '',
    { swrOptions: { revalidateOnFocus: false } }
  );
  const onSubmit = async ({ formData }) => {
    await doChange(`${op.route}/processes/${workflowProcess.id}/start`, 'POST', formData);
    if (mutate) mutate();
    setOpen(false);
  };
  return (
    <LargeDialog
      title={workflowProcess?.name}
      open={open}
      setOpen={setOpen}
      slideDirection={slideDirection}
    >
      {!data ? (
        <ContentLoader errorMessage={error?.message} />
      ) : (
        <Form
          schema={data.definitions.StartWorkflow}
          uiSchema={{
            'ui:title': ' ',
          }}
          onSubmit={onSubmit}
        >
          <Button
            variant="contained"
            type="submit"
            className={classes.buttonSubmit}
            endIcon={<AngleDoubleRightIcon />}
          >
            Proceed
          </Button>
        </Form>
      )}
    </LargeDialog>
  );
};
export default StartWorkflowModal;
