import React from 'react';
import DmnJS from './dmn-viewer.development.js';

interface Props {
  dmnViewer?: any;
  containerRef?: any;
}

interface MyProps {
  url: any;
  diagramXML?: any;
  onLoading: any;
  onError: any;
  onShown: any;
}

interface MyState {
  diagramXML?: any;
  containerRef?: any;
}

class ReactDmn<Props> extends React.Component<MyProps, MyState> {
  containerRef: any = undefined;
  dmnViewer: any = undefined;

  constructor(props) {
    super(props);
    this.state = {};
    this.containerRef = React.createRef();
  }

  componentDidMount() {
    const { url, diagramXML } = this.props;

    const container = this.containerRef.current;

    this.dmnViewer = new (DmnJS as any)({ container });
    this.dmnViewer.on('import.done', (event) => {
      const { error, warnings } = event;

      if (error) {
        return this.handleError(error);
      }

      this.dmnViewer.getActiveViewer().get('canvas').zoom('fit-viewport');
      return this.handleShown(warnings);
    });

    if (url) {
      return this.fetchDiagram(url);
    }

    if (diagramXML) {
      return this.displayDiagram(diagramXML);
    }
  }

  componentWillUnmount() {
    this.dmnViewer.destroy();
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;

    if (props.url !== prevProps.url) {
      return this.fetchDiagram(props.url);
    }

    const currentXML = props.diagramXML || state.diagramXML;
    const previousXML = prevProps.diagramXML || prevState.diagramXML;

    if (currentXML && currentXML !== previousXML) {
      return this.displayDiagram(currentXML);
    }
  }

  displayDiagram(diagramXML) {
    this.dmnViewer.importXML(diagramXML);
  }

  fetchDiagram(url) {
    this.handleLoading();

    fetch(url)
      .then((response) => response.text())
      .then((text) => this.setState({ diagramXML: text }))
      .catch((err) => this.handleError(err));
  }

  handleLoading() {
    const { onLoading } = this.props;

    if (onLoading) {
      onLoading();
    }
  }

  handleError(err) {
    const { onError } = this.props;

    if (onError) {
      onError(err);
    }
  }

  handleShown(warnings) {
    const { onShown } = this.props;

    if (onShown) {
      onShown(warnings);
    }
  }

  render() {
    return <div className="react-dmn-diagram-container" ref={this.containerRef}></div>;
  }
}

export default ReactDmn;
