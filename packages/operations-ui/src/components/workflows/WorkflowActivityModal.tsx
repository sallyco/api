/* eslint-disable react/no-children-prop */
import React, { useEffect } from 'react';
import { Form } from '@packages/gbt-ui';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { Button } from '@mui/material';
import { ContentLoader, LargeDialog } from '@packages/gbt-ui';
import { useRequest, doChange, objectProps } from '@packages/wired-gbt-ui';
import { AngleDoubleRightIcon } from 'react-line-awesome';

interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  workflowInstance: {
    id: string;
    definitionId: string;
  };
}
const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  buttonSubmit: {
    float: 'right',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    backgroundColor: theme.palette.success.main,
    color: theme.palette.common.white,
  },
}));
const WorkflowActivityModal = ({ open, setOpen, workflowInstance }: Props) => {
  const classes = useStyles();
  const op = objectProps.workflows;
  const [tasks, setTasks] = React.useState<any>(undefined);

  useEffect(() => {
    const getTasks = async () => {
      const tsks = await doChange(`${op.route}/tasks`, 'POST', {
        processInstanceId: workflowInstance.id,
        processDefinitionId: workflowInstance.definitionId,
        sorting: [{ sortBy: 'created', sortOrder: 'desc' }],
        activityInstanceIdIn: [],
      });
      setTasks(tsks);
    };
    if (workflowInstance) {
      getTasks();
    }
  }, [op.route, workflowInstance]);

  const { data, isValidating, error } = useRequest(
    tasks ? `${op.route}/forms/${tasks[0].formKey}/describe` : '',
    { swrOptions: { revalidateOnFocus: false } }
  );

  const onSubmit = ({ formData }) => {
    doChange(`${op.route}/processes/${workflowInstance.id}/complete`, 'POST', formData);
    setOpen(false);
  };
  return (
    <LargeDialog title={workflowInstance?.id} open={open} setOpen={setOpen}>
      {!data ? (
        <ContentLoader errorMessage={error?.message} />
      ) : (
        <Form
          schema={data.definitions[Object.keys(data.definitions)[0]]}
          uiSchema={{
            'ui:title': ' ',
          }}
          onSubmit={onSubmit}
        >
          <Button
            variant="contained"
            type="submit"
            className={classes.buttonSubmit}
            endIcon={<AngleDoubleRightIcon />}
          >
            Proceed
          </Button>
        </Form>
      )}
    </LargeDialog>
  );
};
export default WorkflowActivityModal;
