import React, { useEffect } from 'react';
import { useRequest } from '@packages/wired-gbt-ui';
import makeStyles from '@mui/styles/makeStyles';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { PlayCircleIcon } from 'react-line-awesome';

import { FullScreenDialog } from '@packages/gbt-ui';

type ReactDmn = {
  diagramXML: string;
  onLoading: () => void;
  onShown: () => void;
  onError: (err: any) => void;
};

const BpmnViewerWithNoSSR = dynamic(() => import('./ReactBpmn'), {
  ssr: false,
});

import { Box, Button } from '@mui/material';

interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setStartOpen?: React.Dispatch<React.SetStateAction<boolean>> | undefined;
  setStartOpenDirection?: React.Dispatch<React.SetStateAction<'up' | 'down'>> | undefined;
  workflowProcess: {
    category: string; //The category of the decision definition.
    deploymentId: string; //The deployment id of the decision definition.
    description: string;
    diagram: string | 'undefined';
    historyTimeToLive: number; //History time to live value of the decision definition. Is used within History cleanup.
    id: string; //The id of the decision definition.
    key: string; //The key of the decision definition, i.e., the id of the DMN 2.0 XML decision definition.
    name: string; //The name of the decision definition.
    resource: string; //The file name of the decision definition.
    startableInTasklist: boolean;
    suspended: boolean;
    tenantId: string; //The tenant id of the decision definition.
    version: number; //The version of the decision definition that the engine assigned to it.
    versionTag: string; //The version tag of the decision or null when no version tag is set

    xml: string | 'undefined';
  };
}

const useStyles = makeStyles({
  '@global': {
    '.powered-by': {
      display: 'none !important',
    },
    '.bjs-powered-by': {
      display: 'none !important',
    },
    '.react-bpmn-diagram-container': {
      height: '100%',
    },
  },
  root: {
    maxWidth: '100%',
  },
});

const WorkflowProcessModal = ({
  open,
  setOpen,
  setStartOpenDirection = undefined,
  setStartOpen = undefined,
  workflowProcess,
}: Props) => {
  const classes = useStyles();
  const { data, isValidating } = useRequest(
    workflowProcess ? `/workflows/processes/${workflowProcess.id}/xml` : '',
    { swrOptions: { revalidateOnFocus: false } }
  );
  const [loading, setLoading] = React.useState(false);

  useEffect(() => setLoading(isValidating), [isValidating]);

  function onError(err) {
    console.error('failed to render diagram', err);
  }

  function onLoading() {
    console.log('loading diagram');
  }

  function onShown() {
    console.log('diagram shown');
  }

  function onInstanceStartClick() {
    setOpen(false);
    if (setStartOpen) {
      if (setStartOpenDirection) {
        setStartOpenDirection('down');
      }
      setStartOpen(true);
    }
  }

  return (
    <FullScreenDialog
      title={workflowProcess?.name}
      open={open}
      setOpen={setOpen}
      loading={loading}
      setLoading={setLoading}
      titleBarAction={
        setStartOpen ? (
          <Button
            variant="contained"
            color="secondary"
            startIcon={<PlayCircleIcon />}
            onClick={onInstanceStartClick}
          >
            Start Workflow
          </Button>
        ) : undefined
      }
    >
      <Head>
        <link rel="stylesheet" href="https://unpkg.com/bpmn-js@8.7.3/dist/assets/diagram-js.css" />
        <link
          rel="stylesheet"
          href="https://unpkg.com/bpmn-js@8.7.3/dist/assets/bpmn-font/css/bpmn.css"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/bpmn-js@8.7.3/dist/assets/bpmn-font/css/bpmn-embedded.css"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/bpmn-js@8.7.3/dist/assets/bpmn-font/css/bpmn-codes.css"
        />
      </Head>
      <Box my={2} mx={'auto'} height="100%" width="90%">
        {data?.bpmn20Xml && (
          <BpmnViewerWithNoSSR
            diagramXML={data.bpmn20Xml}
            onLoading={onLoading}
            onShown={onShown}
            onError={onError}
          />
        )}
      </Box>
    </FullScreenDialog>
  );
};

export default WorkflowProcessModal;
