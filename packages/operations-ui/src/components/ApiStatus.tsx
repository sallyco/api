import React from 'react';

import { InfoCard } from '@packages/gbt-ui';
import { NetworkWiredIcon } from 'react-line-awesome';

import moment from 'moment';

type Props = {
  name: string;
  apiStatus: any;
};

const ApiStatus: React.FunctionComponent<Props> = ({ name, apiStatus }) => {
  return (
    <InfoCard
      icon={<NetworkWiredIcon />}
      title={name}
      item={{
        ...(apiStatus?.buildDate
          ? {
              'Build Date':
                moment.utc(apiStatus.buildDate).local().format('lll') +
                ' (' +
                moment.utc(apiStatus.buildDate).local().fromNow() +
                ')',
            }
          : {}),
        ...(apiStatus?.buildName ? { 'Build Name': apiStatus.buildName } : {}),
        ...(apiStatus?.buildVersion ? { 'Build Version': apiStatus.buildVersion } : {}),
        ...(apiStatus?.url ? { 'Ping Endpoint': apiStatus.url } : {}),
      }}
    />
  );
};

export default ApiStatus;
