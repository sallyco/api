import React from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  MenuItem,
  TextField,
  Typography,
} from '@mui/material';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { getCountries, getStates } from 'country-state-picker';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Profile } from '../../api/profilesApi';

interface AddressFormProps {
  address: Profile['address'];
  onSuccess: (address) => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
  button: {
    background: '#1976d2',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 40,
    padding: '0 30px',
  },
  textField: {
    margin: '0px 0px 20px 0px',
  },
  divider: {
    margin: '0px 0px 20px 0px',
  },
  error: {
    color: '#f83245',
    fontFamily: 'Lato, Helvetica, Arial, sans-serif',
    fontWeight: 400,
    lineHeight: 1.66,
  },
}));

const isValidZip = (
  schema: Yup.StringSchema,
  validationErrorMessage = 'ZIP code format is incorrect'
) => schema.matches(/^\d{5}(?:[-\s]\d{4})?$/, validationErrorMessage);

const AddressForm = ({ address, onSuccess }: AddressFormProps) => {
  const classes = useStyles();

  const validationSchema = Yup.object().shape({
    address1: Yup.string().min(2).max(1024).required('Address Line 1 is Required'),
    address2: Yup.string().max(1024),
    city: Yup.string().min(2).max(1024).required('City is Required'),
    state: Yup.string().min(2).max(1024).required('State is Required'),
    postalCode: Yup.string()
      .min(2)
      .max(1024)
      .required('Postal Code is Required')
      .when('country', (country, schema) => {
        if (country === 'United States of America') {
          return isValidZip(schema);
        }
        return schema;
      }),
    country: Yup.string().min(2).max(1024).required('Country is Required'),
  });

  const initialValues = { ...address };

  const onSubmit = (data) => {
    onSuccess(data);
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: false,
    validateOnBlur: true,
    onSubmit,
  });

  const isTouchedAndHaveError = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        )
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey])))
      );
    }
  };

  const getErrorText = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        ) &&
        (!subKey ? formik.errors[key][index] : formik.errors[key][index][subKey])
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey]))) &&
        (!subKey ? formik.errors[key] : formik.errors[key][subKey])
      );
    }
  };

  return (
    <>
      <Card className={classes.root}>
        <CardHeader title={<Typography variant="h4">Change Address</Typography>} />
        <Divider />
        <CardContent>
          <form
            onSubmit={formik.handleSubmit}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          >
            <TextField
              label="Address 1"
              id="address1"
              name="address1"
              type="text"
              fullWidth
              className={classes.textField}
              value={formik.values.address1}
              error={isTouchedAndHaveError('address1')}
              helperText={getErrorText('address1')}
            />
            <TextField
              label="Address 2"
              id="address2"
              name="address2"
              type="text"
              fullWidth
              className={classes.textField}
              value={formik.values.address2}
              error={isTouchedAndHaveError('address2')}
              helperText={getErrorText('address2')}
            />
            <Box pt={1}>
              <div>
                <Grid container justifyContent="space-between" wrap="nowrap">
                  <TextField
                    label="City"
                    id="city"
                    name="city"
                    type="text"
                    fullWidth
                    className={classes.textField}
                    value={formik.values.city}
                    error={isTouchedAndHaveError('city')}
                    helperText={getErrorText('city')}
                    InputLabelProps={{ shrink: true }}
                  />
                  <TextField
                    label="State"
                    id="state"
                    name="state"
                    type="text"
                    select
                    fullWidth
                    className={classes.textField}
                    value={formik.values.state}
                    error={isTouchedAndHaveError('state')}
                    helperText={getErrorText('state')}
                    onChange={formik.handleChange}
                  >
                    {(formik.values.country
                      ? getStates(
                          getCountries().find((item) => {
                            return item.name === formik.values.country;
                          })?.code
                        )
                      : []
                    ).map((item) => {
                      return (
                        <MenuItem key={item} value={item}>
                          {item}
                        </MenuItem>
                      );
                    })}
                  </TextField>
                </Grid>
              </div>
            </Box>
            <Box pt={1}>
              <div>
                <Grid container justifyContent="space-between" wrap="nowrap">
                  <TextField
                    label="Postal Code"
                    id="postalCode"
                    name="postalCode"
                    type="text"
                    fullWidth
                    className={classes.textField}
                    value={formik.values.postalCode}
                    error={isTouchedAndHaveError('postalCode')}
                    helperText={getErrorText('postalCode')}
                  />
                  <TextField
                    label="Country"
                    id="country"
                    name="country"
                    type="text"
                    select
                    fullWidth
                    className={classes.textField}
                    value={formik.values.country}
                    error={isTouchedAndHaveError('country')}
                    helperText={getErrorText('country')}
                    onChange={formik.handleChange}
                  >
                    {getCountries().map((item) => {
                      return (
                        <MenuItem key={item.code} value={item.name}>
                          {item.name}
                        </MenuItem>
                      );
                    })}
                  </TextField>
                </Grid>
              </div>
            </Box>
            <Button
              color="primary"
              fullWidth
              type="submit"
              disabled={!formik.isValid}
              data-testid="address-change-submit"
            >
              Update Address
            </Button>
          </form>
        </CardContent>
      </Card>
    </>
  );
};

export default AddressForm;
