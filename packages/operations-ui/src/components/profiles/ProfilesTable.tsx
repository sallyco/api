import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import makeStyles from '@mui/styles/makeStyles';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  profiles: any[];
}

const useStyles = makeStyles({
  '@global': {
    '.MuiDataGrid-root': {
      border: 0,
    },
  },
  root: {},
});

const ProfilesTable = ({ profiles }: Props) => {
  const classes = useStyles();
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
      options: {
        filter: false,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
    },
    {
      name: 'profileType',
      label: 'Type',
    },
    {
      name: 'email',
      label: 'E-mail',
      options: {
        filter: false,
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    name: 'string',
    ownerId: 'string',
    tenantId: 'string',
    profileType: 'string',
    stateOfFormation: 'string',
    countryOfFormation: 'string',
    legalName: 'string',
    typeOfEntity: 'string',
    firstName: 'string',
    lastName: 'string',
    phone: 'string',
    dateOfBirth: 'date',
    dateOfFormation: 'string',
    isUSPerson: 'boolean',
    isSingleMemberLLC: 'boolean',
    createdAt: 'date',
    updatedAt: 'date',
    email: 'string',
  };

  const tableService = new TableWithToolsService(
    'profiles-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={profiles}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`profiles/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default ProfilesTable;
