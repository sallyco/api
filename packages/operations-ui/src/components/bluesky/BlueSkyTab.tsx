import React from 'react';
import { useRequest } from '@packages/wired-gbt-ui';

import { ContentLoader } from '@packages/gbt-ui';
import BlueSkyTable from './BlueSkyTable';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const BlueSkyTab = () => {
  const { data, error } = useRequest('/blue-sky-fees?filter[order][]=state ASC');

  return !data ? (
    <ContentLoader />
  ) : (
    <React.Fragment>
      <BlueSkyTable entries={data} />
    </React.Fragment>
  );
};

export default BlueSkyTab;
