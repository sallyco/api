import React, { useEffect } from 'react';

import { Form } from '@packages/gbt-ui';
import { SnackbarUtils, metadataFetcher } from '@packages/wired-gbt-ui';
import { FullScreenDialog } from '@packages/gbt-ui';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import useSWR, { mutate } from 'swr';

import { Container } from '@mui/material';

interface Props {
  open: any;
  setOpen: any;
  id: number | undefined;
}

export default function BlueSkyModal({ open, setOpen, id }: Props) {
  const metadataUrl = baseUrl + '/metadata/blue-sky-fee/update/describe';
  const url = baseUrl + `/blue-sky-fees/${id}?filter[include][]=blueSkyFeeTiers`;
  const { data, error } = useSWR(metadataUrl, metadataFetcher);
  const { data: fd, error: err } = useSWR(id ? url : null);
  const [loading, setLoading] = React.useState(false);

  const [formData, setFormData] = React.useState(null);
  useEffect(() => setFormData(fd), [fd]);

  const onSubmit = ({ formData }) => {
    setLoading(true);
    fetch(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formData),
    }).then((r) => {
      setLoading(false);
      if (!r.ok) {
        SnackbarUtils.error('err');
        throw r;
      }
      setOpen(false);
      mutate(baseUrl + '/blue-sky-fees?filter[order][]=state ASC');
    });
  };

  return (
    <FullScreenDialog
      title={formData?.state}
      open={open}
      setOpen={setOpen}
      loading={loading}
      setLoading={setLoading}
    >
      <Container>
        {data && (
          <Form
            schema={data}
            onSubmit={onSubmit}
            showErrorList={false}
            omitExtraData={true}
            formData={formData}
            onChange={(e) => setFormData(e.formData)}
          />
        )}
      </Container>
    </FullScreenDialog>
  );
}
