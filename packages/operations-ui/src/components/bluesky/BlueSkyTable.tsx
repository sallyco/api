import React from 'react';
import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import BlueSkyModal from './BlueSkyModal';
import numeral from 'numeral';

interface Props {
  entries: any[];
}

const BlueSkyTable = ({ entries }: Props) => {
  const router = useRouter();
  const [open, setOpen] = React.useState(false);
  const [id, setId] = React.useState<number | undefined>(undefined);

  return (
    <React.Fragment>
      <TableWithTools
        data={entries}
        columnDefinitions={[
          {
            name: 'id',
            options: {
              display: false,
              filter: false,
            },
          },
          {
            name: 'state',
            label: 'State',
          },
          {
            name: 'type',
            label: 'Type Fee',
          },
          {
            name: 'paymentMethod',
            label: 'Method',
          },
          {
            name: 'fee',
            label: 'Amount',
            options: {
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) =>
                tableMeta.rowData[2] !== 'Fixed' ? 'N/A' : numeral(value).format('$0,0'),
            },
          },
        ]}
        onRowClick={(r, m) => {
          setId(r[0]);
          setOpen(true);
        }}
        elevation={0}
        selectable={false}
        clickable
        columns={false}
      />
      <BlueSkyModal open={open} setOpen={setOpen} id={id} />
    </React.Fragment>
  );
};

export default BlueSkyTable;
