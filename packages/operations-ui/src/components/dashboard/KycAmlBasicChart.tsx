import React from 'react';
import { Card, CardHeader, CardContent } from '@mui/material';
import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import moment from 'moment';
import numeral from 'numeral';
import { ApexOptions } from 'apexcharts';
import useSWR from 'swr';

const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  gridItemProgressValue: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },
  gridItemProgressBar: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },
  gridItemProgressBarValue: {
    paddingLeft: '1rem',
  },
}));

const KycAmlBasicChart = () => {
  const classes = useStyles();

  const offset = new Date().getTimezoneOffset();
  const lastTime = moment().subtract(1, 'week');

  const kycAmlChartUrl = `${baseUrl}/kyc-aml/chart/week/${offset}`;
  const { data: kycAmlChartData } = useSWR(() => `${kycAmlChartUrl}`, fetcher);

  const chartOptions: ApexOptions = {
    chart: {
      id: 'basic',
      toolbar: {
        show: false,
      },
      sparkline: {
        enabled: false,
      },
      zoom: {
        autoScaleYaxis: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    grid: {
      strokeDashArray: 2,
      borderColor: 'rgba(125, 138, 156, 0.3)',
    },
    stroke: {
      width: 3,
      curve: 'smooth',
      colors: ['#33cc33', '#0080ff', '#ff0000', '#ffbf00'],
    },
    fill: {
      type: 'gradient',
      opacity: 0.1,
      gradient: {
        shade: 'dark',
        type: 'vertical',
        shadeIntensity: 0.1,
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 0.5,
        stops: [0, 50, 100],
      },
    },
    plotOptions: {
      bar: {
        columnWidth: '20%',
      },
    },
    tooltip: {
      x: {
        format: 'dd MMM yyyy',
      },
    },
    colors: ['#33cc33', '#0080ff', '#ff0000', '#ffbf00'],
    legend: {
      show: true,
    },
    xaxis: {
      type: 'datetime',
      min: lastTime.valueOf(),
      tickAmount: 6,
      labels: {
        datetimeFormatter: {
          year: 'yyyy',
          month: "MMM 'yy",
          day: 'dd MMM',
          hour: 'HH:mm',
        },
      },
    },
    yaxis: {
      min: 0,
      labels: {
        formatter: function (val, index) {
          return numeral(val).format('0,0');
        },
      },
    },
  };

  const chartsLarge3Data = kycAmlChartData;

  return (
    <>
      {kycAmlChartData && (
        <Card className={classes.root}>
          <CardHeader
            classes={{ action: classes.current }}
            subheader="Recent KYC/AML checks activity (Last week)"
            subheaderTypographyProps={{
              color: 'textSecondary',
              variant: 'caption',
            }}
            title="Activity Summary"
            titleTypographyProps={{ color: 'textPrimary' }}
          />
          <CardContent className={classes.content}>
            <Chart options={chartOptions} series={chartsLarge3Data} type={'line'} height={317} />
          </CardContent>
        </Card>
      )}
    </>
  );
};

export default KycAmlBasicChart;
