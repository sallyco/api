import React from 'react';
import { useRouter } from 'next/router';
import { Box, Grid } from '@mui/material';
import { FilesDropzone } from '@packages/gbt-ui';
import { SnackbarUtils, useRequest, doChange } from '@packages/wired-gbt-ui';

import FilesTable from '../files/FilesTable';
import { Company } from '../../api/companiesApi';

interface Props {
  company: Company;
}

const CompanyDocumentsTable = ({ company }: Props) => {
  const router = useRouter();
  const { data: files, mutate: mutateFiles } = useRequest(`/companies/${company.id}/files`);

  const handleUpload = async (files) => {
    const file = files[0];
    // eslint-disable-next-line no-control-regex
    if (/[^\u0000-\u00ff]/.test(file.name)) {
      SnackbarUtils.error(`File name should not contain Unicode characters. '${file.name}' `);
      return;
    }
    const data = new FormData();
    data.append('file', file);
    const uploadedFiles: any = await doChange(
      `/files/upload`,
      'POST',
      data,
      undefined,
      undefined,
      { 'x-file-upload-tenant-id': company.tenantId },
      true
    );
    if (!uploadedFiles?.length) {
      return;
    }
    const uploadedFileIds = uploadedFiles.map((file) => file.id);
    await updateCompanyWithNewFiles(uploadedFileIds);
  };

  const updateCompanyWithNewFiles = async (fileIds) => {
    const companyToUpdate = { ...company };
    const updatedFileIds = companyToUpdate.files ? [...companyToUpdate.files, ...fileIds] : fileIds;
    await doChange(`/companies/${company.id}`, 'PUT', {
      ...companyToUpdate,
      files: updatedFileIds,
    });
    router.reload();
  };

  return (
    <Grid container spacing={2}>
      <Grid item sm={8} xl={9} xs={12}>
        <FilesTable files={files?.data ? [...files.data] : []} />
      </Grid>
      <Grid item sm={4} xl={3} xs={12}>
        <Box pb={2}>
          <FilesDropzone handleUpload={handleUpload} />
        </Box>
      </Grid>
    </Grid>
  );
};

export default CompanyDocumentsTable;
