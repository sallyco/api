import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import { Company } from '../../api/companiesApi';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  companies: Company[];
  type: string;
  isValidating: boolean;
}

const CompaniesTable = ({ companies, type, isValidating }: Props) => {
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
      options: {
        filter: false,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant Id',
    },
    {
      name: 'stateOfFormation',
      label: 'State of Formation',
    },
  ];

  const fieldMap: any = {
    id: 'string',
    ownerId: 'string',
    dealId: 'string',
    profileId: 'string',
    companyId: 'string',
    name: 'string',
    email: 'string',
    status: 'string',
    isDocsSigned: 'boolean',
    isKycAmlPassed: 'boolean',
    amount: 'number',
    createdAt: 'date',
    updatedAt: 'date',
  };

  const tableService = new TableWithToolsService(
    'companies-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={companies}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`${type}/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
      showLoading={true}
      isLoading={isValidating}
    />
  );
};

export default CompaniesTable;
