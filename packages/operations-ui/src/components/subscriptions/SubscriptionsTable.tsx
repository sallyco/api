import React from 'react';
import { useRouter } from 'next/router';
import { InvestmentsTable } from '@packages/gbt-ui';
import {
  SubscriptionStatusColors,
  SubscriptionStatusText,
  getStatusOrder,
  isQPSubscriberProfile,
} from '../../services/SubscriptionService';
import { getColumnDefinition } from '../../tools/columnDefinitionGenerator';

interface Props {
  subscriptions: any[];
  showEnitity?: true;
}

const SubscriptionsTable = ({ subscriptions, showEnitity }: Props) => {
  const router = useRouter();

  const fieldMap: any = {
    ownerId: 'string',
    dealId: 'string',
    profileId: 'string',
    isAmountMatched: 'boolean',
    isDocsSigned: 'boolean',
    isKycAmlPassed: 'boolean',
    createdAt: 'date',
    updatedAt: 'date',
  };

  const columnDefinitions = getColumnDefinition(fieldMap, []);

  const columnIndexMapping = new Map();
  columnIndexMapping.set('id', 0);
  columnIndexMapping.set('profile', 5);

  const hiddenColumns = ['#', 'entityName', 'actions', 'signingStatus', 'note'];

  return (
    <>
      {subscriptions && (
        <InvestmentsTable
          data={subscriptions}
          columnIndexMapping={columnIndexMapping}
          onRowClick={(r, m) => {
            router.push(`/subscriptions/${r[0]}`);
          }}
          elevation={0}
          selectable={false}
          clickable
          hiddenColumns={hiddenColumns}
          statusFunctions={{
            statusText: SubscriptionStatusText,
            statusColor: SubscriptionStatusColors,
            statusOrder: getStatusOrder,
          }}
          checkQualifiedPurchaser={isQPSubscriberProfile}
          additionalColumnDefinitions={columnDefinitions}
        />
      )}
    </>
  );
};

export default SubscriptionsTable;
