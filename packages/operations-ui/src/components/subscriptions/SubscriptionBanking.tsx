import { Subscription } from '../../api/subscriptionsApi';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import {
  Card,
  CardContent,
  CardHeader,
  IconButton,
  List,
  ListItem,
  Typography,
} from '@mui/material';
import { PencilAltIcon } from 'react-line-awesome';
import React from 'react';
import { toCapitalizedWords } from '../../tools/textFormatters';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
}));

interface SubscriptionBankingProps {
  subscription: Subscription;
}

const SubscriptionBanking = ({ subscription }: SubscriptionBankingProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        className={classes.header}
        title={
          <Typography display="block" variant="overline" color="textSecondary">
            Bank Account
          </Typography>
        }
        action={
          <IconButton size="large">
            <PencilAltIcon />
          </IconButton>
        }
      />
      <CardContent className={classes.content}>
        <List>
          {subscription?.bankAccount && (
            <>
              {Object.keys(subscription.bankAccount).map(
                (key) =>
                  typeof subscription.bankAccount[key] !== 'object' && (
                    <ListItem key={key} className={classes.listItem} disableGutters divider>
                      <Typography variant="subtitle2" color="textPrimary">
                        {toCapitalizedWords(key)}
                      </Typography>
                      <Typography variant="h6" color="textSecondary">
                        {subscription.bankAccount[key]}
                      </Typography>
                    </ListItem>
                  )
              )}
            </>
          )}

          {!subscription?.bankAccount && (
            <ListItem>No Banking attached to this subscription</ListItem>
          )}
        </List>
      </CardContent>
    </Card>
  );
};
export default SubscriptionBanking;
