import useSWR from 'swr';
import TransactionsTable from './TransactionsTable';

// Data imports
const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

function TransactionsTab() {
  const filter = {
    where: {
      bankAccountTransactionId: {
        exists: true,
      },
    },
  };
  const encodeFilter = encodeURIComponent(JSON.stringify(filter));
  const transactionsUrl = baseUrl + '/transactions?filter=' + encodeFilter;
  const { data: transactionsValue } = useSWR(transactionsUrl, fetcher);
  return <TransactionsTable title={' '} entries={transactionsValue} />;
}

export default TransactionsTab;
