import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

function TransactionsTable({ title, entries }) {
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      label: 'ID',
      options: {
        filter: false,
        display: false,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
      options: {
        filter: true,
      },
    },
    {
      name: 'OrganizerId',
      label: 'Organizer',
      options: {
        filter: false,
      },
    },
    {
      name: 'dealId',
      label: 'Deal',
      options: {
        filter: true,
      },
    },
    {
      name: 'investorId',
      label: 'Investor',
      options: {
        filter: true,
      },
    },
    {
      name: 'amount',
      label: 'Amount',
      options: {
        filter: true,
      },
    },
    {
      name: 'direction',
      label: 'Direction',
      options: {
        filter: false,
      },
    },
    {
      name: 'status',
      label: 'Status',
      options: {
        filter: true,
      },
    },
    {
      name: 'type',
      label: 'Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'reason',
      label: 'Reason',
      options: {
        filter: false,
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    accountID: 'string',
    amount: 'number',
    counterpartyId: 'string',
    dealId: 'string',
    direction: 'string',
    status: 'string',
    subscriptionId: 'string',
    tenantId: 'string',
    transactionId: 'string',
    type: 'string',
    createdAt: 'date',
    addedToNetwork: 'date',
    updatedAt: 'date',
  };

  const tableService = new TableWithToolsService(
    'transactions-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      title={title}
      selectable={false}
      data={entries}
      clickable
      onRowClick={(r, m) => {
        router.push(`bank-reconciliation/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      columnDefinitions={columnDefinitions}
    />
  );
}

export default TransactionsTable;
