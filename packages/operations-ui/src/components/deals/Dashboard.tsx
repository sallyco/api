import React, { useState } from 'react';
import useSWR from 'swr';
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Container,
  Paper,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Divider,
} from '@mui/material';
import numeral from 'numeral';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { CardHeaderAction, getStartDate } from '../common/CardHeaderAction';
import CategoryChart, { ChartTypeGrid, ChartTypes } from '../common/CategoryChart';
import DoubleTextChip from '../common/DoubleTextChip';
// Data imports
const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  gridItemProgressValue: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },
  gridItemProgressBar: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },
  gridItemProgressBarValue: {
    paddingLeft: '1rem',
  },
}));

const StatusClips = ({ classes, title, count, amount }) => (
  <TableRow key={title}>
    <TableCell component="th" scope="row">
      {title}
    </TableCell>
    <TableCell align="right">
      <DoubleTextChip
        text1={numeral(count).format('0,0')}
        text2={numeral(amount).format('$ 0,0')}
        gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
        size={'small'}
        tooltip
      />
    </TableCell>
  </TableRow>
);

const Status = ({ classes, status }) => {
  if (typeof status !== 'object' || !status) return <></>;
  const statusArray = ['OPEN', 'IN CLOSING', 'CLOSED', 'DRAFT'];
  return (
    <div className={classes.gridItem}>
      <span className={classes.gridItemLabel}>Deals Count and Amount by Status</span>
      <Divider />
      <TableContainer component={Paper}>
        <Table className={classes.table} size="small">
          <TableBody>
            {statusArray.map((key) => (
              <StatusClips
                key={key}
                {...{
                  classes,
                  title: key,
                  count: status[key]?.count,
                  amount: status[key]?.amount,
                }}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};
export default function Dashboard() {
  const classes = useStyles();
  const [timeBox, setTimeBox] = useState<{
    startDate: number;
    endDate: number;
    text?: string;
  }>({
    startDate: getStartDate('ytd').getTime(),
    endDate: new Date().getTime(),
    text: 'YTD',
  });
  const dealsUrl = `${baseUrl}/deals/charts`;
  const { data: dealsValue } = useSWR(
    () => `${dealsUrl}?startDate=${timeBox.startDate}&endDate=${timeBox.endDate}`,
    fetcher
  );
  const [chartType, setChartType] = useState<ChartTypes>('line');
  return (
    <Container>
      <Card className={classes.root}>
        <CardHeader
          action={<CardHeaderAction {...{ classes, timeBox, setTimeBox }} />}
          classes={{ action: classes.current }}
          subheader="Recent Deals Activities"
          subheaderTypographyProps={{
            color: 'textSecondary',
            variant: 'caption',
          }}
          title="Activity Summary"
          titleTypographyProps={{ color: 'textPrimary' }}
        />
        <CardContent className={classes.content}>
          <Grid container spacing={6}>
            <Grid item sm={6} md={6}>
              <Status {...{ classes, status: dealsValue?.status }} />
            </Grid>
            <Grid item sm={6} md={6}>
              <ChartTypeGrid {...{ classes, chartType, setChartType, timeBox }} />
            </Grid>
          </Grid>
          <CategoryChart data={dealsValue} chartType={chartType} />
        </CardContent>
      </Card>
    </Container>
  );
}
