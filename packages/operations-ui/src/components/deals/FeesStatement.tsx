import React from 'react';
import { StatementTable } from '@packages/gbt-ui';
import { useRequest, doChange } from '@packages/wired-gbt-ui';

export default function FeesStatement({ dealId }) {
  const { data, error, isValidating, mutate } = useRequest(`/deals/${dealId}/fees-statement`);

  const editHandler = (r, m) => {
    const payload = {
      amount: Number(r),
      ...m,
    };
    const url = `/deals/${dealId}/fees-statement/override`;
    doChange(url, 'POST', payload, mutate);
  };

  return <StatementTable statementData={data} editHandler={editHandler} loading={isValidating} />;
}
