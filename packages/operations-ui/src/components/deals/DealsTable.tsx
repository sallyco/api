import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import { Chip } from '@mui/material';
import tableDateFilter from '../common/TableDatesFilter';
import { DealStatusColors } from 'src/tools';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';
interface Props {
  deals: any[];
  isValidating: boolean;
}
const DealsTable = ({ deals, isValidating }: Props) => {
  const router = useRouter();
  const [dateRange, setDateRange] = useState({
    startDate: 0,
    endDate: new Date().getTime(),
  });
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Deal Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'profile',
      label: 'Organizer',
      options: {
        customBodyRender: (
          value: { firstName: string; lastName: string },
          _tableMeta: any,
          _updateValue: any
        ) => value?.firstName + ' ' + value?.lastName ?? `---`,
        filter: true,
      },
    },
    {
      name: 'entity',
      label: 'Entity Name',
      options: {
        filter: true,
        customBodyRender: (value: { name: string }, _tableMeta: any, _updateValue: any) =>
          value?.name,
      },
    },
    {
      name: 'status',
      label: 'Status',
      options: {
        filter: true,
        customFilterListOptions: {
          render: (v) => v.toLowerCase(),
        },
        // eslint-disable-next-line react/display-name
        customBodyRender: (status: string, _tableMeta: any, _updateValue: any) => (
          <Chip
            label={status}
            style={{
              backgroundColor: DealStatusColors(status),
              opacity: '80%',
            }}
            size="small"
          />
        ),
      },
    },
    {
      name: 'estimatedCloseDate',
      label: 'Close Date',
      options: {
        // eslint-disable-next-line react/display-name
        customBodyRender: (status: string, _tableMeta: any, _updateValue: any) => (
          <span>{new Date(status).toDateString()}</span>
        ),
        filter: true,
        filterType: 'custom',
        filterOptions: {
          ...tableDateFilter(dateRange, setDateRange),
        },
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
      options: {
        filter: true,
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    ownerId: 'string',
    tenantId: 'string',
    entityId: 'string',
    profileId: 'string',
    name: 'string',
    description: 'string',
    assetUrl: 'string',
    marketingLogo: 'string',
    marketingSlogan: 'string',
    portfolioCompanyName: 'string',
    portfolioCompanyState: 'string',
    portfolioCompanyEntity: 'string',
    companyMarketSector: 'string',
    raiseAmount: 'number',
    previouslyRaisedAmount: 'number',
    securityType: 'string',
    estimatedCloseDate: 'string',
    minInvestmentAmount: 'number',
    carryPercentage: 'number',
    status: 'string',
    createdAt: 'date',
    updatedAt: 'date',
  };

  const tableService = new TableWithToolsService(
    'deals-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  if (!deals) return <></>;

  return (
    <TableWithTools
      data={deals}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, _m) => {
        router.push(`deals/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
      showLoading={true}
      isLoading={isValidating}
    />
  );
};

export default DealsTable;
