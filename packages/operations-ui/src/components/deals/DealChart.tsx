import React, { useEffect } from 'react';
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Chip,
  Avatar,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from '@mui/material';
import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import _ from 'lodash';
import numeral from 'numeral';
import { FundDocument } from '../../api/subscriptionsApi';
import { ApexOptions } from 'apexcharts';

interface Props {
  fundDocuments: FundDocument[];
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  table: {
    minWidth: 200,
  },
}));

const DealChart = ({ fundDocuments }: Props) => {
  const classes = useStyles();
  const [chartData, setchartData] = React.useState<any>(undefined);
  const totalFundReceived = _.sumBy(fundDocuments, (entry) => entry.subscriptionAmount);
  const _chartData = [];

  useEffect(() => {
    if (fundDocuments) {
      const groups = _.groupBy(fundDocuments, 'tenantId');
      for (const [key, value] of Object.entries(groups)) {
        const _data = value;
        let _total = 0;
        let _targetRaiseAmount = 0;
        const item = {};
        for (const [, value1] of Object.entries(_data)) {
          _total += value1.subscriptionAmount;
          _targetRaiseAmount += value1.targetRaiseAmount;
          item['x'] = key;
        }
        const goals = [
          {
            name: 'Target RaiseAmount',
            value: _targetRaiseAmount,
            strokeWidth: 5,
            strokeColor: 'red',
          },
        ];
        item['y'] = _total;
        item['goals'] = goals;
        _chartData.push(item);
      }
      setchartData(_chartData);
    }
  }, [fundDocuments]);

  const options: ApexOptions = {
    series: [
      {
        name: 'Actual Fund Received',
        data: chartData,
      },
    ],
    chart: {
      height: 350,
      type: 'bar',
    },
    xaxis: {
      title: {
        text: 'Tenants ',
        style: {
          color: '#008FFB',
        },
      },
    },
    yaxis: [
      {
        axisTicks: {
          show: true,
        },
        axisBorder: {
          show: true,
          color: '#s',
        },
        labels: {
          style: {
            colors: '#008FFB',
          },
        },
        title: {
          text: 'Fund Received ($)',
          style: {
            color: '#008FFB',
          },
        },
        tooltip: {
          enabled: true,
        },
      },
    ],
    plotOptions: {
      bar: {
        columnWidth: '30%',
      },
    },
    colors: ['#00E396'],
    dataLabels: {
      enabled: true,
    },
    legend: {
      show: true,
      showForSingleSeries: true,
      customLegendItems: ['Actual Fund Received', 'Target RaiseAmount'],
      markers: {
        fillColors: ['#00E396', 'red'],
      },
    },
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        classes={{ action: classes.current }}
        subheader="Deal Fund Received"
        subheaderTypographyProps={{ color: 'textSecondary', variant: 'caption' }}
        title="Summary"
        titleTypographyProps={{ color: 'textPrimary' }}
      />
      <CardContent className={classes.content}>
        <Grid container spacing={6}>
          <Grid item sm={6} md={7}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}></span>
              <TableContainer component={Paper}>
                <Table className={classes.table} size="small">
                  <TableBody>
                    <TableRow key="ACH">
                      <TableCell component="th" scope="row">
                        Total Funds Received
                      </TableCell>
                      <TableCell align="right">
                        <Chip
                          size="small"
                          avatar={<Avatar>{totalFundReceived}</Avatar>}
                          label={numeral(totalFundReceived).format('$ 0,0')}
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Grid>
        </Grid>
        <Chart options={options} series={options.series} type="bar" height={390} />
      </CardContent>
    </Card>
  );
};

export default DealChart;
