import { NextPage } from 'next';
import { Box } from '@mui/material';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import DealsTable from '../../components/deals/DealsTable';

const Deals: NextPage = () => {
  const op = objectProps.deals;
  const { data: deals, isValidating } = useRequest(`${op.route}`);

  return (
    <Box mt={2}>
      <DealsTable deals={deals?.data} isValidating={isValidating} />
    </Box>
  );
};

export default Deals;
