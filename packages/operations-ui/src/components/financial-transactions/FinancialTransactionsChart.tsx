import React from 'react';
import { Transaction } from '../../api/transactionsApi';
import {
  Grid,
  LinearProgress,
  Card,
  CardHeader,
  CardContent,
  Button,
  Menu,
  MenuItem,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Paper,
  Typography,
} from '@mui/material';
import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { ChevronDownIcon } from 'react-line-awesome';
import _ from 'lodash';
import moment, { unitOfTime } from 'moment';
import numeral from 'numeral';
import { ApexOptions } from 'apexcharts';
import { ModelTransformMode } from 'uniforms';
import { AutoForm, DateField, SubmitField } from 'uniforms-unstyled';
import FadeInModal from '../common/FadeInModal';
import { Bridge } from '../../forms';
import DoubleTextChip from '../common/DoubleTextChip';
import ChartTypeSelection from '../common/ChartTypeSelection';

interface Props {
  transactions: Transaction[];
}

interface IntervalSelection {
  value: moment.unitOfTime.DurationConstructor;
  startOf?: unitOfTime.StartOf;
  label?: string;
}

interface DateSelection {
  startDate?: moment.Moment;
  endDate?: moment.Moment;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  gridItemProgressValue: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },
  gridItemProgressBar: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },
  gridItemProgressBarValue: {
    paddingLeft: '1rem',
  },
  table: {
    minWidth: 200,
  },
}));

const achReserveLimit = 7000000;

const FinancialTransactionsChart = ({ transactions }: Props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [interval, setInterval] = React.useState<IntervalSelection>({
    value: 'year',
    startOf: 'year',
    label: 'ytd',
  });

  const [selectingDates, setSelectingDates] = React.useState(false);
  const [dateSelection, setDateSelection] = React.useState<DateSelection>({});

  const [chartType, setChartType] = React.useState<
    | 'line'
    | 'area'
    | 'bar'
    | 'histogram'
    | 'pie'
    | 'donut'
    | 'radialBar'
    | 'scatter'
    | 'bubble'
    | 'heatmap'
    | 'treemap'
    | 'boxPlot'
    | 'candlestick'
    | 'radar'
    | 'polarArea'
    | 'rangeBar'
  >('line');

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (intervalSelection: IntervalSelection) => {
    if (intervalSelection.label === 'dates') {
      setSelectingDates(true);
      setAnchorEl(null);
    } else {
      setInterval(intervalSelection);
      setAnchorEl(null);
    }
  };

  const lastTime = (): DateSelection => {
    const newTime = moment().subtract(1, interval.value);
    if (interval.startOf === 'year') {
      return { startDate: moment([moment().year()]) };
    } else if (interval.startOf === 'quarter') {
      return { startDate: moment().startOf('quarter') };
    } else if (interval.startOf === 'date') {
      return {
        startDate: dateSelection.startDate,
        endDate: dateSelection.endDate,
      };
    }
    return { startDate: newTime };
  };

  const recentTrans = _.filter(transactions, (entry) =>
    moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
      .local()
      .isBetween(
        lastTime().startDate,
        moment(lastTime().endDate).add(1, 'day').startOf('day'),
        undefined,
        '[]'
      )
  );

  const totalAmountRecent = _.sumBy(recentTrans, (entry) => entry.amount);
  const totalCountRecent = _.sumBy(recentTrans, (entry) => 1);

  const achTransactions = _.filter(transactions, (entry) => {
    return (
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) &&
      entry.type === 'ACH' &&
      !['RETURNED', 'TRANSACTION_EXCEPTION', 'TRANSACTION EXCEPTION', 'ERROR'].includes(
        entry.status
      )
    );
  });

  const wireTransactions = _.filter(transactions, (entry) => {
    return (
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) &&
      entry.type === 'WIRE' &&
      !['RETURNED', 'TRANSACTION_EXCEPTION', 'TRANSACTION EXCEPTION', 'ERROR'].includes(
        entry.status
      )
    );
  });

  const returnedTransactions = _.filter(transactions, (entry) => {
    return (
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) && entry.status === 'RETURNED'
    );
  });

  const failedTransactions = _.filter(transactions, (entry) => {
    return (
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) &&
      (entry.status === 'TRANSACTION_EXCEPTION' ||
        entry.status === 'TRANSACTION EXCEPTION' ||
        entry.status === 'ERROR')
    );
  });

  const transactionCounts = {
    ach: _.sumBy(achTransactions, (entry) => 1),
    wire: _.sumBy(wireTransactions, (entry) => 1),
    returned: _.sumBy(returnedTransactions, (entry) => 1),
    failed: _.sumBy(failedTransactions, (entry) => 1),
  };

  const transactionAmounts = {
    ach: _.sumBy(achTransactions, (entry) => entry.amount),
    wire: _.sumBy(wireTransactions, (entry) => entry.amount),
    returned: _.sumBy(returnedTransactions, (entry) => entry.amount),
    failed: _.sumBy(failedTransactions, (entry) => entry.amount),
  };

  const daysDiff = moment(lastTime().endDate).diff(lastTime().startDate, 'days');
  const capacityUsage = (transactionAmounts.ach ?? 0) / (achReserveLimit * daysDiff);

  const recentDataSeries = _(recentTrans)
    .groupBy((entry) =>
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, 'amount')])
    .value();

  const achDataSeries = _(achTransactions)
    .groupBy((entry) =>
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, (value) => 1)])
    .value();

  const wireDataSeries = _(wireTransactions)
    .groupBy((entry) =>
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, (value) => 1)])
    .value();

  const failedDataSeries = _(failedTransactions)
    .groupBy((entry) =>
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, (value) => 1)])
    .value();

  const returnedDataSeries = _(returnedTransactions)
    .groupBy((entry) =>
      moment(entry.createdAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, (value) => 1)])
    .value();

  const chartOptions = (chartType = 'recent'): ApexOptions => {
    return {
      chart: {
        id: 'basic-' + chartType,
        toolbar: {
          show: false,
        },
        sparkline: {
          enabled: false,
        },
        zoom: {
          autoScaleYaxis: true,
        },
      },
      dataLabels: {
        enabled: false,
      },
      grid: {
        strokeDashArray: 2,
        borderColor: 'rgba(125, 138, 156, 0.3)',
      },
      stroke: {
        width: 3,
        curve: 'smooth',
        colors:
          chartType === 'recent'
            ? ['#3c44b1']
            : [
                '#3DDD2D',
                '#247BA0',
                '#FF1654',
                '#DFFF00',
                '#9FE2BF',
                '#40E0D0',
                '#6495ED',
                '#008080',
              ],
      },
      fill: {
        type: 'gradient',
        opacity: 0.1,
        gradient: {
          shade: 'dark',
          type: 'vertical',
          shadeIntensity: 0.1,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 0.5,
          stops: [0, 50, 100],
        },
      },
      plotOptions: {
        bar: {
          columnWidth: '20%',
        },
      },
      tooltip: {
        x: {
          format: interval.value == 'day' ? 'dd MMM yyyy : HH:mm' : 'dd MMM yyyy',
        },
      },
      colors:
        chartType === 'recent'
          ? ['#3c44b1']
          : [
              '#3DDD2D',
              '#247BA0',
              '#FF1654',
              '#DFFF00',
              '#9FE2BF',
              '#40E0D0',
              '#6495ED',
              '#008080',
            ],
      legend: {
        show: chartType === 'recent' ? false : true,
      },
      xaxis: {
        type: 'datetime',
        min: lastTime().startDate.valueOf(),
        max: (lastTime().endDate ?? moment()).valueOf(),
        labels: {
          datetimeFormatter: {
            year: 'yyyy',
            month: "MMM 'yy",
            day: 'dd MMM',
            hour: 'HH:mm',
          },
          datetimeUTC: false,
        },
      },
      yaxis: {
        min: 0,
        labels: {
          formatter: function (val, index) {
            return numeral(val).format('0,0');
          },
        },
      },
    };
  };

  const recentDataForChart = [
    {
      name: 'Amount',
      data: _.sortBy(recentDataSeries, [(entry) => new Date(entry[0])]),
    },
  ];

  const countDataForChart = [
    {
      name: 'ACH',
      data: _.sortBy(achDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Wire',
      data: _.sortBy(wireDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Failed',
      data: _.sortBy(failedDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Returned',
      data: _.sortBy(returnedDataSeries, [(entry) => new Date(entry[0])]),
    },
  ];

  const dateRangeSelectionSchema = {
    title: 'Select dates',
    type: 'object',
    properties: {
      startDate: {
        description: 'Start Date',
      },
      endDate: {
        description: 'End Date',
      },
    },
    required: ['startDate', 'endDate'],
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <React.Fragment>
            <Button onClick={handleClick} className={classes.button} endIcon={<ChevronDownIcon />}>
              Last {interval.label ?? interval.value}
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={() => handleClose({ value: 'day' })}>Last Day</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'week' })}>Last Week</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'month' })}>Last Month</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'quarter' })}>Last Quarter</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'year' })}>Last Year</MenuItem>
              <MenuItem
                onClick={() =>
                  handleClose({
                    value: 'quarter',
                    startOf: 'quarter',
                    label: 'qtd',
                  })
                }
              >
                QTD
              </MenuItem>
              <MenuItem
                onClick={() => handleClose({ value: 'year', startOf: 'year', label: 'ytd' })}
              >
                YTD
              </MenuItem>
              <MenuItem
                onClick={() =>
                  handleClose({
                    value: 'year',
                    startOf: 'date',
                    label: 'dates',
                  })
                }
              >
                Date Selection
              </MenuItem>
            </Menu>
          </React.Fragment>
        }
        classes={{ action: classes.current }}
        subheader="Recent Financial Transfers"
        subheaderTypographyProps={{
          color: 'textSecondary',
          variant: 'caption',
        }}
        title="Transfer Summary"
        titleTypographyProps={{ color: 'textPrimary' }}
      />
      <CardContent className={classes.content}>
        <Grid container spacing={6}>
          <Grid item sm={6} md={5}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>
                {interval.label &&
                interval.label === 'dates' &&
                lastTime().startDate &&
                lastTime().endDate
                  ? 'Total : ' +
                    moment(lastTime().startDate).format('DD-MMM-yyyy') +
                    ' to ' +
                    moment(lastTime().endDate).format('DD-MMM-yyyy')
                  : 'Total last ' + (interval.label ?? interval.value)}
              </span>
              <span className={classes.gridItemValue}>
                <DoubleTextChip
                  text1={numeral(totalCountRecent).format('0,0')}
                  text2={numeral(totalAmountRecent).format('$ 0,0')}
                  gradient={{ color1: '#15487a', color2: '#2b66a1' }}
                />
              </span>
            </div>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Details</span>

              <TableContainer component={Paper}>
                <Table className={classes.table} size="small">
                  <TableBody>
                    <TableRow key="ACH">
                      <TableCell component="th" scope="row">
                        ACH
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(transactionCounts.ach).format('0,0')}
                          text2={numeral(transactionAmounts.ach).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow key="WIRE">
                      <TableCell component="th" scope="row">
                        WIRE
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(transactionCounts.wire).format('0,0')}
                          text2={numeral(transactionAmounts.wire).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow key="FAILED">
                      <TableCell component="th" scope="row">
                        FAILED
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(transactionCounts.failed).format('0,0')}
                          text2={numeral(transactionAmounts.failed).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow key="RETURNED">
                      <TableCell component="th" scope="row">
                        RETURNED
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(transactionCounts.returned).format('0,0')}
                          text2={numeral(transactionAmounts.returned).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Grid>
          <Grid item sm={6} md={7}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Capacity usage</span>
              <span className={classes.gridItemProgressValue}>
                <LinearProgress
                  variant="determinate"
                  value={capacityUsage > 1 ? 100 : capacityUsage * 100}
                  className={classes.gridItemProgressBar}
                />
                <span className={classes.gridItemProgressBarValue}>
                  {numeral(
                    capacityUsage >= 1 ? capacityUsage : Math.round(capacityUsage * 100) / 100
                  ).format('0%')}
                </span>
              </span>
              <span className={classes.gridItemLabel}>
                {numeral(transactionAmounts.ach).format('$ 0,0')}/
                {numeral(achReserveLimit * daysDiff).format('$ 0,0')}
              </span>
            </div>
            <div className={classes.gridItem}>
              <ChartTypeSelection
                {...{
                  classes,
                  chartType,
                  setChartType,
                  allowedChartTypes: ['line', 'area', 'bar', 'heatmap'],
                }}
              />
            </div>
          </Grid>
        </Grid>
        <Chart options={chartOptions()} series={recentDataForChart} type={chartType} height={317} />
        <Chart
          options={chartOptions('count')}
          series={countDataForChart}
          type={chartType}
          height={317}
        />
      </CardContent>

      {dateRangeSelectionSchema && (
        <FadeInModal open={selectingDates} setOpen={setSelectingDates}>
          <Typography gutterBottom variant="h5" component="h2">
            {dateRangeSelectionSchema.title}
          </Typography>
          <AutoForm
            schema={Bridge(dateRangeSelectionSchema)}
            onSubmit={(submittedData) => {
              setSelectingDates(false);
              setDateSelection(submittedData);
              setInterval({ value: 'year', startOf: 'date', label: 'dates' });
            }}
            modelTransform={(mode: ModelTransformMode, model: any) => {
              if (mode === 'validate' || mode === 'submit') {
                const { startDate, endDate } = model || {};
                const newModel = {
                  startDate: moment(startDate).utc().startOf('day'),
                  endDate: moment(endDate).utc().startOf('day'),
                };
                return newModel;
              }
              return model;
            }}
          >
            <DateField name="startDate" />
            <DateField name="endDate" />
            <SubmitField />
          </AutoForm>
        </FadeInModal>
      )}
    </Card>
  );
};

export default FinancialTransactionsChart;
