import React, { useEffect, useState } from 'react';
import { Transaction } from '../../api/transactionsApi';
import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import numeral from 'numeral';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  transactions: Transaction[];
}

const DIRECTION_COLUMN_INDEX = 8;
const SUBSCRIPTION_ID_COLUMN_INDEX = 5;

const FinancialTransactionsTable = ({ transactions }: Props) => {
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: true,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
    },
    {
      name: 'dealId',
      label: 'Deal ID',
      options: {
        display: false,
      },
    },
    {
      name: 'deal',
      label: 'Deal',
      options: {
        customBodyRender: (deal, tableMeta, updateValue) => deal?.name ?? '',
      },
    },
    {
      name: 'deal',
      label: 'Organizer',
      options: {
        customBodyRender: (deal, tableMeta, updateValue) => deal?.profile?.name ?? '',
      },
    },
    {
      name: 'subscriptionId',
      label: 'Subscription ID',
      options: {
        display: false,
      },
    },
    {
      name: 'subscription',
      label: 'Investor',
      options: {
        customBodyRender: (subscription, tableMeta, updateValue) =>
          subscription?.profile?.name ?? '',
      },
    },
    {
      name: 'type',
      label: 'Type',
    },
    {
      name: 'direction',
      label: 'Direction',
    },
    {
      name: 'status',
      label: 'Status',
    },
    {
      name: 'amount',
      label: 'Amount',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => numeral(value).format('$0,0'),
      },
    },
    {
      name: 'isRefund',
      label: 'Is Refund',
      options: {
        customBodyRender: (value, tableMeta, updateValue) =>
          tableMeta.rowData[SUBSCRIPTION_ID_COLUMN_INDEX] &&
          tableMeta.rowData[DIRECTION_COLUMN_INDEX] === 'CREDIT'
            ? 'Yes'
            : 'No',
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    accountID: 'string',
    amount: 'number',
    counterpartyId: 'string',
    dealId: 'string',
    direction: 'string',
    status: 'string',
    subscriptionId: 'string',
    tenantId: 'string',
    transactionId: 'string',
    type: 'string',
    createdAt: 'date',
    addedToNetwork: 'date',
    updatedAt: 'date',
  };

  const tableService = new TableWithToolsService(
    'financial-transactions-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={transactions}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`financial-transactions/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default FinancialTransactionsTable;
