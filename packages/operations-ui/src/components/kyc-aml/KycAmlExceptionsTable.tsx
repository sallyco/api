import React, { useState } from 'react';
import { Profile } from '../../api/profilesApi';
import { TableWithTools } from '@packages/gbt-ui';
import { PencilAltIcon } from 'react-line-awesome';
import { SnackbarUtils, useRequest, doChange, objectProps } from '@packages/wired-gbt-ui';
import { IconButton, ListItemIcon, Menu, MenuItem } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import CachedIcon from '@mui/icons-material/Cached';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import KycAmlChip from './KycAmlChip';
import { formatReportLabel } from '../../tools/kycAmlFormatters';

interface Props {
  profiles: Profile[];
}

const KycRowMenu: React.FC<{
  profiles: Profile[];
  profileId: string;
}> = ({ profileId, profiles }) => {
  const op = objectProps['kyc-aml'];
  const { mutate: revalidateProfiles } = useRequest(`${op.routeExceptions}`);

  const [actionsAnchorEl, setActionsAnchorEl] = useState(null);

  const handleActionsClick = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setActionsAnchorEl(event.currentTarget);
  };
  const handleActionsClose = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    setActionsAnchorEl(null);
  };
  const actionsOpen = Boolean(actionsAnchorEl);

  const getProfileFromId = (id) => profiles.find((p) => p.id === id);

  const overrideToClearCustomer = async (id) => {
    const profile = getProfileFromId(id);
    const kycAml = {
      result: 'CLEAR',
    };
    await doChange(`/identity/kyc/${profile.kycAml.id}`, 'PATCH', kycAml);
    await revalidateProfiles();
  };

  const reRunProfileCheck = async (profileId) => {
    await doChange(`/profiles/${profileId}/createKycAmlApplicant`, 'POST');
    await revalidateProfiles();
  };

  const openOnfido = async (id) => {
    const profile = getProfileFromId(id);
    if (profile?.kycAml?.checkId && profile?.kycAml?.checkId !== '') {
      window.open(`https://dashboard.onfido.com/checks/${profile?.kycAml?.checkId}`, '_blank');
    } else {
      SnackbarUtils.warning('No Check ID to open');
    }
  };

  return (
    <>
      <IconButton onClick={handleActionsClick} size="large">
        <MoreVertIcon />
      </IconButton>
      <Menu anchorEl={actionsAnchorEl} keepMounted open={actionsOpen} onClose={handleActionsClose}>
        <MenuItem
          divider
          onClick={async (e) => {
            await handleActionsClose(e);
            await openOnfido(profileId);
          }}
        >
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          View in Onfido
        </MenuItem>
        <MenuItem
          divider
          onClick={async (e) => {
            await handleActionsClose(e);
            await reRunProfileCheck(profileId);
          }}
        >
          <ListItemIcon>
            <CachedIcon />
          </ListItemIcon>
          Re-Check Identity / KYC
        </MenuItem>
        <MenuItem
          disabled={getProfileFromId(profileId)?.kycAml?.result == 'CLEAR'}
          key={'Clear User'}
          onClick={async (e) => {
            await handleActionsClose(e);
            await overrideToClearCustomer(profileId);
          }}
        >
          <ListItemIcon>
            <PencilAltIcon />
          </ListItemIcon>
          Mark Clear
        </MenuItem>
      </Menu>
    </>
  );
};

const multiIndexOf = (array, key, value) => {
  const idxs = [];
  for (let i = array.length - 1; i >= 0; i--) {
    if (array[i]?.[key] === value) {
      idxs.unshift(i);
    }
  }
  return idxs;
};

const KYCAML_COLUMN_INDEX = 6;
const NAME_COLUMN_INDEX = 2;

const KycAmlExceptionsTable = ({ profiles }: Props) => {
  const getProfileName = (rowData) => {
    const name = rowData[2];
    const firstName = rowData[8];
    const lastName = rowData[9];
    return name ?? firstName + (lastName ? ' ' + lastName : '');
  };

  const getExportableData = (columns, data, omitColumns = []) => {
    const displayedColumns = columns
      .filter((c) => c.display && c.display === 'true' && !omitColumns.includes(c.label))
      .map((c, i) => {
        return { label: c.label, index: i };
      });

    const displayedIndices = multiIndexOf(
      columns.filter((c) => !omitColumns.includes(c.label)),
      'display',
      'true'
    );

    const displayedData = [];
    data.forEach((rowData, ri) => {
      displayedData.push(
        rowData.data
          .map((d, i) => {
            if (displayedIndices.includes(i)) {
              if (i === NAME_COLUMN_INDEX) {
                return getProfileName(rowData.data);
              }
              if (i === KYCAML_COLUMN_INDEX) {
                const record = profiles[ri];
                return (
                  formatReportLabel(record, 'identity_enhanced') +
                  '/' +
                  formatReportLabel(record, 'watchlist_enhanced')
                );
              }
              return d;
            }
          })
          .filter((d) => d)
      );
    });

    return {
      displayedColumns: displayedColumns.map((c) => c.label),
      displayedData,
    };
  };

  const getCsvStringFromArrayOfStrings = (columns: any[], data) => {
    const csvHeader = columns.map((column) => `"${column}"`).join();
    const csvBody = data
      .map((row) => row.map((cell) => (cell !== null ? `"${cell}"` : '""')).join())
      .join('\n');

    return `${csvHeader}\n${csvBody}`;
  };

  const saveCsvStringAsFile = (csvString, fileName) => {
    const url = window.URL.createObjectURL(new Blob([csvString]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${fileName.replace(' ', '')}.csv`);
    document.body.appendChild(link);
    link.click();
    link.parentNode.removeChild(link);
  };

  const overrideDefaultExport = (buildHead, buildBody, columns, data) => {
    const omitColumns = ['Actions'];
    const dataToExport = getExportableData(columns, data, omitColumns);
    const csvString = getCsvStringFromArrayOfStrings(
      dataToExport.displayedColumns,
      dataToExport.displayedData
    );
    saveCsvStringAsFile(csvString, 'KYC_AML_Exceptions.csv');
    return false;
  };

  return (
    <TableWithTools
      data={profiles}
      columnDefinitions={[
        {
          name: 'kycAml',
          label: 'checkId',
          options: {
            filter: false,
            display: false,
            customBodyRender: (kycAml, tableMeta, updateValue) => kycAml?.checkId,
          },
        },
        {
          name: 'id',
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: 'name',
          label: 'Name',
          options: {
            filter: false,
            customBodyRender: function fn(value, tableMeta, updateValue) {
              return <>{getProfileName(tableMeta.rowData)}</>;
            },
          },
        },
        {
          name: 'tenantId',
          label: 'Tenant',
        },
        {
          name: 'profileType',
          label: 'Type',
        },
        {
          name: 'email',
          label: 'E-mail',
          options: {
            filter: false,
          },
        },
        {
          name: 'kycAml',
          label: 'Identity / KYC Checks',
          options: {
            filter: true,
            filterOptions: {
              names: ['NOT RUN', 'IN PROGRESS', 'CONSIDER', 'UNIDENTIFIED', 'CLEAR'],
              logic(value, filter) {
                const identity_label = formatReportLabel(value, 'identity_enhanced');
                const watchlist_label = formatReportLabel(value, 'watchlist_enhanced');
                console.log(filter.includes(identity_label) || filter.includes(watchlist_label));
                return filter.includes(identity_label) || filter.includes(watchlist_label);
              },
            },
            customBodyRender: function fn(kycAml, tableMeta, updateValue) {
              const record = profiles[tableMeta.currentTableData[tableMeta.rowIndex].index];
              return (
                <>
                  <KycAmlChip profile={record} />
                </>
              );
            },
          },
        },
        {
          name: 'id',
          label: 'Actions',
          options: {
            filter: false,
            customBodyRender: function Fn(value, tableMeta, updateValue) {
              return (
                <>
                  <KycRowMenu profiles={profiles} profileId={value} />
                </>
              );
            },
          },
        },
        {
          name: 'firstName',
          label: 'First Name',
          options: {
            display: false,
          },
        },
        {
          name: 'lastName',
          label: 'Last Name',
          options: {
            display: false,
          },
        },
        {
          name: 'profileType',
          label: 'ProfileType',
          options: {
            display: false,
          },
        },
      ]}
      // onRowClick={(r, m) => {
      //
      // }}
      selectable={false}
      // clickable
      downloadOptions={{
        filename: 'KYC_AML_Exceptions.csv',
        separator: ';',
        useDisplayedRowsOnly: true,
      }}
      onDownload={overrideDefaultExport}
    />
  );
};

export default KycAmlExceptionsTable;
