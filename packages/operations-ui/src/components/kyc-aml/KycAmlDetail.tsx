import React from 'react';
import { Profile } from '../../api/profilesApi';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from '@mui/material';
import { Alert } from '@mui/material';
import { formatReportLabel } from '../../tools/kycAmlFormatters';
import { InfoCard } from '@packages/gbt-ui';

const KycAmlDetail: React.FC<{
  profile: Profile;
  open: boolean;
  report: string;
  handleClose: (e) => void;
}> = ({ profile, open, report, handleClose }) => {
  const getKycAmlReason = (profile: Profile) => {
    if (profile?.primarySignatory && !profile?.primarySignatory?.address) {
      return 'Missing Primary Signatory Address';
    }
    if (
      profile?.primarySignatory?.taxDetails?.type === 'ssn' &&
      profile?.primarySignatory?.taxDetails?.value === ''
    ) {
      return 'Missing SSN on Primary Signatory';
    }
    if (!profile?.dateOfBirth && !profile?.primarySignatory?.dateOfBirth) {
      return 'Missing Date of Birth';
    }
    if (
      profile?.taxDetails?.taxIdentification?.type === 'ssn' &&
      profile?.taxDetails?.taxIdentification?.value === ''
    ) {
      return 'Missing SSN';
    }
    return profile?.kycAml?.reason;
  };

  const reportNameMap = {
    identity_enhanced: 'Identity',
    watchlist_enhanced: 'KYC & Watchlists',
  };
  const reportData = profile?.kycAml?.providerMeta?.reports?.find((r) => r.name === report);
  return (
    <>
      <Dialog open={open} onClose={handleClose} fullWidth={true}>
        <DialogTitle>
          <Typography variant="h3">{reportNameMap[report]} Report Detail</Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {'NOT RUN' === formatReportLabel(profile, report) && (
              <Alert severity={'warning'}>This report has not been run</Alert>
            )}
            {['EXCEPTION', 'NOT RUN'].includes(formatReportLabel(profile, report)) &&
              getKycAmlReason(profile) && (
                <Alert severity={'error'}>
                  Issue with running the report: {getKycAmlReason(profile)}
                </Alert>
              )}

            {'NOT RUN' !== formatReportLabel(profile, report) && <InfoCard item={reportData} />}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
export default KycAmlDetail;
