import { Profile } from '../../api/profilesApi';
import { Chip } from '@mui/material';
import React, { useState } from 'react';
import KycAmlDetail from './KycAmlDetail';
import { formatReportLabel, useKycAmlChipStyles } from '../../tools/kycAmlFormatters';

const KycAmlChip: React.FC<{ profile: Profile }> = ({ profile }) => {
  const styles = useKycAmlChipStyles();
  const mapStatusToChipStyle = {
    CLEAR: styles.greenChip,
    EXCEPTION: styles.grayChip,
    CONSIDER: styles.yellowChip,
    UNIDENTIFIED: styles.yellowChip,
    'IN PROGRESS': styles.blueChip,
  };
  const mapChipPositionToStyle = {
    first: styles.firstChip,
    middle: styles.middleChip,
    last: styles.lastChip,
  };

  const KycChipBuilder = (
    profile: Profile,
    report_name = 'identity_enhanced',
    position: 'first' | 'middle' | 'last' = 'first',
    clickHandler?: (e) => void
  ) => {
    const label = formatReportLabel(profile, report_name);

    return (
      <Chip
        label={label}
        color="default"
        size="medium"
        onClick={clickHandler}
        className={`${styles.kycChip} ${mapStatusToChipStyle[label]} ${mapChipPositionToStyle[position]}`}
      />
    );
  };

  const [detailOpen, setDetailOpen] = useState(false);
  const [selectedReport, setSelectedReport] = useState('');

  return (
    <>
      <div className={styles.kycChipContainer}>
        {KycChipBuilder(profile, 'identity_enhanced', 'first', () => {
          setSelectedReport('identity_enhanced');
          setDetailOpen(true);
        })}
        {KycChipBuilder(profile, 'watchlist_enhanced', 'last', () => {
          setSelectedReport('watchlist_enhanced');
          setDetailOpen(true);
        })}
      </div>
      <KycAmlDetail
        profile={profile}
        open={detailOpen}
        report={selectedReport}
        handleClose={() => setDetailOpen(false)}
      />
    </>
  );
};
export default KycAmlChip;
