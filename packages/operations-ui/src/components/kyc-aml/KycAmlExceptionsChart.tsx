import React from 'react';
import { Grid, Card, CardHeader, CardContent, Button, Menu, MenuItem } from '@mui/material';
import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { ChevronDownIcon } from 'react-line-awesome';
import _ from 'lodash';
import moment from 'moment';
import numeral from 'numeral';
import { ApexOptions } from 'apexcharts';
import { Profile } from '../../api/profilesApi';
import ChartTypeSelection from '../common/ChartTypeSelection';

interface Props {
  profiles: Profile[];
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  gridItemProgressValue: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },
  gridItemProgressBar: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },
  gridItemProgressBarValue: {
    paddingLeft: '1rem',
  },
}));

const KycAmlExceptionsChart = ({ profiles }: Props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [timeBox, setTimeBox] = React.useState<moment.unitOfTime.DurationConstructor>('month');

  const [chartType, setChartType] = React.useState<
    | 'line'
    | 'area'
    | 'bar'
    | 'histogram'
    | 'pie'
    | 'donut'
    | 'radialBar'
    | 'scatter'
    | 'bubble'
    | 'heatmap'
    | 'treemap'
    | 'boxPlot'
    | 'candlestick'
    | 'radar'
    | 'polarArea'
    | 'rangeBar'
  >('line');

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (value: moment.unitOfTime.DurationConstructor) => {
    setTimeBox(value);
    setAnchorEl(null);
  };

  const getBreakdownFromCheckResults = (kycAml) => {
    const breakdown: any = {};
    for (const report of kycAml?.providerMeta?.reports ?? []) {
      if (report.name === 'watchlist_enhanced') {
        breakdown.sanction = report.breakdown!['sanction'];
        breakdown.monitored_lists = report.breakdown!['monitored_lists'];
      } else if (report.name === 'identity_enhanced') {
        breakdown.identity = {
          result: report.result,
        };
      }
    }
    return breakdown;
  };

  const lastTime = moment().subtract(1, timeBox);

  const recentChecks = _.filter(profiles, (entry) => {
    return (
      entry.kycAml &&
      moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isAfter(lastTime) &&
      (entry.kycAml.overrideStatus === 'CLEAR' ||
        entry.kycAml.status === 'EXCEPTION' ||
        entry.kycAml.result === 'CLEAR' ||
        entry.kycAml.result === 'CONSIDER')
    );
  });

  const queuedChecks = _.filter(profiles, (entry) => {
    return (
      entry.kycAml &&
      !['CLEAR', 'CONSIDER'].includes(entry.kycAml.result) &&
      !['EXCEPTION'].includes(entry.kycAml.status) &&
      !['CLEAR'].includes(entry.kycAml.overrideStatus)
    );
  });

  const totalProfiles = _.sumBy(profiles, (entry) => {
    return 1;
  });
  const totalRecent = _.sumBy(recentChecks, (entry) => {
    return 1;
  });
  const totalQueued = _.sumBy(queuedChecks, (entry) => {
    return 1;
  });

  const recentDataSeries = _(recentChecks)
    .groupBy((entry) => {
      return moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(timeBox == 'day' ? 'hour' : 'day');
    })
    .map((entry, id) => [
      id,
      _.sumBy(entry, (en) => {
        return 1;
      }),
    ])
    .value();

  const recentCleared = _.filter(profiles, (entry) => {
    return (
      entry.kycAml &&
      moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isAfter(lastTime) &&
      (entry.kycAml.overrideStatus === 'CLEAR' || entry.kycAml.result === 'CLEAR')
    );
  });
  const recentClearedDataSeries = _(recentCleared)
    .groupBy((entry) => {
      return (
        entry.kycAml &&
        moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
          .local()
          .startOf(timeBox == 'day' ? 'hour' : 'day')
      );
    })
    .map((entry, id) => [id, _.sumBy(entry, (en) => 1)])
    .value();

  const recentException = _.filter(profiles, (entry) => {
    return (
      entry.kycAml &&
      moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isAfter(lastTime) &&
      entry.kycAml.status === 'EXCEPTION'
    );
  });
  const recentExceptionDataSeries = _(recentException)
    .groupBy((entry) => {
      return (
        entry.kycAml &&
        moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
          .local()
          .startOf(timeBox == 'day' ? 'hour' : 'day')
      );
    })
    .map((entry, id) => [id, _.sumBy(entry, (en) => 1)])
    .value();

  const recentSuspected = _.filter(profiles, (entry) => {
    const breakdown = getBreakdownFromCheckResults(entry.kycAml);
    return (
      entry.kycAml &&
      moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isAfter(lastTime) &&
      entry.kycAml.overrideStatus !== 'CLEAR' &&
      entry.kycAml.result !== 'CLEAR' &&
      breakdown &&
      ((breakdown['sanction'] && breakdown['sanction'].result.toLowerCase() === 'consider') ||
        (breakdown['monitored_lists'] &&
          breakdown['monitored_lists'].result.toLowerCase() === 'consider'))
    );
  });
  const recentSuspectedDataSeries = _(recentSuspected)
    .groupBy((entry) => {
      return (
        entry.kycAml &&
        moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
          .local()
          .startOf(timeBox == 'day' ? 'hour' : 'day')
      );
    })
    .map((entry, id) => [id, _.sumBy(entry, (en) => 1)])
    .value();

  const recentIdentity = _.filter(profiles, (entry) => {
    const breakdown = getBreakdownFromCheckResults(entry.kycAml);
    return (
      entry.kycAml &&
      moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isAfter(lastTime) &&
      entry.kycAml.overrideStatus !== 'CLEAR' &&
      entry.kycAml.result !== 'CLEAR' &&
      breakdown &&
      breakdown['identity'] &&
      breakdown['identity'].result.toLowerCase() === 'consider'
    );
  });
  const recentIdentityDataSeries = _(recentIdentity)
    .groupBy((entry) => {
      return (
        entry.kycAml &&
        moment(entry.kycAml.lastDateChecked, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
          .local()
          .startOf(timeBox == 'day' ? 'hour' : 'day')
      );
    })
    .map((entry, id) => [id, _.sumBy(entry, (en) => 1)])
    .value();

  const chartOptions: ApexOptions = {
    chart: {
      id: 'basic',
      toolbar: {
        show: false,
      },
      sparkline: {
        enabled: false,
      },
      zoom: {
        autoScaleYaxis: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    grid: {
      strokeDashArray: 2,
      borderColor: 'rgba(125, 138, 156, 0.3)',
    },
    stroke: {
      width: 3,
      curve: 'smooth',
      colors: ['#33cc33', '#0080ff', '#ff0000', '#ffbf00'],
    },
    fill: {
      type: 'gradient',
      opacity: 0.1,
      gradient: {
        shade: 'dark',
        type: 'vertical',
        shadeIntensity: 0.1,
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 0.5,
        stops: [0, 50, 100],
      },
    },
    plotOptions: {
      bar: {
        columnWidth: '20%',
      },
    },
    tooltip: {
      x: {
        format: timeBox === 'day' ? 'dd MMM yyyy : HH:mm' : 'dd MMM yyyy',
      },
    },
    colors: ['#33cc33', '#0080ff', '#ff0000', '#ffbf00'],
    legend: {
      show: true,
    },
    xaxis: {
      type: 'datetime',
      min: lastTime.valueOf(),
      tickAmount: 6,
      labels: {
        datetimeFormatter: {
          year: 'yyyy',
          month: "MMM 'yy",
          day: 'dd MMM',
          hour: 'HH:mm',
        },
      },
    },
    yaxis: {
      min: 0,
      labels: {
        formatter: function (val, index) {
          return numeral(val).format('0,0');
        },
      },
    },
  };

  const chartsLarge3Data = [
    {
      name: 'Clear',
      data: _.sortBy(recentClearedDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Exception',
      data: _.sortBy(recentExceptionDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Suspected',
      data: _.sortBy(recentSuspectedDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Identity',
      data: _.sortBy(recentIdentityDataSeries, [(entry) => new Date(entry[0])]),
    },
  ];

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <React.Fragment>
            <Button onClick={handleClick} className={classes.button} endIcon={<ChevronDownIcon />}>
              Last {timeBox}
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={() => handleClose('day')}>Last Day</MenuItem>
              <MenuItem onClick={() => handleClose('week')}>Last Week</MenuItem>
              <MenuItem onClick={() => handleClose('month')}>Last Month</MenuItem>
            </Menu>
          </React.Fragment>
        }
        classes={{ action: classes.current }}
        subheader="Recent KYC/AML checks activity"
        subheaderTypographyProps={{
          color: 'textSecondary',
          variant: 'caption',
        }}
        title="Activity Summary"
        titleTypographyProps={{ color: 'textPrimary' }}
      />
      <CardContent className={classes.content}>
        <Grid container spacing={6}>
          <Grid item sm={6} md={5}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total checks last {timeBox}</span>
              <span className={classes.gridItemValue}>{numeral(totalRecent).format('0,0')}</span>
            </div>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total checks queued</span>
              <span className={classes.gridItemValue}>{numeral(totalQueued).format('0,0')}</span>
            </div>
          </Grid>
          <Grid item sm={6} md={7}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total profiles</span>
              <span className={classes.gridItemValue}>{numeral(totalProfiles).format('0,0')}</span>
            </div>
            <div className={classes.gridItem}>
              <ChartTypeSelection
                {...{
                  classes,
                  chartType,
                  setChartType,
                  allowedChartTypes: ['line', 'area', 'bar', 'heatmap'],
                }}
              />
            </div>
          </Grid>
        </Grid>
        <Chart options={chartOptions} series={chartsLarge3Data} type={chartType} height={317} />
      </CardContent>
    </Card>
  );
};

export default KycAmlExceptionsChart;
