import React, { useEffect, useState } from 'react';
import { Tenant } from '../../api/tenantsApi';
import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  tenants: Tenant[];
}

const TenantsTable = ({ tenants }: Props) => {
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      label: 'Tenant',
    },
    {
      name: 'assets.images.logo',
      label: 'Logo',
      options: {
        filter: false,
        customBodyRender: function Rn(value, tableMeta, updateValue) {
          return <img src={value} style={{ maxHeight: '50px' }} />;
        },
      },
    },
    {
      name: 'url',
      label: 'URL',
      width: 200,
    },
  ];

  const fieldMap: any = {
    id: 'string',
    name: 'string',
    url: 'string',
    inverted: 'boolean',
    assets: 'object',
    settings: 'object',
    dealLimit: 'number',
    closedStats: 'number',
    address: 'object',
    startDate: 'date',
    endDate: 'date',
    saasPackageType: 'object',
    paymentMethod: 'object',
    services: 'object',
    createBankAccount: 'boolean',
    maxAllowedDaysToClose: 'number',
  };

  const tableService = new TableWithToolsService(
    'tenants-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={tenants}
      columnDefinitions={columnDefinitions}
      clickable
      onRowClick={(r, m) => {
        router.push(`tenants/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
    />
  );
};

export default TenantsTable;
