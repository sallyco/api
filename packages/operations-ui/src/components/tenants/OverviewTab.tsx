import React from 'react';
//import { Button, Container, Form } from "semantic-ui-react";
import * as Yup from 'yup';

interface Props {
  tenant: any;
}

export const OverviewTab = ({ tenant }: Props) => {
  const validation = Yup.object().shape({
    name: Yup.string().label('Email').required(),
    url: Yup.string().label('Password').trim().required(),
    'owner.name': Yup.string().trim(),
    'owner.email': Yup.string().trim(),
    'owner.phone': Yup.string().trim(),
  });

  async function onSubmit(data) {
    //dispatch(updateTenantById({ ...data, id: tenant.id }));
  }

  return <></>;
};
/**
    <Container>
      <Formik
        initialValues={{ ...tenant }}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} data-testid={"login-form"}>
            <FormField
              label="Name"
              id="name"
              name="name"
              placeholder="Name"
              type={"text"}
            />
            <FormField
              label="URL"
              id="url"
              name="url"
              type="text"
              placeholder="URL"
            />
            <FormField
              label="Tenant Owner Name"
              id="owner.name"
              name="owner.name"
              type="text"
              placeholder="Tenant Owner Name"
            />
            <FormField
              label="Tenant Owner Email"
              id="owner.email"
              name="owner.email"
              type="text"
              placeholder="Tenant Owner Email"
            />
            <FormField
              label="Tenant Owner Phone"
              id="owner.phone"
              name="owner.phone"
              type="text"
              placeholder="Tenant Owner Phone"
            />
            <Button
              primary
              fluid
              type="submit"
              data-testid={"submit-button"}
              // loading={submitting}
              disabled={!props.dirty || !props.isValid}
              content="Update"
            />
          </Form>
        )}
      </Formik>
    </Container>
  );
};
**/
