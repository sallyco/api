import React from 'react';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import { SnackbarUtils } from '@packages/wired-gbt-ui';
import useSWR from 'swr';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { useFormik, FieldArray, FormikProvider } from 'formik';
import {
  Checkbox,
  Divider,
  Button,
  Typography,
  TextField,
  FormControlLabel,
  MenuItem,
  Box,
  Grid,
  IconButton,
  Card,
  CardContent,
  CardHeader,
} from '@mui/material';
import * as Yup from 'yup';
import { TrashIcon } from 'react-line-awesome';

const fetcher = (url) => fetch(url).then((res) => res.json());

interface Props {
  title: string | undefined;
  item_name: any;
  items_name: string;
  setTabData: any;
  currentTabdata: any;
  previousTabData?: any;
  tenantId?: any;
  setTenantId?: any;
  setTabIndex?: any;
  companyId?: any;
  setCompanyId?: any;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
  button: {
    background: '#1976d2',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 40,
    padding: '0 30px',
  },
  textField: {
    margin: '0px 0px 20px 0px',
  },
  divider: {
    margin: '0px 0px 20px 0px',
  },
  error: {
    color: '#f83245',
    fontFamily: 'Lato, Helvetica, Arial, sans-serif',
    fontWeight: 400,
    lineHeight: 1.66,
  },
}));

export default function TenantCreateModal({
  title,
  item_name,
  items_name,
  setTabData,
  currentTabdata,
  previousTabData,
  tenantId = '',
  setTenantId,
  setTabIndex,
  companyId = '',
  setCompanyId,
}: Props) {
  const createurl = `${baseUrl}/${items_name}`;
  const classes = useStyles();

  const productsUrl = `${baseUrl}/products`;
  const { data: products } = useSWR(() => `${productsUrl}`, fetcher);

  const getInitialTabData = (tabName) => {
    if (tabName === 'tenant-tab-1') {
      const tabData = currentTabdata ?? {
        id: '',
        name: '',
        url: '',
        address: {
          address: '',
          city: '',
          state: '',
          postalCode: '',
        },
        assets: {
          images: {
            logo: '',
            logoInverted: '',
          },
          colors: {
            primaryColor: '',
            secondaryColor: '',
          },
        },
        settings: {
          emails: {
            fromAddress: '',
            complianceAddress: '',
          },
        },
        maxAllowedDaysToClose: 180,
        billingContact: '',
      };
      return tabData;
    } else if (tabName === 'tenant-tab-2') {
      const tabData = currentTabdata ?? {
        startDate: '',
        endDate: '',
        saasPackageType: {
          saasPackageType: '',
          monthlyRecurringPrice: null,
          noOfContractedSPVs: null,
          noOfSPVsconsumed: null,
          spvsremaining: null,
        },
        paymentMethod: {
          type: '',
          frequency: '',
          ach: {
            accountName: '',
            accountNumber: '',
            routingNumber: '',
            typeOfAccount: '',
          },
        },
        tenantServices: [
          {
            name: '',
            price: 0,
          },
        ],
        createBankAccount: true,
        bankAccountDetails: {
          ABA: null,
          accountnumber: '',
          bankname: '',
        },
      };
      return tabData;
    } else if (tabName === 'tenant-tab-3') {
      const tabData = currentTabdata ?? {
        name: '',
        type: '',
        stateOfFormation: '',
        entitytype: '',
        typeOfBusiness: '',
        typeOfBusinessActivity: '',
        professionalEntity: false,
        useOrganizerSSNForEINOrders: false,
        address: {
          address1: previousTabData?.address?.address ?? '',
          address2: '',
          city: previousTabData?.address?.city ?? '',
          state: previousTabData?.address?.state ?? '',
          postalCode: previousTabData?.address?.postalCode ?? '',
          country: '',
        },
        taxContact: {
          firstname: '',
          lastname: '',
          phone: '',
          address1: previousTabData?.address?.address ?? '',
          address2: '',
          city: previousTabData?.address?.city ?? '',
          state: previousTabData?.address?.state ?? '',
          postalCode: previousTabData?.address?.postalCode ?? '',
          country: '',
          taxId: '',
        },
        howWillBeBusinessManaged: '',
        individualOrBusiness: '',
        individuals: [
          {
            firstName: '',
            lastName: '',
            memberAddress: '',
            isManager: false,
            percentageOwnership: null,
            capitalContribution: null,
          },
        ],
        businesses: [
          {
            businessName: '',
            address: '',
            isManager: false,
            percentageOwnership: null,
            capitalContribution: null,
          },
        ],
      };
      return tabData;
    }
  };

  const validation = (tabName) => {
    if (tabName === 'tenant-tab-1') {
      return Yup.object().shape({
        id: Yup.string().label('Tenant Id').trim().required(),
        name: Yup.string().label('Tenant name').trim().required(),
        url: Yup.string().label('URL').trim().required(),
        address: Yup.object().shape({
          address: Yup.string().label('Address').trim().required(),
          city: Yup.string().label('City').trim().required(),
          state: Yup.string().label('State').trim().required(),
          postalCode: Yup.string().label('Postal code').trim().required(),
        }),
        maxAllowedDaysToClose: Yup.number()
          .label('Maximum days allowed to close a deal')
          .required(),
      });
    } else if (tabName === 'tenant-tab-2') {
      return Yup.object().shape({
        startDate: Yup.string().label('Contract start date').trim().required(),
        endDate: Yup.string().label('Contract end date').trim().required(),
        saasPackageType: Yup.object().shape({
          saasPackageType: Yup.string().label('Saas package type').trim().required(),
          monthlyRecurringPrice: Yup.number()
            .label('Monthly recurring price')
            .typeError('Monthly recurring price must be a number')
            .required(),
          noOfContractedSPVs: Yup.number()
            .label('Number of contracted SPVs')
            .typeError('Number of contracted SPVs must be a number')
            .required(),
          noOfSPVsconsumed: Yup.number()
            .label('Number of SPVs consumed')
            .typeError('Number of SPVs consumed must be a number')
            .required(),
          spvsremaining: Yup.number()
            .label('Contracted SPVs remaining')
            .typeError('Contracted SPVs remaining must be a number')
            .required(),
        }),
        paymentMethod: Yup.object().shape({
          type: Yup.string().label('Payment type').trim().required(),
          frequency: Yup.string().label('Payment frequency').trim().required(),
        }),
      });
    } else if (tabName === 'tenant-tab-3') {
      return Yup.object().shape({
        name: Yup.string().label('Company name').trim().required(),
        address: Yup.object().shape({
          address1: Yup.string().label('Address 1').trim().required(),
          city: Yup.string().label('City').trim().required(),
          postalCode: Yup.string().label('Postal code').trim().required(),
          country: Yup.string().label('Country').trim().required(),
        }),
        taxContact: Yup.object().shape({
          firstname: Yup.string().label("Tax contact's first name").trim().required(),
          lastname: Yup.string().label("Tax contact's last name").trim().required(),
          phone: Yup.string().label("Tax contact's phone number").trim().required(),
          address1: Yup.string().label("Tax contact's address").trim().required(),
          city: Yup.string().label("Tax contact's city").trim().required(),
          postalCode: Yup.string().label("Tax contact's postal code").trim().required(),
          country: Yup.string().label("Tax contact's country").trim().required(),
          taxId: Yup.string().label('Tax Id').trim().required(),
        }),
        individualOrBusiness: Yup.string().label('Individual/Business').trim().required(),
        individuals: Yup.array()
          .when('individualOrBusiness', {
            is: (val) => val && val.toUpperCase() === 'INDIVIDUAL',
            then: Yup.array()
              .of(
                Yup.object().shape({
                  firstName: Yup.string().label("Indvidual's first name").trim().required(),
                  lastName: Yup.string().label("Individual's last name").trim().required(),
                  memberAddress: Yup.string()
                    .label("Individual's member address")
                    .trim()
                    .required(),
                })
              )
              .test(
                'individuals.percentageOwnership test',
                'Sum of percentage ownership should be 100',
                (array) => {
                  return (
                    array.reduce((total, value) => total + (value.percentageOwnership ?? 0), 0) ===
                    100
                  );
                }
              ),
          })
          .min(1),
        businesses: Yup.array()
          .when('individualOrBusiness', {
            is: (val) => val && val.toUpperCase() === 'BUSINESS',
            then: Yup.array()
              .of(
                Yup.object().shape({
                  businessName: Yup.string().label('Business name').trim().required(),
                  address: Yup.string().label('Business address').trim().required(),
                })
              )
              .test(
                'businesses.percentageOwnership test',
                'Sum of percentage ownership should be 100',
                (array) => {
                  return (
                    array.reduce((total, value) => total + (value.percentageOwnership ?? 0), 0) ===
                    100
                  );
                }
              ),
          })
          .min(1),
      });
    }
  };

  const submitHandler = (formData) => {
    if (item_name == 'tenant-tab-1') {
      setTabData(formData);
      setTabIndex(1);
    } else if (item_name === 'tenant-tab-2' && previousTabData !== null) {
      setTabData(formData);
      if (previousTabData.id === undefined) {
        SnackbarUtils.info('Please Submit tenant details.(Tab 1)');
      } else {
        const dataTenant = { ...formData, ...previousTabData };

        if (dataTenant.createBankAccount) {
          delete dataTenant.bankAccountDetails;
        }

        if (dataTenant.paymentMethod.type !== 'ACH Debit') {
          delete dataTenant.paymentMethod.ach;
        }

        fetch(createurl, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(dataTenant),
        }).then(async (response) => {
          if (!response.ok) {
            SnackbarUtils.error('An error occured');
            throw response;
          } else {
            const data = await response.json();
            setTenantId(data.id);
            SnackbarUtils.success(`Tenant ${tenantId} Created.`);
            setTabIndex(2);
          }
        });
      }
    } else if (item_name === 'tenant-tab-3') {
      if (tenantId === '') {
        SnackbarUtils.info('Please Save Tenant details(Tab1 & Tab2)');
      } else {
        formData.tenantId = tenantId;
        setTabData(formData);

        const companyData = { ...formData };

        if (companyData.individualOrBusiness && companyData.individualOrBusiness === 'Individual') {
          delete companyData.businesses;
        } else if (
          companyData.individualOrBusiness &&
          companyData.individualOrBusiness === 'Business'
        ) {
          delete companyData.individuals;
        }

        fetch(createurl, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(companyData),
        }).then(async (response) => {
          if (!response.ok) {
            SnackbarUtils.error('An error occured');
            throw response;
          } else {
            const data = await response.json();
            setCompanyId(data.id);
            SnackbarUtils.success(`Company created.`);
            setTabIndex(3);
          }
        });
      }
    } else {
      SnackbarUtils.error('An error occured');
    }
  };

  const formik = useFormik({
    initialValues: { ...getInitialTabData(item_name) },
    validationSchema: validation(item_name),
    validateOnChange: false,
    validateOnBlur: true,
    onSubmit: (formData) => {
      submitHandler(formData);
    },
  });

  const getSaasPackageDetails = (type: string): { price: number; spvCount: number } => {
    switch (type) {
      case 'Bronze':
        return {
          price: 995,
          spvCount: 1,
        };

      case 'Silver':
        return {
          price: 2995,
          spvCount: 4,
        };

      case 'Gold':
        return {
          price: 4995,
          spvCount: 8,
        };

      default:
        return {
          price: 0,
          spvCount: 0,
        };
    }
  };

  const setSaasPackageDetails = (e) => {
    const selectedType = e.target.value;
    const packageDetails = getSaasPackageDetails(selectedType);
    formik.setFieldValue('saasPackageType.monthlyRecurringPrice', packageDetails.price, true);
    formik.setFieldValue('saasPackageType.noOfContractedSPVs', packageDetails.spvCount, true);
  };

  const getBase64 = (file, cb) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (error) {};
  };

  const setLogo = (e, inverted = false) => {
    getBase64(e.target.files[0], (result) => {
      if (inverted) {
        formik.setFieldValue('assets.images.logoInverted', result, true);
      } else {
        formik.setFieldValue('assets.images.logo', result, true);
      }
    });
  };

  const setServicePrice = (e, index) => {
    const selectedProduct = e.target.value;
    const productDetails = products.filter((product) => product.id === selectedProduct)![0] ?? {};
    formik.setFieldValue(`tenantServices.${index}.price`, productDetails?.amount, true);
    formik.setFieldValue(`tenantServices.${index}.name`, productDetails?.name, true);
  };

  const isTouchedAndHaveError = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        )
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey])))
      );
    }
  };

  const getErrorText = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        ) &&
        (!subKey ? formik.errors[key][index] : formik.errors[key][index][subKey])
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey]))) &&
        (!subKey ? formik.errors[key] : formik.errors[key][subKey])
      );
    }
  };

  return (
    <>
      {item_name === 'tenant-tab-1' && (
        <form
          onSubmit={formik.handleSubmit}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        >
          <Typography variant="subtitle2" color="textPrimary">
            Tenant Details
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Tenant Id"
            id="id"
            name="id"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.id}
            error={isTouchedAndHaveError('id')}
            helperText={getErrorText('id')}
          />
          <TextField
            label="Name"
            id="name"
            name="name"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.name}
            error={isTouchedAndHaveError('name')}
            helperText={getErrorText('name')}
          />
          <TextField
            label="URL"
            id="url"
            name="url"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.url}
            error={isTouchedAndHaveError('url')}
            helperText={getErrorText('url')}
          />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Address
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Address"
            id="address.address"
            name="address.address"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.address.address}
            error={isTouchedAndHaveError('address', 'address')}
            helperText={getErrorText('address', 'address')}
          />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="City"
                  id="address.city"
                  name="address.city"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.city}
                  error={isTouchedAndHaveError('address', 'city')}
                  helperText={getErrorText('address', 'city')}
                />
                <TextField
                  label="Postal Code"
                  id="address.postalCode"
                  name="address.postalCode"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.postalCode}
                  error={isTouchedAndHaveError('address', 'postalCode')}
                  helperText={getErrorText('address', 'postalCode')}
                />
              </Grid>
            </div>
          </Box>
          <TextField
            label="State"
            id="address.state"
            name="address.state"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.address.state}
            error={isTouchedAndHaveError('address', 'state')}
            helperText={getErrorText('address', 'state')}
          />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Images
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Logo"
                  id="assets.images.logo"
                  name="assets.images.logo"
                  type="file"
                  onChange={(e) => {
                    formik.handleChange(e);
                    setLogo(e);
                  }}
                  fullWidth
                  className={classes.textField}
                  InputLabelProps={{ shrink: true }}
                />
                <img
                  src={formik.values.assets?.images?.logo ?? ''}
                  style={{ width: 50, height: 50 }}
                />
              </Grid>
            </div>
          </Box>
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Logo Inverted"
                  id="assets.images.logoInverted"
                  name="assets.images.logoInverted"
                  type="file"
                  onChange={(e) => {
                    formik.handleChange(e);
                    setLogo(e, true);
                  }}
                  fullWidth
                  className={classes.textField}
                  InputLabelProps={{ shrink: true }}
                />
                <img
                  src={formik.values.assets?.images?.logoInverted ?? ''}
                  style={{ border: 'solid white 0px', width: 50, height: 50 }}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Colors
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Primary Color"
                  id="assets.colors.primaryColor"
                  name="assets.colors.primaryColor"
                  type="color"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.assets.colors.primaryColor}
                  InputLabelProps={{ shrink: true }}
                />
                <TextField
                  label="Secondary Color"
                  id="assets.colors.secondaryColor"
                  name="assets.colors.secondaryColor"
                  type="color"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.assets.colors.secondaryColor}
                  InputLabelProps={{ shrink: true }}
                />
                <TextField
                  label="Background Color"
                  id="assets.colors.backgroundColor"
                  name="assets.colors.backgroundColor"
                  type="color"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.assets.colors.backgroundColor}
                  InputLabelProps={{ shrink: true }}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Email
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="From Address"
                  id="settings.emails.fromAddress"
                  name="settings.emails.fromAddress"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.settings.emails.fromAddress}
                />
                <TextField
                  label="Compliance Address"
                  id="settings.emails.complianceAddress"
                  name="settings.emails.complianceAddress"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.settings.emails.complianceAddress}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} />
          <TextField
            label="Max Allowed Days for Close"
            id="maxAllowedDaysToClose"
            name="maxAllowedDaysToClose"
            type="number"
            fullWidth
            className={classes.textField}
            value={formik.values.maxAllowedDaysToClose}
            error={isTouchedAndHaveError('maxAllowedDaysToClose')}
            helperText={getErrorText('maxAllowedDaysToClose')}
          />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Billing Details
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Billing Contact"
            id="billingContact"
            name="billingContact"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.billingContact}
          />

          <Button color="primary" fullWidth type="submit" disabled={!formik.isValid}>
            {'Next'}
          </Button>
        </form>
      )}
      {item_name === 'tenant-tab-2' && (
        <form
          onSubmit={formik.handleSubmit}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        >
          <Typography variant="subtitle2" color="textPrimary">
            Contracts Administration Details
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Start Date"
                  id="startDate"
                  name="startDate"
                  type="date"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.startDate}
                  error={isTouchedAndHaveError('startDate')}
                  helperText={getErrorText('startDate')}
                  InputLabelProps={{ shrink: true }}
                />
                <TextField
                  label="End Date"
                  id="endDate"
                  name="endDate"
                  type="date"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.endDate}
                  error={isTouchedAndHaveError('endDate')}
                  helperText={getErrorText('endDate')}
                  InputLabelProps={{ shrink: true }}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Pricing Plans
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Saas Package Type"
            id="saasPackageType.saasPackageType"
            name="saasPackageType.saasPackageType"
            type="text"
            select
            fullWidth
            className={classes.textField}
            onChange={(e) => {
              formik.handleChange(e);
              setSaasPackageDetails(e);
            }}
            value={formik.values.saasPackageType.saasPackageType}
            error={isTouchedAndHaveError('saasPackageType', 'saasPackageType')}
            helperText={getErrorText('saasPackageType', 'saasPackageType')}
          >
            <MenuItem value={'Bronze'}>Bronze</MenuItem>
            <MenuItem value={'Silver'}>Silver</MenuItem>
            <MenuItem value={'Gold'}>Gold</MenuItem>
          </TextField>
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Monthly Recurring Price"
                  id="saasPackageType.monthlyRecurringPrice"
                  name="saasPackageType.monthlyRecurringPrice"
                  type="number"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.saasPackageType.monthlyRecurringPrice}
                  error={isTouchedAndHaveError('saasPackageType', 'monthlyRecurringPrice')}
                  helperText={getErrorText('saasPackageType', 'monthlyRecurringPrice')}
                  InputLabelProps={{ shrink: true }}
                />
                <TextField
                  label="No Of Contracted SPVs"
                  id="saasPackageType.noOfContractedSPVs"
                  name="saasPackageType.noOfContractedSPVs"
                  type="number"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.saasPackageType.noOfContractedSPVs}
                  error={isTouchedAndHaveError('saasPackageType', 'noOfContractedSPVs')}
                  helperText={getErrorText('saasPackageType', 'noOfContractedSPVs')}
                  InputLabelProps={{ shrink: true }}
                />
              </Grid>
            </div>
          </Box>
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="No Of SPVs Consumed"
                  id="saasPackageType.noOfSPVsconsumed"
                  name="saasPackageType.noOfSPVsconsumed"
                  type="number"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.saasPackageType.noOfSPVsconsumed}
                  error={isTouchedAndHaveError('saasPackageType', 'noOfSPVsconsumed')}
                  helperText={getErrorText('saasPackageType', 'noOfSPVsconsumed')}
                />
                <TextField
                  label="SPVs Remaining"
                  id="saasPackageType.spvsremaining"
                  name="saasPackageType.spvsremaining"
                  type="number"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.saasPackageType.spvsremaining}
                  error={isTouchedAndHaveError('saasPackageType', 'spvsremaining')}
                  helperText={getErrorText('saasPackageType', 'spvsremaining')}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Payment Method
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Type"
                  id="paymentMethod.type"
                  name="paymentMethod.type"
                  type="text"
                  select
                  fullWidth
                  className={classes.textField}
                  value={formik.values.paymentMethod.type}
                  error={isTouchedAndHaveError('paymentMethod', 'type')}
                  helperText={getErrorText('paymentMethod', 'type')}
                  onChange={formik.handleChange}
                >
                  <MenuItem value={'With hold from Deal'}>With hold from Deal</MenuItem>
                  <MenuItem value={'Credit Card'}>Credit Card</MenuItem>
                  <MenuItem value={'Wire'}>Wire</MenuItem>
                  <MenuItem value={'ACH Debit'}>ACH Debit</MenuItem>
                </TextField>
                <TextField
                  label="Frequency"
                  id="paymentMethod.frequency"
                  name="paymentMethod.frequency"
                  type="text"
                  select
                  fullWidth
                  className={classes.textField}
                  value={formik.values.paymentMethod.frequency}
                  error={isTouchedAndHaveError('paymentMethod', 'frequency')}
                  helperText={getErrorText('paymentMethod', 'frequency')}
                  onChange={formik.handleChange}
                >
                  <MenuItem value={'Monthly'}>Monthly</MenuItem>
                  <MenuItem value={'Annually'}>Annually</MenuItem>
                </TextField>
              </Grid>
            </div>
          </Box>
          {formik.values.paymentMethod?.type && formik.values.paymentMethod?.type === 'ACH Debit' && (
            <>
              <Card>
                <CardContent>
                  <Divider className={classes.divider} hidden />
                  <Typography variant="subtitle2" color="textPrimary">
                    ACH Details
                  </Typography>
                  <Divider className={classes.divider} />
                  <Box pt={1}>
                    <div>
                      <Grid container justifyContent="space-between" wrap="nowrap">
                        <TextField
                          label="Name on Account"
                          id="paymentMethod.ach.accountName"
                          name="paymentMethod.ach.accountName"
                          type="text"
                          size="small"
                          fullWidth
                          className={classes.textField}
                          value={formik.values.paymentMethod.ach.accountName}
                        />
                        <TextField
                          label="Type of Account"
                          id="paymentMethod.ach.typeOfAccount"
                          name="paymentMethod.ach.typeOfAccount"
                          type="text"
                          size="small"
                          select
                          fullWidth
                          className={classes.textField}
                          value={formik.values.paymentMethod.ach.typeOfAccount}
                          onChange={formik.handleChange}
                        >
                          <MenuItem value={'Monthly'}>Checking</MenuItem>
                          <MenuItem value={'Annually'}>Savings</MenuItem>
                        </TextField>
                      </Grid>
                    </div>
                    <div>
                      <Grid container justifyContent="space-between" wrap="nowrap">
                        <TextField
                          label="Routing Number"
                          id="paymentMethod.ach.routingNumber"
                          name="paymentMethod.ach.routingNumber"
                          type="text"
                          size="small"
                          fullWidth
                          className={classes.textField}
                          value={formik.values.paymentMethod.ach.routingNumber}
                        />
                        <TextField
                          label="Account Number"
                          id="paymentMethod.ach.accountNumber"
                          name="paymentMethod.ach.accountNumber"
                          type="text"
                          size="small"
                          fullWidth
                          className={classes.textField}
                          value={formik.values.paymentMethod.ach.accountNumber}
                        />
                      </Grid>
                    </div>
                  </Box>
                </CardContent>
              </Card>
              <Divider className={classes.divider} />
            </>
          )}
          <Typography variant="subtitle2" color="textPrimary">
            Services
          </Typography>
          <Divider className={classes.divider} />
          <FormikProvider value={formik}>
            <FieldArray
              name="tenantServices"
              render={(arrayHelpers) => (
                <div>
                  <Button
                    type="button"
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push({
                        name: '',
                        price: 0,
                      });
                    }}
                  >
                    + Add a Service
                  </Button>
                  {products &&
                    formik.values.tenantServices &&
                    formik.values.tenantServices.length > 0 &&
                    formik.values.tenantServices.map((service, index) => (
                      <div key={index} style={{ marginBottom: 10 }}>
                        <Card>
                          <CardHeader
                            title={`Service #${index + 1}`}
                            action={
                              <IconButton
                                size="medium"
                                type="button"
                                color="secondary"
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                <TrashIcon />
                              </IconButton>
                            }
                          />
                          <CardContent>
                            <Box pt={1}>
                              <Grid container justifyContent="space-between" wrap="nowrap">
                                <TextField
                                  label="Name"
                                  id={`tenantServices.${index}.productId`}
                                  name={`tenantServices.${index}.productId`}
                                  type="text"
                                  size="small"
                                  fullWidth
                                  select
                                  className={classes.textField}
                                  value={service.productId}
                                  onChange={(e) => {
                                    formik.handleChange(e);
                                    setServicePrice(e, index);
                                  }}
                                >
                                  {products.map((product, index) => (
                                    <MenuItem key={index} value={product.id}>
                                      {product.name}
                                    </MenuItem>
                                  ))}
                                </TextField>
                                <TextField
                                  label="Price"
                                  id={`tenantServices.${index}.price`}
                                  name={`tenantServices.${index}.price`}
                                  type="number"
                                  size="small"
                                  fullWidth
                                  className={classes.textField}
                                  value={service.price}
                                />
                              </Grid>
                            </Box>
                          </CardContent>
                        </Card>
                      </div>
                    ))}
                </div>
              )}
            />
          </FormikProvider>
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Bank Account Details
          </Typography>
          <Divider className={classes.divider} />
          <FormControlLabel
            control={
              <Checkbox
                id="createBankAccount"
                name="createBankAccount"
                checked={formik.values.createBankAccount}
              />
            }
            label="Create operating account for tenant"
            className={classes.textField}
          />
          {!formik.values.createBankAccount && (
            <>
              <TextField
                label="ABA Routing Number"
                id="bankAccountDetails.ABA"
                name="bankAccountDetails.ABA"
                type="number"
                fullWidth
                className={classes.textField}
                value={formik.values.bankAccountDetails.ABA}
                error={isTouchedAndHaveError('bankAccountDetails', 'ABA')}
                helperText={getErrorText('bankAccountDetails', 'ABA')}
              />
              <TextField
                label="Account Number"
                id="bankAccountDetails.accountnumber"
                name="bankAccountDetails.accountnumber"
                type="text"
                fullWidth
                className={classes.textField}
                value={formik.values.bankAccountDetails.accountnumber}
                error={isTouchedAndHaveError('bankAccountDetails', 'accountnumber')}
                helperText={getErrorText('bankAccountDetails', 'accountnumber')}
              />
              <TextField
                label="Bank Name"
                id="bankAccountDetails.bankname"
                name="bankAccountDetails.bankname"
                type="text"
                fullWidth
                className={classes.textField}
                value={formik.values.bankAccountDetails.bankname}
                error={isTouchedAndHaveError('bankAccountDetails', 'bankname')}
                helperText={getErrorText('bankAccountDetails', 'bankname')}
              />
            </>
          )}
          <Button color="primary" fullWidth type="submit" disabled={!formik.isValid}>
            {'Add Tenant'}
          </Button>
        </form>
      )}
      {item_name === 'tenant-tab-3' && (
        <form
          onSubmit={formik.handleSubmit}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        >
          <Typography variant="subtitle2" color="textPrimary">
            Master Series Details
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Name"
            id="name"
            name="name"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.name}
            error={isTouchedAndHaveError('name')}
            helperText={getErrorText('name')}
          />
          <TextField
            label="State Of Formation"
            id="stateOfFormation"
            name="stateOfFormation"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.stateOfFormation}
            error={isTouchedAndHaveError('stateOfFormation')}
            helperText={getErrorText('stateOfFormation')}
          />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="Type"
                  id="type"
                  name="type"
                  type="text"
                  select
                  fullWidth
                  className={classes.textField}
                  value={formik.values.type}
                  error={formik.touched.type && Boolean(formik.errors.type)}
                  helperText={formik.touched.type && formik.errors.type}
                  onChange={formik.handleChange}
                >
                  <MenuItem value={'MASTER_ENTITY'}>Master Entity</MenuItem>
                  <MenuItem value={'REGISTERED_AGENT'}>Registered Agent</MenuItem>
                  <MenuItem value={'MANAGER'}>Manager</MenuItem>
                </TextField>
                <TextField
                  label="Entity Type"
                  id="entitytype"
                  name="entitytype"
                  type="text"
                  select
                  fullWidth
                  className={classes.textField}
                  value={formik.values.entitytype}
                  error={formik.touched.entitytype && Boolean(formik.errors.entitytype)}
                  helperText={formik.touched.entitytype && formik.errors.entitytype}
                  onChange={formik.handleChange}
                >
                  <MenuItem value={'LLC'}>LLC</MenuItem>
                  <MenuItem value={'Corporation'}>Corporation</MenuItem>
                  <MenuItem value={'Non-Profit'}>Non-Profit</MenuItem>
                  <MenuItem value={'LP'}>LP</MenuItem>
                  <MenuItem value={'LLP'}>LLP</MenuItem>
                  <MenuItem value={'GP'}>GP</MenuItem>
                  <MenuItem value={'DBA'}>DBA</MenuItem>
                </TextField>
              </Grid>
            </div>
          </Box>
          <TextField
            label="Type Of Business"
            id="typeOfBusiness"
            name="typeOfBusiness"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.typeOfBusiness}
            error={isTouchedAndHaveError('typeOfBusiness')}
            helperText={getErrorText('typeOfBusiness')}
          />
          <TextField
            label="Type Of Business Activity"
            id="typeOfBusinessActivity"
            name="typeOfBusinessActivity"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.typeOfBusinessActivity}
            error={isTouchedAndHaveError('typeOfBusinessActivity')}
            helperText={getErrorText('typeOfBusinessActivity')}
          />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Address
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="Address 1"
            id="address.address1"
            name="address.address1"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.address.address1}
            error={isTouchedAndHaveError('address', 'address1')}
            helperText={getErrorText('address', 'address1')}
          />
          <TextField
            label="Address 2"
            id="address.address2"
            name="address.address2"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.address.address2}
            error={isTouchedAndHaveError('address', 'address2')}
            helperText={getErrorText('address', 'address2')}
          />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="City"
                  id="address.city"
                  name="address.city"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.city}
                  error={isTouchedAndHaveError('address', 'city')}
                  helperText={getErrorText('address', 'city')}
                />
                <TextField
                  label="Postal Code"
                  id="address.postalCode"
                  name="address.postalCode"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.postalCode}
                  error={isTouchedAndHaveError('address', 'postalCode')}
                  helperText={getErrorText('address', 'postalCode')}
                />
              </Grid>
            </div>
          </Box>
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="State"
                  id="address.state"
                  name="address.state"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.state}
                  error={isTouchedAndHaveError('address', 'state')}
                  helperText={getErrorText('address', 'state')}
                />
                <TextField
                  label="Country"
                  id="address.country"
                  name="address.country"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.address.country}
                  error={isTouchedAndHaveError('address', 'country')}
                  helperText={getErrorText('address', 'country')}
                />
              </Grid>
            </div>
          </Box>
          <Divider className={classes.divider} />
          <FormControlLabel
            control={
              <Checkbox
                id="useOrganizerSSNForEINOrders"
                name="useOrganizerSSNForEINOrders"
                checked={formik.values.useOrganizerSSNForEINOrders}
              />
            }
            label="Use the organizer's SSN for EIN obtainment"
            className={classes.textField}
          />
          <Divider className={classes.divider} />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Tax Contact Information
          </Typography>
          <Divider className={classes.divider} />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="First Name"
                  id="taxContact.firstname"
                  name="taxContact.firstname"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.firstname}
                  error={isTouchedAndHaveError('taxContact', 'firstname')}
                  helperText={getErrorText('taxContact', 'firstname')}
                />
                <TextField
                  label="Last Name"
                  id="taxContact.lastname"
                  name="taxContact.lastname"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.lastname}
                  error={isTouchedAndHaveError('taxContact', 'lastname')}
                  helperText={getErrorText('taxContact', 'lastname')}
                />
              </Grid>
            </div>
          </Box>
          <TextField
            label="Address 1"
            id="taxContact.address1"
            name="taxContact.address1"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.taxContact.address1}
            error={isTouchedAndHaveError('taxContact', 'address1')}
            helperText={getErrorText('taxContact', 'address1')}
          />
          <TextField
            label="Address 2"
            id="taxContact.address2"
            name="taxContact.address2"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.taxContact.address2}
            error={isTouchedAndHaveError('taxContact', 'address2')}
            helperText={getErrorText('taxContact', 'address2')}
          />
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="City"
                  id="taxContact.city"
                  name="taxContact.city"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.city}
                  error={isTouchedAndHaveError('taxContact', 'city')}
                  helperText={getErrorText('taxContact', 'city')}
                />
                <TextField
                  label="Postal Code"
                  id="taxContact.postalCode"
                  name="taxContact.postalCode"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.postalCode}
                  error={isTouchedAndHaveError('taxContact', 'postalCode')}
                  helperText={getErrorText('taxContact', 'postalCode')}
                />
              </Grid>
            </div>
          </Box>
          <Box pt={1}>
            <div>
              <Grid container justifyContent="space-between" wrap="nowrap">
                <TextField
                  label="State"
                  id="taxContact.state"
                  name="taxContact.state"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.state}
                  error={isTouchedAndHaveError('taxContact', 'state')}
                  helperText={getErrorText('taxContact', 'state')}
                />
                <TextField
                  label="Country"
                  id="taxContact.country"
                  name="taxContact.country"
                  type="text"
                  fullWidth
                  className={classes.textField}
                  value={formik.values.taxContact.country}
                  error={isTouchedAndHaveError('taxContact', 'country')}
                  helperText={getErrorText('taxContact', 'country')}
                />
              </Grid>
            </div>
          </Box>
          <TextField
            label="Phone Number"
            id="taxContact.phone"
            name="taxContact.phone"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.taxContact.phone}
            error={isTouchedAndHaveError('taxContact', 'phone')}
            helperText={getErrorText('taxContact', 'phone')}
          />
          <TextField
            label="Tax Id"
            id="taxContact.taxId"
            name="taxContact.taxId"
            type="text"
            fullWidth
            className={classes.textField}
            value={formik.values.taxContact.taxId}
            error={isTouchedAndHaveError('taxContact', 'taxId')}
            helperText={getErrorText('taxContact', 'taxId')}
          />
          <Divider className={classes.divider} hidden />
          <Typography variant="subtitle2" color="textPrimary">
            Management Information
          </Typography>
          <Divider className={classes.divider} />
          <TextField
            label="How Business will Be Managed"
            id="howWillBeBusinessManaged"
            name="howWillBeBusinessManaged"
            type="text"
            select
            fullWidth
            className={classes.textField}
            value={formik.values.howWillBeBusinessManaged}
            error={isTouchedAndHaveError('howWillBeBusinessManaged')}
            helperText={getErrorText('howWillBeBusinessManaged')}
            onChange={formik.handleChange}
          >
            <MenuItem value={'Solely by the owner(s) (Member-managed)'}>
              Solely by the owner(s) (Member-managed)
            </MenuItem>
            <MenuItem value={'Some, but not all, owners (Manager-managed)'}>
              Some, but not all, owners (Manager-managed)
            </MenuItem>
            <MenuItem value={'By the owner(s) and other managers (Manager-managed)'}>
              By the owner(s) and other managers (Manager-managed)
            </MenuItem>
            <MenuItem value={'Solely by the manager(s) (Manager-Managed)'}>
              Solely by the manager(s) (Manager-Managed)
            </MenuItem>
          </TextField>
          <TextField
            label="Individual Or Business"
            id="individualOrBusiness"
            name="individualOrBusiness"
            type="text"
            select
            fullWidth
            className={classes.textField}
            value={formik.values.individualOrBusiness}
            error={
              formik.touched.individualOrBusiness && Boolean(formik.errors.individualOrBusiness)
            }
            helperText={formik.touched.individualOrBusiness && formik.errors.individualOrBusiness}
            onChange={formik.handleChange}
          >
            <MenuItem value={'Individual'}>Individual</MenuItem>
            <MenuItem value={'Business'}>Business</MenuItem>
          </TextField>
          {formik.values.individualOrBusiness &&
            formik.values.individualOrBusiness === 'Individual' && (
              <>
                <Divider className={classes.divider} hidden />
                <Typography variant="subtitle2" color="textPrimary">
                  Individual Details
                </Typography>
                <Divider className={classes.divider} />
                <FormikProvider value={formik}>
                  <FieldArray
                    name="individuals"
                    render={(arrayHelpers) => (
                      <div>
                        <Button
                          type="button"
                          color="primary"
                          onClick={() => {
                            arrayHelpers.push({
                              firstName: '',
                              lastName: '',
                              memberAddress: '',
                              isManager: false,
                              percentageOwnership: null,
                              capitalContribution: null,
                            });
                          }}
                        >
                          + Add an Individual
                        </Button>
                        {formik.values.individuals &&
                          formik.values.individuals.length > 0 &&
                          formik.values.individuals.map((individuals, index) => (
                            <div key={index} style={{ marginBottom: 10 }}>
                              <Card>
                                <CardHeader
                                  title={`Individual #${index + 1}`}
                                  action={
                                    <>
                                      {formik.values.individuals.length > 1 && (
                                        <IconButton
                                          size="medium"
                                          type="button"
                                          color="secondary"
                                          onClick={() => {
                                            arrayHelpers.remove(index);
                                          }}
                                        >
                                          <TrashIcon />
                                        </IconButton>
                                      )}
                                    </>
                                  }
                                />
                                <CardContent>
                                  <Box pt={1}>
                                    <div>
                                      <Grid container justifyContent="space-between" wrap="nowrap">
                                        <TextField
                                          label="First Name of Individual"
                                          id={`individuals.${index}.firstName`}
                                          name={`individuals.${index}.firstName`}
                                          type="text"
                                          size="small"
                                          fullWidth
                                          className={classes.textField}
                                          value={individuals.firstName}
                                          error={isTouchedAndHaveError(
                                            'individuals',
                                            'firstName',
                                            true,
                                            index
                                          )}
                                          helperText={getErrorText(
                                            'individuals',
                                            'firstName',
                                            true,
                                            index
                                          )}
                                        />
                                        <TextField
                                          label="Last Name of Individual"
                                          id={`individuals.${index}.lastName`}
                                          name={`individuals.${index}.lastName`}
                                          type="text"
                                          size="small"
                                          fullWidth
                                          className={classes.textField}
                                          value={individuals.lastName}
                                          error={isTouchedAndHaveError(
                                            'individuals',
                                            'lastName',
                                            true,
                                            index
                                          )}
                                          helperText={getErrorText(
                                            'individuals',
                                            'lastName',
                                            true,
                                            index
                                          )}
                                        />
                                      </Grid>
                                    </div>
                                    <div>
                                      <TextField
                                        label="The Member/Manager Address of individuals"
                                        id={`individuals.${index}.memberAddress`}
                                        name={`individuals.${index}.memberAddress`}
                                        type="text"
                                        size="small"
                                        fullWidth
                                        className={classes.textField}
                                        value={individuals.memberAddress}
                                        error={isTouchedAndHaveError(
                                          'individuals',
                                          'memberAddress',
                                          true,
                                          index
                                        )}
                                        helperText={getErrorText(
                                          'individuals',
                                          'memberAddress',
                                          true,
                                          index
                                        )}
                                      />
                                    </div>
                                    <div>
                                      {formik.values.howWillBeBusinessManaged &&
                                        formik.values.howWillBeBusinessManaged ===
                                          'Solely by the owner(s) (Member-managed)' && (
                                          <FormControlLabel
                                            control={
                                              <Checkbox
                                                id={`individuals.${index}.isManager`}
                                                name={`individuals.${index}.isManager`}
                                                checked={individuals.isManager}
                                              />
                                            }
                                            label="Is this owner also a manager"
                                            className={classes.textField}
                                          />
                                        )}
                                    </div>
                                    <Grid container justifyContent="space-between" wrap="nowrap">
                                      <TextField
                                        label="% Ownership of Member"
                                        id={`individuals.${index}.percentageOwnership`}
                                        name={`individuals.${index}.percentageOwnership`}
                                        type="number"
                                        size="small"
                                        fullWidth
                                        className={classes.textField}
                                        value={individuals.percentageOwnership}
                                        error={isTouchedAndHaveError(
                                          'individuals',
                                          'percentageOwnership',
                                          true,
                                          index
                                        )}
                                        helperText={getErrorText(
                                          'individuals',
                                          'percentageOwnership',
                                          true,
                                          index
                                        )}
                                      />
                                      <TextField
                                        label="Capital contribution for Member"
                                        id={`individuals.${index}.capitalContribution`}
                                        name={`individuals.${index}.capitalContribution`}
                                        type="number"
                                        size="small"
                                        fullWidth
                                        className={classes.textField}
                                        value={individuals.capitalContribution}
                                        error={isTouchedAndHaveError(
                                          'individuals',
                                          'capitalContribution',
                                          true,
                                          index
                                        )}
                                        helperText={getErrorText(
                                          'individuals',
                                          'capitalContribution',
                                          true,
                                          index
                                        )}
                                      />
                                    </Grid>
                                  </Box>
                                </CardContent>
                              </Card>
                            </div>
                          ))}
                        {formik.errors.individuals &&
                          typeof formik.errors.individuals === 'string' && (
                            <span className={classes.error}>{formik.errors.individuals}</span>
                          )}
                      </div>
                    )}
                  />
                </FormikProvider>
              </>
            )}
          {formik.values.individualOrBusiness && formik.values.individualOrBusiness === 'Business' && (
            <>
              <Divider className={classes.divider} hidden />
              <Typography variant="subtitle2" color="textPrimary">
                Business Details
              </Typography>
              <Divider className={classes.divider} />
              <FormikProvider value={formik}>
                <FieldArray
                  name="businesses"
                  render={(arrayHelpers) => (
                    <div>
                      <Button
                        type="button"
                        color="primary"
                        onClick={() => {
                          arrayHelpers.push({
                            businessName: '',
                            address: '',
                            isManager: false,
                            percentageOwnership: null,
                            capitalContribution: null,
                          });
                        }}
                      >
                        + Add a Business
                      </Button>
                      {formik.values.businesses &&
                        formik.values.businesses.length > 0 &&
                        formik.values.businesses.map((businesses, index) => (
                          <div key={index} style={{ marginBottom: 10 }}>
                            <Card>
                              <CardHeader
                                title={`Business #${index + 1}`}
                                action={
                                  <>
                                    {formik.values.businesses.length > 1 && (
                                      <IconButton
                                        size="medium"
                                        type="button"
                                        color="secondary"
                                        onClick={() => {
                                          arrayHelpers.remove(index);
                                        }}
                                      >
                                        <TrashIcon />
                                      </IconButton>
                                    )}
                                  </>
                                }
                              />
                              <CardContent>
                                <Box pt={1}>
                                  <div>
                                    <TextField
                                      label="The Name of businesses"
                                      id={`businesses.${index}.businessName`}
                                      name={`businesses.${index}.businessName`}
                                      type="text"
                                      size="small"
                                      fullWidth
                                      className={classes.textField}
                                      value={businesses.businessName}
                                      error={isTouchedAndHaveError(
                                        'businesses',
                                        'businessName',
                                        true,
                                        index
                                      )}
                                      helperText={getErrorText(
                                        'businesses',
                                        'businessName',
                                        true,
                                        index
                                      )}
                                    />
                                    <TextField
                                      label="Member/Business Address"
                                      id={`businesses.${index}.address`}
                                      name={`businesses.${index}.address`}
                                      type="text"
                                      size="small"
                                      fullWidth
                                      className={classes.textField}
                                      value={businesses.address}
                                      error={isTouchedAndHaveError(
                                        'businesses',
                                        'address',
                                        true,
                                        index
                                      )}
                                      helperText={getErrorText(
                                        'businesses',
                                        'address',
                                        true,
                                        index
                                      )}
                                    />
                                  </div>
                                  <div>
                                    {formik.values.howWillBeBusinessManaged &&
                                      formik.values.howWillBeBusinessManaged ===
                                        'Solely by the owner(s) (Member-managed)' && (
                                        <FormControlLabel
                                          control={
                                            <Checkbox
                                              id={`businesses.${index}.isManager`}
                                              name={`businesses.${index}.isManager`}
                                              checked={businesses.isManager}
                                            />
                                          }
                                          label="Is this owner also a manager"
                                          className={classes.textField}
                                        />
                                      )}
                                  </div>
                                  <div>
                                    <Grid container justifyContent="space-between" wrap="nowrap">
                                      <TextField
                                        label="% Ownership of Member"
                                        id={`businesses.${index}.percentageOwnership`}
                                        name={`businesses.${index}.percentageOwnership`}
                                        type="number"
                                        size="small"
                                        fullWidth
                                        className={classes.textField}
                                        value={businesses.percentageOwnership}
                                        error={isTouchedAndHaveError(
                                          'businesses',
                                          'percentageOwnership',
                                          true,
                                          index
                                        )}
                                        helperText={getErrorText(
                                          'businesses',
                                          'percentageOwnership',
                                          true,
                                          index
                                        )}
                                      />
                                      <TextField
                                        label="Capital contribution for Member"
                                        id={`businesses.${index}.capitalContribution`}
                                        name={`businesses.${index}.capitalContribution`}
                                        type="number"
                                        size="small"
                                        fullWidth
                                        className={classes.textField}
                                        value={businesses.capitalContribution}
                                        error={isTouchedAndHaveError(
                                          'businesses',
                                          'capitalContribution',
                                          true,
                                          index
                                        )}
                                        helperText={getErrorText(
                                          'businesses',
                                          'capitalContribution',
                                          true,
                                          index
                                        )}
                                      />
                                    </Grid>
                                  </div>
                                </Box>
                              </CardContent>
                            </Card>
                          </div>
                        ))}
                      {formik.errors.businesses && typeof formik.errors.businesses === 'string' && (
                        <span className={classes.error}>{formik.errors.businesses}</span>
                      )}
                    </div>
                  )}
                />
              </FormikProvider>
            </>
          )}
          <Button color="primary" fullWidth type="submit" disabled={!formik.isValid}>
            {'Add Company'}
          </Button>
        </form>
      )}
    </>
  );
}
