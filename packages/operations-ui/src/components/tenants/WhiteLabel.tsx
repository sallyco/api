import React from 'react';
import { Formik, Form } from 'formik';
import {
  Container,
  Box,
  Button,
  Typography,
  List,
  ListItem,
  Card,
  CardContent,
} from '@mui/material';

//import {
//  Image,
//  Button,
//  Table,
//  Form,
//  Header,
//  Container,
//  Segment,
//  Icon,
//} from "semantic-ui-react";
//
import * as Yup from 'yup';

interface Props {
  tenant: any;
}

import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
}));

const WhiteLabel = ({ tenant }: Props) => {
  const classes = useStyles();

  const validation = Yup.object().shape({
    name: Yup.string().label('Email').required(),
    url: Yup.string().label('Password').trim().required(),
    'owner.name': Yup.string().trim(),
    'owner.email': Yup.string().trim(),
    'owner.phone': Yup.string().trim(),
  });

  async function onSubmit(data) {
    //    dispatch(updateTenantById({ ...data, id: tenant.id }));
  }

  return (
    <Container>
      <Formik initialValues={{ ...tenant }} validationSchema={validation} onSubmit={onSubmit}>
        {(props) => (
          <Form onSubmit={props.handleSubmit} data-testid={'login-form'}>
            <Box py={2}>
              <Card className={classes.root}>
                <CardContent className={classes.content}>
                  <List>
                    <ListItem className={classes.listItem} disableGutters divider>
                      <Typography variant="subtitle2" color="textPrimary">
                        Logo
                      </Typography>
                      <Typography variant="h6" color="textSecondary">
                        <img src={tenant.assets.images.logo} style={{ maxHeight: '50px' }} />
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.listItem} disableGutters divider>
                      <Typography variant="subtitle2" color="textPrimary">
                        Primary Color
                      </Typography>
                      <Typography variant="h6" color="textSecondary">
                        {props.values.assets.colors.primaryColor}
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.listItem} disableGutters divider>
                      <Typography variant="subtitle2" color="textPrimary">
                        Secondary Color
                      </Typography>
                      <Typography variant="h6" color="textSecondary">
                        {props.values.assets.colors.secondaryColor}
                      </Typography>
                    </ListItem>
                  </List>
                </CardContent>
              </Card>
            </Box>
            <Button
              color="primary"
              fullWidth
              type="submit"
              // loading={submitting}
              disabled={!props.dirty || !props.isValid}
            >
              Update
            </Button>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default WhiteLabel;
