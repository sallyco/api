import React from 'react';
import { Form } from '@packages/gbt-ui';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import { SnackbarUtils } from '@packages/wired-gbt-ui';
import useSWR from 'swr';
import makeStyles from '@mui/styles/makeStyles';

interface Props {
  title: string | undefined;
  item_name: any;
  items_name: string;
  setTabData: any;
  currentTabdata: any;
  previousTabData?: any;
  tenantId?: any;
  setTenantId?: any;
  setTabIndex?: any;
  companyId?: any;
  setCompanyId?: any;
}

const useStyles = makeStyles((theme) => ({
  button: {
    background: '#1976d2',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 40,
    padding: '0 30px',
  },
}));
export default function TenantCreateModal({
  title,
  item_name,
  items_name,
  setTabData,
  currentTabdata,
  previousTabData,
  tenantId = '',
  setTenantId,
  setTabIndex,
  companyId = '',
  setCompanyId,
}: Props) {
  const fetcher = (url) =>
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        for (const def in data.definitions) {
          for (const prop in data.definitions[def].properties) {
            const entry = data.definitions[def].properties[prop];
            const oType = data.definitions[def].properties[prop].type;
            if (entry.nullable && oType && typeof oType.valueOf() !== 'object') {
              data.definitions[def].properties[prop].type = [oType.valueOf(), 'null'];
            }
          }
        }
        data.title = title;
        return data;
      });

  const metadataUrl = baseUrl + `/metadata/${item_name}/create/describe`;
  const { data, error } = useSWR(metadataUrl, fetcher);
  const createurl = `${baseUrl}/${items_name}`;
  const [back, setback] = React.useState(false);
  const classes = useStyles();

  const loadData = () => {
    if (currentTabdata === null || currentTabdata === undefined) {
      setTabData(data);
    }
    return true;
  };

  const onSubmit = ({ formData }) => {
    if (item_name == 'tenant-tab-1') {
      setTabData(formData);
      setTabIndex(1);
    } else if (item_name === 'tenant-tab-2' && previousTabData !== null) {
      setTabData(formData);
      if (previousTabData.id === undefined) {
        SnackbarUtils.info('Please Submit tenant details.(Tab 1)');
      } else {
        const dataTenant = { ...formData, ...previousTabData };
        fetch(createurl, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(dataTenant),
        }).then(async (response) => {
          if (!response.ok) {
            SnackbarUtils.error('An error occured');
            throw response;
          } else {
            const data = await response.json();
            setTenantId(data.id);
            SnackbarUtils.success(`Tenant ${tenantId} Created.`);
            setTabIndex(2);
          }
        });
      }
    } else if (item_name === 'tenant-tab-3') {
      if (tenantId === '') {
        SnackbarUtils.info('Please Save Tenant details(Tab1 & Tab2)');
      } else {
        formData.tenantId = tenantId;
        setTabData(formData);
        fetch(createurl, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(formData),
        }).then(async (response) => {
          if (!response.ok) {
            SnackbarUtils.error('An error occured');
            throw response;
          } else {
            const data = await response.json();
            setCompanyId(data.id);
            SnackbarUtils.success(`Company created.`);
            setTabIndex(3);
          }
        });
      }
    } else {
      SnackbarUtils.error('An error occured');
    }
  };

  return (
    <>
      {loadData() && data?.definitions && (
        <Form
          schema={data}
          onSubmit={onSubmit}
          showErrorList={false}
          omitExtraData={true}
          liveValidate={false}
          formData={currentTabdata}
          disabled={
            (tenantId !== '' &&
              (item_name === 'tenant-tab-1' || item_name == 'tenant-tab-2') &&
              item_name !== 'tenant-tab- 3') ||
            (companyId !== '' && item_name === 'tenant-tab-3')
          }
        >
          <input
            value={
              item_name === 'tenant-tab-1'
                ? 'Next'
                : item_name == 'tenant-tab-3'
                ? 'Add Company'
                : 'Add Tenant'
            }
            className={classes.button}
            type="submit"
            color="primary"
            disabled={
              (tenantId !== '' &&
                (item_name === 'tenant-tab-1' || item_name == 'tenant-tab-2') &&
                item_name !== 'tenant-tab- 3') ||
              (companyId !== '' && item_name === 'tenant-tab-3')
            }
          />
        </Form>
      )}
    </>
  );
}
