import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import { Asset } from '../../api/assetsApi';
import numeral from 'numeral';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  assets: Asset[];
}

const AssetsTable = ({ assets }: Props) => {
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
      options: {
        filter: false,
      },
    },
    {
      name: 'type',
      label: 'Type',
    },
    {
      name: 'allocationAmount',
      label: 'Allocation Amount',
      options: {
        filter: false,
        customBodyRender: (value, tableMeta, updateValue) => numeral(value).format('$0,0'),
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    ownerId: 'string',
    tenantId: 'string',
    dealId: 'string',
    assetType: 'string',
    name: 'string',
    description: 'string',
    assetUrl: 'string',
    videoURL: 'string',
    title: 'string',
    isEntity: 'boolean',
    createdAt: 'date',
    updatedAt: 'date',
    deletedAt: 'date',
    isPublic: 'boolean',
    isDeleted: 'boolean',
    pithcDoc: 'string',
    appraisedValue: 'number',
    fractionalOwnershipAmount: 'number',
    category: 'number',
    size: 'string',
  };

  const tableService = new TableWithToolsService(
    'assets-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={assets}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`assets/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default AssetsTable;
