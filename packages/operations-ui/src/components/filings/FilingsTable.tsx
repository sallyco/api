import React, { useEffect, useState } from 'react';
import { TableWithTools } from '@packages/gbt-ui';
import { TableWithToolsService } from '../../services/TableWithToolsService';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import clsx from 'clsx';

interface FilingsTableProps {
  filingData: {
    id: any;
    listId: string;
    onClick: () => Promise<boolean>;
    footer: string;
    title: any;
    description: any;
    className: string;
  }[];
}

const ID_COLUMN_INDEX = 0;

const getStatusText = (status) => {
  let returnText = '';
  if (!status) {
    returnText = '';
  } else if (status === 'needsfiling') {
    returnText = 'Needs Filing';
  } else if (status === 'needscik') {
    returnText = 'Needs CIK';
  } else if (status === 'needsblue') {
    returnText = 'Needs Blue Sky';
  } else if (status === 'waitingclose') {
    returnText = 'Awaiting Close';
  } else {
    returnText = '';
  }

  return returnText;
};

const useStyles = makeStyles((theme: Theme) => ({
  warning: {
    backgroundColor: theme.palette.warning.light,
  },
  error: {
    backgroundColor: theme.palette.error.light,
  },
}));

const FilingsTable = ({ filingData }: FilingsTableProps) => {
  const classes = useStyles();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      label: 'ID',
      options: {
        display: false,
      },
    },
    {
      name: 'title',
      label: 'DEAL',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
    {
      name: 'description',
      label: 'FUND',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
    {
      name: 'listId',
      label: 'STATUS',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => getStatusText(value),
      },
    },
    {
      name: 'footer',
      label: 'DUE DATE',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    title: 'string',
    idescriptiond: 'string',
    listId: 'string',
    footer: 'string',
  };

  const tableService = new TableWithToolsService(
    'filings-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={filingData}
      columnDefinitions={columnDefinitions}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
      onRowClick={(rowData, rowMeta) => {
        const selectedItem = filingData.find((d) => d.id === rowData[ID_COLUMN_INDEX]);
        selectedItem.onClick();
      }}
      setRowProps={(rowData) => {
        const selectedItem = filingData.find((d) => d.id === rowData[ID_COLUMN_INDEX]);
        if (selectedItem.className !== '' && selectedItem.listId === 'needsblue') {
          if (selectedItem.className === 'error') {
            return {
              class: clsx(classes.error),
            };
          } else if (selectedItem.className === 'warning') {
            return {
              class: clsx(classes.warning),
            };
          }
        }
        return {
          class: '',
        };
      }}
    />
  );
};

export default FilingsTable;
