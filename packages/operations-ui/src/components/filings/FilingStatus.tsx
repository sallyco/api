import React, { useEffect } from 'react';
import { Theme } from '@mui/material/styles';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

import { Form } from '@packages/gbt-ui';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import useSWR from 'swr';
import { useRequest, doChange, metadataFetcher } from '@packages/wired-gbt-ui';

import { Backdrop, Container, CircularProgress } from '@mui/material';

interface Props {
  entityId: string | undefined;
  dealId: string | undefined;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  })
);

export default function FilingStatus({ entityId, dealId }: Props) {
  const classes = useStyles();

  const metadataUrl = baseUrl + '/metadata/form-d/create/describe';
  const { data, error } = useSWR(metadataUrl, metadataFetcher);

  const formDUrl = `/entities/${entityId}/form-d`;
  const dealUrl = `/deals/${dealId}`;
  const { data: fd, error: ferr, mutate } = useRequest(formDUrl, { silent: true });
  //
  const [loading, setLoading] = React.useState(false);
  const [formData, setFormData] = React.useState(null);
  useEffect(() => {
    setFormData(fd);
  }, [fd]);

  const onSubmit = ({ formData }) => {
    setLoading(true);
    doChange(formDUrl, !fd ? 'POST' : 'PATCH', formData, mutate);
    if (
      formData.filedDate &&
      formData.filedDate !== '' &&
      formData.blueSkyPaidDate &&
      formData.blueSkyPaidDate !== ''
    ) {
      doChange(dealUrl, 'PUT', { status: 'CLOSED' });
    }
    setLoading(false);
  };

  //const onClick = () => {
  //  const xUrl = baseUrl + `/entities/${entityId}/form-d/edgar-xml`;

  //  fetch(xUrl)
  //    .then((res) => res.blob())
  //    .then((blob) => {
  //      var file = window.URL.createObjectURL(blob);
  //      window.location.assign(file);
  //    });
  //};

  return (
    <Container>
      {data && (
        <Form
          schema={data}
          uiSchema={{
            'ui:options': { label: false },
            id: { 'ui:widget': 'hidden' },
            closeId: { 'ui:widget': 'hidden' },
            entitiesId: { 'ui:widget': 'hidden' },
          }}
          onSubmit={onSubmit}
          showErrorList={false}
          omitExtraData={true}
          formData={formData}
          onChange={(e) => setFormData(e.formData)}
        />
      )}
      <Backdrop className={classes.backdrop} open={loading} onClick={() => setLoading(false)}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Container>
  );
}

/**
      <Box my={2}>
        <Button fullWidth variant="contained" color="primary" onClick={onClick}>
          Download XML
        </Button>
      </Box>
**/
