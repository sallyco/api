import React, { useEffect, useState } from 'react';
import numeral from 'numeral';
import { TableWithTools } from '@packages/gbt-ui';
import { TableWithToolsService } from '../../services/TableWithToolsService';
import { useRequest } from '@packages/wired-gbt-ui';

const getProfileName = (profile): string => {
  if (!profile) {
    return '';
  }
  const { name, firstName, lastName, displayName } = profile;
  if (displayName) return `${displayName}`;
  if (name) return `${name}`;
  if (firstName && lastName) return `${firstName} ${lastName}`;
  if (lastName) return `${lastName}`;
  if (firstName) return `${firstName}`;
  return '';
};

const ClosedDealsTable = () => {
  const { data: closedDeals } = useRequest(`/closed-deals`);
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const getFeesSplit = (feeType, sections) => {
    try {
      if (feeType === 'BlueSky') {
        const regulatoryFeesSection = sections.filter((s) => s.title === 'Regulatory Filings Fees');
        if (regulatoryFeesSection?.length) {
          const blueSkyFees = regulatoryFeesSection[0].entries.filter((e) =>
            e.description.includes('Blue Sky')
          );
          return blueSkyFees?.length
            ? Number((blueSkyFees[0].expenses ?? '0').replace(/[^\d.]/g, ''))
            : 0;
        }
      } else if (feeType === 'RegD') {
        const regulatoryFeesSection = sections.filter((s) => s.title === 'Regulatory Filings Fees');
        if (regulatoryFeesSection?.length) {
          const regDFees = regulatoryFeesSection[0].entries.filter((e) =>
            e.description.includes('NASAA EFD System Fee')
          );
          return regDFees?.length
            ? Number((regDFees[0].expenses ?? '0').replace(/[^\d.]/g, ''))
            : 0;
        }
      }
      return 0;
    } catch (e) {
      return 0;
    }
  };

  const initialColumnDefinitions = [
    {
      name: 'close.id',
      label: 'CLOSE ID',
      options: {
        display: false,
      },
    },
    {
      name: 'close.dealId',
      label: 'DEAL ID',
      options: {
        display: false,
      },
    },
    {
      name: 'close.tenantId',
      label: 'TENANT',
    },
    {
      name: 'close.entity.deal.profile',
      label: 'ORGANIZER',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => getProfileName(value),
      },
    },
    {
      name: 'close.entity.deal.name',
      label: 'DEAL',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
    {
      name: 'close.entity.name',
      label: 'FUND NAME',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
    {
      name: 'close.entity.deal.status',
      label: 'STATUS',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => value ?? '',
      },
    },
    {
      name: 'close.statement.sections',
      label: 'REG-D FEES',
      options: {
        customBodyRender: (value, tableMeta, updateValue) =>
          numeral(getFeesSplit('RegD', value)).format('$0,0'),
      },
    },
    {
      name: 'close.statement.sections',
      label: 'BLUESKY FEES',
      options: {
        customBodyRender: (value, tableMeta, updateValue) =>
          numeral(getFeesSplit('BlueSky', value)).format('$0,0'),
      },
    },
    {
      name: 'close.statement.sections',
      label: 'OTHER FEES',
      options: {
        customBodyRender: (value, tableMeta, updateValue) => numeral(0).format('$0,0'),
      },
    },
  ];

  const fieldMap: any = {
    'close.id': 'string',
    'close.dealId': 'string',
    'close.tenantId': 'string',
    'close.entity.deal.profile': 'string',
    'close.entity.deal.name': 'string',
    'close.entity.name': 'string',
    'close.entity.deal.status': 'string',
    'close.entity.deal.updatedAt': 'date',
    'close.statement.sections': 'string',
  };

  const tableService = new TableWithToolsService(
    'filings-closed-deals-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={closedDeals}
      columnDefinitions={columnDefinitions}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default ClosedDealsTable;
