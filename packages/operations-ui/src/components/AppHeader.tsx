import React, { useContext } from 'react';
import { styled } from '@mui/material/styles';

import Image from 'next/image';
import About from './About';
import Link from 'next/link';
import Avatar from 'react-avatar';

import { TenantsContext, UserContext } from '../contexts';
import { AngleDownIcon } from 'react-line-awesome';

import {
  Box,
  Divider,
  Typography,
  Menu,
  MenuItem,
  Toolbar,
  useTheme,
  IconButton,
} from '@mui/material';

import SearchDrawer from './search/SearchDrawer';
import { Notifications } from '@packages/gbt-ui';

const PREFIX = 'AppHeader';

const classes = {
  toolbar: `${PREFIX}-toolbar`,
  flexCenter: `${PREFIX}-flexCenter`,
  button: `${PREFIX}-button`,
  angleDown: `${PREFIX}-angleDown`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.toolbar}`]: {
    minHeight: '48px',
    justifyContent: 'space-between',
    borderBottom: '1px solid rgba(0, 0, 0, 0.18)',
  },

  [`& .${classes.flexCenter}`]: {
    display: 'flex',
    alignItems: 'center',
  },

  [`& .${classes.button}`]: {
    padding: 10,
    [theme.breakpoints.down('sm')]: {
      padding: 6,
    },
  },

  [`& .${classes.angleDown}`]: {
    fontSize: '16px',
    padding: '4px',
  },
}));

const AppHeader = ({ sideBarTrigger }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const tenant: any = useContext(TenantsContext);
  const user: any = useContext(UserContext);
  const isDarkTheme = useTheme().palette.mode === 'dark';

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Root>
      <Toolbar className={classes.toolbar}>
        <Box className={classes.flexCenter}>
          {React.cloneElement(sideBarTrigger, {
            className: classes.button,
          })}
          <Link href="/dashboard" passHref>
            <React.Fragment>
              {tenant?.assets?.images ? (
                <Image
                  alt={tenant?.name}
                  src={
                    isDarkTheme
                      ? tenant?.assets?.images?.logoInverted
                      : tenant?.assets?.images?.logo
                  }
                  width={200}
                  height={48}
                  objectFit="contain"
                />
              ) : (
                <Typography variant="h5" component="h2">
                  {tenant?.name}
                </Typography>
              )}
            </React.Fragment>
          </Link>
        </Box>
        <Box className={classes.flexCenter} alignItems={'flex-end'}>
          <SearchDrawer />
          <Notifications />
          <IconButton onClick={handleClick}>
            <Avatar
              name={user?.family_name ? `${user?.given_name} ${user?.family_name}` : user?.username}
              email={user?.email}
              size="30px"
              round={true}
            />
            <AngleDownIcon className={classes.angleDown} />
          </IconButton>
          <Menu
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            id="profile-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>
              <Link href="/logout" passHref>
                <span>Logout</span>
              </Link>
            </MenuItem>
            <Divider />
            <About />
          </Menu>
        </Box>
      </Toolbar>
    </Root>
  );
};

export default AppHeader;
