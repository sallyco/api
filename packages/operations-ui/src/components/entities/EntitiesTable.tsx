import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import makeStyles from '@mui/styles/makeStyles';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  entities: any[];
  isValidating: boolean;
}

const useStyles = makeStyles({
  '@global': {
    '.MuiDataGrid-root': {
      border: 0,
    },
  },
  root: {},
});

const EntitiesTable = ({ entities, isValidating }: Props) => {
  const classes = useStyles();
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
      options: {
        filter: false,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
    },
  ];

  const fieldMap: any = {
    id: 'string',
    dealId: 'string',
    ownerId: 'string',
    tenantId: 'string',
    name: 'string',
    entityType: 'string',
    countryOfFormation: 'string',
    stateOfFormation: 'string',
    regulationType: 'string',
    ein: 'string',
    createdAt: 'date',
    updatedAt: 'date',
    bankingNodeId: 'string',
  };

  const tableService = new TableWithToolsService(
    'entities-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={entities}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`entities/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable={true}
      showLoading={true}
      isLoading={isValidating}
    />
  );
};

export default EntitiesTable;
