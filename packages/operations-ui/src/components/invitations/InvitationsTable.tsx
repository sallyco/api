import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import makeStyles from '@mui/styles/makeStyles';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  invitations: any[];
}

const useStyles = makeStyles({
  '@global': {
    '.MuiDataGrid-root': {
      border: 0,
    },
  },
  root: {},
});

const InvitationsTable = ({ invitations }: Props) => {
  const classes = useStyles();
  const router = useRouter();

  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
    },
    {
      name: 'email',
      label: 'E-mail',
    },
    {
      name: 'tenantId',
      label: 'Tenant',
    },
  ];

  const fieldMap: any = {};

  const tableService = new TableWithToolsService(
    'invitations-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={invitations}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`invitations/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
    />
  );
};

export default InvitationsTable;
