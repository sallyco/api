import React, { useContext } from 'react';
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { File } from '../../api/filesApi';

import Head from 'next/head';

import { useUiStore } from '../../store';
import { TenantsContext } from '../../contexts';

import { Root, Header, EdgeTrigger, Content } from '@mui-treasury/layout';

//const DrawerSidebar = getDrawerSidebar(styled);
//const SidebarContent = getSidebarContent(styled);
//const CollapseBtn = getCollapseBtn(styled);

import AppFileViewHeader from '../AppFileViewHeader';

const useStyles = makeStyles((theme: Theme) => ({
  header: {
    backgroundColor: theme.palette.background.paper,
  },
  content: {
    flex: '1 0 auto',
    height: '100vh',
  },
}));

interface Props {
  children: any;
  title: string;
  file: File | undefined;
}

export default function LoggedInFileViewLayout({
  children,
  title = 'Glassboard Technology',
  file = undefined,
}: Props) {
  const theme = useTheme();
  const styles = useStyles();

  const collapsed = useUiStore((state) => state.collapsed);
  const toggleCollapsed = useUiStore((state) => state.toggleCollapsed);

  const tenant: any = useContext(TenantsContext);

  return (
    <React.Fragment>
      <Head>
        <title>{`${title} | ${tenant.name}`}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Root
        scheme={{
          header: {
            config: {
              xs: {
                position: 'sticky',
                height: 48,
              },
              sm: {
                position: 'fixed',
                clipped: true,
                height: 48,
              },
            },
          },
          leftEdgeSidebar: {
            autoCollapse: 'sm',
            config: {
              xs: {
                variant: 'temporary',
                width: 200,
              },
              sm: {
                variant: 'permanent',
                width: 200,
                collapsible: true,
                collapsedWidth: 66,
                headerMagnetEnabled: true,
              },
            },
          },
        }}
      >
        <Header className={styles.header}>
          <AppFileViewHeader
            sideBarTrigger={
              <EdgeTrigger target={{ anchor: 'left', field: 'collapsed' }}>
                {(collapsed, setCollapsed) => (
                  <button onClick={() => setCollapsed(!collapsed)}>
                    {collapsed ? 'Expand' : 'Collapse'}
                  </button>
                )}
              </EdgeTrigger>
            }
            file={file}
          />
        </Header>

        <Content className={styles.content}>{children}</Content>
      </Root>
    </React.Fragment>
  );
}
/**
        <DrawerSidebar sidebarId="primaryNav">
          <SidebarContent>
            <NavSideBar />
          </SidebarContent>
          <CollapseBtn onClick={toggleCollapsed} />
        </DrawerSidebar>
**/
