import React, { useContext } from 'react';
import { styled } from '@mui/material/styles';
import { Box, ButtonBase, IconButton } from '@mui/material';
import { CopyrightFooter } from '@packages/gbt-ui';
import Menu from '@mui/icons-material/Menu';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';

import Head from 'next/head';

import { useUiStore } from '../../store';
import { TenantsContext } from '../../contexts';

import {
  Root,
  Header,
  EdgeTrigger,
  Content,
  Footer,
  EdgeSidebar,
  SidebarContent,
} from '@mui-treasury/layout';

import AppHeader from '../AppHeader';
import NavSideBar from '../navigation/NavSideBar';

const PREFIX = 'LoggedInLayout';

const classes = {
  header: `${PREFIX}-header`,
  contentWrapper: `${PREFIX}-contentWrapper`,
  content: `${PREFIX}-content`,
  footer: `${PREFIX}-footer`,
};

const LayoutRoot = styled('div')(({ theme }) => ({
  [`& .${classes.header}`]: {
    backgroundColor: theme.palette.background.paper,
  },

  [`& .${classes.contentWrapper}`]: {
    height: 'calc(100vh - 48px)',
    display: 'flex',
    flexDirection: 'column',
  },

  [`& .${classes.content}`]: {
    flex: '1 0 auto',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },

  [`& .${classes.footer}`]: {
    flexShrink: 1,
    textAlign: 'center',
  },
}));

interface Props {
  children: any;
  title: string;
}

export default function LoggedInLayout({ children, title = 'Glassboard Technology' }: Props) {
  const collapsed = useUiStore((state) => state.collapsed);
  const toggleCollapsed = useUiStore((state) => state.toggleCollapsed);

  const tenant: any = useContext(TenantsContext);

  return (
    <LayoutRoot>
      <Head>
        <title>{`${title} | ${tenant.name}`}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Root
        scheme={{
          header: {
            config: {
              xs: {
                position: 'sticky',
                height: '48px',
              },
              sm: {
                position: 'fixed',
                clipped: true,
                height: '48px',
              },
            },
          },
          leftEdgeSidebar: {
            autoCollapse: 'sm',
            config: {
              xs: {
                variant: 'temporary',
                width: 200,
              },
              sm: {
                variant: 'permanent',
                width: 200,
                collapsible: true,
                collapsedWidth: 66,
                headerMagnetEnabled: true,
              },
            },
          },
        }}
        initialState={{
          leftEdgeSidebar: {
            collapsed: collapsed,
          },
        }}
      >
        <Header className={classes.header}>
          <AppHeader
            sideBarTrigger={
              <EdgeTrigger target={{ anchor: 'left', field: 'open' }}>
                {(open, setOpen) => (
                  <IconButton onClick={() => setOpen(!open)} edge="end">
                    {open ? <KeyboardArrowLeft /> : <Menu />}
                  </IconButton>
                )}
              </EdgeTrigger>
            }
          />
        </Header>

        <EdgeSidebar anchor="left">
          <SidebarContent>
            <NavSideBar />
          </SidebarContent>
          <EdgeTrigger target={{ anchor: 'left', field: 'collapsed' }}>
            {(c, setCollapsed) => (
              <ButtonBase
                onClick={() => {
                  setCollapsed(!c);
                  toggleCollapsed();
                }}
                sx={{ flexGrow: 1, height: 48 }}
              >
                {c ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              </ButtonBase>
            )}
          </EdgeTrigger>
        </EdgeSidebar>

        <div className={classes.contentWrapper}>
          <Content className={classes.content}>{children}</Content>

          <Footer className={classes.footer}>
            <Box pt={3} px={1} pb={2}>
              <CopyrightFooter />
            </Box>
          </Footer>
        </div>
      </Root>
    </LayoutRoot>
  );
}
