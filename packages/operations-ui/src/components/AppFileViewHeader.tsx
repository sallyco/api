import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { File } from '../api/filesApi';

import Link from 'next/link';

import { TenantsContext } from '../contexts/TenantsContext';

import { Box, Button, Theme, useTheme, Toolbar, Typography } from '@mui/material';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

import { FileDownloadIcon, ArrowLeftIcon } from 'react-line-awesome';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    logo: {
      height: 26,
      [theme.breakpoints.down('sm')]: {
        height: 18,
      },
    },
    toolbar: {
      minHeight: '48px !important',
      justifyContent: 'space-between',
      borderBottom: '1px solid rgba(0, 0, 0, 0.18)',
    },
    flexCenter: {
      display: 'flex',
      alignItems: 'center',
    },
    button: {
      padding: 10,
      [theme.breakpoints.down('sm')]: {
        padding: 6,
      },
    },
    simpleLink: {
      textDecoration: 'none',
    },
  })
);

interface Props {
  sideBarTrigger: any;
  file: File | undefined;
}

const AppFileViewHeader = ({ sideBarTrigger, file = undefined }: Props) => {
  const styles = useStyles();
  const router = useRouter();
  //const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const tenant: any = useContext(TenantsContext);
  const isDarkTheme = useTheme().palette.mode === 'dark';

  //const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //  setAnchorEl(event.currentTarget);
  //};

  //const handleClose = () => {
  //  setAnchorEl(null);
  //};

  return (
    <Toolbar className={styles.toolbar}>
      <Box className={styles.flexCenter}>
        {React.cloneElement(sideBarTrigger, {
          className: styles.button,
        })}
        <Link href="/dashboard" passHref>
          <img
            className={styles.logo}
            alt=""
            src={isDarkTheme ? tenant?.assets?.images?.logoInverted : tenant?.assets?.images?.logo}
          />
        </Link>
        <Box ml={2}>
          <Button
            color="secondary"
            size="small"
            variant="contained"
            startIcon={<ArrowLeftIcon />}
            onClick={() => router.back()}
          >
            Back
          </Button>
        </Box>
        <Box mx={1}>
          <Button color="primary" size="small" variant="contained" startIcon={<FileDownloadIcon />}>
            Download
          </Button>
        </Box>
      </Box>
      <Box className={styles.flexCenter} alignItems={'flex-end'}>
        <Typography variant="h3" component="h1">
          {file?.name}
        </Typography>
      </Box>
    </Toolbar>
  );
};

export default AppFileViewHeader;
