/**
 * This file will eventually be replaced by
 * a server-side call to generate a naviation menu
 * based on role and other permissions.  For now,
 * we can control things based on user
 **/
import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { useSidebarCtx, useLayoutCtx } from '@mui-treasury/layout';
import { TenantsContext } from '../../contexts/TenantsContext';

import { Box, List, Divider } from '@mui/material';
import NavSideBarItem from './NavSideBarItem';

import { objectProps as op } from '@packages/wired-gbt-ui';

const NavSideBarItems = ({ onItemClick = null }: { onItemClick?: any }) => {
  const { expanded } = useSidebarCtx();
  const {
    state: { leftEdgeSidebar },
  } = useLayoutCtx();
  const collapsed = !expanded && leftEdgeSidebar.collapsed && !leftEdgeSidebar.open;
  const router = useRouter();
  const tenant: any = useContext(TenantsContext);

  const commonProps = (route) => ({
    route,
    collapsed,
    selected: router.pathname.startsWith(`/${route}`),
    onClick: () => {},
    dotOnCollapsed: true,
  });

  const navEntry = (props) => (
    <NavSideBarItem
      key={props.name}
      label={props.label}
      startIcon={props.icon}
      {...commonProps(props.name)}
    />
  );

  return (
    <List>
      {tenant.isMaster && ['tenants'].map((entry) => navEntry(op[entry]))}
      {['users'].map((entry) => navEntry(op[entry]))}
      <Box m={1}>
        <Divider />
      </Box>
      {[
        'deals',
        'entities',
        'profiles',
        'subscriptions',
        'closes',
        'assets',
        'bank-accounts',
        'managers',
        'master-entities',
        'invitations',
        'fundDocuments',
      ].map((entry) => navEntry(op[entry]))}
      <Box m={1}>
        <Divider />
      </Box>
      {['kyc-aml', 'financial-transactions', 'bank-reconciliation', 'transfer-approvals'].map(
        (entry) => navEntry(op[entry])
      )}
      <Box m={1}>
        <Divider />
      </Box>
      {['approvals', 'signing', 'filings'].map((entry) => navEntry(op[entry]))}
      <Box m={1}>
        <Divider />
      </Box>
      {tenant.id === 'master' && (
        <React.Fragment>
          {['decisions', 'workflows', 'globals'].map((entry) => navEntry(op[entry]))}
          <Box m={1}>
            <Divider />
          </Box>
          {['questions', 'products', 'data-import'].map((entry) => navEntry(op[entry]))}
          <Box m={1}>
            <Divider />
          </Box>
          {['emails'].map((entry) => navEntry(op[entry]))}
        </React.Fragment>
      )}
      {['system'].map((entry) => navEntry(op[entry]))}
    </List>
  );
};

export default NavSideBarItems;
