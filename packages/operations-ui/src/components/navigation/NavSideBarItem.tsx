import React from 'react';
import { styled } from '@mui/material/styles';
import cx from 'clsx';
import Link from 'next/link';
import ButtonBase, { ButtonBaseProps } from '@mui/material/ButtonBase';
import Tooltip from '@mui/material/Tooltip';
import Color from 'color';

const PREFIX = 'NavSideBarItem';

const classes = {
  root: `${PREFIX}-root`,
  selected: `${PREFIX}-selected`,
  collapsed: `${PREFIX}-collapsed`,
  dot: `${PREFIX}-dot`,
  focusVisible: `${PREFIX}-focusVisible`,
  startIcon: `${PREFIX}-startIcon`,
  label: `${PREFIX}-label`,
  value: `${PREFIX}-value`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-start',
    minHeight: 26,
    transition: '0.2s',
    webkitFontSmoothing: 'antialiased',
    padding: '0 12px 0 26px',
    overflowX: 'hidden',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    [`& .${classes.startIcon}`]: {
      display: 'inline-flex',
      opacity: 0.72,
      marginRight: 18,
      '& > *': {
        fontSize: 20,
      },
    },
    [`& .${classes.label}`]: {
      fontSize: 12,
    },
    [`& .${classes.value}`]: {
      fontSize: '0.75rem',
      letterSpacing: 0.3,
      marginLeft: 'auto',
      paddingLeft: 16,
    },
  },

  [`& .${classes.selected}`]: {
    fontWeight: 'bold' as const,
    color: theme.palette.mode === 'dark' ? '#1D3557' : '#EFEFEF',
    backgroundColor: `${Color(
      theme.palette.mode === 'dark' ? theme.palette.primary.light : theme.palette.primary.dark
    )
      .saturate(0.5)
      .lighten(0.2)
      .toString()} !important`,
    [`& .${classes.startIcon}`]: {
      opacity: 1,
    },
  },

  [`& .${classes.collapsed}`]: {
    padding: '0 6px',
    width: 32,
    borderRadius: 20,
    marginLeft: 16,
    [`& .${classes.label}`]: {
      display: 'none',
    },
    [`& .${classes.value}`]: {
      position: 'absolute' as const,
      top: 0,
      right: -8,
      backgroundColor: '#1D3557',
      color: '#fff',
      padding: '0 4px',
      borderRadius: 10,
      fontWeight: 'normal',
    },
  },

  [`& .${classes.dot}`]: {
    [`& .${classes.value}`]: {
      display: 'none',
    },
    '&:after': {
      display: 'block',
      content: '""',
      position: 'absolute',
      width: 8,
      height: 8,
      backgroundColor: '#da3125',
      top: 4,
      right: 4,
      borderRadius: '50%',
    },
  },

  [`& .${classes.focusVisible}`]: {},
}));

export type NavSideBarItemProps = {
  route: string;
  startIcon: React.ReactElement;
  label: React.ReactNode;
  value?: React.ReactNode;
  collapsed?: boolean;
  dotOnCollapsed?: boolean;
  selected?: boolean;
  onClick: ButtonBaseProps['onClick'];
  className?: string;
  ButtonBaseProps?: ButtonBaseProps;
};

export type NavSideBarClassKey = 'root' | 'selected' | 'focusVisible' | 'collapsed' | 'dot';

const NavSideBarItem = ({
  route,
  startIcon,
  label,
  value,
  ButtonBaseProps = {},
  collapsed,
  dotOnCollapsed,
  selected,
  onClick,
}: NavSideBarItemProps) => {
  return (
    <Root>
      <Link href={`/${route}`} passHref>
        <Tooltip title={collapsed ? label : ''} enterDelay={500}>
          <ButtonBase
            onClick={onClick}
            classes={{
              ...ButtonBaseProps.classes,
              root: cx(classes.root, {
                [classes.selected]: selected,
                [classes.collapsed]: collapsed,
                [classes.dot]: collapsed && dotOnCollapsed && value,
              }),
              focusVisible: classes.focusVisible,
            }}
          >
            <div className={classes.startIcon}>{startIcon}</div>
            <div className={classes.label}>{label}</div>
            {value && <div className={classes.value}>{value}</div>}
          </ButtonBase>
        </Tooltip>
      </Link>
    </Root>
  );
};

export default NavSideBarItem;
