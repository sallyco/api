import React from 'react';
import Box from '@mui/material/Box';
import { SidebarContent } from '@mui-treasury/layout';

import NavSideBarItems from './NavSideBarItems';

const NavSideBar = () => {
  return (
    <Box display={'flex'} flexGrow={1} flexDirection={'column'}>
      <SidebarContent>
        <Box maxWidth={240}>
          <NavSideBarItems />
        </Box>
      </SidebarContent>
    </Box>
  );
};

export default NavSideBar;
