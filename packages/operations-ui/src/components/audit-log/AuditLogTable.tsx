import React from 'react';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { ContentLoader, TableWithTools, Timestamp } from '@packages/gbt-ui';
import { Chip, Link, TableRow, TableCell, Typography } from '@mui/material';

import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SwapVerticalCircleOutlinedIcon from '@mui/icons-material/SwapVerticalCircleOutlined';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';

import { PlusIcon, MinusIcon } from 'react-line-awesome';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import ReactDiffViewer, { DiffMethod } from 'react-diff-viewer';
import { useUiStore } from '../../store';
import { diffLines } from 'diff';
import _ from 'lodash';

interface Props {
  id: string;
}

const LogColors = {
  INSERT_ONE: {
    icon: <AddCircleOutlineIcon />,
    text: 'CREATE ONE',
    color: 'primary',
  },
  INSERT_MANY: {
    icon: <AddCircleOutlineIcon />,
    text: 'CREATE MANY',
    color: 'primary',
  },
  UPDATE_ONE: {
    icon: <SwapVerticalCircleOutlinedIcon />,
    text: 'UPDATE ONE',
    color: 'secondary',
  },
  UPDATE_MANY: {
    icon: <SwapVerticalCircleOutlinedIcon />,
    text: 'UPDATE MANY',
    color: 'secondary',
  },
  DELETE_ONE: {
    icon: <RemoveCircleOutlineIcon />,
    text: 'DELETE ONE',
    color: 'default',
  },
  DELETE_MANY: {
    icon: <RemoveCircleOutlineIcon />,
    text: 'DELETE MANY',
    color: 'default',
  },
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  plus: {
    color: theme.palette.success.main,
    fontSize: 18,
  },
  minus: {
    color: theme.palette.error.main,
    fontSize: 18,
  },
}));

const AuditLogTable = ({ id }: Props) => {
  const classes = useStyles();
  const op = objectProps['audit-logs'];
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      where: {
        entityId: id,
      },
      include: [
        {
          relation: 'actionActor',
          scope: {
            fields: ['id', 'firstName', 'lastName', 'username'],
          },
        },
      ],
    })
  );
  const { data, error } = useRequest(`${op.route}?filter=${encodedFilter}`);
  const useDarkTheme = useUiStore((state) => state.darkTheme);

  return (
    <React.Fragment>
      {!data ? (
        <ContentLoader errorMessage={error?.message} />
      ) : (
        <TableWithTools
          data={data}
          columnDefinitions={[
            {
              name: 'id',
              options: {
                display: false,
                filter: false,
                sort: false,
              },
            },
            {
              name: 'action',
              label: 'Action',
              options: {
                sort: false,
                setCellProps: () => ({
                  style: {
                    width: '150px',
                    maxWidth: '150px',
                  },
                }),
                customBodyRender: function fn(value, tableMeta, updateValue) {
                  return (
                    <Chip
                      icon={LogColors[value].icon}
                      label={LogColors[value].text}
                      color={LogColors[value].color}
                      variant="outlined"
                    />
                  );
                },
              },
            },
            {
              name: 'actionActor',
              label: 'Actor',
              options: {
                sort: false,
                customBodyRender: function fn(value, tableMeta, updateValue) {
                  return (
                    <React.Fragment>
                      <Typography variant="h4">
                        <Link href={`/users/${value.id}`} variant="h4">
                          {value.firstName} {value.lastName}
                        </Link>
                      </Typography>
                      <Typography variant="caption">({value.username})</Typography>
                    </React.Fragment>
                  );
                },
              },
            },
            {
              name: 'difference',
              label: 'Changes',
              options: {
                sort: false,
                customBodyRender: function fn(value, tableMeta, updateValue) {
                  const entry = _.find(data, { id: tableMeta.rowData[0] });
                  if (!entry.before || !entry.after) {
                    return ``;
                  }
                  const diff = diffLines(
                    JSON.stringify(entry.before, null, 4),
                    JSON.stringify(entry.after, null, 4)
                  );
                  const ds = diff
                    .map<React.ReactNode>((e) => {
                      if (e.added) {
                        return <PlusIcon className={classes.plus} />;
                      } else if (e.removed) {
                        return <MinusIcon className={classes.minus} />;
                      }
                      return <></>;
                    })
                    .reduce((prev, curr) => [prev, '', curr]);

                  return ds;
                },
              },
            },
            {
              name: 'actedAt',
              label: 'Timestamp',
              options: {
                setCellProps: () => ({
                  style: {
                    width: '220px',
                    maxWidth: '220px',
                  },
                }),
                customBodyRender: function fn(value, tableMeta, updateValue) {
                  return <Timestamp value={value} />;
                },
              },
            },
            {
              name: 'actedOn',
              label: 'Acted On',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
            {
              name: 'tenantId',
              label: 'Tenant Id',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
            {
              name: 'entityId',
              label: 'Target Id',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
            {
              name: 'actionKey',
              label: 'Action Key',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
            {
              name: 'actedOn',
              label: 'Acted On',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
            {
              name: 'actor',
              label: 'User Id',
              options: {
                display: false,
                sort: false,
                filter: false,
              },
            },
          ]}
          elevation={0}
          selectable={false}
          clickable
          expandableRows={true}
          expandableRowsHeader={false}
          expandableRowsOnClick={true}
          renderExpandableRow={(rowData, rowMeta) => {
            const colSpan = rowData.length + 1;
            const entry = _.find(data, { id: rowData[0] });
            const newstyles = {
              line: {
                pre: {
                  lineHeight: '1.25 !important',
                },
              },
            };
            return (
              <TableRow>
                <TableCell colSpan={colSpan}>
                  <ReactDiffViewer
                    styles={newstyles}
                    oldValue={JSON.stringify(entry?.before, null, 4)}
                    newValue={JSON.stringify(entry?.after, null, 4)}
                    splitView={true}
                    showDiffOnly={false}
                    hideLineNumbers={true}
                    compareMethod={DiffMethod.LINES}
                    useDarkTheme={useDarkTheme}
                  />
                </TableCell>
              </TableRow>
            );
          }}
        />
      )}
    </React.Fragment>
  );
};

export default AuditLogTable;
