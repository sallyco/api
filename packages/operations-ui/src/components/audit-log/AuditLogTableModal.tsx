import React from 'react';
import { FullScreenDialog } from '@packages/gbt-ui';

import AuditLogTable from './AuditLogTable';

import { Container } from '@mui/material';

interface Props {
  id: string | undefined;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function AuditLogTableModal({ id = undefined, open, setOpen }: Props) {
  return (
    <FullScreenDialog title={'Audit Log'} open={open} setOpen={setOpen}>
      <Container maxWidth={false}>{id && <AuditLogTable id={id} />}</Container>
    </FullScreenDialog>
  );
}
