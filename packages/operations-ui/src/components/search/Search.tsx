import React, { useState } from 'react';
import {
  OutlinedInput,
  FormControl,
  InputAdornment,
  Box,
  Typography,
  CircularProgress,
} from '@mui/material';
import { Alert } from '@mui/material';
import Link from 'next/link';

import { SearchIcon } from 'react-line-awesome';
import useSWR from 'swr';

export interface SearchResult {
  id: string;
  name: Array<string>;
}

export interface SearchResults {
  numFound: number;
  start: number;
  numFoundExact: boolean;
  docs: Array<SearchResult>;
}

const baseUrl = process.env.NEXT_PUBLIC_ADMIN_URL;

const Search = () => {
  const [query, setQuery] = useState<string | undefined>(undefined);
  const [isLoading, setLoading] = useState<boolean>(false);
  const [results, setResults] = useState<SearchResult[]>([]);

  const url = `${baseUrl}/search?q=${query}`;
  const { data, error, isValidating } = useSWR(query ? url : null);

  return (
    <React.Fragment>
      <FormControl fullWidth variant="outlined">
        <OutlinedInput
          id="outlined-adornment-search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
        />
      </FormControl>

      <Box mt={4}>
        {isValidating ? (
          <Box display="flex" justifyContent="center">
            <CircularProgress />
          </Box>
        ) : (
          <React.Fragment>
            {data?.numFound > 0 ? (
              <div style={{ cursor: 'pointer' }}>
                {data.docs.map((result, i) => (
                  <Link key={result.id} href={`/deals/${result.id}`} passHref>
                    <Box key={i} mb={2}>
                      <Typography variant="h4" color="textPrimary">
                        {result.name}
                      </Typography>
                      <Typography variant="body2" color="textPrimary">
                        <strong>Deal:</strong> {result.id}
                      </Typography>
                    </Box>
                  </Link>
                ))}
              </div>
            ) : (
              <React.Fragment>
                {query !== '' && query !== undefined && (
                  <Alert severity="info">No results found</Alert>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
      </Box>
    </React.Fragment>
  );
};

export default Search;
