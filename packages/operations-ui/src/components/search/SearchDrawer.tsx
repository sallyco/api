import React from 'react';

import { SlideDrawer } from '@packages/gbt-ui';
import Search from './Search';

const SearchDrawer: React.FC = () => {
  return (
    <SlideDrawer title="Search">
      <Search />
    </SlideDrawer>
  );
};

export default SearchDrawer;
