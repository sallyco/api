import React, { useEffect } from 'react';
import 'grapesjs/dist/css/grapes.min.css';
import grapesjs from 'grapesjs';
// import Component from 'grapesjs/src/dom_components/model/Component';
// import ComponentView from 'grapesjs/src/dom_components/view/ComponentView';
import grapesjsMJML from 'grapesjs-mjml';

const MJMLEditor = () => {
  useEffect(() => {
    const editor = grapesjs.init({
      container: '#email-editor',
      fromElement: true,
      avoidInlineStyle: false,
      plugins: [grapesjsMJML],
      pluginsOpts: {
        [grapesjsMJML]: {},
      },
    }) as grapesjsMJML;
    const blockManager = editor.BlockManager;
    const components = editor.Components;

    // Only inject initial components if no edits present
    if (editor.getComponents().length < 1) {
      // Initialize content for React div
      editor.setComponents(
        `<mjml>
              <mj-body>
                <mj-section>
                  <mj-column>
                   <mj-text>Edit This</mj-text>
                  </mj-column>
                </mj-section>
              </mj-body>
           </mjml>`
      );
    }

    // console.log(editor);
    //
    // const TextComponent = editor.DomComponents.getType('text');

    // components.addType('handlebars', {
    //   extend: 'text',
    // model: TextComponent.model.extend({
    //   defaults: {
    //     ...TextComponent.model.prototype.defaults,
    //     droppable: true,
    //     editable: true
    //   },
    //   init() {
    //     // If I have no components inside, I compile the template
    //     // and it will be an editable content
    //     if (!this.components().length) {
    //       this.components(Handlebars.compile(this.get('template')));
    //     }
    //   },
    //   toHTML() {
    //     // If I have components I'll follow the original method
    //     // otherwise return the template
    //     if (this.components().length) {
    //       return TextComponent.model.prototype.toHTML.apply(this, arguments);
    //     } else {
    //       return this.get('template');
    //     }
    //   }
    // }, {
    //   // isComponent is mandatory when you define new components
    //   isComponent(el) {
    //     console.log(el);
    //     // Accept it only when `handlebars` type is explicitly requested
    //     return true;
    //   }
    // }),
    // view: TextComponent.view
    // });

    // var comps = editor.DomComponents;
    //
    // // Get the model and the view from the default Component type
    // var defaultType = comps.getType('text');
    // var defaultModel = defaultType.model;
    // var defaultView = defaultType.view;

    // comps.addType('handlebars', {
    //   view: defaultType.view.extend({
    //     // The render() should return 'this'
    //     render: function () {
    //       // Extend the original render method
    //       console.log(this);
    //       defaultType.view.prototype.render.apply(this, arguments);
    //       const template = Handlebars.compile(this.el.innerHTML);
    //       this.el.innerHTML = template(this);
    //       return this
    //     },
    //   }),
    // });

    // comps.addType('handlebars', {
    //   model: defaultModel.extend({
    //     // defaults: {
    //     //   ...defaultModel.prototype.defaults,
    //     //   droppable: true,
    //     //   editable: true
    //     // },
    //     init() {
    //       // If I have no components inside, I compile the template
    //       // and it will be an editable content
    //       if (!this.components().length) {
    //         this.components(Handlebars.compile(this.get('template')));
    //       }
    //     },
    //     toHTML() {
    //       // If I have components I'll follow the original method
    //       // otherwise return the template
    //       if (this.components().length) {
    //         debugger;
    //         return defaultModel.prototype.toHTML.apply(this, arguments);
    //       } else {
    //         debugger;
    //         return this.get('template');
    //       }
    //     }
    //   }, {
    //     // isComponent is mandatory when you define new components
    //     isComponent(el) {
    //       // Accept it only when `handlebars` type is explicitly requested
    //       return /{{{?(#[a-z]+ )?[a-z]+.[a-z]*}?}}/.test(el?.innerText);
    //     }
    //   }),
    //   view: defaultType.view
    // });
    //
    // blockManager.add('handlebars', {
    //   label: 'First Name',
    //   content: 'John',
    //   template: '<mj-raw>{{firstName}}</mj-raw>',
    //   type: 'handlebars'
    // })

    // editor.DomComponents.addType('handlebars', {
    //   // Make the editor understand when to bind `my-input-type`
    //   isComponent: el => {
    //     const text = el?.innerText;
    //     if(text){
    //       console.log(text);
    //       return /{{{?(#[a-z]+ )?[a-z]+.[a-z]*}?}}/.test(text);
    //     }
    //     console.log("Nope");
    //     return false;
    //   },
    //
    //   // Model definition
    //   model: {
    //     // Default properties
    //     defaults: {
    //       tagName: 'mj-raw',
    //       droppable: false, // Can't drop other elements inside
    //       attributes: { // Default attributes
    //         type: 'text',
    //         name: 'default-name',
    //         placeholder: 'Insert text here',
    //       }
    //     }
    //   },
    //   view: defaultView.extend()
    // });
  }, []);
  return <div id="email-editor" />;
};

export default MJMLEditor;
