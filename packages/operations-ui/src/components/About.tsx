import React, { useState } from 'react';

import moment from 'moment';
import { useUiStore } from '../store';

import {
  Dialog,
  DialogTitle,
  DialogContent,
  List,
  Typography,
  MenuItem,
  ListItem,
  Theme,
  Switch,
} from '@mui/material';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

import { ProjectDiagramIcon } from 'react-line-awesome';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      marginTop: theme.spacing(8),
    },
    listItem: {
      padding: theme.spacing(0.6, 0),
      justifyContent: 'space-between',
    },
    listValue: {
      padding: '0px 0px 0px 30px',
      textAlign: 'right',
      overflowWrap: 'anywhere',
    },
    dialogOverlay: {
      position: 'absolute',
      left: 0,
      top: -20,
      right: 0,
      width: '100%',
      zIndex: 5,
      opacity: 0.2,
      maskImage:
        'linear-gradient(to bottom, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0.6) 40%, rgba(0, 0, 0, 0))',
    },
    menuFooter: {
      textDecoration: 'none',
      color: '#777',
      fontSize: 12,
      marginTop: 3,
    },
    toggleButton: {
      position: 'absolute',
      right: theme.spacing(2),
      top: theme.spacing(2),
      zIndex: 4,
    },
  })
);

const About = () => {
  const styles = useStyles();
  const [open, setOpen] = useState(false);

  const darkTheme = useUiStore((state) => state.darkTheme);
  const toggleDarkTheme = useUiStore((state) => state.toggleDarkTheme);

  return (
    <React.Fragment>
      <MenuItem onClick={() => setOpen(true)}>
        <span className={styles.menuFooter}>v. {process.env.NEXT_PUBLIC_VERSION}</span>
      </MenuItem>
      <Dialog onClose={() => setOpen(false)} open={open}>
        <DialogTitle>
          <ProjectDiagramIcon /> Operations
          <Switch
            checked={darkTheme}
            onChange={toggleDarkTheme}
            name="theme"
            className={styles.toggleButton}
            size="small"
            color="primary"
          />
        </DialogTitle>
        <DialogContent>
          <List className={styles.list}>
            <ListItem className={styles.listItem} disableGutters divider>
              <Typography variant="subtitle2" color="textPrimary">
                Version
              </Typography>
              <Typography variant="h6" color="textSecondary" className={styles.listValue}>
                {process.env.NEXT_PUBLIC_VERSION}
              </Typography>
            </ListItem>
            <ListItem className={styles.listItem} disableGutters divider>
              <Typography variant="subtitle2" color="textPrimary">
                SHA1 Hash
              </Typography>
              <Typography variant="h6" color="textSecondary" className={styles.listValue}>
                {process.env.NEXT_PUBLIC_NAME}
              </Typography>
            </ListItem>
            <ListItem className={styles.listItem} disableGutters>
              <Typography variant="subtitle2" color="textPrimary">
                Build Date
              </Typography>
              <Typography variant="h6" color="textSecondary" className={styles.listValue}>
                {moment.utc(process.env.NEXT_PUBLIC_DATE).local().format('lll')} (
                {moment.utc(process.env.NEXT_PUBLIC_DATE).local().fromNow()})
              </Typography>
            </ListItem>
          </List>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

export default About;
