import React from 'react';
import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import { FundDocument } from '../../api/subscriptionsApi';
import { List, ListItem, ListItemIcon } from '@mui/material';
import { CheckSquareIcon, FileIcon, SquareIcon } from 'react-line-awesome';

interface Props {
  fundDocuments: FundDocument[];
}

const FundDocumentsTable = ({ fundDocuments }: Props) => {
  const router = useRouter();

  const getProfileName = (rowData) => {
    const name = rowData[4];
    const firstName = rowData[11];
    const lastName = rowData[12];
    return name ?? firstName + (lastName ? ' ' + lastName : '');
  };

  return (
    <TableWithTools
      data={fundDocuments}
      columnDefinitions={[
        {
          name: 'subscriptionId',
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: 'tenantId',
          label: 'Tenant',
        },
        {
          name: 'dealId',
          label: 'Deal ID',
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: 'dealName',
          label: 'Deal',
        },
        {
          name: 'organizerName',
          label: 'Organizer',
          options: {
            customBodyRender: (value, tableMeta, updateValue) => getProfileName(tableMeta.rowData),
          },
        },
        {
          name: 'investorName',
          label: 'Investor',
        },
        {
          name: 'entityDocs',
          label: 'Documents',
          options: {
            // eslint-disable-next-line react/display-name
            customBodyRender: function (entityDocs, tableMeta, updateValue) {
              return (
                <List dense>
                  {entityDocs?.operatingAgreement && (
                    <ListItem>
                      <ListItemIcon>
                        <FileIcon />
                      </ListItemIcon>
                      Operating Agreement
                    </ListItem>
                  )}
                  {entityDocs?.privatePlacementMemorandum && (
                    <ListItem>
                      <ListItemIcon>
                        <FileIcon />
                      </ListItemIcon>
                      Private Placement Memorandum
                    </ListItem>
                  )}
                  {entityDocs?.subscriptionAgreement && (
                    <ListItem>
                      <ListItemIcon>
                        <FileIcon />
                      </ListItemIcon>
                      Subscription Agreement
                    </ListItem>
                  )}
                </List>
              );
            },
          },
        },
        {
          name: 'investorSigned',
          label: 'Investor Signed',
          options: {
            display: false,
          },
        },
        {
          name: 'organizerSigned',
          label: 'Organizer Signed',
          options: {
            display: false,
          },
        },
        {
          name: 'managerSigned',
          label: 'Manager Signed',
          options: {
            display: false,
          },
        },
        {
          name: 'investorSigned',
          label: 'Signing',
          options: {
            filter: false,
            // eslint-disable-next-line react/display-name
            customBodyRender: function (value, tableMeta, updateValue) {
              const signedStatus: any = {
                investorSigned: tableMeta.rowData[7],
                organizerSigned: tableMeta.rowData[8],
                managerSigned: tableMeta.rowData[9],
              };
              return (
                <List dense>
                  <ListItem>
                    <ListItemIcon>
                      {signedStatus.investorSigned ? <CheckSquareIcon /> : <SquareIcon />}
                    </ListItemIcon>
                    Investor Signed
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      {signedStatus.organizerSigned ? <CheckSquareIcon /> : <SquareIcon />}
                    </ListItemIcon>
                    Organizer Signed
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      {signedStatus.managerSigned ? <CheckSquareIcon /> : <SquareIcon />}
                    </ListItemIcon>
                    Manager Signed
                  </ListItem>
                </List>
              );
            },
          },
        },
        {
          name: 'organizerFirstName',
          label: 'Organizer First Name',
          options: {
            display: false,
          },
        },
        {
          name: 'organizerLastName',
          label: 'Organizer Last Name',
          options: {
            display: false,
          },
        },
        {
          name: 'entityId',
          options: {
            display: false,
            filter: false,
          },
        },
      ]}
      onRowClick={(r, m) => {
        router.push(`fund-documents/${r[13]}`);
      }}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default FundDocumentsTable;
