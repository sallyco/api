import React from 'react';
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  Button,
  Menu,
  MenuItem,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Paper,
  Typography,
} from '@mui/material';
import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { ChevronDownIcon } from 'react-line-awesome';
import _ from 'lodash';
import moment, { unitOfTime } from 'moment';
import numeral from 'numeral';
import { ApexOptions } from 'apexcharts';
import { ModelTransformMode } from 'uniforms';
import { AutoForm, DateField, SubmitField } from 'uniforms-unstyled';
import FadeInModal from '../common/FadeInModal';
import { Bridge } from '../../forms';
import { FundDocument } from '../../api/subscriptionsApi';
import DoubleTextChip from '../common/DoubleTextChip';
import ChartTypeSelection from '../common/ChartTypeSelection';

interface Props {
  fundDocuments: FundDocument[];
}

interface IntervalSelection {
  value: moment.unitOfTime.DurationConstructor;
  startOf?: unitOfTime.StartOf;
  label?: string;
}

interface DateSelection {
  startDate?: moment.Moment;
  endDate?: moment.Moment;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  table: {
    minWidth: 200,
  },
}));

const FundDocumentsChart = ({ fundDocuments }: Props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [interval, setInterval] = React.useState<IntervalSelection>({
    value: 'year',
    startOf: 'year',
    label: 'ytd',
  });

  const [selectingDates, setSelectingDates] = React.useState(false);
  const [dateSelection, setDateSelection] = React.useState<DateSelection>({});

  const [chartType, setChartType] = React.useState<
    | 'line'
    | 'area'
    | 'bar'
    | 'histogram'
    | 'pie'
    | 'donut'
    | 'radialBar'
    | 'scatter'
    | 'bubble'
    | 'heatmap'
    | 'treemap'
    | 'boxPlot'
    | 'candlestick'
    | 'radar'
    | 'polarArea'
    | 'rangeBar'
  >('line');

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (intervalSelection: IntervalSelection) => {
    if (intervalSelection.label === 'dates') {
      setSelectingDates(true);
      setAnchorEl(null);
    } else {
      setInterval(intervalSelection);
      setAnchorEl(null);
    }
  };

  const validDocData = fundDocuments.filter((doc) => doc.entityDocs);

  const lastTime = (): DateSelection => {
    const newTime = moment().subtract(1, interval.value);
    if (interval.startOf === 'year') {
      return { startDate: moment([moment().year()]) };
    } else if (interval.startOf === 'quarter') {
      return { startDate: moment().startOf('quarter') };
    } else if (interval.startOf === 'date') {
      return {
        startDate: dateSelection.startDate,
        endDate: dateSelection.endDate,
      };
    }
    return { startDate: newTime };
  };

  const recentTrans = _.filter(validDocData, (entry) =>
    moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
      .local()
      .isBetween(
        lastTime().startDate,
        moment(lastTime().endDate).add(1, 'day').startOf('day'),
        undefined,
        '[]'
      )
  );

  const totalAmountRecent = _.sumBy(recentTrans, (entry) => entry.subscriptionAmount);
  const totalDocCountRecent = _.sumBy(recentTrans, (entry) =>
    Object.keys(entry.entityDocs)
      .map((key) => (!entry.entityDocs[key] ? 0 : 1))
      .reduce((a, b) => a + b, 0)
  );

  const investorToSign = _.filter(validDocData, (entry) => {
    return (
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) && !entry.investorSigned
    );
  });

  const organizerToSign = _.filter(validDocData, (entry) => {
    return (
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) &&
      entry.investorSigned &&
      !entry.organizerSigned
    );
  });

  const managerToSign = _.filter(validDocData, (entry) => {
    return (
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .isBetween(
          lastTime().startDate,
          moment(lastTime().endDate).add(1, 'day').startOf('day'),
          undefined,
          '[]'
        ) &&
      entry.investorSigned &&
      entry.organizerSigned &&
      !entry.managerSigned
    );
  });

  const documentCounts = {
    investorToSign: _.sumBy(investorToSign, (entry) =>
      Object.keys(entry.entityDocs)
        .map((key) => (!entry.entityDocs[key] ? 0 : 1))
        .reduce((a, b) => a + b, 0)
    ),
    organizerToSign: _.sumBy(organizerToSign, (entry) =>
      Object.keys(entry.entityDocs)
        .map((key) => (!entry.entityDocs[key] ? 0 : 1))
        .reduce((a, b) => a + b, 0)
    ),
    managerToSign: _.sumBy(managerToSign, (entry) =>
      Object.keys(entry.entityDocs)
        .map((key) => (!entry.entityDocs[key] ? 0 : 1))
        .reduce((a, b) => a + b, 0)
    ),
  };

  const transactionAmounts = {
    investorToSign: _.sumBy(investorToSign, (entry) => entry.subscriptionAmount),
    organizerToSign: _.sumBy(organizerToSign, (entry) => entry.subscriptionAmount),
    managerToSign: _.sumBy(managerToSign, (entry) => entry.subscriptionAmount),
  };

  const recentDataSeries = _(recentTrans)
    .groupBy((entry) =>
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [id, _.sumBy(entry, 'subscriptionAmount')])
    .value();

  const investorToSignDataSeries = _(investorToSign)
    .groupBy((entry) =>
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [
      id,
      _.sumBy(entry, (value) =>
        Object.keys(value.entityDocs)
          .map((key) => (!value.entityDocs[key] ? 0 : 1))
          .reduce((a, b) => a + b, 0)
      ),
    ])
    .value();

  const organizerToSignDataSeries = _(organizerToSign)
    .groupBy((entry) =>
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [
      id,
      _.sumBy(entry, (value) =>
        Object.keys(value.entityDocs)
          .map((key) => (!value.entityDocs[key] ? 0 : 1))
          .reduce((a, b) => a + b, 0)
      ),
    ])
    .value();

  const managerToSignDataSeries = _(managerToSign)
    .groupBy((entry) =>
      moment(entry.subscriptionCreatedAt, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(interval.value == 'day' ? 'hour' : 'day')
    )
    .map((entry, id) => [
      id,
      _.sumBy(entry, (value) =>
        Object.keys(value.entityDocs)
          .map((key) => (!value.entityDocs[key] ? 0 : 1))
          .reduce((a, b) => a + b, 0)
      ),
    ])
    .value();

  const chartOptions = (chartType = 'recent'): ApexOptions => {
    return {
      chart: {
        id: 'basic-' + chartType,
        toolbar: {
          show: false,
        },
        sparkline: {
          enabled: false,
        },
        zoom: {
          autoScaleYaxis: true,
        },
      },
      dataLabels: {
        enabled: false,
      },
      grid: {
        strokeDashArray: 2,
        borderColor: 'rgba(125, 138, 156, 0.3)',
      },
      stroke: {
        width: 3,
        curve: 'smooth',
        colors: chartType === 'recent' ? ['#3c44b1'] : ['#3c44b1', '#42c2f5', '#d61c1c', '#ffa200'],
      },
      fill: {
        type: 'gradient',
        opacity: 0.1,
        gradient: {
          shade: 'dark',
          type: 'vertical',
          shadeIntensity: 0.1,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 0.5,
          stops: [0, 50, 100],
        },
      },
      plotOptions: {
        bar: {
          columnWidth: '20%',
        },
      },
      tooltip: {
        x: {
          format: interval.value == 'day' ? 'dd MMM yyyy : HH:mm' : 'dd MMM yyyy',
        },
      },
      colors: chartType === 'recent' ? ['#3c44b1'] : ['#3c44b1', '#42c2f5', '#d61c1c', '#ffa200'],
      legend: {
        show: chartType === 'recent' ? false : true,
      },
      xaxis: {
        type: 'datetime',
        min: lastTime().startDate.valueOf(),
        max: (lastTime().endDate ?? moment()).valueOf(),
        labels: {
          datetimeFormatter: {
            year: 'yyyy',
            month: "MMM 'yy",
            day: 'dd MMM',
            hour: 'HH:mm',
          },
          datetimeUTC: false,
        },
      },
      yaxis: {
        min: 0,
        labels: {
          formatter: function (val, index) {
            return numeral(val).format('0,0');
          },
        },
      },
    };
  };

  const recentDataForChart = [
    {
      name: 'Amount',
      data: _.sortBy(recentDataSeries, [(entry) => new Date(entry[0])]),
    },
  ];

  const countDataForChart = [
    {
      name: 'Waiting for Investor Signature',
      data: _.sortBy(investorToSignDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Waiting for Organizer Signature',
      data: _.sortBy(organizerToSignDataSeries, [(entry) => new Date(entry[0])]),
    },
    {
      name: 'Waiting for Manager Signature',
      data: _.sortBy(managerToSignDataSeries, [(entry) => new Date(entry[0])]),
    },
  ];

  const dateRangeSelectionSchema = {
    title: 'Select dates',
    type: 'object',
    properties: {
      startDate: {
        description: 'Start Date',
      },
      endDate: {
        description: 'End Date',
      },
    },
    required: ['startDate', 'endDate'],
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <React.Fragment>
            <Button onClick={handleClick} className={classes.button} endIcon={<ChevronDownIcon />}>
              Last {interval.label ?? interval.value}
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                onClick={() => handleClose({ value: 'year', startOf: 'year', label: 'ytd' })}
              >
                YTD
              </MenuItem>
              <MenuItem
                onClick={() =>
                  handleClose({
                    value: 'quarter',
                    startOf: 'quarter',
                    label: 'qtd',
                  })
                }
              >
                QTD
              </MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'year' })}>Last Year</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'quarter' })}>Last Quarter</MenuItem>
              <MenuItem onClick={() => handleClose({ value: 'month' })}>Last Month</MenuItem>
              <MenuItem
                onClick={() =>
                  handleClose({
                    value: 'year',
                    startOf: 'date',
                    label: 'dates',
                  })
                }
              >
                Date Selection
              </MenuItem>
            </Menu>
          </React.Fragment>
        }
        classes={{ action: classes.current }}
        subheader="Recent Fund Documents"
        subheaderTypographyProps={{
          color: 'textSecondary',
          variant: 'caption',
        }}
        title="Summary"
        titleTypographyProps={{ color: 'textPrimary' }}
      />
      <CardContent className={classes.content}>
        <Grid container spacing={6}>
          <Grid item sm={6} md={5}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>
                {interval.label &&
                interval.label === 'dates' &&
                lastTime().startDate &&
                lastTime().endDate
                  ? 'Total : ' +
                    moment(lastTime().startDate).format('DD-MMM-yyyy') +
                    ' to ' +
                    moment(lastTime().endDate).format('DD-MMM-yyyy')
                  : 'Total last ' + (interval.label ?? interval.value)}
              </span>
              <span className={classes.gridItemValue}>
                <DoubleTextChip
                  text1={numeral(totalDocCountRecent).format('0,0')}
                  text2={numeral(totalAmountRecent).format('$ 0,0')}
                  gradient={{ color1: '#15487a', color2: '#2b66a1' }}
                />
              </span>
            </div>
            <div className={classes.gridItem}>
              <ChartTypeSelection
                {...{
                  classes,
                  chartType,
                  setChartType,
                  allowedChartTypes: ['line', 'area', 'bar', 'heatmap'],
                }}
              />
            </div>
          </Grid>
          <Grid item sm={6} md={7}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Details</span>
              <TableContainer component={Paper}>
                <Table className={classes.table} size="small">
                  <TableBody>
                    <TableRow key="ACH">
                      <TableCell component="th" scope="row">
                        Waiting for Investor Signature
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(documentCounts.investorToSign).format('0,0')}
                          text2={numeral(transactionAmounts.investorToSign).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow key="WIRE">
                      <TableCell component="th" scope="row">
                        Waiting for Organizer Signature
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(documentCounts.organizerToSign).format('0,0')}
                          text2={numeral(transactionAmounts.organizerToSign).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow key="FAILED">
                      <TableCell component="th" scope="row">
                        Waiting for Manager Signature
                      </TableCell>
                      <TableCell align="right">
                        <DoubleTextChip
                          text1={numeral(documentCounts.managerToSign).format('0,0')}
                          text2={numeral(transactionAmounts.managerToSign).format('$ 0,0')}
                          gradient={{ color1: '#bdbdbd', color2: '#e0e0e0' }}
                          size={'small'}
                          tooltip
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Grid>
        </Grid>
        <Chart options={chartOptions()} series={recentDataForChart} type={chartType} height={317} />
        <Chart
          options={chartOptions('count')}
          series={countDataForChart}
          type={chartType}
          height={317}
        />
      </CardContent>

      {dateRangeSelectionSchema && (
        <FadeInModal open={selectingDates} setOpen={setSelectingDates}>
          <Typography gutterBottom variant="h5" component="h2">
            {dateRangeSelectionSchema.title}
          </Typography>
          <AutoForm
            schema={Bridge(dateRangeSelectionSchema)}
            onSubmit={(submittedData) => {
              setSelectingDates(false);
              setDateSelection(submittedData);
              setInterval({ value: 'year', startOf: 'date', label: 'dates' });
            }}
            modelTransform={(mode: ModelTransformMode, model: any) => {
              if (mode === 'validate' || mode === 'submit') {
                const { startDate, endDate } = model || {};
                const newModel = {
                  startDate: moment(startDate).utc().startOf('day'),
                  endDate: moment(endDate).utc().startOf('day'),
                };
                return newModel;
              }
              return model;
            }}
          >
            <DateField name="startDate" />
            <DateField name="endDate" />
            <SubmitField />
          </AutoForm>
        </FadeInModal>
      )}
    </Card>
  );
};

export default FundDocumentsChart;
