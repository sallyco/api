import React, { useEffect, useRef, useState } from 'react';
//import "react-pdf/dist/Page/AnnotationLayer.css";
//import getAdobeInfo from "../../tools/adobeInfo";

interface Props {
  fileId: any;
  pageNumber?: number;
  width?: number;
  tenantId?: string;
}

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

export default function PdfDocument({
  fileId,
  pageNumber = 1,
  width = 380,
  tenantId = 'master',
}: Props) {
  const [numPages, setNumPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(pageNumber);
  const [fileData, setFileData] = useState('');

  const viewerContainerRef = useRef<HTMLDivElement>(null);

  const downloadDocument = async (fileId: any) => {
    const response = await fetch(`${baseUrl}/files/${fileId}/download`, {
      headers: {
        'x-file-download-tenant-id': tenantId,
      },
    });
    const contentLength = response.headers.get('content-length');
    const total = parseInt(contentLength, 10);
    let loaded = 0;

    const res = new Response(
      new ReadableStream({
        async start(controller) {
          const reader = response.body.getReader();
          for (;;) {
            const { done, value } = await reader.read();
            if (done) break;
            loaded += value.byteLength;

            controller.enqueue(value);
          }
          controller.close();
        },
      })
    );
    const blob = await res.blob();
    return window.URL.createObjectURL(blob);
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    if (fileId) {
      downloadFile();
    }
  }, []);

  useEffect(() => {
    if (!fileData) {
      return;
    }
    const iframe = document.createElement('iframe');

    iframe.src = `/pdfjs/web/viewer.html?file=${fileData}`;
    iframe.width = '100%';
    iframe.height = '100%';

    if (viewerContainerRef.current.childElementCount > 0) {
      const childNode = viewerContainerRef.current.children[0];
      viewerContainerRef.current.removeChild(childNode);
    }
    viewerContainerRef.current.appendChild(iframe);
  }, [fileData, viewerContainerRef]);

  return (
    <React.Fragment>
      <div ref={viewerContainerRef} style={{ width: '100%', height: '100%' }}></div>
    </React.Fragment>
  );
}
