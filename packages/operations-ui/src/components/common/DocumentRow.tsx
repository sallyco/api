import React, { useEffect, useState } from 'react';
import { Document as DocumentPreview, Page } from 'react-pdf';
import { SnackbarUtils, useRequest, objectProps } from '@packages/wired-gbt-ui';
import moment from 'moment';
import { Button, Menu, MenuItem, TableCell, TableRow, Typography } from '@mui/material';
import { EllipsisVIcon } from 'react-line-awesome';
import { ContentLoader } from '@packages/gbt-ui';
import { fileIcons } from '../../tools/fileIcons';
import FadeInModal from './FadeInModal';
import { useRouter } from 'next/router';

interface Props {
  fileId: any; //string
  size?: 'medium' | 'big' | 'small' | 'large' | 'mini' | 'tiny' | 'huge' | 'massive' | undefined;
  sharable?: boolean;
  shareCallback?: any; //function
  documentTitle?: string;
}

const DocumentRow = ({
  fileId,
  size = 'medium',
  sharable,
  shareCallback = () => {},
  documentTitle,
}: Props) => {
  const router = useRouter();
  const [downloading, setDownloading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [numPages, setNumPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [fileData, setFileData] = useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);

  const op = objectProps.files;
  const { data: file } = useRequest(`${op.route}/${fileId}`);

  const downloadDocument = async (fileId: string) => {
    const blobURL = fetch(process.env.NEXT_PUBLIC_API_URL + '/files/' + fileId + '/download', {
      method: 'GET',
      headers: {
        'x-file-download-tenant-id': file.tenantId ?? 'master',
      },
    })
      .then((response) => response.blob())
      .then((blob) => {
        return window.URL.createObjectURL(blob);
      });

    return blobURL;
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    if (file && !file.generating) {
      downloadFile();
    }
  }, [fileId, file]);

  const DownloadFile = async (fileId: string, fileName?: string) => {
    try {
      if (fileData) {
        const url = fileData;
        const link = document.createElement('a');
        link.href = url;
        if (fileName) {
          link.setAttribute('download', fileName);
        }
        document.body.appendChild(link);
        link.click();
      } else {
        SnackbarUtils.error('Download is unavailable at this time for this document');
      }
    } catch {
      SnackbarUtils.error('Download is unavailable at this time for this document');
    }
  };

  const onPage = (type) => {
    let newPage = type ? currentPage + 1 : currentPage - 1;
    if (newPage > numPages) {
      newPage = numPages;
    } else if (newPage < 1) {
      newPage = 1;
    }
    setCurrentPage(newPage);
  };

  const previewFile = () => {
    setModalOpen(true);
  };

  const handleDownload = () => {
    previewFile();
  };

  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const navigateToFileView = () => {
    router.push(`/files/view/${file.id}`);
  };

  return (
    <>
      {!file ? (
        <TableRow>
          <TableCell>
            <ContentLoader />
          </TableCell>
        </TableRow>
      ) : (
        <TableRow>
          <TableCell width={1}>{fileIcons(file)}</TableCell>
          <TableCell onClick={() => handleDownload()} style={{ cursor: 'pointer' }}>
            <h3>
              {file.name}
              <h5>
                {documentTitle && <b>{documentTitle} - </b>}
                {moment(file.createdAt).format('ll')}
              </h5>
            </h3>
          </TableCell>
          <TableCell>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuClick}>
              <EllipsisVIcon />
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleMenuClose}
            >
              <MenuItem
                onClick={async () => {
                  navigateToFileView();
                }}
              >
                Preview in Viewer
              </MenuItem>
              <MenuItem
                onClick={async () => {
                  setDownloading(true);
                  await DownloadFile(file?.id, file?.name);
                  setDownloading(false);
                }}
              >
                Download
              </MenuItem>
            </Menu>
          </TableCell>
        </TableRow>
      )}
      {file && (
        <FadeInModal open={modalOpen} setOpen={setModalOpen}>
          <Typography gutterBottom variant="h5" component="h2">
            {file?.name}
          </Typography>

          <div style={{ margin: 'auto' }}>
            <DocumentPreview
              file={fileData}
              onLoadSuccess={({ numPages }) => setNumPages(numPages)}
              loading={<ContentLoader />}
              error={<>Failed to load file. Please refresh this page</>}
            >
              <Page pageNumber={currentPage} width={500} />
            </DocumentPreview>
          </div>

          <Button disabled={currentPage <= 1} onClick={() => onPage(0)}>
            Previous
          </Button>
          <Button disabled={currentPage >= numPages} onClick={() => onPage(1)}>
            Next
          </Button>
          <Button onClick={() => setModalOpen(false)}>Close</Button>
          <Button
            onClick={async () => {
              setDownloading(true);
              await DownloadFile(file?.id, file?.name);
              setDownloading(false);
            }}
          >
            Download
          </Button>
        </FadeInModal>
      )}
    </>
  );
};

export default DocumentRow;
