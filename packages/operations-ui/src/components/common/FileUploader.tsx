const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import useSWR, { mutate } from 'swr';
import React from 'react';
import { FilesDropzone } from '@packages/gbt-ui';
import { SnackbarUtils, doChange } from '@packages/wired-gbt-ui';

interface Props {
  objectType: string;
  objectId: string;
  tenantId?: string;
  omitFields?: string[];
  uploadSuccess?: (arg: any) => void;
}

export default function FileUploader({
  objectType,
  objectId,
  tenantId = 'master',
  omitFields = [],
  uploadSuccess = () => {},
}: Props) {
  const objectUrl = baseUrl + `/${objectType}/${objectId}`;
  const { data: objectValue, error: err } = useSWR(objectUrl, fetcher);

  const updateObjectWithNewFiles = (fileIds) => {
    const objectToUpdate = { ...objectValue };
    omitFields.forEach((field) => delete objectToUpdate[field]);
    const updatedFileIds = objectToUpdate.files ? [...objectToUpdate.files, ...fileIds] : fileIds;
    fetch(`${objectUrl}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        ...objectToUpdate,
        files: updatedFileIds,
      }),
    }).then((response) => {
      mutate(objectUrl);
      uploadSuccess(fileIds);
    });
  };

  const handleUpload = async (files) => {
    const fileToUpload = files[0];
    // eslint-disable-next-line no-control-regex
    if (/[^\u0000-\u00ff]/.test(fileToUpload.name)) {
      SnackbarUtils.error(
        `File name should not contain Unicode characters. '${fileToUpload.name}' `
      );
      return;
    }
    const data = new FormData();
    data.append('file', fileToUpload);
    const uploadedFiles: any = await doChange(
      `/files/upload`,
      'POST',
      data,
      undefined,
      undefined,
      { 'x-file-upload-tenant-id': tenantId },
      true
    );
    if (!uploadedFiles?.length) {
      return;
    }
    const uploadedFileIds = uploadedFiles.map((file) => file.id);
    updateObjectWithNewFiles(uploadedFileIds);
  };

  return (
    <>
      <FilesDropzone handleUpload={handleUpload} />
    </>
  );
}
