import React, { useEffect, useState } from 'react';

interface Props {
  fileId: string;
  removeHandler?: (id) => void;
  showRemove?: boolean;
}

export default function ImageFile({ fileId, removeHandler = null, showRemove = false }: Props) {
  const [isLoading, setIsLoading] = useState(false);
  const [fileData, setFileData] = useState('');

  const downloadDocument = async (fileId: string) => {
    //const response = await useRequest("/files/" + fileId + "/download");
    //if (!response.ok) {
    //  SnackbarUtils.error(
    //    "Download is unavailable at this time for this document"
    //  );
    //  return "";
    //}
    //return window.URL.createObjectURL(new Blob([response.data]));
  };

  useEffect(() => {
    const downloadFile = async () => {
      // setFileData(await downloadDocument(fileId));
    };
    setIsLoading(true);
    if (!fileData && !isLoading) {
      downloadFile();
    }
    setIsLoading(false);
  }, [fileId, isLoading]);

  return (
    <>
      {fileData === '' || isLoading ? <></> : <img src={fileData} style={{ maxHeight: '100%' }} />}
    </>
  );
}
