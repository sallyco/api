import React from 'react';
import { SnackbarUtils } from '@packages/wired-gbt-ui';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;
import { ExternalLinkSquareAltIcon } from 'react-line-awesome';

import { Button, Tooltip } from '@mui/material';

export async function startImpersonateSession(realmId, userId, path) {
  const url = `${baseUrl}/users/${realmId}/${userId}/impersonate/`;

  const response = await fetch(url, { method: 'POST' });
  const result = await response.json();
  if (!response.ok) {
    SnackbarUtils.warning(result?.error_description);
    return;
  }

  const env = process.env.NEXT_PUBLIC_ENVIRONMENT;
  let domain = `.glassboardtech.com`;
  let redirect_uri = ``;

  if (env === 'localhost') {
    domain = 'localhost';
    redirect_uri = 'http://localhost:3000/#/' + path;
  } else if (env === 'dev' || env === 'qa' || env === 'sandbox') {
    redirect_uri = `https://app.${env}.glassboardtech.com/#/` + path;
  } else if (env == 'production') {
    redirect_uri = `https://${realmId}.glassboardtech.com/#/` + path;
  }

  document.cookie = `__session= ;path=/;domain=${domain}; expires = Thu, 01 Jan 1970 00:00:00 GMT`;
  document.cookie = `__session=${result.access_token};path=/;domain=${domain}`;
  window.open(redirect_uri, '_blank');
}

interface Props {
  tenantId: string | undefined;
  ownerId: string | undefined;
  destinationUrl?: string | undefined;
}

const ImpersonateButton = ({
  tenantId,
  ownerId,
  destinationUrl = '/dashboard',
  ...rest
}: Props) => {
  return (
    <Tooltip
      title={!tenantId || !ownerId ? 'Can’t impersonate from this page' : ''}
      placement="top"
      arrow
    >
      <Button
        {...rest}
        color="primary"
        size="small"
        variant="outlined"
        onClick={() => startImpersonateSession(tenantId, ownerId, destinationUrl)}
        startIcon={<ExternalLinkSquareAltIcon />}
        disabled={!tenantId || !ownerId}
      >
        Impersonate
      </Button>
    </Tooltip>
  );
};

export default ImpersonateButton;
