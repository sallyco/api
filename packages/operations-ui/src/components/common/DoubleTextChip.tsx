import React from 'react';
import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import Tooltip from '@mui/material/Tooltip';

interface DoubleTextChipProps {
  text1: string;
  text2: string;
  gradient: { color1: string; color2: string };
  size?: 'normal' | 'small';
  tooltip?: boolean;
}

const relativeLuminance = function (R8bit, G8bit, B8bit) {
  const RsRGB = R8bit / 255.0;
  const GsRGB = G8bit / 255.0;
  const BsRGB = B8bit / 255.0;

  const R = RsRGB <= 0.03928 ? RsRGB / 12.92 : Math.pow((RsRGB + 0.055) / 1.055, 2.4);
  const G = GsRGB <= 0.03928 ? GsRGB / 12.92 : Math.pow((GsRGB + 0.055) / 1.055, 2.4);
  const B = BsRGB <= 0.03928 ? BsRGB / 12.92 : Math.pow((BsRGB + 0.055) / 1.055, 2.4);

  return 0.2126 * R + 0.7152 * G + 0.0722 * B;
};

const blackContrast = function (r, g, b) {
  const L = relativeLuminance(r, g, b);
  return (L + 0.05) / 0.05;
};

const whiteContrast = function (r, g, b) {
  const L = relativeLuminance(r, g, b);
  return 1.05 / (L + 0.05);
};

const chooseFGcolor = function (color, prefer = 'white') {
  const hex = color.replace(/#/, '');
  const r = parseInt(hex.substr(0, 2), 16);
  const g = parseInt(hex.substr(2, 2), 16);
  const b = parseInt(hex.substr(4, 2), 16);

  const Cb = blackContrast(r, g, b);
  const Cw = whiteContrast(r, g, b);
  if (Cb >= 7.0 && Cw >= 7.0) return prefer;
  else return Cb > Cw ? 'black' : 'white';
};

const useStyles = (props) =>
  makeStyles((theme: Theme) => ({
    outerStyle: {
      verticalAlign: 'middle !important',
      padding: props.size === 'normal' ? '0px 5px 6px 5px' : '4px 5px 6px 5px',
      borderRadius: '16px',
      backgroundColor: props.color2,
      whiteSpace: 'nowrap',
    },
    text1Style: {
      backgroundColor: props.color1,
      color: chooseFGcolor(props.color1),
      fontSize: props.size === 'normal' ? '0.75rem' : '0.625rem',
      verticalAlign: 'middle',
      marginRight: '1px',
      borderRadius: '16px',
      padding: props.size === 'normal' ? '4px 4px 4px 4px' : '4px 4px 4px 4px',
    },
    text2Style: {
      backgroundColor: props.color2,
      color: chooseFGcolor(props.color2),
      fontSize: props.size === 'normal' ? '1rem' : '0.8rem',
      verticalAlign: 'middle',
      marginLeft: '1px',
    },
  }));

const DoubleTextChip = ({
  text1,
  text2,
  gradient,
  size = 'normal',
  tooltip = false,
}: DoubleTextChipProps) => {
  const props = { size, color1: gradient.color1, color2: gradient.color2 };
  const classes = useStyles(props)();
  if (size === 'small') {
    classes.text1Style.fontsize('0.625rem');
    classes.text2Style.fontsize('0.8rem');
  }

  return (
    <>
      <Tooltip title={tooltip ? text1 + ' / ' + text2 : ''}>
        <span className={classes.outerStyle}>
          <span className={classes.text1Style}>{text1}</span>
          <span className={classes.text2Style}>{text2}</span>
        </span>
      </Tooltip>
    </>
  );
};

export default DoubleTextChip;
