import { useEffect, useRef, useState } from 'react';
import dynamic from 'next/dynamic';
import { MenuItem, Select } from '@mui/material';
import FormControl from '@mui/material/FormControl';

const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
export type ChartTypes =
  | 'line'
  | 'area'
  | 'bar'
  | 'histogram'
  | 'pie'
  | 'donut'
  | 'radialBar'
  | 'scatter'
  | 'bubble'
  | 'heatmap'
  | 'treemap'
  | 'boxPlot'
  | 'candlestick'
  | 'radar'
  | 'polarArea'
  | 'rangeBar';

interface Props {
  data: any;
  chartType: ChartTypes;
  timeBox?: any;
}
interface ChartOptions {
  chart: { id: string };
  dataLabels: { enabled: boolean };
  colors: string[];
  stroke: {
    curve: 'smooth' | 'straight' | 'stepline' | ('smooth' | 'straight' | 'stepline')[];
    fill: { type: string };
  };
  plotOptions: { bar: { columnWidth: string } };
  xaxis: { categories?: number[] };
  yaxis:
    | [
        {
          axisTicks: { show: true };
          axisBorder: { show: true; color: string };
        },
        {
          opposite: true;
          axisTicks: { show: true };
          axisBorder: { show: true; color: string };
        }
      ]
    | any;
  tooltip: any;
  legend: any;
}
const ChartTypeGrid = ({ classes, chartType, setChartType, timeBox }) => (
  <div className={classes.gridItem}>
    <span className={classes.gridItemLabel}>Chart Type : </span>
    <FormControl variant="outlined" className={classes.gridItemValue}>
      <Select
        id="select-chart"
        value={chartType}
        onChange={(event: any) => setChartType(event.target.value as string)}
        style={{ width: '230px' }}
      >
        <MenuItem value={'line'}>Line</MenuItem>
        <MenuItem value={'bar'}>Bar</MenuItem>
        <MenuItem value={'area'}>Area</MenuItem>
        <MenuItem value={'heatmap'}>Heat Map</MenuItem>
      </Select>
    </FormControl>
  </div>
);

const yaxis = (Y_max) => [
  {
    min: 0,
    max: Y_max + 1,
  },
  // {
  //     min:0,
  //     max:Y_max,
  //     opposite: true,
  //     axisTicks: {
  //         show: true
  //     },
  //     axisBorder: {
  //         show: true,
  //         color: "#247BA0"
  //     },
  //    // forceNiceScale:true,
  //     tickAmount:Y_max+1
  // }
];

const options = (categories, Y_max, timeBox) =>
  ({
    chart: {
      id: 'basic',
      toolbar: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    colors: [
      '#FF1654',
      '#247BA0',
      '#3DDD2D',
      '#DFFF00',
      '#9FE2BF',
      '#40E0D0',
      '#6495ED',
      '#008080',
    ],
    stroke: {
      curve: 'smooth',
      fill: {
        type: 'gradient',
      },
    },
    plotOptions: {
      bar: {
        columnWidth: '20%',
      },
    },
    // xaxis: {
    //     type: 'datetime',
    //     min: new Date(`01-01-${new Date().getFullYear()}`).getTime(),
    //     max: Date.now(),
    //     labels: {
    //         datetimeFormatter: {
    //             year: 'yyyy',
    //             month: 'MMM \'yy',
    //             day: 'dd MMM',
    //             hour: 'HH:mm'
    //         },
    //         datetimeUTC: false
    //     },

    // },
    xaxis: {
      categories: typeof categories === 'object' && categories ? categories : [],
    },
    yaxis: yaxis(Y_max),
    tooltip: {
      x: {
        format: timeBox?.interval == 'day' ? 'dd MMM yyyy : HH:mm' : 'dd MMM yyyy',
      },
    },
    legend: {
      horizontalAlign: 'left',
      offsetX: 40,
    },
  } as ChartOptions);

const CategoryChart = ({ data, chartType, timeBox }: Props) => {
  const Y_max =
    typeof data?.series === 'object' && data?.series
      ? Math.max(...data?.series.map((e) => Math.max(...e?.data)))
      : 0;
  const [chartOption, setChartOptionsOption] = useState({
    options: options(data?.categories, Y_max, timeBox),
    series: data?.series ? data?.series : [],
    type: chartType,
  }); // chartOptions(transaction, chartType)
  const chartRef = useRef({ retry: () => {} });
  useEffect(() => {
    if (data) {
      setChartOptionsOption({
        ...chartOption,
        options: {
          ...chartOption.options,
          xaxis: {
            categories: data?.categories,
          },
          yaxis: yaxis(Y_max),
        },
        series: data.series,
        type: chartType,
      });
    }
    chartRef.current && chartRef.current.retry();
  }, [data, chartType]);

  if (data && data.categories)
    return (
      <Chart
        ref={chartRef}
        options={chartOption.options}
        series={chartOption.series}
        type={chartOption.type}
        height={410}
      />
    );
  else return <></>;
};

export { ChartTypeGrid };
export default CategoryChart;
