import React from 'react';
//import { Input, Header, Popup, Button } from "semantic-ui-react";

interface Props {
  label?: string;
  value?: string;
  [x: string]: any;
}
export default function SubformCopy({ label = '', value = '', ...props }: Props) {
  return <></>;
}
/**
    <>
      <Header as={"h5"}>{label}</Header>
      <Input fluid type={"text"} defaultValue={value} action {...props}>
        <input defaultValue={value} {...props} />
        <Popup
          content={"Copy to Clipboard"}
          trigger={
            <Button
              onClick={(e) => {
                const el = e.currentTarget.previousElementSibling;
                if (el.hasAttribute("value")) {
                  const text = Object.entries(el)
                    .filter((entry) => entry[0] === "value")
                    .pop()[1];
                  navigator.clipboard.writeText(text);
                }
              }}
              icon="copy"
            />
          }
        />
      </Input>
    </>
  );
}
**/
