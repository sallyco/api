import React from 'react';
import { FormControl, MenuItem, Select } from '@mui/material';
import { ClassNameMap } from '@mui/styles';

type ChartType =
  | 'line'
  | 'area'
  | 'bar'
  | 'histogram'
  | 'pie'
  | 'donut'
  | 'radialBar'
  | 'scatter'
  | 'bubble'
  | 'heatmap'
  | 'treemap'
  | 'boxPlot'
  | 'candlestick'
  | 'radar'
  | 'polarArea'
  | 'rangeBar';

interface ChartTypeSelectionProps {
  classes: ClassNameMap<any>;
  chartType: ChartType;
  setChartType: React.Dispatch<React.SetStateAction<ChartType>>;
  allowedChartTypes?: ChartType[] | 'all';
}

const ChartTypeSelection = ({
  classes,
  chartType,
  setChartType,
  allowedChartTypes = 'all',
}: ChartTypeSelectionProps) => {
  const allTypes =
    typeof allowedChartTypes === 'string' && allowedChartTypes === 'all' ? true : false;

  return (
    <div>
      <span className={classes.gridItemLabel}>Chart Type</span>
      <FormControl variant="outlined" className={classes.gridItemValue}>
        <Select
          id="select-chart"
          value={chartType}
          onChange={(event: any) => setChartType(event.target.value as ChartType)}
          style={{ maxHeight: 20 }}
        >
          {(allTypes || allowedChartTypes.includes('line')) && (
            <MenuItem value={'line'}>Line</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('area')) && (
            <MenuItem value={'area'}>Area</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('bar')) && (
            <MenuItem value={'bar'}>Bar</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('histogram')) && (
            <MenuItem value={'histogram'}>Histogram</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('pie')) && (
            <MenuItem value={'pie'}>Pie</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('donut')) && (
            <MenuItem value={'donut'}>Donut</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('radialBar')) && (
            <MenuItem value={'radialBar'}>Radial Bar</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('scatter')) && (
            <MenuItem value={'scatter'}>Scatter</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('bubble')) && (
            <MenuItem value={'bubble'}>Bubble</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('heatmap')) && (
            <MenuItem value={'heatmap'}>Heat Map</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('treemap')) && (
            <MenuItem value={'treemap'}>Tree Map</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('boxPlot')) && (
            <MenuItem value={'boxPlot'}>Box Plot</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('candlestick')) && (
            <MenuItem value={'candlestick'}>Candlestick</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('radar')) && (
            <MenuItem value={'radar'}>Radar</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('polarArea')) && (
            <MenuItem value={'polarArea'}>Polar Area</MenuItem>
          )}
          {(allTypes || allowedChartTypes.includes('rangeBar')) && (
            <MenuItem value={'rangeBar'}>Range Bar</MenuItem>
          )}
        </Select>
      </FormControl>
    </div>
  );
};

export default ChartTypeSelection;
