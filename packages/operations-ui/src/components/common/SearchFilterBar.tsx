import * as React from 'react';
import type { FC } from 'react';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Grid from '@mui/material/Grid';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { SearchIcon, TimesCircleIcon } from 'react-line-awesome';
import TextField from '@mui/material/TextField';

export interface SearchFilterBarProps {
  searchText: string;
  onSearch: (e: any) => void;
  onHide?: () => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  fullWidthSearchFilterBar: {
    maxWidth: '100%',
  },
  title: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  icons: {
    marginTop: 'auto',
    marginBottom: 'auto',
    fontSize: 14,
  },
  main: {
    display: 'flex',
    flex: '1 0 auto',
  },
  searchText: {
    flex: '0.8 0',
  },
  clearIcon: {
    '&:hover': {
      color: theme.palette.error.main,
    },
  },
}));

const SearchFilterBar: FC<SearchFilterBarProps> = ({ searchText, onSearch, onHide }) => {
  const classes = useStyles();
  // const [showSearch, setShowSearch] = React.useState(false);

  const handleTextChange = (event) => {
    onSearch(event.target.value);
  };

  const onKeyDown = (event) => {
    if (event.key === 'Escape') {
      hideSearch();
    }
  };

  const hideSearch = () => {
    onSearch('');
    //  setShowSearch(false);
    if (onHide) {
      onHide();
    }
  };

  return (
    <Box pt={2} className={classes.fullWidthSearchFilterBar}>
      <Grid container justifyContent="space-between" wrap="nowrap" spacing={2}>
        <Grid item className={classes.title} xs={6}>
          <div className={classes.main}>
            <TextField
              className={classes.searchText}
              value={searchText || ''}
              autoFocus={true}
              onChange={handleTextChange}
              fullWidth={true}
              placeholder="Search"
            />
            <IconButton className={classes.clearIcon} onClick={hideSearch} size="large">
              <TimesCircleIcon />
            </IconButton>
          </div>
        </Grid>
        <Grid item className={classes.icons}>
          <IconButton onClick={() => {}} size="large">
            <SearchIcon />
          </IconButton>
        </Grid>
      </Grid>
    </Box>
  );
};
export default SearchFilterBar;
