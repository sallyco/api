import React, { useEffect, useState } from 'react';
import Link from 'next/link';

import { Document as DocumentPreview, Page, pdfjs } from 'react-pdf';
import { FilePdfIcon } from 'react-line-awesome';
import styled from 'styled-components';

//import "react-pdf/dist/esm/Page/AnnotationLayer.css";
//import { toast } from 'react-toastify';
//
import { Box, Typography, LinearProgress, CircularProgress } from '@mui/material';

pdfjs.GlobalWorkerOptions.workerSrc = '/pdfjs/build/pdf.worker.min.js';

interface PdfDocumentProps {
  file: any;
  width?: number;
}

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const PDFDocumentWrapper = styled.div`
  canvas {
    width: 100% !important;
    height: auto !important;
    border: 1px solid lightgray;
    cursor: pointer;
  }
`;

export default function PdfDocument({ file, width = 380 }: PdfDocumentProps) {
  const [numPages, setNumPages] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState(1);

  const [fileData, setFileData] = useState('');
  const [filename, setFilename] = useState('');
  const [currentPercent, setCurrentPercent] = useState(0);

  const onPDFLoadSuccess = ({ numPages }) => setNumPages(numPages);

  const onPage = (type) => {
    let newPage = type ? currentPage + 1 : currentPage - 1;
    if (newPage > numPages) {
      newPage = numPages;
    } else if (newPage < 1) {
      newPage = 1;
    }
    setCurrentPage(newPage);
  };

  const downloadDocument = async (file: any) => {
    const response = await fetch(`${baseUrl}/files/${file.id}/download`, {
      headers: {
        'x-file-download-tenant-id': file.tenantId,
      },
      cache: 'force-cache',
    });
    const contentLength = response.headers.get('content-length');
    const total = parseInt(contentLength, 10);
    let loaded = 0;

    const res = new Response(
      new ReadableStream({
        async start(controller) {
          const reader = response.body.getReader();
          for (;;) {
            const { done, value } = await reader.read();
            if (done) break;
            loaded += value.byteLength;

            const percentCompleted = Math.floor((loaded / total) * 100);
            setCurrentPercent(percentCompleted);

            controller.enqueue(value);
          }
          controller.close();
        },
      })
    );
    const blob = await res.blob();
    return window.URL.createObjectURL(blob);
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(file));
    };
    if (file && !fileData) {
      downloadFile();
    }
  }, [file, fileData]);

  //  goToPrevPage = () =>
  //    this.setState(state => ({ pageNumber: state.pageNumber - 1 }));
  //  goToNextPage = () =>
  //    this.setState(state => ({ pageNumber: state.pageNumber + 1 }));

  //  const file = useSelector(
  //    (state: RootState) => state.files.filesById[fileId]
  //  )
  //
  //  useEffect(() => {
  //    if (fileId && (!file || file.id !== fileId)) {
  //      dispatch(fetchFileById(fileId))
  //    }
  //  }, [file, fileId])

  return (
    <React.Fragment>
      {!fileData ? (
        <React.Fragment>
          <Typography variant="caption" display="block" gutterBottom>
            Loading Preview{currentPercent > 0 && ` ${currentPercent}%`}...
          </Typography>
          {currentPercent > 0 && currentPercent !== 100 ? (
            <LinearProgress variant="determinate" value={currentPercent} />
          ) : (
            <LinearProgress />
          )}
        </React.Fragment>
      ) : (
        <Link href={`/files/view/${file.id}`} passHref>
          <PDFDocumentWrapper>
            <DocumentPreview
              file={fileData}
              onLoadSuccess={onPDFLoadSuccess}
              loading={<CircularProgress />}
              error={
                <Box m={3}>
                  <Typography variant="h1" display="block" gutterBottom align="center">
                    <FilePdfIcon />
                    404
                  </Typography>
                  <Typography variant="body1" display="block" align="center">
                    Not Found
                  </Typography>
                </Box>
              }
            >
              <Page pageNumber={currentPage} />
            </DocumentPreview>
          </PDFDocumentWrapper>
        </Link>
      )}
    </React.Fragment>
  );
}
