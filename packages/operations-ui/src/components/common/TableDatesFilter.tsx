import { useState } from 'react';
import { Button, Typography } from '@mui/material';
import FadeInModal from '../common/FadeInModal';
import { AutoForm, DateField, SubmitField } from 'uniforms-unstyled';
import { Bridge } from '../../forms/FormBuilder';
import CalendarIcon from '@mui/icons-material/CalendarTodaySharp';
const DateRangeSelectFade = ({ label, setDateRange, filterList, index, onChange, column }) => {
  const [selectingDates, setSelectingDates] = useState(false);
  const dateRangeSelectionSchema = {
    title: 'Select dates',
    type: 'object',
    properties: {
      startDate: {
        description: 'Start Date',
      },
      endDate: {
        description: 'End Date',
      },
    },
    required: ['startDate', 'endDate'],
  };
  return (
    <>
      <Button
        variant="outlined"
        onClick={() => {
          setSelectingDates(true);
        }}
        endIcon={<CalendarIcon />}
      >
        {label}
      </Button>
      <FadeInModal open={selectingDates} setOpen={setSelectingDates}>
        <Typography gutterBottom variant="h5" component="h2">
          {dateRangeSelectionSchema.title}
        </Typography>
        <AutoForm
          schema={Bridge(dateRangeSelectionSchema)}
          onSubmit={(submittedData: any) => {
            setDateRange({
              startDate: new Date(submittedData.startDate).getTime(),
              endDate: new Date(submittedData.endDate).getTime(),
            });
            filterList[index][0] = `${column.label}: ${new Date(
              submittedData.startDate
            ).toDateString()} - ${new Date(submittedData.endDate).toDateString()}`;
            onChange(filterList[index], index, column);
            setSelectingDates(false);
          }}
        >
          <DateField name="startDate" />
          <DateField name="endDate" />
          <SubmitField />
        </AutoForm>
      </FadeInModal>
    </>
  );
};

const tableDateFilter = (dateRange, setDateRange) => ({
  logic: (data, filters) =>
    dateRange.startDate &&
    dateRange.endDate &&
    filters.length &&
    !(
      data &&
      dateRange.startDate <= new Date(data).getTime() &&
      new Date(data).getTime() <= dateRange.endDate
    )
      ? true
      : false,
  // eslint-disable-next-line react/display-name
  display: (filterList, onChange, index, column) => (
    <DateRangeSelectFade
      {...{
        label: column.label,
        setDateRange,
        filterList,
        index,
        onChange,
        column,
      }}
    />
  ),
});

export default tableDateFilter;
