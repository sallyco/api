import { useState, MouseEvent } from 'react';
import { Button, Menu, MenuItem, Typography } from '@mui/material';
import FadeInModal from '../common/FadeInModal';
import { ChevronDownIcon } from 'react-line-awesome';
import { AutoForm, DateField, SubmitField } from 'uniforms-unstyled';
import { Bridge } from '../../forms/FormBuilder';

type DateRanges = 'qtd' | 'ytd' | 'ttm' | 'day' | 'week' | 'month' | 'quarter' | 'custom';
export function getStartDate(type: DateRanges): Date {
  const dt = new Date();
  if (type === 'qtd') {
    const mm = new Date().getMonth();
    const start = Math.floor(mm / 3) * 3 + 1;
    return new Date(start + '/01/' + dt.getFullYear());
  }
  if (type === 'ytd') return new Date('01/01/' + dt.getFullYear());
  if (type === 'ttm') dt.setFullYear(dt.getFullYear() - 1);
  if (type === 'day') dt.setDate(dt.getDate() - 1);
  if (type === 'week') dt.setDate(dt.getDate() - 6);
  if (type === 'month') dt.setMonth(dt.getMonth() - 1);
  if (type === 'quarter') dt.setMonth(dt.getMonth() - 4);
  return dt;
}
export function CardHeaderAction({ classes, timeBox, setTimeBox }) {
  const dropDownArray = [
    { id: 'day', subject: 'Last Day' },
    { id: 'week', subject: 'Last Week' },
    { id: 'month', subject: 'Last Month' },
    { id: 'quarter', subject: 'Last Quarter' },
    { id: 'ttm', subject: 'Last Year ' },
    { id: 'qtd', subject: 'QTD' },
    { id: 'ytd', subject: 'YTD' },
    { id: 'custom', subject: 'Date Selection' },
  ];
  const dateRangeSelectionSchema = {
    title: 'Select dates',
    type: 'object',
    properties: {
      startDate: {
        description: 'Start Date',
      },
      endDate: {
        description: 'End Date',
      },
    },
    required: ['startDate', 'endDate'],
  };
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [selectingDates, setSelectingDates] = useState(false);
  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (value: DateRanges) => {
    if (value === 'custom') {
      setSelectingDates(true);
      setAnchorEl(null);
    } else {
      setTimeBox({
        startDate: getStartDate(value).getTime(),
        endDate: new Date().getTime(),
        text: dropDownArray.find((x) => x.id === value).subject,
        interval: value,
      });
      setAnchorEl(null);
    }
  };
  return (
    <>
      <Button onClick={handleClick} className={classes.button} endIcon={<ChevronDownIcon />}>
        {timeBox.text}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => {}}
      >
        {dropDownArray.map((e) => (
          <MenuItem key={e.id} onClick={() => handleClose(e.id as DateRanges)}>
            {e.subject}
          </MenuItem>
        ))}
      </Menu>
      <FadeInModal open={selectingDates} setOpen={setSelectingDates}>
        <Typography gutterBottom variant="h5" component="h2">
          {dateRangeSelectionSchema.title}
        </Typography>
        <AutoForm
          schema={Bridge(dateRangeSelectionSchema)}
          onSubmit={(submittedData: any) => {
            setTimeBox({
              startDate: new Date(submittedData.startDate).getTime(),
              endDate: new Date(submittedData.endDate).getTime(),
              text: 'Date Selection',
              value: 'date',
            });
            setSelectingDates(false);
          }}
        >
          <DateField name="startDate" />
          <DateField name="endDate" />
          <SubmitField />
        </AutoForm>
      </FadeInModal>
    </>
  );
}

export default CardHeaderAction;
