import React from 'react';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { useRequest } from '@packages/wired-gbt-ui';
import { TextField, ListItem, ListItemIcon, ListSubheader } from '@mui/material';
import Autocomplete, { AutocompleteRenderGroupParams } from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import _ from 'lodash';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  groupIcon: {
    maxHeight: '30px',
  },
}));

export interface DealPickerProps {
  setSelectedDeal: React.Dispatch<React.SetStateAction<object>>;
}

const DealPicker = ({ setSelectedDeal }: DealPickerProps) => {
  const classes = useStyles();
  const { data, error, isValidating } = useRequest('/deals');
  const { data: tenants } = useRequest('/tenants');

  const renderGroupLabel = (key, tenantId) => {
    const tenant = tenants.find((obj) => {
      return obj.id === tenantId;
    });
    return (
      <ListItem>
        {tenant?.assets?.images?.logo && (
          <ListItemIcon>
            <img className={classes.groupIcon} src={tenant.assets.images.logo} />
          </ListItemIcon>
        )}
        <ListSubheader key={key} component="div">
          {tenant?.name ?? tenantId}
        </ListSubheader>
      </ListItem>
    );
  };
  return (
    <Autocomplete
      options={_.orderBy(data?.data ?? [], ['tenantId', 'name'])}
      onChange={(e: any, v: any | null) => {
        setSelectedDeal(v);
      }}
      groupBy={(deal: any) => deal.tenantId}
      getOptionLabel={(deal: any) => `${deal.name} - [${deal.entity?.name}]`}
      loading={isValidating}
      renderGroup={(params: AutocompleteRenderGroupParams) => [
        renderGroupLabel(params.key, params.group),
        params.children,
      ]}
      renderInput={(params) => (
        <TextField
          {...params}
          label={isValidating ? 'Loading deals' : 'Choose a deal'}
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {isValidating ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
};

export default DealPicker;
