import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { TableWithTools } from '@packages/gbt-ui';
import makeStyles from '@mui/styles/makeStyles';

import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

interface Props {
  files: any[];
}

const useStyles = makeStyles({
  '@global': {
    '.MuiDataGrid-root': {
      border: 0,
    },
  },
  root: {},
});

const FilesTable = ({ files }: Props) => {
  const classes = useStyles();
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'name',
      label: 'Name',
      options: {
        filter: false,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
    },
    {
      name: 'type',
      label: 'Type',
    },
  ];

  const fieldMap: any = {
    id: 'string',
    name: 'string',
    type: 'string',
    key: 'string',
    lastModified: 'number',
    createdAt: 'date',
    updatedAt: 'date',
    generating: 'boolean',
    isPublic: 'boolean',
    tenantId: 'string',
  };

  const tableService = new TableWithToolsService(
    'files-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={files}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`/files/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default FilesTable;
