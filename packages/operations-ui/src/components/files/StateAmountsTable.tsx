import React from 'react';
import { TableCell, TableFooter, TablePagination, TableRow, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { TableWithTools } from '@packages/gbt-ui';
import _ from 'lodash';
import numeral from 'numeral';

interface Props {
  amountData: {
    firstInvestmentDate: Date;
    totalNASAAFee?: number;
    stateSummaries: {
      state: string;
      count: number;
      amount: number;
      fee?: number;
    }[];
  };
}

const useStyles = makeStyles({
  '@global': {
    '.MuiDataGrid-root': {
      border: 0,
    },
  },
  root: {},
});

const StateAmountsTable = ({ amountData }: Props) => {
  const { stateSummaries: data, firstInvestmentDate, totalNASAAFee } = amountData;

  const totalBlueSkyFees = (s, e) => {
    return _.sumBy(data, (value) => value.fee ?? 0);
  };

  const displayDate = firstInvestmentDate ? new Date(firstInvestmentDate).toDateString() : '';

  const getTotalFees = (blueSkyFees) => {
    return (blueSkyFees ?? 0) + (totalNASAAFee ?? 0);
  };

  return (
    <>
      <Typography>
        <strong>Date of first investment:</strong> {displayDate}
      </Typography>
      <Typography>
        <strong>NAASA Fee:</strong> {numeral(totalNASAAFee ?? 0).format('$0,0')}
      </Typography>
      <TableWithTools
        data={data}
        columnDefinitions={[
          {
            name: 'state',
            label: 'State',
            options: {
              filter: false,
            },
          },
          {
            name: 'count',
            label: 'Count',
            options: {
              filter: false,
            },
          },
          {
            name: 'method',
            label: 'Method',
          },
          {
            name: 'amount',
            label: 'Amount',
            options: {
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => numeral(value).format('$0,0'),
            },
          },
          {
            name: 'fee',
            label: 'Fee',
            options: {
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => numeral(value).format('$0,0'),
            },
          },
        ]}
        elevation={0}
        selectable={false}
        customFooter={(count, page, rowsPerPage, changeRowsPerPage, changePage) => {
          const startIndex = page * rowsPerPage;
          const endIndex = (page + 1) * rowsPerPage;
          return (
            <TableFooter>
              <TableRow>
                <TableCell colSpan={1} align={'right'} data-testid="footer-fee">
                  Total Fee:{' '}
                  {numeral(getTotalFees(totalBlueSkyFees(startIndex, endIndex))).format('$0,0')}
                </TableCell>
              </TableRow>
              <TableRow>
                <TablePagination
                  colSpan={5}
                  count={count}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onRowsPerPageChange={(event) => changeRowsPerPage(event.target.value)}
                  rowsPerPageOptions={[10, 50, 100]}
                  component={'div'}
                  onPageChange={(_, page) => changePage(page)}
                />
              </TableRow>
            </TableFooter>
          );
        }}
      />
    </>
  );
};

export default StateAmountsTable;
