import { Container } from '@mui/material';
import React, { Dispatch, SetStateAction, useContext } from 'react';
import { Form } from '@packages/gbt-ui';
import { FullScreenDialog } from '@packages/gbt-ui';
import { JSONSchema7 } from 'json-schema';
import { UiSchema } from '@rjsf/core';
import { getOmdUrl } from '../../tools';
import { useRequest, doChange } from '@packages/wired-gbt-ui';
import { TenantsContext } from '../../contexts';

interface Props {
  formOpen: boolean;
  setFormOpen: Dispatch<SetStateAction<boolean>>;
}
const UsersInviteForm = ({ formOpen, setFormOpen }: Props) => {
  const [loading, setLoading] = React.useState(false);
  const { data: tenants } = useRequest('/tenants');
  const ownTenant: any = useContext(TenantsContext);

  const onSubmit = async ({ formData }) => {
    const inviteObj = {
      acceptableBy: formData.email,
      invitedTo: [
        {
          type: 'user',
          role: 'investor',
        },
      ],
      invitedRole: formData.roles.join(','),
      tenantId: formData.tenantId,
    };

    const invite = await doChange('/invites', 'POST', inviteObj);

    let tenant = ownTenant;
    if (formData.tenantId !== ownTenant.id) {
      tenant = tenants.find((tenant) => tenant.id === formData.tenantId);
    }

    const emailData = {
      templateId: 'inviteUser',
      to: formData.email,
      subject: 'Account Invitation',
      recipient: {
        firstName: formData.firstName,
        email: formData.email,
      },
      returnUrl: getOmdUrl(tenant) + '#/register/' + invite.id,
    };
    await doChange('/email/sendEmail', 'POST', emailData, undefined, formData.tenantId);
    setFormOpen(false);
  };

  const formSchema: JSONSchema7 = {
    type: 'object',
    required: ['firstName', 'lastName', 'email', 'tenantId'],
    properties: {
      firstName: {
        type: 'string',
        title: 'First Name',
        minLength: 2,
      },
      lastName: {
        type: 'string',
        title: 'Last Name',
        minLength: 2,
      },
      email: {
        type: 'string',
        title: 'email',
        format: 'email',
        minLength: 2,
      },
      roles: {
        type: 'array',
        minItems: 1,
        title: 'Enabled Roles',
        items: {
          type: 'string',
          oneOf: [
            { enum: ['organizer'], title: 'Organizer' },
            { enum: ['investor'], title: 'Investor' },
          ],
        },
        uniqueItems: true,
      },
      tenantId: {
        title: 'Tenant',
        type: 'string',
        description: 'Tenant to Invite User to',
        oneOf: [
          ...(!Array.isArray(tenants) || !tenants.length ? [ownTenant] : tenants).map((tenant) => {
            return {
              enum: [tenant.id],
              title: `${tenant.name} - ${tenant.id}`,
            };
          }),
        ],
      },
    },
  };

  const uiSchema: UiSchema = {
    firstName: {
      'ui:placeholder': 'John',
      'ui:autofocus': true,
    },
    lastName: {
      'ui:placeholder': 'Doe',
    },
  };

  return (
    <FullScreenDialog
      title={'Invite User'}
      open={formOpen}
      setOpen={() => setFormOpen(false)}
      loading={loading}
      setLoading={setLoading}
    >
      <Container>
        <Form
          schema={formSchema}
          uiSchema={uiSchema}
          onSubmit={onSubmit}
          liveValidate
          formData={{}}
        />
      </Container>
    </FullScreenDialog>
  );
};
export default UsersInviteForm;
