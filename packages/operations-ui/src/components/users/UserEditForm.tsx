import React from 'react';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  FormControlLabel,
  TextField,
  Typography,
} from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { User } from '../../api/usersApi';
import { SxProps } from '@mui/system';

interface Props {
  user: User;
  mutateUser?: any;
  onSuccess: (userData) => void;
}

const textFieldStyle: SxProps = {
  margin: '0px 0px 20px 0px',
};

const UserEditForm = ({ user, mutateUser = null, onSuccess }: Props) => {
  const validationSchema = Yup.object().shape({});

  const initialValues = {
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    enabled: user.enabled,
    attributes: user.attributes,
  };

  const onSubmit = (data) => {
    onSuccess(data);
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: false,
    validateOnBlur: true,
    onSubmit,
  });

  const isTouchedAndHaveError = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        )
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey])))
      );
    }
  };

  const getErrorText = (key, subKey = undefined, isArray = false, index = 0) => {
    if (isArray) {
      return (
        Boolean(
          formik.touched[key] &&
            formik.touched[key][index] &&
            (!subKey || (subKey && formik.touched[key][index][subKey]))
        ) &&
        Boolean(
          formik.errors[key] &&
            formik.errors[key][index] &&
            (!subKey || (subKey && formik.errors[key][index][subKey]))
        ) &&
        (!subKey ? formik.errors[key][index] : formik.errors[key][index][subKey])
      );
    } else {
      return (
        Boolean(formik.touched[key] && (!subKey || (subKey && formik.touched[key][subKey]))) &&
        Boolean(formik.errors[key] && (!subKey || (subKey && formik.errors[key][subKey]))) &&
        (!subKey ? formik.errors[key] : formik.errors[key][subKey])
      );
    }
  };

  return (
    <>
      <Card>
        <CardHeader title={<Typography variant="h4">Edit User</Typography>} />
        <Divider />
        <CardContent>
          <form
            onSubmit={formik.handleSubmit}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          >
            <TextField
              label="First Name"
              id="firstName"
              name="firstName"
              type="text"
              fullWidth
              sx={textFieldStyle}
              value={formik.values.firstName}
              error={isTouchedAndHaveError('firstName')}
              helperText={getErrorText('firstName')}
            />
            <TextField
              label="Last Name"
              id="lastName"
              name="lastName"
              type="text"
              fullWidth
              sx={textFieldStyle}
              value={formik.values.lastName}
              error={isTouchedAndHaveError('lastName')}
              helperText={getErrorText('lastName')}
            />
            <TextField
              label="Email"
              id="email"
              name="email"
              type="text"
              fullWidth
              sx={textFieldStyle}
              value={formik.values.email}
              error={isTouchedAndHaveError('email')}
              helperText={getErrorText('email')}
            />

            {!!formik.values.attributes?.phone?.length &&
              formik.values.attributes.phone.map((phone, index) => (
                <TextField
                  label="Phone"
                  id={`attributes.phone.${index}`}
                  name={`attributes.phone.${index}`}
                  type="text"
                  fullWidth
                  sx={textFieldStyle}
                  value={phone}
                  key={index}
                />
              ))}
            <FormControlLabel
              control={<Checkbox id="enabled" name="enabled" checked={formik.values.enabled} />}
              label="User is enabled"
              sx={textFieldStyle}
            />
            <Button
              color="primary"
              fullWidth
              type="submit"
              disabled={!formik.isValid}
              data-testid="submit-button"
            >
              Update User
            </Button>
          </form>
        </CardContent>
      </Card>
    </>
  );
};

export default UserEditForm;
