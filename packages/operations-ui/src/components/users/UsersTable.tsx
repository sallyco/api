import React, { useEffect, useState, useContext } from 'react';
import { User } from '../../api/usersApi';

import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import { IconButton } from '@mui/material';
import { ExternalLinkSquareAltIcon } from 'react-line-awesome';
import { startImpersonateSession } from '../../services/ImpersonateService';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';
import { TenantsContext } from '../../contexts/TenantsContext';

interface Props {
  users: User[];
  isValidating: boolean;
}

const UsersTable = ({ users, isValidating }: Props) => {
  const tenant: any = useContext(TenantsContext);
  const router = useRouter();
  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    ...(tenant.id === 'master'
      ? [
          {
            name: 'tenantId',
            label: 'Tenant',
          },
        ]
      : []),
    {
      name: 'username',
      label: 'Username',
      options: {
        filter: true,
      },
    },
    {
      name: 'firstName',
      label: 'First Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'lastName',
      label: 'Last Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'attributes.phone',
      label: 'Phone',
      options: {
        filter: true,
        customBodyRender: (value, tableMeta, updateValue) => (value ? value[0] : 'Not Set'),
      },
    },
    {
      name: 'id',
      label: 'Impersonate',
      options: {
        filter: false,
        viewColumns: false,
        customBodyRender: function Rn(value, tableMeta, updateValue) {
          return (
            <IconButton
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                startImpersonateSession(
                  tenant.id === 'master' ? tableMeta.rowData[1] : tenant.id,
                  value,
                  'dashboard'
                );
              }}
              size="large"
            >
              <ExternalLinkSquareAltIcon />
            </IconButton>
          );
        },
      },
    },
  ];

  const fieldMap: any = {
    id: 'string',
    username: 'string',
    enabled: 'boolean',
    totp: 'boolean',
    emailVerified: 'boolean',
    firstName: 'string',
    lastName: 'string',
    phoneNumber: 'string',
    email: 'string',
    notBefore: 'number',
    createdTimestamp: 'string',
    type: 'string',
    tenantId: 'string',
  };

  const actionFieldLabels = [{ name: 'id', label: 'Impersonate' }];

  const tableService = new TableWithToolsService(
    'users-table-columns',
    fieldMap,
    initialColumnDefinitions,
    actionFieldLabels
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <TableWithTools
      data={users}
      columnDefinitions={columnDefinitions}
      onRowClick={(r, m) => {
        router.push(`users/${r[0]}`);
      }}
      onViewColumnsChange={onViewColumnsChange}
      enableNestedDataAccess={'.'}
      elevation={0}
      selectable={false}
      clickable
      showLoading={true}
      isLoading={isValidating}
    />
  );
};

export default UsersTable;
