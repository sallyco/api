import React, { useState } from 'react';
import { ChevronRightIcon, ChevronLeftIcon, InfoCircleIcon } from 'react-line-awesome';
import {
  Grid,
  Fab,
  Checkbox,
  Card,
  CardHeader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  Tooltip,
  Divider,
} from '@mui/material';
import { ContentLoader } from '@packages/gbt-ui';
import { useRequest, doChange, objectProps } from '@packages/wired-gbt-ui';

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}
function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}
function union(a, b) {
  return [...a, ...not(b, a)];
}
interface Props {
  id: string;
}
const UserRoleMappings = ({ id }: Props) => {
  const op = objectProps.users;
  const baseUrl = `${op.route}/${id}/role-mappings`;
  const { data, mutate, isValidating } = useRequest(baseUrl);

  const [checked, setChecked] = useState([]);
  const leftChecked = intersection(checked, data?.available);
  const rightChecked = intersection(checked, data?.roleMappings);
  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };
  const numberOfChecked = (items) => intersection(checked, items).length;
  const handleToggleAll = (items) => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items));
    } else {
      setChecked(union(checked, items));
    }
  };
  const handleCheckedRight = () => {
    console.log(leftChecked);
    doChange(baseUrl, 'POST', { roles: leftChecked }, mutate);
    setChecked(not(checked, leftChecked));
  };
  const handleCheckedLeft = () => {
    doChange(baseUrl, 'DELETE', { roles: rightChecked }, mutate);
    setChecked(not(checked, rightChecked));
  };
  const selectList = (title, items) => (
    <Card>
      <CardHeader
        avatar={
          <Checkbox
            onClick={handleToggleAll(items)}
            checked={numberOfChecked(items) === items.length && items.length !== 0}
            indeterminate={numberOfChecked(items) !== items.length && numberOfChecked(items) !== 0}
            disabled={items.length === 0}
          />
        }
        title={title}
      />
      <Divider />
      <List dense role="list">
        {items.map((value) => {
          const labelId = 'transfer-list-all-item';
          return (
            <ListItem key={value.id} role="listitem" button onClick={handleToggle(value)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={value.name} />
              <ListItemSecondaryAction>
                <Tooltip title={value.description} arrow>
                  <InfoCircleIcon />
                </Tooltip>
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );
  const staticList = (title, items) => (
    <Card>
      <CardHeader title={title} />
      <Divider />
      <List dense role="list">
        {items.map((value) => {
          const labelId = 'transfer-list-all-item';
          return (
            <ListItem key={value.id} role="listitem">
              <ListItemText id={labelId} primary={value.name} />
              <ListItemSecondaryAction>
                <Tooltip title={value.description} arrow>
                  <InfoCircleIcon />
                </Tooltip>
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );
  return (
    <React.Fragment>
      {!data || isValidating ? (
        <ContentLoader />
      ) : (
        <Grid container spacing={4} justifyContent="center" alignItems="center">
          <Grid item>{selectList('Available', data.available)}</Grid>
          <Grid item>
            <Grid container direction="column" alignItems="center">
              <Fab
                color="primary"
                size="small"
                onClick={handleCheckedRight}
                disabled={leftChecked.length === 0}
              >
                <ChevronRightIcon />
              </Fab>
              <Fab
                color="primary"
                size="small"
                onClick={handleCheckedLeft}
                disabled={rightChecked.length === 0}
              >
                <ChevronLeftIcon />
              </Fab>
            </Grid>
          </Grid>
          <Grid item>{selectList('Assigned', data.roleMappings)}</Grid>
          <Grid item>{staticList('Composite', data.composite)}</Grid>
        </Grid>
      )}
    </React.Fragment>
  );
};

export default UserRoleMappings;
