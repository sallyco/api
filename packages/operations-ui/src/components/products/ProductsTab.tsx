import React from 'react';
import { useRequest, objectProps, EditModal, CreateButton } from '@packages/wired-gbt-ui';
import { IconButton } from '@mui/material';
import { TrashIcon, BinocularsIcon } from 'react-line-awesome';

import { ContentLoader, TableWithTools } from '@packages/gbt-ui';
import AuditLogTableModel from '../audit-log/AuditLogTableModal';

function ProductsTab() {
  const op = objectProps.products;
  const { data, mutate, isValidating, error } = useRequest(op.route);
  const [editOpen, setEditOpen] = React.useState(false);
  const [auditOpen, setAuditOpen] = React.useState(false);
  const [id, setId] = React.useState<string | undefined>(undefined);

  return (
    <React.Fragment>
      {!data ? (
        <ContentLoader errorMessage={error?.message} />
      ) : (
        <React.Fragment>
          <AuditLogTableModel id={id} open={auditOpen} setOpen={setAuditOpen} />
          <EditModal op={op} open={editOpen} setOpen={setEditOpen} id={id} mutate={mutate} />
          <TableWithTools
            title={<CreateButton op={op} mutate={mutate} />}
            selectable={false}
            data={data}
            elevation={0}
            clickable
            showLoading={true}
            isLoading={isValidating}
            onRowClick={(r, m) => {
              setId(r[0]);
              setEditOpen(true);
            }}
            columnDefinitions={[
              {
                name: 'id',
                label: 'ID',
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: 'name',
                label: 'Title',
                options: {
                  filter: false,
                },
              },
              {
                name: 'description',
                label: 'Description',
                options: {
                  filter: false,
                },
              },
              {
                name: 'doAudit',
                label: 'Audit',
                options: {
                  setCellProps: () => ({
                    style: {
                      padding: '0 0 0 10px',
                      width: '80px',
                      maxWidth: '80px',
                    },
                  }),
                  customBodyRender: function Rn(value, tableMeta) {
                    return (
                      <IconButton
                        onClick={(e) => {
                          e.preventDefault();
                          e.stopPropagation();
                          setId(tableMeta.rowData[0]);
                          setAuditOpen(true);
                        }}
                        size="large"
                      >
                        <BinocularsIcon />
                      </IconButton>
                    );
                  },
                },
              },
              {
                name: 'actions',
                label: 'Delete',
                options: {
                  setCellProps: () => ({
                    style: {
                      padding: '0 0 0 10px',
                      width: '80px',
                      maxWidth: '80px',
                    },
                  }),
                  customBodyRender: function Rn(value, tableMeta) {
                    return (
                      <IconButton
                        onClick={(e) => {
                          e.preventDefault();
                          e.stopPropagation();
                        }}
                        size="large"
                      >
                        <TrashIcon />
                      </IconButton>
                    );
                  },
                },
              },
            ]}
          />
        </React.Fragment>
      )}
    </React.Fragment>
  );
}

export default ProductsTab;
