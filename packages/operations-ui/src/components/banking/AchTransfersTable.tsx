import React from 'react';
import { Transaction } from '../../api/transactionsApi';
import { TableWithTools } from '@packages/gbt-ui';
import { useRouter } from 'next/router';
import numeral from 'numeral';

interface Props {
  transactions: Transaction[];
}

const AchTransfersTable = ({ transactions }: Props) => {
  const router = useRouter();

  return (
    <TableWithTools
      data={transactions}
      columnDefinitions={[
        {
          name: 'id',
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: 'tenantId',
          label: 'Tenant',
        },
        {
          name: 'dealId',
          label: 'Deal ID',
          options: {
            display: false,
          },
        },
        {
          name: 'subscriptionId',
          label: 'Subscription ID',
          options: {
            display: false,
          },
        },
        {
          name: 'type',
          label: 'Type',
        },
        {
          name: 'direction',
          label: 'Direction',
        },
        {
          name: 'status',
          label: 'Status',
        },
        {
          name: 'amount',
          label: 'Amount',
          options: {
            customBodyRender: (value, tableMeta, updateValue) => numeral(value).format('$0,0'),
          },
        },
      ]}
      onRowClick={(r, m) => {
        router.push(`ach-transfers/${r[0]}`);
      }}
      elevation={0}
      selectable={false}
      clickable
    />
  );
};

export default AchTransfersTable;
