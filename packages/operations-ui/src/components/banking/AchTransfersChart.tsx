import React from 'react';
import { Transaction } from '../../api/transactionsApi';
import {
  Grid,
  LinearProgress,
  Card,
  CardHeader,
  CardContent,
  Button,
  Menu,
  MenuItem,
} from '@mui/material';

import dynamic from 'next/dynamic';
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { ChevronDownIcon } from 'react-line-awesome';

import _ from 'lodash';
import moment from 'moment';
import numeral from 'numeral';

interface Props {
  transactions: Transaction[];
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  current: {
    marginTop: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(1),
  },
  gridItem: {
    paddingBottom: '1.5rem',
    paddingRight: '1rem',
    paddingLeft: '1rem',
  },
  gridItemLabel: {
    paddingBottom: '1.5rem',
    color: '#7a7b97',
  },
  gridItemValue: {
    display: 'block',
    fontSize: '1.15rem',
  },
  gridItemProgressValue: {
    display: 'flex',
    fontSize: '1.15rem',
    alignItems: 'center',
  },
  gridItemProgressBar: {
    borderRadius: '30px',
    height: '0.52rem',
    flexGrow: 1,
  },
  gridItemProgressBarValue: {
    paddingLeft: '1rem',
  },
}));

const AchTransfersChart = ({ transactions }: Props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [timeBox, setTimeBox] = React.useState<moment.unitOfTime.DurationConstructor>('day');

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (value: moment.unitOfTime.DurationConstructor) => {
    setTimeBox(value);
    setAnchorEl(null);
  };

  const lastTime = moment().subtract(1, timeBox);

  const recentTrans = _.filter(transactions, function (entry) {
    return (
      moment(entry.addedToNetwork, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true).local().isAfter(lastTime) &&
      (entry.status === 'SENT' || entry.status === 'PENDING')
    );
  });
  const queuedTrans = _.filter(transactions, function (entry) {
    return entry.status === 'QUEUED';
  });

  const totalLife = _.sumBy(transactions, function (entry) {
    return entry.amount;
  });
  const totalRecent = _.sumBy(recentTrans, function (entry) {
    return entry.amount;
  });
  const capacityUsage = totalRecent / 400000;
  const totalQueued = _.sumBy(queuedTrans, function (entry) {
    return entry.amount;
  });

  const recentDataSeries = _(recentTrans)
    .groupBy((entry) => {
      return moment(entry.addedToNetwork, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
        .local()
        .startOf(timeBox == 'day' ? 'hour' : 'day');
    })
    .map((entry, id) => [id, _.sumBy(entry, 'amount')])
    .value();

  //  const chartOptions = {
  //    chart: {
  //      toolbar: {
  //        show: false,
  //      },
  //      sparkline: {
  //        enabled: false,
  //      },
  //    },
  //    dataLabels: {
  //      enabled: false,
  //    },
  //    grid: {
  //      strokeDashArray: '5',
  //      borderColor: 'rgba(125, 138, 156, 0.3)',
  //    },
  //    stroke: {
  //      show: true,
  //      width: 2,
  //      colors: ['transparent'],
  //    },
  //    tooltip: {
  //      x: {
  //        format: timeBox == 'day' ? 'dd MMM yyyy : HH:mm' : 'dd MMM yyyy',
  //      },
  //    },
  //    fill: {
  //      color: '#3c44b1',
  //      gradient: {
  //        shade: 'light',
  //        type: 'vertical',
  //        shadeIntensity: 0.2,
  //        inverseColors: false,
  //        opacityFrom: 0.8,
  //        opacityTo: 0,
  //        stops: [0, 100],
  //      },
  //    },
  //    colors: ['#3c44b1'],
  //    legend: {
  //      show: false,
  //    },
  //    xaxis: {
  //      type: 'datetime',
  //      min: lastTime.valueOf(),
  //      tickAmount: 6,
  //      labels: {
  //        datetimeFormatter: {
  //          year: 'yyyy',
  //          month: "MMM 'yy",
  //          day: 'dd MMM',
  //          hour: 'HH:mm',
  //        },
  //      },
  //    },
  //    yaxis: {
  //      labels: {
  //        formatter: function (val, index) {
  //          return numeral(val).format('$0,0 a');
  //        },
  //      },
  //    },
  //  };
  const chartsLarge3Data = [
    {
      name: 'Processed',
      data: recentDataSeries,
    },
  ];

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <React.Fragment>
            <Button onClick={handleClick} className={classes.button} endIcon={<ChevronDownIcon />}>
              Last {timeBox}
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={() => handleClose('day')}>Last Day</MenuItem>
              <MenuItem onClick={() => handleClose('week')}>Last Week</MenuItem>
              <MenuItem onClick={() => handleClose('month')}>Last Month</MenuItem>
            </Menu>
          </React.Fragment>
        }
        classes={{ action: classes.current }}
        subheader="Recent ACH transfer request activity"
        subheaderTypographyProps={{ color: 'textSecondary', variant: 'caption' }}
        title="Activity Summary"
        titleTypographyProps={{ color: 'textPrimary' }}
      />
      <CardContent className={classes.content}>
        <Grid container spacing={6}>
          <Grid item sm={6} md={5}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total last {timeBox}</span>
              <span className={classes.gridItemValue}>
                <small>$</small> {numeral(totalRecent).format('0,0')}
              </span>
            </div>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total queued</span>
              <span className={classes.gridItemValue}>
                <small>$</small> {numeral(totalQueued).format('0,0')}
              </span>
            </div>
          </Grid>
          <Grid item sm={6} md={7}>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Total lifetime</span>
              <span className={classes.gridItemValue}>
                <small>$</small> {numeral(totalLife).format('0,0')}
              </span>
            </div>
            <div className={classes.gridItem}>
              <span className={classes.gridItemLabel}>Capacity usage</span>
              <span className={classes.gridItemProgressValue}>
                <LinearProgress
                  variant="determinate"
                  value={capacityUsage > 1 ? 100 : capacityUsage * 100}
                  className={classes.gridItemProgressBar}
                />
                <span className={classes.gridItemProgressBarValue}>
                  {numeral(capacityUsage).format('0%')}
                </span>
              </span>
            </div>
          </Grid>
        </Grid>
        <Chart series={chartsLarge3Data} type="area" height={317} />
      </CardContent>
    </Card>
  );
};
//options={chartOptions}

export default AchTransfersChart;
