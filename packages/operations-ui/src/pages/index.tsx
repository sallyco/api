import React from 'react';
import { useRouter } from 'next/router';

interface Props {
  next: string | null;
}

const Login = ({ next = null }: Props) => {
  const router = useRouter();
  const redirectPath = next ?? '/dashboard';

  router.push(redirectPath);

  //  signIn('oauth2', { callbackUrl: `${url}/dashboard` })

  return <></>;
};

Login.getInitialProps = async ({ query }) => {
  const next = query.next;
  return { next };
};

export default Login;
