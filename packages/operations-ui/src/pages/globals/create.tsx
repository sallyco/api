import React from 'react';
import useSWR from 'swr';
import { NextPage } from 'next';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader } from '@packages/gbt-ui';
import { Form } from '@packages/gbt-ui';

const fetcher = (url) => fetch(url).then((res) => res.json());
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

import { ContentLoader } from '@packages/gbt-ui';

import { Container } from '@mui/material';

const CreateDefinition: NextPage = () => {
  const metadataUrl = baseUrl + '/metadata/definition/create/describe';
  const createUrl = baseUrl + '/definitions';
  const { data, error } = useSWR(metadataUrl, fetcher);
  const onSubmit = ({ formData }) =>
    fetch(createUrl, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formData),
    });

  return (
    <LoggedInLayout title={'Create Definition'}>
      {!data ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader title={'Create New Definition'} />
            <Form schema={data.definitions.DefinitionWrite} onSubmit={onSubmit} />
          </Container>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default CreateDefinition;
