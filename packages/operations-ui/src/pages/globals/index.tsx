import React from 'react';
import { useRouter } from 'next/router';
import { useRequest, objectProps, CreateButton } from '@packages/wired-gbt-ui';
import { NextPage } from 'next';
import Link from 'next/link';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { ContentLoader, TitleHeader, TableWithTools } from '@packages/gbt-ui';

import { Container, Box } from '@mui/material';

const Globals: NextPage = () => {
  const router = useRouter();
  const op = objectProps.globals;
  const { data, error } = useRequest(op.route);

  return (
    <LoggedInLayout title={op.label}>
      {!data ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Global Data Definitions'}>
              <Link href={`/globals/create`} passHref>
                <CreateButton op={op} />
              </Link>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <TableWithTools
              data={data}
              columnDefinitions={[
                {
                  name: 'key',
                  options: {
                    display: false,
                    filter: false,
                  },
                },
                {
                  name: 'category',
                  label: 'Category',
                  options: {
                    filter: false,
                  },
                },
                {
                  name: 'entry',
                  label: 'Entry',
                },
              ]}
              onRowClick={(r, m) => {
                router.push(`definitions/${r[0]}`);
              }}
              elevation={0}
              selectable={false}
              clickable
            />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Globals;
