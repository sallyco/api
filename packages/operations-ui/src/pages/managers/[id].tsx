import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { ButtonGroup, Container } from '@mui/material';

import { BasicTabs, ContentLoader, TitleHeader, DeletePopup, InfoCard } from '@packages/gbt-ui';
import { useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { SoftDeleteById } from '../../services/DeleteService';

import AuditLogTable from '../../components/audit-log/AuditLogTable';
import CompanyDocumentsTable from '../../components/companies/CompanyDocumentsTable';

const ManagerDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);

  const op = objectProps.managers;
  const { data: company, mutate } = useRequest(`/companies/${id}`);

  const isDisabled =
    company?.tenantId === '' || company?.tenantId === null || !company?.tenantId ? true : false;

  return (
    <LoggedInLayout title={`${company?.name ?? 'Loading'} ~ Companies`}>
      {!company ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={company?.name}>
              <ButtonGroup color="secondary" variant="outlined" size="small">
                <EditButton op={{ ...op, route: '/companies' }} id={id} mutate={mutate} />
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'companies-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      title={'Company Details'}
                      item={company}
                      icon={op.icon}
                      omitFields={['feesAccount']}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Documents',
                tabContent: (
                  <Container maxWidth={false}>
                    <CompanyDocumentsTable company={company} />
                  </Container>
                ),
              },
              {
                tabName: 'Audit',
                tabContent: <AuditLogTable id={id} />,
              },
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={company?.id}
                    objectType={'Company'}
                    name={company?.name}
                    processing={processing}
                    onClickOk={async () => {
                      setProcessing(true);
                      await SoftDeleteById(company?.id, 'company');
                      router.push('/companies');
                      setProcessing(false);
                    }}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default ManagerDetails;
