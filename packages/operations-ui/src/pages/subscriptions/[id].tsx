import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { SnackbarUtils, useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';
import { FilePdfIcon } from 'react-line-awesome';
import { Box, Button, ButtonGroup, Container, Grid } from '@mui/material';

import { BasicTabs, ContentLoader, TitleHeader, DeletePopup, InfoCard } from '@packages/gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import SubscriptionBanking from '../../components/subscriptions/SubscriptionBanking';
import ImpersonateButton from '../../components/common/ImpersonateButton';
import FileUploader from '../../components/common/FileUploader';
import { SoftDeleteById } from '../../services/DeleteService';
import FilesTable from '../../components/files/FilesTable';
import useSWR from 'swr';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;
const multiFetcher = (...urls) => {
  const f = (u) => fetch(u).then((r) => r.json());
  if (urls.length > 0) {
    return Promise.all(urls.map(f));
  }
  return f(urls);
};

const SubscriptionDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);

  const op = objectProps.subscriptions;
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: 'profile',
          scope: {
            fields: ['id', 'name', 'firstName', 'lastName', 'email'],
          },
        },
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name'],
          },
        },
        {
          relation: 'owner',
        },
      ],
    })
  );
  const { data: subscription, mutate } = useRequest(`${op.route}/${id}?filter=${encodedFilter}`);
  const { data: files, error } = useSWR(
    !subscription?.files?.length ? null : subscription.files.map((f) => `${baseUrl}/files/${f}`),
    multiFetcher
  );

  const LinkFields = new Map();
  LinkFields.set('dealId', { value_field: 'deal', url_field: 'deals' });
  LinkFields.set('profileId', {
    value_field: 'profile',
    url_field: 'profiles',
  });
  LinkFields.set('ownerId', {
    value_field: 'owner',
    url_field: 'users',
  });
  ///

  const downloadStatement = async () => {
    const response = await fetch(`${baseUrl}/subscriptions/${id}/capital-account-statement/pdf`);
    const contentLength = response.headers.get('content-length');
    const total = parseInt(contentLength, 10);
    let loaded = 0;
    const res = new Response(
      new ReadableStream({
        async start(controller) {
          const reader = response.body.getReader();
          for (;;) {
            const { done, value } = await reader.read();
            if (done) break;
            loaded += value.byteLength;

            controller.enqueue(value);
          }
          controller.close();
        },
      })
    );
    const blob = await res.blob();
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'statement.pdf';
    document.body.appendChild(link);
    link.click();
  };
  ///
  const isDisabled =
    subscription?.tenantId === '' || subscription?.tenantId === null || !subscription?.tenantId
      ? true
      : false;

  const getFiles = () => {};

  return (
    <LoggedInLayout title={`${subscription?.name ?? 'Loading'} ~ Subscriptions`}>
      {!subscription ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={subscription?.name}>
              <ButtonGroup>
                <EditButton op={op} id={id} mutate={mutate} />
                <ImpersonateButton
                  tenantId={subscription?.tenantId}
                  ownerId={subscription?.ownerId}
                />
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'subscriptions-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      icon={op.icon}
                      title={'Subscription Details'}
                      item={subscription}
                      linkfields={LinkFields}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Banking',
                tabContent: (
                  <Container maxWidth={false}>
                    <SubscriptionBanking subscription={subscription} />
                  </Container>
                ),
              },
              {
                tabName: 'Documents',
                tabContent: (
                  <Container maxWidth={false}>
                    <Grid container spacing={2}>
                      <Grid item sm={8} xl={9} xs={12}>
                        <Button
                          onClick={downloadStatement}
                          color="primary"
                          variant="contained"
                          startIcon={<FilePdfIcon />}
                          fullWidth
                        >
                          Export Capital Account Statement
                        </Button>
                        <Box mt={2}>
                          <FilesTable files={files ?? []} />
                        </Box>
                      </Grid>
                      <Grid item sm={4} xl={3} xs={12}>
                        <Box pb={2}>
                          <FileUploader
                            objectType={'subscriptions'}
                            objectId={subscription.id}
                            tenantId={subscription.tenantId}
                            omitFields={[]}
                            uploadSuccess={async (fileIds) => {
                              //await dispatch();
                              //  fetchSubscriptionById(subscription.id)
                              SnackbarUtils.success(
                                `File${fileIds.length > 1 ? 's' : ''} uploaded to subscription`
                              );
                            }}
                          />
                        </Box>
                      </Grid>
                    </Grid>
                  </Container>
                ),
              },
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={subscription?.id}
                    objectType={'Subscription'}
                    name={subscription?.name}
                    processing={processing}
                    onClickOk={async () => {
                      setProcessing(true);
                      await SoftDeleteById(subscription?.id, 'subscription');
                      router.push('/subscriptions');
                      setProcessing(false);
                    }}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default SubscriptionDetails;
