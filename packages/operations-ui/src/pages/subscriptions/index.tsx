import React from 'react';
import { Container, Box } from '@mui/material';
import { useRouter } from 'next/router';
import { NextPage } from 'next';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import SubscriptionsTable from '../../components/subscriptions/SubscriptionsTable';

const Subscriptions: NextPage = () => {
  const router = useRouter();
  const op = objectProps.subscriptions;

  const { data: subs } = useRequest(`${op.route}?filter[include][]=profile`);

  return (
    <LoggedInLayout title={'Subscriptions'}>
      {!subs ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Subscriptions'}></TitleHeader>
          </Container>
          <Box mt={2}>
            <SubscriptionsTable subscriptions={subs.data} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Subscriptions;
