import React from 'react';
import { Theme } from '@mui/material/styles';

import makeStyles from '@mui/styles/makeStyles';

import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { TitleHeader, BasicTabs } from '@packages/gbt-ui';

import { Container } from '@mui/material';
import { objectProps } from '@packages/wired-gbt-ui';

import * as QuestComp from '../components/questions';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  icon: {
    fontSize: 'xx-large',
  },
}));
const QuestionsPage = () => {
  const classes = useStyles();
  const op = objectProps.questions;
  return (
    <LoggedInLayout title={op.label}>
      <Container maxWidth={false}>
        <TitleHeader title={op.label} />
      </Container>
      <BasicTabs
        tabName={'questions-ui-tab'}
        headerColor="primary"
        tabs={[
          {
            tabName: objectProps.questionaires.label,
            tabIcon: <span className={classes.icon}>{objectProps.questionaires.icon}</span>,
            tabContent: <QuestComp.QuestionairesTab />,
          },
          {
            tabName: op.label,
            tabIcon: <span className={classes.icon}>{op.icon}</span>,
            tabContent: <QuestComp.QuestionsTab />,
          },
          {
            tabName: objectProps['questionaire-responses'].label,
            tabIcon: (
              <span className={classes.icon}>{objectProps['questionaire-responses'].icon}</span>
            ),
            tabContent: <QuestComp.QuestionaireResponsesTab />,
          },
        ]}
      />
    </LoggedInLayout>
  );
};

export default QuestionsPage;
