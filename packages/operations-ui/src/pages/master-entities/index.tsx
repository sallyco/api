import React from 'react';
import { NextPage } from 'next';

import { Container, Box } from '@mui/material';

import { ContentLoader, TitleHeader } from '@packages/gbt-ui';
import { useRequest, objectProps, CreateButton } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import CompaniesTable from '../../components/companies/CompaniesTable';

const Managers: NextPage = () => {
  const op = objectProps['master-entities'];
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      where: {
        type: 'MASTER_ENTITY',
      },
    })
  );
  const { data: companies, isValidating } = useRequest(
    `/companies?page=0&perPage=100&filter=${encodedFilter}`
  );

  return (
    <LoggedInLayout title={`${op.label}`}>
      {!companies ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={op.label}>
              <CreateButton op={op} />
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <CompaniesTable companies={companies.data} type={op.name} isValidating={isValidating} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Managers;
