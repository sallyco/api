import React from 'react';
import { useRouter } from 'next/router';
import { useRequest, doChange, objectProps, ToggleField } from '@packages/wired-gbt-ui';
import { NextPage } from 'next';

import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader, TableWithTools, ConfirmationDialog } from '@packages/gbt-ui';
import { BanIcon, CheckCircleIcon, PencilIcon } from 'react-line-awesome';

import numeral from 'numeral';

import { Typography, Container, Box, IconButton, Tooltip } from '@mui/material';
import { AutoForm, NumField, SubmitField } from 'uniforms-unstyled';
import FadeInModal from '../components/common/FadeInModal';
import { Bridge } from '../forms/FormBuilder';
import { ModelTransformMode } from 'uniforms';

const TransferApprovals: NextPage = () => {
  const router = useRouter();
  const op = objectProps['transfer-approvals'];
  const { data, isValidating, mutate } = useRequest(
    `${op.route}?filter[where][needsApproval]=true&filter[where][status]=QUEUED&filter[include][]=deal&filter[include][]=close`
  );

  const [viewAmountEdit, setViewAmountEdit] = React.useState(false);
  const [confirmCancel, setConfirmCancel] = React.useState(false);
  const [selectedId, setSelectedId] = React.useState(null);

  const amountEditSchema = {
    title: 'Edit Amount',
    type: 'object',
    properties: {
      amount: {
        description: 'Amount',
        type: 'number',
        exclusiveMinimum: 0,
      },
    },
    required: ['amount'],
  };

  return (
    <LoggedInLayout title={`${op.label}`}>
      {!data ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={op.label} />
          </Container>
          <Box mt={2}>
            <TableWithTools
              data={data}
              columnDefinitions={[
                {
                  name: 'id',
                  label: 'Id',
                  options: {
                    display: false,
                    filter: false,
                  },
                },
                {
                  name: 'deal',
                  label: 'Deal',
                  options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => value.name,
                  },
                },
                {
                  name: 'tenantId',
                  label: 'Tenant',
                },
                {
                  name: 'type',
                  label: 'Type',
                },
                {
                  name: 'amount',
                  label: 'Amount',
                  options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) =>
                      numeral(value).format('$0,0'),
                  },
                },
                {
                  name: 'actions',
                  label: 'Actions',
                  options: {
                    filter: false,
                    customBodyRender: function Rn(value, tableMeta, updateValue) {
                      return (
                        <>
                          <Tooltip title={'Approve'} placement="bottom" arrow>
                            <ToggleField
                              objectKey={'transactions'}
                              objectId={tableMeta.rowData[0]}
                              fieldName={'needsApproval'}
                              newValue={false}
                              mutate={mutate}
                              icon={<CheckCircleIcon />}
                            />
                          </Tooltip>
                          <Tooltip title={'Cancel'} placement="bottom" arrow>
                            <IconButton
                              color="primary"
                              onClick={(e) => {
                                e.preventDefault();
                                e.stopPropagation();
                                setSelectedId(tableMeta.rowData[0]);
                                setConfirmCancel(true);
                              }}
                              size="large"
                            >
                              <BanIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title={'Edit'} placement="bottom" arrow>
                            <IconButton
                              color="primary"
                              onClick={(e) => {
                                e.preventDefault();
                                e.stopPropagation();
                                setSelectedId(tableMeta.rowData[0]);
                                setViewAmountEdit(true);
                              }}
                              size="large"
                            >
                              <PencilIcon />
                            </IconButton>
                          </Tooltip>
                        </>
                      );
                    },
                  },
                },
              ]}
              onRowClick={(r, m) => {
                router.push(`financial-transactions/${r[0]}`);
              }}
              elevation={0}
              selectable={false}
              filter={false}
              download={false}
              clickable
            />
          </Box>
        </React.Fragment>
      )}

      {amountEditSchema && (
        <FadeInModal open={viewAmountEdit} setOpen={setViewAmountEdit}>
          <Typography gutterBottom variant="h5" component="h2">
            {amountEditSchema.title}
          </Typography>
          <AutoForm
            schema={Bridge(amountEditSchema)}
            onSubmit={(submittedData: any) => {
              setViewAmountEdit(false);
              doChange(
                `/transactions/${selectedId}`,
                'PUT',
                { amount: submittedData.amount },
                mutate
              );
              setSelectedId(null);
            }}
            modelTransform={(mode: ModelTransformMode, model: any) => {
              if (mode === 'validate' || mode === 'submit') {
                const { amount } = model || {};
                const newModel = {
                  amount,
                };
                return newModel;
              }
              return model;
            }}
          >
            <NumField name="amount" />
            <SubmitField />
          </AutoForm>
        </FadeInModal>
      )}
      <ConfirmationDialog
        title={'Confirm Cancelling Transaction'}
        message={'The system is going to cancel the selected transaction. Continue?'}
        cancelText={'No'}
        confirmText={'Yes, Proceed'}
        open={confirmCancel}
        setOpen={setConfirmCancel}
        onConfirm={() => {
          setConfirmCancel(false);
          doChange(`/transactions/${selectedId}`, 'PUT', { status: 'CANCELED' }, mutate);
          setSelectedId(null);
        }}
      />
    </LoggedInLayout>
  );
};

export default TransferApprovals;
