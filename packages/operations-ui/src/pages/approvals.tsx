import React, { useState } from 'react';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import { Close } from '../api/closesApi';
import { TitleHeader, ConfirmationDialog, AuditList } from '@packages/gbt-ui';
import { CheckCircleIcon } from 'react-line-awesome';
import ClosePicker from '../components/common/ClosePicker';

import { Container, Grid, Box, Divider, Button } from '@mui/material';

import LoggedInLayout from '../components/layouts/LoggedInLayout';
import FeesStatement from '../components/deals/FeesStatement';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  approve: {
    backgroundColor: 'green',
  },
  sublistDivider: {
    marginTop: theme.spacing(1),
  },
}));

const Approvals = () => {
  const classes = useStyles();
  const op = objectProps.approvals;

  const [canApprove, setCanApprove] = useState(false);
  const [selectedClose, setSelectedClose] = React.useState<Close | undefined>(undefined);
  const [openConfirm, setOpenConfirm] = useState(false);

  const url = `/subscriptions-by-deal/${selectedClose?.dealId}?page=0&perPage=100&filter[include][]=profile`;
  const { data, isValidating } = useRequest(selectedClose?.dealId ? url : '', {
    swrOptions: {
      revalidateOnFocus: false,
    },
  });

  const balanceUrl = `/entities/${selectedClose?.entityId}/bank-account/balance`;
  const { data: fdata, isValidating: fisV } = useRequest(
    selectedClose?.entityId ? balanceUrl : '',
    {
      swrOptions: {
        revalidateOnFocus: false,
      },
    }
  );

  async function onConfirm() {
    //  dispatch(fundManagerSignClose(selectedCloseId, data));
    //  dispatch(setSelectedClose(""));
    //  setShowForm(false);
  }

  return (
    <LoggedInLayout title={op.label}>
      <Container>
        <TitleHeader title={op.label}>
          <Box mb={0.6}>
            <Button
              size="large"
              color="primary"
              variant="contained"
              startIcon={<CheckCircleIcon />}
              className={classes.approve}
              disableElevation
              disabled={!canApprove}
              onClick={() => setOpenConfirm(true)}
            >
              Approve
            </Button>
          </Box>
        </TitleHeader>
        <Divider />
        <Box mt={2}>
          <Grid container>
            <Grid item xs={12}>
              <ClosePicker
                whereField={'needsApproval'}
                whereFieldValue={'true'}
                setSelectedClose={setSelectedClose}
              />
              <Divider className={classes.sublistDivider} />
            </Grid>
            {selectedClose && (
              <React.Fragment>
                <Grid item xs={12}>
                  <AuditList
                    subscriptions={data?.data}
                    subscriptionsLoading={isValidating}
                    accountBalance={fdata?.balance}
                    acouuntBalanceLoading={fisV}
                    useCheck5={data?.data[0]?.tenantId === 'assure'}
                    setCanApprove={setCanApprove}
                  />
                  <Divider className={classes.sublistDivider} />
                </Grid>
                <Grid item xs={12}>
                  <FeesStatement dealId={selectedClose.dealId} />
                </Grid>
              </React.Fragment>
            )}
          </Grid>
        </Box>
        <ConfirmationDialog
          title={'Confirm Funds Transfer'}
          message={
            "The system is going to move '$xx,xxx in to account with [Assure/GBT] ending in [####]. Continue?"
          }
          cancelText={'Cancel'}
          confirmText={'Yes, Proceed'}
          open={openConfirm}
          setOpen={setOpenConfirm}
          onConfirm={() => onConfirm()}
        />
      </Container>
    </LoggedInLayout>
  );
};

export default Approvals;
