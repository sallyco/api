import { Container } from '@mui/material';
import { BasicTabs } from '@packages/gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';

import Dashboard from '../../components/bank-reconciliation/Dashboard';
import TransactionsTab from '../../components/bank-reconciliation/TransactionsTab';

const BankReconciliation = () => {
  return (
    <LoggedInLayout title={'Bank Account Reconciliation'}>
      <Container>
        <BasicTabs
          tabName={'reconciliation-ui-tab'}
          headerColor="primary"
          tabs={[
            {
              tabName: 'Dashboard',
              tabContent: <Dashboard />,
            },
            {
              tabName: 'Transactions',
              tabContent: <TransactionsTab />,
            },
          ]}
        />
      </Container>
    </LoggedInLayout>
  );
};

export default BankReconciliation;
