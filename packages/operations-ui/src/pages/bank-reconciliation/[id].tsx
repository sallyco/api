import { NextPage, NextPageContext } from 'next';
import { useRouter } from 'next/router';
import Container from '@mui/material/Container';
import { InfoCard, BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import useSWR from 'swr';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const BankReconciliationDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q[0];
  const omitFields = ['providerMeta'];
  const fetcher = (url) =>
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        console.info('kyc-aml', data);
        return data;
      });

  const metadataUrl = baseUrl + `/transactions/${id}`;
  const { data: transactionsDetails, error } = useSWR(metadataUrl, fetcher);
  return (
    <LoggedInLayout title={`${transactionsDetails?.id ?? 'Loading'} ~ Reconciliation Details`}>
      {!transactionsDetails?.id ? (
        <ContentLoader />
      ) : (
        <>
          <Container>
            <TitleHeader title={'Reconciliation Details'} />
          </Container>
          <BasicTabs
            tabName={'reconciliation-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container>
                    <InfoCard
                      title={'Transaction Details'}
                      item={transactionsDetails}
                      omitFields={omitFields}
                    />
                  </Container>
                ),
              },
            ]}
          />
        </>
      )}
    </LoggedInLayout>
  );
};

BankReconciliationDetails.getInitialProps = async (ctx: NextPageContext) => {
  return {};
};

export default BankReconciliationDetails;
