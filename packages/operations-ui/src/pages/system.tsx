import React from 'react';

import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { BasicTabs, TitleHeader } from '@packages/gbt-ui';

import ApiStatus from '../components/ApiStatus';
import BlueSkyTab from '../components/bluesky/BlueSkyTab';

import { useRequest } from '@packages/wired-gbt-ui';

import { Container } from '@mui/material';

const System = () => {
  const { data: apiStatus } = useRequest('/ping');

  return (
    <LoggedInLayout title={'System Settings'}>
      <Container>
        <TitleHeader title={'System Settings'} />
        <BasicTabs
          tabName={'systemsettings-ui-tab'}
          headerColor="primary"
          tabs={[
            {
              tabName: 'Status',
              tabContent: (
                <React.Fragment>
                  {apiStatus && <ApiStatus name="API Status" apiStatus={apiStatus} />}
                </React.Fragment>
              ),
            },
            {
              tabName: 'Blue Sky Fees',
              tabContent: <BlueSkyTab />,
            },
          ]}
        />
      </Container>
    </LoggedInLayout>
  );
};

export default System;
