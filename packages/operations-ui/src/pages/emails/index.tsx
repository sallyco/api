import React, { useEffect, useState } from 'react';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogTitle,
  FormControl,
  InputLabel,
  List,
  ListItem,
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { TableWithTools, TitleHeader } from '@packages/gbt-ui';
import { PlusIcon } from 'react-line-awesome';
import { getOmdUrl } from '../../tools';
import { useRequest, doChange, objectProps } from '@packages/wired-gbt-ui';
import dynamic from 'next/dynamic';
import _ from 'lodash';
import { Alert } from '@mui/material';

const MJMLEditorNoSSR = dynamic(() => import('../../components/mjml/MJMLEditor'), {
  ssr: false,
});

const Emails = () => {
  const op = objectProps.emails;

  interface EmailData {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    tenantId: string;
  }

  enum DATA_QUERY_KEYS {
    USERS = 'USERS',
    PROFILES_MISSING_DOB = 'PROFILES_MISSING_DOB',
  }

  const DATA_QUERIES = {
    [DATA_QUERY_KEYS.USERS]: {
      route: objectProps.users.route,
      transformData: (data: any[]): EmailData[] => {
        return data.map((user) => ({
          id: user.id,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          tenantId: user.tenantId,
        }));
      },
    },
    [DATA_QUERY_KEYS.PROFILES_MISSING_DOB]: {
      route: `/profiles?filter=${encodeURI(
        JSON.stringify({
          where: {
            or: [
              {
                dateOfBirth: {
                  exists: false,
                },
              },
              {
                dateOfBirth: null,
              },
            ],
          },
          include: ['owner'],
        })
      )}`,
      transformData: (data: any[]): EmailData[] => {
        return data.map((profile) => {
          return {
            id: profile.id,
            email: profile?.owner?.email,
            firstName: profile?.owner?.firstName,
            lastName: profile?.owner?.lastName,
            tenantId: profile?.tenantId,
          };
        });
      },
    },
  };

  // Eventually, we should make this pull templates from the API
  const availableEmailTemplates = {
    ProfileMissingData: 'ProfileMissingData',
  };

  const [availableEmails, setAvailableEmails] = useState<EmailData[]>([]);
  const [selectableEmails, setSelectableEmails] = useState<EmailData[]>([]);
  const [selectedEmails, setSelectedEmails] = useState<EmailData[]>([]);
  const [emailSelectorOpen, setEmailSelectorOpen] = useState(false);
  const [emailConfirmationOpen, setEmailConfirmationOpen] = useState(false);
  const [usersQuery, setUsersQuery] = useState<DATA_QUERY_KEYS>(DATA_QUERY_KEYS.USERS);
  const [emailTemplate, setEmailTemplate] = useState(availableEmailTemplates.ProfileMissingData);
  const [emailSubject, setEmailSubject] = useState('');
  const { data: users, isValidating: usersValidating } = useRequest(DATA_QUERIES[usersQuery].route);
  const { data: tenants } = useRequest(
    selectedEmails.length > 0 &&
      `/tenants?filter=${encodeURI(
        JSON.stringify({
          where: {
            id: {
              in: [
                ..._.uniqBy(
                  selectedEmails.map((email) => email.tenantId),
                  'id'
                ),
              ],
            },
          },
        })
      )}`
  );
  const [mailingReport, setMailingReport] = useState([]);
  const [sendingMail, setSendingMail] = useState(false);

  useEffect(() => {
    // Fix for inconsistent pagination format from API -_-
    const usersData = users?.data ?? users;

    if (usersData) {
      let filteredUsers = DATA_QUERIES[usersQuery].transformData(usersData);
      filteredUsers = filteredUsers.filter((user) => user.email);
      filteredUsers = _.uniqWith(filteredUsers, (item1, item2) => {
        return item1.email === item2.email && item1.tenantId === item2.tenantId;
      });
      setAvailableEmails(filteredUsers);
    }
  }, [users, usersQuery]);

  const handleSubjectChange = (e) => {
    setEmailSubject(e.currentTarget.value);
  };

  const columns = [
    {
      name: 'id',
      options: {
        display: false,
        filter: false,
      },
    },
    {
      name: 'email',
      label: 'Email',
      options: {
        filter: true,
      },
    },
    {
      name: 'firstName',
      label: 'First Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'lastName',
      label: 'Last Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant ID',
      options: {
        display: true,
        filter: true,
      },
    },
  ];

  return (
    <LoggedInLayout title={'User Settings'}>
      <Container maxWidth={false} style={{ flexGrow: 1, display: 'flex', flexDirection: 'column' }}>
        <TitleHeader title={op.label}>
          <Button
            color="primary"
            size="small"
            startIcon={<PlusIcon />}
            onClick={() => setEmailSelectorOpen(true)}
          >
            Add Emails
          </Button>
        </TitleHeader>
        <Box py={1}>
          <TableWithTools
            data={selectedEmails}
            columnDefinitions={columns}
            onRowsDelete={(rowsDeleted, data, newTableData) => {
              let newData = selectedEmails;
              const indexesDeleted = rowsDeleted.data.map((row) => row.dataIndex);
              newData = newData.filter((email, emailIndex) => {
                return !indexesDeleted.includes(emailIndex);
              });
              setSelectedEmails(newData);
            }}
          />
          {/* Not ready yet, has issues with Block types */}
          {/*<MJMLEditorNoSSR/>*/}
        </Box>
        <Box py={1}>
          <InputLabel>Email Template</InputLabel>
          <Select
            label={'Email Template'}
            value={emailTemplate}
            onChange={(e) => setEmailTemplate(String(e.target.value))}
          >
            <MenuItem value={availableEmailTemplates.ProfileMissingData}>
              Profile Missing Data Warning
            </MenuItem>
          </Select>
        </Box>
        <Box py={1}>
          <InputLabel>Email Subject</InputLabel>
          <TextField value={emailSubject} onChange={handleSubjectChange} />
        </Box>
        <Box py={1}>
          <Button
            onClick={() => setEmailConfirmationOpen(true)}
            variant={'outlined'}
            disabled={selectedEmails.length < 1 || emailSubject.length < 1}
          >
            Send Email
          </Button>
        </Box>
      </Container>
      <Dialog
        onClose={() => setEmailSelectorOpen(false)}
        open={emailSelectorOpen}
        fullWidth
        maxWidth={'xl'}
      >
        <DialogTitle>Choose Emails to Send to</DialogTitle>
        <Box m={2}>
          <FormControl fullWidth>
            <InputLabel>Data Query</InputLabel>
            <Select
              value={usersQuery}
              label="Users Query"
              onChange={(e) => setUsersQuery(DATA_QUERY_KEYS[String(e.target.value)])}
            >
              <MenuItem value={DATA_QUERY_KEYS.USERS}>All Users</MenuItem>
              <MenuItem value={DATA_QUERY_KEYS.PROFILES_MISSING_DOB}>
                Profiles Missing Date of Birth
              </MenuItem>
            </Select>
          </FormControl>
        </Box>
        {!users && usersValidating ? (
          <Box display={'flex'} justifyContent={'center'}>
            <CircularProgress />
          </Box>
        ) : (
          <TableWithTools
            onRowSelectionChange={(currentRowsSelected, allRowsSelected, rowsSelected) => {
              setSelectableEmails(
                currentRowsSelected.map((item) => availableEmails[item.dataIndex])
              );
            }}
            data={availableEmails}
            columnDefinitions={columns}
          />
        )}
        <DialogActions>
          <Button
            onClick={() => {
              setSelectedEmails(_.uniqWith([...selectedEmails, ...selectableEmails], _.isEqual));
              setEmailSelectorOpen(false);
            }}
          >
            Select
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog onClose={() => setEmailConfirmationOpen(false)} open={emailConfirmationOpen}>
        <DialogTitle>Confirm Email Sending</DialogTitle>
        <Box m={2}>
          <Typography>Send this email to {selectedEmails.length} users?</Typography>
        </Box>
        <DialogActions>
          <Button disabled={sendingMail} onClick={() => setEmailConfirmationOpen(false)}>
            Cancel
          </Button>
          <Button
            disabled={sendingMail}
            onClick={async () => {
              setSendingMail(true);
              const report = await doChange('/email/send-mass-email', 'POST', {
                emails: selectedEmails.map(
                  (email) =>
                    new Object({
                      ...email,
                      returnUrl: getOmdUrl(tenants.find((tenant) => tenant.id === email.tenantId)),
                    })
                ),
                template: 'profileNeedsUpdate',
                subject: emailSubject,
              });
              setSendingMail(false);
              setEmailConfirmationOpen(false);
              setMailingReport(report);
            }}
            autoFocus
            variant={'contained'}
          >
            Send
            {sendingMail && <CircularProgress size={15} />}
          </Button>
        </DialogActions>
      </Dialog>
      <Box m={2}>
        <Paper>
          <Box p={2}>
            <Typography variant={'h2'} color={'textSecondary'}>
              Mailing Report
            </Typography>
            <List>
              {mailingReport.map(function item(item) {
                return (
                  <>
                    <ListItem>
                      <Alert severity={item.type === 'success' ? 'success' : 'warning'}>
                        <Typography>{item.message}</Typography>
                      </Alert>
                    </ListItem>
                  </>
                );
              })}
              {mailingReport.length < 1 && 'No Mail Sent'}
            </List>
          </Box>
        </Paper>
      </Box>
    </LoggedInLayout>
  );
};

export default Emails;
