import React from 'react';
import { NextPage } from 'next';
import { Container } from '@mui/material';
import { useRequest } from '@packages/wired-gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import FinancialTransactionsChart from '../../components/financial-transactions/FinancialTransactionsChart';
import FinancialTransactionsTable from '../../components/financial-transactions/FinancialTransactionsTable';

const FinancialTransactions: NextPage = () => {
  const { data: transactions } = useRequest(`/transactions-all`);

  return (
    <LoggedInLayout title={'Financial Transactions'}>
      {!transactions ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Financial Transactions'} />
          </Container>
          <BasicTabs
            tabName={'transactions-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Dashboard',
                tabContent: (
                  <Container>
                    <FinancialTransactionsChart transactions={transactions} />
                  </Container>
                ),
              },
              {
                tabName: 'Transactions',
                tabContent: <FinancialTransactionsTable transactions={transactions} />,
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default FinancialTransactions;
