import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Container } from '@mui/material';
import { InfoCard, BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import useSwr from 'swr';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;
const fetcher = (url) => fetch(url).then((res) => res.json());

const FinancialTransactionsDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q[0];
  const [submitting, setSubmitting] = useState(false);

  const { data: transaction } = useSwr(`${baseUrl}/transactions/${id}`, fetcher);
  const { data: beneficiary } = useSwr(
    () => `${baseUrl}/banking/beneficiary-by-serviceid/${transaction.counterpartyId}`,
    fetcher
  );

  const isDisabled =
    transaction?.tenantId === '' || transaction?.tenantId === null || !transaction?.tenantId
      ? true
      : false;

  return (
    <LoggedInLayout title={`${transaction?.id ?? 'Loading'} ~ Companies`}>
      {!transaction ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Financial Transaction : ' + transaction?.id}></TitleHeader>
          </Container>
          <BasicTabs
            tabName={'transactions-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard title={'Transaction Details'} item={transaction} />
                    {beneficiary && <InfoCard title={'Beneficiary Details'} item={beneficiary} />}
                  </Container>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default FinancialTransactionsDetails;
