import React from 'react';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import { NextPage } from 'next';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { BasicTabs, TitleHeader, ContentLoader, TableWithTools } from '@packages/gbt-ui';
import { PlayCircleIcon, StepForwardIcon } from 'react-line-awesome';

import WorkflowProcessModal from '../../components/workflows/WorkflowProcessModal';
import StartWorkflowModal from '../../components/workflows/StartWorkflowModal';
import WorkflowActivityModal from '../../components/workflows/WorkflowActivityModal';

import { Container, IconButton } from '@mui/material';

import _ from 'lodash';

const Workflows: NextPage = () => {
  const op = objectProps.workflows;
  const { data: processes, isValidating: processesValidating } = useRequest(
    `${op.route}/processes`
  );
  const {
    data: instances,
    mutate,
    isValidating: instancesValidating,
  } = useRequest(`${op.route}/instances`);

  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [startOpen, setStartOpen] = React.useState(false);
  const [startOpenDirection, setStartOpenDirection] = React.useState<'up' | 'down'>('up');
  const [activityOpen, setActivityOpen] = React.useState(false);
  const [selected, setSelected] = React.useState<any>(undefined);
  const [selectedInstance, setSelectedInstance] = React.useState<any>(undefined);

  return (
    <LoggedInLayout title={op.label}>
      {!processes ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Workflows'} />
          </Container>
          <BasicTabs
            tabName={'deals-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Processes',
                tabContent: (
                  <TableWithTools
                    data={processes}
                    showLoading={true}
                    isLoading={processesValidating}
                    columnDefinitions={[
                      {
                        name: 'id',
                        label: 'Id',
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: 'deploymentId',
                        label: 'Deployment Id',
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: 'name',
                        label: 'Name',
                      },
                      {
                        name: 'key',
                        label: 'Resource',
                        options: {
                          filter: false,
                          customBodyRender: (value, tableMeta, updateValue) => `/${value}`,
                        },
                      },
                      {
                        name: 'resource',
                        label: 'Filename',
                        options: {
                          filter: false,
                        },
                      },
                      {
                        name: 'version',
                        label: 'Version',
                      },
                      {
                        name: 'id',
                        label: 'Initiate',
                        options: {
                          filter: false,
                          viewColumns: false,
                          setCellProps: () => ({
                            style: {
                              padding: '0 0 0 10px',
                              width: '80px',
                              maxWidth: '80px',
                            },
                          }),
                          customBodyRender: function Rn(value, tableMeta, updateValue) {
                            return (
                              <IconButton
                                color="secondary"
                                onClick={(e) => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  setSelected(processes.find((x) => x.id === tableMeta.rowData[0]));
                                  setStartOpenDirection('up');
                                  setStartOpen(true);
                                }}
                                size="large"
                              >
                                <PlayCircleIcon />
                              </IconButton>
                            );
                          },
                        },
                      },
                    ]}
                    onRowClick={(r, m) => {
                      setSelected(processes.find((x) => x.id === r[0]));
                      setOpen(true);
                    }}
                    elevation={0}
                    selectable={false}
                    filter={false}
                    download={false}
                    clickable
                  />
                ),
              },
              {
                tabName: 'Instances',
                tabContent: (
                  <TableWithTools
                    data={instances}
                    showLoading={true}
                    isLoading={instancesValidating}
                    columnDefinitions={[
                      {
                        name: 'id',
                        label: 'Id',
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: 'businessKey',
                        label: 'Business Key',
                      },
                      {
                        name: 'definitionId',
                        label: 'Process Name',
                        options: {
                          customBodyRender: (value, tableMeta, updateValue) =>
                            _.find(processes, { id: value })?.name,
                        },
                      },
                      {
                        name: 'id',
                        label: 'Continue',
                        options: {
                          setCellProps: () => ({
                            style: {
                              padding: '0 0 0 10px',
                              width: '80px',
                              maxWidth: '80px',
                            },
                          }),
                          filter: false,
                          viewColumns: false,
                          customBodyRender: function Rn(value, tableMeta, updateValue) {
                            return (
                              <IconButton
                                color="secondary"
                                onClick={(e) => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  setSelectedInstance(
                                    instances.find((x) => x.id === tableMeta.rowData[0])
                                  );
                                  setActivityOpen(true);
                                }}
                                size="large"
                              >
                                <StepForwardIcon />
                              </IconButton>
                            );
                          },
                        },
                      },
                    ]}
                    elevation={0}
                    selectable={false}
                    filter={false}
                    download={false}
                  />
                ),
              },
            ]}
          />
          <WorkflowProcessModal
            open={open}
            setOpen={setOpen}
            workflowProcess={selected}
            setStartOpen={setStartOpen}
            setStartOpenDirection={setStartOpenDirection}
          />
          <StartWorkflowModal
            workflowProcess={selected}
            open={startOpen}
            setOpen={setStartOpen}
            slideDirection={startOpenDirection}
            mutate={mutate}
          />
          <WorkflowActivityModal
            workflowInstance={selectedInstance}
            open={activityOpen}
            setOpen={setActivityOpen}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Workflows;
