import React, { useState } from 'react';
import { NextPage } from 'next';
import { SnackbarUtils, useRequest, objectProps } from '@packages/wired-gbt-ui';
import { Container, Button, ButtonGroup, Box } from '@mui/material';
import { EraserIcon, PlusIcon } from 'react-line-awesome';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import UsersTable from '../../components/users/UsersTable';

import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import UsersInviteForm from '../../components/users/UsersInviteForm';

const Users: NextPage = () => {
  const op = objectProps.users;
  const { data: users, isValidating } = useRequest(op.route);
  const [formOpen, setFormOpen] = useState(false);

  const clearSessionCookies = () => {
    const domain = `.glassboardtech.com`;
    document.cookie = `__session= ;path=/;domain=${domain}; expires = Thu, 01 Jan 1970 00:00:00 GMT`;
    document.cookie = `__sessionrefresh= ;path=/;domain=${domain}; expires = Thu, 01 Jan 1970 00:00:00 GMT`;
    SnackbarUtils.info('Sessions Cleared');
  };

  return (
    <LoggedInLayout title={'Users'}>
      {!users ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Users'}>
              <ButtonGroup variant="outlined" size="small">
                <Button
                  color="secondary"
                  variant={'outlined'}
                  startIcon={<EraserIcon />}
                  onClick={clearSessionCookies}
                  size="small"
                >
                  Clear Sessions
                </Button>
                <Button
                  color="primary"
                  variant="outlined"
                  startIcon={<PlusIcon />}
                  onClick={() => setFormOpen(true)}
                  size="small"
                >
                  Invite New
                </Button>
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <UsersTable users={users} isValidating={isValidating} />
          </Box>
          <UsersInviteForm setFormOpen={setFormOpen} formOpen={formOpen} />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Users;
