import React, { useState } from 'react';
import { NextPage } from 'next';
import Router, { useRouter } from 'next/router';
import { Container, Button, ButtonGroup } from '@mui/material';

import { useRequest, objectProps, doChange, SnackbarUtils } from '@packages/wired-gbt-ui';

import {
  BasicTabs,
  ContentLoader,
  TitleHeader,
  InfoCard,
  FullScreenDialog,
} from '@packages/gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { ExternalLinkSquareAltIcon, PencilIcon } from 'react-line-awesome';
import UserEditForm from '../../components/users/UserEditForm';
import UserRoleMappings from '../../components/users/UserRoleMappings';

import { startImpersonateSession } from '../../services/ImpersonateService';

const UserDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;

  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';

  const op = objectProps.users;
  const { data: user, mutate } = useRequest(`${op.route}/${id}`);

  const [activeTab, setActiveTab] = useState(0);
  const [showEditForm, setShowEditForm] = useState(false);

  const onUserEditSubmit = async (updatedUser) => {
    setShowEditForm(false);
    await doChange(`${op.route}/${user.id}`, 'PUT', { ...updatedUser }, mutate);
    SnackbarUtils.success('User updated successfully.');
    mutate();
    Router.replace('/');
  };

  return (
    <LoggedInLayout title={`${user?.username ?? 'Loading'} ~ Users`}>
      {!user ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={user?.username}>
              <ButtonGroup variant="outlined" size="small">
                <Button
                  color="primary"
                  size="small"
                  onClick={() => setShowEditForm(true)}
                  startIcon={<PencilIcon />}
                >
                  Edit User
                </Button>
                <Button
                  color="primary"
                  size="small"
                  variant="outlined"
                  onClick={() => startImpersonateSession(user.tenantId, user.id, `dashboard/deals`)}
                  startIcon={<ExternalLinkSquareAltIcon />}
                >
                  Impersonate
                </Button>
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'users-details-ui-tab'}
            headerColor="primary"
            handleTabChange={(r) => setActiveTab(r)}
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      title={'User Details'}
                      item={user}
                      omitFields={['access', 'attributes']}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Attributes',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard title={'User Attributes'} item={user.attributes} />
                  </Container>
                ),
              },
              {
                tabName: 'Role Mappings',
                tabContent: (
                  <Container maxWidth={false}>
                    <UserRoleMappings id={id} />
                  </Container>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
      {showEditForm && (
        <FullScreenDialog title={''} open={showEditForm} setOpen={setShowEditForm}>
          <UserEditForm user={user} onSuccess={onUserEditSubmit} />
        </FullScreenDialog>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default UserDetails;
