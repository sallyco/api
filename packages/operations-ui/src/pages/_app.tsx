import React from 'react';

//import { StyledEngineProvider } from '@mui/material/styles';
//import StylesProvider from '@mui/styles/StylesProvider';
import CssBaseline from '@mui/material/CssBaseline';

import { Authorization, TenantsProvider, UserProvider, SnacksProvider } from '../contexts';
import { fetcher, SnackbarUtilsConfigurator } from '@packages/wired-gbt-ui';
import { Cache, ConfigProvider } from 'react-avatar';

import { Provider } from 'next-auth/client';
import { SWRConfig } from 'swr';

//declare module '@mui/styles/defaultTheme' {
//  interface DefaultTheme extends Theme {}
//}

const cache = new Cache({
  sourceTTL: 7 * 24 * 3600 * 1000,
  sourceSize: 50,
});

const App = ({ Component, pageProps }) => {
  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  React.useEffect(() => {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw.js').then(
        function (registration) {},
        function (err) {}
      );
    }
  }, []);

  return (
    <SWRConfig value={{ fetcher: fetcher }}>
      <CssBaseline />
      <TenantsProvider>
        <CssBaseline />
        <SnacksProvider>
          <Authorization>
            <ConfigProvider cache={cache}>
              <UserProvider>
                <Provider session={pageProps.session}>
                  <Component {...pageProps} />
                  <SnackbarUtilsConfigurator />
                </Provider>
              </UserProvider>
            </ConfigProvider>
          </Authorization>
        </SnacksProvider>
      </TenantsProvider>
    </SWRConfig>
  );
};

export default App;
