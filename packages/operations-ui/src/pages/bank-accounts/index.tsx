import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { NextPage } from 'next';

import { ContentLoader, TitleHeader } from '@packages/gbt-ui';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';

import { Chip, Container, Box } from '@mui/material';

import { TableWithTools } from '@packages/gbt-ui';
import { TableWithToolsService } from '../../../src/services/TableWithToolsService';

const Assets: NextPage = () => {
  const router = useRouter();
  const op = objectProps['bank-accounts'];
  const { data, error } = useRequest(`${op.route}`);

  const [columnDefinitions, setColumnDefinitions] = useState<any>([]);

  const initialColumnDefinitions = [
    {
      name: 'id',
      label: 'Id',
      options: {
        display: true,
        filter: false,
      },
    },
    {
      name: 'status',
      label: 'Status',
      options: {
        display: true,
        filter: true,
        // eslint-disable-next-line react/display-name
        customBodyRender: (value: any, _tableMeta: any, _updateValue: any) => (
          <Chip
            label={value.label}
            style={{
              backgroundColor: value.labelColor,
              opacity: '80%',
            }}
            size="small"
          />
        ),
      },
    },
    {
      name: 'tenantId',
      label: 'Tenant',
      options: {},
    },
  ];

  const fieldMap: any = {
    id: 'string',
    dealId: 'string',
    entityId: 'string',
    amount: 'number',
    fundManagerSigned: 'boolean',
    createdAt: 'date',
    updatedAt: 'date',
  };

  const tableService = new TableWithToolsService(
    'closes-table-columns',
    fieldMap,
    initialColumnDefinitions,
    []
  );

  useEffect(() => {
    const colDef = tableService.getColumnDefinition();
    setColumnDefinitions(colDef);
  }, []);

  const onViewColumnsChange = (v, a) => {
    const colDef = tableService.onViewColumnsChange(columnDefinitions, v, a);
    setColumnDefinitions(colDef);
    return;
  };

  return (
    <LoggedInLayout title={op.label}>
      {!data ? (
        <ContentLoader errorMessage={error?.message} />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={op.label} />
          </Container>
          <Box mt={2}>
            <TableWithTools
              data={data?.data}
              columnDefinitions={columnDefinitions}
              onRowClick={(r, m) => {
                router.push(`${op.route}/${r[0]}`);
              }}
              onViewColumnsChange={onViewColumnsChange}
              enableNestedDataAccess={'.'}
              elevation={0}
              selectable={false}
              clickable
              fixedHeader={false}
            />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Assets;
