import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Container } from '@mui/material';

import { ContentLoader, TitleHeader } from '@packages/gbt-ui';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';

const InvitationDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';

  const op = objectProps.invitations;
  const { data: invitation } = useRequest(`${op.route}/${id}`);

  return (
    <LoggedInLayout title={`Invitations`}>
      {!invitation ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader title="" />
          </Container>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default InvitationDetails;
