import React from 'react';
import { PlusIcon } from 'react-line-awesome';
import { Box, Container, Button } from '@mui/material';
import { NextPage } from 'next';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import InvitationsTable from '../../components/invitations/InvitationsTable';

const Invitations: NextPage = () => {
  const op = objectProps.invitations;
  const { data: invitations } = useRequest(`${op.route}`);

  return (
    <LoggedInLayout title={'Invitations'}>
      {!invitations ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Invitations'}>
              <Button color="primary" size="small" startIcon={<PlusIcon />}>
                Create New
              </Button>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <InvitationsTable invitations={invitations.data} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Invitations;
