import React from 'react';
import { NextPage } from 'next';
import { Container } from '@mui/material';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';

import { BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import KycAmlExceptionsTable from '../../components/kyc-aml/KycAmlExceptionsTable';
import KycAmlExceptionsChart from '../../components/kyc-aml/KycAmlExceptionsChart';

const KycAmlExceptions: NextPage = () => {
  const op = objectProps['kyc-aml'];
  const { data: profiles } = useRequest(`${op.routeExceptions}`);

  const filterExceptions = (profile) => {
    if (!profile?.kycAml || !profile.kycAml.providerMeta.reports) {
      console.log('Missing Property');
      return true;
    }
    const requiredReports = ['identity_enhanced', 'watchlist_enhanced'];

    for (const reportName of requiredReports) {
      if (!profile.kycAml.providerMeta.reports.find((report) => report.name === reportName)) {
        console.log('Missing Report', reportName);
        return true;
      }
    }
    console.log('Clear check', profile.kycAml.result !== 'CLEAR');
    return profile.kycAml.result !== 'CLEAR';
  };

  return (
    <LoggedInLayout title={'KYC/AML Exceptions'}>
      {!profiles ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'KYC/AML Exceptions'} />
          </Container>
          <BasicTabs
            tabName={'kycaml-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Dashboard',
                tabContent: (
                  <Container>
                    <KycAmlExceptionsChart profiles={profiles.data} />
                  </Container>
                ),
              },
              {
                tabName: 'Transactions',
                tabContent: (
                  <Container maxWidth={false}>
                    <KycAmlExceptionsTable profiles={profiles.data} />
                  </Container>
                ),
              },
              {
                tabName: 'Exceptions',
                tabContent: (
                  <Container maxWidth={false}>
                    <KycAmlExceptionsTable profiles={profiles.data.filter(filterExceptions)} />
                  </Container>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default KycAmlExceptions;
