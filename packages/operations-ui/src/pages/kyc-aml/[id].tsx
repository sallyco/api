import { NextPage, NextPageContext } from 'next';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { ExternalLinkSquareAltIcon, PencilAltIcon } from 'react-line-awesome';
import { Button, Card, CardContent, Chip, Container, Theme, Tooltip } from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import { BasicTabs, InfoCard, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import useSWR from 'swr';
import { doChange } from '@packages/wired-gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { startImpersonateSession } from '../../services/ImpersonateService';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    redButton: {
      backgroundColor: 'red',
      color: 'white',
    },
    orangeButton: {
      backgroundColor: 'orange',
      color: 'white',
    },
    greenButton: {
      backgroundColor: 'green',
      color: 'white',
    },
    blueButton: {
      backgroundColor: 'blue',
      color: 'white',
    },
  })
);

const KycAmlExceptionsDetails: NextPage = () => {
  const styles = useStyles();
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q[0];
  const [submitting, setSubmitting] = useState(false);

  const omitFields = ['providerMeta'];

  const fetcher = (url) =>
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        console.info('kyc-aml', data);
        return data;
      });

  const metadataUrl = baseUrl + `/kyc-aml/${id}`;
  const { data: kycAmlProfile, error } = useSWR(metadataUrl, fetcher);

  const isDisabled =
    kycAmlProfile?.profile?.tenantId === '' ||
    kycAmlProfile?.profile?.tenantId === null ||
    !kycAmlProfile?.profile?.tenantId
      ? true
      : false;

  async function ImpersonateSession(realmId, ownerId) {
    setSubmitting(true);
    startImpersonateSession(realmId, ownerId, `dashboard`);
    setSubmitting(false);
  }

  const overrideToClearCustomer = async () => {
    if (kycAmlProfile?.profile) {
      const kycAml = {
        kycAml: {
          ...kycAmlProfile.profile.kycAml,
          overrideStatus: 'CLEAR',
        },
      };
      doChange(`/kyc-aml/${id}`, 'PATCH', kycAml);
    }
  };

  const getExceptionTag = (kycAml) => {
    const breakdown = undefined; //getBreakdownFromCheckResults(kycAml?.providerMeta);
    const overrideStatus = kycAml?.overrideStatus;
    if (overrideStatus && overrideStatus.toLowerCase() === 'clear') {
      return (
        <Chip label="CLEAR" color="default" size="small" classes={{ root: styles.greenButton }} />
      );
    }
    const kycStatus = kycAml?.status;
    if (kycStatus && kycStatus.toLowerCase() === 'exception') {
      return <Chip label="EXCEPTION" color="default" size="small" />;
    }
    const kycResult = kycAml?.result;
    if (kycResult && kycResult.toLowerCase() === 'clear') {
      return (
        <Chip label="CLEAR" color="default" size="small" classes={{ root: styles.greenButton }} />
      );
    }
    let suspected = false;
    let identityFailure = false;
    if (breakdown) {
      const sanction = breakdown?.sanction?.result;
      if (sanction && sanction.toLowerCase() === 'consider') {
        suspected = true;
      }
      const monitoredList = breakdown?.monitored_lists?.result;
      if (monitoredList && monitoredList.toLowerCase() === 'consider') {
        suspected = true;
      }
      const identity = breakdown?.identity?.result;
      if (identity && identity.toLowerCase() === 'consider') {
        identityFailure = true;
      }

      if (suspected) {
        return (
          <Chip
            label="SUSPECTED"
            color="default"
            size="small"
            classes={{ root: styles.redButton }}
          />
        );
      } else if (identityFailure) {
        return (
          <Chip
            label="IDENTITY"
            color="default"
            size="small"
            classes={{ root: styles.orangeButton }}
          />
        );
      }
    }
    return (
      <Chip
        label="IN PROGRESS"
        color="default"
        size="small"
        classes={{ root: styles.blueButton }}
      />
    );
  };

  return (
    <LoggedInLayout title={`${kycAmlProfile?.profile?.id ?? 'Loading'} ~ KYC/AML`}>
      {!kycAmlProfile?.profile ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader
              title={
                kycAmlProfile.profile?.name ??
                kycAmlProfile.profile?.firstName +
                  (kycAmlProfile.profile?.lastName ? ' ' + kycAmlProfile.profile?.lastName : '') +
                  (kycAmlProfile.profile?.profileType
                    ? ' (' + kycAmlProfile.profile.profileType + ')'
                    : '')
              }
            >
              <Button
                color="primary"
                size="small"
                onClick={() => overrideToClearCustomer()}
                startIcon={<PencilAltIcon />}
              >
                Clear Customer
              </Button>
              <Tooltip
                title={isDisabled ? 'Can’t impersonate from this page' : ''}
                placement="top"
                arrow
              >
                <span>
                  <Button
                    color="primary"
                    size="small"
                    onClick={() =>
                      ImpersonateSession(
                        kycAmlProfile.profile?.tenantId,
                        kycAmlProfile.profile?.ownerId
                      )
                    }
                    startIcon={<ExternalLinkSquareAltIcon />}
                    disabled={isDisabled}
                  >
                    Impersonate
                  </Button>
                </span>
              </Tooltip>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'kycaml-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <>
                    <Card variant="outlined">
                      <CardContent>
                        STATUS: {getExceptionTag(kycAmlProfile.profile.kycAml)}
                      </CardContent>
                    </Card>
                    <InfoCard title={'Manager Details'} item={kycAmlProfile.manager} />
                    <InfoCard title={'Deals'} item={kycAmlProfile.deals} />
                    <InfoCard
                      title={'KYC Details'}
                      item={kycAmlProfile.profile.kycAml}
                      omitFields={omitFields}
                    />
                  </>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

KycAmlExceptionsDetails.getInitialProps = async (ctx: NextPageContext) => {
  return {};
};

export default KycAmlExceptionsDetails;
