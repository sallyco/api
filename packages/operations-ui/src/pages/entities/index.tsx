import React from 'react';
import { Container, Box } from '@mui/material';
import { NextPage } from 'next';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import EntitiesTable from '../../components/entities/EntitiesTable';

const Entities: NextPage = () => {
  const op = objectProps.entities;

  const { data: entities, isValidating } = useRequest(`${op.route}?page=0&perPage=100`);

  return (
    <LoggedInLayout title={`${op.label}`}>
      {!entities ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={op.label} />
          </Container>
          <Box mt={2}>
            <EntitiesTable entities={entities.data} isValidating={isValidating} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Entities;
