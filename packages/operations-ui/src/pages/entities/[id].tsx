import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Box, Button, ButtonGroup, Container, Grid, Tooltip } from '@mui/material';

import { BasicTabs, ContentLoader, TitleHeader, DeletePopup, InfoCard } from '@packages/gbt-ui';

import { useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { startImpersonateSession } from '../../services/ImpersonateService';
import { ExternalLinkSquareAltIcon } from 'react-line-awesome';
import FileUploader from '../../components/common/FileUploader';
import { SoftDeleteById } from '../../services/DeleteService';
import FilesTable from '../../components/files/FilesTable';
import AuditLogTable from '../../components/audit-log/AuditLogTable';

const EntityDetails: NextPage = () => {
  const router = useRouter();

  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);

  const op = objectProps.entities;
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name'],
          },
        },
        {
          relation: 'owner',
        },
        {
          relation: 'manager',
          scope: {
            fields: ['id', 'name'],
          },
        },
        {
          relation: 'masterEntity',
          scope: {
            fields: ['id', 'name'],
          },
        },
      ],
    })
  );
  const { data: entity, mutate: mutateCurrent } = useRequest(
    `${op.route}/${id}?filter=${encodedFilter}`
  );

  const LinkFields = new Map();
  LinkFields.set('dealId', { value_field: 'deal', url_field: 'deals' });
  LinkFields.set('ownerId', {
    value_field: 'owner',
    url_field: 'users',
  });
  LinkFields.set('managerId', {
    value_field: 'manager',
    url_field: 'managers',
  });
  LinkFields.set('masterEntityId', {
    value_field: 'masterEntity',
    url_field: 'master-entities',
  });

  const isDisabled =
    entity?.tenantId === '' || entity?.tenantId === null || !entity?.tenantId ? true : false;

  return (
    <LoggedInLayout title={`${entity?.name ?? 'Loading'} ~ Entities`}>
      {!entity ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={entity?.name}>
              <ButtonGroup color="secondary" variant="outlined" size="small">
                <EditButton op={op} id={id} mutate={mutateCurrent} />
                <Tooltip
                  title={isDisabled ? 'Can’t impersonate from this page' : ''}
                  placement="top"
                  arrow
                >
                  <Button
                    color="primary"
                    size="small"
                    variant="outlined"
                    onClick={() =>
                      startImpersonateSession(entity?.tenantId, entity?.ownerId, `dashboard/deals`)
                    }
                    startIcon={<ExternalLinkSquareAltIcon />}
                    disabled={isDisabled}
                  >
                    Impersonate
                  </Button>
                </Tooltip>
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'entities-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      title={'Entity Details'}
                      icon={op.icon}
                      item={entity}
                      linkfields={LinkFields}
                      omitFields={[
                        'bankAccount',
                        'deal',
                        'owner',
                        'files',
                        'entityDocuments',
                        'manager',
                        'masterEntity',
                      ]}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Documents',
                tabContent: (
                  <Grid container spacing={2}>
                    <Grid item sm={8} xl={9} xs={12}>
                      <FilesTable files={[]} />
                    </Grid>
                    <Grid item sm={4} xl={3} xs={12}>
                      <Box pb={2}>
                        <FileUploader
                          objectType={'entities'}
                          objectId={entity.id}
                          tenantId={entity.tenantId}
                          omitFields={[]}
                          uploadSuccess={async (fileIds) => {
                            //await dispatch(fetchEntityById(entity.id));
                          }}
                        />
                      </Box>
                    </Grid>
                  </Grid>
                ),
              },
              {
                tabName: 'Banking',
                tabContent: entity.bankAccount ? (
                  <Container maxWidth={false}>
                    <InfoCard
                      title={"Entity's Banking Details"}
                      icon={op.icon}
                      item={entity.bankAccount}
                    />
                  </Container>
                ) : (
                  <></>
                ),
              },
              {
                tabName: 'Audit',
                tabContent: <AuditLogTable id={id} />,
              },
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={entity?.id}
                    objectType={'Entity'}
                    name={entity?.name}
                    processing={processing}
                    onClickOk={async () => {
                      setProcessing(true);
                      await SoftDeleteById(entity?.id, 'entity');
                      router.push('/entities');
                      setProcessing(false);
                    }}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default EntityDetails;
