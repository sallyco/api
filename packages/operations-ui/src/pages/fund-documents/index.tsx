import React from 'react';
import { NextPage } from 'next';
import { Container } from '@mui/material';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import FundDocumentsTable from '../../components/fund-documents/FundDocumentsTable';
import FundDocumentsChart from '../../components/fund-documents/FundDocumentsChart';

import useSWR from 'swr';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const FundDocuments: NextPage = () => {
  const fetcher = (url) =>
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        return data;
      });

  const metadataUrl = baseUrl + `/fund-docs`;
  const { data: fundDocs, error } = useSWR(metadataUrl, fetcher);

  return (
    <LoggedInLayout title={'Fund Documents'}>
      {!fundDocs ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Fund Documents'} />
          </Container>
          <BasicTabs
            tabName={'funddocs-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Dashboard',
                tabContent: (
                  <Container>
                    <FundDocumentsChart fundDocuments={Object.values(fundDocs)} />
                  </Container>
                ),
              },
              {
                tabName: 'Documents',
                tabContent: <FundDocumentsTable fundDocuments={Object.values(fundDocs)} />,
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default FundDocuments;
