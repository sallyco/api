import { NextPage, NextPageContext } from 'next';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { ExternalLinkSquareAltIcon } from 'react-line-awesome';
import {
  Button,
  Card,
  CardContent,
  Container,
  Paper,
  Table,
  TableBody,
  TableContainer,
  Theme,
  Tooltip,
} from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import { BasicTabs, ContentLoader, TitleHeader } from '@packages/gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { startImpersonateSession } from '../../services/ImpersonateService';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import DocumentRow from '../../components/common/DocumentRow';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    redButton: {
      backgroundColor: 'red',
      color: 'white',
    },
    orangeButton: {
      backgroundColor: 'orange',
      color: 'white',
    },
    greenButton: {
      backgroundColor: 'green',
      color: 'white',
    },
    blueButton: {
      backgroundColor: 'blue',
      color: 'white',
    },
  })
);

const FundDocumentsDetails: NextPage = () => {
  const styles = useStyles();
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q[0];
  const [submitting, setSubmitting] = useState(false);

  const op = objectProps.entities;
  const { data: entity } = useRequest(`${op.route}/${id}`);

  const isDisabled =
    entity?.tenantId === '' || entity?.tenantId === null || !entity?.tenantId ? true : false;

  async function ImpersonateSession(realmId, ownerId) {
    setSubmitting(true);
    startImpersonateSession(realmId, ownerId, `dashboard`);
    setSubmitting(false);
  }

  return (
    <LoggedInLayout title={`Fund Documents for Entity: ${entity?.id ?? 'Loading'} ~ KYC/AML`}>
      {!entity ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader title={`Fund Documents for Entity: ${entity?.id}`}>
              <Tooltip
                title={isDisabled ? 'Can’t impersonate from this page' : ''}
                placement="top"
                arrow
              >
                <span>
                  <Button
                    color="primary"
                    size="small"
                    onClick={() => ImpersonateSession(entity.tenantId, entity.ownerId)}
                    startIcon={<ExternalLinkSquareAltIcon />}
                    disabled={isDisabled}
                  >
                    Impersonate
                  </Button>
                </span>
              </Tooltip>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'funddocs-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Documents',
                tabContent: (
                  <>
                    <Card variant="outlined">
                      <CardContent>
                        <TableContainer component={Paper}>
                          <Table size="small">
                            <TableBody>
                              <DocumentRow
                                fileId={entity?.entityDocuments?.operatingAgreement}
                                documentTitle={'Operating Agreement'}
                              />
                              <DocumentRow
                                fileId={entity?.entityDocuments?.privatePlacementMemorandum}
                                documentTitle={'Private Placement Memorandum'}
                              />
                              <DocumentRow
                                fileId={entity?.entityDocuments?.subscriptionAgreement}
                                documentTitle={'Subscription Agreement'}
                              />
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </CardContent>
                    </Card>
                  </>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

FundDocumentsDetails.getInitialProps = async (ctx: NextPageContext) => {
  return {};
};

export default FundDocumentsDetails;
