import React from 'react';
import Link from 'next/link';
import Error from './_error';

const ErrorPage = ({ errorCode = 500 }) => {
  return (
    <Error
      statusCode={errorCode}
      title={
        <React.Fragment>
          There was an error, <Link href={'/dashboard'}>go home</Link>
        </React.Fragment>
      }
    />
  );
};

export async function getServerSideProps(ctx) {
  const { error } = ctx.query;
  return { props: { error } };
}

export default ErrorPage;
