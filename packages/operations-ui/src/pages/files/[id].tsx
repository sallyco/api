import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Container, Button, Grid } from '@mui/material';
import PdfPreview from '../../components/common/PdfPreview';
import Link from 'next/link';

import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';
import { BasicTabs, ContentLoader, TitleHeader, DeletePopup, InfoCard } from '@packages/gbt-ui';
import { FileDownloadIcon, FileExportIcon } from 'react-line-awesome';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { SoftDeleteById } from '../../services/DeleteService';
import { DownloadFile } from '../../tools/download';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
}));

const checkIfPDF = (fileName) => {
  if (fileName) {
    const fileNameParts = fileName.split('.');
    const ext = fileNameParts[fileNameParts.length - 1];
    switch (ext.toLowerCase()) {
      case 'pdf':
        return true;
    }
    return false;
  }
  return false;
};

const FileDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);

  const op = objectProps.files;
  const { data: file, mutate: mutateCurrent } = useRequest(`${op.route}/${id}`);

  return (
    <LoggedInLayout title={`${file?.name ?? 'Loading'} ~ Files`}>
      {!file ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={file?.name}>
              <EditButton op={op} id={id} mutate={mutateCurrent} />
              {checkIfPDF(file?.name) ? (
                <Link href={`/files/view/${file.id}`} passHref>
                  <Button
                    color="primary"
                    size="small"
                    variant="outlined"
                    endIcon={<FileExportIcon />}
                  >
                    View
                  </Button>
                </Link>
              ) : (
                <Button
                  color="primary"
                  size="small"
                  variant="outlined"
                  endIcon={<FileDownloadIcon />}
                  onClick={async () => {
                    await DownloadFile(file.id, file.name, file.tenantId ?? 'master');
                  }}
                >
                  Download
                </Button>
              )}
            </TitleHeader>

            <BasicTabs
              tabName={'files-details-ui-tab'}
              headerColor="primary"
              tabs={[
                {
                  tabName: 'Details',
                  tabContent: (
                    <Grid container spacing={2}>
                      <Grid item sm={8} xl={9} xs={12}>
                        <InfoCard title={'File Details'} item={file} icon={op.icon} />
                      </Grid>
                      {checkIfPDF && (
                        <Grid item sm={4} xl={3} xs={12}>
                          <PdfPreview file={file} />
                        </Grid>
                      )}
                    </Grid>
                  ),
                },
                {
                  tabName: 'Advanced',
                  tabContent: (
                    <DeletePopup
                      objectId={file?.id}
                      objectType={'File'}
                      name={file?.name}
                      processing={processing}
                      onClickOk={async () => {
                        setProcessing(true);
                        await SoftDeleteById(file?.id, 'file');
                        router.push('/files');
                        setProcessing(false);
                      }}
                    />
                  ),
                },
              ]}
            />
          </Container>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default FileDetails;
