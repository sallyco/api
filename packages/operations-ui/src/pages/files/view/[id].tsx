import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import PdfDocument from '../../../components/common/PdfDocument';

import { ContentLoader } from '@packages/gbt-ui';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInFileViewLayout from '../../../components/layouts/LoggedInFileViewLayout';

const FileDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const op = objectProps.files;
  const { data: file } = useRequest(`${op.route}/${id}`);

  return (
    <LoggedInFileViewLayout title={`${file?.name ?? 'Loading'} ~ Files`} file={file}>
      {!file ? <ContentLoader /> : <PdfDocument fileId={id} tenantId={file.tenantId} />}
    </LoggedInFileViewLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default FileDetails;
