import React from 'react';
import { Container, Box } from '@mui/material';
import { NextPage } from 'next';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { ContentLoader, TitleHeader } from '@packages/gbt-ui';
import FilesTable from '../../components/files/FilesTable';

const Files: NextPage = () => {
  const op = objectProps.files;
  const { data: files } = useRequest(`${op.route}`);

  return (
    <LoggedInLayout title={'Files'}>
      {!files ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Files'} />
          </Container>
          <Box mt={2}>
            <FilesTable files={files.data} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Files;
