import React from 'react';
import { useRequest } from '@packages/wired-gbt-ui';
import { NextPage } from 'next';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader, TableWithTools } from '@packages/gbt-ui';

import DecisionModal from '../../components/workflows/DecisionModal';

import { Container, Box } from '@mui/material';

const Decisions: NextPage = () => {
  const { data, isValidating } = useRequest('/decisions');

  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [selected, setSelected] = React.useState<any>(undefined);

  return (
    <LoggedInLayout title={'Decisions'}>
      {!data ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Decision Definitions'} />
          </Container>
          <Box mt={2}>
            <TableWithTools
              data={data}
              columnDefinitions={[
                {
                  name: 'id',
                  label: 'Id',
                  options: {
                    display: false,
                    filter: false,
                  },
                },
                {
                  name: 'deploymentId',
                  label: 'Deployment Id',
                  options: {
                    display: false,
                    filter: false,
                  },
                },
                {
                  name: 'name',
                  label: 'Name',
                },
                {
                  name: 'key',
                  label: 'Resource',
                  options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => `/${value}`,
                  },
                },
                {
                  name: 'resource',
                  label: 'Filename',
                  options: {
                    filter: false,
                  },
                },
                {
                  name: 'version',
                  label: 'Version',
                },
              ]}
              onRowClick={(r, m) => {
                setSelected(data.find((x) => x.id === r[0]));
                setOpen(true);
              }}
              elevation={0}
              selectable={false}
              filter={false}
              download={false}
              clickable
            />
          </Box>
          <DecisionModal open={open} setOpen={setOpen} workflowDecision={selected} />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Decisions;
