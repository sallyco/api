import React, { useState, useRef } from 'react';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { TitleHeader } from '@packages/gbt-ui';
import {
  Backdrop,
  CircularProgress,
  Container,
  Grid,
  Box,
  Divider,
  Button,
  ButtonGroup,
} from '@mui/material';

import { doChange } from '@packages/wired-gbt-ui';
import SignatureCanvas from 'react-signature-canvas';
import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { Close } from '../api/closesApi';
import ClosePicker from '../components/common/ClosePicker';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  approve: {
    backgroundColor: 'green',
  },
  sublistDivider: {
    marginTop: theme.spacing(1),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Signing = () => {
  const classes = useStyles();

  const [selectedClose, setSelectedClose] = React.useState<Close | undefined>(undefined);
  const [canAccept, setCanAccept] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [submitting, setSubmitting] = useState(false);

  const sigCanvas = useRef(SignatureCanvas);

  const handleClick = async () => {
    setSubmitting(true);
    const createdSig = sigCanvas.current.getTrimmedCanvas().toDataURL('image/png');
    await doChange(`/closes/${selectedClose.id}/fund-manager-sign`, 'POST', {
      fundManagerSignature: createdSig,
    });
    setSubmitting(false);
    setSelectedClose(undefined);
  };

  const sigClear = () => {
    setCanAccept(false);
    sigCanvas.current.clear();
  };

  return (
    <LoggedInLayout title={'Signing'}>
      <Container>
        <TitleHeader title={'Signing'} />
        <Divider />
        <Box mt={2}>
          <Grid container>
            <Grid item xs={12}>
              <ClosePicker whereField={'fundManagerSigned'} setSelectedClose={setSelectedClose} />
            </Grid>
          </Grid>
          {selectedClose && (
            <Grid container>
              <Grid item xs={12}>
                <div style={{ border: '1px solid black', backgroundColor: '#FFF' }}>
                  <SignatureCanvas
                    ref={sigCanvas}
                    penColor="black"
                    canvasProps={{
                      width: 1000,
                      height: 200,
                      className: 'sigCanvas',
                    }}
                    attached
                    onEnd={() => setCanAccept(true)}
                  />
                </div>
                <ButtonGroup variant="contained" fullWidth>
                  <Button onClick={sigClear} color="secondary" variant="contained">
                    Clear
                  </Button>
                  <Button
                    onClick={handleClick}
                    disabled={!canAccept}
                    color="primary"
                    variant="contained"
                  >
                    Apply Signature
                  </Button>
                </ButtonGroup>
              </Grid>
            </Grid>
          )}
        </Box>
        <Backdrop className={classes.backdrop} open={submitting}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </Container>
    </LoggedInLayout>
  );
};
/**
              <div key={doc}>
                <PdfDocument fileId={doc} pageNumber={1} />
                <Divider hidden />
              </div>
            <Container textAlign={"center"}>
              <Formik
                initialValues={{}}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form onSubmit={props.handleSubmit} data-testid={"login-form"}>
                    <SignatureForm
                      name={"managerSignature"}
                      id={"managerSignature"}
                    />
                    {closesById[selectedCloseId]?.files &&
                      closesById[selectedCloseId]?.files.map((doc) => (
                      ))}
                    <Button
                      primary
                      fluid
                      type="submit"
                      data-testid={"submit-button"}
                      loading={props.isSubmitting}
                      disabled={!props.dirty || !props.isValid}
                      content="Sign Documents"
                    />
                  </Form>
                )}
              </Formik>
            </Container>
    <LoggedInLayout title={"Signings"}>
      <Container text>
        <Grid style={{paddingTop: "2rem"}}>
          <Grid.Row columns={"equal"}>
            <Grid.Column>
              <Header content={"SPVs Ready for Signing"} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={"equal"}>
            <Grid.Column>
              <Dropdown
                clearable
                fluid
                search
                selection
                options={Object.values(closesById)
                  .filter(readyToSign)
                  .map((close) => {
                    return {
                      key: close.id,
                      value: close.id,
                      text: `${dealsById[close.dealId]?.name} -- ${formatDate(
                        close.createdAt
                      )}`,
                    };
                  })}
                closeOnChange
                closeOnEscape
                placeholder="Choose a close"
                onChange={(e, {value}) => {
                  dispatch(setSelectedClose(value));
                  setShowForm(value !== "");
                }}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Divider />
    </LoggedInLayout>
  );
};
**/

export default Signing;
