import React from 'react';
import { Container, Button, Box } from '@mui/material';
import { PlusIcon } from 'react-line-awesome';
import Link from 'next/link';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import TenantsTable from '../../components/tenants/TenantsTable';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';

const Tenants = () => {
  const op = objectProps.tenants;
  const { data: tenants } = useRequest(`${op.route}?page=0&perPage=100`);

  return (
    <LoggedInLayout title={'Tenants'}>
      {!tenants ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Tenants'}>
              <Link href="/tenants/create" passHref>
                <Button color="primary" size="small" variant="outlined" startIcon={<PlusIcon />}>
                  Create New
                </Button>
              </Link>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <TenantsTable tenants={tenants} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Tenants;
