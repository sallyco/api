import React from 'react';
import { styled } from '@mui/material/styles';
import { Typography, Tabs, Tab, AppBar, Box } from '@mui/material';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import TenantCreateModal from 'src/components/tenants/TenantCreateModal-v2';
import FileUploader from 'src/components/common/FileUploader';
import { SnackbarUtils } from '@packages/wired-gbt-ui';
import { useRouter } from 'next/router';

const PREFIX = 'Tenants';

const classes = {
  root: `${PREFIX}-root`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    margin: 0,
    padding: theme.spacing(2),
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const Tenants = () => {
  function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {<Box p={3}>{children}</Box>}
      </Typography>
    );
  }

  function a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
  }

  const router = useRouter();

  const [value, setValue] = React.useState(0);
  const [companyId, setCompanyId] = React.useState('');
  const [tenantId, setTenantId] = React.useState('');
  const [Tab1Data, setTab1Data] = React.useState(null);
  const [Tab2Data, setTab2Data] = React.useState(null);
  const [Tab3Data, setTab3Data] = React.useState(null);

  const handleChange = (event, newValue) => {
    if (tenantId !== '' || companyId !== '' || newValue === 0 || newValue === 1) {
      setValue(newValue);
    } else {
      SnackbarUtils.warning(`Please add Teanant Details(tab1 & tab2)`);
    }
  };

  return (
    <Root>
      <LoggedInLayout title={'Tenants'}>
        <React.Fragment>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Tab label="New Tenant" {...a11yProps(0)} />
              <Tab label="Contract Administration" {...a11yProps(1)} />
              <Tab label="Master Series Creation " {...a11yProps(2)} />
              <Tab label="Documents" {...a11yProps(3)} />
            </Tabs>
          </AppBar>
          <div className={classes.root}>
            <TabPanel value={value} index={0}>
              <TenantCreateModal
                title={'Tenant Details'}
                item_name={'tenant-tab-1'}
                items_name={'tenants'}
                currentTabdata={Tab1Data}
                setTabData={setTab1Data}
                setTabIndex={setValue}
                tenantId={tenantId}
                setTenantId={setTenantId}
              />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <TenantCreateModal
                title={'Contracts Administration Details'}
                items_name={'tenants'}
                item_name={'tenant-tab-2'}
                previousTabData={Tab1Data}
                currentTabdata={Tab2Data}
                setTabData={setTab2Data}
                setTabIndex={setValue}
                tenantId={tenantId}
                setTenantId={setTenantId}
              />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <TenantCreateModal
                title={'Master Series Details'}
                items_name={'companies'}
                item_name={'tenant-tab-3'}
                currentTabdata={Tab3Data}
                setTabData={setTab3Data}
                setTabIndex={setValue}
                tenantId={tenantId}
                companyId={companyId}
                setCompanyId={setCompanyId}
                previousTabData={Tab1Data}
              />
            </TabPanel>
            <TabPanel value={value} index={3}>
              {tenantId !== '' ? (
                <FileUploader
                  objectType={'tenants'}
                  objectId={tenantId}
                  tenantId={'gbt'}
                  omitFields={[]}
                  uploadSuccess={async (fileIds) => {
                    SnackbarUtils.success(
                      `File${fileIds.length > 1 ? 's' : ''} uploaded to Tenant:${tenantId}`
                    );
                    router.push('/tenants');
                  }}
                />
              ) : (
                <p>{'Please Add TenantDetails'}</p>
              )}
            </TabPanel>
          </div>
        </React.Fragment>
      </LoggedInLayout>
    </Root>
  );
};
export default Tenants;
