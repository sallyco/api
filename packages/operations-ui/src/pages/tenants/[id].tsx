import React, { useEffect, useState } from 'react';
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Container } from '@mui/material';

import { BasicTabs, ContentLoader, TitleHeader, InfoCard } from '@packages/gbt-ui';

import { useRequest, doChange, objectProps, EditButton } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import _ from 'lodash';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
}));

const TenantDetails: NextPage = () => {
  const classes = useStyles();
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);
  const [editingDealLimit, setEditingDealLimit] = useState(false);
  const [editingMaxDaysToClose, setEditingMaxDaysToClose] = useState(false);

  const [activeTab, setActiveTab] = useState(0);

  const op = objectProps.tenants;
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: 'manager',
          scope: {
            fields: ['id', 'name', 'ein'],
          },
        },
        {
          relation: 'masterEntity',
          scope: {
            fields: ['id', 'name'],
          },
        },
        {
          relation: 'whiteLabel',
        },
        {
          relation: 'realm',
        },
        {
          relation: 'feesPaymentsAccount',
        },
        {
          relation: 'blueSkyPaymentsAccount',
        },
      ],
    })
  );
  const { data: tenant, mutate: mutateCurrent } = useRequest(
    `${op.route}/${id}?filter=${encodedFilter}`
  );
  const [tenantData, setTenantData] = useState(tenant);
  useEffect(() => {
    if (!_.isEqual(tenantData, tenant)) {
      setTenantData(tenant);
    }
  }, [tenant]);

  const createHandler = async (purpose: string) => {
    const payload = {
      accountPurpose: purpose,
    };
    const url = `/tenants/${id}/bank-account`;
    await doChange(url, 'POST', payload, mutateCurrent);
  };

  const omitFields = ['dealLimit', 'whiteLabel', 'realm', 'masterEntity', 'manager'];

  const LinkFields = new Map();
  LinkFields.set('managerId', {
    value_field: 'manager',
    url_field: 'companies',
  });
  LinkFields.set('masterEntityId', {
    value_field: 'masterEntity',
    url_field: 'companies',
  });

  const titleChanges = new Map();
  titleChanges.set('closedStats', {
    title: 'Deal Limit',
    subTitle: '(Closed/Available)',
  });

  return (
    <LoggedInLayout title={`${tenant?.name ?? 'Loading'} ~ Tenants`}>
      {!tenant ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={tenant.name}>
              <EditButton
                op={activeTab === 0 ? op : objectProps['white-label']}
                id={activeTab === 0 ? tenantData?.id : tenant.whiteLabel.id}
                mutate={mutateCurrent}
                disabled={activeTab === 2}
              />
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'tenants-details-ui-tab'}
            headerColor="primary"
            handleTabChange={(r) => setActiveTab(r)}
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      icon={op.icon}
                      title={'Tenant Details'}
                      item={tenant}
                      titleChanges={titleChanges}
                      omitFields={omitFields}
                      linkfields={LinkFields}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'White Label',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      icon={objectProps['white-label'].icon}
                      title={'White Label'}
                      item={tenant.whiteLabel}
                      omitFields={['id']}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Realm (Keycloak)',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      icon={objectProps['realm'].icon}
                      title={'Realm'}
                      item={tenant.realm}
                    />
                  </Container>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default TenantDetails;
