import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { SnackbarUtils, useRequest, objectProps } from '@packages/wired-gbt-ui';

import { Button, Container, Box, Grid } from '@mui/material';

import {
  BasicTabs,
  ContentLoader,
  TitleHeader,
  DeletePopup,
  InfoCard,
  MiniCard,
  StatementTable,
} from '@packages/gbt-ui';

import moment from 'moment';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import ImpersonateButton from '../../components/common/ImpersonateButton';
import { FilePdfIcon } from 'react-line-awesome';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const CloseDetails: NextPage = () => {
  const router = useRouter();
  const op = objectProps.closes;
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';

  const [processing, setProcessing] = useState(false);
  const {
    data: close,
    isValidating,
    mutate,
  } = useRequest(`${op.route}/${id}?filter[include][]=deal&filter[include][]=entity`);

  const omitFields = ['deal', 'entity', 'statement'];
  const LinkFields = new Map();
  LinkFields.set('dealId', { value_field: 'deal', url_field: 'deals' });
  LinkFields.set('entityId', { value_field: 'entity', url_field: 'entities' });
  const [fileData, setFileData] = useState('');

  const downloadStatement = async () => {
    const response = await fetch(`${baseUrl}/closes/${id}/fees-statement/pdf`);
    const contentLength = response.headers.get('content-length');
    const total = parseInt(contentLength, 10);
    let loaded = 0;
    const res = new Response(
      new ReadableStream({
        async start(controller) {
          const reader = response.body.getReader();
          for (;;) {
            const { done, value } = await reader.read();
            if (done) break;
            loaded += value.byteLength;
            controller.enqueue(value);
          }
          controller.close();
        },
      })
    );
    const blob = await res.blob();
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'statement.pdf';
    document.body.appendChild(link);
    link.click();
  };

  const onDelete = () => {
    const baseUrl = process.env.NEXT_PUBLIC_API_URL;
    fetch(`${baseUrl}/${op.route}/${id}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    }).then((r) => {
      console.log(r);
      if (!r.ok) {
        SnackbarUtils.error('There was an error');
        throw r;
      }
      router.push(op.route);
    });
  };

  return (
    <LoggedInLayout
      title={`${
        close?.deal?.name
          ? `${close?.deal?.name} [${moment(close?.createdAt).local().format('DD-MMM-yyyy')}]`
          : 'Loading'
      } ~ Close`}
    >
      {!close ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader
              title={`${close?.deal?.name} [${moment(close?.createdAt)
                .local()
                .format('DD-MMM-yyyy')}]`}
            >
              <ImpersonateButton tenantId={close?.tenantId} ownerId={close?.ownerId} />
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'closes-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <Grid container spacing={2}>
                      <Grid item sm={8} xl={9} xs={12}>
                        <InfoCard
                          icon={op.icon}
                          title={'Close Details'}
                          item={close}
                          omitFields={omitFields}
                          linkfields={LinkFields}
                        />
                      </Grid>
                      <Grid item sm={4} xl={3} xs={12}>
                        <Box pb={2}>
                          <MiniCard
                            title={'Deal'}
                            icon={objectProps.deals.icon}
                            item={close.deal}
                            url={objectProps.deals.name}
                          />
                        </Box>
                        <Box pb={2}>
                          <MiniCard
                            title={'Entity'}
                            icon={objectProps.entities.icon}
                            item={close.entity}
                            url={objectProps.entities.name}
                          />
                        </Box>
                      </Grid>
                    </Grid>
                  </Container>
                ),
              },
              ...(close.statement
                ? [
                    {
                      tabName: 'Statement',
                      tabContent: (
                        <Container>
                          <Button
                            onClick={downloadStatement}
                            color="primary"
                            variant="contained"
                            startIcon={<FilePdfIcon />}
                            fullWidth
                          >
                            Export to PDF
                          </Button>
                          <Box mt={2}>
                            <StatementTable statementData={close.statement} hideLoading={true} />
                          </Box>
                        </Container>
                      ),
                    },
                  ]
                : []),
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={close?.id}
                    objectType={'Close (unwind)'}
                    name={close?.deal?.name}
                    processing={processing}
                    onClickOk={onDelete}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default CloseDetails;
