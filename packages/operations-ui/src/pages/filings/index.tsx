import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import _ from 'lodash';
import moment from 'moment';

import { Container } from '@mui/material';

import { BasicTabs, TitleHeader, ContentLoader } from '@packages/gbt-ui';
import { useRequest } from '@packages/wired-gbt-ui';
import ClosedDealsTable from '../../components/filings/ClosedDealsTable';
import FilingsTable from '../../components/filings/FilingsTable';

const cikDefinitions = {
  needscik: {
    id: 'needscik',
    title: 'Needs CIK',
  },
  waitingclose: {
    id: 'waitingclose',
    title: 'Awaiting Close',
  },
};
const filingDefinitions = {
  needsfiling: {
    id: 'needsfiling',
    title: 'Needs Filing',
  },
  needsblue: {
    id: 'needsblue',
    title: 'Needs Blue Sky',
  },
};

// Create a list in the format for the Kanban component
// Created manually to guarantee order
const cikLists = [cikDefinitions.needscik, cikDefinitions.waitingclose];
const filingLists = [filingDefinitions.needsfiling, filingDefinitions.needsblue];

function determineFormDKanbanList(entry) {
  let listId = '';
  if (entry.formD?.blueSkyReceipt) {
    listId = '';
  } else if (entry.formD?.cik && entry.formD?.fileNumber) {
    listId = filingDefinitions.needsblue.id;
  } else if (entry.formD?.cik && entry.close) {
    listId = filingDefinitions.needsfiling.id;
  } else if (entry.formD?.cik) {
    listId = cikDefinitions.waitingclose.id;
  } else {
    listId = cikDefinitions.needscik.id;
  }

  return listId;
}

const Filings: NextPage = () => {
  const router = useRouter();
  const { data, error } = useRequest('/entities-to-file');
  const [searchText, setSearchText] = React.useState('');

  const cards = _(data)
    .filter((entry) => {
      return (
        (!searchText || entry.deal?.name?.toLowerCase().includes(searchText.toLowerCase())) &&
        (!entry.hasOwnProperty('formD') || !entry.formD.hasOwnProperty('blueSkyReceipt'))
      );
    })
    .map((entry) => {
      const dueDate = moment
        .utc(entry.deal?.estimatedCloseDate ?? entry.createdAt)
        .add(14, 'days')
        .local();

      let className = '';
      if (dueDate < moment()) className = 'error';
      else if (dueDate < moment().add(3, 'days')) className = 'warning';

      return {
        id: entry.id,
        listId: determineFormDKanbanList(entry),
        onClick: () => router.push(`/filings/${entry.id}`),
        footer: `Due: ${dueDate.format('ll')} (${dueDate.fromNow()})`,
        title: entry?.deal?.name,
        description: entry?.name,
        className: className,
      };
    })
    .sortBy('createdAt')
    .reverse()
    .value();

  return (
    <LoggedInLayout title={'Form D Filings'}>
      {!data ? (
        <ContentLoader />
      ) : (
        <Container maxWidth={false}>
          <TitleHeader title={'Form D Filings'} />
          <BasicTabs
            tabName={'filings-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Regulatory Filings and BlueSky',
                tabContent: <FilingsTable filingData={cards} />,
              },
              {
                tabName: 'Closed',
                tabContent: <ClosedDealsTable />,
              },
            ]}
          />
        </Container>
      )}
    </LoggedInLayout>
  );
};

export default Filings;
