import React from 'react';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';

import { BasicTabs } from '@packages/gbt-ui';

const Account = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <LoggedInLayout title={'User Settings'}>
      <BasicTabs
        tabName={'settings-ui-tab'}
        headerColor="primary"
        tabs={[
          {
            tabName: 'Account',
            tabContent: <p>Account</p>,
          },
          {
            tabName: 'Profile',
            tabContent: <p>Profile</p>,
          },
          {
            tabName: 'Notifications',
            tabContent: <p>Notifications</p>,
          },
        ]}
      />
    </LoggedInLayout>
  );
};

export default Account;
