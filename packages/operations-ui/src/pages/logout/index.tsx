import React from 'react';
import { NextPage, NextPageContext } from 'next';
import { getSession, signOut } from 'next-auth/client';
import { getTenantIdFromReq } from '../../tools/urlFormatters';

interface Props {}

const Logout: NextPage = () => {
  return <></>;
};
const url = process.env.NEXT_PUBLIC_AUTH_URL;

Logout.getInitialProps = async (ctx: NextPageContext) => {
  const session = await getSession(ctx);
  const { req } = ctx;
  const tenantId = getTenantIdFromReq(req);
  const logoutUrl = `${process.env.NEXT_PUBLIC_AUTH_URL}/${tenantId}/protocol/openid-connect/logout`;

  if (session?.refreshToken) {
    fetch(logoutUrl, {
      body: new URLSearchParams({
        client_id: process.env.NEXT_PUBLIC_CLIENT_ID,
        client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
        refresh_token: String(session.refreshToken),
      }),
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }).then((r) => {
      signOut({ redirect: false, callbackUrl: '/' });
    });
  } else {
    signOut({ redirect: false, callbackUrl: '/' });
  }
  return {};
};

export default Logout;
