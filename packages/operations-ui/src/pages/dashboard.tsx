import React from 'react';
import { styled } from '@mui/material/styles';
import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { Container, Grid, Paper } from '@mui/material';
import ChartDialog from 'src/forms/ChartDialog';
import useSWR from 'swr';
import { fetcher } from '@packages/wired-gbt-ui';
import KycAmlBasicChart from '../../src/components/dashboard/KycAmlBasicChart';
import FinancialTransactionsBasicChart from '../../src/components/dashboard/FinancialTransactionsBasicChart';

const PREFIX = 'Dashboard';

const classes = {
  root: `${PREFIX}-root`,
  paper: `${PREFIX}-paper`,
  gridBox: `${PREFIX}-gridBox`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.root}`]: {
    flexGrow: 1,
    margin: 5,
  },

  [`& .${classes.paper}`]: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    maxHeight: 500,
    height: 475,
    width: '100%',
    overflow: 'auto',
  },

  [`& .${classes.gridBox}`]: {
    marginTop: theme.spacing(1),
    '&:hover': {
      background: '#a9b2bf14',
    },
  },
}));

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const Dashboard = () => {
  const metadataDealUrl = baseUrl + `/fund-docs`;
  const { data: fundDocs, error } = useSWR(metadataDealUrl, fetcher);

  function Row1() {
    return (
      <React.Fragment>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'KYC/AML'} chart_Name={'kycaml'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}>
            <KycAmlBasicChart />
          </Paper>
        </Grid>

        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog
            title={'FINANCIAL TRANSACTIONS'}
            chart_Name={'financial-trans'}
            chart_Data={null}
          ></ChartDialog>
          <Paper className={classes.paper}>
            <FinancialTransactionsBasicChart />
          </Paper>
        </Grid>

        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
      </React.Fragment>
    );
  }

  function Row2() {
    return (
      <React.Fragment>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>

        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
      </React.Fragment>
    );
  }

  function Row3() {
    return (
      <React.Fragment>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
        <Grid item xs={4} className={classes.gridBox}>
          <ChartDialog title={'To Do'} chart_Name={'To Do'} chart_Data={null}></ChartDialog>
          <Paper className={`${classes.paper}`}></Paper>
        </Grid>
      </React.Fragment>
    );
  }
  return (
    <Root>
      <LoggedInLayout title={'Home'}>
        <Container>
          {/* <Box mx={{ xs: 2, sm: 4 }} my={4}>
          <Search />
        </Box> */}
        </Container>
        <div className={classes.root}>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={2}>
              <Row1 />
            </Grid>
            <Grid container item xs={12} spacing={2}>
              <Row2 />
            </Grid>
            <Grid container item xs={12} spacing={2}>
              <Row3 />
            </Grid>
          </Grid>
        </div>
      </LoggedInLayout>
    </Root>
  );
};

export default Dashboard;
