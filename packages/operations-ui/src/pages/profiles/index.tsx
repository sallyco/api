import React from 'react';
import { PlusIcon } from 'react-line-awesome';
import { Container, Button, Box } from '@mui/material';
import { useRouter } from 'next/router';
import { NextPage } from 'next';

import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { TitleHeader, ContentLoader } from '@packages/gbt-ui';
import ProfilesTable from '../../components/profiles/ProfilesTable';

const Profiles: NextPage = () => {
  const router = useRouter();
  const op = objectProps.profiles;

  const { data: profiles } = useRequest(`${op.route}?page=0&perPage=100`);

  return (
    <LoggedInLayout title={'Profiles'}>
      {!profiles ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={op.label}>
              <Button color="primary" size="small" startIcon={<PlusIcon />}>
                Create New Profile
              </Button>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <ProfilesTable profiles={profiles.data} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Profiles;
