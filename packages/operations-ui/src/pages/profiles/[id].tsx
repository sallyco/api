import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useRequest, doChange, objectProps, EditButton } from '@packages/wired-gbt-ui';
import { Button, Container, Tooltip } from '@mui/material';

import {
  BasicTabs,
  ContentLoader,
  TitleHeader,
  DeletePopup,
  InfoCard,
  FullScreenDialog,
} from '@packages/gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import AddressForm from '../../components/profiles/AddressForm';
import { startImpersonateSession } from '../../services/ImpersonateService';
import { AddressCardIcon, ExternalLinkSquareAltIcon } from 'react-line-awesome';
import { SoftDeleteById } from '../../services/DeleteService';

const baseUrl = process.env.NEXT_PUBLIC_API_URL;

const ProfileDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';

  const [processing, setProcessing] = useState(false);
  const [showAddressForm, setShowAddressForm] = useState(false);

  const op = objectProps.profiles;
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: 'owner',
        },
      ],
    })
  );
  const { data: profile, mutate: mutateCurrent } = useRequest(
    `${op.route}/${id}?filter=${encodedFilter}`
  );

  const LinkFields = new Map();
  LinkFields.set('ownerId', {
    value_field: 'owner',
    url_field: 'users',
  });

  return (
    <LoggedInLayout title={`${profile?.name ?? 'Loading'} ~ Profile`}>
      {!profile ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={profile?.name ?? `${profile?.firstName} ${profile?.lastName}`}>
              <EditButton op={op} id={id} mutate={mutateCurrent} />
              <Button
                color="primary"
                size="small"
                onClick={() => setShowAddressForm(true)}
                startIcon={<AddressCardIcon />}
              >
                Change Address
              </Button>
              <Tooltip
                title={
                  !profile.tenantId || !profile.ownerId ? 'Can’t impersonate from this page' : ''
                }
                placement="top"
                arrow
              >
                <span>
                  <Button
                    color="primary"
                    size="small"
                    onClick={() =>
                      startImpersonateSession(
                        profile?.tenantId,
                        profile?.ownerId,
                        `dashboard/deals`
                      )
                    }
                    startIcon={<ExternalLinkSquareAltIcon />}
                    disabled={!profile.tenantId || !profile.ownerId}
                  >
                    Impersonate
                  </Button>
                </span>
              </Tooltip>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'profiles-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container>
                    <InfoCard
                      icon={op.icon}
                      title={'Profile Details'}
                      item={profile}
                      linkfields={LinkFields}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={profile?.id}
                    objectType={'Profile'}
                    name={profile?.name}
                    processing={processing}
                    onClickOk={async () => {
                      setProcessing(true);
                      await SoftDeleteById(profile?.id, 'profile');
                      router.push('/profiles');
                      setProcessing(false);
                    }}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
      {showAddressForm && (
        <FullScreenDialog title={''} open={showAddressForm} setOpen={setShowAddressForm}>
          <AddressForm
            address={profile?.address}
            onSuccess={async (address) => {
              setShowAddressForm(false);
              await doChange(`${baseUrl}${op.route}/${profile.id}`, 'PUT', { address });
              router.reload();
            }}
          />
        </FullScreenDialog>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default ProfileDetails;
