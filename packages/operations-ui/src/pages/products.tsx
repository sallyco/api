import React from 'react';
import { Theme } from '@mui/material/styles';

import makeStyles from '@mui/styles/makeStyles';

import LoggedInLayout from '../components/layouts/LoggedInLayout';
import { TitleHeader, BasicTabs } from '@packages/gbt-ui';

import { Container } from '@mui/material';
import { objectProps } from '@packages/wired-gbt-ui';

import * as ProdComp from '../components/products';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  icon: {
    fontSize: 'xx-large',
  },
}));
const Product = () => {
  const classes = useStyles();
  const op = objectProps.products;
  return (
    <LoggedInLayout title={op.label}>
      <Container maxWidth={false}>
        <TitleHeader title={op.label} />
      </Container>
      <BasicTabs
        tabName={'products-ui-tab'}
        headerColor="primary"
        tabs={[
          //{
          //  tabName: "Overview",
          //  tabIcon: <PeopleCarryIcon className={classes.icon} />,
          //  tabContent: <></>,
          //},
          //{
          //  tabName: "Default Package",
          //  tabIcon: <VectorSquareIcon className={classes.icon} />,
          //  tabContent: <></>,
          //},
          {
            tabName: objectProps.packages.label,
            tabIcon: <span className={classes.icon}>{objectProps.packages.icon}</span>,
            tabContent: <ProdComp.PackagesTab />,
          },
          {
            tabName: op.label,
            tabIcon: <span className={classes.icon}>{op.icon}</span>,
            tabContent: <ProdComp.ProductsTab />,
          },
          {
            tabName: 'Categories',
            tabIcon: <span className={classes.icon}>{objectProps['product-categories'].icon}</span>,
            tabContent: <ProdComp.ProductCategoriesTab />,
          },
          {
            tabName: 'Attributes',
            tabIcon: <span className={classes.icon}>{objectProps['package-attributes'].icon}</span>,
            tabContent: <ProdComp.PackageAttributesTab />,
          },
          //{
          //  tabName: "Types",
          //  tabIcon: <CircleIcon className={classes.icon} />,
          //  tabContent: <></>,
          //},
        ]}
      />
    </LoggedInLayout>
  );
};

export default Product;
