import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { ButtonGroup, Container } from '@mui/material';

import { useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';

import { BasicTabs, ContentLoader, TitleHeader, InfoCard } from '@packages/gbt-ui';

import FeesStatement from '../../components/deals/FeesStatement';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import ImpersonateButton from '../../components/common/ImpersonateButton';
//import { fetchEntityById } from "../../slices/entitiesSlice";
//import { fetchFileById } from "../../slices/filesSlice";
//import { fetchSubscriptionsListByDeal } from "../../slices/subscriptionsSlice";
import SubscriptionsTable from '../../components/subscriptions/SubscriptionsTable';

const DealDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [value, setValue] = React.useState(0);
  const [processing, setProcessing] = useState(false);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const op = objectProps.deals;
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: 'profile',
          scope: {
            fields: ['id', 'name', 'firstName', 'lastName', 'email'],
          },
        },
        {
          relation: 'owner',
        },
        {
          relation: 'package',
        },
      ],
    })
  );
  const { data: deal, mutate } = useRequest(`${op.route}/${id}?filter=${encodedFilter}`);

  const url = `/subscriptions-by-deal/${id}?page=0&perPage=100&filter[include][]=profile`;
  const { data: subscriptions, isValidating } = useRequest(url);

  //const { filesById } = useSelector((state: RootState) => state.files);

  //useEffect(() => {
  //  if (deal && entity && entity.id === deal.entityId) {
  //    let filesForDeal = [
  //      entity?.entityDocuments?.operatingAgreement,
  //      entity?.entityDocuments?.privatePlacementMemorandum,
  //      entity?.entityDocuments?.subscriptionAgreement
  //    ];
  //    if (subscriptions) {
  //      Object.entries(subscriptions).forEach(([key, subscription]) => {
  //        if (subscription.dealId === deal.id && subscription.files) {
  //          filesForDeal.push(...subscription.files);
  //        }
  //      });
  //    }
  //    if (deal.organizerFiles) {
  //      filesForDeal.push(...deal.organizerFiles);
  //    }
  //    if (deal.files) {
  //      filesForDeal.push(...deal.files);
  //    }
  //    if (entity.files) {
  //      filesForDeal.push(...entity.files);
  //    }
  //    filesForDeal.forEach(fileId => {
  //      dispatch(fetchFileById(fileId));
  //    });
  //  }
  //}, [deal, dispatch, entity, subscriptions]);

  const omitFields = ['entity', 'owner', 'package'];
  const LinkFields = new Map();
  LinkFields.set('entityId', { value_field: 'entity', url_field: 'entities' });
  LinkFields.set('profileId', {
    value_field: 'profile',
    url_field: 'profiles',
  });
  LinkFields.set('ownerId', {
    value_field: 'owner',
    url_field: 'users',
  });
  LinkFields.set('packageId', {
    value_field: 'package',
    url_field: '',
  });

  const titleChanges = new Map();
  titleChanges.set('profile', {
    title: 'Organizer Details',
  });

  return (
    <LoggedInLayout title={`${deal?.name ?? 'Loading'} ~ Deals`}>
      {!deal ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={deal?.name} image={deal?.marketing.logo}>
              <ButtonGroup color="secondary" variant="outlined" size="small">
                <EditButton op={op} id={id} mutate={mutate} />
                <ImpersonateButton tenantId={deal?.tenantId} ownerId={deal?.ownerId} />
              </ButtonGroup>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'deals-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: (
                  <Container maxWidth={false}>
                    <InfoCard
                      title={'Deal Details'}
                      icon={objectProps.deals.icon}
                      item={deal}
                      omitFields={omitFields}
                      linkfields={LinkFields}
                      titleChanges={titleChanges}
                    />
                  </Container>
                ),
              },
              {
                tabName: 'Investors',
                tabContent: <SubscriptionsTable subscriptions={subscriptions?.data} />,
              },
              {
                tabName: 'Fees',
                tabContent: (
                  <Container maxWidth={false}>
                    <FeesStatement dealId={deal.id} />
                  </Container>
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default DealDetails;
