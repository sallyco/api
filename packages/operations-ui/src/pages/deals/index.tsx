import { Container } from '@mui/material';
import { BasicTabs, TitleHeader } from '@packages/gbt-ui';
import { objectProps } from '@packages/wired-gbt-ui';
import LoggedInLayout from '../../components/layouts/LoggedInLayout';

import Dashboard from '../../components/deals/Dashboard';
import DealsTab from 'src/components/deals/DealsTab';

const Deals = () => {
  const op = objectProps.deals;
  return (
    <LoggedInLayout title={`${op.label}`}>
      <Container maxWidth={false}>
        <TitleHeader title={op.label} />
      </Container>
      <BasicTabs
        tabName={'deals-ui-tab'}
        headerColor="primary"
        tabs={[
          {
            tabName: 'Dashboard',
            tabContent: <Dashboard />,
          },
          {
            tabName: 'Listing',
            tabContent: <DealsTab />,
          },
        ]}
      />
    </LoggedInLayout>
  );
};

export default Deals;
