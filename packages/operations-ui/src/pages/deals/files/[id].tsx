import React from 'react';
import { FileExportIcon } from 'react-line-awesome';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import { Container, Box, Button } from '@mui/material';
import { ContentLoader, TitleHeader, InfoCard } from '@packages/gbt-ui';
import LoggedInLayout from '../../../components/layouts/LoggedInLayout';

import { Theme } from '@mui/material/styles';

import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: 'space-between',
  },
}));

const FundFileDetails: NextPage = () => {
  const classes = useStyles();

  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q[0];

  const op = objectProps.files;
  const { data: file } = useRequest(`${op.route}/${id}`);

  return (
    <LoggedInLayout title={`${file?.name ?? 'Loading'} ~ Files`}>
      {!file ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader title={file?.name}>
              <Link href={`/files/view/${file.id}`} passHref>
                <Button color="primary" size="small" endIcon={<FileExportIcon />}>
                  View
                </Button>
              </Link>
            </TitleHeader>
            <Box py={2}>
              <InfoCard title={'File Details'} item={file} icon={op.icon} />
            </Box>
          </Container>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default FundFileDetails;
