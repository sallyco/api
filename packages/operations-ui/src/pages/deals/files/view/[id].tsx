import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { ContentLoader } from '@packages/gbt-ui';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';
import PdfDocument from '../../../../components/common/PdfDocument';
import LoggedInFileViewLayout from '../../../../components/layouts/LoggedInFileViewLayout';

const FundFileDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';

  const op = objectProps.files;
  const { data: file } = useRequest(`${op.route}/${id}`);

  return (
    <LoggedInFileViewLayout title={`${file?.name ?? 'Loading'} ~ Files`} file={file}>
      {!file ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <PdfDocument fileId={id} tenantId={file.tenantId} />
        </React.Fragment>
      )}
    </LoggedInFileViewLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default FundFileDetails;
