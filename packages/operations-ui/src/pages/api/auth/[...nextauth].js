import React, { useContext } from "react";
import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import {
  getTenantIdFromReq,
  getAuthUrl,
  getRedirectUrl,
} from "../../../tools/urlFormatters";

const options = (req) => {
  const tenantId = getTenantIdFromReq(req);
  const url = getAuthUrl(tenantId);

  process.env.NEXTAUTH_URL = getRedirectUrl(tenantId);

  return {
    debug: false,
    session: {
      jwt: true,
      maxAge: 30 * 24 * 60 * 60, // 30 days
    },
    providers: [
      {
        id: "oauth2",
        type: "oauth",
        version: "2.0",
        scope: "openid",
        params: {
          grant_type: "authorization_code",
        },
        accessTokenUrl: `${url}/token`,
        requestTokenUrl: `${url}/auth`,
        authorizationUrl: `${url}/auth?response_type=code`,
        profileUrl: `${url}/userinfo`,
        profile: (profile) => ({ ...profile, id: profile.sub }),
        idToken: true,
        clientId: process.env.NEXT_PUBLIC_CLIENT_ID,
        clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
      },
    ],
    secret: "allyourbasearbelongtoglassboardtechnology",
    pages: {
      signOut: "/logout",
      error: "/error",
    },
    callbacks: {
      async jwt(token, user, account, profile, isNewUser) {
        if (account?.refreshToken) {
          token.refreshToken = account.refreshToken;
        }
        return token;
      },
      async session(sess, token) {
        if (token) {
          sess.user = token.sub;
          sess.refreshToken = token.refreshToken;
        }
        return Promise.resolve(sess);
      },
    },
  };
};

export default (req, res) => NextAuth(req, res, options(req));
