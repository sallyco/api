import React from 'react';
import { NextPage } from 'next';
import { PlusIcon } from 'react-line-awesome';

import { ContentLoader, TitleHeader } from '@packages/gbt-ui';
import { useRequest, objectProps } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import AssetsTable from '../../components/assets/AssetsTable';

import { Container, Button, Box } from '@mui/material';

const Assets: NextPage = () => {
  const op = objectProps.assets;
  const { data: assets } = useRequest(`${op.route}`);

  return (
    <LoggedInLayout title={'Assets'}>
      {!assets ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container maxWidth={false}>
            <TitleHeader title={'Assets'}>
              <Button color="primary" size="small" variant="outlined" startIcon={<PlusIcon />}>
                Create New
              </Button>
            </TitleHeader>
          </Container>
          <Box mt={2}>
            <AssetsTable assets={assets.data} />
          </Box>
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export default Assets;
