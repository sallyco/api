import React, { useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Button, Container, Tooltip } from '@mui/material';

import { InfoCard, BasicTabs, ContentLoader, TitleHeader, DeletePopup } from '@packages/gbt-ui';
import { useRequest, objectProps, EditButton } from '@packages/wired-gbt-ui';

import LoggedInLayout from '../../components/layouts/LoggedInLayout';
import { ExternalLinkSquareAltIcon } from 'react-line-awesome';
import { startImpersonateSession } from '../../services/ImpersonateService';
import { SoftDeleteById } from '../../services/DeleteService';

const AssetDetails: NextPage = () => {
  const router = useRouter();
  const { id: q } = router.query;
  const id = typeof q === 'string' ? q : q != undefined ? q[0] : '0';
  const [processing, setProcessing] = useState(false);

  const op = objectProps.assets;
  const { data: asset, mutate: mutateCurrent } = useRequest(`${op.route}/${id}`);

  const isDisabled =
    asset?.tenantId === '' || asset?.tenantId === null || !asset?.tenantId ? true : false;

  return (
    <LoggedInLayout title={`${asset?.name ?? 'Loading'} ~ Assets`}>
      {!asset ? (
        <ContentLoader />
      ) : (
        <React.Fragment>
          <Container>
            <TitleHeader title={asset?.name}>
              <EditButton op={op} id={id} mutate={mutateCurrent} />
              <Tooltip
                title={isDisabled ? 'Can’t impersonate from this page' : ''}
                placement="top"
                arrow
              >
                <span>
                  <Button
                    color="primary"
                    size="small"
                    onClick={() =>
                      startImpersonateSession(
                        asset?.tenantId,
                        asset?.ownerId,
                        `deals/${asset?.dealId}`
                      )
                    }
                    startIcon={<ExternalLinkSquareAltIcon />}
                    disabled={isDisabled}
                  >
                    Impersonate
                  </Button>
                </span>
              </Tooltip>
            </TitleHeader>
          </Container>
          <BasicTabs
            tabName={'assets-details-ui-tab'}
            headerColor="primary"
            tabs={[
              {
                tabName: 'Details',
                tabContent: <InfoCard title={'Assest Details'} item={asset} />,
              },
              {
                tabName: 'Advanced',
                tabContent: (
                  <DeletePopup
                    objectId={asset?.id}
                    objectType={'Asset'}
                    name={asset?.name}
                    processing={processing}
                    onClickOk={async () => {
                      setProcessing(true);
                      await SoftDeleteById(asset?.id, 'asset');
                      router.push('/assets');
                      setProcessing(false);
                    }}
                  />
                ),
              },
            ]}
          />
        </React.Fragment>
      )}
    </LoggedInLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  return { props: { id } };
}

export default AssetDetails;
