import { SnackbarUtils } from '@packages/wired-gbt-ui';
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

export async function startImpersonateSession(realmId, userId, path) {
  const url = `${baseUrl}/users/${realmId}/${userId}/impersonate/`;

  const response = await fetch(url, { method: 'POST' });
  const result = await response.json();
  if (!response.ok) {
    if (result?.error?.message) SnackbarUtils.warning(result.error.message);
    else SnackbarUtils.warning(result?.error_description);
    return;
  }

  const env = process.env.NEXT_PUBLIC_ENVIRONMENT;
  let domain = `.glassboardtech.com`;
  let redirect_uri = ``;

  if (env === 'localhost') {
    domain = 'localhost';
    redirect_uri = 'http://localhost:3000/#/' + path;
  } else if (env === 'dev' || env === 'qa' || env === 'sandbox') {
    redirect_uri = `https://app.${env}.glassboardtech.com/#/` + path;
  } else if (env == 'production') {
    redirect_uri = `https://${realmId}.glassboardtech.com/#/` + path;
  }

  document.cookie = `__session= ;path=/;domain=${domain}; expires = Thu, 01 Jan 1970 00:00:00 GMT`;
  document.cookie = `__session=${result.access_token};path=/;domain=${domain}`;
  window.open(redirect_uri, '_blank');
}
