import { getColumnDefinition } from '../../src/tools/columnDefinitionGenerator';
import create, { UseStore } from 'zustand';
import { persist } from 'zustand/middleware';

type ViewColumnsState = {
  viewColumns: string[];
  setViewColumns: (v: string, a: string) => void;
};

export class TableWithToolsService {
  private useStore: UseStore<ViewColumnsState>;

  private viewColumns: string[];
  private setViewColumns: (v: string, a: string) => void;

  private fieldMap: any;
  private initialColumnDefinitions: any;

  private actionFieldLabels: { name: string; label: string }[];

  constructor(
    storeName: string,
    fieldMap: any,
    initialColumnDefinitions: any,
    actionFieldLabels: { name: string; label: string }[]
  ) {
    this.useStore = create<ViewColumnsState>(
      persist(
        (set) => ({
          viewColumns: [],
          setViewColumns: (v, a) =>
            set((state) => {
              if (a === 'add') {
                if (state.viewColumns.includes(v)) {
                  return { viewColumns: [...state.viewColumns] };
                }
                return { viewColumns: [...state.viewColumns, v] };
              } else if (a === 'remove') {
                return { viewColumns: state.viewColumns.filter((c) => c !== v) };
              }
              return { viewColumns: state.viewColumns };
            }),
        }),
        {
          name: storeName,
        }
      )
    );
    this.viewColumns = this.useStore((state) => state.viewColumns);
    this.setViewColumns = this.useStore((state) => state.setViewColumns);
    this.fieldMap = fieldMap;
    this.initialColumnDefinitions = initialColumnDefinitions;
    this.actionFieldLabels = actionFieldLabels;
  }

  public getColumnDefinition(): any[] {
    const colDef = getColumnDefinition(this.fieldMap, this.initialColumnDefinitions);
    for (const def of colDef) {
      if (
        this.actionFieldLabels.filter(
          (field) => field.name === def.name && field.label === def.label
        ).length > 0
      )
        continue;

      if (
        this.viewColumns &&
        this.viewColumns.length === 0 &&
        (def.options?.display === undefined || def.options?.display === true)
      ) {
        this.setViewColumns(def.name, 'add');
      }

      if (this.viewColumns && this.viewColumns.length > 0) {
        if (this.viewColumns.includes(def.name)) {
          def.options = {
            ...(def.options ?? {}),
            display: true,
          };
        } else {
          def.options = {
            ...(def.options ?? {}),
            display: false,
          };
        }
      }
    }

    return colDef;
  }

  public onViewColumnsChange(columnDefinitions, v, a): any[] {
    this.setViewColumns(v, a);

    const colDef = columnDefinitions;
    const index = colDef.findIndex(
      (c) =>
        c.name === v &&
        this.actionFieldLabels.filter((field) => field.name === c.name && field.label === c.label)
          .length === 0
    );

    colDef[index]['options'] = {
      ...(colDef[index]['options'] ?? {}),
      display: a === 'add' ? true : false,
    };

    return colDef;
  }
}
