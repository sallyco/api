const DB_STATUSES = {
  INVITED: 'INVITED',
  PENDING: 'PENDING',
  COMMITTED: 'COMMITTED',
  COMPLETED: 'COMPLETED',
  FUNDS_IN_TRANSIT_MANUAL: 'FUNDS_IN_TRANSIT-MANUAL',
  COMPLETED_FUNDS_RECEIVED: 'COMPLETED_FUNDS_RECEIVED',
};

const DISPLAYED_STATUSES = {
  INVITED: 'INVITED',
  PENDING: 'PENDING',
  NEEDS_SIGNING: 'NEEDS SIGNING',
  COMMITTED: 'COMMITTED',
  NEEDS_FUNDING: 'NEEDS FUNDING',
  FUNDING_SENT: 'FUNDING SENT',
  FUNDS_IN_TRANSIT_MANUAL: 'FUNDS IN TRANSIT - MANUAL',
  COMPLETED: 'COMPLETED',
  COMPLETED_FUNDS_RECEIVED: 'COMPLETED - FUNDS RECEIVED',
  CLOSE_PENDING: 'CLOSE PENDING',
  CLOSED: 'CLOSED',
  CANCELLED: 'CANCELLED',
  TRANSFERRED: 'TRANSFERRED',
  REFUNDED: 'REFUNDED',
  CUSTOM: 'CUSTOM',
};

const {
  INVITED,
  PENDING,
  NEEDS_SIGNING,
  COMMITTED,
  NEEDS_FUNDING,
  FUNDING_SENT,
  FUNDS_IN_TRANSIT_MANUAL,
  COMPLETED,
  COMPLETED_FUNDS_RECEIVED,
  CLOSE_PENDING,
  CLOSED,
  CANCELLED,
  TRANSFERRED,
  CUSTOM,
  REFUNDED,
} = DISPLAYED_STATUSES;

export const SubscriptionStatusText = (subscription: any): string => {
  if ([DB_STATUSES.INVITED].includes(subscription.status)) {
    return subscription.status;
  } else if (subscription.status === DB_STATUSES.COMPLETED) {
    if (subscription?.transactions) {
      const sentTransactions = subscription.transactions.filter(
        (transaction) => transaction.direction === 'DEBIT'
      );
      if (
        sentTransactions.filter((transaction) => transaction.bankAccountTransactionId).length ===
          sentTransactions.length &&
        sentTransactions.length > 0
      ) {
        if (!subscription?.close) {
          return COMPLETED_FUNDS_RECEIVED;
        }

        return !subscription.close?.fundManagerSigned ? CLOSE_PENDING : CLOSED;
      }
    }
    if (!subscription?.close) {
      return COMPLETED;
    }

    return !subscription.close?.fundManagerSigned ? CLOSE_PENDING : CLOSED;
  } else if (subscription.status === DB_STATUSES.PENDING) {
    return NEEDS_SIGNING;
  } else if (subscription.status === DB_STATUSES.COMMITTED) {
    if (!subscription?.transactionIds) {
      return NEEDS_FUNDING;
    } else {
      return FUNDING_SENT;
    }
  } else if (subscription.status === DB_STATUSES.COMPLETED_FUNDS_RECEIVED) {
    return COMPLETED_FUNDS_RECEIVED;
  } else {
    return subscription.status.replace(/[_]/gi, ' ').replace(/[-]/gi, ' - ');
  }
};

const subscriptionStatusColors: {
  [key: string]:
    | 'teal'
    | 'red'
    | 'orange'
    | 'yellow'
    | 'olive'
    | 'green'
    | 'blue'
    | 'violet'
    | 'purple'
    | 'pink'
    | 'brown'
    | 'grey'
    | 'black'
    | undefined;
} = {
  [INVITED]: 'red',
  [PENDING]: 'orange',
  [NEEDS_SIGNING]: 'orange',
  [COMMITTED]: 'yellow',
  [NEEDS_FUNDING]: 'yellow',
  [FUNDING_SENT]: 'olive',
  [FUNDS_IN_TRANSIT_MANUAL]: 'olive',
  [COMPLETED]: 'green',
  [COMPLETED_FUNDS_RECEIVED]: 'green',
  [CLOSE_PENDING]: 'olive',
  [CLOSED]: 'grey',
  [CANCELLED]: 'grey',
  [TRANSFERRED]: 'grey',
  [CUSTOM]: 'yellow',
};

export const SubscriptionStatusColors = (status: string) => subscriptionStatusColors[status];

const DISPLAY_ORDER = Object.values(DISPLAYED_STATUSES);
export const getStatusOrder = (displayedStatus) => {
  return DISPLAY_ORDER.indexOf(displayedStatus);
};

const includesQPOption = (purchaserStatus: number[]): boolean => {
  const validQPOptions = [0, 1, 2, 3, 4, 5, 6, 7];
  for (let i = 0; i < purchaserStatus.length; i++) {
    if (validQPOptions.includes(purchaserStatus[i])) {
      return true;
    }
  }

  return false;
};

export const isQPSubscriberProfile = (profile): boolean => {
  const purchaserStatus = profile?.purchaserStatus;
  if (!purchaserStatus) {
    return false;
  }

  if (Array.isArray(purchaserStatus)) {
    if (includesQPOption(purchaserStatus)) {
      return true;
    }
  }

  return false;
};
