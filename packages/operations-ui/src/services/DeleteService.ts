import { SnackbarUtils, doChange } from '@packages/wired-gbt-ui';

async function DeleteObject(objectType: string, id: string) {
  doChange(`${objectType}/${id}`, { method: 'DELETE' });
}

export async function SoftDeleteById(objectId, objectType) {
  try {
    DeleteObject(objectType, objectId);
    if (objectType !== '') {
      SnackbarUtils.success(`Deleted: ${objectType}`);
    }
  } catch (error) {
    SnackbarUtils.error(`Delete : ${error.message}`);
  }
}
