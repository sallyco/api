module.exports = {
    roots: ["<rootDir>/src"],
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
    moduleNameMapper: {
      '\\.(scss|sass|css)$': 'identity-obj-proxy',
    },
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.ts[x]?$",
    setupFilesAfterEnv: ["./jest.setup.ts"],
    testEnvironment: 'jsdom'
  };
  