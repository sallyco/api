@echo off

set VERSION=1.0.12

echo.
echo -----------------------
echo Tilt Driver for Windows
echo -----------------------

cd /D "%~dp0"

net session >nul 2>&1
if %errorLevel% neq 0 (
    echo Tilt Driver requires Admin Elevation
    powershell -c "Start-Process -Wait %~f0 -Verb RunAs"
    goto finish
    exit /b
)

WHERE git >nul 2>&1
IF %ERRORLEVEL% == 0 (
   git fetch --tags
   for /f %%a in ('git describe --abbrev^=0 origin/master') do (
       IF %%a NEQ %VERSION% (
          echo Current Version is %VERSION%, Latest Version is %%a.
          CHOICE /N /C:YN /M "Would you like to Update? (Y/N)"%1
          IF ERRORLEVEL == 1 (
             git reset --hard HEAD
             git checkout %%a
             start "" "%~f0"
             exit /b
          )
          IF ERRORLEVEL == 2 GOTO setupchoco
       )
   )
)

:setupchoco

WHERE choco >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
    echo chocolatey not found, installing now
    powershell -c "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
    PATH=PATH;C:\ProgramData\chocolatey\bin
    WHERE choco
    IF %ERRORLEVEL% NEQ 0 (
        echo Something went wrong with the chocolatey install, exiting
        exit /b
    )
)

set len=8
set pkg[0].CmdName=git
set pkg[0].PkgName=git

set pkg[1].CmdName=docker
set pkg[1].PkgName=docker-desktop

set pkg[2].CmdName=aws
set pkg[2].PkgName=awscli

set pkg[3].CmdName=npm
set pkg[3].PkgName=nodejs-lts

set pkg[4].CmdName=aws-iam-authenticator
set pkg[4].PkgName=aws-iam-authenticator

set pkg[5].CmdName=tilt
set pkg[5].InstallerCmd=powershell -ExecutionPolicy Bypass -c "installer_scripts/tilt-install.ps1"

set pkg[6].CmdName=kustomize
set pkg[6].InstallerCmd=powershell -ExecutionPolicy Bypass -c "installer_scripts/kustomize-install.ps1"

set pkg[7].CmdName=kubectl
set pkg[7].PkgName=kubernetes-cli

set i=0

echo Installing Packages
goto :installpkgs

:installpkgs

Set-ExecutionPolicy unrestricted -force

if "%i%" equ "%len%" goto :configure
set cur.CmdName= 
set cur.PkgName=
set cur.InstallerCmd=

for /f "usebackq delims==. tokens=2-3" %%a in (`set pkg[%i%]`) do (
   set cur.%%a=%%b
)

WHERE "%cur.CmdName%" >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
    IF "%cur.PkgName%" neq "" (
        choco install -y "%cur.PkgName%"
        call RefreshEnv.cmd
    ) ELSE (
        echo Installing %cur.CmdName%
        setlocal enabledelayedexpansion
        echo !pkg[%i%].InstallerCmd!
        cmd /C !pkg[%i%].InstallerCmd!
        call RefreshEnv.cmd
    )
) ELSE (
    echo %cur.CmdName% already installed
)

set /a i = %i%+1 
goto installpkgs

:configure

if not exist "%USERPROFILE%\.aws\credentials" (
    echo Setting Up your AWS Account, please enter your AWS Account information:
    aws configure
)
aws eks update-kubeconfig --name dae --region us-east-2

goto finish

:finish
echo Tilt-Driver Install / Configure Completed
pause
