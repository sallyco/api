---
title: Getting Started
lang: en-US
description: Using Tilt to set up a development environment
---
# Introduction
This guide will walk you through setting up a development environment using [Tilt][1].

To begin, clone the tilt-driver repository to your development machine

``` bash
$ git clone git@git.gbt.sh:gbt/devops/tilt-driver.git
```
This process is configured to automatically launch the entire application, partially in the local environment and partially in a cloud Kubernetes cluster.  Relative path reference is made to the other components, so you will want to clone those components to the same directory.

``` bash
$ git clone git@git.gbt.sh:gbt/frontend/1-minute-deal-ui.git
$ git clone git@git.gbt.sh:gbt/microservices/omnibus-paas.git
$ git clone git@git.gbt.sh:gbt/microservices/camunda-development.git
```

## Windows Installation

* *Note - Windows requires administrative privileges*
* *Note - Windows installation has only been testing using Powershell*
* *Note - You will need to enable script execution ([more information][2])*

Change to the `tilt-driver` directory and run the install script:

``` powershell
PS> ./setup.bat

-----------------------
Tilt Driver for Windows
-----------------------
Installing Packages
...

```

You will see a substantial amount of output as the script install the required tools for development.


## macOS / Linux Installation
Run the install script `./setup.sh`

[1]:	https://tilt.dev/
[2]:	https:/go.microsoft.com/fwlink/?LinkID=135170
