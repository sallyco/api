Invoke-WebRequest "https://github.com/tilt-dev/tilt/releases/download/v0.20.0/tilt.0.20.0.windows.x86_64.zip" -OutFile "tilt.zip"
Expand-Archive "tilt.zip" -Force -DestinationPath "tilt"
New-Item -ItemType Directory -Force -Path "$home\bin"
Move-Item -Force -Path "tilt\tilt.exe" -Destination "$home\bin\tilt.exe"
Remove-Item -Force -Recurse -Path "tilt"
Remove-Item -Force "tilt.zip"
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$home\bin\", "Machine")