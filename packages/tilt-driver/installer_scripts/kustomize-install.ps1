Invoke-WebRequest "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.5.4/kustomize_v3.5.4_windows_amd64.tar.gz" -OutFile "kustomize.tar.gz"
Function DeGZip-File{
    Param(
        $infile,
        $outfile = ($infile -replace '\.gz$','')
        )
    $input = New-Object System.IO.FileStream $inFile, ([IO.FileMode]::Open), ([IO.FileAccess]::Read), ([IO.FileShare]::Read)
    $output = New-Object System.IO.FileStream $outFile, ([IO.FileMode]::Create), ([IO.FileAccess]::Write), ([IO.FileShare]::None)
    $gzipStream = New-Object System.IO.Compression.GzipStream $input, ([IO.Compression.CompressionMode]::Decompress)
    $buffer = New-Object byte[](1024)
    while($true){
        $read = $gzipstream.Read($buffer, 0, 1024)
        if ($read -le 0){break}
        $output.Write($buffer, 0, $read)
        }
    $gzipStream.Close()
    $output.Close()
    $input.Close()
}
DeGZip-File "kustomize.tar.gz" "temp.tar"

if (-not (Get-Command Expand-7Zip -ErrorAction Ignore)) {
Install-Package -Scope CurrentUser -Force 7Zip4PowerShell > $null
}
Expand-7Zip temp.tar kustomize

New-Item -ItemType Directory -Force -Path "$home\bin"
Move-Item -Force -Path "kustomize\kustomize.exe" -Destination "$home\bin\kustomize.exe"
Remove-Item -Force -Recurse -Path "kustomize"
Remove-Item -Force "kustomize.tar.gz"
Remove-Item -Force "temp.tar"
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$home\bin\", "Machine")