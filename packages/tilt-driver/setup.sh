#!/bin/bash

VERSION="1.0.12"


GREEN="\033[0;32m"
RED="\033[0;31m"
NC="\033[0m"

echo -e "${RED}_________  ___  ___   _________        ________  ________  ___  ___      ___ _______   ________     ";
echo -e "|\___   ___\\  \|\  \ |\___   ___\     |\   ___ \|\   __  \|\  \|\  \    /  /|\  ___ \ |\   __  \    ";
echo -e "\|___ \  \_\ \  \ \  \\|___ \  \_|     \ \  \_|\ \ \  \|\  \ \  \ \  \  /  / | \   __/|\ \  \|\  \   ";
echo -e "     \ \  \ \ \  \ \  \    \ \  \       \ \  \ \\ \ \   _  _\ \  \ \  \/  / / \ \  \_|/_\ \   _  _\  ";
echo -e "      \ \  \ \ \  \ \  \____\ \  \       \ \  \_\\ \ \  \\  \\ \  \ \    / /   \ \  \_|\ \ \  \\  \| ";
echo -e "       \ \__\ \ \__\ \_______\ \__\       \ \_______\ \__\\ _\\ \__\ \__/ /     \ \_______\ \__\\ _\ ";
echo -e "        \|__|  \|__|\|_______|\|__|        \|_______|\|__|\|__|\|__|\|__|/       \|_______|\|__|\|__|";
echo -e "                                                                                                     ";
echo -e "                                                                                                     ";
echo -e "                                                                                                     ";
echo -e "----------------------------------------------------------------------------------------------------\n\n${NC}";

if [[ "$OSTYPE" == "linux-gnu" ]]; then
  OS="linux";
elif [[ "$OSTYPE" == "darwin"* ]]; then
  OS="macos";
fi;

function version() { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

SKIP_UPDATE=false
if [[ $1 == "skip-update" ]]; then
  SKIP_UPDATE=true
fi

# CHECK UPDATE
function check_updates() {
  if [[ $(command -v git) && $SKIP_UPDATE == "false" ]]; then
    echo "Checking for updates..."
    git fetch --tags
    if [[ -z $(git describe --abbrev=0 origin/master) ]]; then
      echo "Unable to check for newer versions: No Tags Found";
    else
      LATEST_TAG=$(git describe --tags "$(git describe --abbrev=0 origin/master)");
      if [ "$(version "$VERSION")" -ge "$(version "$LATEST_TAG")" ]; then
          echo "Version is up to date";
          return
      else
          echo "Version is not up to date. Current version is $VERSION, latest is $LATEST_TAG.";
          read -p "Would you like to update? (y/n): " -r -n1 UPDATE_CONFIRM
          if [ "$UPDATE_CONFIRM" = "y" ]; then
            git checkout "$LATEST_TAG"
            ./"$0" skip-update
            exit;
          fi
      fi
    fi
    exit;
  fi
}

# SETUP PACKAGES
function setup_packages() {
  if [[ $OS == "macos" ]]; then
    BREW=$(command -v brew)
    if [[ -z "$BREW" ]]; then
      echo "Installing brew (required)";
      # shellcheck disable=SC2091
      $(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh);
    fi
    if [[ \
          $(command -v tilt) && \
          $(command -v aws-iam-authenticator) && \
          $(command -v aws) && \
          $(command -v kustomize) && \
          $(command -v kubectl) && \
          $(command -v git) && \
          $(command -v node) && \
          $(command -v mkcert)
    ]]; then
      echo "All required tools are available";
    else
      echo "Installing required system tools"
      set -x;
      brew tap windmilleng/tap;
      brew install windmilleng/tap/tilt aws-iam-authenticator awscli kustomize kubectl git node@14 mkcert;
    fi
  fi
  if [[ $OS == "linux" ]]; then
    if [[ \
          $(command -v tilt) && \
          $(command -v aws-iam-authenticator) && \
          $(command -v aws) && \
          $(command -v kustomize) && \
          $(command -v kubectl) && \
          $(command -v git) && \
          $(command -v node) \
          $(command -v mkcert) \
    ]]; then
      echo "All required tools are available";
    else
      echo "Installing required system tools"
      set -x;

      sudo apt update;

      sudo apt install -y apt-transport-https ca-certificates curl git make build-essential;
      sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
      echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
      sudo apt update;

      echo "Installing Kubectl"
      sudo apt-get install -y kubectl

      # Installing Tilt
      echo "Installing Tilt"
      curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash;


      # Install AWS IAM Authenticator
      echo "Installing AWS IAM Authenticator"
      curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator;
      sudo chmod +x ./aws-iam-authenticator
      sudo mv ./aws-iam-authenticator /usr/local/bin

      # Install AWS CLI
      echo "Install AWS CLI"
      curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
      unzip awscliv2.zip
      sudo ./aws/install

      # Install Kustomize
      curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
      sudo mv ./kustomize /usr/local/bin

      # Install Node14
      curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
      sudo apt-get install -y nodejs
    fi
  fi
}

# Set Up the Credentials needed
function configure_credentials() {
  if test -f "$HOME/.aws/credentials"; then
    echo "AWS already configured";
  else
    echo "Now configuring your AWS Credentials";
    aws configure;
  fi

  echo "Configuring access to Kubernetes"
  KUBE_USER="$(aws iam get-user --output text --query 'User.UserName' | cut -d@ -f1)";
  FILE=$( < ./kube-setup/config sed "s/\[NAMESPACE\]/${KUBE_USER}/" );
  mkdir -p ~/.kube;
  echo "$FILE" > "$HOME/.kube/config";
  if kubectl cluster-info | grep -q 'is running'; then
    kube-setup/kube-setup.sh
    echo "Connected to cluster successfully";
  fi
}

check_updates
setup_packages
configure_credentials

echo -e "\n\n${GREEN}Tilt Driver Installed${NC}";
echo -e "Run \`tilt up\` to get started";
