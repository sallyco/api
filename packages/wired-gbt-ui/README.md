# _Wired_ UI Components

Connected component library for use across all applications.

Components should be fully realized, documented and polished.  Importantly, these components are wired, i.e. they should take a url instead of data.  They **do** reach out to read or write data from APIs.  If you want inert components use the [UI Components](https://gitlab.gbt.sh/packages/gbt-ui)

#### Development Tip

Generally, there will be a lot of overlap with names from the regular UI components library.  A typical pattern is to wrap the basic component with a function to `GET` data and perhaps one to `PUT` or `POST` changes.

For example.  In `gbt-ui` we have `<InfoCard />` that takes an objects JSON and puts it in a nice table.  Here we also have `<InfoCard />` that imports the basic version and enriches it with the ability to make an API call itself for data.
