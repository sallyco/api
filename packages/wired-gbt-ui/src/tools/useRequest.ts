import useSwr from 'swr';
import { fetcher as fch } from './fetchers';
import { SnackbarUtils } from './SnackbarUtils';

const baseUrl = process.env.NEXT_PUBLIC_API_URL || process.env.API_URL;

export const useRequest = (
  path: string,
  { silent = false, fetcher = undefined, swrOptions = {} } = {}
) => {
  const url = path.startsWith('http') ? path : baseUrl + path;

  const swrActions = useSwr(url, fetcher ?? fch, {
    shouldRetryOnError: false,
    ...swrOptions,
  });

  //  const toastId = React.useRef(null);
  if (
    swrActions.error?.info?.error?.message &&
    !silent &&
    !swrActions.error.info.error.message.includes('jwt expired')
  ) {
    SnackbarUtils.error(swrActions.error.info.error.message);
  }

  return swrActions;
};
