import { SnackbarUtils } from './SnackbarUtils';

const baseUrl = process.env.NEXT_PUBLIC_API_URL || process.env.API_URL;

export const doChange = async (
  path: string,
  method: any,
  body?: any,
  mutate?: () => void | undefined,
  overrideTenant: string | false = false,
  customHeaders?: any,
  isFileUpload: boolean = false
) => {
  const url = path.startsWith('http') ? path : baseUrl + path;

  const response = await fetch(url, {
    method: method,
    headers: {
      ...(isFileUpload
        ? {}
        : {
            'Content-Type': 'application/json',
          }),
      ...(overrideTenant
        ? {
            'x-tenant-override': overrideTenant,
          }
        : {}),
      ...(customHeaders ?? {}),
    },
    ...(body ? (isFileUpload ? { body } : { body: JSON.stringify(body) }) : {}),
  });

  let text;
  if (response.ok) {
    text = response.status !== 204 ? await response.json() : undefined;
    SnackbarUtils.success('Success');
    if (mutate && typeof mutate === 'function') {
      mutate();
    }
  } else {
    SnackbarUtils.error('There was an error');
  }

  //if (error?.info?.error?.message) {
  //  enqueueSnackbar(error.info.error.message, {
  //    preventDuplicate: true,
  //    variant: "error"
  //  });
  //}
  return text;
};
