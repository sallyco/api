export function metadataFetcher(req: RequestInfo, init?: RequestInit) {
  return fetch(req, init)
    .then((res) => res.json())
    .then((data) => {
      //add a title
      data.title = data.$ref?.replace('#/definitions/', '');
      //handle entries
      for (let def in data.definitions) {
        //handle weird descriptions
        if (data?.definitions[def]?.description) {
          const tsRegex = /\ ?\(tsType.*}\)/g;
          data.definitions[def].description = data.definitions[def].description.replace(
            tsRegex,
            ''
          );
        }
        //handle nullable
        for (let prop in data.definitions[def].properties) {
          let entry = data.definitions[def].properties[prop];
          const oType = data.definitions[def].properties[prop].type;
          if (entry.nullable && oType && typeof oType.valueOf() !== 'object') {
            data.definitions[def].properties[prop].type = [oType.valueOf(), 'null'];
          }
        }
      }
      //add a top-level description
      data.description = data.definitions[data.title].description;
      return data;
    });
}

export const fetcher = async (url: string) => {
  const res = await fetch(url, { cache: 'no-store' });
  if (!res.ok) {
    const error: any = new Error('An error occurred while fetching the data.');
    // Attach extra info to the error object.
    error.info = await res.json();
    error.status = res.status;
    throw error;
  }

  return res.json();
};
