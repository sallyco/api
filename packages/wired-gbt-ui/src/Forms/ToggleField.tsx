import * as React from 'react';

import IconButton from '@mui/material/IconButton';
import { doChange } from '../tools/doChange';

export interface ToggleFieldProps {
  objectKey: string;
  objectId: string;
  fieldName: string;
  newValue: any;
  mutate?: any;
  icon: JSX.Element;
}

const ToggleField: React.FC<ToggleFieldProps> = ({
  objectKey,
  objectId,
  fieldName,
  newValue,
  mutate,
  icon,
}) => {
  const handleClick = () => {
    doChange(`/${objectKey}/${objectId}`, 'PUT', { [fieldName]: newValue } as any, mutate);
  };

  return <>
    <IconButton
      color="secondary"
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        handleClick();
      }}
      size="large">
      {icon}
    </IconButton>
  </>;
};

export default ToggleField;
