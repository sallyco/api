/* eslint-disable react/no-children-prop */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';
import { styled } from '@mui/material/styles';
import { useRequest, doChange, ObjectProp } from '../tools';
import { fetcher, metadataFetcher } from '../tools/fetchers';
import { TimesIcon, PlusIcon } from 'react-line-awesome';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';

import { ContentLoader, Form, FullScreenDialog } from '@packages/gbt-ui';

import { Container, Button } from '@mui/material';

const StyledButton = styled(Button)(({ theme }) => ({
  float: 'right',
  marginLeft: theme.spacing(1),
  marginTop: theme.spacing(2),
  marginBottom: theme.spacing(8),
  color: theme.palette.common.white,
}));

export interface CreateModalProps {
  op: ObjectProp;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  mutate?: () => void;
}


const CreateModal: React.FC<CreateModalProps> = ({ op, open, setOpen, mutate }) => {
  const { data } = useRequest(!open ? '' : `/metadata/${op?.oType}/create/describe`, {
    fetcher: metadataFetcher as any,
  });
  const { data: uiSchema } = useRequest(!open ? '' : `/metadata/${op?.oType}/create/ui`, {
    fetcher: fetcher as any,
  });
  const [submitting, setSubmitting] = React.useState(false);
  const [formData, setFormData] = React.useState(undefined);

  const onSubmit = async ({ formData }: { formData: any }) => {
    setSubmitting(true);
    await doChange(op.route, 'POST', formData);
    if (mutate) {
      mutate();
    }
    setFormData(undefined);
    setSubmitting(false);
    setOpen(false);
  };

  const onClose = () => {
    setFormData(undefined);
    setOpen(false);
  };

  return (
    <FullScreenDialog
      title={data?.title ?? 'Loading'}
      open={open}
      setOpen={setOpen}
      loading={submitting}
      setLoading={setSubmitting}
      closeAction={() => setFormData(undefined)}
    >
      <Container>
        {!data || !uiSchema ? (
          <ContentLoader />
        ) : (
          <Form
            schema={{ ...data, title: '' }}
            uiSchema={{
              'ui:description': (
                <ReactMarkdown children={data.description} remarkPlugins={[remarkGfm]} />
              ),
              'ui:order': ['id', 'name', 'title', 'description', '*'],
              ...{ uiSchema },
            }}
            onSubmit={onSubmit}
            showErrorList={false}
            omitExtraData={true}
            formData={formData}
            onChange={(e) => setFormData(e.formData)}
          >
            <StyledButton
              variant="contained"
              type="submit"
              startIcon={<PlusIcon />}
              sx={{
                backgroundColor: (theme) => theme.palette.success.main,
              }}
            >
              Create
            </StyledButton>
            <StyledButton
              variant="contained"
              onClick={onClose}
              startIcon={<TimesIcon />}
              sx={{
                backgroundColor: (theme) => theme.palette.error.main,
              }}
            >
              Cancel
            </StyledButton>
          </Form>
        )}
      </Container>
    </FullScreenDialog>
  );
};

export default CreateModal;
