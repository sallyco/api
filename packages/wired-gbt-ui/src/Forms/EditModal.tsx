/* eslint-disable react/no-children-prop */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';
import { styled } from '@mui/material/styles';
import { useRequest, doChange, ObjectProp } from '../tools';
import { fetcher, metadataFetcher } from '../tools/fetchers';
import { TimesIcon, ArrowUpIcon } from 'react-line-awesome';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';

import { ContentLoader, Form, FullScreenDialog } from '@packages/gbt-ui';

import { Button, Container } from '@mui/material';
import _ from 'lodash';

const StyledButton = styled(Button)(({ theme }) => ({
  float: 'right',
  marginLeft: theme.spacing(1),
  marginTop: theme.spacing(2),
  marginBottom: theme.spacing(8),
  color: theme.palette.common.white,
}));

export interface EditModalProps {
  op: ObjectProp;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  id: string | undefined;
  mutate?: () => void;
  encodedFilter?: string | undefined;
}

const EditItemModal: React.FC<EditModalProps> = ({
  op,
  open,
  setOpen,
  id,
  mutate,
  encodedFilter = undefined,
}) => {
  const { data } = useRequest(!open ? '' : `/metadata/${op?.oType}/update/describe`, {
    fetcher: metadataFetcher as any,
  });
  const { data: uiSchema } = useRequest(!open ? '' : `/metadata/${op?.oType}/create/ui`, {
    fetcher: fetcher as any,
  });
  const { data: fd } = useRequest(
    open && id ? `${op.route}/${id}` + (encodedFilter ? `?filter=${encodedFilter}` : '') : '',
    { swrOptions: { revalidateOnFocus: false } }
  );
  const [submitting, setSubmitting] = React.useState(false);

  const [formData, setFormData] = React.useState(undefined);
  const [originalFormData, setOriginalFormData] = React.useState(undefined);
  React.useEffect(() => {
    _.forEach(fd, function (value, key) {
      if (Array.isArray(value) && value.length > 0 && typeof value[0] === 'object') {
        _.update(fd, key, (value) => _.map(value, 'id'));
      }
    });
    setOriginalFormData(fd);
    setFormData(fd);
  }, [fd]);

  const onSubmit = async ({ formData }: { formData: any }) => {
    setSubmitting(true);
    await doChange(`${op.route}/${id}`, 'PUT', formData);
    if (mutate) {
      mutate();
    }
    setSubmitting(false);
    setOpen(false);
  };

  const onClose = () => {
    setFormData(originalFormData);
    setOpen(false);
  };

  return (
    <FullScreenDialog
      title={data?.title ?? 'Loading'}
      open={open}
      setOpen={setOpen}
      loading={submitting}
      setLoading={setSubmitting}
      closeAction={() => setFormData(originalFormData)}
    >
      <Container>
        {!data || !uiSchema ? (
          <ContentLoader />
        ) : (
          <Form
            schema={{ ...data, title: '' }}
            uiSchema={{
              'ui:description': (
                <ReactMarkdown children={data.description} remarkPlugins={[remarkGfm]} />
              ),
              'ui:order': ['id', 'name', 'title', 'description', '*'],
              ...{ uiSchema },
            }}
            onSubmit={onSubmit}
            showErrorList={false}
            omitExtraData={true}
            formData={formData}
            onChange={(e) => setFormData(e.formData)}
          >
            <StyledButton
              variant="contained"
              type="submit"
              startIcon={<ArrowUpIcon />}
              sx={{
                backgroundColor: (theme) => theme.palette.success.main,
              }}
            >
              Update
            </StyledButton>
            <StyledButton
              variant="contained"
              onClick={onClose}
              startIcon={<TimesIcon />}
              sx={{
                backgroundColor: (theme) => theme.palette.error.main,
              }}
            >
              Cancel
            </StyledButton>
          </Form>
        )}
      </Container>
    </FullScreenDialog>
  );
};
export default EditItemModal;
