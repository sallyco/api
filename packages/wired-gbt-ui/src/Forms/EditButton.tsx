import * as React from 'react';
import { ObjectProp } from '../tools';

import Button from '@mui/material/Button';
import { PencilIcon } from 'react-line-awesome';
import EditModal from './EditModal';

export interface EditButtonProps {
  op: ObjectProp;
  id: string | undefined;
  mutate?: () => void;
  encodedFilter?: string | undefined;
  disabled?: boolean;
}

const EditItem: React.FC<EditButtonProps> = ({
  op,
  id,
  mutate,
  encodedFilter = undefined,
  disabled = false,
  ...rest
}) => {
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <EditModal
        op={op}
        open={open}
        setOpen={setOpen}
        id={id}
        mutate={mutate}
        encodedFilter={encodedFilter}
      />
      <Button
        size="small"
        onClick={() => {
          setOpen(true);
        }}
        startIcon={<PencilIcon />}
        disabled={disabled}
        {...rest}
      >
        Edit
      </Button>
    </React.Fragment>
  );
};

export default EditItem;
