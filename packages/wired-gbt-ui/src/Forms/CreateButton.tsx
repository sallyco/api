import * as React from 'react';
import { ObjectProp } from '../tools';
import Button from '@mui/material/Button';
import { PlusIcon } from 'react-line-awesome';
import CreateModal from './CreateModal';

export interface CreateButtonProps {
  op: ObjectProp;
  mutate?: () => void;
  disabled?: boolean;
}

const CreateButton: React.FC<CreateButtonProps> = ({ op, mutate, disabled = false, ...rest }) => {
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <CreateModal op={op} mutate={mutate} open={open} setOpen={setOpen} />
      <Button
        size="small"
        startIcon={<PlusIcon />}
        disabled={disabled}
        onClick={() => {
          setOpen(true);
        }}
        {...rest}
      >
        Create New
      </Button>
    </React.Fragment>
  );
};

export default CreateButton;
