import {TokenService} from '@loopback/authentication';
import {BindingKey} from '@loopback/core';
import {PasswordHasher} from './authorization/services/hash.password.bcryptjs';
import {FileUploadHandler} from './types';
import {MinIOService, DocumentsService} from './services';
import {Onfido} from '@onfido/api';

export namespace TokenServiceConstants {
  export const TOKEN_SECRET_VALUE = 'xxxxx';
  export const TOKEN_EXPIRES_IN_VALUE = '600';
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expires.in.seconds',
  );
  export const TOKEN_SERVICE = BindingKey.create<TokenService>(
    'services.authentication.jwt.tokenservice',
  );
}

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER =
    BindingKey.create<PasswordHasher>('services.hasher');
  export const ROUNDS = BindingKey.create<number>('services.hasher.round');
}

export const FILE_UPLOAD_SERVICE = BindingKey.create<FileUploadHandler>(
  'services.FileUpload',
);

export const STORAGE_DIRECTORY = BindingKey.create<string>('storage.directory');

export namespace MinIOServiceBindings {
  export const MINIO_SERVICE =
    BindingKey.create<MinIOService>('services.MinIO');
}
export namespace DocumentsServiceBindings {
  export const DOCUMENTS_SERVICE =
    BindingKey.create<DocumentsService>('services.Documents');
}
export namespace PackageBindings {
  export const ONFIDIO_PACKAGE = BindingKey.create<Onfido>('onfido.Package');
}

export namespace PackageFactory {
  export const ONFIDO_INSTANCE = () => {
    return new Onfido({
      apiToken:
        process.env.ONFIDO_TOKEN ??
        'api_sandbox.F5FNEEMeIrX.uLDCsO5rOoxWIhYUPB7FJfZWGXPlcZa0',
    });
  };
}
