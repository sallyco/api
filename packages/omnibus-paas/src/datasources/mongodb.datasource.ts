import {
  inject,
  lifeCycleObserver,
  LifeCycleObserver,
  ValueOrPromise,
} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as fs from 'fs';
import debugFactory from 'debug';

const debug = debugFactory('omnibus-paas:mongodb:datasource');

interface MongoConfigInterface {
  name: string;
  connector: string;
  url: string;
  useNewUrlParser: boolean;
  lazyConnect: boolean;
  ssl: boolean;
  sslValidate: boolean;
  sslCA: Array<Buffer>;
  useUnifiedTopology: boolean;
  connectionTimeout: number;
  allowExtendedOperators: boolean;
  disableMigration?: boolean;
}

const config: MongoConfigInterface = {
  name: 'mongodb',
  connector: 'mongodb',
  url: process.env.MONGODB_URL ?? '',
  useNewUrlParser: true,
  lazyConnect: true,
  ssl: false,
  sslValidate: true,
  sslCA: [],
  useUnifiedTopology: true,
  connectionTimeout: 10000,
  allowExtendedOperators: true,
  disableMigration: true,
};

if (process.env.MONGODB_SSL === 'true') {
  config.ssl = true;
  const caFilePath = process.env.MONGODB_SSL_CA_PATH ?? '';
  if (caFilePath !== '') {
    try {
      config.sslCA = [fs.readFileSync(caFilePath)];
    } catch (e) {
      debug('error', e);
    }
  }
}

@lifeCycleObserver('datasource')
export class MongodbDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'mongodb';

  constructor(
    @inject('datasources.config.mongodb', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }

  /**
   * Start the datasource when application is started
   */
  start(): ValueOrPromise<void> {
    // Add your logic here to be invoked when the application is started
  }

  /**
   * Disconnect the datasource when application is stopped. This allows the
   * application to be shut down gracefully.
   */
  stop(): ValueOrPromise<void> {
    return super.disconnect();
  }
}
