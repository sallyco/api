import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'ParallelMarkets',
  connector: 'openapi',
  url:
    process.env.PARALLEL_MARKETS_URL ??
    'https://demo-api.parallelmarkets.com/v1',
  server:
    process.env.PARALLEL_MARKETS_URL ??
    'https://demo-api.parallelmarkets.com/v1',
  // Required because loopback swagger-client isn't fully OA3 compatible yet
  // and strips out base paths
  basePath: '/v1',
  spec: './external_service_specs/parallelmarkets.json',
  validate: false,
  positional: false,
  // Convert to Normalized form
  transformResponse: res => {
    if (res.status < 400) {
      return res.body;
    }
    const err = new Error(`${res.status} ${res.statusText}`);
    err.message = res;
    throw err;
  },
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class ParallelMarketsDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'ParallelMarkets';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.ParallelMarkets', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
