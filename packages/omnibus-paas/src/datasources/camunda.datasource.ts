import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'camunda',
  connector: 'openapi',
  spec: './external_service_specs/camunda.json',
  validate: true,
  positional: true,
  url:
    process.env.CAMUNDA_BASE_URL ??
    'http://camunda-bpm-platform:8080/engine-rest',
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class CamundaDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'camunda';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.camunda', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
