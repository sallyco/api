import {inject} from '@loopback/core';
import {juggler, AnyObject} from '@loopback/repository';
import path from 'path';

const config = {
  name: 'healthstatus',
  connector: 'loopback-connector-grpc',
  url:
    process.env.ENVIRONMENT === 'local'
      ? '127.0.0.1:50000'
      : 'healthstatus:9090',
  spec: 'protos/healthstatus.proto',
};

export class HealthStatusDataSource extends juggler.DataSource {
  static readonly dataSourceName = config.name;
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.healthstatus', {optional: true})
    dsConfig: AnyObject = config,
  ) {
    super({
      ...dsConfig,
      spec: HealthStatusDataSource.getProtoFile(),
    });
  }

  private static getProtoFile() {
    return path.resolve(__dirname, '../../protos/healthstatus.proto');
  }
}
