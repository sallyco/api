import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const LEGAL_INC_API_URI =
  process.env.LEGAL_INC_API_URI ??
  'https://orders-uat.objectlegal.com:443/api/v1/';

const config = {
  name: 'LegalInc',
  connector: 'rest',
  baseURL: LEGAL_INC_API_URI,
  crud: true,
  operations: [
    {
      template: {
        method: 'POST',
        url: LEGAL_INC_API_URI + 'oauth/access_token',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
        },
        body: {
          grant_type: 'password',
          client_id: '{client_id:string}',
          client_secret: '{client_secret:string}',
          username: '{username:string}',
          password: '{password:string}',
        },
      },
      functions: {
        getToken: ['client_id', 'client_secret', 'username', 'password'],
      },
    },
    {
      template: {
        method: 'GET',
        url: LEGAL_INC_API_URI + 'orders',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
      },
      functions: {
        getOrders: ['access_token'],
      },
    },
    {
      template: {
        method: 'GET',
        url: LEGAL_INC_API_URI + 'orders/{order_id:string}',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
      },
      functions: {
        getOrder: ['access_token', 'order_id'],
      },
    },
    {
      template: {
        method: 'POST',
        url: LEGAL_INC_API_URI + 'orders',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
        body: '{order_data}',
      },
      functions: {
        createOrder: ['access_token', 'order_data'],
      },
    },
    {
      template: {
        method: 'GET',
        url: LEGAL_INC_API_URI + 'payments',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
      },
      functions: {
        getPayments: ['access_token'],
      },
    },
    {
      template: {
        method: 'POST',
        url: LEGAL_INC_API_URI + 'payments',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
        body: '{payment_data}',
      },
      functions: {
        createPayment: ['access_token', 'payment_data'],
      },
    },
    {
      template: {
        method: 'GET',
        url: LEGAL_INC_API_URI + 'companies/{company_id:string}',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: 'Bearer {access_token:string}',
        },
      },
      functions: {
        getCompany: ['access_token', 'company_id'],
      },
    },
  ],
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class LegalIncDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'LegalInc';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.LegalInc', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
