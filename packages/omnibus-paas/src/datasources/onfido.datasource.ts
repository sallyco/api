import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'onfido',
  connector: 'openapi',
  spec: './external_service_specs/onfido.json',
  url: 'https://api.eu.onfido.com/v3.1',
  validate: false,
  positional: false,
  authorizations: {
    oauth2: {
      token: {
        access_token: process.env.ONFIDO_TOKEN
          ? `token=${process.env.ONFIDO_TOKEN}`
          : 'token=api_sandbox.F5FNEEMeIrX.uLDCsO5rOoxWIhYUPB7FJfZWGXPlcZa0',
        token_type: 'Token',
      },
    },
  },
  transformResponse: res => {
    if (res.status < 400) {
      return res.body;
    }
    const err = new Error(`${res.status} ${res.statusText}`);
    err.message = res;
    throw err;
  },
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class OnfidoDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'onfido';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.onfido', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }

  static async tolerantCall<Type>(
    call: Promise<Type>,
    maxAttempts = 2,
    attemptDelayInMs = 5000,
    tolerantStatus: number[] = [429],
  ) {
    let attempts = 0;
    while (attempts < maxAttempts) {
      try {
        return await call;
      } catch (e) {
        attempts++;
        const matchedException = tolerantStatus.find(
          exception => e.status === exception,
        );
        if (!matchedException) {
          throw e;
        }
        await new Promise(resolve => setTimeout(resolve, attemptDelayInMs));
      }
    }
  }
}
