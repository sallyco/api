import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'keycloak',
  connector: 'openapi',
  spec: './external_service_specs/keycloak.json',
  validate: false,
  positional: false,
  url: process.env.KC_BASE_URL,
  authorizations: {
    access_token: 'lldImc17dnLEoeIbw7UR0mFYoeotorcK',
  },
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class KeycloakDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'keycloak';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.keycloak', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
