import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  connector: 'rest',
  debug: false,
  options: {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    strictSSL: false,
  },
  operations: [
    {
      template: {
        method: 'GET',
        url: 'https://uat-api.synapsefi.com/v3.1/users/{format=json}',
        query: {
          page: '{page}',
          per_page: '{per_page}',
          query: '{query}',
        },
        options: {
          strictSSL: true,
          useQuerystring: true,
        },
        responsePath: '$.results[0]',
      },
      functions: {
        getUsers: ['page', 'per_page', 'query'],
      },
    },
  ],
};
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class SynapsefiDatasource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'synapsefi';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.synapsefi', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
