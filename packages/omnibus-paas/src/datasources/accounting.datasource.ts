import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

interface AccountingConfigInterface {
  name: string;
  connector: string;
  url: string;
  host?: string;
  port?: number;
  user?: string;
  password?: string;
  database?: string;
  connectionTimeout: number;
}

const config: AccountingConfigInterface = {
  name: 'accounting',
  connector: 'postgresql',
  url: process.env.ACCOUNTINGDB_URL ?? '',
  connectionTimeout: 20000,
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class AccountingDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'accounting';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.accounting', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
