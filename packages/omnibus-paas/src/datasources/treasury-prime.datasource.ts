import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'TreasuryPrime',
  connector: 'openapi',
  spec: './external_service_specs/treasury-prime.json',
  url: process.env.TREASURYPRIME_URL ?? 'https://api.sandbox.treasuryprime.com',
  validate: false,
  positional: true,
  authorizations: {
    basicAuth: {
      username: process.env.TREASURYPRIME_ID ?? 'key_glassboard',
      password:
        process.env.TREASURYPRIME_KEY ?? 'lldImc17dnLEoeIbw7UR0mFYoeotorcK',
    },
  },
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class TreasuryPrimeDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'TreasuryPrime';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.TreasuryPrime', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
