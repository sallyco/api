import {
  bind,
  /* inject, */ BindingScope,
  inject,
  service,
} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Request, RestBindings} from '@loopback/rest';
import moment from 'moment';
import Synapse, {Node} from 'synapsenode';
import {Entities} from '../../models';
import {AccountRepository} from '../../repositories';
import {
  AccountACHDetails,
  AccountTransaction,
  AccountWireDetails,
  BankAccount,
  CounterPartyAccount,
  DepositAccount,
  Paging,
  TransactionACHDetail,
  TransactionFilter,
  TransactionWireDetail,
} from './banking.service';
import {PlaidService} from './plaid.service';

@bind({scope: BindingScope.SINGLETON})
export class SynapseFiService {
  public typeId = 'SYNAPSEFI';

  private IP_ADDRESS = '127.0.0.1';
  private synapseClient: Synapse.Client;

  constructor(
    @service(PlaidService) private plaidService: PlaidService,
    @repository(AccountRepository) private accountRepository: AccountRepository,
  ) {
    this.synapseClient = new Synapse.Client({
      client_id: String(process.env.SYNAPSEFI_CLIENT_ID),
      client_secret: String(process.env.SYNAPSEFI_CLIENT_SECRET),
      fingerprint: String(process.env.SYNAPSEFI_FINGERPRINT),
      ip_address: this.resolveIP(),
      isProduction: ['production'].includes(String(process.env.ENVIRONMENT)),
    });
  }

  getACHCreditLimit: () => Promise<number>;
  getACHDebitLimit: () => Promise<number>;

  getDepositAccountTransactions: (
    depositAccount: DepositAccount,
    paging?: Paging,
    filter?: TransactionFilter,
  ) => Promise<AccountTransaction[]>;

  createACHTransaction: (
    accountId: string,
    counterpartyId: string,
    amount: string,
    direction: 'CREDIT' | 'DEBIT',
    idempotenceKey: string,
  ) => Promise<TransactionACHDetail>;
  getACHTransactionDetail: (
    transactionID: string,
  ) => Promise<TransactionACHDetail>;

  createACHTransfer: (
    accountId: string,
    counterpartyId: string,
    amount: string,
    direction: 'CREDIT' | 'DEBIT',
    idempotenceKey: string,
  ) => Promise<TransactionACHDetail>;

  createACHCounterParty: (
    ACHDetails: AccountACHDetails,
  ) => Promise<CounterPartyAccount>;
  createEntityDepositAccount: (entity: Entities) => Promise<DepositAccount>;
  getDepositAccountDetails: (accountId: string) => Promise<BankAccount>;
  getDepositAccountWireDetails: (
    depositAccount: DepositAccount,
  ) => Promise<AccountWireDetails>;

  resolveIP(
    @inject(RestBindings.Http.REQUEST, {optional: true}) request?: Request,
  ): string {
    if (request) {
      return String(
        request?.headers['x-forwarded-for'] ??
          request?.connection.remoteAddress,
      );
    }
    return this.IP_ADDRESS;
  }

  createUser(userData: {
    logins: Synapse.Login[];
    phone_numbers: string[];
    legal_names: string[];
    documents: Synapse.Document[];
    extra?: Synapse.Extra;
  }) {
    return this.synapseClient.createUser(userData, this.resolveIP());
  }

  getUser(userId: string) {
    return this.synapseClient.getUser(userId);
  }

  async createNodeFromPlaidAccount(
    accountId: string,
    plaidSubAccountId: string,
    synapseUserId: string,
  ): Promise<string> {
    const account = await this.accountRepository.findById(accountId);
    const authObject = await this.plaidService.getAuth(
      account.accessToken,
      plaidSubAccountId,
    );
    const transactionsObject = await this.plaidService.getTransactions(
      account.accessToken,
      plaidSubAccountId,
    );
    const synapseUser = await this.synapseClient.getUser(synapseUserId);

    const NewNodeData: Node = {
      type: 'ACH-US',
      extra: {
        other: {
          transactions: transactionsObject.transactions.map(transaction => {
            return {
              description: transaction.name,
              amount: transaction.amount,
              date: moment(transaction.date).unix(),
              pending: transaction.pending,
              debit: Number(transaction.amount) < 0,
            };
          }),
        },
      },
      info: {
        account_num: authObject.numbers.ach[0].account,
        routing_num: authObject.numbers.ach[0].routing,
        nickname: authObject.accounts[0].name,
        class: authObject.accounts[0].subtype.toUpperCase(),
        // TODO: Figure this out from Plaid Data?
        type: 'PERSONAL',
        balance: {
          // TODO: Synapse isn't using this data in UAT?
          amount: transactionsObject.accounts[0].balances.current,
          // TODO: Figure out currency conversion handling?
          currency: 'USD',
        },
      },
    };
    const nodeData = await synapseUser.createNode(NewNodeData);
    return nodeData.data.nodes[0]._id!;
  }

  createWireTransfer: (
    idempotenceKey: string,
    accountId: string,
    amount: string,
    counterpartyId: string,
    instructions: string,
    // intermediary: object,
    userData?: object,
  ) => Promise<TransactionWireDetail>;
  getWireTransfer: (transactionID: string) => Promise<TransactionWireDetail>;
  getWireCreditLimit: () => Promise<number>;
  createWireCounterParty: (
    WireDetails: AccountWireDetails,
  ) => Promise<CounterPartyAccount>;
}
//{
//         logins: [
//           {
//             email: 'test@synapsefi.com'
//           }
//         ],
//         phone_numbers: [
//           '901.111.1111',
//           'test@synapsefi.com'
//         ],
//         legal_names: [
//           'Test User'
//         ],
//         documents: [
//           {
//             email: 'test@test.com',
//             phone_number: '901.111.1111',
//             ip: '::1',
//             name: 'Test User',
//             alias: 'Test',
//             entity_type: 'M',
//             entity_scope: 'Arts & Entertainment',
//             day: 2,
//             month: 5,
//             year: 1989,
//             address_street: '944 Market St.',
//             address_city: 'SF',
//             address_subdivision: 'CA',
//             address_postal_code: '94102',
//             address_country_code: 'US',
//             virtual_docs: [
//               {
//                 document_value: '2222',
//                 document_type: 'SSN'
//               }
//             ],
//             physical_docs: [
//               {
//                 document_value: 'data:image/gif;base64,SUQs==',
//                 document_type: 'GOVT_ID'
//               }
//             ],
//             social_docs: [
//               {
//                 document_value: 'https://www.facebook.com/valid',
//                 document_type: 'FACEBOOK'
//               }
//             ]
//           }
//         ],
//         extra: {
//           supp_id: '122eddfgbeafrfvbbb',
//           cip_tag: 1,
//           is_business: false
//         }
//       }
