import {bind, BindingScope, inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import moment from 'moment';
import plaid, {
  AuthResponse,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  CreateLinkTokenResponse,
  ResetLoginResponse,
  TokenResponse,
} from 'plaid';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {BankingExternalAccountConnector} from './banking-external-account-connector.service';
@bind({scope: BindingScope.TRANSIENT})
export class PlaidService implements BankingExternalAccountConnector {
  public typeId = 'PLAID';
  private plaidClient: plaid.Client;
  constructor() {
    this.plaidClient = new plaid.Client({
      clientID: process.env.PLAID_CLIENT_ID ?? '5f6bb2ca13826c0013ee6e18',
      secret: process.env.PLAID_SECRET ?? '06861c8e0a62066c3ee8590636c5fd',
      env: ['local', 'development', 'sandbox', 'qa'].includes(
        process.env.ENVIRONMENT ?? 'local',
      )
        ? plaid.environments.sandbox
        : plaid.environments.production,
      options: {
        version: '2019-05-29',
      },
    });
  }

  async createLinkToken(
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<CreateLinkTokenResponse> {
    return this.plaidClient.createLinkToken({
      user: {
        client_user_id: String(profile.id),
      },
      client_name: String(tenant.name ?? 'Glassboard Technology'),
      products: ['auth', 'transactions'],
      country_codes: ['US'],
      language: 'en',
    });
  }

  async exchangePublicToken(publicToken: string): Promise<TokenResponse> {
    return this.plaidClient.exchangePublicToken(publicToken);
  }

  async getTransactions(accessToken: string, subAccountId: string) {
    const getDaysInMonth = (year, month) => new Date(year, month, 0).getDate();

    const addMonths = (input, months) => {
      const date = new Date(input);
      date.setDate(1);
      date.setMonth(date.getMonth() + months);
      date.setDate(
        Math.min(
          input.getDate(),
          getDaysInMonth(date.getFullYear(), date.getMonth() + 1),
        ),
      );
      return date;
    };

    const now = new Date();
    return this.plaidClient.getTransactions(
      accessToken,
      moment(addMonths(now, -6)).format('YYYY-MM-DD'),
      moment(now).format('YYYY-MM-DD'),
      {
        account_ids: [subAccountId],
      },
    );
  }

  async getAuth(
    accessToken: string,
    subAccountId: string,
  ): Promise<AuthResponse> {
    return this.plaidClient.getAuth(accessToken, {
      account_ids: [subAccountId],
    });
  }

  async createUpdateLinkToken(
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(SecurityBindings.USER) profile: UserProfile,
    accessToken: string,
  ): Promise<CreateLinkTokenResponse> {
    return this.plaidClient.createLinkToken({
      user: {
        client_user_id: String(profile.id),
      },
      client_name: String(tenant.name ?? 'Glassboard Technology'),
      products: [],
      country_codes: ['US'],
      language: 'en',
      access_token: accessToken,
    });
  }

  async resetLogin(accessToken: string): Promise<ResetLoginResponse> {
    return this.plaidClient.resetLogin(accessToken);
  }
}
