import {BindingScope, injectable, service} from '@loopback/core';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import assert from 'assert';
import debugFactory from 'debug';
import moment from 'moment';
import {distance} from 'fastest-levenshtein';
import {
  BankAccount,
  BankingBeneficiary,
  Deal,
  Entities,
  Entities as Entity,
  Subscription,
  Transaction,
  TransactionStatus,
} from '../../models';
import {
  BankAccountRepository,
  BankingBeneficiaryRepository,
  DealRepository,
  EntitiesRepository as EntityRepository,
  ProfileRepository,
  SubscriptionRepository,
  TransactionRepository,
} from '../../repositories';
import {
  AccountTransaction,
  Banking,
  BankingProvider,
  CounterpartyTransactionDetail,
  CounterpartyTransactionType,
} from './banking.service';
import {EmailsService} from '../notifications/emails.service';
import {inject} from '@loopback/context';
import {TenantService} from '../tenant/tenant.service';
import {AccountTransactionResponse} from '../../interfaces/services/treasury-prime.interface';
import {SystemNotificationsService} from '../system/system-notifications.service';

const debug = debugFactory('omnibus-paas:service:transactions');

export enum TransactionServiceErrors {
  'COUNTERPARTY_MISSING' = 'Counterparty ID is Missing',
  'AMOUNT_INVALID' = 'Transaction Amount is Invalid',
  'ENTITY_MISSING_BANKING' = 'Entity Missing Banking',
  'BANK_ACCOUNT_NOT_READY' = 'Bank Account is not Ready for Transactions',
  'ENTITY_BANKING_NOT_READY' = 'Entity Bank Account is not ready',
  'SUBSCRIPTION_NOT_READY' = 'Subscription is not ready for Transaction',
  'SUBSCRIPTION_DUPLICATE_TRANSACTION' = 'Subscription has another transaction attached',
  'TOO_MANY_REQUESTS' = 'Too Many Requests',
  'SUBSCRIPTION_MISSING_TRANSACTION' = 'Subscription existing transaction missing',
  'SUBSCRIPTION_CLOSED' = 'Subscription is in Closed State',
  'SUBSCRIPTION_EXISTING_TRANSACTION_NOT_RESOLVED' = 'Transaction to be reversed is not in a resolved state',
  'INSUFFICIENT_FUNDS' = 'Account has insufficient funds to support the requested transaction',
  'NEEDS_APPROVAL' = 'Transaction needs approval',
}

@injectable({scope: BindingScope.TRANSIENT})
export class TransactionService {
  constructor(
    @service(BankingProvider) private readonly banking: Banking,
    @repository(TransactionRepository)
    private readonly transactionRepository: TransactionRepository,
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @repository(DealRepository) private readonly dealRepository: DealRepository,
    @repository(EntityRepository)
    private readonly entityRepository: EntityRepository,
    @repository(BankingBeneficiaryRepository)
    private readonly bankingBeneficiaryRepository: BankingBeneficiaryRepository,
    @service(EmailsService) private emailService: EmailsService,
    @inject('services.TenantService') private tenantService: TenantService,
    @repository(ProfileRepository)
    private readonly profileRepository: ProfileRepository,
    @repository(BankAccountRepository)
    private readonly bankAccountRepository: BankAccountRepository,
    @service(SystemNotificationsService)
    private readonly systemNotifications: SystemNotificationsService,
  ) {}

  async validateSubscriptionReadyForTransaction(subscription: Subscription) {
    if (!subscription.bankAccount?.counterpartyId) {
      throw new Error(TransactionServiceErrors.COUNTERPARTY_MISSING);
    }
    if (!subscription?.amount || subscription?.amount <= 0) {
      throw new Error(TransactionServiceErrors.AMOUNT_INVALID);
    }
  }

  async validateSubscriptionReadyForReverseTransaction(
    subscription: Subscription,
  ) {
    await this.validateSubscriptionReadyForTransaction(subscription);
    if (
      !subscription.transactionIds &&
      !subscription?.bankAccount?.counterpartyId
    ) {
      throw new Error(
        TransactionServiceErrors.SUBSCRIPTION_MISSING_TRANSACTION,
      );
    }
    if (subscription.status === 'CLOSED') {
      throw new Error(TransactionServiceErrors.SUBSCRIPTION_CLOSED);
    }
    if (subscription.transactionIds) {
      for (const transactionId of subscription.transactionIds) {
        const transaction = await this.transactionRepository.findById(
          transactionId,
        );
        if (transaction.status !== 'SENT') {
          throw new Error(
            TransactionServiceErrors.SUBSCRIPTION_EXISTING_TRANSACTION_NOT_RESOLVED,
          );
        }
      }
    }
  }

  async validateSubscriptionReadyForTransactionProcessing(
    subscription: Subscription,
    transaction: Transaction,
  ) {
    if (transaction.direction === 'DEBIT') {
      if (!['COMMITTED', 'FUNDING SENT'].includes(subscription.status)) {
        throw new Error(TransactionServiceErrors.SUBSCRIPTION_NOT_READY);
      }
    }
    if (transaction.direction === 'CREDIT') {
      if (['INVITED'].includes(subscription.status)) {
        throw new Error(TransactionServiceErrors.SUBSCRIPTION_NOT_READY);
      }
      if (
        String(subscription.reverseTransactionId) !== String(transaction.id)
      ) {
        throw new Error(
          TransactionServiceErrors.SUBSCRIPTION_DUPLICATE_TRANSACTION,
        );
      }
    }
  }

  async validateEntityBankingReady(entity: Entity) {
    if (!entity.bankAccount?.providerMeta?.accountId) {
      throw new Error(TransactionServiceErrors.ENTITY_MISSING_BANKING);
    }
    if (
      !['open', 'active', 'approved'].includes(
        (entity.bankAccount.providerMeta?.accountStatus ?? '').toLowerCase(),
      )
    ) {
      throw new Error(TransactionServiceErrors.ENTITY_BANKING_NOT_READY);
    }
  }

  //TODO: Merge with `validateEntityBankingReady`?
  // Only check top-level status instead of providerMeta, or move to
  // Validation from banking provider
  async validateBankAccountReady(bankAccount: Partial<BankAccount>) {
    if (!bankAccount?.providerMeta?.accountId) {
      throw new Error(TransactionServiceErrors.BANK_ACCOUNT_NOT_READY);
    }
    if (
      !['open', 'active', 'approved'].includes(
        (bankAccount.providerMeta?.accountStatus ?? '').toLowerCase(),
      )
    ) {
      throw new Error(TransactionServiceErrors.BANK_ACCOUNT_NOT_READY);
    }
  }

  async getSubscriptionRelatedModels(subscriptionId: string): Promise<{
    subscription?: Subscription;
    deal: Deal;
    entity: Entities;
  }> {
    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );
    assert(subscription);
    const deal = await this.dealRepository.findById(subscription.dealId);
    assert(deal);
    assert(deal.entityId);
    const entity = await this.entityRepository.findById(deal.entityId);
    assert(entity);
    return {subscription, deal, entity};
  }

  async createACHTransactionForEntity(
    entityId: string,
    direction: 'CREDIT' | 'DEBIT' = 'DEBIT',
    amount: number,
    counterpartyId: string,
    closeId?: string | undefined,
    needsApproval = true,
  ) {
    const entity = await this.entityRepository.findById(entityId);
    let initialStatus = TransactionStatus.QUEUED;
    let initialReason = undefined;
    try {
      await this.validateEntityBankingReady(entity);
    } catch (e) {
      initialStatus = TransactionStatus.WAITING;
      initialReason = e.message;
      if (direction === 'CREDIT') {
        throw new Error(initialReason);
      }
    }

    return this.transactionRepository.save(
      new Transaction({
        tenantId: entity.tenantId,
        type: 'ACH',
        direction: direction,
        amount: amount,
        status: initialStatus,
        needsApproval,
        closeId,
        entitiesId: entity.id,
        dealId: entity.dealId,
        counterpartyId: counterpartyId,
        reason: initialReason,
        createdAt: new Date(),
      }),
    );
  }

  async createACHTransaction(
    bankAccountId: string,
    direction: 'CREDIT' | 'DEBIT' = 'DEBIT',
    amount: number,
    counterpartyId: string,
  ) {
    const bankAccount = await this.bankAccountRepository.findById(
      bankAccountId,
    );

    let initialStatus = TransactionStatus.QUEUED;
    let initialReason = undefined;
    try {
      await this.validateBankAccountReady(bankAccount);
    } catch (e) {
      initialStatus = TransactionStatus.WAITING;
      initialReason = e.message;
    }

    return this.transactionRepository.save(
      new Transaction({
        tenantId: bankAccount.tenantId,
        type: 'ACH',
        direction: direction,
        amount: amount,
        status: initialStatus,
        bankAccountId: bankAccountId,
        counterpartyId: counterpartyId,
        reason: initialReason,
        createdAt: new Date(),
      }),
    );
  }

  async createSubscriptionTransaction(
    subscriptionId: string,
    direction: 'CREDIT' | 'DEBIT' = 'DEBIT',
    amount?: number,
    scheduledDate?: Date,
  ) {
    const {subscription, entity} = await this.getSubscriptionRelatedModels(
      subscriptionId,
    );
    assert(subscription);

    let transactionAmount = amount;
    if (!amount) {
      transactionAmount = subscription.amount;
    }
    let counterpartyId;
    if (direction === 'DEBIT') {
      await this.validateSubscriptionReadyForTransaction(subscription);
      counterpartyId = subscription.bankAccount?.counterpartyId ?? '';
    } else if (direction === 'CREDIT') {
      await this.validateSubscriptionReadyForReverseTransaction(subscription);
    }

    let initialStatus = TransactionStatus.QUEUED;
    let initialReason = undefined;
    try {
      await this.validateEntityBankingReady(entity);
    } catch (e) {
      initialStatus = TransactionStatus.WAITING;
      initialReason = e.message;
    }

    const TransactionRecord = await this.transactionRepository.save(
      new Transaction({
        tenantId: subscription.tenantId,
        type: 'ACH',
        direction: direction,
        amount: transactionAmount,
        status: initialStatus,
        counterpartyId: counterpartyId,
        subscriptionId: subscription.id,
        dealId: subscription.dealId,
        reason: initialReason,
        createdAt: scheduledDate ?? new Date(),
      }),
    );
    if (direction === 'DEBIT') {
      subscription.transactionIds = [
        ...(subscription.transactionIds ? subscription.transactionIds : []),
        TransactionRecord.id!,
      ];
    } else if (direction === 'CREDIT') {
      subscription.reverseTransactionId = TransactionRecord.id;
      if (!amount || amount === subscription.amount) {
        subscription.status = 'REFUNDED';
      } else if (amount > 0 && amount < (subscription?.amount ?? 0)) {
        subscription.status = 'PENDING';
        subscription.amount = (subscription.amount ?? 0) - amount;
      }
    }
    await this.subscriptionRepository.update(subscription);
    return TransactionRecord;
  }

  async createReverseSubscriptionTransaction(
    subscriptionId: string,
    amount?: number,
    scheduledDate?: Date,
  ) {
    return this.createSubscriptionTransaction(
      subscriptionId,
      'CREDIT',
      amount,
      scheduledDate,
    );
  }

  async getNextWaitingTransaction(
    transactionType: CounterpartyTransactionType = 'ACH',
  ): Promise<Transaction | null> {
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.or([
      {
        and: [
          {
            status: {
              inq: [
                TransactionStatus.WAITING,
                TransactionStatus.DEPRECATED_WAITING_ON_ENTITY,
                TransactionStatus.DEPRECATED_TRANSACTION_FAILURE,
              ],
            },
          },
          {
            type: transactionType,
          },
        ],
      },
      {
        and: [
          {
            status: {
              eq: TransactionStatus.TRANSACTION_EXCEPTION,
            },
          },
          {
            reason: {
              eq: 'Too Many Requests',
            },
          },
          {
            type: transactionType,
          },
        ],
      },
    ]);
    return this.transactionRepository.findOne({
      limit: 50,
      where: transactionsWhere.build(),
      order: ['createdAt ASC'],
    });
  }

  async getNextQueuedTransaction(
    transactionType: CounterpartyTransactionType = 'ACH',
    skipTransactionIds?: string[],
  ): Promise<Transaction | null> {
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.and([
      {
        status: {
          inq: [TransactionStatus.QUEUED],
        },
      },
      {
        type: transactionType,
      },
      {
        needsApproval: {
          neq: true,
        },
      },
      {
        createdAt: {
          lte: new Date(),
        },
      },
      ...(skipTransactionIds?.length && skipTransactionIds.length > 0
        ? [
            {
              id: {
                nin: skipTransactionIds,
              },
            },
          ]
        : []),
    ]);
    return this.transactionRepository.findOne({
      where: transactionsWhere.build(),
      limit: 50,
      order: ['createdAt ASC'],
    });
  }

  async processWaitingTransactions(
    transactionType: CounterpartyTransactionType = 'ACH',
  ) {
    let transactionsProcessed = 0;
    const transactionsToProcess =
      process.env.BANKING_WAITING_TRANSACTION_BATCH_SIZE ?? 10;
    let transaction = await this.getNextWaitingTransaction(transactionType);
    let entity: Entity;
    while (transaction && transactionsProcessed < transactionsToProcess) {
      try {
        if (transaction.subscriptionId) {
          const {entity: subscriptionEntity} =
            await this.getSubscriptionRelatedModels(transaction.subscriptionId);
          entity = subscriptionEntity;
          await this.validateEntityBankingReady(entity);
        } else if (transaction.entitiesId) {
          entity = await this.entityRepository.findById(transaction.entitiesId);
          await this.validateEntityBankingReady(entity);
        } else if (transaction.bankAccountId) {
          const bankAccount = await this.bankAccountRepository.findById(
            transaction.bankAccountId,
          );
          await this.validateBankAccountReady(bankAccount);
        } else {
          throw new Error(TransactionServiceErrors.ENTITY_BANKING_NOT_READY);
        }

        transaction.status = TransactionStatus.QUEUED;
        debug(`Queued transaction`, transaction.id);
        delete transaction.reason;
      } catch (e) {
        debug(`Transaction waiting for reason: ${e.message}`, transaction.id);
        transaction.status = TransactionStatus.WAITING;
        transaction.reason = e.message;
      }

      await this.transactionRepository.save(transaction);
      transactionsProcessed++;
      transaction = await this.getNextWaitingTransaction(transactionType);
    }
  }

  async getCurrentACHLimits(): Promise<{
    creditLimit: number;
    debitLimit: number;
  }> {
    const maxDebitLimit = await this.banking.getACHDebitLimit();
    const maxCreditLimit = await this.banking.getACHCreditLimit();
    let remainingDebitLimit = 0;
    let remainingCreditLimit = 0;
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.and([
      {
        status: {
          inq: [
            TransactionStatus.SENT,
            TransactionStatus.PENDING,
            TransactionStatus.PROCESSING,
          ],
        },
      },
      {
        addedToNetwork: {
          gt: moment().subtract(24, 'hours').toDate(),
        },
      },
      {
        type: 'ACH',
      },
    ]);
    const transactions = await this.transactionRepository.find({
      where: transactionsWhere.build(),
    });
    if (transactions.length < 1) {
      return {debitLimit: maxDebitLimit, creditLimit: maxCreditLimit};
    }
    remainingDebitLimit = transactions
      .filter(transaction => transaction.direction === 'DEBIT')
      .map(transaction => transaction.amount)
      .reduce((totalLimit, amount) => {
        return totalLimit + amount;
      }, 0);
    remainingCreditLimit = transactions
      .filter(transaction => transaction.direction === 'CREDIT')
      .map(transaction => transaction.amount)
      .reduce((totalLimit, amount) => {
        return totalLimit + amount;
      }, 0);
    return {
      debitLimit: maxDebitLimit - remainingDebitLimit,
      creditLimit: maxCreditLimit - remainingCreditLimit,
    };
  }

  async getCurrentWireLimits(): Promise<{creditLimit: number}> {
    const wireLimit = await this.banking.getWireCreditLimit();
    let remainingCreditLimit = 0;

    const transactionsWhere = new WhereBuilder();
    transactionsWhere.and([
      {
        status: {
          inq: [
            TransactionStatus.SENT,
            TransactionStatus.PENDING,
            TransactionStatus.PROCESSING,
          ],
        },
      },
      {
        addedToNetwork: {
          gt: moment().subtract(24, 'hours').toDate(),
        },
      },
      {
        type: 'WIRE',
      },
    ]);
    const transactions = await this.transactionRepository.find({
      where: transactionsWhere.build(),
    });

    if (transactions.length <= 0) {
      return {creditLimit: wireLimit};
    }

    remainingCreditLimit = transactions
      .map(transaction => transaction.amount)
      .reduce((totalLimit, amount) => {
        return totalLimit + amount;
      }, 0);

    return {creditLimit: wireLimit - remainingCreditLimit};
  }

  /**
   *
   * @param transaction used to for the amount and direction
   * @param adjustedCreditLimit current credit limit that is left
   * @param adjustedDebitLimit current debit limit that is left
   * @returns true if this transactions pushes over the limit
   */
  achLimitReached(
    transaction,
    adjustedCreditLimit,
    adjustedDebitLimit,
  ): boolean {
    if (
      transaction.direction === 'DEBIT' &&
      adjustedDebitLimit - transaction.amount < 0
    ) {
      debug('ACH Debit Limit Reached, waiting for capacity to process more');
      return true;
    } else if (
      transaction.direction === 'CREDIT' &&
      adjustedCreditLimit - transaction.amount < 0
    ) {
      debug('ACH Credit Limit Reached, waiting for capacity to process more');
      return true;
    }

    return false;
  }

  /**
   *
   * @param transaction used to for the amount and direction
   * @param adjustedCreditLimit current credit limit that is left
   * @returns true if this transactions pushes over the limit
   */
  wireLimitReached(transaction, adjustedCreditLimit): boolean {
    if (adjustedCreditLimit - transaction.amount < 0) {
      debug('Wire Credit Limit Reached, waiting for capacity to process more');
      return true;
    }
    return false;
  }

  async processACHQueuedTransactions() {
    const transactionsInBatch: string[] = [];
    const transactionsToProcess =
      process.env.BANKING_TRANSACTION_BATCH_SIZE ?? 1;
    const {debitLimit, creditLimit} = await this.getCurrentACHLimits();
    // const {creditLimit: wireCreditLimit} = await this.getCurrentWireLimits();
    let adjustedDebitLimit = debitLimit;
    let adjustedCreditLimit = creditLimit;

    let transaction = await this.getNextQueuedTransaction(
      'ACH',
      transactionsInBatch,
    );
    while (transaction && transactionsInBatch.length < transactionsToProcess) {
      debug(
        'Calculated ACH Debit/Credit Limit is ',
        `${debitLimit} / ${creditLimit}`,
      );

      if (
        this.achLimitReached(
          transaction,
          adjustedCreditLimit,
          adjustedDebitLimit,
        )
      ) {
        transactionsInBatch.push(transaction.id!);
        continue;
      }

      assert(transaction.id);

      let subscription, entity, deal;

      let bankAccountId;
      let counterpartyId;
      let idempotenceKey;
      let additionalData = {};
      let description;

      try {
        if (transaction.subscriptionId) {
          const models = await this.getSubscriptionRelatedModels(
            transaction.subscriptionId,
          );
          subscription = models.subscription;
          entity = models.entity;
          deal = models.deal;
          await this.validateEntityBankingReady(entity);
          await this.validateSubscriptionReadyForTransactionProcessing(
            subscription,
            transaction,
          );
          bankAccountId = entity!.bankAccount!.providerMeta!.accountId!;
          counterpartyId = subscription!.bankAccount!.counterpartyId!;
          idempotenceKey = subscription.id;
          description = `${subscription.id.substr(-4)}:${deal.id.substr(-4)}`;
          additionalData = {
            subscriptionId: subscription.id,
          };
        } else if (transaction.bankAccountId) {
          const bankAccount = await this.bankAccountRepository.findById(
            transaction.bankAccountId,
          );
          await this.validateBankAccountReady(bankAccount);
          counterpartyId = transaction.counterpartyId;
          idempotenceKey =
            counterpartyId + bankAccount.id + transaction.direction;
          description = 'ACH';
          bankAccountId = bankAccount.providerMeta?.accountId;
        } else if (transaction.entitiesId) {
          entity = await this.entityRepository.findById(transaction.entitiesId);
          await this.validateEntityBankingReady(entity);
          bankAccountId = entity!.bankAccount!.providerMeta!.accountId!;
          counterpartyId = transaction.counterpartyId;
          idempotenceKey =
            counterpartyId + bankAccountId + transaction.direction;
          description = `${entity.id.substr(-4)} ACH`;
          additionalData = {
            entityId: entity.id,
          };
        }

        const {
          id: TransactionID,
          ...BankingTransaction
        }: CounterpartyTransactionDetail = await this.banking.createACHTransaction(
          bankAccountId,
          counterpartyId,
          transaction!.amount!.toFixed(2),
          transaction.direction,
          transaction!.amount! + idempotenceKey + new Date().getDay(),
          description,
          additionalData,
        );
        debug(`Processed transaction`, transaction.id);

        await this.transactionRepository.updateById(transaction.id, {
          status: BankingTransaction.status,
          transactionId: TransactionID,
          providerMeta: {
            ...transaction.providerMeta,
            ...BankingTransaction.providerMeta,
          },
        });
        // Should be deprecated, needs arch
        if (transaction.direction === 'DEBIT') {
          adjustedDebitLimit = adjustedDebitLimit - transaction.amount;
          if (transaction.subscriptionId) {
            subscription.status = 'COMPLETED';
            await this.subscriptionRepository.update(subscription);
          }
        } else if (transaction.direction === 'CREDIT') {
          adjustedCreditLimit = adjustedCreditLimit - transaction.amount;
        }
      } catch (e) {
        debug(`Transaction Failure for Transaction ${transaction.id}`, e);
        await this.transactionRepository.updateById(transaction.id, {
          status: TransactionStatus.TRANSACTION_EXCEPTION,
          reason: `${e?.message}${e?.error ?? ''}`,
          reasonDetail: e,
        });
        await this.systemNotifications.Notify_Transaction_Exception(
          await this.transactionRepository.findById(transaction.id),
        );
      }

      transactionsInBatch.push(transaction.id);
      transaction = await this.getNextQueuedTransaction(
        'ACH',
        transactionsInBatch,
      );
    }
  }

  async syncACHTransactionsBanking() {
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.and([
      {
        transactionId: {
          exists: true,
          neq: null,
        },
      },
      {
        type: 'ACH',
      },
      {
        status: {
          nin: [
            TransactionStatus.SENT,
            TransactionStatus.QUEUED,
            TransactionStatus.WAITING,
            TransactionStatus.TRANSACTION_EXCEPTION,
          ],
        },
      },
    ]);
    // eslint-disable-next-line @typescript-eslint/no-invalid-this
    const transactions: Transaction[] = await this.transactionRepository.find({
      where: transactionsWhere.build(),
    });
    for (const transactionRecord of transactions) {
      try {
        const transactionDetail = await this.banking.getACHTransactionDetail(
          transactionRecord.transactionId!,
        );
        transactionRecord.status = transactionDetail.status;
        if (transactionRecord.status === TransactionStatus.SENT) {
          transactionRecord.addedToNetwork = new Date();
          if (transactionRecord.subscriptionId) {
            // Do this until transaction resolution
            const subscription = await this.subscriptionRepository.findById(
              transactionRecord.subscriptionId,
            );
            if (subscription.status === 'FUNDING SENT') {
              subscription.status = 'COMPLETED';
              await this.subscriptionRepository.save(subscription);
            }
          }
        }
        transactionRecord.providerMeta = {
          ...transactionRecord.providerMeta,
          ...transactionDetail.providerMeta,
        };
        transactionRecord.updatedAt = new Date();
        debug(
          'Successfully synced AccreditationStatus for Transaction ID:',
          transactionRecord.id,
        );
        await this.transactionRepository.update(transactionRecord);
      } catch (e) {
        debug(
          `Issue updating Banking Status for Transaction ID: ${transactionRecord.id}`,
          e,
        );
      }
    }
  }

  public hasACHId(
    transaction: AccountTransaction | AccountTransactionResponse,
  ): undefined | string {
    if (transaction.ach_id) {
      return transaction.ach_id;
    }
    let text = '';
    if (transaction.summary && transaction.summary.length > 0) {
      text = transaction.summary;
    } else if (transaction.desc && transaction.desc.length > 0) {
      text = transaction.desc;
    }
    const matches = text.match(/(?:ID NBR: )([0-9a-z]+)$/);
    if (matches?.[1]) {
      return `ach_${matches?.[1]}`;
    }

    const new_pattern_matches = text.match(/\s([a-z0-9]{6,})/);
    if (new_pattern_matches?.[1]) {
      return `ach_${new_pattern_matches?.[1]}`;
    }

    return undefined;
  }

  public hasWireId(transaction: AccountTransaction): undefined | string {
    if (transaction.wire_id) {
      return transaction.wire_id;
    }
    const text = transaction.summary ?? transaction.desc ?? '';
    const matches = text.match(/(?:ID NBR: )([0-9a-z]+)$/);
    return matches?.[1];
  }

  async syncAccountTransactionBanking() {
    const entitiesWhere = new WhereBuilder()
      .and([
        {
          'bankAccount.providerMeta.accountId': {
            exists: true,
            neq: null,
          },
        },
        {
          'bankAccount.providerMeta.accountStatus': {
            inq: ['active', 'open', 'approved'],
          },
        },
      ])
      .build();
    const entities: Entity[] = await this.entityRepository.find({
      where: entitiesWhere,
    });
    for (const entity of entities) {
      assert(entity.bankAccount?.providerMeta?.accountId);
      const bankTransactions = await this.banking.getDepositAccountTransactions(
        entity.bankAccount,
        {
          pageSize: 500,
          pageNumber: 0,
        },
      );
      for (const bankTransaction of bankTransactions) {
        try {
          await this.checkForACHLink(bankTransaction);
          await this.checkForWireLink(bankTransaction, entity);
        } catch (e) {
          debug(`Failed to sync Transaction ${bankTransaction.id}`, e);
        }
      }
    }
  }

  async checkForWireLink(bankTransaction: AccountTransaction, entity: Entity) {
    const deal = await this.dealRepository.findOne({
      where: {
        entityId: entity.id,
      },
    });
    assert(deal);
    const subscriptionFilter: Filter<Subscription> = {
      where: {
        dealId: deal.id,
        bankTransactionIds: {
          nin: [[bankTransaction.id]],
        },
      },
    };
    const subscriptions = await this.subscriptionRepository.find(
      subscriptionFilter,
    );
    for (const subscription of subscriptions) {
      if (
        moment(subscription.createdAt).isSameOrBefore(bankTransaction.date) &&
        Number(subscription.amount) === Number(bankTransaction.amount) &&
        subscription.profileId
      ) {
        const profile = await this.profileRepository.findById(
          subscription.profileId,
        );
        const consolidatedName =
          (String(profile.firstName) + String(profile.lastName)).length > 1
            ? profile.firstName + ' ' + profile.lastName
            : String(profile.name);
        if (
          consolidatedName.length &&
          this.levenshtein_substring_distance(
            consolidatedName,
            String(bankTransaction.summary) + String(bankTransaction.desc),
          ) < 2
        ) {
          debug(
            `Found Wire Link for transaction id ${bankTransaction.id} for profile ${profile.id}`,
          );
          subscription.bankTransactionIds =
            subscription.bankTransactionIds ?? [];
          subscription.bankTransactionIds.push(bankTransaction.id);
          await this.subscriptionRepository.save(subscription);
          return true;
        }
      }
    }
    return false;
  }

  // levenshtein_substring_distance(needle: string, haystack: string): number{
  //   let distances = new Array(haystack.length+1).fill(0);
  //
  //   [...needle].forEach((needle_char, needle_index)=>{
  //     const next_distances = [needle_index+1];
  //     [...haystack].forEach((haystack_char, haystack_index)=>{
  //       const [deletion, insertion, substitution] = [
  //         distances[haystack_index+1].length+1 || 1,
  //         next_distances[haystack_index]+1 || 1,
  //         distances[haystack_index] + (needle_char.toUpperCase() === haystack_char.toUpperCase() ? 0 : 1)
  //       ];
  //       next_distances.push(Math.min(...[deletion, insertion, substitution]));
  //     })
  //     distances = next_distances;
  //   });
  //   return Math.min(...distances);
  // }

  levenshtein_substring_distance(needle: string, haystack: string): number {
    let needleWords = needle.split(/[^\w]/);
    let haystackWords = haystack.split(/[^\w]/);
    // Discard any single letter matches
    needleWords = needleWords.filter(word => word.length > 2);
    haystackWords = haystackWords.filter(word => word.length > 2);
    if (needleWords.length < 1 || haystackWords.length < 1) {
      return Math.max(needle.length, haystack.length);
    }
    const totalDistances: number[][] = [];
    for (const needleWord of needleWords) {
      const wordDistances: number[] = [];
      for (const haystackWord of haystackWords) {
        const wordDistance = distance(
          needleWord.toUpperCase(),
          haystackWord.toUpperCase(),
        );
        wordDistances.push(wordDistance);
        if (wordDistance === 0) {
          totalDistances.push(wordDistances);
          break;
        }
      }
      totalDistances.push(wordDistances);
    }
    const totalDistance = totalDistances.map(calculatedDistance =>
      Math.min(...calculatedDistance),
    );
    const calculatedDistance = totalDistance.reduce(
      (total, current) => total + current,
    );
    // console.log(needleWords, haystackWords, totalDistances);
    return calculatedDistance;
  }

  async checkForACHLink(bankTransaction: AccountTransaction) {
    const ACH_ID = this.hasACHId(bankTransaction);
    if (!ACH_ID) {
      return;
    }
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.eq('transactionId', `${ACH_ID}`);
    const TransactionObject = await this.transactionRepository.findOne({
      where: transactionsWhere.build(),
    });
    if (!TransactionObject) {
      debug(`Unable to find matching Transaction object for ACH ID`, ACH_ID);
      return;
    }
    if (TransactionObject.direction === 'DEBIT') {
      TransactionObject.bankAccountTransactionId = bankTransaction.id;
    } else if (TransactionObject.direction === 'CREDIT') {
      TransactionObject.refundBankAccountTransactionId = bankTransaction.id;
    }
    await this.transactionRepository.save(TransactionObject);
  }

  async createWireTransaction(
    bankAccountId: string,
    direction: 'CREDIT',
    amount: number,
    counterPartyId: string,
    instructions?: string,
    needsApproval = false,
  ) {
    const bankAccount = await this.bankAccountRepository.findById(
      bankAccountId,
    );
    const {currentBalance} = await this.banking.getDepositAccountDetails(
      bankAccount!.providerMeta!.accountId!,
    );
    if (!currentBalance || currentBalance < amount) {
      throw new Error(TransactionServiceErrors.INSUFFICIENT_FUNDS);
    }
    return this.transactionRepository.save(
      new Transaction({
        tenantId: bankAccount.tenantId,
        type: 'WIRE',
        direction: direction,
        amount: amount,
        status: 'QUEUED',
        needsApproval,
        counterpartyId: counterPartyId,
        bankAccountId: bankAccountId,
        instructions: instructions,
      }),
    );
  }

  async createWireTransactionForEntity(
    entityId: string,
    direction: 'CREDIT',
    amount: number,
    beneficiaryId: string,
    instructions: string,
    closeId?: string | undefined,
    scheduledDate?: Date,
    needsApproval = true,
  ) {
    const entity = await this.entityRepository.findById(entityId);
    let beneficiary: BankingBeneficiary;
    try {
      beneficiary = await this.bankingBeneficiaryRepository.findById(
        beneficiaryId,
      );
      if (!beneficiary?.serviceObjectId) {
        throw new Error('Provided Banking Beneficiary is not valid');
      }
    } catch (e) {
      throw new Error('Provided Banking Beneficiary is not valid');
    }
    const {currentBalance} = await this.banking.getDepositAccountDetails(
      entity.bankAccount!.providerMeta!.accountId!,
    );
    if (!currentBalance || currentBalance < amount) {
      throw new Error(TransactionServiceErrors.INSUFFICIENT_FUNDS);
    }
    return this.transactionRepository.save(
      new Transaction({
        tenantId: entity.tenantId,
        type: 'WIRE',
        direction: direction,
        amount: amount,
        status: 'QUEUED',
        needsApproval,
        closeId,
        counterpartyId: beneficiary.serviceObjectId,
        entitiesId: entity.id,
        dealId: entity.dealId,
        instructions: instructions,
        updateEmail: beneficiary.email,
        createdAt: scheduledDate ?? undefined,
      }),
    );
  }

  async processWireQueuedTransactions() {
    const transactionsInBatch: string[] = [];
    const transactionsToProcess =
      process.env.BANKING_TRANSACTION_BATCH_SIZE ?? 1;
    const {creditLimit} = await this.getCurrentWireLimits();
    let adjustedCreditLimit = creditLimit;
    let transaction = await this.getNextQueuedTransaction(
      'WIRE',
      transactionsInBatch,
    );
    while (transaction && transactionsInBatch.length < transactionsToProcess) {
      debug('Calculated Wire Credit Limit is ', `${creditLimit}`);

      if (this.wireLimitReached(transaction, adjustedCreditLimit)) {
        transactionsInBatch.push(transaction.id!);
        continue;
      }
      const entity = await this.entityRepository.findById(
        transaction.entitiesId!,
      );
      try {
        await this.validateEntityBankingReady(entity);
        const {
          id: TransactionID,
          ...BankingTransaction
        }: CounterpartyTransactionDetail = await this.banking.createWireTransfer(
          entity!.bankAccount!.providerMeta!.accountId!,
          transaction.counterpartyId!,
          transaction!.amount!.toFixed(2),
          transaction.instructions!,
          entity.id +
            transaction!.amount! +
            transaction.counterpartyId +
            new Date().getDay(),
          {
            entityId: entity.id,
          },
        );
        debug(`Processed wire transaction`, transaction.id);

        await this.transactionRepository.updateById(transaction.id, {
          status: BankingTransaction.status,
          transactionId: TransactionID,
          providerMeta: {
            ...transaction.providerMeta,
            ...BankingTransaction.providerMeta,
          },
        });
        adjustedCreditLimit = adjustedCreditLimit - transaction.amount;
      } catch (e) {
        debug(`Transaction Failure for Transaction ${transaction.id}`, e);
        await this.transactionRepository.updateById(transaction.id, {
          status: TransactionStatus.TRANSACTION_EXCEPTION,
          reason: e.message,
        });
      }

      transactionsInBatch.push(transaction.id!);
      transaction = await this.getNextQueuedTransaction(
        'WIRE',
        transactionsInBatch,
      );
    }
  }

  async syncWireTransactionsBanking() {
    const transactionsWhere = new WhereBuilder();
    transactionsWhere.and([
      {
        transactionId: {
          exists: true,
          neq: null,
        },
      },
      {
        type: 'WIRE',
      },
      {
        status: {
          nin: [
            TransactionStatus.SENT,
            TransactionStatus.QUEUED,
            TransactionStatus.WAITING,
            TransactionStatus.TRANSACTION_EXCEPTION,
          ],
        },
      },
    ]);
    const transactions: Transaction[] = await this.transactionRepository.find({
      where: transactionsWhere.build(),
    });
    for (const transactionRecord of transactions) {
      try {
        const transactionDetail = await this.banking.getWireTransfer(
          transactionRecord.transactionId!,
        );
        if (
          transactionDetail.status !== transactionRecord.status &&
          transactionRecord.updateEmail &&
          transactionDetail.status === TransactionStatus.SENT
        ) {
          try {
            await this.sendTransactionNotification(transactionRecord);
          } catch (e) {
            debug('Failed to Send Transaction Update', e);
          }
        }
        transactionRecord.status = transactionDetail.status;
        if (transactionRecord.status === TransactionStatus.SENT) {
          transactionRecord.addedToNetwork = new Date();
        }
        transactionRecord.providerMeta = {
          ...transactionRecord.providerMeta,
          ...transactionDetail.providerMeta,
        };
        transactionRecord.updatedAt = new Date();
        debug(
          'Successfully synced AccreditationStatus for Transaction ID:',
          transactionRecord.id,
        );
        await this.transactionRepository.update(transactionRecord);
      } catch (e) {
        debug(
          `Issue updating Banking Status for Transaction ID: ${transactionRecord.id}`,
          e,
        );
      }
    }
  }

  async sendTransactionNotification(transaction: Transaction) {
    await this.tenantService.setCurrentTenant(transaction.tenantId!);
    const entity = await this.entityRepository.findById(
      transaction.entitiesId!,
    );
    await this.emailService.sendWireNotificationEmail(
      transaction.updateEmail!,
      `A Wire Transfer from ${entity.name} is on its way`,
      {
        amount: transaction.amount,
        entity: {
          name: entity.name,
        },
        recipient: {
          email: transaction.updateEmail!,
        },
      },
    );
  }

  async createBookTransaction(
    entityId: string,
    toAccountId: string,
    amount: number,
    description?: string,
  ) {
    const entity = await this.entityRepository.findById(entityId);
    const {currentBalance} = await this.banking.getDepositAccountDetails(
      entity.bankAccount!.providerMeta!.accountId!,
    );
    if (!currentBalance || currentBalance < amount) {
      throw new Error(TransactionServiceErrors.INSUFFICIENT_FUNDS);
    }

    return this.transactionRepository.save(
      new Transaction({
        tenantId: entity.tenantId,
        type: 'BOOK',
        direction: 'DEBIT',
        amount: amount,
        status: 'QUEUED',
        counterpartyId: toAccountId,
        entitiesId: entity.id,
        dealId: entity.dealId,
        description: description,
      }),
    );
  }
  //async processBookQueuedTransactions() {
  //}
}
