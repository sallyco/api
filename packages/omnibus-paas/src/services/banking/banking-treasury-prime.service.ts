import {bind, BindingScope, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  Address,
  BusinessApplicationRequest,
  PersonApplicationRequest,
  BookTransfer,
  WireTransfer,
} from '../../interfaces/services/treasury-prime.interface';
import {Entities as Entity, Company} from '../../models';
import {
  EntitiesRepository,
  ProfileRepository,
  TransactionRepository,
} from '../../repositories';
import {
  AccountACHDetails,
  AccountTransaction,
  AccountTransactionType,
  AccountWireDetails,
  BankAccount,
  Banking,
  CounterPartyAccount,
  DepositAccount,
  DepositAccountApplicationData,
  Paging,
  TransactionBookDetail,
  TransactionACHDetail,
  TransactionACHStatus,
  TransactionFilter,
  TransactionWireDetail,
  TransactionWireStatus,
} from './banking.service';
import {
  TreasuryPrimeService,
  TreasuryPrimeServiceProvider,
} from './treasury-prime.service';

interface TreasuryPrimeDepositAccount extends DepositAccount {
  providerMeta: {
    typeId: 'TREASURYPRIME';
    accountStatus: string;
    accountApplicationId: string;
    [property: string]: unknown;
  };
  createdAt: Date;
}

export interface TreasuryPrimeAccountTransaction extends AccountTransaction {
  subscription_id?: string;
  transaction_type?: 'WIRE' | 'ACH';
  investor_name?: string;
  consolidated_description?: string;
  transaction_data?: string;
}

interface TreasuryPrimeTransactionACHDetail extends TransactionACHDetail {
  providerMeta: {
    typeId: string;
    userdata?: {
      subscriptionId?: string;
    };
  };
}

@bind({scope: BindingScope.TRANSIENT})
export class BankingTreasuryPrimeService implements Banking {
  static typeId: 'TREASURYPRIME';
  typeId = 'TREASURYPRIME';

  constructor(
    @service(TreasuryPrimeServiceProvider)
    protected treasuryPrimeService: TreasuryPrimeService,
    @repository(EntitiesRepository)
    protected entityRepository: EntitiesRepository,
    @repository(TransactionRepository)
    protected transactionRepository: TransactionRepository,
    @repository(ProfileRepository)
    protected profileRepository: ProfileRepository,
  ) {}

  async createEntityBusinessApplication(entity: Entity): Promise<string> {
    if (!entity.ein || entity.ein.length < 1) {
      throw new Error('Entity must have EIN for banking');
    }
    const address: Address = {
      street_line_1: '6510 Millrock Drive',
      street_line_2: 'Suite 400',
      city: 'Salt Lake City',
      state: 'UT',
      postal_code: '84121',
    };

    const BusinessResponse =
      await this.treasuryPrimeService.createBusinessApplication({
        description: entity.name,
        established_on: TreasuryPrimeServiceProvider.convertToISO8601Date(
          entity.createdAt ?? new Date(),
        ),
        incorporation_state: 'DE',
        legal_structure: 'llc',
        naics: '52',
        naics_description: 'Finance and Insurance',
        name: entity.name,
        person_applications: [
          {
            id:
              process.env.TREASURY_PRIME_MS_PRIMARY_PERSON_APPLICATION_ID ??
              'apsn_11g2zt0721wggh',
            roles: ['signer', 'control_person'],
            ownership_percentage: 100,
            title: 'Manager',
          },
        ],
        phone_number: '877-492-7555',
        physical_address: address,
        tin: entity.ein,
      });

    return BusinessResponse.body.id;
  }

  async createTenantBusinessApplication(manager: Company): Promise<string> {
    if (!manager.ein || manager.ein.length < 1) {
      throw new Error('Manager must have EIN for banking');
    }
    if (!manager.address) {
      throw new Error('Manager must have an address');
    }
    if (!manager.stateOfFormation) {
      throw new Error('Manager must have a state of formation');
    }
    const address: Address = {
      street_line_1: manager.address.address1 ?? '',
      street_line_2: manager.address.address2,
      city: manager.address.city,
      state: manager.address.state,
      postal_code: manager.address.postalCode,
    };

    try {
      const BusinessResponse =
        await this.treasuryPrimeService.createBusinessApplication({
          description: manager.name,
          established_on: TreasuryPrimeServiceProvider.convertToISO8601Date(
            manager.createdAt ?? new Date(),
          ),
          incorporation_state: manager.stateOfFormation,
          legal_structure: 'llc',
          naics: '52',
          naics_description: 'Finance and Insurance',
          name: manager.name,
          person_applications: [
            {
              id:
                process.env.TREASURY_PRIME_MS_PRIMARY_PERSON_APPLICATION_ID ??
                'apsn_11g2zt0721wggh',
              roles: ['signer', 'control_person'],
              ownership_percentage: 100,
              title: 'Manager',
            },
          ],
          phone_number: manager.phone ?? '',
          physical_address: address,
          tin: manager.ein,
        });

      return BusinessResponse.body.id;
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  async createBusinessApplication(
    businessApplication: BusinessApplicationRequest,
  ): Promise<string> {
    const BusinessResponse =
      await this.treasuryPrimeService.createBusinessApplication(
        businessApplication,
      );
    return BusinessResponse.body.id;
  }

  async createPersonApplication(
    personApplication: PersonApplicationRequest,
  ): Promise<string> {
    const PersonResponse =
      await this.treasuryPrimeService.createPersonApplication(
        personApplication,
      );
    return PersonResponse.body.id;
  }

  async createEntityDepositAccount(
    entity: Entity,
  ): Promise<TreasuryPrimeDepositAccount> {
    if (entity.bankAccount) {
      throw new Error(`Can't add another bank account to entity ${entity.id}`);
    }
    const BusinessApplicationID = await this.createEntityBusinessApplication(
      entity,
    );
    const AccountApplication =
      await this.treasuryPrimeService.createAccountApplication({
        business_application_id: BusinessApplicationID,
        primary_person_application_id:
          process.env.TREASURY_PRIME_MS_PRIMARY_PERSON_APPLICATION_ID ??
          'apsn_11g2zt0721wggh',
        product: process.env.TREASURY_PRIME_PRODUCT ?? 'glassboard_checking',
        bankdata: {
          account_description: entity.name.substr(0, 36),
        },
      });

    const bankData: TreasuryPrimeDepositAccount = {
      providerMeta: {
        typeId: 'TREASURYPRIME',
        accountStatus: AccountApplication.body.status,
        accountApplicationId: AccountApplication.body.id,
      },
      createdAt: new Date(),
    };
    entity.bankAccount = bankData;
    await this.entityRepository.update(entity);
    return bankData;
  }

  async createTenantFeesAccount(
    manager: Company,
    accountName: string,
  ): Promise<TreasuryPrimeDepositAccount> {
    const BusinessApplicationID = await this.createTenantBusinessApplication(
      manager,
    );
    const AccountApplication =
      await this.treasuryPrimeService.createAccountApplication({
        business_application_id: BusinessApplicationID,
        primary_person_application_id:
          process.env.TREASURY_PRIME_MS_PRIMARY_PERSON_APPLICATION_ID ??
          'apsn_11g2zt0721wggh',
        product: process.env.TREASURY_PRIME_PRODUCT ?? 'glassboard_checking',
        bankdata: {
          account_description: manager.name.substr(0, 36),
        },
      });

    const bankData: TreasuryPrimeDepositAccount = {
      providerMeta: {
        typeId: 'TREASURYPRIME',
        accountStatus: AccountApplication.body.status,
        accountApplicationId: AccountApplication.body.id,
      },
      createdAt: new Date(),
    };
    return bankData;
  }

  async createDepositAccount(
    application: DepositAccountApplicationData,
  ): Promise<TreasuryPrimeDepositAccount> {
    const BusinessApplicationID = await this.createEntityBusinessApplication(
      new Entity({
        ein: application.ein,
        name: application.entityName,
      }),
    );
    const AccountApplication =
      await this.treasuryPrimeService.createAccountApplication({
        business_application_id: BusinessApplicationID,
        primary_person_application_id:
          process.env.TREASURY_PRIME_MS_PRIMARY_PERSON_APPLICATION_ID ??
          'apsn_11g2zt0721wggh',
        product: process.env.TREASURY_PRIME_PRODUCT ?? 'glassboard_checking',
        bankdata: {
          account_description: application.entityName.substr(0, 36),
        },
      });

    const statusMap = {
      pending: 'PENDING',
      open: 'OPEN',
      closed: 'closed',
    };

    return {
      availableBalance: 0,
      currentBalance: 0,
      accountName: application.entityName.substr(0, 36),
      providerMeta: {
        typeId: 'TREASURYPRIME',
        ein: application.ein,
        accountStatus: AccountApplication.body.status,
        accountApplicationId: AccountApplication.body.id,
      },
      status:
        statusMap[AccountApplication.body.status.toUpperCase()] ?? 'PENDING',
      createdAt: new Date(),
    };
  }

  async getDepositAccountDetails(accountId: string): Promise<BankAccount> {
    const {body: account} = await this.treasuryPrimeService.getAccount(
      accountId,
    );
    return {
      accountNumber: account.account_number,
      routingNumber: account.routing_number,
      currentBalance: parseFloat(account.current_balance),
      availableBalance: parseFloat(account.available_balance),
    };
  }

  async createACHCounterParty(
    ACHDetails: AccountACHDetails,
  ): Promise<CounterPartyAccount> {
    try {
      const counterPartyResponse =
        await this.treasuryPrimeService.createCounterparty({
          name_on_account: ACHDetails.nameOnAccount.replace(
            // eslint-disable-next-line no-control-regex
            /[^\x00-\x7F]/g,
            '',
          ),
          ach: {
            account_type:
              ACHDetails.accountType === 'CHECKING' ? 'checking' : 'savings',
            account_number: ACHDetails.accountNumber,
            routing_number: ACHDetails.routingNumber,
          },
        });
      return counterPartyResponse.body;
    } catch (e) {
      throw new Error(e?.response?.body?.error ?? e);
    }
  }

  async createWireCounterParty(
    WireDetails: AccountWireDetails,
  ): Promise<CounterPartyAccount> {
    if (WireDetails.bankAddress.country !== 'US') {
      throw new Error('Only US bank addresses are supported');
    }
    try {
      const counterPartyResponse =
        await this.treasuryPrimeService.createCounterparty({
          name_on_account: WireDetails.nameOnAccount,
          wire: {
            account_number: WireDetails.accountNumber,
            routing_number: WireDetails.routingNumber,
            bank_address: {
              street_line_1: WireDetails.bankAddress.street_1,
              street_line_2: WireDetails.bankAddress.street_2,
              city: WireDetails.bankAddress.city,
              state: WireDetails.bankAddress.region,
              postal_code: WireDetails.bankAddress.postal_code,
            },
            bank_name: WireDetails.bankName,
            address_on_account: {
              street_line_1: WireDetails.addressOnAccount.street_1,
              street_line_2: WireDetails.addressOnAccount.street_2,
              city: WireDetails.addressOnAccount.city,
              state: WireDetails.addressOnAccount.region,
              postal_code: WireDetails.addressOnAccount.postal_code,
            },
          },
        });
      return {...counterPartyResponse.body};
    } catch (e) {
      throw new Error(e?.response?.body?.error ?? e);
    }
  }

  async createACHTransaction(
    accountId: string,
    counterpartyId: string,
    amount: string,
    direction: 'CREDIT' | 'DEBIT',
    idempotenceKey: string,
    description?: string,
    userData?: object,
  ) {
    const achTransactionResponse =
      await this.treasuryPrimeService.createACHTransaction(idempotenceKey, {
        account_id: accountId,
        amount: amount,
        counterparty_id: counterpartyId,
        direction: direction === 'CREDIT' ? 'credit' : 'debit',
        sec_code: 'web',
        userdata: userData,
        description: description,
      });
    return this.convertACHTransactionResponseToTransactionACHDetail(
      achTransactionResponse,
    );
  }

  async getACHTransactionDetail(transactionID: string) {
    const achTransactionResponse =
      await this.treasuryPrimeService.getACHTransaction(transactionID);
    return this.convertACHTransactionResponseToTransactionACHDetail(
      achTransactionResponse,
    );
  }

  public hasACHId(text: string): undefined | string {
    const matches = text.match(/(?:ID NBR: )([0-9a-z]+)$/);
    return matches?.[1] ? 'ach_' + matches[1] : undefined;
  }

  async getDepositAccountTransactions(
    depositAccount: DepositAccount,
    paging: Paging = {pageNumber: 0, pageSize: 100},
    filter?: TransactionFilter,
  ): Promise<TreasuryPrimeAccountTransaction[]> {
    if (!depositAccount.providerMeta?.accountId) {
      throw new Error('Account ID is missing');
    }
    const data = await this.treasuryPrimeService.getAccountTransactions(
      depositAccount.providerMeta.accountId,
      paging.pageSize.toString(),
      paging.pageNumber.toString(),
    );
    let transactions = data.body.data;
    transactions = transactions.map(transaction => {
      transaction.ach_id =
        transaction.ach_id ??
        this.hasACHId((transaction.summary ?? '') + (transaction.desc ?? ''));
      return transaction;
    });
    return Promise.all(
      transactions.map(async transaction => {
        const transactionTransform: TreasuryPrimeAccountTransaction = {
          amount: transaction.amount,
          balance: transaction.balance,
          date: transaction.date,
          desc: transaction.desc,
          id: transaction.id,
          summary: transaction.summary,
          wire: transaction.wire,
          type: transaction.type,
          ach_id: transaction.ach_id,
          providerMeta: {
            ...transaction,
          },
        };
        return transactionTransform;
      }),
    );
  }

  convertStringToAccountTransactionType(
    accountTransactionType: string,
  ): AccountTransactionType {
    return <
      | 'CHARGE'
      | 'DEPOSIT'
      | 'HOLD'
      | 'INTEREST'
      | 'PAYMENT'
      | 'REVERSAL'
      | 'WITHDRAWL'
    >accountTransactionType;
  }

  convertACHTransactionResponseToTransactionACHDetail(transactionResponse) {
    const StatusConvert = (status): TransactionACHStatus => {
      return status.toUpperCase();
    };
    const transactionDetail: TreasuryPrimeTransactionACHDetail = {
      type: 'ACH',
      accountID: transactionResponse.body.account_id,
      amount: parseFloat(transactionResponse.body.amount),
      counterpartyId: transactionResponse.body.counterparty_id,
      createdAt: transactionResponse.body.created_at,
      direction:
        transactionResponse.body.direction === 'debit' ? 'DEBIT' : 'CREDIT',
      id: transactionResponse.body.id,
      status: StatusConvert(transactionResponse.body.status),
      providerMeta: {
        ...transactionResponse.body,
        typeId: 'TREASURYPRIME',
      },
    };
    return transactionDetail;
  }

  async getSettings() {
    return this.treasuryPrimeService.getSettings();
  }

  async getACHCreditLimit() {
    const settings = await this.getSettings();
    return settings.body.ach_limit_credit;
  }
  async getACHDebitLimit() {
    const settings = await this.getSettings();
    return settings.body.ach_limit_debit;
  }

  convertWireToTransaction(wireTransfer: WireTransfer): TransactionWireDetail {
    const statusConvert = (status): TransactionWireStatus => {
      return status.toUpperCase();
    };
    return {
      type: 'WIRE',
      status: statusConvert(wireTransfer.status),
      accountID: wireTransfer.account_id,
      amount: parseFloat(wireTransfer.amount),
      counterpartyId: wireTransfer.counterparty_id,
      createdAt: wireTransfer.created_at ?? '',
      direction: 'CREDIT',
      id: wireTransfer.id ?? '',
      providerMeta: {
        ...wireTransfer,
        typeId: 'TREASURYPRIME',
      },
    };
  }

  async createWireTransfer(
    accountId: string,
    counterpartyId: string,
    amount: string,
    instructions: string,
    idempotenceKey: string,
    userData?: object,
  ) {
    const wireTransferResponse =
      await this.treasuryPrimeService.createWireTransfer(idempotenceKey, {
        account_id: accountId,
        counterparty_id: counterpartyId,
        instructions: instructions,
        amount: amount,
        userdata: userData,
      });
    if (!wireTransferResponse?.body) {
      throw Error('No response body from Treasury Prime after wire creation');
    }

    return this.convertWireToTransaction(wireTransferResponse.body);
  }

  async getWireTransfer(transactionId: string) {
    const response = await this.treasuryPrimeService.getWireTransfer(
      transactionId,
    );
    return this.convertWireToTransaction(response.body);
  }

  async getWireCreditLimit() {
    const settings = await this.getSettings();
    return settings.body.wire_daily_limit;
  }

  convertBookToTransaction(bookTransfer: BookTransfer): TransactionBookDetail {
    const statusConvert = (status): string => {
      return status.toUpperCase();
    };
    return {
      type: 'BOOK',
      status: statusConvert(bookTransfer.status),
      fromAccountId: bookTransfer.from_account_id,
      amount: parseFloat(bookTransfer.amount),
      toAccountId: bookTransfer.to_account_id,
      createdAt: bookTransfer.created_at ?? '',
      updatedAt: bookTransfer.updated_at ?? '',
      id: bookTransfer.id ?? '',
      description: bookTransfer.description,
      providerMeta: {
        ...bookTransfer,
        typeId: 'TREASURYPRIME',
      },
    };
  }

  async createBookTransfer(
    amount: string,
    fromAccountId: string,
    toAccountId: string,
    idempotenceKey: string,
    description?: string,
    userData?: object,
  ) {
    const bookTransferResponse =
      await this.treasuryPrimeService.createBookTransfer(idempotenceKey, {
        amount: amount,
        from_account_id: fromAccountId,
        to_account_id: toAccountId,
        description: description,
        userdata: userData,
      });
    if (!bookTransferResponse?.body) {
      throw Error('No response body from Treasury Prime after book creation');
    }

    return this.convertBookToTransaction(bookTransferResponse.body);
  }

  async getBookTransfer(transactionId: string) {
    const response = await this.treasuryPrimeService.getBookTransfer(
      transactionId,
    );
    return this.convertBookToTransaction(response.body);
  }
}
