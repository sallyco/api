import {
  bind,
  /* inject, */ BindingScope,
  Provider,
  service,
} from '@loopback/core';
import {PlaidService} from './plaid.service';

export interface BankingExternalAccountConnector {
  typeId: string;
}

@bind({scope: BindingScope.TRANSIENT})
export class BankingExternalAccountProvider
  implements Provider<BankingExternalAccountConnector>
{
  constructor(@service(PlaidService) private plaidService: PlaidService) {}

  value(): BankingExternalAccountConnector {
    return this.plaidService;
  }
}
