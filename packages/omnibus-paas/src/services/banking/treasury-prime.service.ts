import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import countrynames from 'countrynames';
import moment from 'moment';
import {TreasuryPrimeDataSource} from '../../datasources';
import {
  AccountApplicationRequest,
  AccountApplicationResponse,
  AccountResponse,
  AccountTransactionResponse,
  ACHTransactionRequest,
  ACHTransactionResponse,
  BookTransfer,
  BusinessApplicationRequest,
  BusinessApplicationResponse,
  CounterpartyRequest,
  CounterpartyResponse,
  ISO_2,
  ISO_8601_Date,
  PaginatedResponse,
  PersonApplicationRequest,
  PersonApplicationResponse,
  RequestResponse,
  SettingsResponse,
  WireTransfer,
} from '../../interfaces/services/treasury-prime.interface';

export interface TreasuryPrimeService {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [methodName: string]: (...args: any[]) => Promise<RequestResponse<any>>;

  createPersonApplication(
    applicationData: PersonApplicationRequest,
  ): Promise<RequestResponse<PersonApplicationResponse>>;

  getPersonApplication(
    id: string,
  ): Promise<RequestResponse<PersonApplicationResponse>>;

  createBusinessApplication(
    applicationData: BusinessApplicationRequest,
  ): Promise<RequestResponse<BusinessApplicationResponse>>;

  getBusinessApplication(
    id: string,
  ): Promise<RequestResponse<BusinessApplicationResponse>>;

  createAccountApplication(
    applicationData: AccountApplicationRequest,
  ): Promise<RequestResponse<AccountApplicationResponse>>;

  getAccountApplication(
    id: string,
  ): Promise<RequestResponse<AccountApplicationResponse>>;

  getAccount(id: string): Promise<RequestResponse<AccountResponse>>;

  getAccountTransactions(
    accountId: string,
    pageSize: string,
    pageCursor: string,
    fromDate?: string,
    toDate?: string,
  ): Promise<RequestResponse<PaginatedResponse<AccountTransactionResponse[]>>>;

  createCounterparty(
    counterpartyData: CounterpartyRequest,
  ): Promise<RequestResponse<CounterpartyResponse>>;

  createACHTransaction(
    idempotenceKey: string,
    achData: ACHTransactionRequest,
  ): Promise<RequestResponse<ACHTransactionResponse>>;

  getACHTransaction(
    id: string,
  ): Promise<RequestResponse<ACHTransactionResponse>>;

  getSettings(): Promise<RequestResponse<SettingsResponse>>;

  createWireTransfer(
    idempotenceKey: string,
    wireData: WireTransfer,
  ): Promise<RequestResponse<WireTransfer>>;

  getWireTransfer(wireId: string): Promise<RequestResponse<WireTransfer>>;

  updateWireTransfer(
    wireData: WireTransfer,
  ): Promise<RequestResponse<WireTransfer>>;

  createBookTransfer(
    idempotenceKey: string,
    bookData: BookTransfer,
  ): Promise<RequestResponse<BookTransfer>>;

  getBookTransfer(bookId: string): Promise<RequestResponse<BookTransfer>>;

  getAllBookTransfers(): Promise<RequestResponse<BookTransfer[]>>;
}

export class TreasuryPrimeServiceProvider
  implements Provider<TreasuryPrimeService>
{
  constructor(
    // TreasuryPrime must match the name property in the datasource json file
    @inject('datasources.TreasuryPrime')
    protected dataSource: TreasuryPrimeDataSource = new TreasuryPrimeDataSource(),
  ) {}

  value(): Promise<TreasuryPrimeService> {
    return getService(this.dataSource);
  }

  static convertToISO8601Date(date: string | Date): ISO_8601_Date {
    const convertedDate = moment(date).format('YYYY-MM-DD');
    if (!convertedDate) {
      throw new Error('Could not resolve date');
    }
    return convertedDate as unknown as ISO_8601_Date;
  }

  static convertToISO_2(countryCode: string): ISO_2 {
    return countrynames.getCode(countryCode) as ISO_2;
  }
}
