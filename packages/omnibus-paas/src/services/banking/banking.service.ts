/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  bind,
  /* inject, */ BindingScope,
  Provider,
  service,
} from '@loopback/core';
import {Entities as Entity, Company} from '../../models';
import {BankingTreasuryPrimeService} from './banking-treasury-prime.service';

export interface Address {
  street_1: string;
  street_2?: string;
  city: string;
  region: string;
  country: string;
  postal_code: string;
}
export interface BankingUser {}
export interface BankingUserIndividual extends BankingUser {}
export interface DepositAccountApplicationData {
  ein: string;
  entityName: string;
}
export interface BankAccount {
  providerMeta?: {
    typeId?: string;
    accountStatus?: string;
    accountApplicationId?: string;
    accountId?: string;
  };
  createdAt?: Date;
  updatedAt?: Date;
  bankName?: string;
  status?: 'OPEN' | 'PENDING' | 'CLOSED';
  bankAddress?: string;
  accountNumber?: string;
  routingNumber?: string;
  accountName?: string;
  availableBalance?: number;
  currentBalance?: number;
}
export interface DepositAccount extends BankAccount {}
export interface CounterPartyAccount {
  id: string;
}

export interface AccountACHDetails {
  nameOnAccount: string;
  accountNumber: string;
  routingNumber: string;
  accountType: 'CHECKING' | 'SAVINGS';
}

export interface AccountWireDetails {
  accountNumber: string;
  routingNumber: string;
  bankAddress: Address;
  addressOnAccount: Address;
  bankName: string;
  nameOnAccount: string;
}

export interface Paging {
  pageSize: number;
  pageNumber: number;
}

interface Filter {
  fromDate?: string;
  toDate?: string;
}
export interface TransactionFilter extends Filter {}

export type AccountTransactionType =
  | 'CHARGE'
  | 'DEPOSIT'
  | 'HOLD'
  | 'INTEREST'
  | 'PAYMENT'
  | 'REVERSAL'
  | 'WITHDRAWL';

export type CounterpartyTransactionType = 'ACH' | 'WIRE';
export interface AccountTransaction {
  amount: number;
  balance: number;
  date: string;
  desc?: string;
  id: string;
  summary?: string;
  type?: AccountTransactionType | string;
  wire?: string;
  wire_id?: string;
  wire_description?: string;
  type_source?: string;
  ach_id?: string;
  trace_id?: string;
  fingerprint?: string;
  billpay_payment_id?: string;
  book_id?: string;
  providerMeta?: {
    achId?: string;
    fingerprint: string;
    wireId?: string;
    checkId?: string;
    checkNumber?: string;
    [x: string]: any;
  };
  subscription_id?: string;
}

export interface CounterpartyTransactionDetail {
  type: CounterpartyTransactionType;
  accountID: string;
  amount: number;
  counterpartyId: string;
  status: string;
  createdAt: string;
  direction: 'CREDIT' | 'DEBIT';
  id: string;
  providerMeta: {
    typeId: string;
  };
}
export type TransactionACHStatus =
  | 'QUEUED'
  | 'PENDING'
  | 'PROCESSING'
  | 'CANCELLED'
  | 'SENT'
  | 'RETURNED';
export interface TransactionACHDetail extends CounterpartyTransactionDetail {
  type: CounterpartyTransactionType;
  status: TransactionACHStatus;
}
export interface TransactionBookDetail {
  type: 'BOOK';
  amount: number;
  createdAt: string;
  updatedAt: string;
  description?: string;
  fromAccountId: string;
  status: string;
  id: string;
  toAccountId: string;
  providerMeta: {
    typeId: string;
  };
}
export type TransactionWireStatus =
  | 'PENDING'
  | 'CANCELLED'
  | 'PROCESSING'
  | 'SENT'
  | 'ERROR';
export interface TransactionWireDetail extends CounterpartyTransactionDetail {
  type: 'WIRE';
  status: TransactionWireStatus;
}

export interface Banking {
  typeId: string;
  createEntityDepositAccount: (entity: Entity) => Promise<DepositAccount>;
  createTenantFeesAccount: (
    manager: Company,
    accountName: string,
  ) => Promise<DepositAccount>;
  createDepositAccount: (
    application: DepositAccountApplicationData,
  ) => Promise<DepositAccount>;
  getDepositAccountDetails: (accountId: string) => Promise<BankAccount>;
  getDepositAccountTransactions: (
    depositAccount: DepositAccount,
    paging?: Paging,
    filter?: TransactionFilter,
  ) => Promise<AccountTransaction[]>;
  createACHCounterParty: (
    ACHDetails: AccountACHDetails,
  ) => Promise<CounterPartyAccount>;
  createWireCounterParty: (
    WireDetails: AccountWireDetails,
  ) => Promise<CounterPartyAccount>;
  createACHTransaction: (
    accountId: string,
    counterpartyId: string,
    amount: string,
    direction: 'CREDIT' | 'DEBIT',
    idempotenceKey: string,
    description?: string,
    userData?: object,
  ) => Promise<TransactionACHDetail>;
  getACHTransactionDetail: (
    transactionID: string,
  ) => Promise<TransactionACHDetail>;
  getACHCreditLimit: () => Promise<number>;
  getACHDebitLimit: () => Promise<number>;

  // Wire related functions
  createWireTransfer: (
    accountId: string,
    counterpartyId: string,
    amount: string,
    instructions: string,
    idempotenceKey: string,
    // intermediary: object,
    userData?: object,
  ) => Promise<TransactionWireDetail>;
  getWireTransfer: (transactionID: string) => Promise<TransactionWireDetail>;
  getWireCreditLimit: () => Promise<number>;
  // Book related functions
  createBookTransfer: (
    amount: string,
    fromAccountId: string,
    toAccountId: string,
    idempotenceKey: string,
    description?: string,
    userData?: object,
  ) => Promise<TransactionBookDetail>;
  getBookTransfer: (transactionID: string) => Promise<TransactionBookDetail>;
  // updateWireTransfer: (
  //   wire: TransactionWireDetail,
  // ) => Promise<TransactionWireDetail>;
}

@bind({scope: BindingScope.TRANSIENT})
export class BankingProvider implements Provider<Banking> {
  constructor(
    @service(BankingTreasuryPrimeService)
    private readonly treasuryPrime: BankingTreasuryPrimeService,
  ) {}

  value(): Banking {
    //TODO: make this tenant-context aware to provide the correct banking service
    return this.treasuryPrime;
  }
}
