import {injectable, BindingScope, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {DealRepository} from '../repositories';
import {CloseService} from './close.service';
import {SubscriptionService, TargetSummary} from './subscription.service';

@injectable({scope: BindingScope.TRANSIENT})
export class DealService {
  constructor(
    @repository(DealRepository)
    private dealRepository: DealRepository,
    @service(SubscriptionService)
    private subscriptionService: SubscriptionService,
    @service(CloseService)
    private closeService: CloseService,
  ) {}

  applyPreviouslyRaisedAmount(
    previousAmount: number,
    targetSummary: TargetSummary,
  ): TargetSummary {
    return {
      ...targetSummary,
      committedAmount: (targetSummary.committedAmount += previousAmount),
      raisedAmount: (targetSummary.raisedAmount += previousAmount),
    };
  }

  async getDealPerformanceSummary(
    dealId,
    {closePerPage = 100, closePageNumber = 0},
  ): Promise<TargetSummary> {
    const deal = await this.dealRepository.findById(dealId);

    let subscriptionSummary =
      await this.subscriptionService.getSubscriptionPerformanceSummary(dealId, {
        closePerPage,
        closePageNumber,
      });

    if (deal?.previouslyRaisedAmount) {
      subscriptionSummary = this.applyPreviouslyRaisedAmount(
        deal.previouslyRaisedAmount,
        subscriptionSummary,
      );
    }

    if (deal?.targetRaiseAmount && subscriptionSummary) {
      subscriptionSummary.targetAmount = deal?.targetRaiseAmount;
    }

    // Return the calculated totals:
    return subscriptionSummary;
  }
}
