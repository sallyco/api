/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  injectable,
  /* inject, */ BindingScope,
  MetadataInspector,
} from '@loopback/core';
import {Entity, Model, repository, WhereBuilder} from '@loopback/repository';
import {WebhookRepository} from '../../repositories';
import {
  WEBHOOK_CONFIG_DECORATOR_KEY,
  WebhookConfigDecoratorMetadata,
} from '../../decorators/webhook-model-name-decorator';
import axios from 'axios';
import debugFactory from 'debug';
import {Webhook} from '../../models';

const debug = debugFactory('omnibus-paas:services:webhook');

export enum WebhookEventTypes {
  'UPDATE' = 'UPDATE',
  'CREATE' = 'CREATE',
  'DELETE' = 'DELETE',
}

@injectable({scope: BindingScope.TRANSIENT})
export class WebhookService {
  constructor(
    @repository(WebhookRepository)
    private readonly webhookRepository: WebhookRepository,
  ) {}

  classOf<T>(o: T): any {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return o.constructor;
  }

  async processWebhookUpdateForObject(
    object: Entity,
    eventType: WebhookEventTypes,
  ) {
    const tenant = object['tenantId'];
    if (!tenant) {
      return;
    }

    const webhookConfig: WebhookConfigDecoratorMetadata | undefined =
      MetadataInspector.getClassMetadata(
        WEBHOOK_CONFIG_DECORATOR_KEY,
        this.classOf(object),
        {
          ownMetadataOnly: true,
        },
      );
    if (!webhookConfig) {
      return;
    }

    const webhookFilter = new WhereBuilder({
      tenantId: tenant,
      'resources.id': webhookConfig.resourceId,
      'resources.events': eventType,
    });
    const webhooks = await this.webhookRepository.find(webhookFilter);

    if (!webhooks || webhooks.length < 1) {
      return;
    }

    for (const webhook of webhooks) {
      await this.sendWebhookUpdate(
        webhook,
        String(webhookConfig.resourceId),
        object,
        eventType,
      );
    }
  }

  private async sendWebhookUpdate(
    webhookData: Webhook,
    resourceId: string,
    data: Model,
    eventType: WebhookEventTypes,
  ) {
    debug('Sending Webhook Update For', webhookData.url, data, eventType);
    try {
      // eslint-disable-next-line no-void
      await axios.post(
        webhookData.url,
        {
          data: data,
          eventType: eventType,
          resourceId: resourceId,
        },
        {
          headers: {
            'X-Webhook-Token': webhookData.token,
          },
          timeout: 1000,
        },
      );
    } catch (e) {
      debug(`Couldn't send webhook update to ${webhookData.url}`, e);
    }
  }
}
