import {injectable, /* inject, */ BindingScope, Provider} from '@loopback/core';
import {initialize, Unleash as UnleashInstance} from 'unleash-client';

export type Unleash = UnleashInstance;

@injectable({scope: BindingScope.APPLICATION})
export class UnleashProvider implements Provider<Unleash> {
  private readonly unleash: Unleash;
  constructor() {
    this.unleash = initialize({
      url:
        process.env.FF_ENDPOINT ??
        'https://gitlab.gbt.sh/api/v4/feature_flags/unleash/55',
      appName: process.env.ENVIRONMENT ?? 'local',
      instanceId: process.env.FF_INSTANCE_ID ?? 'XGg5ZC3x4QMWChutkxD5',
      environment: process.env.ENVIRONMENT ?? 'local',
    });
  }
  value() {
    return this.unleash;
  }
}
