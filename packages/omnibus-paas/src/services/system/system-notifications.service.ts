import {injectable, /* inject, */ BindingScope, service} from '@loopback/core';
import {Entities, Profile, Transaction} from '../../models';
import {MessageAttachment} from '@slack/web-api';
import {Unleash, UnleashProvider} from './unleash.service';
import {ILegalInc} from '../../interfaces/services/legal-inc.interface';
import Tax = ILegalInc.API.Tax;

const {WebClient} = require('@slack/web-api');

@injectable({scope: BindingScope.TRANSIENT})
export class SystemNotificationsService {
  private readonly SLACK_TOKEN: string;
  private readonly SLACK_CHANNEL: string;
  private readonly slack: typeof WebClient;

  constructor(@service(UnleashProvider) private readonly unleash: Unleash) {
    this.SLACK_TOKEN =
      process.env.SLACK_TOKEN ??
      'xoxb-890098323541-2152232592004-hvzYo2f5nKYcx5PpWWNpFlEX';
    this.SLACK_CHANNEL = process.env.SLACK_CHANNEL ?? 'G01CZQ40D35';
    this.slack = new WebClient(this.SLACK_TOKEN);
  }

  async Notify_Failed_KYC_AML_Check({id, kycAml, tenantId}: Profile) {
    const URL = kycAml?.providerMeta?.results_uri
      ? kycAml.providerMeta.results_uri
      : 'No URL Generated';
    return this.sendSlackMessage(
      `A Profile (ID ${id} on Tenant ${tenantId}) has failed the KYC/AML check`,
      [
        {
          text: `Onfido Results: ${URL}`,
        },
      ],
    );
  }

  async Notify_Transaction_Exception(transaction: Transaction) {
    return this.sendSlackMessage(
      `A Transaction (ID ${transaction.id} on Tenant ${transaction.tenantId}) is in EXCEPTION state`,
      [
        {
          text: `Reason: ${transaction.reason}`,
        },
      ],
    );
  }

  async Notify_Transaction_Approval_Needed(transaction: Transaction) {
    return this.sendSlackMessage(
      `A Transaction (ID ${transaction.id} on Tenant ${transaction.tenantId}) is waiting for approval`,
      [
        {
          text: `Amount: ${transaction.amount} (${transaction.type})`,
        },
      ],
    );
  }

  async Notify_Failed_Entity_EIN_Order(entity: Entities) {
    return this.sendSlackMessage(
      `An Entity EIN Order has failed (ID ${entity.id} on Tenant ${entity.tenantId})`,
    );
  }

  async Notify_Successful_Entity_EIN_Order(entity: Entities, taxContact: Tax) {
    return this.sendSlackMessage(
      `An Entity EIN Order for ${entity.name} has been placed (ID ${entity.id} on Tenant ${entity.tenantId})`,
      [
        {
          text: `Tax Contact Name: ${taxContact.first_name} ${taxContact.last_name}`,
        },
      ],
    );
  }

  async sendSlackMessage(text: string, attachments?: MessageAttachment[]) {
    console.log('System Alert', text, attachments);
    if (
      this.unleash.isEnabled('system_notifications_in_slack', {
        userId: 'gbt',
      })
    ) {
      await this.slack.chat.postMessage({
        text: text,
        attachments: attachments,
        channel: this.SLACK_CHANNEL,
      });
    }
  }
}
