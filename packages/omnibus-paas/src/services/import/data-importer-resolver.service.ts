import {injectable, /* inject, */ BindingScope, service} from '@loopback/core';
import {GbtLegacyDataImporterService} from './gbt-legacy-data-importer.service';
import {
  AllowedModels,
  DataImporterService,
  IMPORTERS,
} from '../../abstracts/data-importer';

@injectable({scope: BindingScope.TRANSIENT})
export class DataImporterResolverService {
  constructor(
    @service(GbtLegacyDataImporterService)
    private gbtLegacyDataImporterService: GbtLegacyDataImporterService,
  ) {}

  resolveImporter(
    type: IMPORTERS = IMPORTERS.GBT_LEGACY,
  ): DataImporterService<AllowedModels> {
    switch (type) {
      case IMPORTERS.GBT_LEGACY:
        return this.gbtLegacyDataImporterService;
    }
    return this.gbtLegacyDataImporterService;
  }
}
