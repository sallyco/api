import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import {
  AllowedModels,
  DataImporterService,
  IMPORTERS,
} from '../../abstracts/data-importer';

@injectable({scope: BindingScope.TRANSIENT})
export class GbtLegacyDataImporterService extends DataImporterService<AllowedModels> {
  type = IMPORTERS.GBT_LEGACY;

  convertDataToModel(data: object[]): Partial<AllowedModels>[] {
    return [];
  }
}
