import {injectable, BindingScope, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Subscription} from 'models';
import {ClosesList} from 'models/closes-list.model';
import {SubscriptionRepository} from '../repositories';
import {CloseService} from './close.service';

export interface TargetSummary {
  targetAmount: number;
  raisedAmount: number;
  committedAmount: number;
}

@injectable({scope: BindingScope.TRANSIENT})
export class SubscriptionService {
  constructor(
    /* Add @inject to inject parameters */
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @service(CloseService)
    private readonly closeService: CloseService,
  ) {}

  getSubscriptionsByDealId(dealId: string, where: object = {}) {
    const subscriptions = this.subscriptionRepository.find({
      where: {
        dealId: dealId,
        ...where,
      },
    });

    return subscriptions;
  }

  async getSubscriptionPerformanceSummary(
    dealId,
    {closePageNumber, closePerPage},
  ) {
    const subscriptions = await this.getSubscriptionsByDealId(dealId);
    const dealCloses = await this.closeService.getClosesByDeal(dealId, {
      page: closePageNumber,
      perPage: closePerPage,
    });

    const subscriptionSummary = this.buildSubscriptionFinancialSummary(
      subscriptions,
      dealCloses,
    );

    return subscriptionSummary;
  }

  // Determine if the amount should be considered as "raised"
  // (isDocsSigned and amount > 0 and )
  // If it is included in a close object it is counted
  subscriptionIsRaised(subscription: Subscription, closesLookup: string[]) {
    return (
      subscription?.isDocsSigned &&
      subscription?.amount &&
      closesLookup.includes(subscription.id)
    );
  }

  // Determine if the amount is "committed"
  // Business rule:
  // - If it is signed
  // - is a positive amount
  // - is NOT cancelled
  subscriptionIsCommitted(subscription: Subscription) {
    const notComittedStatuses = ['CANCELLED', 'REFUNDED', 'TRANSFERRED'];
    return (
      subscription?.isDocsSigned &&
      subscription?.amount &&
      !notComittedStatuses.includes(subscription.status)
    );
  }

  applyRaisedAmountToResult(
    amount: number,
    targetSummary: TargetSummary,
  ): TargetSummary {
    return {
      ...targetSummary,
      raisedAmount: (targetSummary.raisedAmount += amount),
      committedAmount: (targetSummary.committedAmount += amount),
    };
  }

  applyCommittedAmountToResult(
    amount: number,
    targetSummary: TargetSummary,
  ): TargetSummary {
    return {
      ...targetSummary,
      committedAmount: (targetSummary.committedAmount += amount),
    };
  }

  buildSubscriptionFinancialSummary(
    subscriptions: Subscription[],
    dealCloses: ClosesList,
  ): TargetSummary {
    // Create a single lookup for subscriptions
    const closesLookup: string[] = [];
    dealCloses.data?.forEach(close => {
      close.subscriptions?.forEach(sub => {
        closesLookup.push(sub);
      });
    });

    const summary: TargetSummary = subscriptions.reduce(
      (summaryObject: TargetSummary, sub) => {
        if (this.subscriptionIsRaised(sub, closesLookup)) {
          return this.applyRaisedAmountToResult(sub.amount!, summaryObject);
        }

        if (this.subscriptionIsCommitted(sub)) {
          return this.applyCommittedAmountToResult(sub.amount!, summaryObject);
        }

        // No additional "interesting" totals
        return summaryObject;
      },
      {
        targetAmount: 0,
        committedAmount: 0,
        raisedAmount: 0,
      },
    );

    return summary;
  }
}
