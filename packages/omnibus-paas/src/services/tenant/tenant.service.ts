import {BindingScope, Context, inject, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Tenant} from '../../models';
import {MultiTenancyBindings} from '../../multi-tenancy';
import {TenantRepository} from '../../repositories';
import {RestBindings} from '@loopback/rest';
import {Request} from 'express-serve-static-core';

@injectable({scope: BindingScope.TRANSIENT})
export class TenantService {
  constructor(
    @inject.context() private ctx: Context,
    @repository(TenantRepository) public tenantRepository: TenantRepository,
    @inject(RestBindings.Http.REQUEST, {optional: true})
    private request: Request | undefined = undefined,
  ) {}

  async getCurrentHydratedTenant(): Promise<Tenant> {
    const currentTenantId = await this.resolveTenantId();
    return this.tenantRepository.findById(currentTenantId);
  }

  async resolveTenantId(): Promise<string> {
    const tenant = await this.ctx.get(MultiTenancyBindings.CURRENT_TENANT);
    if (this.request && tenant.id === 'master') {
      const tenantOverride = this.request?.header('x-tenant-override');
      if (tenantOverride) {
        await this.setCurrentTenant(tenantOverride);
        return tenantOverride;
      }
    }
    return tenant.id;
  }

  async setCurrentTenant(tenantId: string): Promise<void> {
    this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({
      id: String(tenantId),
    });
  }
}
