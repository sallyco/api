/* eslint-disable @typescript-eslint/no-explicit-any */
import {bind, BindingScope, inject, service} from '@loopback/core';
import debugFactory from 'debug';
import {MinIOServiceBindings} from '../../keys';
import {MinIOService} from './minio.service';
import {Readable as ReadableStream} from 'stream';
import {v4 as uuidv4} from 'uuid';
import fs from 'fs';
import AdmZip from 'adm-zip';
import jsmart from 'jsmart';
import pdfFiller from 'pdffiller';
import muhammara from 'muhammara';
import path from 'path';
import gm from 'gm';
import {HttpErrors} from '@loopback/rest';
import * as Convert from '../../utils/documentUtils/docxToPdf';
import {File} from '../../models';
import {repository} from '@loopback/repository';
import {
  DealRepository,
  EntitiesRepository,
  FileRepository,
  ProfileRepository,
  SubscriptionRepository,
} from '../../repositories';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../../multi-tenancy';

const extractText = require('../../utils/signingUtils/lib/text-extraction');

import {PDFDocument, PDFTextField, PDFCheckBox} from 'pdf-lib';
import {BucketItemStat} from 'minio';
import {DocumentsGeneratorService} from './documents-generator.service';
import moment from 'moment';

const debug = debugFactory('omnibus-paas:service:documentsService');

const TEMP_DIR = '/tmp';

@bind({scope: BindingScope.TRANSIENT})
export class DocumentsService {
  private minio: MinIOService;

  constructor(
    @inject(MinIOServiceBindings.MINIO_SERVICE) minio: MinIOService,
    @repository(FileRepository) public fileRepository: FileRepository,
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
    @repository(DealRepository) public dealRepository: DealRepository,
    @repository(EntitiesRepository)
    public entitiesRepository: EntitiesRepository,
    @service(DocumentsGeneratorService)
    public documentsGeneratorService: DocumentsGeneratorService,
  ) {
    this.minio = minio;
  }

  deleteFolderRecursive(folder) {
    if (fs.existsSync(folder)) {
      fs.readdirSync(folder).forEach((file, index) => {
        const curPath = path.join(folder, file);
        if (fs.lstatSync(curPath).isDirectory()) {
          // recurse
          this.deleteFolderRecursive(curPath);
        } else {
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(folder);
    }
  }

  base64Encode(file) {
    // read binary data
    const bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
  }

  populateDocxTemplate(
    dictionaryData: object,
    templateFilePath: string,
    outputFilePath: string,
    tempDir: string,
  ) {
    // TODO (Andrew) deliminaters seemed to be important in the original
    // version of smarty template usage, they don't seem to be important anymore
    // let delimitorsDefined;
    // if (delimiters === '{}') {
    //   delimitorsDefined = {delimiters: {start: '{', end: '}'}};
    // } else if (delimiters === '{{}}') {
    //   delimitorsDefined = {delimiters: {start: '{{', end: '}}'}};
    // } else if (delimiters === '[]') {
    //   delimitorsDefined = {delimiters: {start: '[', end: ']'}};
    // } else if (delimiters === '{[[]]') {
    //   delimitorsDefined = {delimiters: {start: '[[', end: ']]'}};
    // } else if (delimiters === '**') {
    //   delimitorsDefined = {delimiters: {start: '**', end: '**'}};
    // }
    try {
      //imports
      debug('templateFilePath', templateFilePath);

      // open the docx as an archive extract the contents
      debug('outputFilePath', outputFilePath);

      const extractPath = path.join(tempDir, uuidv4()); // the files gotta go somewhere using tmp
      debug('extractPath', extractPath);

      let zip = new AdmZip(templateFilePath); // open docx as an archive
      zip.extractAllTo(extractPath, true);

      const targetFilePath = path.join(extractPath, 'word', 'document.xml'); // the files gotta go somewhere using tmp
      debug('targetFilePath', outputFilePath);

      // peform the merge with jsmart
      const tpl = fs.readFileSync(targetFilePath, {
        encoding: 'utf-8',
      });
      const compiledTemplate = new jsmart(tpl);
      const output = compiledTemplate.fetch(dictionaryData);

      // write the results
      fs.writeFileSync(targetFilePath, output);

      // rebuild the docx
      zip = new AdmZip();
      zip.addLocalFolder(extractPath);
      zip.writeZip(outputFilePath);

      this.deleteFolderRecursive(extractPath.toString());

      return outputFilePath;
    } catch (err) {
      debug('error', err);
      throw err;
    }
  }

  async createThumbnailJpeg(fileName: string) {
    return new Promise<void>(function (resolve, reject) {
      gm(`/tmp/${fileName}.pdf`)
        .setFormat('jpg')
        .resize(400) // Resize to fixed 200px width, maintaining aspect ratio
        .quality(75) // Quality from 0 to 100
        .write(`/tmp/${fileName}.jpeg`, function (error: any) {
          if (!error) {
            resolve();
          } else {
            reject(error);
          }
        });
    });
  }

  async createThumbnailPng(fileName: string) {
    return new Promise<void>(function (resolve, reject) {
      gm(`/tmp/${fileName}.pdf`).thumb(
        400, // Width
        517, // Height
        `/tmp/${fileName}.png`, // Output file name
        75, // Quality from 0 to 100
        function (error, stdout, stderr, command) {
          if (!error) {
            resolve();
          } else {
            reject(error);
          }
        },
      );
    });
  }

  async uploadFileToBucket(
    inputFileName: string,
    outputFileName: string,
    contentType: string,
  ) {
    const fileStream = fs.createReadStream(inputFileName);
    const fileUUID = uuidv4();
    /* eslint-disable no-control-regex */
    await this.minio.putObject(fileUUID, fileStream, {
      'Content-Type': contentType,
      'Content-Disposition':
        'attachment; filename=' +
        outputFileName
          .replace(/\s\s+/g, ' ')
          .replace(/[^\x00-\x7F]/g, '')
          .trim(),
    });
    return fileUUID;
  }

  async uploadJpegToBucket(fileName: string) {
    return this.uploadFileToBucket(fileName, fileName, 'application/jpeg');
  }

  async getDocumentFromBucket(key: string): Promise<ReadableStream> {
    return this.minio.getObject(key);
  }

  async getDocumentStats(key: string): Promise<BucketItemStat> {
    return this.minio.statObject(key);
  }

  async getDocumentUrl(key: string) {
    return this.minio.presignedGetObject(key);
  }

  async populatePdfTemplate(
    template: string,
    dataDictionary: any,
    contentDispositionName: string,
    signatureFile?: string,
    isGenerated = false,
  ) {
    try {
      debug('template', template);
      const fileName = uuidv4();
      const outputFilePath = path.join('/tmp', fileName) + '.pdf';

      if (!isGenerated) {
        const filePath = path.join(
          process.cwd(),
          'src',
          'documentTemplates',
          template,
        );

        debug('filePath', filePath);
        debug('outputFilePath', outputFilePath);
        debug('dictionaryData', dataDictionary);

        const formPdfBytes = fs.readFileSync(filePath);
        const pdfDoc = await PDFDocument.load(formPdfBytes);
        const form = pdfDoc.getForm();

        //fields
        const fields = form.getFields();
        fields.forEach(field => {
          const fieldType = field.constructor.name;
          const name = field.getName();

          if (name in dataDictionary) {
            if (fieldType === 'PDFTextField') {
              // pdf-lib fill requires no null values
              (field as PDFTextField).setText(dataDictionary[name] ?? '');
            } else if (
              fieldType === 'PDFCheckBox' &&
              dataDictionary[name] === 'Yes'
            ) {
              (field as PDFCheckBox).check();
            }

            field.enableReadOnly();
          }
        });

        //signature
        const signBytes = fs.readFileSync(
          signatureFile
            ? signatureFile
            : `./src/utils/signingUtils/signhere.png`,
        );
        const signImage = await pdfDoc.embedPng(signBytes);
        const signField = form.getTextField(
          dataDictionary['memberIndividual'] !== ''
            ? 'memberIndividual'
            : 'memberEntity',
        );
        signField.setImage(signImage);

        //save
        const pdfBytes = await pdfDoc.save();
        fs.writeFileSync(outputFilePath, pdfBytes);
      } else {
        const SATemplate =
          await this.documentsGeneratorService.buildSubscriptionAgreement(
            dataDictionary,
          );
        const pdfBytes = await SATemplate.save();
        fs.writeFileSync(outputFilePath, pdfBytes);
      }
      await this.minio.fputObject(fileName, outputFilePath, {
        'Content-Type': 'application/pdf',
        'Content-Disposition':
          'attachment; filename=' +
          contentDispositionName
            .replace(/\s\s+/g, ' ')
            .replace(/[^\x00-\x7F]/g, '')
            .trim(),
      });
      return fileName;
    } catch (err) {
      debug('Error ', err);
      const error = {error: err};
      return error;
    }
  }

  async oldPopulatePdfTemplate(
    template: string,
    dictionaryData: any,
    contentDispositionName: string,
    signatureFile?: string,
  ) {
    try {
      console.log('Populating...');
      //var content = "";
      const filePath = path.join(
        process.cwd(),
        'src',
        'documentTemplates',
        template,
      );

      const fileName = uuidv4();
      const outputFilePath = path.join('/tmp', fileName) + '.pdf';

      console.log(filePath);
      console.log(outputFilePath);
      console.log(dictionaryData);
      await new Promise<void>((resolve, reject) => {
        pdfFiller.fillFormWithOptions(
          filePath,
          outputFilePath,
          dictionaryData,
          true,
          '/tmp',
          function (err: any) {
            if (err) reject(err);
            console.log('FIlling PDF form done');
            resolve();
          },
        );
      });

      console.log('whitespace generating...');

      let pdfWriter = muhammara.createWriterToModify(outputFilePath, {
        modifiedFilePath: path.join('/tmp', fileName) + '.pdf',
      });
      let pdfReader = muhammara.createReader(outputFilePath);
      let pagesPlacements = extractText(pdfReader);

      const roleTags = '**memberIndividual**';

      try {
        let totalPlacements = 0;
        for (let i = 0; i < pagesPlacements.length; ++i) {
          const pageModifier = new muhammara.PDFPageModifier(pdfWriter, i);
          const cxt = pageModifier.startContext().getContext();
          pagesPlacements[i].forEach((placement: any) => {
            if (placement.text === roleTags) {
              const x = placement.localBBox[0] + 2;
              const y = placement.localBBox[1] + 2;
              const pHeight = 1;
              const pWidth = 1;
              cxt.q();
              console.log(placement);
              cxt.cm = {...placement.matrix, ...cxt};
              cxt.drawRectangle(x, y, pWidth, pHeight, {
                color: 'White',
                type: 'fill',
              });
              cxt.Q();
              totalPlacements++;
            }
          });
          pageModifier.endContext().writePage();
        }
        pdfWriter.end();
        console.log('whitespace skipped - modifying tags');
        console.log('totalPlacements ' + totalPlacements);

        //        if (totalPlacements > 1){
        //            await replaceTags(outputKeyShort, signerRole, totalPlacements);
        //            dbman.placementsUpdate(outputKeyShort, totalPlacements);
        //        }

        //        fs.copyFileSync(`${tempPath}/${outputKeyShort}.pdf`, `${tempPath}/mod/${outputKeyShort}.pdf`);
        //        await imageWrite(inputKey, outputBucket, outputKey, signerRole, inputKeyShort, outputKeyShort,res, sen  dRes, signer2Role,totalPlacements, displayName, assignedUuid);
      } catch (err) {
        console.log('Error ' + err);
        const error = {error: err};
        return error;
      }

      console.log('image adding...');
      const fileToRun = path.join('/tmp', fileName) + '.pdf';
      pdfReader = muhammara.createReader(fileToRun);
      pagesPlacements = extractText(pdfReader);
      pdfWriter = muhammara.createWriterToModify(fileToRun, {
        modifiedFilePath: path.join('/tmp', fileName) + '.pdf',
      });

      // const signer1Flag = 0;
      // const signer2Flag = 0;

      console.log('///////SIGNER ROLE//////');
      //    console.log("roleTags: "+signerRole+" signer2Role: "+signer2Role +" placements " + totalPlacements);
      console.log('///////SIGNER ROLE//////');

      //    if (totalPlacements > 1) {
      //        try {
      //            for(let i = 0; i < pagesPlacements.length; ++i) {
      //                var formXObject = pdfWriter.createFormXObjectFromPNG(`./signhere.png`);
      //                var pageModifier = new muhammara.PDFPageModifier(pdfWriter,i);
      //                var cxt = pageModifier.startContext().getContext();
      //                pagesPlacements[i].forEach((placement)=> {
      //                    for(let j = 0; j < totalPlacements; j++){
      //                        roleTags = "**" + signerRole +j+"**";
      //                        if (placement.text == roleTags){
      //
      //                            signer1Flag = 1;
      //                            cxt.q();
      //                            //width, rotation angel ,rotation angel, height, left, bottom
      //                            cxt.cm(0.10,0,0,0.10,placement.globalBBox[0],placement.globalBBox[1]);
      //                            cxt.doXObject(formXObject);
      //                            cxt.Q();
      ////                            dbman.signerIdUpdate(outputKeyShort);
      //                        }
      //
      //                        if (placement.text == "**"+signer2Role+"**"){
      //                            signer2Flag = 1;
      // //                           dbman.signerId2Update(outputKeyShort);
      //                        }
      //                    }
      //                });
      //
      //                pageModifier.writePage();
      //            }
      //            pdfWriter.end();
      //            console.log("images on PDF added");
      //        } catch (err) {
      //            console.log('Error ' + err);
      //        }
      //    } else {
      try {
        for (let i = 0; i < pagesPlacements.length; ++i) {
          console.log(signatureFile);
          const formXObject = pdfWriter.createFormXObjectFromPNG(
            signatureFile
              ? signatureFile
              : `./src/utils/signingUtils/signhere.png`,
          );
          const pageModifier = new muhammara.PDFPageModifier(pdfWriter, i);
          const cxt = pageModifier.startContext().getContext();
          pagesPlacements[i].forEach(placement => {
            if (placement.text === roleTags) {
              // signer1Flag = 1;
              cxt.q();
              //width, rotation angel ,rotation angel, height, left, bottom
              cxt.cm(
                0.1,
                0,
                0,
                0.1,
                placement.globalBBox[0],
                placement.globalBBox[1],
              );
              cxt.doXObject(formXObject);
              cxt.Q();
              //                      dbman.signerIdUpdate(outputKeyShort);
            }

            //if (placement.text == "**"+signer2Role+"**") {
            //    signer2Flag = 1;
            //               //      dbman.signerId2Update(outputKeyShort);
            //}
          });

          pageModifier.writePage();
        }
        pdfWriter.end();
        console.log('image on PDF added');
      } catch (err) {
        console.log('Error ' + err);
        const error = {error: err};
        return error;
      }
      //    }

      console.log('link adding...');
      //    fileToRun = outputFilePath;
      pdfReader = muhammara.createReader(fileToRun);
      pagesPlacements = extractText(pdfReader);
      pdfWriter = muhammara.createWriterToModify(fileToRun, {
        modifiedFilePath: path.join('/tmp', fileName) + '.pdf',
      });

      let x = 0;
      let y = 0;
      let z = 0;
      let w = 0;

      //    if (totalPlacements>1) {
      //        var signUuid = assignedUuid;
      //        try {
      //            for(let i = 0; i < pagesPlacements.length; ++i) {
      //                let pageModifier = new muhammara.PDFPageModifier(pdfWriter,i);
      //                let cxt = pageModifier.startContext().getContext();
      //                pagesPlacements[i].forEach((placement)=> {
      //                    for(let j = 0; j < totalPlacements; j++) {
      //                        roleTags = "**" + signerRole +j+"**";
      //
      //                        if (placement.text == roleTags) {
      //                            cxt.q();
      //                            x = placement.globalBBox[0];
      //                            y = placement.globalBBox[1];
      //                            z = placement.globalBBox[0]+110;
      //                            w = placement.globalBBox[1]+30;
      //                            console.log(x);
      //                            cxt.Q();
      //                            //(left,bottom,right,top)
      //                            pageModifier.endContext().attachURLLinktoCurrentPage(`https://${config.ServerIP}/server/routes/signing-route/web/signature_pad.html?iKS=${inputKeyShort}&oKS=${outputKeyShort}&sR=${signerRole}&oB=${outputBucket}&sN=${j}&tP=${totalPlacements}&dN=${displayName}&suuid=${signUuid}`, x, y, z, w);
      //                        }
      //                    }
      //                });
      //
      //                pageModifier.writePage();
      //            }
      //        pdfWriter.end();
      //        console.log("link added");
      //        dbman.signUuidUpdate(outputKeyShort, assignedUuid);
      //        webPageGenerate(outputKeyShort, res, sendRes, signerRole, outputKey, outputBucket, displayName, totalPlacements, assignedUuid);
      //        } catch (err) {
      //            console.log('Error ' + err);
      //        }
      //    } else {
      //var signUuid = assignedUuid;
      try {
        for (let i = 0; i < pagesPlacements.length; ++i) {
          const pageModifier = new muhammara.PDFPageModifier(pdfWriter, i);
          const cxt = pageModifier.startContext().getContext();
          pagesPlacements[i].forEach(placement => {
            if (placement.text === roleTags) {
              cxt.q();
              x = placement.globalBBox[0];
              y = placement.globalBBox[1];
              z = placement.globalBBox[0] + 110;
              w = placement.globalBBox[1] + 30;
              console.log(x);
              cxt.Q();
              //(left,bottom,right,top)
              //pageModifier.endContext().attachURLLinktoCurrentPage(`https://${config.ServerIP}/server/routes/signing-route/web/signature_pad.html?iKS=${inputKeyShort}&oKS=${outputKeyShort}&sR=${signerRole}&oB=${outputBucket}&sN=0&tP=1&dN=${displayName}&suuid=${signUuid}`, x, y, z, w);
              pageModifier
                .endContext()
                .attachURLLinktoCurrentPage(`#signature-pad`, x, y, z, w);
            }
          });
          pageModifier.writePage();
        }
        pdfWriter.end();
        console.log('link added');

        /* eslint-disable no-control-regex */
        await this.minio.fputObject(fileName, outputFilePath, {
          'Content-Type': 'application/pdf',
          'Content-Disposition':
            'attachment; filename=' +
            contentDispositionName
              .replace(/\s\s+/g, ' ')
              .replace(/[^\x00-\x7F]/g, '')
              .trim(),
        });

        return fileName;
        //dbman.signUuidUpdate(outputKeyShort, assignedUuid);
        //webPageGenerate(outputKeyShort, res, sendRes, signerRole, outputKey, outputBucket, displayName, totalPlacements, assignedUuid);
      } catch (err) {
        console.log('Error ' + err);
        const error = {error: err};
        return error;
      }
      //}
    } catch (err) {
      const error = {error: err};
      return error;
    }
  }

  async generateDocumentForEntity(
    entityId: string,
    documentType: string,
    documentData: any,
    profile: UserProfile,
    tenant: Tenant,
    existingFileId?: string,
  ) {
    if (!['oa', 'sub', 'ppm'].includes(documentType)) {
      throw new HttpErrors.NotFound('documentType not supported');
    }
    const typeNames = {
      oa: 'Operating Agreement',
      sub: 'Subscription Agreement',
      ppm: 'Private Placement Memorandum',
    };

    let typePrefix = 'llc';
    if (documentData.entityType === 'LIMITED_PARTNERSHIP') {
      typePrefix = 'lp';
    }

    const templateFileName = `${typePrefix}-${documentType}.docx`;

    let templateFilePath = path.join(
      process.cwd(),
      'src',
      'documentTemplates',
      typePrefix + (documentData.regulationType === 'REGULATION_S' ? '-s' : ''),
    );

    if (fs.existsSync(path.join(templateFilePath, tenant.id))) {
      templateFilePath = path.join(templateFilePath, tenant.id);
    }
    templateFilePath = path.join(templateFilePath, templateFileName);

    debug('templateFilePath', templateFilePath);

    const workingFilePath = path.join(TEMP_DIR, templateFileName);

    let outputFileName = `${documentData.dealName} - ${typeNames[documentType]}.docx`;
    debug('outputFileName', outputFileName);

    this.populateDocxTemplate(
      documentData.dictionaryData,
      templateFilePath,
      workingFilePath,
      TEMP_DIR,
    );

    debug('Merge Success', outputFileName);
    let s3Object;
    if (documentData.fileType === 'pdf') {
      // convert to PDF
      await Convert.convertToPdf(
        `${typePrefix}-${documentType}`,
        '.docx',
        '.pdf',
      );
      debug('Conversion Success', outputFileName);

      // Save pdf to s3
      s3Object = await this.uploadFileToBucket(
        path.join(TEMP_DIR, `${typePrefix}-${documentType}.pdf`),
        `${documentData.dealName} - ${typeNames[documentType]}.pdf`,
        'application/pdf',
      );
      outputFileName = `${documentData.dealName} - ${typeNames[documentType]}.pdf`;
      debug('Uploaded to S3', s3Object);
    } else {
      // Save the document to s3
      s3Object = await this.uploadFileToBucket(
        workingFilePath,
        outputFileName,
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      );
      debug('Uploaded to S3', s3Object);
    }

    const fileData = {
      name: outputFileName,
      ownerId: profile.id,
      tenantId: tenant.id,
      type: documentData.fileType,
      key: s3Object,
      lastModified: Date.now().toString(),
    };
    if (existingFileId) {
      const fileItem = await this.fileRepository.findById(existingFileId);
      const fileItemObject = {
        ...fileItem.toObject(),
        ...fileData,
        generating: false,
      };
      return this.fileRepository.updateById(existingFileId, fileItemObject);
    }
    //create new File Object instance
    const fileObj = new File(fileData);

    // save new file objects to DB
    // Get a url of the generated document from S3
    return this.fileRepository.create(fileObj);
  }

  async generateSigningDocument(
    documentType: string,
    subscriptionId: string,
    documentData: any,
    userProfile: UserProfile,
    tenant: Tenant,
    existingFileId?: string,
  ) {
    if (!['oa', 'sub', 'sub-s', 'w8ben', 'w9'].includes(documentType)) {
      throw new HttpErrors.NotFound('documentType not supported');
    }

    const typeNames = {
      oa: 'Operating Agreement',
      sub: 'Subscription Agreement',
      'sub-s': 'Subscription Agreement',
      w8ben: 'W-8BEN',
      w9: 'W-9',
    };

    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );
    // Check if this is a Joint Profile
    let isGeneratedDocument = false;
    const profile = await this.profileRepository.findById(
      subscription.profileId,
    );

    let fileName = '';
    if (documentType === 'oa') {
      fileName = 'investor/investorOA.pdf';
    } else if (documentType === 'sub') {
      fileName = 'investor/investorSA.pdf';
      if (subscription?.signers && subscription.signers.length > 0) {
        isGeneratedDocument = true;
        const deal = await this.dealRepository.findById(subscription.dealId);
        const entity = await this.entitiesRepository.findById(deal.entityId!);

        documentData = {
          ...documentData,
          entityName: entity.name,
          entityStateOfFormation: entity?.stateOfFormation ?? 'Delaware',
          date: moment().format('MM/DD/YYYY'),
          principalAmount: documentData.investmentAmount,
          expenseContribution: 0,
          totalSubscriptionAmount: documentData.investmentAmount,
          taxName1:
            profile?.primarySignatory?.name ??
            `${profile.firstName} ${profile?.lastName}`,
          investmentName: `${
            profile?.name
              ? profile?.name
              : `${profile?.firstName} ${profile?.lastName}`
          }`,
          signatories: [
            ...(subscription.signers ?? []).map(signer => {
              return {
                printName: signer?.name,
                signature: signer?.signature,
              };
            }),
          ],
          jointTenants:
            profile.jointType === 'JOINT_TENANTS_WITH_RIGHTS_OF_SURVIVORSHIP'
              ? 'Yes'
              : 'No',
          tenantsInCommon:
            profile.jointType === 'TENANTS_IN_COMMON' ? 'Yes' : 'No',
          communityProperty:
            profile.jointType === 'COMMUNITY_PROPERTY' ? 'Yes' : 'No',
          LLC:
            profile?.typeOfEntity === 'LIMITED_LIABILITY_COMPANY'
              ? 'Yes'
              : 'No',
          LP: profile?.typeOfEntity === 'LIMITED_PARTNERSHIP' ? 'Yes' : 'No',
          corp:
            profile?.typeOfEntity === 'C_CORPORATION' ||
            profile?.typeOfEntity === 'S_CORPORATION'
              ? 'Yes'
              : 'No',
          isEstimate:
            documentData.entityDisclaimer !== ' ' ||
            documentData.individualDisclaimer !== ' ',
        };
      }
    } else if (documentType === 'sub-s') {
      fileName = 'investor/investorSA-s.pdf';
    } else if (documentType === 'w9') {
      fileName = 'investor/investor1w9.pdf';
    } else if (documentType === 'w8ben') {
      fileName = 'investor/investor1w8ben.pdf';
    } else {
      throw new HttpErrors.NotFound('documentType not supported');
    }
    const deal = await this.subscriptionRepository.deal(subscription.id);
    const contentDispositionName = `${subscription.name} - ${deal.name} - ${typeNames[documentType]}.pdf`;
    //<subscriber profile name> - <deal name> - <file type>.pdf
    let retryCounter = 0;
    const populate = async (signatureFilename?: string): Promise<File> => {
      const result = await this.populatePdfTemplate(
        fileName,
        documentData,
        contentDispositionName,
        signatureFilename,
        isGeneratedDocument,
      );
      if (result?.error) {
        retryCounter++;
        if (retryCounter === 3) {
          throw new HttpErrors[422]();
        }
        return populate(signatureFilename);
      } else {
        const fileData = {
          name: contentDispositionName,
          ownerId: userProfile.id,
          tenantId: tenant.id,
          type: 'pdf',
          key: result,
          lastModified: Date.now().toString(),
        };
        if (existingFileId) {
          const fileItem = await this.fileRepository.findById(existingFileId);
          const fileItemObject = {
            ...fileItem.toObject(),
            ...fileData,
            generating: false,
          };
          await this.fileRepository.updateById(existingFileId, fileItemObject);
          return this.fileRepository.findById(existingFileId);
        }
        return this.fileRepository.create(new File(fileData));
      }
    };

    let file: File;
    if (documentData?.signature) {
      const sigFilename = path.join('/tmp', uuidv4()) + '.png';

      fs.writeFileSync(
        sigFilename,
        documentData.signature.replace(/^data:image\/png;base64,/, ''),
        'base64',
      );

      file = await populate(sigFilename);
    } else {
      //merge files
      file = await populate();
    }
    return file;
  }
}
