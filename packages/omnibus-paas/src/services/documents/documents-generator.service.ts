import {BindingScope, injectable} from '@loopback/core';
import {PDFCheckBox, PDFDocument, PDFTextField} from 'pdf-lib';
import path from 'path';
import fs from 'fs';
import InvestorSABuilder from '../../documentTemplates/react-pdf/investor/InvestorSA';
import StatementBuilder from '../../documentTemplates/react-pdf/close/statement';
import CapStatementBuilder from '../../documentTemplates/react-pdf/subscription/capStatement';
import DistroStatementBuilder from '../../documentTemplates/react-pdf/subscription/distroStatement';

interface DocumentSignatory {
  printName?: string;
  signature?: string;
}

interface SubscriptionAgreementIndividuals {
  isEstimate: boolean;
  entityName: string;
  entityStateOfFormation: string;
  date: string;
  principalAmount: string;
  expenseContribution: string;
  totalSubscriptionAmount: string;
  signatories: DocumentSignatory[];
}

@injectable({scope: BindingScope.TRANSIENT})
export class DocumentsGeneratorService {
  async buildSubscriptionAgreement(
    data: SubscriptionAgreementIndividuals,
  ): Promise<PDFDocument> {
    const signaturePages = await PDFDocument.load(
      await InvestorSABuilder(
        data.isEstimate,
        data.entityName,
        data.entityStateOfFormation,
        data.date,
        data.principalAmount,
        data.expenseContribution,
        data.totalSubscriptionAmount,
        data.signatories,
      ),
    );
    // const signaturePages = await this.buildSubscriptionAgreementSigningPages(
    //   data,
    // );

    //TODO: This should conditionally load the template needed
    // Currently only used for JOINT profile types
    const filePath = path.join(
      process.cwd(),
      'src',
      'documentTemplates',
      'investor',
      'investorSA.pdf',
    );
    const formPdfBytes = fs.readFileSync(filePath);
    let baseTemplate = await PDFDocument.load(formPdfBytes);
    const form = baseTemplate.getForm();

    //fields
    const fields = form.getFields();
    fields.forEach(field => {
      const fieldType = field.constructor.name;
      const name = field.getName();

      if (name in data) {
        if (fieldType === 'PDFTextField') {
          // pdf-lib fill requires no null values
          (field as PDFTextField).setText(data[name] ?? '');
        } else if (fieldType === 'PDFCheckBox' && data[name] === 'Yes') {
          (field as PDFCheckBox).check();
        }
        field.enableReadOnly();
      }
    });
    form.updateFieldAppearances();
    baseTemplate = await PDFDocument.load(await baseTemplate.save());
    const pages = await signaturePages.copyPages(
      baseTemplate,
      [...Array(baseTemplate.getPageCount()).keys()].splice(2),
    );
    for (const page of pages) {
      signaturePages.addPage(page);
    }

    return signaturePages;
  }

  async buildStatement(data: object): Promise<PDFDocument> {
    const statement = await PDFDocument.load(await StatementBuilder(data));

    return statement;
  }

  async buildCapStatement(data: object): Promise<PDFDocument> {
    const statement = await PDFDocument.load(await CapStatementBuilder(data));

    return statement;
  }

  async buildDistroStatement(data: object): Promise<PDFDocument> {
    const statement = await PDFDocument.load(
      await DistroStatementBuilder(data),
    );

    return statement;
  }
}
