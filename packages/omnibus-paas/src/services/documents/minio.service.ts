import {bind, /* inject, */ BindingScope, Context} from '@loopback/core';
import {BucketItemStat, Client, ItemBucketMetadata} from 'minio';
const Minio = require('minio');
const http = require('https');
import {inject} from '@loopback/core';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {Readable as ReadableStream} from 'stream';
import {authenticate} from '@loopback/authentication';
import {RestBindings} from '@loopback/rest';
import {Request} from 'express-serve-static-core';

@bind({scope: BindingScope.TRANSIENT})
export class MinIOService {
  private minioClient: Client;
  private tenant: Tenant;
  private minioRegion: string;

  constructor(
    @inject.context() private ctx: Context,
    @inject(RestBindings.Http.REQUEST, {optional: true})
    private request: Request | undefined = undefined,
  ) {
    this.minioRegion = process.env.MINIO_REGION ?? 'us-east-2';
    this.minioClient = new Minio.Client({
      endPoint: process.env.MINIO_ENDPOINT ?? 'localhost',
      region: this.minioRegion,
      port: parseInt(process.env.MINIO_PORT ?? '9000', 10),
      useSSL: process.env.MINIO_PROTOCOL === 'https',
      accessKey: process.env.MINIO_ACCESS_KEY ?? 'development',
      secretKey: process.env.MINIO_SECRET_KEY ?? 'development',
    });
  }

  public async fputObject(
    objectName: string,
    filePath: string,
    metaData: ItemBucketMetadata,
  ) {
    await this.ensureBucketExists();
    return this.minioClient.fPutObject(
      await this.getBucketName(),
      objectName,
      filePath,
      metaData,
    );
  }

  public async putObject(
    objectName: string,
    stream: ReadableStream | Buffer | string,
    metaData: ItemBucketMetadata,
  ) {
    await this.ensureBucketExists();
    return this.minioClient.putObject(
      await this.getBucketName(),
      objectName,
      stream,
      metaData,
    );
  }

  public async downloadAndPutObject(
    objectURI: string,
    objectName: string,
    tenantId: string,
  ) {
    return new Promise((resolve, reject) => {
      http
        .get(objectURI, stream => {
          const metaData = {
            'Content-Type': stream.headers['content-type'],
            'Content-Disposition': stream.headers['content-disposition'] ?? '',
          };
          /* eslint-disable */
          this.minioClient.putObject(
            this.createBucketName(tenantId),
            objectName,
            stream,
            metaData,
          );
          /* eslint-enable */
          resolve(stream.headers['content-type']);
        })
        .on('error', err => {
          reject(err);
        });
    });
  }

  public async getObject(objectName: string): Promise<ReadableStream> {
    return this.minioClient.getObject(await this.getBucketName(), objectName);
  }

  public async statObject(objectName: string): Promise<BucketItemStat> {
    return this.minioClient.statObject(await this.getBucketName(), objectName);
  }

  public async fgetObject(objectName: string, filePath: string) {
    return this.minioClient.fGetObject(
      await this.getBucketName(),
      objectName,
      filePath,
    );
  }

  public async createBucket(bucketName) {
    return this.minioClient.makeBucket(bucketName, this.minioRegion);
  }

  public async presignedGetObject(objectName: string) {
    return this.minioClient.presignedGetObject(
      await this.getBucketName(),
      objectName,
    );
  }

  private async ensureBucketExists() {
    const bucketExists = await this.minioClient.bucketExists(
      await this.getBucketName(),
    );
    if (!bucketExists) {
      await this.createBucket(this.getBucketName());
    }
    return;
  }

  @authenticate('jwt')
  private async getBucketName() {
    await this.resolveTenant();
    const BucketPrefix = process.env.MINIO_BUCKET_PREFIX ?? 'development';
    const BucketMainSlug = this.tenant.id;
    return BucketPrefix + '-' + BucketMainSlug;
  }

  private createBucketName(tenantId: string) {
    const BucketPrefix = process.env.MINIO_BUCKET_PREFIX ?? 'development';
    const BucketMainSlug = tenantId;
    return BucketPrefix + '-' + BucketMainSlug;
  }

  private async resolveTenant() {
    const tenantOverride =
      this.request?.header('x-file-download-tenant-id') ??
      this.request?.header('x-file-upload-tenant-id');
    if (
      tenantOverride &&
      this.ctx.getSync(MultiTenancyBindings.CURRENT_TENANT).id === 'master'
    ) {
      this.tenant = {id: tenantOverride};
      return;
    }
    this.tenant = await this.ctx.get(MultiTenancyBindings.CURRENT_TENANT);
  }
}
