import {injectable, BindingScope} from '@loopback/core';
import {repository} from '@loopback/repository';
import {FeesStatementEntry, SubscriptionWithRelations} from '../models';
import {
  BlueSkyFeeRepository,
  CloseRepository,
  SubscriptionRepository,
} from '../repositories';
import * as _ from 'lodash';

export interface BlueSkySummary {
  stateSummaries: {
    state: string;
    count: number;
    method: string;
    amount: number;
    fee: number;
  }[];
  firstInvestmentDate: Date;
  totalNASAAFee?: number;
}

@injectable({scope: BindingScope.TRANSIENT})
export class BlueSkyFeeService {
  constructor(
    @repository(BlueSkyFeeRepository)
    private blueSkyFeeRepository: BlueSkyFeeRepository,
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
    @repository(CloseRepository)
    private readonly closeRepository: CloseRepository,
  ) {}

  // Found this regex example online, do we want a full dependency like numeral js
  // for formatting numbers into display strings?
  useCurrencyFormat(number: number) {
    // This check is in place because postgres was giving us strings...
    // This will prevent silent errors from creeping in, if we ever try
    // to display something that isn't a number this may save us headaches.
    // ex: prevents a bad string concatination to a number
    if (typeof number !== 'number') {
      throw new Error(
        'Fee Service expected a number amount and found the typeof: ' +
          typeof number,
      );
    }

    return '$' + number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  async getSubscriptions(dealId): Promise<SubscriptionWithRelations[]> {
    return this.subscriptionRepository.find({
      where: {
        and: [
          {
            status: {
              inq: ['CLOSED', 'COMPLETED'],
            },
          },
          {
            dealId: dealId,
          },
        ],
      },
      include: ['profile', 'transactions'],
    });
  }

  async getBlueSkyAmounts() {
    const variablefees = await this.blueSkyFeeRepository.find({
      where: {or: [{type: 'Variable'}, {type: 'Fixed & Variable'}]},
    });
    const variableState: object = _.keyBy(variablefees, 'state');

    const tieredfees = await this.blueSkyFeeRepository.find({
      where: {type: 'Tiered'},
      include: [{relation: 'blueSkyFeeTiers'}],
    });
    const tieredState: object = _.keyBy(tieredfees, 'state');

    const fixedfees = await this.blueSkyFeeRepository.find({
      where: {type: 'Fixed'},
    });
    const fixedState = _.chain(fixedfees)
      .keyBy('state')
      .mapValues('fee')
      .value();

    return {
      variableState,
      tieredState,
      fixedState,
    };
  }

  async getInvestmentAmountsByState(dealId: string): Promise<BlueSkySummary> {
    const subscriptions = await this.getSubscriptions(dealId);
    const blueskyfees = await this.blueSkyFeeRepository.find();
    const bsf: object = _.keyBy(blueskyfees, 'state');

    // Error conditions
    if (subscriptions.length <= 0 || !subscriptions[0].createdAt) {
      throw new Error(
        'getInvestmentAmountsByState Error: no subscriptions, or createdAt is undefined',
      );
    }

    // have subscriptions gather up investment amounts per address state
    const summariesByState = {};
    let firstSaleDate: Date = subscriptions[0].createdAt;
    subscriptions.forEach(sub => {
      // Create the state prop if it doesn't exist yet
      if (!summariesByState[sub?.profile?.address?.state ?? '']) {
        summariesByState[sub?.profile?.address?.state ?? ''] = {
          count: 0,
          amount: 0,
          fee: 0,
        };
      }

      if (!firstSaleDate && sub.createdAt) {
        firstSaleDate = sub.createdAt;
      }

      summariesByState[sub?.profile?.address?.state ?? ''].amount += sub.amount;
      summariesByState[sub?.profile?.address?.state ?? ''].count += 1;

      if (firstSaleDate && sub.createdAt && sub.createdAt < firstSaleDate) {
        firstSaleDate = sub.createdAt;
      }
    });

    const states = Object.keys(summariesByState);
    const stateSummaries = states.map(state => {
      return {
        state,
        count: summariesByState[state].count,
        method: bsf[state]?.paymentMethod ?? '',
        amount: summariesByState[state].amount,
        fee: 0,
      };
    });

    const closesForDeal = await this.closeRepository.find({
      where: {dealId},
    });
    for (const summary of stateSummaries) {
      summary.fee = await this.getBlueSkyFeesPerState(
        closesForDeal,
        summary.state,
      );
    }

    const totalNASAAFee = await this.getTotalNASAAFee(closesForDeal);
    return {
      stateSummaries,
      firstInvestmentDate: firstSaleDate,
      totalNASAAFee,
    };
  }

  async calculateBlueSkyFees(
    subscriptions: SubscriptionWithRelations[],
    totalFeesAccumulator: number[],
    feesOverride: object,
  ) {
    // Start calculating the totals
    const {variableState, tieredState, fixedState} =
      await this.getBlueSkyAmounts();

    const blueSkyEntries: FeesStatementEntry[] = [];
    const foundStates: {}[] = [];
    const stateProceeds = {};
    let totalProceeds = 0;

    for (const subscription of subscriptions) {
      totalProceeds += subscription?.amount ?? 0;
      const state = subscription?.profile?.address?.state ?? '';

      if (!(state in stateProceeds)) {
        stateProceeds[state] = 0;
      }
      stateProceeds[state] += subscription.amount;
      if (state in fixedState && !foundStates.includes(state)) {
        const entryName = `Blue Sky - ${state}`;
        const amount =
          entryName in feesOverride
            ? feesOverride[entryName].amount
            : fixedState[state];
        blueSkyEntries.push(
          new FeesStatementEntry({
            description: entryName,
            expenses: this.useCurrencyFormat(amount),
          }),
        );
        foundStates.push(state);
        // Hacky until we use numbers for state amounts
        totalFeesAccumulator.push(amount);
      }
    }

    for (const stateProceedsKey in stateProceeds) {
      if (stateProceedsKey in variableState) {
        const baseAmt =
          variableState[stateProceedsKey].variableBase === 'EDGAR Offering'
            ? totalProceeds
            : stateProceeds[stateProceedsKey];
        let stateProceedsAmount =
          variableState[stateProceedsKey].variablePercent * baseAmt;
        stateProceedsAmount =
          stateProceedsAmount < variableState[stateProceedsKey].variableMin
            ? variableState[stateProceedsKey].variableMin
            : stateProceedsAmount;
        stateProceedsAmount =
          stateProceedsAmount > variableState[stateProceedsKey].variableMax
            ? variableState[stateProceedsKey].variableMax
            : stateProceedsAmount;
        stateProceedsAmount += variableState[stateProceedsKey].fee;

        const entryName = `Blue Sky - ${stateProceedsKey}`;
        const amount =
          entryName in feesOverride
            ? feesOverride[entryName].amount
            : stateProceedsAmount;

        blueSkyEntries.push(
          new FeesStatementEntry({
            description: entryName,
            expenses: this.useCurrencyFormat(amount),
          }),
        );
        totalFeesAccumulator.push(amount);
      }

      if (typeof tieredState[stateProceedsKey] !== 'undefined') {
        let value = 0;
        const baseAmt =
          variableState[stateProceedsKey].variableBase === 'EDGAR Offering'
            ? totalProceeds
            : stateProceeds[stateProceedsKey];
        for (const tier in tieredState[stateProceedsKey].blueSkyFeeTiers) {
          value = tieredState[stateProceedsKey].blueSkyFeeTiers[tier].fee;
          if (
            baseAmt <
            tieredState[stateProceedsKey].blueSkyFeeTiers[tier].breakpoint
          ) {
            break;
          }
        }

        const entryName = `Blue Sky - ${stateProceedsKey}`;
        const amount =
          entryName in feesOverride ? feesOverride[entryName].amount : value;

        blueSkyEntries.push(
          new FeesStatementEntry({
            description: entryName,
            expenses: this.useCurrencyFormat(amount),
          }),
        );
        totalFeesAccumulator.push(amount);
      }
    }
    return {blueSkyEntries, totalProceeds};
  }

  async getBlueSkyFeesPerState(closesForDeal, state) {
    let totalBlueSkyFees = 0;
    try {
      if (closesForDeal?.length) {
        closesForDeal.forEach(c => {
          const regulatoryFeesSection = c.statement['sections'].filter(
            s => s.title === 'Regulatory Filings Fees',
          );
          if (regulatoryFeesSection?.length) {
            const blueSkyFees = regulatoryFeesSection[0].entries.filter(
              e => e.description === `Blue Sky - ${state}`,
            );
            totalBlueSkyFees += blueSkyFees?.length
              ? Number((blueSkyFees[0].expenses ?? '0').replace(/[^\d.]/g, ''))
              : 0;
          }
        });
      }
      return totalBlueSkyFees;
    } catch (e) {
      return 0;
    }
  }

  async getTotalNASAAFee(closesForDeal) {
    let totalNASAAFee = 0;
    try {
      if (closesForDeal?.length) {
        closesForDeal.forEach(c => {
          const regulatoryFeesSection = c.statement['sections'].filter(
            s => s.title === 'Regulatory Filings Fees',
          );
          if (regulatoryFeesSection?.length) {
            const NASAAFee = regulatoryFeesSection[0].entries.filter(
              e => e.description === `NASAA EFD System Fee`,
            );
            totalNASAAFee += NASAAFee?.length
              ? Number((NASAAFee[0].expenses ?? '0').replace(/[^\d.]/g, ''))
              : 0;
          }
        });
      }
      return totalNASAAFee;
    } catch (e) {
      return 0;
    }
  }
}
