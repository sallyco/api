/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {ParallelMarketsDataSource} from '../../datasources';

export interface ParallelMarkets {
  getMe: (params: null, options: any) => Promise<ProfileInterface>;
  getAccreditations: (
    params: null,
    options: any,
  ) => Promise<AccreditationsInterface>;

  execute: (operation: string, params: any, config: any) => Promise<any>;
}

export interface ProfileInterface {
  access_expires_at?: null;
  access_revoked_by?: null;
  id: string;
  profile: {
    email: string;
    first_name: string;
    last_name: string;
  };
  type: 'individual';
  user_id: string;
  user_profile: {
    email: string;
    first_name: string;
    last_name: string;
  };
  user_providing_for: 'self';
}

export interface AccreditationsInterface {
  accreditations: [
    {
      assertion_type: 'evaluator-assertion';
      certified_at: number;
      created_at: number;
      documents: {
        download_url: string;
        id: string;
        expires_at: number;
        download_url_expires: number;
      }[];
      expires_at?: null;
      first_name: string;
      id: string;
      last_name: string;
      status: 'pending';
    },
  ];
  id: string;
  indicated_unaccredited: null;
  type: 'individual';
  user_id: string;
  user_providing_for: 'self';
}

export class ParallelMarketsProvider implements Provider<ParallelMarkets> {
  constructor(
    // ParallelMarkets must match the name property in the datasource json file
    @inject('datasources.ParallelMarkets')
    protected dataSource: ParallelMarketsDataSource = new ParallelMarketsDataSource(),
  ) {}

  value(): Promise<ParallelMarkets> {
    return getService(this.dataSource);
  }
}
