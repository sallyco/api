import {
  injectable,
  /* inject, */ BindingScope,
  service,
  Context,
} from '@loopback/core';
import {AccreditationService} from './accreditations.service';
import {
  ParallelMarkets,
  ParallelMarketsProvider,
} from './parallel-markets.service';
import {Accreditation, AccreditationStatus, File} from '../../models';
import {PassThrough} from 'stream';
import axios from 'axios';
import {MultiTenancyBindings} from '../../multi-tenancy';
import * as uuid from 'uuid';
import {repository} from '@loopback/repository';
import {FileRepository} from '../../repositories';
import {inject} from '@loopback/context';
import {MinIOServiceBindings} from '../../keys';
import {MinIOService} from '../documents/minio.service';
import debugFactory from 'debug';
import moment from 'moment';

const debug = debugFactory(
  'omnibus-paas:services:accreditation-parallel-markets',
);

const statusMap: {[key: string]: AccreditationStatus} = {
  current: 'CURRENT',
  pending: 'PENDING',
  third_party_pending: 'THIRD_PARTY_PENDING',
  expired: 'EXPIRED',
  rejected: 'REJECTED',
};

@injectable({scope: BindingScope.TRANSIENT})
export class AccreditationParallelMarketsService
  implements AccreditationService
{
  static typeId: 'PARALLELMARKETS';
  typeId = 'PARALLELMARKETS';

  constructor(
    @inject.context() private readonly ctx: Context,
    @service(ParallelMarketsProvider)
    protected readonly parallelMarketsService: ParallelMarkets,
    @repository(FileRepository)
    private readonly fileRepository: FileRepository,
    @inject(MinIOServiceBindings.MINIO_SERVICE)
    private readonly minioService: MinIOService,
  ) {}

  getTypeId() {
    return this.typeId;
  }

  buildApiOptions(accreditation: Accreditation) {
    return {
      server:
        process.env.PARALLEL_MARKETS_URL ??
        'https://demo-api.parallelmarkets.com/v1',
      securities: {
        authorized: {
          oauth2: {
            token: {
              access_token: `${accreditation?.providerMeta?.auth?.access_token}`,
            },
          },
        },
      },
    };
  }

  async getAccreditationUpdate(
    accreditation: Accreditation,
  ): Promise<Accreditation> {
    const profile = await this.parallelMarketsService.getMe(
      null,
      this.buildApiOptions(accreditation),
    );
    const accreditationData =
      await this.parallelMarketsService.getAccreditations(
        null,
        this.buildApiOptions(accreditation),
      );

    const files: string[] = [];

    if (
      accreditationData.accreditations[0].documents &&
      accreditationData.accreditations[0].documents.length > 0
    ) {
      const fileId = await this.uploadCertificationLetter(
        accreditationData.accreditations[0].documents[0].download_url,
        accreditation.ownerId,
        accreditation.tenantId,
      );
      files.push(fileId);
    }

    accreditation = new Accreditation({
      ...accreditation,
      type: '506c',
      status: statusMap[accreditationData.accreditations[0].status],
      files: files,
      providerMeta: {
        ...accreditation.providerMeta,
        profile,
        accreditation: accreditationData,
      },
      expirationDate: accreditationData.accreditations[0].expires_at
        ? moment.unix(accreditationData.accreditations[0].expires_at).toDate()
        : undefined,
      certificationDate: accreditationData.accreditations[0].certified_at
        ? moment.unix(accreditationData.accreditations[0].certified_at).toDate()
        : undefined,
    });
    return accreditation;
  }

  async uploadCertificationLetter(
    url: string,
    ownerId: string,
    tenantId: string,
  ): Promise<string> {
    const fileStorageName = `Certification Letter.pdf`;
    const passThroughStream = new PassThrough();
    const responseStream = await axios.get(url, {
      responseType: 'stream',
    });
    const fileKey = uuid.v4();
    this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({
      id: tenantId,
    });
    const MinioPromise = this.minioService.putObject(
      fileKey,
      passThroughStream,
      {
        'Content-Type': responseStream.headers['content-type'],
        'Content-Disposition': `attachment; filename=Certification Letter.pdf`,
      },
    );
    responseStream.data.pipe(passThroughStream);
    try {
      await MinioPromise;
    } catch (e) {
      debug('Failed to download file from Parallel Markets: ', e);
    }
    const file = new File({
      name: fileStorageName,
      type: 'pdf',
      ownerId: ownerId,
      tenantId: tenantId,
      key: fileKey,
      lastModified: Date.now().toString(),
    });
    const fileObject = await this.fileRepository.save(file);
    return String(fileObject.id);
  }
}
