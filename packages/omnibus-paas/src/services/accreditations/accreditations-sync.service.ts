import {injectable, /* inject, */ BindingScope, service} from '@loopback/core';
import {AccreditationRepository} from '../../repositories';
import {repository} from '@loopback/repository';
import debugFactory from 'debug';
import {
  AccreditationProvider,
  AccreditationService,
} from './accreditations.service';

const debug = debugFactory('omnibus-paas:services:accreditations-sync');

@injectable({scope: BindingScope.TRANSIENT})
export class AccreditationsSyncService {
  constructor(
    @repository(AccreditationRepository)
    private readonly accreditationRepository: AccreditationRepository,
    @service(AccreditationProvider)
    private readonly accreditationService: AccreditationService,
  ) {}

  async synchronizeAccreditations() {
    const accreditations = await this.accreditationRepository.find({
      where: {
        status: {
          eq: 'PENDING',
        },
      },
      limit: 10,
    });
    for (const accreditation of accreditations) {
      try {
        const updatedAccreditation =
          await this.accreditationService.getAccreditationUpdate(accreditation);
        await this.accreditationRepository.update(updatedAccreditation);
      } catch (e) {
        debug(`Unable to sync ${accreditation.id} accreditation:`, e);
        await this.accreditationRepository.update({
          ...accreditation,
          status: 'EXCEPTION',
          reason: e.message,
        });
      }
    }
  }
}
