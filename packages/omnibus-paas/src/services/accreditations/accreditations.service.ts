import {bind, BindingScope, Provider, service} from '@loopback/core';
import {AccreditationParallelMarketsService} from './accreditation-parallel-markets.service';
import {Accreditation} from '../../models';

export interface AccreditationService {
  typeId: string;
  getTypeId: () => string;

  getAccreditationUpdate(accreditation: Accreditation): Promise<Accreditation>;
}

@bind({scope: BindingScope.TRANSIENT})
export class AccreditationProvider implements Provider<AccreditationService> {
  constructor(
    @service(AccreditationParallelMarketsService)
    private readonly parallelMarkets: AccreditationParallelMarketsService,
  ) {}

  value(): AccreditationService {
    // TODO: Swap this out based on config
    return this.parallelMarkets;
  }
}
