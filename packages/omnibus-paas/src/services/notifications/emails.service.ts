/* eslint-disable  @typescript-eslint/no-explicit-any */
import {inject} from '@loopback/context';
import {bind, BindingScope} from '@loopback/core';
import {repository} from '@loopback/repository';
import debugFactory from 'debug';
import Handlebars from 'handlebars';
import mjml2html from 'mjml';
import moment from 'moment';
import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import path from 'path';
import {OpenEXOTemplate} from '../../emailTemplates/templates/deals/openEXO';
import {
  DevelopmentLegalIncNotificationData,
  DevelopmentLegalncNotificationTemplate,
} from '../../emailTemplates/templates/developmentLegalIncNotification';
import {InviteAccountAdminUserTemplate} from '../../emailTemplates/templates/inviteAccountAdminUserTemplate';
import {
  InviteInvestorExistingEmailData,
  InviteInvestorExistingTemplate,
} from '../../emailTemplates/templates/inviteInvestorExistingTemplate';
import {
  InviteInvestorEmailData,
  InviteInvestorTemplate,
} from '../../emailTemplates/templates/inviteInvestorTemplate';
import {InviteOrganizerTemplate} from '../../emailTemplates/templates/inviteOrganizerTemplate';
import {
  NewAccountEmailData,
  NewAccountTemplate,
} from '../../emailTemplates/templates/newAccountTemplate';
import {
  NudgeInvestorExistingEmailData,
  NudgeInvestorExistingTemplate,
} from '../../emailTemplates/templates/nudgeInvestorExisting';
import {OnfidoConsiderTemplate} from '../../emailTemplates/templates/onfidoConsiderTemplate';
import {
  RemoveInviteEmailData,
  RemoveInviteTemplate,
} from '../../emailTemplates/templates/removeInvite';
import {
  ResetPasswordEmailData,
  ResetPasswordEmailTemplate,
} from '../../emailTemplates/templates/resetPasswordTemplate';
import {DealRepository, EmailTemplateRepository} from '../../repositories';
import {TenantService} from '../tenant/tenant.service';
import {
  WireSentNotificationEmailData,
  WireSentNotificationTemplate,
} from '../../emailTemplates/templates/wireSentNotification';
import {BankNoNewChangesEmailTemplate} from '../../emailTemplates/templates/bankNoNewChangesEmail';
import {
  InviteSignerEmailData,
  InviteSignerTemplate,
} from '../../emailTemplates/templates/inviteSignerTemplate';
import {ContactOrganizerEmailTemplate} from '../../emailTemplates/templates/ContactOrganizerEmail';
import {InviteUserTemplate} from '../../emailTemplates/templates/inviteUserTemplate';
import {ProfileNeedsUpdateTemplate} from '../../emailTemplates/templates/profileNeedsUpdate';
import {InviteFounderTemplate} from '../../emailTemplates/templates/inviteFounderTemplate';

export enum EmailTypes {
  inviteInvestor = 'inviteInvestor',
  inviteInvestorExisting = 'inviteInvestorExisting',
  resetPassword = 'resetPassword',
  newAccount = 'newAccount',
  onfidoConsider = 'onfidoConsider',
  inviteOrganizer = 'inviteOrganizer',
  developmentLegalIncNotification = 'developmentLegalIncNotification',
  DEALOpenExo = 'DEALOpenExo',
  nudgeInvestor = 'nudgeInvestor',
  inviteAccountAdminUser = 'inviteAccountAdminUser',
  removeInvite = 'removeInvite',
  wireSentNotification = 'wireSentNotification',
  bankNoNewChangesEmail = 'bankNoNewChangesEmail',
  inviteSigner = 'inviteSigner',
  contactOrganizer = 'contactOrganizer',
  inviteUser = 'inviteUser',
  profileNeedsUpdate = 'profileNeedsUpdate',
  inviteFounder = 'inviteFounder',
}

const debug = debugFactory('omnibus-paas:service:emailsService');

export interface EmailData {}

export interface TemplateData {
  isMJML: boolean;
  messageData: (data?: unknown) => string;
  replyTo?: string;
}

@bind({scope: BindingScope.TRANSIENT})
export class EmailsService {
  public transporter: Mail;

  private readonly templates: {[key in EmailTypes]?: TemplateData} = {
    [EmailTypes.inviteInvestor]: {
      isMJML: true,
      messageData: InviteInvestorTemplate,
    },
    [EmailTypes.inviteInvestorExisting]: {
      isMJML: true,
      messageData: InviteInvestorExistingTemplate,
    },
    [EmailTypes.resetPassword]: {
      isMJML: true,
      messageData: ResetPasswordEmailTemplate,
    },
    [EmailTypes.newAccount]: {
      isMJML: true,
      messageData: NewAccountTemplate,
    },
    [EmailTypes.onfidoConsider]: {
      isMJML: false,
      messageData: OnfidoConsiderTemplate,
    },
    [EmailTypes.inviteOrganizer]: {
      isMJML: true,
      messageData: InviteOrganizerTemplate,
    },
    [EmailTypes.developmentLegalIncNotification]: {
      isMJML: false,
      messageData: DevelopmentLegalncNotificationTemplate,
    },
    [EmailTypes.developmentLegalIncNotification]: {
      isMJML: false,
      messageData: DevelopmentLegalncNotificationTemplate,
    },
    [EmailTypes.bankNoNewChangesEmail]: {
      isMJML: false,
      messageData: BankNoNewChangesEmailTemplate,
    },
    [EmailTypes.DEALOpenExo]: {
      isMJML: true,
      messageData: OpenEXOTemplate,
      replyTo: 'salim@openexo.com',
    },
    [EmailTypes.nudgeInvestor]: {
      isMJML: true,
      messageData: NudgeInvestorExistingTemplate,
    },
    [EmailTypes.inviteAccountAdminUser]: {
      isMJML: true,
      messageData: InviteAccountAdminUserTemplate,
    },
    [EmailTypes.removeInvite]: {
      isMJML: true,
      messageData: RemoveInviteTemplate,
    },
    [EmailTypes.wireSentNotification]: {
      isMJML: true,
      messageData: WireSentNotificationTemplate,
    },
    [EmailTypes.inviteSigner]: {
      isMJML: true,
      messageData: InviteSignerTemplate,
    },
    [EmailTypes.contactOrganizer]: {
      isMJML: true,
      messageData: ContactOrganizerEmailTemplate,
    },
    [EmailTypes.inviteUser]: {
      isMJML: true,
      messageData: InviteUserTemplate,
    },
    [EmailTypes.profileNeedsUpdate]: {
      isMJML: true,
      messageData: ProfileNeedsUpdateTemplate,
    },
    [EmailTypes.inviteFounder]: {
      isMJML: true,
      messageData: InviteFounderTemplate,
    },
  };

  constructor(
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(EmailTemplateRepository)
    private readonly emailTemplateRepository: EmailTemplateRepository,
    @inject('services.TenantService')
    private readonly tenantService: TenantService,
  ) {
    const transport: SMTPTransport.Options = {
      host: process.env.SMTP_HOST,
      port: parseInt(process.env.SMTP_PORT ?? '25'),
      ignoreTLS: JSON.parse(process.env.SMTP_NO_SSL ?? 'false'),
    };

    if (process.env.SMTP_AUTH_USER && process.env.SMTP_AUTH_PASS) {
      transport.auth = {
        user: process.env.SMTP_AUTH_USER,
        pass: process.env.SMTP_AUTH_PASS,
      };
    }
    this.transporter = nodemailer.createTransport(transport);
    Handlebars.registerHelper('year', function () {
      return new Date().getFullYear();
    });
    Handlebars.registerHelper('formatDate', function (date) {
      return moment(date).format('MM/DD/YYYY');
    });
    Handlebars.registerHelper(
      'formatCurrency',
      function (amount, type = 'USD') {
        const formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: type,
        });
        return formatter.format(amount);
      },
    );
    Handlebars.registerHelper('getByKey', function (context, options) {
      if (!context) return '';
      return context.find(el => el.key === options.hash.key).value || '';
    });
  }

  async buildTemplate(emailType: EmailTypes, data: any) {
    const currentTenant = await this.tenantService.getCurrentHydratedTenant();
    let messageData = '';
    if (data?.deal?.id) {
      try {
        data.deal = await this.dealRepository.findById(data.deal.id);
      } catch (e) {
        debug("Email template builder can't find deal: ", e);
      }
    }
    let templateData = await this.emailTemplateRepository.findOne({
      where: {
        key: emailType,
        tenantId: currentTenant.id,
      },
    });
    if (!templateData) {
      templateData = await this.emailTemplateRepository.findOne({
        where: {key: emailType, tenantId: 'global'},
      });
    }
    data.template = templateData?.data ? templateData.data : {};
    const template = this.templates[emailType];
    if (template) {
      messageData = template.messageData(data);
      if (template.isMJML) {
        messageData = mjml2html(messageData, {
          filePath: path.resolve(__dirname, '../../emailTemplates/templates'),
        }).html;
      }
      const HandleBarsTemplate = Handlebars.compile(messageData);
      messageData = HandleBarsTemplate(data);
    }
    return messageData;
  }

  async sendEmail(
    emailType: EmailTypes,
    to: string,
    subject: string,
    emailData: EmailData,
    tenantId?: string,
  ) {
    if (tenantId) {
      await this.tenantService.setCurrentTenant(tenantId);
    }
    const currentTenant = await this.tenantService.getCurrentHydratedTenant();
    const emailBody = await this.buildTemplate(emailType, {
      ...emailData,
      tenant: currentTenant,
    });
    return this.transporter.sendMail({
      from:
        currentTenant?.settings?.emails?.fromAddress ??
        `${
          currentTenant.name ?? 'Glassboard Technology'
        } <no-reply@glassboardtech.com>`,
      to,
      subject,
      html: emailBody,
      replyTo: this.templates[emailType]?.replyTo,
      headers: {
        'X-SES-CONFIGURATION-SET': 'default',
      },
    });
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendInviteInvestorEmail(
    to: string,
    subject: string,
    data: InviteInvestorEmailData,
  ) {
    return this.sendEmail(EmailTypes.inviteInvestor, to, subject, data);
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendInviteInvestorExistingEmail(
    to: string,
    subject: string,
    data: InviteInvestorExistingEmailData,
  ) {
    return this.sendEmail(EmailTypes.inviteInvestorExisting, to, subject, data);
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendResetPasswordEmail(
    to: string,
    subject: string,
    data: ResetPasswordEmailData,
  ) {
    return this.sendEmail(EmailTypes.resetPassword, to, subject, data);
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendNewAccountEmail(
    to: string,
    subject: string,
    data: NewAccountEmailData,
  ) {
    return this.sendEmail(EmailTypes.newAccount, to, subject, data);
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendDevelopmentLegalIncNotification(
    data: DevelopmentLegalIncNotificationData,
  ) {
    return this.sendEmail(
      EmailTypes.developmentLegalIncNotification,
      'aqeel@legalinc.com',
      `DEVELOPMENT: UAT Order #${data.orderId}`,
      data,
    );
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendNudgeInvestorExistingEmail(
    to: string,
    subject: string,
    data: NudgeInvestorExistingEmailData,
  ) {
    return this.sendEmail(EmailTypes.nudgeInvestor, to, subject, data);
  }

  // TODO: (checkup) A code search doesn't find this function used anywhere in omnibus, remove?
  async sendRemoveInviteEmail(
    to: string,
    subject: string,
    data: RemoveInviteEmailData,
  ) {
    return this.sendEmail(EmailTypes.removeInvite, to, subject, data);
  }

  async sendWireNotificationEmail(
    to: string,
    subject: string,
    data: WireSentNotificationEmailData,
  ) {
    return this.sendEmail(EmailTypes.wireSentNotification, to, subject, data);
  }

  async sendBankNoNewChangesEmail(
    to: string,
    subject: string,
    organizationName: string,
  ) {
    return this.sendEmail(EmailTypes.bankNoNewChangesEmail, to, subject, {
      organizationName: organizationName,
    });
  }

  async sendInviteSignerEmail(
    to: string,
    subject: string,
    data: InviteSignerEmailData,
  ) {
    return this.sendEmail(EmailTypes.inviteSigner, to, subject, data);
  }
}
