import {injectable, /* inject, */ BindingScope, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  DealRepository,
  EntitiesRepository,
  SubscriptionRepository,
} from '../../repositories';
import {Deal, Entities, SubscriptionWithRelations} from '../../models';
import _ from 'lodash';
import {Banking, BankingProvider} from '../banking/banking.service';
import moment from 'moment';

enum AuditChecks {
  ALL_SUBSCRIPTION_DOCS_SIGNED = 'ALL_SUBSCRIPTION_DOCS_SIGNED',
  POSITIVE_CLOSING_BALANCE = 'POSITIVE_CLOSING_BALANCE',
  INVESTOR_COUNT_LIMIT = 'INVESTOR_COUNT_LIMIT',
  SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH = 'SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH',
  ALL_INVESTORS_PASSED_KYC = 'ALL_INVESTORS_PASSED_KYC',
  ALL_INVESTORS_ACCREDITED = 'ALL_INVESTORS_ACCREDITED',
}
type AuditCheckType = `${AuditChecks}`;

interface CheckResult {
  status: 'PASS' | 'FAIL';
  failDetail?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
}

export type CheckResultMap = {
  [key in AuditCheckType]?: CheckResult;
};

type CheckRunMap = {
  [key in AuditCheckType]?: (
    subscriptions: SubscriptionWithRelations[],
    deal: Deal,
    entity: Entities,
  ) => Promise<CheckResult>;
};

// Functionality should be moved to Camunda, in backend for now
@injectable({scope: BindingScope.TRANSIENT})
export class CloseAuditsService {
  checkMapping: CheckRunMap = {
    [AuditChecks.ALL_SUBSCRIPTION_DOCS_SIGNED]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      const failedSubscriptions = _.filter(subscriptions, {
        isDocsSigned: false,
      });
      if (failedSubscriptions.length > 0) {
        result.status = 'FAIL';
        result.data = {
          subscriptions: failedSubscriptions,
        };
        result.failDetail = 'Failed doc signing';
      }
      return result;
    },
    [AuditChecks.POSITIVE_CLOSING_BALANCE]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      if (entity?.bankAccount?.providerMeta?.accountId) {
        try {
          const providerBankDetails =
            await this.bankingService.getDepositAccountDetails(
              entity.bankAccount.providerMeta.accountId,
            );
          if (
            !providerBankDetails?.currentBalance ||
            providerBankDetails.currentBalance < 0
          ) {
            result.status = 'FAIL';
            result.failDetail = 'Failed bank balance check';
          }
        } catch (e) {
          //
        }
      }
      return result;
    },
    [AuditChecks.INVESTOR_COUNT_LIMIT]: async (subscriptions, deal, entity) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      if (subscriptions.length >= 100) {
        result.status = 'FAIL';
        result.failDetail = 'Deal is over subscribed';
      }
      return result;
    },
    [AuditChecks.ALL_INVESTORS_PASSED_KYC]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      const kycFailedInvestors = subscriptions.filter(sub => {
        return sub.profile?.kycAml?.result !== 'CLEAR';
      });
      if (kycFailedInvestors.length > 0) {
        result.status = 'FAIL';
        result.failDetail = 'Failed KYC/AML';
      }
      return result;
    },
    [AuditChecks.SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      if (
        _.find(subscriptions, sub => {
          return !sub?.isAmountMatched;
        })
      ) {
        result.status = 'FAIL';
        result.failDetail = 'Amounts do not match';
      }
      return result;
    },
    [AuditChecks.SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      if (
        _.find(subscriptions, sub => {
          return !sub?.isAmountMatched;
        })
      ) {
        result.status = 'FAIL';
      }
      return result;
    },
    [AuditChecks.ALL_INVESTORS_ACCREDITED]: async (
      subscriptions,
      deal,
      entity,
    ) => {
      const result: CheckResult = {
        status: 'PASS',
      };
      const failedSubscriptions = subscriptions.filter(sub => {
        const accreditations = sub?.profile?.accreditations;
        if (!accreditations) {
          return true;
        }
        const latestAccreditation = accreditations.reduce((a, b) =>
          a.createdAt > b.createdAt ? a : b,
        );
        if (
          latestAccreditation.status !== 'CURRENT' ||
          !latestAccreditation?.expirationDate ||
          moment(latestAccreditation.expirationDate).isBefore(moment())
        ) {
          return true;
        }
      });
      if (failedSubscriptions.length > 0) {
        result.status = 'FAIL';
        result.data = {
          subscriptions: failedSubscriptions,
        };
      }
      return result;
    },
  };

  constructor(
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(EntitiesRepository)
    private readonly entitiesRepository: EntitiesRepository,
    @service(BankingProvider)
    private readonly bankingService: Banking,
  ) {}

  async is506c(entity: Entities) {
    return (
      entity.regulationType === 'REGULATION_D' && entity.regDExemption === 'c'
    );
  }

  async getChecksToRun(deal: Deal, entity: Entities): Promise<AuditChecks[]> {
    const checks: AuditChecks[] = [
      AuditChecks.ALL_SUBSCRIPTION_DOCS_SIGNED,
      AuditChecks.POSITIVE_CLOSING_BALANCE,
      AuditChecks.INVESTOR_COUNT_LIMIT,
      AuditChecks.ALL_INVESTORS_PASSED_KYC,
      AuditChecks.SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH,
    ];

    if (await this.is506c(entity)) {
      checks.push(AuditChecks.ALL_INVESTORS_ACCREDITED);
    }
    return checks;
  }

  async auditPotentialClose(
    subscriptionIds: string[],
    dealId: string,
  ): Promise<CheckResultMap> {
    const subscriptions = await this.subscriptionRepository.find({
      where: {
        and: [
          {dealId: dealId},
          {
            isDeleted: {
              neq: true,
            },
          },
          {
            id: {
              inq: subscriptionIds,
            },
          },
        ],
      },
      include: [
        {
          relation: 'profile',
          scope: {
            include: [
              {
                relation: 'kycAml',
              },
              {
                relation: 'accreditations',
              },
            ],
          },
        },
      ],
    });
    const deal = await this.dealRepository.findById(dealId);
    const entity = await this.entitiesRepository.findById(deal.entityId!);

    const checksToRun = await this.getChecksToRun(deal, entity);
    const results: CheckResultMap = {};
    for (const auditCheck of checksToRun) {
      if (typeof this.checkMapping[auditCheck] === 'function') {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        results[auditCheck] = await this.checkMapping[auditCheck](
          subscriptions,
          deal,
          entity,
        );
      } else {
        results[auditCheck] = {
          status: 'FAIL',
        };
      }
    }
    return results;
  }
}
