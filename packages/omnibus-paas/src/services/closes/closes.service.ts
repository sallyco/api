/* eslint-disable @typescript-eslint/no-explicit-any */
import {BindingScope, Context, inject, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  CloseRepository,
  CompanyRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
} from '../../repositories';
import {PDFDocument, PDFTextField, rgb} from 'pdf-lib';
import * as fs from 'fs';
import {Close, Entities, File, Subscription, Tenant} from '../../models';
import debugFactory from 'debug';
import {MinIOService} from '../documents/minio.service';
import {MultiTenancyBindings} from '../../multi-tenancy';
import moment from 'moment';
import path from 'path';
import {MinIOServiceBindings} from '../../keys';
import {PDFHelpers} from '../../utils/documentUtils/PDFHelpers';

const {v4: uuidv4} = require('uuid');

const debug = debugFactory('omnibus-paas:services:closes');

interface DocumentEntry {
  documentType: DocType;
  file: File;
  data: Buffer;
}

interface FillableField {
  type: 'text' | 'signature';
  value?: string;
  textAlignment?: 'left' | 'right' | 'center';
}

interface FillableData {
  organizerName: FillableField;
  organizerSignaturePrint: FillableField;
  organizerSignatureTitle: FillableField;
  organizerInfoBlock: FillableField;
  organizerSignature: FillableField;

  managerName: FillableField;
  managerSignaturePrint: FillableField;
  managerSignatureTitle: FillableField;
  managerInfoBlock: FillableField;
  managerSignature: FillableField;

  fundManagerName: FillableField;
  fundManagerSignaturePrint: FillableField;
  fundManagerSignatureTitle: FillableField;
  fundManagerInfoBlock: FillableField;
  fundManagerSignature: FillableField;

  date: FillableField;
  agreementTitle: FillableField;
  entityLegalName: FillableField;
  fundEntityInfoBlock: FillableField;
}

export enum DocType {
  SubscriptionAgreement = 'SubscriptionAgreement',
  OperatingAgreement = 'OperatingAgreement',
  PrivatePlacementMemorandum = 'PrivatePlacementMemorandum',
  SubscriptionAgreementSignaturePage = 'SubscriptionAgreementSignaturePage',
  OperatingAgreementSignaturePage = 'OperatingAgreementSignaturePage',
  W9 = 'W9',
  W8BEN = 'W-8BEN',
  Unknown = 'Unknown',
}

const formatMoney = amount => {
  return amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

@injectable({scope: BindingScope.TRANSIENT})
export class ClosesService {
  constructor(
    @repository(CloseRepository)
    private readonly closeRepository: CloseRepository,
    @repository(EntitiesRepository)
    private readonly entityRepository: EntitiesRepository,
    @repository(TenantRepository)
    private readonly tenantRepository: TenantRepository,
    @repository(CompanyRepository)
    private readonly companyRepository: CompanyRepository,
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @repository(FileRepository)
    private readonly fileRepository: FileRepository,
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(ProfileRepository)
    private readonly profileRepository: ProfileRepository,
    @inject(MinIOServiceBindings.MINIO_SERVICE)
    private readonly minio: MinIOService,
    @inject.context() private ctx: Context,
  ) {}

  async processClose(
    closeId: string,
    closeDate?: Date,
    organizerSignature?: string,
    fundManagerSignature?: string,
  ) {
    const close = await this.closeRepository.findById(closeId);
    const entity = await this.entityRepository.findById(close.entityId);
    const tenant = await this.tenantRepository.findById(String(close.tenantId));
    if (fundManagerSignature) {
      close.fundManagerSignature = fundManagerSignature;
    } else {
      close.fundManagerSignature = !(await this.isMultiStepSigningTenant(
        tenant,
      ))
        ? organizerSignature
        : close.fundManagerSignature;
    }
    if (!close.dateClosed) {
      close.dateClosed = new Date();
    }
    await this.closeRepository.update(close);

    await this.processSubscriptionsForClose(
      close,
      entity,
      closeDate ?? close.dateClosed,
      organizerSignature ?? close.organizerSignature,
      fundManagerSignature ?? close.fundManagerSignature,
    );
    const isMultiSignClose = await this.isMultiStepSigningTenant(tenant);
    if (!fundManagerSignature ?? close.fundManagerSignature) {
      debug(
        `This is ${isMultiSignClose ? '' : 'not'} a MultiSign deal: ${
          isMultiSignClose ? 'stopping' : 'proceeding'
        }`,
        close.id,
      );
    }

    if (
      (!isMultiSignClose && close.fundManagerSignature) ||
      (isMultiSignClose && close.fundManagerSignature)
    ) {
      close.fundManagerSigned = true;
    }
    await this.closeRepository.update(close);
  }

  async resolveFileNameToDocType(name: string) {
    // This is a pretty fragile way of matching these documents, but
    // subscriptions don't store a document type on the file listing or
    // File object currently
    if (name.search('Operating Agreement.pdf') > -1) {
      return DocType.OperatingAgreement;
    } else if (name.search('Subscription Agreement.pdf') > -1) {
      return DocType.SubscriptionAgreement;
    } else if (name.search('Private Placement Memorandum.pdf') > -1) {
      return DocType.PrivatePlacementMemorandum;
    } else if (name.search('W9.pdf') > -1 || name.search('W-9.pdf') > -1) {
      return DocType.W9;
    } else if (
      name.search('W8-BEN.pdf') > -1 ||
      name.search('W8BEN.pdf') > -1
    ) {
      return DocType.W8BEN;
    }
    return undefined;
  }

  async mergeOADocs(
    entityDocumentTemplate: Buffer | Uint8Array,
    subscriptionSignaturePages: Buffer | Uint8Array,
    managerSignaturePages: Buffer | Uint8Array,
    advisoryAgreementTemplate: Buffer | Uint8Array,
  ): Promise<PDFDocument> {
    const signaturePageStart = await PDFHelpers.findPageIndexWithText(
      entityDocumentTemplate,
      /Signature\s+Page\s+Follows/,
    );
    const advisoryAgreementStart = await PDFHelpers.findPageIndexWithText(
      entityDocumentTemplate,
      /INVESTMENT\s+ADVISORY\s+AGREEMENT/,
    );
    const compiledDoc = await PDFDocument.load(entityDocumentTemplate);

    await PDFHelpers.removePages(
      compiledDoc,
      advisoryAgreementStart + 1,
      compiledDoc.getPageCount() - 1,
    );
    await PDFHelpers.removePages(
      compiledDoc,
      signaturePageStart,
      advisoryAgreementStart - 1,
    );

    // Add Manager Signature Pages
    const managerSignatureDoc = await PDFDocument.load(managerSignaturePages);
    await PDFHelpers.mergeDocumentAtIndex(
      compiledDoc,
      managerSignatureDoc,
      signaturePageStart + 1,
    );

    // Add Subscriber Signature Pages
    const subscriptionSignatureDoc = await PDFDocument.load(
      subscriptionSignaturePages,
    );
    await PDFHelpers.mergeDocumentAtIndex(
      compiledDoc,
      subscriptionSignatureDoc,
      signaturePageStart + 1,
    );

    // // Add Advisory Agreement Signature Page
    const advisoryDoc = await PDFDocument.load(advisoryAgreementTemplate);
    await PDFHelpers.mergeDocumentAtIndex(
      compiledDoc,
      advisoryDoc,
      compiledDoc.getPageCount(),
    );

    return compiledDoc;
  }

  async mergeSADocs(
    dealDocumentTemplate: Buffer | Uint8Array,
    subscriptionSignatureTemplate: Buffer | Uint8Array,
    managerSignatureTemplate: Buffer | Uint8Array,
  ): Promise<PDFDocument> {
    const signaturePageStart = 14;
    const compiledDoc = await PDFDocument.load(dealDocumentTemplate);
    const subscriptionSignatureDoc = await PDFDocument.load(
      subscriptionSignatureTemplate,
    );
    const managerSignatureDoc = await PDFDocument.load(
      managerSignatureTemplate,
    );

    await PDFHelpers.removePages(
      compiledDoc,
      signaturePageStart,
      compiledDoc.getPageCount() - 1,
    );

    await PDFHelpers.mergeDocumentAtIndex(
      compiledDoc,
      subscriptionSignatureDoc,
      compiledDoc.getPageCount(),
    );
    await PDFHelpers.mergeDocumentAtIndex(
      compiledDoc,
      managerSignatureDoc,
      compiledDoc.getPageCount(),
    );
    return compiledDoc;
  }

  async resolveFillableData(
    close: Close,
    closeDate?: string,
    organizerSignature?: string,
    fundManagerSignature?: string,
  ): Promise<FillableData> {
    const resolvedData: FillableData = {
      organizerName: {
        type: 'text',
      },
      organizerSignaturePrint: {
        type: 'text',
      },
      organizerSignatureTitle: {
        type: 'text',
      },
      organizerInfoBlock: {
        type: 'text',
      },
      organizerSignature: {
        type: 'signature',
        value: organizerSignature,
      },
      managerName: {
        type: 'text',
      },
      managerSignaturePrint: {
        type: 'text',
      },
      managerSignatureTitle: {
        type: 'text',
        value: 'Manager',
      },
      managerInfoBlock: {
        type: 'text',
      },
      managerSignature: {
        type: 'signature',
        value: fundManagerSignature,
      },
      fundManagerName: {
        type: 'text',
      },
      fundManagerSignaturePrint: {
        type: 'text',
      },
      fundManagerSignatureTitle: {
        type: 'text',
      },
      fundManagerInfoBlock: {
        type: 'text',
      },
      fundManagerSignature: {
        type: 'signature',
        value: fundManagerSignature,
      },
      date: {
        type: 'text',
      },
      agreementTitle: {
        type: 'text',
        value: 'LIMITED LIABILITY COMPANY OPERATING AGREEMENT',
      },
      entityLegalName: {
        type: 'text',
        textAlignment: 'center',
      },
      fundEntityInfoBlock: {
        type: 'text',
      },
    };
    const entity = await this.entityRepository.findById(close.entityId);
    const deal = await this.dealRepository.findById(entity.dealId);
    const organizerProfile = await this.profileRepository.findById(
      deal.profileId,
    );
    resolvedData.organizerSignatureTitle.value = 'Organizer';
    resolvedData.entityLegalName.value = entity.name;
    resolvedData.date.value = moment(close.dateClosed).format('MM/DD/YY');
    //TODO: This needs to be considered and resolved from entity data
    //TODO: (Currently not stored)
    resolvedData.fundEntityInfoBlock.value = `a Delaware Limited Liability Company`;
    switch (organizerProfile.taxDetails?.registrationType) {
      case 'ENTITY':
      case 'JOINT':
      case 'TRUST':
        resolvedData.organizerName.value = `${organizerProfile.name}`;
        resolvedData.organizerSignaturePrint.value = `${organizerProfile.firstName} ${organizerProfile.lastName}`;
        resolvedData.organizerInfoBlock.value = `a ${
          organizerProfile.stateOfFormation
        } ${organizerProfile.typeOfEntity ?? 'Entity'}`;
        break;
      case 'INDIVIDUAL':
        resolvedData.organizerName.value = `${organizerProfile.firstName} ${organizerProfile.lastName}`;
        resolvedData.organizerSignaturePrint.value = `${organizerProfile.firstName} ${organizerProfile.lastName}`;
        resolvedData.organizerInfoBlock.value = `a ${organizerProfile.stateOfFormation} Individual`;
        break;
      default:
        throw new Error(
          'Unknown Entity type, unable to resolve Organizer data',
        );
    }
    if (await this.isGBTMaster(entity)) {
      // TODO: Move this to the DB
      resolvedData.fundManagerName.value = 'Jeremy Neilson';
      resolvedData.fundManagerSignaturePrint.value = 'Jeremy Neilson';
      resolvedData.fundManagerSignatureTitle.value = 'Fund Manager';
      resolvedData.managerName.value = 'Glassboard Management II, LLC';
      resolvedData.managerSignaturePrint.value =
        'Glassboard Management II, LLC';
      resolvedData.fundManagerSignatureTitle.value = 'Manager';
      resolvedData.managerInfoBlock.value = 'a Utah Limited Liability Company';
      return resolvedData;
    }
    const tenant = await this.tenantRepository.findById(
      String(entity.tenantId),
    );
    const managerId =
      entity.managerId ?? organizerProfile.managerId ?? tenant.managerId;
    const manager = await this.companyRepository.findById(String(managerId));
    resolvedData.fundManagerName.value = manager.name;
    resolvedData.fundManagerSignaturePrint.value = manager.name;
    resolvedData.fundManagerSignatureTitle.value = 'Fund Manager';
    resolvedData.managerName.value = manager.name;
    resolvedData.managerSignaturePrint.value = manager.name;
    resolvedData.managerSignatureTitle.value = 'Manager';
    resolvedData.managerInfoBlock.value = `a ${manager.stateOfFormation} ${manager.entityType}`;
    return resolvedData;
  }

  async isGBTMaster(entity: Entities) {
    // TODO: This seems pretty fragile, should use other criteria?
    // (Presence of Master / Manager ID?)
    return entity.name.search('Glassboard Master II') > -1;
  }
  async isMultiStepSigningTenant(tenant: Tenant) {
    return !!tenant?.settings?.useMultiStepSigningForCloses;
  }

  async processSubscriptionsForClose(
    close: Close | any,
    entity: Entities,
    closeDate?: Date,
    organizerSignature?: string,
    fundManagerSignature?: string,
  ) {
    if (!close.subscriptions) {
      throw new Error('Close is missing subscriptions');
    }
    this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({
      id: String(entity.tenantId),
    });
    const entityDocuments = await this.getEntityDocuments(entity);

    /// clear disclaimer and update
    //
    debug('processSubscriptionsForClose:entityDocuments', entityDocuments);

    const fillableData = await this.resolveFillableData(
      close,
      moment(closeDate).format('MM-DD-YYYY'),
      organizerSignature,
      fundManagerSignature,
    );
    for (const subscriptionId of close.subscriptions) {
      const subscription = await this.subscriptionRepository.findById(
        subscriptionId,
      );
      // if (subscription.status === 'CLOSED') {
      //   debug(`Skipping already closed subscription ${subscription.id}`);
      //   continue;
      // }
      const subscriptionDocuments = await this.getSubscriptionDocuments(
        subscription,
      );
      const subscriptionOA = subscriptionDocuments.find(
        doc => doc.documentType === DocType.OperatingAgreement,
      );
      const subscriptionSA = subscriptionDocuments.find(
        doc => doc.documentType === DocType.SubscriptionAgreement,
      );
      const otherSubscriptionDocuments = subscriptionDocuments.filter(
        doc =>
          ![DocType.OperatingAgreement, DocType.SubscriptionAgreement].includes(
            doc.documentType,
          ),
      );

      const filledOperatingAgreement = await this.mergeOADocs(
        await PDFHelpers.populateTemplate(
          entityDocuments[DocType.OperatingAgreement].data,
          fillableData,
        ),
        await PDFHelpers.populateTemplate(subscriptionOA!.data, fillableData),
        await PDFHelpers.populateTemplate(
          await this.getManagerTemplateFile('managerOATemplate'),
          fillableData,
        ),
        await PDFHelpers.populateTemplate(
          await this.getManagerTemplateFile('managerAdvisorAgreementTemplate'),
          fillableData,
        ),
      );

      // Update subscription contribution
      const profile = await this.profileRepository.findById(
        subscription.profileId,
      );
      const isIndividual =
        profile?.taxDetails?.registrationType === 'INDIVIDUAL' ||
        profile?.taxDetails?.registrationType === 'JOINT';

      let expenseAmt = 0;
      const own_percent =
        (subscription?.amount ?? 1) / close?.statement?.totalProceeds;
      if (entity?.expenseReserve?.type === 'Flat Fee') {
        expenseAmt = (entity?.expenseReserve?.amount ?? 1) * own_percent;
      } else if (entity?.expenseReserve?.type === 'Percent') {
        expenseAmt =
          (close?.statement?.totalProceeds ?? 1) *
          (entity?.expenseReserve?.amount ?? 1) *
          0.01 *
          own_percent;
      }

      const totalAmt = (subscription?.amount ?? 1) - expenseAmt;

      const pdfDoc = await PDFDocument.load(subscriptionSA!.data);
      const form = pdfDoc.getForm();

      // Determine version
      const individualDisclaimerField = form.getFieldMaybe(
        'individualDisclaimer',
      );

      if (individualDisclaimerField) {
        const fields = form.getFields();

        fields.forEach(field => {
          const name = field.getName();
          if (name === 'individualDisclaimer' || name === 'entityDisclaimer') {
            (field as PDFTextField).setText('');
          } else if (!isIndividual && name === 'entityExpenseContribution') {
            (field as PDFTextField).setText(formatMoney(expenseAmt));
          } else if (!isIndividual && name === 'entityTotalSubscription') {
            (field as PDFTextField).setText(formatMoney(totalAmt));
          } else if (isIndividual && name === 'individualExpenseContribution') {
            (field as PDFTextField).setText(formatMoney(expenseAmt));
          } else if (isIndividual && name === 'individualTotalSubscription') {
            (field as PDFTextField).setText(formatMoney(totalAmt));
          }
        });
      } else {
        const individualInvestmentAmount = form.getFieldMaybe(
          'individualInvestmentAmount',
        );
        if (individualInvestmentAmount) {
          (individualInvestmentAmount as PDFTextField).setText(' ');
        }
        const entityInvestmentAmount = form.getFieldMaybe(
          'entityInvestmentAmount',
        );
        if (entityInvestmentAmount) {
          (entityInvestmentAmount as PDFTextField).setText(' ');
        }

        const pages = pdfDoc.getPages();
        const page = pages[isIndividual ? 0 : 1];
        if (isIndividual) {
          //blank values
          page.drawRectangle({
            x: 165,
            y: 456,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          page.drawRectangle({
            x: 185,
            y: 430,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          page.drawRectangle({
            x: 205,
            y: 400,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          //fill values
          page.drawText(formatMoney(subscription.amount), {
            x: 170,
            y: 458,
            size: 11,
            opacity: 1,
          });
          page.drawText(formatMoney(expenseAmt), {
            x: 185,
            y: 432,
            size: 11,
            opacity: 1,
          });
          page.drawText(formatMoney(totalAmt), {
            x: 209,
            y: 402,
            size: 11,
            opacity: 1,
          });
        } else {
          //blank values
          page.drawRectangle({
            x: 162,
            y: 491,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          page.drawRectangle({
            x: 180,
            y: 465,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          page.drawRectangle({
            x: 204,
            y: 436,
            width: 250,
            height: 18,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          //fill values
          page.drawText(formatMoney(subscription.amount), {
            x: 171,
            y: 493,
            size: 11,
            opacity: 1,
          });
          page.drawText(formatMoney(expenseAmt), {
            x: 184,
            y: 468,
            size: 11,
            opacity: 1,
          });
          page.drawText(formatMoney(totalAmt), {
            x: 209,
            y: 438,
            size: 11,
            opacity: 1,
          });
        }
        const investmentAmount = form.getFieldMaybe('investmentAmount');
        if (investmentAmount) {
          (investmentAmount as PDFTextField).setText(formatMoney(totalAmt));
        } else {
          pages[2].drawRectangle({
            x: 110,
            y: 579,
            width: 350,
            height: 12,
            color: rgb(1, 1, 1),
            opacity: 1,
            borderOpacity: 0,
          });
          //fill values
          pages[2].drawText(formatMoney(totalAmt), {
            x: 110,
            y: 581,
            size: 11,
            opacity: 1,
          });
        }
      }
      const newsubscriptionSA = await pdfDoc.save();

      const filledSubscriptionAgreement = await this.mergeSADocs(
        await PDFHelpers.populateTemplate(
          entityDocuments[DocType.SubscriptionAgreement].data,
          fillableData,
        ),
        newsubscriptionSA ?? subscriptionSA!.data,
        await PDFHelpers.populateTemplate(
          await this.getManagerTemplateFile('managerSATemplate'),
          fillableData,
        ),
      );

      await PDFHelpers.mergeDocumentAtIndex(
        filledOperatingAgreement,
        filledSubscriptionAgreement,
        filledOperatingAgreement.getPageCount(),
      );

      await PDFHelpers.mergeDocumentAtIndex(
        filledOperatingAgreement,
        await PDFDocument.load(
          entityDocuments[DocType.PrivatePlacementMemorandum].data,
        ),
        filledOperatingAgreement.getPageCount(),
      );

      for (const extraDocument of otherSubscriptionDocuments) {
        await PDFHelpers.mergeDocumentAtIndex(
          filledOperatingAgreement,
          await PDFDocument.load(
            await PDFHelpers.populateTemplate(
              extraDocument!.data,
              fillableData,
            ),
          ),
          filledOperatingAgreement.getPageCount(),
        );
      }

      const fileName = `${subscription.name} - Close Document - ${closeDate}.pdf`;
      const fileUUID = uuidv4();
      /* eslint-disable no-control-regex */
      await this.minio.putObject(
        fileUUID,
        Buffer.from((await filledOperatingAgreement.save()).buffer),
        {
          'Content-Type': 'application/pdf',
          'Content-Disposition':
            'attachment; filename=' +
            fileName
              .replace(/\s\s+/g, ' ')
              .replace(/[^\x00-\x7F]/g, '')
              .trim(),
        },
      );

      const file = await this.fileRepository.create({
        key: fileUUID,
        name: fileName,
        tenantId: subscription.tenantId,
      });
      subscription.closeDoc = file.id;
      if (fundManagerSignature) {
        subscription.status = 'CLOSED';
      }
      await this.subscriptionRepository.update(subscription);
    }
  }

  // async processSubscriptionsExistingCloseDoc(
  //   close: Close,
  //   entity: Entities,
  //   closeDate?: string,
  //   organizerSignature?: string,
  //   fundManagerSignature?: string,
  // ) {
  //   if (!close.subscriptions) {
  //     throw new Error('Close is missing subscriptions');
  //   }
  //   this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({
  //     id: String(entity.tenantId),
  //   });
  //   const fillableData = await this.resolveFillableData(
  //     close,
  //     closeDate,
  //     organizerSignature,
  //     fundManagerSignature,
  //   );
  //   for (const subscriptionId of close.subscriptions) {
  //     const subscription = await this.subscriptionRepository.findById(
  //       subscriptionId,
  //     );
  //     if (!subscription.closeDoc) {
  //       throw new Error(
  //         `Subscription ${subscription.id} is missing Close Doc, unable to countersign`,
  //       );
  //     }
  //     const fileObject = await this.fileRepository.findById(
  //       subscription.closeDoc,
  //     );
  //     const closeDoc = await PDFHelpers.convertStreamToBuffer(
  //       await this.minio.getObject(fileObject.key),
  //     );
  //     const newFile = await PDFHelpers.populateTemplate(
  //       closeDoc,
  //       fillableData,
  //       true,
  //     );
  //
  //     /* eslint-disable no-control-regex */
  //     await this.minio.putObject(fileObject.key, Buffer.from(newFile.buffer), {
  //       'Content-Type': 'application/pdf',
  //       'Content-Disposition':
  //         'attachment; filename=' +
  //         fileObject.name
  //           .replace(/\s\s+/g, ' ')
  //           .replace(/[^\x00-\x7F]/g, '')
  //           .trim(),
  //     });
  //     subscription.status = 'CLOSED';
  //     await this.subscriptionRepository.update(subscription);
  //   }
  // }

  async getDocumentEntry(
    fileId: string,
    documentType?: DocType,
  ): Promise<DocumentEntry> {
    const fileObject = await this.fileRepository.findById(fileId);
    if (!documentType) {
      documentType = await this.resolveFileNameToDocType(fileObject.name);
    }
    return {
      documentType: documentType ?? DocType.Unknown,
      file: fileObject,
      data: await PDFHelpers.convertStreamToBuffer(
        await this.minio.getObject(fileObject.key),
      ),
    };
  }

  async getEntityDocuments({entityDocuments}: Entities) {
    if (
      !entityDocuments ||
      !entityDocuments.operatingAgreement ||
      !entityDocuments.privatePlacementMemorandum ||
      !entityDocuments.subscriptionAgreement
    ) {
      throw new Error('Entity is missing Documents');
    }
    const documents: {
      [DocType.OperatingAgreement]: DocumentEntry;
      [DocType.PrivatePlacementMemorandum]: DocumentEntry;
      [DocType.SubscriptionAgreement]: DocumentEntry;
    } = {
      [DocType.OperatingAgreement]: await this.getDocumentEntry(
        entityDocuments.operatingAgreement,
        DocType.OperatingAgreement,
      ),
      [DocType.PrivatePlacementMemorandum]: await this.getDocumentEntry(
        entityDocuments.privatePlacementMemorandum,
        DocType.PrivatePlacementMemorandum,
      ),
      [DocType.SubscriptionAgreement]: await this.getDocumentEntry(
        entityDocuments.subscriptionAgreement,
        DocType.SubscriptionAgreement,
      ),
    };
    return documents;
  }

  async getSubscriptionDocuments({
    id,
    files: fileIds,
  }: Subscription): Promise<DocumentEntry[]> {
    if (!fileIds) {
      throw new Error(`Subscription ${id} is missing Documents`);
    }
    const files: DocumentEntry[] = [];
    for (const fileId of fileIds) {
      try {
        const document = await this.getDocumentEntry(fileId);
        files.push(document);
      } catch (e) {
        debug(`Failed to resolve Document Entry for file ${fileId}`);
      }
    }
    return files;
  }

  async getManagerTemplateFile(name: string) {
    return fs.readFileSync(
      path.join(
        process.cwd(),
        'src',
        'documentTemplates',
        'manager',
        `${name}.pdf`,
      ),
    );
  }
}

// Adding re-process documents in closing
