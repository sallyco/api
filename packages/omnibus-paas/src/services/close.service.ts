import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import {repository} from '@loopback/repository';
import {ClosesList} from '../models/closes-list.model';
import {CloseRepository} from '../repositories';

@injectable({scope: BindingScope.TRANSIENT})
export class CloseService {
  constructor(
    @repository(CloseRepository)
    public closeRepository: CloseRepository,
  ) {}

  /*
   * Add service methods here
   */
  async getClosesByDeal(dealId, {page, perPage}) {
    const countObj = await this.closeRepository.count({
      and: [{isDeleted: false}, {dealId: dealId}],
    });

    return new ClosesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.closeRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: {and: [{isDeleted: false}, {dealId: dealId}]},
      }),
    });
  }
}
