import {ILegalInc} from 'interfaces/services/legal-inc.interface';
import {CompanyWithRelations, ProfileWithRelations} from 'models';

export function getDefaultEINOrderAddress() {
  // TODO: Move this info out of the code
  const orderAddress: ILegalInc.API.Address = {
    address_type: 'billing',
    country: 'United States of America',
    address: '6510 Millrock Drive',
    address2: 'Suite 400',
    city: 'Salt Lake City',
    state: 'UT',
    zip: '84121',
  };
  return orderAddress;
}

export function buildEINOrderAddress({
  address1,
  address2,
  city,
  state,
  postalCode,
  country,
}: {
  address1?: string;
  address2?: string;
  city?: string;
  state?: string;
  postalCode?: string;
  country?: string;
}): ILegalInc.API.Address {
  if (!address1 || !city || !state || !postalCode || !country) {
    throw new Error('Unable to build address');
  }
  return {
    address_type: 'billing',
    country,
    address: address1,
    address2: address2 ?? '',
    city,
    state,
    zip: postalCode,
  };
}

/**
 * @deprecated Global Fallback Tax contact is an anti-pattern
 * @param orderAddress
 */
export function getDefaultEINTaxContact(orderAddress: ILegalInc.API.Address) {
  // TODO: Move this info out of the code
  const orderTaxContact: ILegalInc.API.Tax = {
    first_name: 'Jeremy',
    last_name: 'Neilson',
    contact_type: 'tax',
    ssn: process.env.LEGALINC_CONTACT_SSN ?? '111-11-1111',
    addresses: [orderAddress],
    phones: [
      {
        number: '877-492-7555',
        phone_type: 'tax',
      },
    ],
  };

  return orderTaxContact;
}

export function getTaxContactFromProfileOrManager(
  data: Partial<ProfileWithRelations> & Partial<CompanyWithRelations>,
) {
  const ssn = getSSNFromTaxDetails(data);
  let firstName = data?.firstName;
  let lastName = data?.lastName;
  let phone = data?.phone;
  if (data?.primarySignatory) {
    firstName = data?.primarySignatory?.name?.split(' ')[0] ?? firstName;
    lastName = data?.primarySignatory?.name?.split(' ')[1] ?? lastName;
    phone = data?.primarySignatory?.phone ?? phone;
  }
  if (data?.taxContact) {
    firstName = data?.taxContact?.name?.split(' ')[0] ?? firstName;
    lastName = data?.taxContact?.name?.split(' ')[1] ?? lastName;
    phone = data?.taxContact?.phone ?? phone;
  }
  const address =
    data?.address ?? data.primarySignatory?.address ?? data.taxContact?.address;
  if (!ssn) {
    throw new Error('Unable to resolve SSN');
  }
  if (!firstName || !lastName || !phone) {
    throw new Error('Unable to resolve contact details');
  }
  if (!address) {
    throw new Error('Unable to resolve address');
  }
  return {
    first_name: firstName,
    last_name: lastName,
    contact_type: 'tax',
    ssn,
    addresses: [buildEINOrderAddress(address)],
    phones: [
      {
        number: phone,
        phone_type: 'tax',
      },
    ],
  };
}

const getSSNFromTaxDetails = (
  data: Partial<ProfileWithRelations> & Partial<CompanyWithRelations>,
): string | false => {
  let ssn: undefined | string;
  if (
    data?.taxDetails?.taxIdentification?.type &&
    data?.taxDetails?.taxIdentification.type.toLowerCase() === 'ssn'
  ) {
    ssn = data?.taxDetails?.taxIdentification?.value;
  }
  if (
    data?.primarySignatory?.taxDetails?.type &&
    data?.primarySignatory?.taxDetails?.type.toLowerCase() === 'ssn'
  ) {
    ssn = data?.primarySignatory?.taxDetails?.value;
  }
  if (
    data?.taxContact?.taxDetails?.type &&
    data?.taxContact?.taxDetails.type.toLowerCase() === 'ssn'
  ) {
    ssn = data?.taxDetails?.taxIdentification?.value;
  }
  if (
    data?.taxContact?.taxIdType &&
    data?.taxContact?.taxIdType.toLowerCase() === 'ssn' &&
    data?.taxContact?.taxId
  ) {
    ssn = data?.taxContact?.taxId;
  }
  return ssn ?? false;
};
