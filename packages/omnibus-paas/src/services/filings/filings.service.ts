import {BindingScope, Context, injectable, service} from '@loopback/core';
import {LegalInc, LegalIncProvider} from './legal-inc.service';
import {
  CompanyWithRelations,
  FormationRequest,
  FormationRequestEINRequest,
  FormationRequestStatus,
  ProfileWithRelations,
  TenantWithRelations,
} from '../../models';
import {ILegalInc} from '../../interfaces/services/legal-inc.interface';
import {Company, Deal, Entities, File, LegalIncOrderModel} from '../../models';
import {repository, WhereBuilder} from '@loopback/repository';
import {
  CompanyRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  FormationRequestRepository,
  ProfileRepository,
  TenantRepository,
} from '../../repositories';
import {Unleash, UnleashProvider} from '../system/unleash.service';
import debugFactory from 'debug';
import {CeleryClientService} from '../celery/celery-client.service';
import assert from 'assert';
import {PassThrough} from 'stream';
import axios from 'axios';
import * as uuid from 'uuid';
import {MinIOService} from '../documents/minio.service';
import {MinIOServiceBindings} from '../../keys';
import {inject} from '@loopback/context';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {
  getDefaultEINOrderAddress,
  getTaxContactFromProfileOrManager,
} from './filings.service.helpers';
import StateId = ILegalInc.Data.StateId;
import Tax = ILegalInc.API.Tax;
import {SystemNotificationsService} from '../system/system-notifications.service';

const debug = debugFactory('omnibus-paas:services:legalinc');

enum OrderTypes {
  'LLC' = 'LLC',
  'SERIES_LLC' = 'SERIES_LLC',
  'LP' = 'LP',
  'SERIES_LP' = 'SERIES_LP',
}

const PackageAndProducts = {
  [OrderTypes.LLC]: {
    product_id: '1,42',
    package_id: '135',
  },
  [OrderTypes.SERIES_LLC]: {
    product_id: '16',
    package_id: '135',
  },
  [OrderTypes.LP]: {
    product_id: '7,42',
    package_id: '247',
  },
  [OrderTypes.SERIES_LP]: {
    product_id: '16',
    package_id: '135',
  },
};

@injectable({scope: BindingScope.TRANSIENT})
export class FilingsService {
  private formationRequestStatusMap = {
    [ILegalInc.API.OrderStatus.NEW]: FormationRequestStatus.ORDER_PLACED,
    [ILegalInc.API.OrderStatus.COMPLETED]: FormationRequestStatus.FULFILLED,
    [ILegalInc.API.OrderStatus.FULFILLMENT]: FormationRequestStatus.PROCESSING,
    [ILegalInc.API.OrderStatus.REVIEW]: FormationRequestStatus.PROCESSING,
    [ILegalInc.API.OrderStatus.AT_STATE]: FormationRequestStatus.PROCESSING,
    [ILegalInc.API.OrderStatus.CANCELLED]: FormationRequestStatus.EXCEPTION,
    [ILegalInc.API.OrderStatus.EXCEPTION]: FormationRequestStatus.EXCEPTION,
  };

  constructor(
    @inject.context() private ctx: Context,
    @service(LegalIncProvider) private readonly legalInc: LegalInc,
    @repository(EntitiesRepository)
    private readonly entityRepository: EntitiesRepository,
    @service(UnleashProvider) private readonly unleash: Unleash,
    @repository(CompanyRepository)
    private readonly companyRepository: CompanyRepository,
    @repository(FileRepository)
    private readonly fileRepository: FileRepository,
    @repository(TenantRepository)
    private readonly tenantRepository: TenantRepository,
    @service(CeleryClientService)
    private readonly celeryClient: CeleryClientService,
    @inject(MinIOServiceBindings.MINIO_SERVICE)
    private readonly minioService: MinIOService,
    @repository(FormationRequestRepository)
    private readonly formationRequestRepository: FormationRequestRepository,
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(ProfileRepository)
    private readonly profileRepository: ProfileRepository,
    @service(SystemNotificationsService)
    private readonly systemNotifications: SystemNotificationsService,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    private readonly tenant?: Tenant,
  ) {}

  // This should be moved into a LegalInc specific service
  async getLegalIncToken() {
    const data = await this.legalInc.getToken(
      process.env.LEGALINC_CLIENT_ID ?? '51270',
      process.env.LEGALINC_CLIENT_SECRET ?? 'xHtIgBxVzuKkUo',
      process.env.LEGALINC_USERNAME ?? 'tcoffelt@glassboardtech.com',
      process.env.LEGALINC_PASSWORD ?? 'yN2!jK1}kC',
    );
    return data.access_token;
  }

  async createEINOrder(
    formationRequest: FormationRequestEINRequest,
  ): Promise<FormationRequest> {
    let orderAddress: ILegalInc.API.Address = {
      address_type: 'billing',
      country: 'United States of America',
      address: '6510 Millrock Drive',
      address2: 'Suite 400',
      city: 'Salt Lake City',
      state: 'UT',
      zip: '84121',
    };
    let orderTaxContact: ILegalInc.API.Tax = {
      first_name: 'Jeremy',
      last_name: 'Neilson',
      contact_type: 'tax',
      ssn: process.env.LEGALINC_CONTACT_SSN ?? '111-11-1111',
      addresses: [orderAddress],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'tax',
        },
      ],
    };
    let masterName = 'Glassboard Master II, LLC';
    // Get the Manager Entity Items
    if (this.tenant) {
      const tenant = await this.tenantRepository.findById(this.tenant?.id);

      // Substitute Details from the Manager object
      orderAddress = {
        address_type: 'billing',
        country: formationRequest.taxContact.address.country,
        address: formationRequest.taxContact.address.street1,
        address2: formationRequest.taxContact.address?.street2,
        city: formationRequest.taxContact.address.city,
        state: formationRequest.taxContact.address.state,
        zip: formationRequest.taxContact.address.postalCode,
      };

      orderTaxContact = {
        first_name: formationRequest.taxContact.firstName,
        last_name: formationRequest.taxContact.lastName,
        contact_type: 'tax',
        ssn: formationRequest?.taxContact?.taxDetails.value,
        addresses: [orderAddress],
        phones: [
          {
            number: formationRequest.taxContact.phone,
            phone_type: 'tax',
          },
        ],
      };
      if (tenant.masterEntityId) {
        const masterEntity = await this.companyRepository.findById(
          tenant.masterEntityId,
        );
        if (masterEntity) {
          masterName = masterEntity.name;
        }
      }
    }

    const legalIncToken = await this.getLegalIncToken();

    const orderContact: ILegalInc.API.Contact = {
      business_type: 'LLC',
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'billing',
      addresses: [orderAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'billing',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'billing',
        },
      ],
    };
    const primaryOrderContact: ILegalInc.API.Contact = {
      business_type: 'LLC',
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'primary',
      addresses: [orderAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'primary',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'primary',
        },
      ],
    };
    // For GBT Master Series
    // ap@glassboardtech.com
    // Company Contact == Billing Contact
    // Tax = Fund Manager / Organizer w/SSN

    const orderData: ILegalInc.API.Order = {
      description: 'EIN Obtainment for ' + formationRequest.entity.name,
      status_id: 1,
      payment_by_card: 1,
      client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
      total_price: 49,
      affiliate_id: 1,
      service_level_id: 2,
      product_id: '16',
      package_id: '135',
      state_id: '9',
      order_contact: [primaryOrderContact, orderContact],
      external_id: '',
      matter_number: '',
      requester_name: '',
      tax: [orderTaxContact],
      // If Master Series, we need to change notes to 'Create this LLC as a Master LLC'
      note: `Create as series under ${masterName}`,
      registered_agent: false,
      company: {
        name: formationRequest.entity.name,
        company_type: 'LLC',
        client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
        registered_state_id: '9',
        domestic_state_id: '9',
        fiscal_year: '12/31',
        purpose: '7',
        address: orderAddress,
        contacts: [primaryOrderContact, orderContact],
      },
    };

    const orderResponseData = await this.legalInc.createOrder(
      legalIncToken,
      orderData,
    );
    if (!orderResponseData.success) {
      throw new Error(orderResponseData.message);
    }
    const OrderResponse: ILegalInc.API.Order = JSON.parse(
      orderResponseData.response,
    );
    const paymentData: ILegalInc.API.Payment = {
      amount: OrderResponse.total_price,
      order_id: OrderResponse.id!,
      payer_id: parseInt(process.env.LEGALINC_CLIENT_ID ?? '51270'),
    };
    const paymentResponseData = await this.legalInc.createPayment(
      legalIncToken,
      paymentData,
    );
    if (!paymentResponseData.success) {
      throw new Error(paymentResponseData.message);
    }

    return new FormationRequest({
      status: FormationRequestStatus.ORDER_PLACED,
      tenantId: this?.tenant?.id ?? 'gbt',
      entity: {
        name: formationRequest.entity.name,
        type: OrderResponse.company.company_type,
        stateOfFormation: OrderResponse.company.state!.name,
      },
      orderId: String(OrderResponse.id),
      taxContact: {
        ...formationRequest.taxContact,
      },
      providerMeta: {
        typeId: 'LEGALINC',
        ...OrderResponse,
      },
    });
  }

  async createEntityEINOrder(entity: Entities, deal: Deal): Promise<boolean> {
    if (
      !this.unleash.isEnabled('create_legalinc_ein', {
        userId: entity.tenantId,
      })
    ) {
      return true;
    }

    try {
      assert(entity.tenantId);
    } catch (e) {
      debug('Entity is missing Tenant ID. Aborting.');
      return false;
    }

    try {
      const defaultOrderAddress = getDefaultEINOrderAddress();

      const tenant: TenantWithRelations = await this.tenantRepository.findById(
        entity.tenantId,
      );
      const organizerProfile: ProfileWithRelations =
        await this.profileRepository.findById(deal.profileId);

      const useOrganizer = tenant?.settings?.useOrganizerForEINOrders === true;

      const orderTaxContact: Tax = await this.resolveTaxContact(
        useOrganizer,
        organizerProfile,
        tenant,
      );

      const legalIncToken = await this.getLegalIncToken();

      let masterName = 'Glassboard Master II, LLC';
      if (entity.masterEntityId) {
        const company = await this.companyRepository.findById(
          entity.masterEntityId,
        );
        if (company.name) {
          masterName = company.name;
        }
      }

      const orderContact: ILegalInc.API.Contact = {
        business_type: 'LLC',
        first_name: 'Robb',
        last_name: 'Kunz',
        contact_type: 'billing',
        addresses: [defaultOrderAddress],
        emails: [
          {
            email: 'ap@glassboardtech.com',
            email_type: 'billing',
          },
        ],
        phones: [
          {
            number: '877-492-7555',
            phone_type: 'billing',
          },
        ],
      };
      const primaryOrderContact: ILegalInc.API.Contact = {
        business_type: 'LLC',
        first_name: 'Robb',
        last_name: 'Kunz',
        contact_type: 'primary',
        addresses: [defaultOrderAddress],
        emails: [
          {
            email: 'ap@glassboardtech.com',
            email_type: 'primary',
          },
        ],
        phones: [
          {
            number: '877-492-7555',
            phone_type: 'primary',
          },
        ],
      };
      // For GBT Master Series
      // ap@glassboardtech.com
      // Company Contact == Billing Contact
      // Tax = Fund Manager / Organizer w/SSN

      const orderData: ILegalInc.API.Order = {
        description: 'EIN Obtainment for ' + deal.name,
        status_id: 1,
        payment_by_card: 1,
        client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
        total_price: 49,
        affiliate_id: 1,
        service_level_id: 2,
        product_id: '16',
        package_id: '135',
        state_id: '9',
        order_contact: [primaryOrderContact, orderContact],
        external_id: '',
        matter_number: '',
        requester_name: '',
        tax: [orderTaxContact],
        // If Master Series, we need to change notes to 'Create this LLC as a Master LLC'
        note: `Create as series under ${masterName}`,
        registered_agent: false,
        company: {
          name: entity.name,
          company_type: 'LLC',
          client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
          registered_state_id: '9',
          domestic_state_id: '9',
          fiscal_year: '12/31',
          purpose: '7',
          address: defaultOrderAddress,
          contacts: [primaryOrderContact, orderContact],
        },
      };

      const orderResponseData = await this.legalInc.createOrder(
        legalIncToken,
        orderData,
      );
      if (!orderResponseData.success) {
        throw new Error(orderResponseData.message);
      }
      const OrderResponse = JSON.parse(orderResponseData.response);
      const paymentData: ILegalInc.API.Payment = {
        amount: OrderResponse.total_price,
        order_id: OrderResponse.id,
        payer_id: parseInt(process.env.LEGALINC_CLIENT_ID ?? '51270'),
      };
      const paymentResponseData = await this.legalInc.createPayment(
        legalIncToken,
        paymentData,
      );
      if (!paymentResponseData.success) {
        throw new Error(paymentResponseData.message);
      }
      entity.legalIncOrder = new LegalIncOrderModel({
        status: ILegalInc.API.OrderStatus.NEW,
        orderId: OrderResponse.id,
      });
      await this.entityRepository.update(entity);

      if (
        ['qa', 'sandbox', 'local', 'development'].includes(
          process.env.ENVIRONMENT ?? 'local',
        )
      ) {
        entity.ein = '12-3' + Math.random().toString().slice(2, 8);
        await this.entityRepository.update(entity);
        await this.celeryClient.createEntityBankAccount(entity.id);
      }
      await this.systemNotifications.Notify_Successful_Entity_EIN_Order(
        entity,
        orderTaxContact,
      );
    } catch (e) {
      await this.systemNotifications.Notify_Failed_Entity_EIN_Order(entity);
      throw e;
    }
    return true;
  }

  async updateLegalIncEINOrderStatuses() {
    const legalIncToken = await this.getLegalIncToken();
    const legalIncOrderQuery = new WhereBuilder();
    legalIncOrderQuery.and({
      legalIncOrder: {
        exists: true,
        neq: null,
      },
      'legalIncOrder.status': {
        exists: true,
        neq: ILegalInc.API.OrderStatus.COMPLETED,
      },
      or: [
        {
          ein: {
            exists: false,
          },
        },
        {
          ein: {
            eq: null,
          },
        },
      ],
    });
    const entities = await this.entityRepository.find({
      where: legalIncOrderQuery.build(),
    });
    for (const entity of entities) {
      if (!entity.legalIncOrder?.orderId) {
        continue;
      }
      // Fetch the status of the order
      try {
        const orderObject = await this.legalInc.getOrder(
          legalIncToken,
          entity.legalIncOrder?.orderId ?? 0,
        );
        if (!orderObject.success) {
          debug(
            `LegalInc API response failure for Entity ${entity.id}`,
            orderObject.message,
          );
          continue;
        }
        const orderData: ILegalInc.Data.Order = JSON.parse(
          orderObject.response,
        );
        entity.legalIncOrder.status = orderData.status?.id;
        entity.legalIncOrder.statusDescription = orderData.status?.description;
        if (orderData.company?.ein && entity.ein !== orderData.company.ein) {
          entity.ein = orderData.company?.ein;
          debug(`Set EIN for Entity ${entity.id} to ${entity.ein}`);

          const EINDocuments = (orderData.document ?? []).filter(document => {
            return document.document_type === '4';
          });
          for (const document of EINDocuments) {
            const einFileId = await this.uploadEINDocumentFile(
              entity.tenantId!,
              entity.name,
              entity.ownerId!,
              document.download_url,
              document.uploaded_file_name,
            );
            entity.entityDocuments = {
              ...entity.entityDocuments,
              einLetter: einFileId,
            };
          }
        }
        await this.entityRepository.update(entity);

        if (
          orderData.status?.id === ILegalInc.API.OrderStatus.COMPLETED ||
          (entity?.ein && entity.ein.length > 0 && !entity?.bankAccount)
        ) {
          if (
            this.unleash.isEnabled('banking_account_creation', {
              userId: entity.tenantId,
            })
          ) {
            await this.celeryClient.createEntityBankAccount(entity.id);
          }
        }
      } catch (e) {
        debug(
          `
          Issue with getting updated LegalInc Order Status for Entity ${entity.id}
        `,
          e,
        );
      }
    }
  }

  async getLegalIncOrderDetails(
    orderId: string | number,
  ): Promise<ILegalInc.Data.Order> {
    const legalIncToken = await this.getLegalIncToken();
    const orderObject = await this.legalInc.getOrder(legalIncToken, orderId);
    return JSON.parse(orderObject.response);
  }

  async updateFormationRequests() {
    const legalIncToken = await this.getLegalIncToken();
    const legalIncOrderQuery = new WhereBuilder({
      status: {
        exists: true,
        neq: FormationRequestStatus.FULFILLED,
      },
      'providerMeta.typeId': 'LEGALINC',
    });
    const formationRequests = await this.formationRequestRepository.find(
      legalIncOrderQuery,
    );
    for (const formationRequest of formationRequests) {
      if (!formationRequest.providerMeta.id) {
        continue;
      }
      // Fetch the status of the order
      try {
        const orderObject = await this.legalInc.getOrder(
          legalIncToken,
          formationRequest.providerMeta.id,
        );
        if (!orderObject.success) {
          debug(
            `LegalInc API response failure for Order ${formationRequest.providerMeta.id}`,
            orderObject.message,
          );
          continue;
        }
        const orderData: ILegalInc.Data.Order = JSON.parse(
          orderObject.response,
        );
        formationRequest.status =
          this.formationRequestStatusMap[orderData.status!.id!] ??
          FormationRequestStatus.PROCESSING;

        if (orderData.company?.ein) {
          formationRequest.ein = orderData.company?.ein;

          const EINDocuments = (orderData.document ?? []).filter(document => {
            return document.document_type === '4';
          });
          formationRequest.documents = formationRequest.documents ?? [];
          for (const document of EINDocuments) {
            const einFileId = await this.uploadEINDocumentFile(
              formationRequest.tenantId,
              formationRequest.entity.name,
              formationRequest.ownerId,
              document.download_url,
              document.uploaded_file_name,
            );
            formationRequest.documents.push(einFileId);
          }
        }
        await this.formationRequestRepository.update(formationRequest);
      } catch (e) {
        debug(
          `
          Issue with getting updated LegalInc Order Status for Order ${formationRequest.providerMeta.id}
        `,
          e,
        );
      }
    }
  }

  async uploadEINDocumentFile(
    tenantId: string,
    entityName: string,
    ownerId: string,
    url: string,
    fileName: string,
  ): Promise<string> {
    const fileSuffix = fileName.split('.').pop();
    const fileStorageName = `EIN Letter for ${entityName}.${fileSuffix}`;
    const passThroughStream = new PassThrough();
    const responseStream = await axios.get(url, {
      responseType: 'stream',
    });
    const fileKey = uuid.v4();
    this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({
      id: tenantId,
    });
    const MinioPromise = this.minioService.putObject(
      fileKey,
      passThroughStream,
      {
        'Content-Type': responseStream.headers['content-type'],
        'Content-Disposition': `attachment; filename=EIN Letter.${fileSuffix}`,
      },
    );
    responseStream.data.pipe(passThroughStream);
    try {
      await MinioPromise;
    } catch (e) {
      debug('Failed to download file from LegalInc: ', e);
    }
    const file = new File({
      name: fileStorageName,
      type: fileSuffix,
      ownerId: ownerId,
      tenantId: tenantId,
      key: fileKey,
      lastModified: Date.now().toString(),
    });
    const einFile = await this.fileRepository.save(file);
    return String(einFile.id);
  }

  async createMasterFormationOrder(company: Company, manager: Company) {
    const orderAddress = {
      address_type: 'billing',
      country:
        manager?.taxContact?.address?.country ?? 'United States of America',
      address: manager?.taxContact?.address?.address1 ?? '6510 Millrock Drive',
      address2: manager?.taxContact?.address?.address2 ?? 'Suite 400',
      city: manager?.taxContact?.address?.city ?? 'Salt Lake City',
      state: manager?.taxContact?.address?.state ?? 'UT',
      zip: manager?.taxContact?.address?.postalCode ?? '84121',
    };

    const firstName = manager?.taxContact?.name
      ? manager?.taxContact?.name.split(' ')[0]
      : 'Jeremy';
    const lastName = manager?.taxContact?.name
      ? manager?.taxContact?.name.split(' ')[1]
      : 'Neilson';
    const orderTaxContact = {
      first_name: firstName,
      last_name: lastName,
      contact_type: 'tax',
      ssn: manager?.taxContact?.taxId ?? '',
      addresses: [orderAddress],
      phones: [
        {
          number: manager?.taxContact?.phone ?? '877-492-7555',
          phone_type: 'tax',
        },
      ],
    };

    const legalIncToken = await this.getLegalIncToken();

    const orderContact: ILegalInc.API.Contact = {
      business_type: 'LLC',
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'billing',
      addresses: [orderAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'billing',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'billing',
        },
      ],
    };
    const primaryOrderContact: ILegalInc.API.Contact = {
      business_type: 'LLC',
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'primary',
      addresses: [orderAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'primary',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'primary',
        },
      ],
    };
    // For GBT Master Series
    // ap@glassboardtech.com
    // Company Contact == Billing Contact
    // Tax = Fund Manager / Organizer w/SSN

    const orderData: ILegalInc.API.Order = {
      description: 'Domestic LLC Filing for ' + company.name,
      status_id: 1,
      payment_by_card: 1,
      client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
      total_price: 269,
      affiliate_id: 1,
      service_level_id: 2,
      product_id: '1,42',
      package_id: '135',
      state_id: '9',
      order_contact: [primaryOrderContact, orderContact],
      external_id: '',
      matter_number: '',
      requester_name: '',
      note: '',
      tax: [orderTaxContact],
      registered_agent: false,
      company: {
        name: company.name,
        company_type: 'LLC',
        client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
        registered_state_id: '9',
        domestic_state_id: '9',
        fiscal_year: '12/31',
        purpose: '7',
        address: orderAddress,
        contacts: [primaryOrderContact, orderContact],
      },
    };

    const orderResponseData = await this.legalInc.createOrder(
      legalIncToken,
      orderData,
    );
    if (!orderResponseData.success) {
      throw new Error(orderResponseData.message);
    }
    const OrderResponse = JSON.parse(orderResponseData.response);
    const paymentData: ILegalInc.API.Payment = {
      amount: OrderResponse.total_price,
      order_id: OrderResponse.id,
      payer_id: parseInt(process.env.LEGALINC_CLIENT_ID ?? '51270'),
    };
    const paymentResponseData = await this.legalInc.createPayment(
      legalIncToken,
      paymentData,
    );
    if (!paymentResponseData.success) {
      throw new Error(paymentResponseData.message);
    }
    company.legalIncOrder = new LegalIncOrderModel({
      status: ILegalInc.API.OrderStatus.NEW,
      orderId: OrderResponse.id,
    });
    await this.companyRepository.update(company);
  }

  async createEntityFormationOrder(formationRequest: FormationRequest) {
    const formationStateId =
      StateId[formationRequest.entity.stateOfFormation.toUpperCase()];
    if (!formationStateId) {
      throw new Error('Unable to resolve state name');
    }

    let packageType = OrderTypes.LLC;
    if (formationRequest.entity.type === 'LP') {
      packageType = OrderTypes.LP;
    }
    if (formationRequest.entity.isSeries) {
      if (formationRequest.entity.type === 'LLC') {
        packageType = OrderTypes.SERIES_LLC;
      } else if (formationRequest.entity.type === 'LP') {
        packageType = OrderTypes.SERIES_LP;
      }
    }
    const selectedPackage = PackageAndProducts[packageType];

    const entityAddress = {
      address_type: 'billing',
      country: String(formationRequest?.entity?.address?.country),
      address: String(formationRequest?.entity?.address?.street1),
      address2: String(formationRequest?.entity?.address?.street2),
      city: String(formationRequest?.entity?.address?.city),
      state: String(formationRequest?.entity?.address?.state),
      zip: String(formationRequest?.entity?.address?.postalCode),
    };

    const taxContactAddress = {
      address_type: 'billing',
      country: String(formationRequest.taxContact?.address?.country),
      address: String(formationRequest.taxContact?.address?.street1),
      address2: String(formationRequest.taxContact?.address?.street2),
      city: String(formationRequest.taxContact?.address?.city),
      state: String(formationRequest.taxContact?.address?.state),
      zip: String(formationRequest.taxContact?.address?.postalCode),
    };

    const billingAddress = {
      address_type: 'billing',
      country: 'United States of America',
      address: '6510 Millrock Drive',
      address2: 'Suite 400',
      city: 'Salt Lake City',
      state: 'UT',
      zip: '84121',
    };

    const firstName = String(formationRequest.taxContact.firstName);
    const lastName = String(formationRequest.taxContact.lastName);
    const orderTaxContact = {
      business_type: formationRequest.entity.type,
      first_name: firstName,
      last_name: lastName,
      contact_type: 'tax',
      ssn: String(formationRequest.taxContact.taxDetails?.value),
      addresses: [taxContactAddress],
      phones: [
        {
          number: String(formationRequest.taxContact.phone),
          phone_type: 'tax',
        },
      ],
    };

    const legalIncToken = await this.getLegalIncToken();

    const orderContact: ILegalInc.API.Contact = {
      business_type: formationRequest.entity.type,
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'billing',
      addresses: [billingAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'billing',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'billing',
        },
      ],
    };
    const primaryOrderContact: ILegalInc.API.Contact = {
      business_type: formationRequest.entity.type,
      first_name: 'Robb',
      last_name: 'Kunz',
      contact_type: 'primary',
      addresses: [billingAddress],
      emails: [
        {
          email: 'ap@glassboardtech.com',
          email_type: 'primary',
        },
      ],
      phones: [
        {
          number: '877-492-7555',
          phone_type: 'primary',
        },
      ],
    };
    // For GBT Master Series
    // ap@glassboardtech.com
    // Tax = Fund Manager / Organizer w/SSN

    const orderData: ILegalInc.API.Order = {
      description:
        `Domestic ${formationRequest.entity.type} Filing for ` +
        formationRequest.entity.name,
      status_id: 1,
      payment_by_card: 1,
      client_id: process.env.LEGALINC_CLIENT_ID ?? '51270',
      affiliate_id: 1,
      service_level_id: 2,
      ...selectedPackage,
      state_id: formationStateId,
      total_price: 0,
      order_contact: [primaryOrderContact, orderContact],
      external_id: '',
      matter_number: '',
      requester_name: '',
      note: formationRequest.entity?.isSeries
        ? `Create as Series under ${formationRequest.entity.masterEntity?.name}`
        : '',
      tax: [orderTaxContact],
      registered_agent: false,
      company: {
        name: formationRequest.entity.name,
        company_type: formationRequest.entity.type,
        client_id: String(process.env.LEGALINC_CLIENT_ID) ?? '51270',
        registered_state_id: formationStateId,
        domestic_state_id: formationStateId,
        fiscal_year: '12/31',
        address: entityAddress,
        contacts: [orderTaxContact],
      },
    };

    const orderResponseData = await this.legalInc.createOrder(
      legalIncToken,
      orderData,
    );
    if (!orderResponseData.success) {
      throw new Error(orderResponseData.message);
    }
    const OrderResponse = JSON.parse(orderResponseData.response);
    const paymentData: ILegalInc.API.Payment = {
      amount: OrderResponse.total_price,
      order_id: OrderResponse.id,
      payer_id: parseInt(process.env.LEGALINC_CLIENT_ID ?? '51270'),
    };
    const paymentResponseData = await this.legalInc.createPayment(
      legalIncToken,
      paymentData,
    );
    if (!paymentResponseData.success) {
      throw new Error(paymentResponseData.message);
    }
    formationRequest.providerMeta.typeId = 'LEGALINC';
    formationRequest.providerMeta.id = OrderResponse.id;
    formationRequest.providerMeta.order = new LegalIncOrderModel({
      status: ILegalInc.API.OrderStatus.NEW,
      ...OrderResponse,
    });
    formationRequest.orderId = OrderResponse.id;
    return this.formationRequestRepository.save(formationRequest);
  }

  private async resolveTaxContact(
    useOrganizer = false,
    organizerProfile: ProfileWithRelations,
    tenant: TenantWithRelations,
  ): Promise<Tax> {
    let taxContact: Tax | undefined;
    taxContact = getTaxContactFromProfileOrManager(organizerProfile);
    if (!useOrganizer && (organizerProfile?.managerId || tenant?.managerId)) {
      const managerId = organizerProfile.managerId
        ? organizerProfile.managerId
        : tenant.managerId;
      const manager: CompanyWithRelations =
        await this.companyRepository.findById(String(managerId));
      taxContact = getTaxContactFromProfileOrManager(manager);
    }
    if (!taxContact) {
      throw new Error(
        'Unable to resolve Tax Contact Data. Please check Tenant settings and profile data',
      );
    }
    return taxContact;
  }
}
