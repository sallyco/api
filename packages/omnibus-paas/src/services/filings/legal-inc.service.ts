import {getService} from '@loopback/service-proxy';
import {inject, Provider} from '@loopback/core';
import {LegalIncDataSource} from '../../datasources';
import {ILegalInc} from '../../interfaces/services/legal-inc.interface';

export interface LegalInc {
  getToken(
    client_id: string,
    client_secret: string,
    username: string,
    password: string,
  ): Promise<ILegalInc.APIResponse.Token>;
  getOrders(
    access_token: string,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
  createOrder(
    access_token: string,
    order_data: ILegalInc.API.Order,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
  getOrder(
    access_token: string,
    order_id: string | number,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
  createPayment(
    access_token: string,
    payment_data: ILegalInc.API.Payment,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
  getPayments(
    access_token: string,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
  getCompany(
    access_token: string,
    company_id: number,
  ): Promise<ILegalInc.APIResponse.ResponseWrapper>;
}

export class LegalIncProvider implements Provider<LegalInc> {
  constructor(
    // LegalInc must match the name property in the datasource json file
    @inject('datasources.LegalInc')
    protected dataSource: LegalIncDataSource = new LegalIncDataSource(),
  ) {}

  async value(): Promise<LegalInc> {
    return getService(this.dataSource);
  }
}
