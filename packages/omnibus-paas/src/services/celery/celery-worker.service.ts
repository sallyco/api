/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  BindingScope,
  Context,
  inject,
  injectable,
  service,
} from '@loopback/core';
import {repository} from '@loopback/repository';
import {UserProfile} from '@loopback/security';
import * as celery from 'celery-node';
import Worker from 'celery-node/dist/app/worker';
import debugFactory from 'debug';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {
  AccountRepository,
  DealRepository,
  EntitiesRepository,
  SubscriptionRepository,
  TransactionRepository,
} from '../../repositories';
import {Banking, BankingProvider} from '../banking/banking.service';
import {DocumentsService} from '../documents/documents.service';
import {FilingsService} from '../filings/filings.service';
import {PlaidService} from '../banking/plaid.service';
import {TransactionService} from '../banking/transaction.service';
import {Unleash, UnleashProvider} from '../system/unleash.service';
import {KycAmlChecksService} from '../kycaml/kycamlchecks.service';
import {EmailsService} from '../notifications/emails.service';
import {ClosesService} from '../closes/closes.service';
import {AccreditationsSyncService} from '../accreditations/accreditations-sync.service';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const debug = debugFactory('omnibus-paas:service:celery-worker');

@injectable({scope: BindingScope.TRANSIENT})
export class CeleryWorkerService {
  worker: Worker;
  constructor(
    @inject.context() private readonly ctx: Context,
    @service(DocumentsService)
    private readonly documentsService: DocumentsService,
    @service(FilingsService) private readonly filings: FilingsService,
    @service(BankingProvider) private readonly banking: Banking,
    @repository(EntitiesRepository)
    private readonly entityRepository: EntitiesRepository,
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @repository(TransactionRepository)
    private readonly transactionRepository: TransactionRepository,
    @repository(AccountRepository)
    private readonly accountRepository: AccountRepository,
    @service(PlaidService)
    private readonly plaidService: PlaidService,
    @service(TransactionService)
    private readonly transactionService: TransactionService,
    @service(UnleashProvider)
    private readonly unleash: Unleash,
    @service(KycAmlChecksService)
    private readonly kycamlChecksService: KycAmlChecksService,
    @service(EmailsService)
    private readonly emailsService: EmailsService,
    @service(ClosesService)
    private readonly closesService: ClosesService,
    @service(AccreditationsSyncService)
    private readonly accreditationSyncService: AccreditationsSyncService,
  ) {
    this.worker = celery.createWorker(
      process.env.CELERY_BROKER ?? 'amqp://localhost:5672',
      process.env.CELERY_BACKEND ?? 'amqp://localhost:5672',
      'omnibus',
    );
  }

  async start() {
    this.worker.register(
      'omnibus.generateEntityDocument',
      async (
        entityId: string,
        documentType: string,
        documentData: any,
        profile: UserProfile,
        tenant: Tenant,
        existingFileId?: string,
      ) => {
        this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to(tenant);
        return this.documentsService.generateDocumentForEntity(
          entityId,
          documentType,
          documentData,
          profile,
          tenant,
          existingFileId,
        );
      },
    );
    this.worker.register(
      'omnibus.generateSigningDocument',
      async (
        documentType: string,
        subscriptionId: string,
        documentData: any,
        profile: UserProfile,
        tenant: Tenant,
        existingFileId?: string,
      ) => {
        this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to(tenant);
        return this.documentsService.generateSigningDocument(
          documentType,
          subscriptionId,
          documentData,
          profile,
          tenant,
          existingFileId,
        );
      },
    );

    this.worker.register('omnibus.updateLegalIncOrders', async () => {
      await this.filings.updateLegalIncEINOrderStatuses();
      await this.filings.updateFormationRequests();
    });

    this.worker.register(
      'omnibus.createEntityBankAccount',
      async (entityId: string) => {
        const entity = await this.entityRepository.findById(entityId);
        return this.banking.createEntityDepositAccount(entity);
      },
    );

    this.worker.register('omnibus.processACHWaitingTransactions', async () => {
      await this.transactionService.processWaitingTransactions('ACH');
    });

    this.worker.register('omnibus.processWaitingTransactions', async () => {
      await this.transactionService.processWaitingTransactions('ACH');
    });

    this.worker.register('omnibus.processWireWaitingTransactions', async () => {
      await this.transactionService.processWaitingTransactions('WIRE');
    });

    this.worker.register('omnibus.processQueuedTransactions', async () => {
      await this.transactionService.processACHQueuedTransactions();
    });

    this.worker.register('omnibus.processACHQueuedTransactions', async () => {
      await this.transactionService.processACHQueuedTransactions();
    });

    this.worker.register('omnibus.processWireQueuedTransactions', async () => {
      if (
        this.unleash.isEnabled('queue_processwirequeuedtransactions', {
          userId: '',
        })
      ) {
        await this.transactionService.processWireQueuedTransactions();
      }
    });

    this.worker.register('omnibus.syncACHTransactionsBanking', async () => {
      await this.transactionService.syncACHTransactionsBanking();
    });

    this.worker.register('omnibus.syncWireTransactionsBanking', async () => {
      await this.transactionService.syncWireTransactionsBanking();
    });

    this.worker.register('omnibus.syncAccountTransactionBanking', async () => {
      await this.transactionService.syncAccountTransactionBanking();
    });

    this.worker.register('omnibus.syncKycAmlProfiles', async () => {
      await this.kycamlChecksService.synchronizeKYCChecks();
    });

    this.worker.register('omnibus.bankNoNewChangesEmail', async () => {
      if (
        this.unleash.isEnabled('bank_no_new_changes_email', {userId: 'gbt'})
      ) {
        this.ctx.bind(MultiTenancyBindings.CURRENT_TENANT).to({id: 'gbt'});
        await this.emailsService.sendBankNoNewChangesEmail(
          process.env.BANK_NO_NEW_CHANGES_EMAIL ??
            'retailstoreyave@bankprov.com',
          'No Organization Changes',
          'MMAC, Inc dba Glassboard Technology',
        );
        await this.emailsService.sendBankNoNewChangesEmail(
          process.env.BANK_NO_NEW_CHANGES_EMAIL ??
            'retailstoreyave@bankprov.com',
          'No Organization Changes',
          'Assure Services, Inc.',
        );
      }
    });

    this.worker.register(
      'omnibus.startClose',
      async (closeId: string, closeDate: Date, organizerSignature: string) => {
        await this.closesService.processClose(
          closeId,
          closeDate,
          organizerSignature,
        );
      },
    );

    this.worker.register(
      'omnibus.fundManagerCountersign',
      async (closeId: string, fundManagerSignature: string) => {
        await this.closesService.processClose(
          closeId,
          undefined,
          undefined,
          fundManagerSignature,
        );
      },
    );

    this.worker.register('omnibus.syncAccreditations', async () => {
      await this.accreditationSyncService.synchronizeAccreditations();
    });

    return this.worker.start();
  }
  async stop() {
    this.worker.stop();
  }
}
