/* eslint-disable @typescript-eslint/no-explicit-any */
import {/* inject, */ BindingScope, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {UserProfile} from '@loopback/security';
import * as celery from 'celery-node';
import Client from 'celery-node/dist/app/client';
import {File} from '../../models';
import {Tenant} from '../../multi-tenancy';
import {FileRepository} from '../../repositories';

@injectable({scope: BindingScope.TRANSIENT})
export class CeleryClientService {
  private omnibusQueueClient: Client;
  private celeryworkerQueueClient: Client;

  constructor(
    @repository(FileRepository) public fileRepository: FileRepository,
  ) {
    this.omnibusQueueClient = celery.createClient(
      process.env.CELERY_BROKER ?? 'amqp://localhost:5672',
      process.env.CELERY_BACKEND ?? 'amqp://localhost:5672',
      'omnibus',
    );
    this.celeryworkerQueueClient = celery.createClient(
      process.env.CELERY_BROKER ?? 'amqp://localhost:5672',
      process.env.CELERY_BACKEND ?? 'amqp://localhost:5672',
      'celeryworker',
    );
  }

  async generateEntityDocument(
    entityId: string,
    documentType: string,
    documentData: any,
    profile: UserProfile,
    tenant: Tenant,
  ): Promise<File> {
    const file = await this.fileRepository.create(
      new File({
        generating: true,
      }),
    );
    const task = this.omnibusQueueClient.createTask(
      'omnibus.generateEntityDocument',
    );
    task.applyAsync([
      entityId,
      documentType,
      documentData,
      profile,
      tenant,
      file.id,
    ]);
    return file;
  }

  async generateSigningDocument(
    documentType: string,
    subscriptionId: string,
    documentData: any,
    profile: UserProfile,
    tenant: Tenant,
  ): Promise<File> {
    const file = await this.fileRepository.create(
      new File({
        generating: true,
      }),
    );
    const task = this.omnibusQueueClient.createTask(
      'omnibus.generateSigningDocument',
    );
    task.applyAsync([
      documentType,
      subscriptionId,
      documentData,
      profile,
      tenant,
      file.id,
    ]);
    return file;
  }

  async startClose(closeId: string, organizerSignature: string): Promise<void> {
    const task = this.omnibusQueueClient.createTask('omnibus.startClose');
    task.applyAsync([closeId, new Date(), organizerSignature]);
  }

  async fundManagerCountersign(
    closeId: string,
    managerSignature: string,
  ): Promise<void> {
    const task = this.omnibusQueueClient.createTask(
      'omnibus.fundManagerCountersign',
    );
    task.applyAsync([closeId, managerSignature]);
  }

  async createEntityBankAccount(entityId: string): Promise<void> {
    const task = this.omnibusQueueClient.createTask(
      'omnibus.createEntityBankAccount',
    );
    task.applyAsync([entityId]);
  }
}
