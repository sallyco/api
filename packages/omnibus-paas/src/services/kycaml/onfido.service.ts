import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {OnfidoDataSource} from '../../datasources';

type IdNumber = {
  type: string;
  value: string;
  stateCode?: string;
};

type AddressOptional = {
  flatNumber?: string;
  buildingNumber?: string;
  buildingName?: string;
  street?: string;
  subStreet?: string;
  town?: string;
  state?: string;
  line1: string;
  line2?: string;
  line3?: string;
};
type Address = {
  postcode: string;
  country: string;
} & Partial<AddressOptional>;

interface ApplicantRequest
  extends Omit<OnfidoApplicant, 'id' | 'created_at' | 'delete_at' | 'href'> {}
export interface OnfidoApplicant {
  id: string;
  created_at: string | null;
  delete_at: string | null;
  href: string;
  first_name: string;
  last_name: string;
  email?: string;
  dob?: string;
  address?: Address;
  id_numbers?: IdNumber[];
}

export type OnfidoCheckRequest = {
  applicant_id: string;
  report_names: string[];
  document_ids?: string[] | null;
  applicant_provides_data?: boolean;
  asynchronous?: boolean;
  tags?: string[] | null;
  suppress_form_emails?: boolean;
  redirect_uri?: string | null;

  // https://documentation.onfido.com/#pre-determined-responses
  // For development only to force a specific sub_result state
  sub_result?: string;
  // For development only to force a specific overall consider state
  consider?: string[];
};
export type OnfidoCheck = {
  id: string;
  report_ids: string[];
  reports?: OnfidoReport[];
  created_at: string;
  href: string;
  applicant_id: string;
  applicant_provides_data: boolean;
  privacy_notices_read_consent_given?: boolean;
  status: string;
  tags: string[];
  paused?: boolean;
  result: string | null;
  form_uri: string | null;
  redirect_uri: string | null;
  results_uri: string;
  sandbox?: boolean;
};

export type OnfidoReport = {
  id: string;
  created_at: string;
  name: string;
  href: string;
  status: string;
  result: string | null;
  sub_result: string | null;
  properties: object | null;
  breakdown: object | null;
  documents: Array<{
    id: string;
  }>;
  check_id: string;
};

type request<Parameters, RequestType, ReturnType> = (
  requestParameters?: Parameters,
  bodyParameters?: {requestBody: RequestType},
  requestContentType?: string,
) => Promise<ReturnType>;

export interface Onfido {
  create_applicant: request<null, ApplicantRequest, OnfidoApplicant>;
  create_check: request<null, OnfidoCheckRequest, OnfidoCheck>;
  find_check: request<{check_id: string}, null, OnfidoCheck>;
  find_report: request<{report_id: string}, null, OnfidoReport>;
  find_applicant: request<{applicant_id: string}, null, OnfidoApplicant>;
  /* eslint-disable @typescript-eslint/no-explicit-any */
  [methodName: string]: (...args: any[]) => Promise<any>;
}

export class OnfidoProvider implements Provider<Onfido> {
  constructor(
    // onfido must match the name property in the datasource json file
    @inject('datasources.onfido')
    protected dataSource: OnfidoDataSource = new OnfidoDataSource(),
  ) {}

  value(): Promise<Onfido> {
    return getService(this.dataSource);
  }
}
