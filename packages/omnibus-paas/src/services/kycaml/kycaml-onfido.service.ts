import {BindingScope, injectable, service} from '@loopback/core';
import {
  Onfido,
  OnfidoApplicant,
  OnfidoCheck,
  OnfidoProvider,
  OnfidoReport,
} from './onfido.service';
import {
  ISO_Alpha_2,
  ISO_Alpha_3,
  ISOHelper,
} from '../../interfaces/datatypes/ISO';
import {
  Applicant,
  ApplicantRequest,
  Check,
  CheckRequest,
  CheckResult,
  KycAml,
} from './kycaml.service.types';
import _ from 'lodash';
import {OnfidoDataSource} from '../../datasources';
import {Profile} from '../../models';
import {KycAmlProvider} from './kycamlprovider.service';

export enum AvailableReportIds {
  document = 'document',
  facial_similarity_photo = 'facial_similarity_photo',
  identity_enhanced = 'identity_enhanced',
  watchlist_standard = 'watchlist_standard',
  watchlist_enhanced = 'watchlist_enhanced',
  watchlist_peps_only = 'watchlist_peps_only',
  watchlist_sanctions_only = 'watchlist_sanctions_only',
}

const EnabledReportIds = [
  AvailableReportIds.watchlist_enhanced,
  AvailableReportIds.identity_enhanced,
];

const ReportRequiredProperties = {
  [AvailableReportIds.identity_enhanced]: [
    'firstName',
    'lastName',
    'dob',
    'address.region',
    'address.postalCode',
    'address.country',
  ],
  [AvailableReportIds.watchlist_enhanced]: ['firstName', 'lastName'],
};

@injectable({scope: BindingScope.TRANSIENT})
export class KycAmlOnfidoService extends KycAml {
  public typeId = 'ONFIDO';

  constructor(@service(OnfidoProvider) private readonly onfido: Onfido) {
    super();
  }

  async createApplicant(applicant: ApplicantRequest): Promise<Applicant> {
    let address;
    if (applicant.address) {
      address = applicant.address;
      address.country = this.sanitizeCountry(address.country);
      address.state = this.sanitizeState(address.country, address.region);
    }
    // Map Tax Ids to Onfido compliant ones
    if (applicant.idNumbers) {
      for (const [index, taxId] of applicant.idNumbers.entries()) {
        if (taxId.type === 'itin') {
          applicant.idNumbers[index].type = 'tax_id';
        }
      }
      applicant.idNumbers = applicant.idNumbers.filter(number =>
        ['ssn', 'tax_id'].includes(number.type),
      );
    }
    const createdApplicant =
      await OnfidoDataSource.tolerantCall<OnfidoApplicant>(
        this.onfido.create_applicant(null, {
          requestBody: {
            ...(address
              ? {
                  address: {
                    line1: address.street1.replace(/!\$%\^*=<>/g, ''),
                    ...(address.street2
                      ? {line2: address.street2.replace(/!\$%\^*=<>/g, '')}
                      : {}),
                    state: address.state,
                    postcode: address.postalCode,
                    town: address.city,
                    country: address.country.toString(),
                  },
                }
              : {}),
            first_name: applicant.firstName.replace(/[\^!#$%*=<>;{}"]/g, ''),
            last_name: applicant.lastName.replace(/[\^!#$%*=<>;{}"]/g, ''),
            ...(applicant.dob ? {dob: applicant.dob} : {}),
            email: applicant.email,
            id_numbers: applicant.idNumbers,
          },
        }),
      );
    return {
      ...applicant,
      providerMeta: {
        typeId: this.typeId,
        ...createdApplicant,
      },
    };
  }

  async getApplicant(applicantId: string): Promise<Applicant> {
    const foundApplicant = await OnfidoDataSource.tolerantCall<OnfidoApplicant>(
      this.onfido.find_applicant({
        applicant_id: applicantId,
      }),
    );
    if (!foundApplicant) {
      throw new Error(`Unable to find applicant with ID ${applicantId}`);
    }
    return this.convertOnfidoApplicantToApplicant(foundApplicant);
  }

  async createCheck(check: CheckRequest): Promise<Check> {
    const reportsToRun = await this.getAllowedReportsByApplicantId(
      check.applicantId,
    );
    if (reportsToRun.length < 1) {
      throw new Error(
        'Unable to run any reports: Missing Critical Information',
      );
    }
    const createdCheck = await OnfidoDataSource.tolerantCall<OnfidoCheck>(
      this.onfido.create_check(null, {
        requestBody: {
          applicant_id: check.applicantId,
          report_names: reportsToRun,
        },
      }),
    );
    if (!createdCheck) {
      throw new Error('Unable to create Check');
    }
    createdCheck.reports = await this.getReportsByIds(createdCheck.report_ids);
    return {
      ...check,
      checkId: createdCheck.id,
      status:
        createdCheck.status === 'in_progress' ? 'IN_PROGRESS' : 'COMPLETE',
      providerMeta: {
        typeId: this.typeId,
        ...createdCheck,
      },
    };
  }

  async getCheck(checkId: string): Promise<Check> {
    const foundCheck = await OnfidoDataSource.tolerantCall<OnfidoCheck>(
      this.onfido.find_check({
        check_id: checkId,
      }),
    );
    if (!foundCheck) {
      throw new Error(`Unable to find check id ${checkId}`);
    }
    foundCheck.reports = await this.getReportsByIds(foundCheck.report_ids);
    return {
      applicantId: foundCheck.applicant_id,
      status: foundCheck.status === 'in_progress' ? 'IN_PROGRESS' : 'COMPLETE',
      result: foundCheck.result
        ? await this.calculateCheckResult(foundCheck)
        : undefined,
      providerMeta: {
        typeId: this.typeId,
        ...foundCheck,
      },
    };
  }

  async calculateCheckResult(check: OnfidoCheck): Promise<CheckResult> {
    for (const report of check.reports ?? []) {
      if (report.name === AvailableReportIds.watchlist_enhanced) {
        if (
          report.breakdown!['sanction'].result !== 'clear' ||
          report.breakdown!['monitored_lists'].result !== 'clear'
        ) {
          check.result = 'consider';
        }
      }
      if (report.name === AvailableReportIds.identity_enhanced) {
        if (report.result !== 'clear') {
          check.result = 'consider';
        }
      }
    }
    return check.result === 'clear' ? 'CLEAR' : 'CONSIDER';
  }

  async getReportsByIds(reportIds: string[]): Promise<OnfidoReport[]> {
    const reports: OnfidoReport[] = [];
    // Resolve Report Data
    for (const report_id of reportIds) {
      const report = await OnfidoDataSource.tolerantCall<OnfidoReport>(
        this.onfido.find_report({
          report_id: report_id,
        }),
      );
      if (report) {
        reports.push(report);
      }
    }
    return reports;
  }

  public sanitizeCountry(country: ISO_Alpha_2): ISO_Alpha_3 {
    return ISOHelper.convertISO_2ToISO_3(country);
  }

  public sanitizeState(country: ISO_Alpha_3, state: string) {
    return country.toString() === 'USA'
      ? ISOHelper.convertToISO_3166_2_US(state).toString()
      : undefined;
  }

  public async getAllowedReportsByApplicantId(
    applicantId: string,
  ): Promise<string[]> {
    const checkRequiredReportProperties = (
      reportId: string,
      applicantObject: Applicant,
    ) => {
      for (const requiredProperty of ReportRequiredProperties[reportId]) {
        if (!_.get(applicantObject, requiredProperty)) {
          return false;
        }
      }
      return true;
    };

    const applicant = await this.getApplicant(applicantId);
    const allowedReports: string[] = [];
    for (const reportId of EnabledReportIds) {
      if (checkRequiredReportProperties(reportId, applicant)) {
        allowedReports.push(reportId);
      }
    }
    return allowedReports;
  }

  async createApplicantForProfile(profile: Profile): Promise<Applicant> {
    return this.createApplicant(
      await KycAmlProvider.createApplicationFromProfile(profile),
    );
  }

  async convertOnfidoApplicantToApplicant(
    applicant: OnfidoApplicant,
  ): Promise<Applicant> {
    return {
      firstName: applicant.first_name,
      lastName: applicant.last_name,
      email: applicant.email,
      ...(applicant.address
        ? {
            address: {
              street1: applicant.address.line1!,
              street2: applicant.address.line2,
              postalCode: applicant.address.postcode,
              city: String(applicant.address.town),
              region: String(applicant.address.state),
              country: ISOHelper.convertISO_3ToISO_2(
                applicant.address.country as unknown as ISO_Alpha_3,
              ),
            },
          }
        : {}),
      dob: applicant.dob ?? undefined,
      providerMeta: {
        typeId: this.typeId,
        ...applicant,
      },
    };
  }
}
