/* eslint-disable @typescript-eslint/no-explicit-any */
import {BindingScope, injectable, service} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {KycAmlProvider} from './kycamlprovider.service';
import {KycAml} from './kycaml.service.types';
import {KycRepository, ProfileRepository} from '../../repositories';
import {Kyc, Profile} from '../../models';
import debugFactory from 'debug';
import {SystemNotificationsService} from '../system/system-notifications.service';

const debug = debugFactory('omnibus-paas:services:kycamlchecks');

@injectable({scope: BindingScope.TRANSIENT})
export class KycAmlChecksService {
  constructor(
    @service(KycAmlProvider) private kycaml: KycAml,
    @repository(ProfileRepository) private profileRepository: ProfileRepository,
    @repository(KycRepository) private kycRepository: KycRepository,
    @service(SystemNotificationsService)
    private systemNotifications: SystemNotificationsService,
  ) {}

  /**
   * @deprecated Not used anymore since KYC/AML Check is Explicit Call now
   */
  async synchronizeProfiles() {
    const kycProfiles = await this.getProfilesToUpdate(
      parseInt(process.env.KYCAML_BATCH_SIZE ?? '6'),
    );
    for (const [profileIndex, profileData] of kycProfiles.entries()) {
      let profile = await this.profileRepository.findById(profileData.id, {
        include: ['kycAml'],
      });
      try {
        const {profile: currentProfile, checkId} =
          await this.getCheckIdForProfile(profile);
        const check = await this.kycaml.getCheck(checkId);
        profile = await this.profileRepository.findById(profileData.id, {
          include: ['kycAml'],
        });
        await this.kycRepository.updateById(profile.kycAml?.id, {
          ...check,
          profileId: profile.id,
          ownerId: profile.ownerId,
        });
        debug(
          `Updated KYC for Profile (${profileIndex + 1}/${kycProfiles.length})`,
          profile.id,
        );
        if (check.result === 'CONSIDER') {
          await this.systemNotifications.Notify_Failed_KYC_AML_Check(
            currentProfile,
          );
        }
      } catch (e) {
        debug(
          `Failed to update KYC for Profile (${profileIndex + 1}/${
            kycProfiles.length
          })`,
          profile.id,
          profile?.kycAml?.id,
          e,
          e.message,
        );
        try {
          const KYC = await this.kycRepository.findById(profile.kycAml?.id);
          await this.kycRepository.updateById(KYC.id, {
            ...KYC,
            status: 'EXCEPTION',
            reason:
              e.message ?? e.response.error ?? JSON.parse(JSON.stringify(e)),
            providerMeta: {
              ...KYC.providerMeta,
            },
          });
        } catch (secondError) {
          debug('Failed to update KYC Object:', secondError);
        }
      }
    }
  }
  async synchronizeKYCChecks() {
    const kycChecks = await this.getKycChecksToUpdate(
      parseInt(process.env.KYCAML_BATCH_SIZE ?? '2'),
    );
    for (const [kycIndex, kyc] of kycChecks.entries()) {
      try {
        const check = await this.kycaml.getCheck(kyc.providerMeta?.id);
        await this.kycRepository.updateById(kyc.id, {
          ...check,
          updatedAt: new Date(),
        });
        debug(
          `Updated KYC for KYC Check Object (${kycIndex + 1}/${
            kycChecks.length
          })`,
          kyc.id,
        );
      } catch (e) {
        debug(
          `Failed to update KYC for KYC Check Object (${kycIndex + 1}/${
            kycChecks.length
          })`,
          kyc.id,
        );
        await this.kycRepository.updateById(kyc.id, {
          status: 'EXCEPTION',
          reason:
            e.message ?? e.response.error ?? JSON.parse(JSON.stringify(e)),
          updatedAt: new Date(),
        });
      }
    }
  }

  async getKycChecksToUpdate(limit = 2, limitToProfiles = false) {
    const kycFilter = {
      where: {
        status: {
          nin: ['COMPLETE', 'EXCEPTION'],
        },
      },
      limit: limit,
    };
    const checks = await this.kycRepository.find(kycFilter as Filter<Kyc>);
    if (!limitToProfiles) {
      return checks;
    }
    return checks.filter(kyc => kyc.profileId);
  }

  async getProfilesToUpdate(limit = 2) {
    const kycChecks = await this.getKycChecksToUpdate(limit, true);
    if (kycChecks.length < 1) {
      return [];
    }
    const profileFilter = {
      include: ['kycAml'],
      where: {
        id: {
          inq: [...kycChecks.map(kyc => kyc.profileId)],
        },
      },
      limit: limit,
      order: ['createdAt ASC'],
    };
    return this.profileRepository.find(profileFilter as Filter<Profile>);
  }

  async getCheckIdForProfile(
    profile: Profile,
  ): Promise<{profile: Profile; checkId: string}> {
    let checkId = profile?.kycAml?.checkId ?? profile.kycAml?.providerMeta?.id;
    if (!checkId) {
      try {
        profile = await this.createCheckForProfile(profile);
        checkId = profile.kycAml?.providerMeta?.id;
      } catch (e) {
        debug("Couldn't generate check for Profile", profile.id, e);
        throw e;
      }
    }
    return {profile: profile, checkId: checkId};
  }

  async createCheckForProfile(profile: Profile): Promise<Profile> {
    const applicant = await this.kycaml.createApplicant(
      await KycAmlProvider.createApplicationFromProfile(profile),
    );
    if (!applicant) {
      throw new Error('Unable to create Applicant');
    }

    const check = await this.kycaml.createCheck({
      applicantId: String(applicant.providerMeta!.id),
    });

    const kycObject = await this.kycRepository.findOne({
      where: {
        profileId: profile.id,
      },
    });
    if (!kycObject) {
      await this.kycRepository.create({
        ...check,
        profileId: profile.id,
        ownerId: profile?.ownerId,
        tenantId: profile?.tenantId,
      });
    } else {
      await this.kycRepository.updateById(kycObject.id, {
        ...check,
        profileId: profile.id,
        ownerId: profile?.ownerId,
        tenantId: profile?.tenantId,
      });
    }
    return this.profileRepository.findById(profile.id, {
      include: ['kycAml'],
    });
  }

  getBreakdownFromCheckResults(kycAml) {
    const breakdown: any = {};
    for (const report of kycAml?.providerMeta?.reports ?? []) {
      if (report.name === 'watchlist_enhanced') {
        breakdown.sanction = report.breakdown!['sanction'];
        breakdown.monitored_lists = report.breakdown!['monitored_lists'];
      } else if (report.name === 'identity_enhanced') {
        breakdown.identity = {
          result: report.result,
        };
      }
    }
    return breakdown;
  }
}
