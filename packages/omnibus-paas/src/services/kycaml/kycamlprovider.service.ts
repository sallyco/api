import {injectable, BindingScope, Provider, service} from '@loopback/core';
import {KycAmlOnfidoService} from './kycaml-onfido.service';
import {Applicant, ApplicantIdNumbers, KycAml} from './kycaml.service.types';
import {Profile} from '../../models';
import {ISOHelper} from '../../interfaces/datatypes/ISO';

@injectable({scope: BindingScope.TRANSIENT})
export class KycAmlProvider implements Provider<KycAml> {
  constructor(
    @service(KycAmlOnfidoService)
    private readonly onfido: KycAmlOnfidoService,
  ) {}

  static async createApplicationFromProfile(
    profile: Profile,
  ): Promise<Applicant> {
    const usesSignatory = await this.usesSignatory(profile);

    const {firstName, lastName} = await this.resolveName(profile);

    const address = usesSignatory
      ? profile.primarySignatory?.address
      : profile.address;
    const email = profile.primarySignatory?.email ?? profile.email;

    const dateOfBirth = profile.dateOfBirth
      ? new Date(String(profile.dateOfBirth)).toUTCString()
      : undefined;

    if (!firstName || firstName.length === 0) {
      throw new Error('First Name is Invalid');
    }
    if (!lastName || lastName.length === 0) {
      throw new Error('Last Name is Invalid');
    }

    return {
      firstName: String(firstName),
      lastName: String(lastName),
      ...(address
        ? {
            address: {
              street1: String(address?.address1),
              street2: address?.address2 ? String(address.address2) : undefined,
              city: String(address?.city),
              postalCode: String(address?.postalCode),
              country: ISOHelper.convertCountryNameToISO_2(
                String(address?.country),
              ),
              region: String(address?.state),
            },
          }
        : {}),
      email: email ? String(email) : undefined,
      dob: dateOfBirth,
      idNumbers: await this.resolveTaxIds(profile),
    };
  }

  static async resolveTaxIds(profile: Profile): Promise<ApplicantIdNumbers[]> {
    const idNumbers: ApplicantIdNumbers[] = [];
    const usesSignatory = await this.usesSignatory(profile);
    if (usesSignatory) {
      if (profile.primarySignatory?.taxDetails) {
        idNumbers.push({
          type: String(profile.primarySignatory.taxDetails.type),
          value: String(profile.primarySignatory.taxDetails.value),
        });
      }
    } else {
      if (profile.taxDetails) {
        idNumbers.push({
          type: String(profile.taxDetails.taxIdentification.type),
          value: String(profile.taxDetails.taxIdentification.value),
        });
      }
    }
    return idNumbers;
  }

  static async resolveName(
    profile: Profile,
  ): Promise<{firstName?: string; lastName?: string}> {
    const usesSignatory = await this.usesSignatory(profile);
    let firstName: string | undefined;
    let lastName: string | undefined;
    if (usesSignatory) {
      firstName = String(profile.primarySignatory?.name)
        .replace(/\s+/g, ' ')
        .split(' ')[0];
      lastName = String(profile.primarySignatory?.name)
        .replace(/\s+/g, ' ')
        .split(' ')[1];
      if (firstName && !lastName) {
        // Attempt to resolve the splitting of names from the Signatory with badly formatted names
        // e.g. "TestTesterton" instead of "Test Testerton"
        for (const [index, letter] of Array.from(firstName).entries()) {
          if (letter === letter.toUpperCase() && index !== 0) {
            lastName = firstName.slice(index);
            firstName = firstName.slice(0, index);
          }
        }
      }
    } else {
      firstName = profile.firstName;
      lastName = profile.lastName;
    }
    return {
      firstName: firstName,
      lastName: lastName,
    };
  }

  static async usesSignatory(profile: Profile): Promise<boolean> {
    return !!(
      ['TRUST', 'JOINT'].includes(
        String(profile.taxDetails?.registrationType),
      ) || profile.primarySignatory
    );
  }

  value() {
    return this.onfido;
  }
}
