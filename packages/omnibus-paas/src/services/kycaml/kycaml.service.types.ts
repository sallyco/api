/**
 * Types for KYC / AML service
 *
 * These have to exist in a separate file to prevent circular dependencies
 */
import {ISO_Alpha_2} from '../../interfaces/datatypes/ISO';

interface ApplicantIdentification {
  type: string;
  value: string;
  region?: string;
}
interface ApplicantAddress {
  street1: string;
  street2?: string;
  city: string;
  postalCode: string;
  region: string;
  country: ISO_Alpha_2;
}
export interface ApplicantIdNumbers {
  type: string;
  value: string;
}
export interface Applicant {
  firstName: string;
  lastName: string;
  email?: string;
  address?: ApplicantAddress;
  dob?: string;
  identification?: ApplicantIdentification[];
  idNumbers?: ApplicantIdNumbers[];
  id?: string;
  createdAt?: string;
  providerMeta?: {
    typeId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}
export interface ApplicantRequest
  extends Omit<Applicant, 'id' | 'createdAt' | 'providerMeta'> {}

export type CheckStatus = 'IN_PROGRESS' | 'COMPLETE' | 'EXCEPTION';
export type CheckResult = 'CLEAR' | 'CONSIDER';
export interface Check {
  createdAt?: string | Date;
  applicantId: string;
  checkId?: string;
  status: CheckStatus;
  result?: CheckResult;
  resultDetail?: {
    adverseMedia?: CheckResult;
    monitoredLists?: CheckResult;
    politicallyExposedPerson?: CheckResult;
    sanction?: CheckResult;
  };
  lastDateChecked?: Date;
  providerMeta?: {
    typeId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}
export interface CheckRequest
  extends Omit<
    Check,
    'id' | 'createdAt' | 'providerMeta' | 'status' | 'result'
  > {}
export abstract class KycAml {
  public abstract typeId: string;

  abstract createApplicant(applicant: ApplicantRequest): Promise<Applicant>;
  abstract getApplicant(applicantId: string): Promise<Applicant>;
  abstract createCheck(check: CheckRequest): Promise<Check>;
  abstract getCheck(checkId: string): Promise<Check>;
}
