import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {CamundaDataSource} from '../../datasources';

export interface Camunda {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  [methodName: string]: (...args: any[]) => Promise<any>;
}

export class CamundaProvider implements Provider<Camunda> {
  constructor(
    // camunda must match the name property in the datasource json file
    @inject('datasources.camunda')
    protected dataSource: CamundaDataSource = new CamundaDataSource(),
  ) {}

  value(): Promise<Camunda> {
    return getService(this.dataSource);
  }
}
