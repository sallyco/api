import {injectable, /* inject, */ BindingScope} from '@loopback/core';
import KcAdminClient from 'keycloak-admin';
import axios from 'axios';

@injectable({scope: BindingScope.TRANSIENT})
export class KeycloakService {
  public adminClient: KcAdminClient;
  public ready: Promise<KcAdminClient>;
  constructor() {
    this.adminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    this.ready = this.getClient();
  }

  async getClient(): Promise<KcAdminClient> {
    await this.adminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
      // clientSecret: '9b3cf0af-5b53-4395-b427-def118fc5623',
    });
    return this.adminClient;
  }

  static async getUserInfo(authorization: string, tenantId: string) {
    return axios.get(
      `${process.env.KC_BASE_URL}/realms/${tenantId}/protocol/openid-connect/userinfo`,
      {
        headers: {
          Authorization: authorization,
        },
      },
    );
  }
}
