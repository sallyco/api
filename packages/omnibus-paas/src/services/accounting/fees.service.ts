import {BindingScope, injectable, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Product, ProductRelations} from '../../models/product.model';
import {BlueSkyFeeService} from '../../services/blue-sky-fee.service';
import {
  //  ExternalBankAccount,
  Entities,
  FeesStatement,
  FeesStatementEntry,
  FeesStatementSection,
  SubscriptionWithRelations,
  Tenant,
} from '../../models';
import {
  DealRepository,
  EntitiesRepository,
  SubscriptionRepository,
  TenantRepository,
  BlueSkyFeeRepository,
  ProductRepository,
} from '../../repositories';
import {
  BankAccount,
  Banking,
  BankingProvider,
} from '../banking/banking.service';
import {FilingsService} from '../filings/filings.service';

const _ = require('lodash');

const NOT_AVAILABLE = 0; //'Not Available';

@injectable({scope: BindingScope.TRANSIENT})
export class FeesService {
  constructor(
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
    @repository(DealRepository)
    private readonly dealRepository: DealRepository,
    @repository(EntitiesRepository)
    private readonly entityRepository: EntitiesRepository,
    @repository(TenantRepository)
    private readonly tenantRepository: TenantRepository,
    @repository(BlueSkyFeeRepository)
    private readonly blueSkyFeeRepository: BlueSkyFeeRepository,
    @service(BankingProvider)
    private readonly banking: Banking,
    @service(FilingsService)
    private readonly filings: FilingsService,
    @repository(ProductRepository)
    private readonly productRepository: ProductRepository,
    @service(BlueSkyFeeService)
    private blueSkyFeeService: BlueSkyFeeService,
  ) {}

  // Found this regex example online, do we want a full dependency like numeral js
  // for formatting numbers into display strings?
  useCurrencyFormat(number: number) {
    // This check is in place because postgres was giving us strings...
    // This will prevent silent errors from creeping in, if we ever try
    // to display something that isn't a number this may save us headaches.
    // ex: prevents a bad string concatination to a number
    if (typeof number !== 'number') {
      throw new Error(
        'Fee Service expected a number amount and found the typeof: ' +
          typeof number,
      );
    }

    return '$' + number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  getRecurringAmount(amount) {
    // We charge up front for the 7 years
    const YEARS_PRE_CHARGED = 7;
    return amount * YEARS_PRE_CHARGED;
  }

  /**
   *
   * @param percentInteger whole number percentage, ex 100 is 100%
   * @param proceedsCollected dollars collected that needs a fee
   * @returns
   */
  calculateManagementPercentFee(
    proceedsCollected: number,
    feeConfig: Entities['managementFee'],
  ) {
    if (!feeConfig?.amount) {
      return 0;
    }

    const amount = feeConfig.isRecurring
      ? this.getRecurringAmount(feeConfig.amount)
      : feeConfig.amount;
    const decimalPercent = amount / 100;
    return decimalPercent * proceedsCollected;
  }

  calculateManagementFlatFee(feeConfig: Entities['managementFee']) {
    if (!feeConfig?.amount) {
      return 0;
    }

    return feeConfig?.isRecurring
      ? this.getRecurringAmount(feeConfig.amount)
      : feeConfig?.amount;
  }

  /**
   * @param entity Entities object
   * @returns {number} number, amount to charge for a fee
   */
  calculateManagementFeeAmount(
    managementFee: Entities['managementFee'],
    proceedsCollected: number,
    subscriptions?: SubscriptionWithRelations[],
  ) {
    const subscriptionsWithSideLetter = (subscriptions ?? []).filter(
      s =>
        s.sideLetter?.managementFee?.amount &&
        s.sideLetter?.managementFee?.type,
    );

    if (!subscriptionsWithSideLetter?.length) {
      if (!managementFee?.amount || !managementFee?.type) {
        return 0;
      }

      if (managementFee.type === 'percent') {
        // Split this into another function because more needs to go on.
        return this.calculateManagementPercentFee(
          proceedsCollected,
          managementFee,
        );
      } else if (managementFee.type === 'flat') {
        // TODO take into account previous closes???
        return this.calculateManagementFlatFee(managementFee);
      }

      throw new Error(
        'Management Fee Error, fee must be a percent or flat amount',
      );
    } else {
      const sideLetterWithPercentageFee = subscriptionsWithSideLetter.filter(
        s => s.sideLetter?.managementFee?.type === 'percent',
      );
      let percentageFee = 0;
      sideLetterWithPercentageFee.forEach(s => {
        percentageFee += this.calculateManagementPercentFee(
          s.amount ?? 0,
          s.sideLetter?.managementFee,
        );
      });

      const sideLetterWithFlatFee = subscriptionsWithSideLetter.filter(
        s => s.sideLetter?.managementFee?.type === 'flat',
      );
      let flatFee = 0;
      sideLetterWithFlatFee.forEach(s => {
        flatFee += this.calculateManagementFlatFee(s.sideLetter?.managementFee);
      });

      return percentageFee + flatFee;
    }
  }

  calculateReserveFeeAmount(
    proceedsCollected,
    reserveConfig: Entities['expenseReserve'],
  ) {
    if (!reserveConfig?.amount || !reserveConfig?.type) {
      return 0;
    }

    if (reserveConfig.type === 'Percent') {
      const decimalPercent = reserveConfig.amount / 100;
      return decimalPercent * proceedsCollected;
    } else if (reserveConfig.type === 'Flat Fee') {
      return reserveConfig.amount;
    }

    throw new Error('Reserve Fee Error, fee must be a percent or flat amount');
  }

  /*
   * Add service methods here
   */

  async getCostPerSpv(tenant: Tenant, totalRaiseAmount = 0) {
    if (
      tenant.saasPackageType?.saasPackageType === 'Tiered' &&
      tenant.saasPackageType?.tieredPricing?.length &&
      tenant.saasPackageType.tieredPricing.length > 0
    ) {
      // Calculate Tier from deal raise
      const tiers = tenant.saasPackageType?.tieredPricing.sort(
        (first, second) => first.breakpoint - second.breakpoint,
      );
      let currentTier = tiers[0];
      for (const tier of tiers) {
        if (totalRaiseAmount >= tier.breakpoint) {
          currentTier = tier;
          continue;
        }
        break;
      }
      return currentTier.price;
    }
    return tenant.dealLimits?.costPerSPV ?? 9500;
  }

  async getFeesData(dealId): Promise<FeesStatement> {
    //TODO: this needs to possibly consider estimated vs actual fees
    const subscriptions: SubscriptionWithRelations[] =
      await this.blueSkyFeeService.getSubscriptions(dealId);

    const deal = await this.dealRepository.findById(dealId);

    const feesOverride =
      deal.additionalProperties && 'feesOverride' in deal.additionalProperties
        ? _.keyBy(deal.additionalProperties['feesOverride'], 'field')
        : {};

    const entity = await this.entityRepository.findById(deal.entityId!);

    let entityBanking: Partial<BankAccount> = {currentBalance: NOT_AVAILABLE};
    if (entity?.bankAccount?.providerMeta?.accountId) {
      entityBanking = await this.banking.getDepositAccountDetails(
        entity.bankAccount!.providerMeta!.accountId!,
      );
    }

    const products = await this.getDefaultProducts();
    const productsByCategory = this.groupProductsByCategory(products);
    const productPrice = this.getTotalProductAmount(products);

    const tenant = await this.tenantRepository.findById(
      String(deal?.tenantId ?? entity.tenantId ?? subscriptions[0].tenantId),
    );
    const costPerEIN =
      'EIN Obtainment' in feesOverride
        ? feesOverride['EIN Obtainment'].amount
        : await this.getEINPrice(entity);
    const costNASAA_EFD_System =
      'NASAA EFD System Fee' in feesOverride
        ? feesOverride['NASAA EFD System Fee'].amount
        : 160;

    const totalFeesAccumulator = [
      costPerEIN,
      costNASAA_EFD_System,
      productPrice,
    ];

    const {blueSkyEntries, totalProceeds} =
      await this.blueSkyFeeService.calculateBlueSkyFees(
        subscriptions,
        totalFeesAccumulator,
        feesOverride,
      );

    const costPerSPV =
      'SPV Administration Fixed Cost per SPV' in feesOverride
        ? feesOverride['SPV Administration Fixed Cost per SPV'].amount
        : await this.getCostPerSpv(tenant, totalProceeds);

    totalFeesAccumulator.push(costPerSPV);

    // Add management Fees
    const managementFeeAmount =
      'Management Fees' in feesOverride
        ? feesOverride['Management Fees'].amount
        : this.calculateManagementFeeAmount(
            entity.managementFee,
            totalProceeds,
            subscriptions,
          );
    totalFeesAccumulator.push(managementFeeAmount);

    // Add Reserve Fees
    const reserveFeeAmount =
      'Expense Reserve' in feesOverride
        ? feesOverride['Expense Reserve'].amount
        : this.calculateReserveFeeAmount(totalProceeds, entity.expenseReserve);
    totalFeesAccumulator.push(reserveFeeAmount);

    const totalFees = totalFeesAccumulator.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
    );
    // Calculate total amount to send to Portfolio Company
    // TODO: move to function?
    const proceedsToSend = totalProceeds - totalFees;

    const spvFee = costPerSPV;

    // Build the sections, add custom products into the table:
    const sections = [
      new FeesStatementSection({
        title: 'Closing Statement Summary',
        entries: [
          new FeesStatementEntry({
            description: 'Bank Balance',
            proceeds: (entityBanking?.currentBalance === NOT_AVAILABLE
              ? entityBanking?.currentBalance
              : this.useCurrencyFormat(entityBanking?.currentBalance ?? 0)
            ).toString(),
          }),
          new FeesStatementEntry({
            description: 'Proceeds Collected From the Investors',
            proceeds: this.useCurrencyFormat(totalProceeds),
            isInvestorRow: true,
          }),
          new FeesStatementEntry({
            description: 'Total Amount Witheld From Investor Proceeds',
            expenses: this.useCurrencyFormat(totalFees),
          }),
          new FeesStatementEntry({
            description: 'Amount of Proceeds to be sent to Portfolio Company',
            proceeds: this.useCurrencyFormat(proceedsToSend),
            bold: true,
          }),
        ],
      }),
      new FeesStatementSection({
        // Requested by Robb, make these values double for MVP1
        // {
        //   title: 'SPV Expenses',
        //   subTitle: 'Enity Management & Maintenance Fees',
        //   entries: [
        //     {
        //       description: 'EIN Obtainment',
        //       expenses: "$15.00",
        //     },
        //     {
        //       description: 'Entity Dissolution',
        //       expenses: "$120.00",
        //     },
        //     {
        //       description: 'Entity Upkeep: DE Annual Report - 7 Years',
        //       expenses: "$840.00",
        //     },
        //   ],
        //   subTotal: "$975.00",
        // },
        title: 'SPV Expenses',
        subTitle: 'Entity Management & Maintenance Fees',
        entries: [
          new FeesStatementEntry({
            description: 'EIN Obtainment',
            expenses: this.useCurrencyFormat(costPerEIN),
          }),
          // TODO: Arch needed for this to be conditionally shown once/year for Master entity
          // new FeesStatementEntry({
          //   description: 'Entity Dissolution',
          //   expenses: '$240.00',
          // }),
          //   new FeesStatementEntry({
          //   description: 'Entity Upkeep: DE Annual Report - 7 Years',
          //   expenses: '$1680.00',
          // }),
        ],
      }),
      new FeesStatementSection({
        title: 'Administration Fees',
        entries: [
          new FeesStatementEntry({
            description: 'SPV Administration Fixed Cost per SPV',
            expenses: this.useCurrencyFormat(costPerSPV),
          }),
          new FeesStatementEntry({
            description: 'Management Fees',
            expenses: this.useCurrencyFormat(managementFeeAmount),
          }),
          new FeesStatementEntry({
            description: 'Expense Reserve',
            expenses: this.useCurrencyFormat(reserveFeeAmount),
          }),
        ],
      }),
      new FeesStatementSection({
        title: 'Tax and Accounting Fees',
        entries: [
          new FeesStatementEntry({
            description: 'Tax and K1 Processing',
            expenses: 'included',
          }),
        ],
      }),
      new FeesStatementSection({
        title: 'Regulatory Filings Fees',
        entries: [
          new FeesStatementEntry({
            description: 'NASAA EFD System Fee',
            expenses: this.useCurrencyFormat(costNASAA_EFD_System),
          }),
          ...blueSkyEntries.map(entry => new FeesStatementEntry(entry)),
        ],
      }),
      new FeesStatementSection({
        title: 'TOTALS',
        entries: [
          new FeesStatementEntry({
            description: '',
            expenses: this.useCurrencyFormat(totalFees),
          }),
        ],
      }),
    ];

    sections.forEach(section => {
      this.checkAndAppendProductsToSection(productsByCategory, section);
    });

    return new FeesStatement({
      title: `${tenant?.name} - ${deal.name}`,
      subTitle: 'SPV Closing Statement',
      investors: subscriptions.map(subscription => {
        return {
          name: String(
            subscription.profile?.firstName && subscription.profile?.lastName
              ? `${subscription.profile.firstName} ${subscription.profile.lastName}`
              : subscription.profile?.name,
          ),
          state: String(subscription.profile?.address?.state),
          amount: this.useCurrencyFormat(subscription?.amount ?? 0),
        };
      }),
      sections,
      totalProceeds,
      totalFees,
      proceedsToSend,
      spvFee,
      createdAt: new Date().toString(),
    });
  }

  getFlatEINPrice() {
    const FLAT_EIN_PRICE = 49;
    return FLAT_EIN_PRICE;
  }

  async getRealEINCost(entity) {
    const flatPrice = this.getFlatEINPrice();
    if (!entity.legalIncOrder?.orderId) {
      // Default value for now, need to consider off-platform entities for imported deals?
      return flatPrice;
    }

    const orderData = await this.filings.getLegalIncOrderDetails(
      entity.legalIncOrder.orderId,
    );

    // Make sure we charge at least flatPrice???
    // This is an assumed business logic piece, discuss!!!
    if (typeof orderData?.total_price === 'number') {
      return orderData.total_price > flatPrice
        ? orderData.total_price
        : flatPrice;
    }

    // LegaInc didn't have a price?
    return orderData?.total_price ?? flatPrice;
  }

  async getEINPrice(entity: Entities): Promise<number> {
    return this.getFlatEINPrice();
    /**
     * The above code is a placement for if we want always want a flat fee
     *
     * Code below could be useful if we want to make sure we never undercharge
     * for obtaining EINs.  NEEDS DISCUSSION THOUGH!!!
     */
    // return await this.getRealEINCost(entity);
  }

  private async getDefaultProducts() {
    const products: (Product & ProductRelations)[] =
      await this.productRepository.find({
        //where: {
        //  closingDefault: true,
        //},
      });

    // parsing to a float is a hack until we handle types in postgres (see below link for assumed issue)
    // https://github.com/strongloop/loopback-connector-postgresql/issues/403#issuecomment-573585160
    return products.map(product => {
      if (typeof product.priceAmount === 'string') {
        product.priceAmount = parseFloat(product.priceAmount);
      }
      return product;
    });
  }

  /**
   *
   * @param products
   * @returns {
   *            cat1: Product[],
   *            cat2: Product[],
   *            ...
   *          }
   */
  private groupProductsByCategory(products: Product[]) {
    const categories = {};
    const reducer = (productCategories, product) => {
      // See if the product category exists:
      if (!productCategories[product.category]) {
        productCategories[product.category] = [];
      }

      productCategories[product.category].push(product);
      return productCategories;
    };

    return products.reduce(reducer, categories);
  }

  /**
   *
   * @description helper to total up the amount in a products array
   * @returns number
   */
  private getTotalProductAmount(products: Product[]) {
    return products.reduce(
      (priceAmount, product) => priceAmount + (product?.priceAmount ?? 0),
      0,
    );
  }

  /**
   *
   * @description add a list of products to a section as expenses
   * @returns FeeStatementSection
   */
  private appendProductExpensesToSection(
    products: Product[],
    section: FeesStatementSection,
  ) {
    if (!Array.isArray(products)) {
      // Throw error?
      // Sentry?
      // is this a bad silent bug?
      return;
    }

    products.forEach(product => {
      section.entries.push(
        new FeesStatementEntry({
          description: product.description,
          expenses: this.useCurrencyFormat(product?.priceAmount ?? 0),
        }),
      );
    });
  }

  /**
   *
   * @description either adds product expenses to a section, or just returns the given section
   * @returns FeeStatementSection
   */
  private checkAndAppendProductsToSection(
    productsByCategory,
    section: FeesStatementSection,
  ) {
    if (
      productsByCategory[section.title] &&
      productsByCategory[section.title].length > 0
    ) {
      this.appendProductExpensesToSection(
        productsByCategory[section.title],
        section,
      );
    }
  }
}
