/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {HealthStatusDataSource} from '../datasources';

export interface HealthStatusService {
  getHealthStatus(userId: string): Promise<any>;
}

export interface HealthStatus {
  healthstats(req: {userId: string}): Promise<any>;
}

export class HealthStatusServiceProvider
  implements Provider<HealthStatusService>
{
  constructor(
    @inject('datasources.healthstatus')
    protected dataSource: HealthStatusDataSource = new HealthStatusDataSource(),
  ) {}

  async value(): Promise<HealthStatusService> {
    const healthService = await getService<HealthStatus>(this.dataSource);
    const service: HealthStatusService = {
      getHealthStatus: async (userId: string) => {
        const res = await healthService.healthstats({userId});
        return res;
      },
    };
    return service;
  }
}
