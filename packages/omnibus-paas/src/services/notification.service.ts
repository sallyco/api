/* eslint-disable  @typescript-eslint/no-explicit-any */
import {bind, BindingScope, inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import Handlebars from 'handlebars';
import KcAdminClient from 'keycloak-admin';
import moment from 'moment';
import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  DealRepository,
  EmailTemplateRepository,
  NotificationRepository,
} from '../repositories';
import {TenantService} from './tenant/tenant.service';
@bind({scope: BindingScope.TRANSIENT})
export class NotificationsService {
  private transporter: Mail;

  constructor(
    @repository(NotificationRepository)
    public notificationRepository: NotificationRepository,
    @inject('services.TenantService') private tenantService: TenantService,
    @inject(MultiTenancyBindings.CURRENT_TENANT)
    public tenant: Tenant,
    @repository(DealRepository) private dealRepository: DealRepository,
    @repository(EmailTemplateRepository)
    private emailTemplateRepository: EmailTemplateRepository,
  ) {
    const transport: SMTPTransport.Options = {
      host: process.env.SMTP_HOST,
      port: parseInt(process.env.SMTP_PORT ?? '25'),
      ignoreTLS: JSON.parse(process.env.SMTP_NO_SSL ?? 'false'),
    };

    if (process.env.SMTP_AUTH_USER && process.env.SMTP_AUTH_PASS) {
      transport.auth = {
        user: process.env.SMTP_AUTH_USER,
        pass: process.env.SMTP_AUTH_PASS,
      };
    }
    this.transporter = nodemailer.createTransport(transport);
    Handlebars.registerHelper('year', function () {
      return new Date().getFullYear();
    });
    Handlebars.registerHelper('formatDate', function (date) {
      return moment(date).format('MM/DD/YYYY');
    });
    Handlebars.registerHelper(
      'formatCurrency',
      function (amount, type = 'USD') {
        const formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: type,
        });
        return formatter.format(amount);
      },
    );
    Handlebars.registerHelper('getByKey', function (context, options) {
      if (!context) return '';
      return context.find(el => el.key === options.hash.key).value || '';
    });
  }

  async getMessage() {
    return 'MESSAGE DATA';
  }

  async getUser(recipient) {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
      // clientSecret: '9b3cf0af-5b53-4395-b427-def118fc5623',
    });
    return kcAdminClient.users.findOne({id: recipient, realm: this.tenant.id});
  }

  async sendNotification(user, message, notificationMethod, event?) {
    switch (notificationMethod) {
      case 'inApp':
        return this.createInAppNotification(user, message, event);
      case 'email':
        return this.createEmailNotification(user, message, event);
      case 'sms':
        return 'COMPLETED';
      default:
        return 'failure';
    }
  }

  async compileNotification(recipient, type, message, event?) {
    return this.getUser(recipient).then(user => {
      if (
        user?.attributes?.notifications &&
        JSON.parse(user?.attributes?.notifications) &&
        JSON.parse(user?.attributes?.notifications)[type]
      ) {
        const notification_settings = JSON.parse(
          user?.attributes?.notifications,
        )[type];
        return Promise.all(
          notification_settings.map(notificationMethod => {
            return this.sendNotification(
              user,
              message,
              notificationMethod,
              event,
            );
          }),
        ).then(el => 'complete');
      } else {
        return this.sendNotification(user, message, 'inApp').then(el => 'done');
      }
    });
  }

  async createInAppNotification(user, message, event) {
    return this.notificationRepository.create({
      event,
      userId: user.id,
      message,
    });
  }

  async createEmailNotification(user, message, event) {
    const currentTenant = await this.tenantService.getCurrentHydratedTenant();
    return this.transporter.sendMail({
      from:
        currentTenant?.settings?.emails?.fromAddress ??
        `${
          currentTenant.name ?? 'Glassboard Technology'
        } <no-reply@glassboardtech.com>`,
      to: user.email,
      subject: event,
      html: message,
      // replyTo: this.templates[emailType]?.replyTo,
      headers: {
        'X-SES-CONFIGURATION-SET': 'default',
      },
    });
  }

  async createSMSNotification(user, message, event) {
    return;
  }
}
