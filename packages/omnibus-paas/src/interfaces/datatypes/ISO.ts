import {getAlpha2Code, getNames, toAlpha2, toAlpha3} from 'i18n-iso-countries';
import moment from 'moment';
import USregions from 'iso3166-2-db/regions/US/dispute/UN/en.json';
import countrynames from 'countrynames';

export interface ISO_Alpha_2 extends String {
  // phantom property to enforce proper conversion to type
  __is_iso_alpha_2__: never;
}
export interface ISO_Alpha_3 extends String {
  // phantom property to enforce proper conversion to type
  __is_iso_alpha_2__: never;
}
export interface ISO_8601_Date extends String {
  // phantom property to enforce proper conversion to type
  __is_iso_8601__: never;
}
export interface ISO_3166_2_US extends String {
  __is_iso_3166_2: never;
}

export class ISOHelper {
  static convertToISO_3166_2_US(stateNameOrCode: string): ISO_3166_2_US {
    const region = USregions.find(item => {
      return (
        item.name.toLowerCase() === stateNameOrCode.toLowerCase() ||
        item.iso === stateNameOrCode
      );
    });
    if (!region) {
      throw new Error('Could not resolve ISO-3166-2:US for State');
    }
    return region.iso as unknown as ISO_3166_2_US;
  }
  static convertCountryNameToISO_2(
    countryName: string,
    looseMatch = true,
  ): ISO_Alpha_2 {
    let convertedCountry;
    try {
      convertedCountry = ISOHelper.convertStringToISO_2(countryName);
    } catch (e) {
      convertedCountry = getAlpha2Code(
        countryName,
        'en',
      ) as unknown as ISO_Alpha_2;
    }

    if (looseMatch && !convertedCountry) {
      convertedCountry = countrynames.getCode(
        countryName,
      ) as unknown as ISO_Alpha_2;
    }
    if (!convertedCountry) {
      throw new Error('Could not resolve country name to ISO_2');
    }
    return convertedCountry;
  }

  static convertCountryNameToISO_3(countryName: string): ISO_Alpha_3 {
    try {
      const convertedCountry = ISOHelper.convertCountryNameToISO_2(countryName);
      if (!convertedCountry) {
        throw new Error('Could not resolve country name to ISO_3');
      }
      return toAlpha3(convertedCountry.toString()) as unknown as ISO_Alpha_3;
    } catch (e) {
      throw new Error('Could not resolve country name to ISO_3');
    }
  }

  static convertISO_2ToISO_3(iso2: ISO_Alpha_2): ISO_Alpha_3 {
    return toAlpha3(iso2.toString()) as unknown as ISO_Alpha_3;
  }
  static convertISO_3ToISO_2(iso3: ISO_Alpha_3): ISO_Alpha_2 {
    return toAlpha2(iso3.toString()) as unknown as ISO_Alpha_2;
  }

  static convertToISO_8601_Date(date: string | Date): ISO_8601_Date {
    const convertedDate = moment(date).format('YYYY-MM-DD');
    if (!convertedDate) {
      throw new Error('Could not resolve date');
    }
    return convertedDate as unknown as ISO_8601_Date;
  }

  static convertStringToISO_2(string: string): ISO_Alpha_2 {
    const formattedString = string.toUpperCase();
    if (
      string.length > 2 ||
      !Object.keys(getNames('en')).includes(formattedString)
    ) {
      throw new Error(`String "${string}" is not ISO_Alpha_2 compatible`);
    }
    return formattedString as unknown as ISO_Alpha_2;
  }
}
