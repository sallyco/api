// GENERIC
/* eslint-disable @typescript-eslint/no-explicit-any */

export interface RequestResponse<T> {
  body: T;
  statusText: string;
  ok: boolean;
  status: number;
}

export interface ISO_8601_Date extends String {
  // phantom property to enforce proper conversion to type
  __is_iso_8601__: never;
}

export interface ISO_2 extends String {
  __is_iso_2__: never;
}

export interface Address {
  street_line_1: string;
  street_line_2?: string;
  city: string;
  // Two-Letter Abbreviation of State
  // ISO 3166-2?
  state: string;
  postal_code: string;
}

// APPLICATION

interface PersonApplication {
  citizenship: ISO_2;
  // Must be ISO_8601
  date_of_birth: ISO_8601_Date;
  document_ids?: string[];
  email_address: string;
  first_name: string;
  last_name: string;
  mailing_address: Address;
  middle_name?: string;
  occupation?: string;
  phone_number: string;
  physical_address: Address;
  userdata?: object;
}
export interface PersonApplicationRequest extends PersonApplication {
  bankdata?: object;
  tin: string;
}

export interface PersonApplicationResponse extends PersonApplication {
  id: string;
  updated_at?: string;
  created_at?: string;
}

interface BusinessApplication {
  bankdata?: object;
  dba?: string;
  description: string;
  document_ids?: string[];
  established_on: ISO_8601_Date;
  incorporation_state: string;
  legal_structure:
    | 'ccorp'
    | 'llc'
    | 'partnership'
    | 'nonprofit'
    | 'scorp'
    | 'soleprop';
  mailing_address?: Address;
  naics: string;
  naics_description: string;
  name: string;
  parent_businesses?: {
    id: string;
    ownership_percentage: number;
  }[];
  person_applications: {
    id: string;
    roles: PersonApplicationRoles[];
    ownership_percentage: number;
    title: string;
  }[];
  phone_number: string;
  physical_address: Address;
  tin: string;
  urls?: string[];
  userdata?: object;
}
type PersonApplicationRoles = 'signer' | 'control_person';

export interface BusinessApplicationRequest extends BusinessApplication {}
export interface BusinessApplicationResponse extends BusinessApplication {
  bank_id: string;
  created_at: string;
  id: string;
}
type ApplicationStatus =
  | 'unsubmitted'
  | 'submitted'
  | 'processing'
  | 'manual_review'
  | 'rejected'
  | 'approved';
type AccountPersonRoles = 'owner' | 'signer';
type AccountProduct =
  | 'personal_savings'
  | 'personal_checking'
  | 'business_savings'
  | 'business_checking'
  | 'glassboard_checking';
interface AccountPerson {
  id: string;
  roles: AccountPersonRoles[];
}
interface AccountApplication {
  bankdata?: object;
  deposit_id?: string;
  product: AccountProduct | string;
  userdata?: object;
  primary_person_application_id: string;
}
interface PersonalAccountApplicationRequest extends AccountApplication {
  person_applications: AccountPerson[];
  business_application_id?: undefined;
}
interface BusinessAccountApplicationRequest extends AccountApplication {
  business_application_id: string;
  person_applications?: undefined;
}
export type AccountApplicationRequest =
  | PersonalAccountApplicationRequest
  | BusinessAccountApplicationRequest;
export interface AccountApplicationResponse extends AccountApplication {
  id: string;
  account_id?: string;
  business_application_id?: string;
  ownership_type: 'business' | 'personal';
  product: AccountProduct;
  person_applications?: AccountPerson[];
  created_at: string;
  updated_at: string;
  status: ApplicationStatus;
}

// ACCOUNT

type AccountType = 'checking' | 'savings';
type AccountStatus = 'open' | 'closed' | 'pending';
export interface AccountResponse {
  account_number: string;
  account_type: AccountType;
  address: Address;
  available_balance: string;
  business_ids: string[];
  bank_id: string;
  created_at: string;
  currency: 'USD';
  current_balance: string;
  id: string;
  name: string;
  primary_person_id: string;
  person_ids: string[];
  routing_number: string;
  status: AccountStatus;
  locked: boolean;
  funded: boolean;
  updated_at: string;
  userdata?: object;
}

// COUNTERPARTY

interface CounterpartyACH {
  account_number: string;
  routing_number: string;
  account_type: AccountType;
}
interface CounterpartyWire {
  account_number: string;
  routing_number: string;
  bank_address: Address;
  address_on_account: Address;
  bank_name: string;
}
interface Counterparty {
  userdata?: object;
  name_on_account: string;
}
interface CounterpartyACHRequest extends Counterparty {
  ach: CounterpartyACH;
  wire?: undefined;
}
interface CounterpartyWireRequest extends Counterparty {
  wire: CounterpartyWire;
  ach?: undefined;
}
export type CounterpartyRequest =
  | CounterpartyACHRequest
  | CounterpartyWireRequest;

interface CounterpartyBaseResponse extends Counterparty {
  id: string;
  created_at: string;
  updated_at: string;
}
interface CounterpartyACHResponse extends CounterpartyBaseResponse {
  ach: CounterpartyACH;
  wire: undefined;
}
interface CounterpartyWireResponse extends CounterpartyBaseResponse {
  wire: CounterpartyWire;
  ach: undefined;
}
export type CounterpartyResponse =
  | CounterpartyACHResponse
  | CounterpartyWireResponse;

interface ACHTransaction {
  account_id: string;
  addenda?: string[];
  amount: string;
  counterparty_id: string;
  description?: string;
  direction: 'credit' | 'debit';
  sec_code: 'ccd' | 'ppd' | 'web';
  service?: 'sameday' | 'standard';
  userdata?: object;
}
export interface ACHTransactionRequest extends ACHTransaction {}
export interface ACHTransactionResponse extends ACHTransaction {
  created_at: string;
  effective_date: string;
  error?: string;
  id: string;
  status: 'pending' | 'processing' | 'canceled' | 'sent' | 'returned';
}
export interface SettingsResponse {
  created_at: string;
  updated_at: string;
  ach_limit_credit: number;
  id: string;
  wire_approvals: number;
  wire_daily_limit: number;
  ach_limit_debit: number;
}
export interface AccountTransactionResponse {
  amount: number;
  balance: number;
  date: string;
  desc: string;
  id: string;
  summary?: string;
  type:
    | 'charge'
    | 'deposit'
    | 'hold'
    | 'interest'
    | 'payment'
    | 'reversal'
    | 'withdrawal'
    | 'null';
  wire?: string;
  ach_id?: string;
  fingerprint: string;
  wire_id?: string;
  wire_description?: string;
  check_id?: string;
  check_number?: string;
  user_data?: any;
  type_source?: string;
  book_id?: string;
  incoming_wire?: string;
  billpay_payment_id?: string;
  trace_id?: string;
  extended_timestamp?: string;
}

export interface PaginatedResponse<T> {
  data: T;
  length: number;
  page_next?: string;
}

// Wires

export interface IntermediaryBank {
  name: string;
  routing_number: string;
  address?: Address;
}

export interface WireTransfer {
  account_id: string;
  amount: string;
  counterparty_id: string;
  instructions?: string;
  intermediary?: IntermediaryBank;
  created_at?: string;
  error?: string;
  id?: string;
  status?:
    | 'pending' // Can still be cancelled
    | 'canceled' // The transfer was canceled and was never executed.
    | 'processing' // Can not be cancelled anymore
    | 'sent' // The transfer has been executed and sent to the bank.
    | 'error'; // An error occurred that prevented the wire from being sent.
  updated_at?: string;
  userdata?: any;
}

// Books

export interface BookTransfer {
  amount: string;
  created_at?: string;
  description?: string;
  error?: string;
  from_account_id: string;
  id?: string;
  status?: string;
  to_account_id: string;
  updated_at?: string;
  userdata?: any;
}
