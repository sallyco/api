/* eslint-disable @typescript-eslint/no-explicit-any */
export namespace ILegalInc {
  export namespace WebHooks {
    export interface Document {
      documentable_id: number;
      user_id: number;
      status: string;
      file_name: string;
      uploaded_file_name: string;
      file_url: string;
      document_type: number;
      source: string;
      metadata: string;
      should_print: number;
      documentable_type: string;
      updated_at: Date;
      created_at: Date;
      id: number;
      download_url: string;
      user: User;
      order: Order;
      order_id: number;
    }

    export interface User {
      id: number;
      userable_type: string;
      userable_id: number;
      name: string;
      email: string;
      status_id: number;
      acceptance_terms: Date;
      is_connected_to_quickbooks: number;
      created_at: Date;
      updated_at: Date;
      type: string;
      tag: string;
      role: null;
      notification_on: number;
      on_crc_login: number;
      activation_key: string;
      onboarding_metadata: null;
      access_level: null;
      printer_id: string;
      deleted_at: null;
      last_logged_in: Date;
      is_active: boolean;
      status: string;
    }

    export interface Order {
      id: number;
      description: string;
      client_id: number;
      status_id: ILegalInc.API.OrderStatus;
      exception_id: null;
      exception_date: null;
      exception_follow_up_date: null;
      irs_date: null;
      start_date: null;
      irs_follow_up_date: null;
      cancelled_id: null;
      cancelled_date: null;
      package_price: number;
      total_price: number;
      affiliate_id: number;
      affiliate_source_code: string;
      parent_company_id: number;
      orderable_id: number;
      orderable_type: string;
      service_level_id: number;
      state_id: number;
      foreign_id: null;
      expected_date: Date;
      used_retail_price: number;
      expected_shipment_date: Date;
      shipped_date: null;
      shipment_confirmation_sent: number;
      assigned_to: null;
      placed_by: number;
      account_manager_id: null;
      sales_rep_id: number;
      status_updated_at: Date;
      created_at: Date;
      updated_at: Date;
      external_id: string;
      requester_name: string;
      matter_number: null;
      sr_number: null;
      order_source: string;
      parent_order_id: null;
    }

    export interface Company {
      activity: null;
      addresses?: Address[];
      agent: null;
      agent_id: null;
      annualized_total_revenue: null;
      arf_pay_recurring: string;
      arf_review_status: string;
      auto_renew: number;
      auto_renew_company_id: null;
      client_id: number;
      clio_company_id: null;
      company_type: string;
      contacts: Contact[];
      created_at: Date;
      created_by: number;
      deleted_at: null;
      description: null;
      domestic_state_id: number;
      ein: string | null;
      encrypted_id: string;
      entity_number: string;
      external_id: string;
      filing_combined_group_report: null;
      fiscal_year: string;
      foreign_id: null;
      formation_on: Date | null;
      gross_assets: null;
      id: number;
      is_tiered_partnership: null;
      issued_shares: null;
      legacy_company_id: number;
      managed_by: null;
      metadata: string;
      name: string;
      par: null;
      parent_id: number;
      pay_initial: string;
      pay_recurring: string;
      purpose: number;
      purpose_description: null;
      registered_state_id: number;
      shares: null;
      source_client_id: null;
      state: State;
      state_status?: StateStatus;
      state_status_id: number;
      status_id: number;
      subscribed_company_id: number;
      tax_payer_id_number: null;
      updated_at: Date;
      web_file_number: null;
    }

    export interface Address {
      id: number;
      address_type: string;
      addressable_id: number;
      addressable_type: string;
      address: string;
      address2: string;
      city: string;
      state: string;
      zip: string;
      country: string;
      created_at: Date;
      updated_at: Date;
      deleted_at: null;
    }

    export interface Contact {
      id: number;
      contact_type: string;
      contactable_type: string;
      contactable_id: number;
      first_name: string;
      last_name: string;
      percentage: null | string;
      units: null;
      ssn: null | string;
      created_at: Date;
      updated_at: Date;
      title: null;
      capital_contribution: null | string;
      share_amount: null;
      consideration_amount: null;
      business_type: string;
      share_acquired_date: string;
      deleted_at: null;
      deactivated_at: null;
      addresses: Address[];
      emails: Email[];
      phones: Phone[];
    }

    export interface Email {
      id: number;
      email_type: string;
      emailable_id: number;
      emailable_type: string;
      email: string;
      created_at: Date;
      updated_at: Date;
      deleted_at: null;
    }

    export interface Phone {
      id: number;
      phone_type: string;
      phoneable_id: number;
      phoneable_type: string;
      number: string;
      extention: null;
      created_at: Date;
      updated_at: Date;
      deleted_at: null;
    }

    export interface State {
      id: number;
      name: string;
      code: string;
      url: string;
      name_search_link: string;
      filing_method: string;
      payment_method: string;
      standard_processing_time: string;
      express_processing_time: string;
      user_name: string;
      password: string;
      forms_link: string;
      filing_notes: string;
      organizer: string;
      created_at: Date;
      updated_at: Date;
      check_payable_to: null;
      country: string;
    }

    export interface StateStatus {
      id: number;
      status: string;
      type: string;
      status_order: number;
      created_at: Date;
      updated_at: Date;
      action: string;
      customer_status: string;
      description: string;
    }
  }
  export namespace APIResponse {
    export interface ResponseWrapper {
      response: string;
      message: string;
      code: number;
      success: boolean;
    }

    export interface Token {
      access_token: string;
      token_type: string;
      expires_in: number;
      refresh_token: string;
    }
  }
  export namespace API {
    export interface Order {
      id?: number;
      description: string;
      status_id: OrderStatus;
      payment_by_card: number;
      client_id: string;
      total_price: number;
      affiliate_id: number;
      service_level_id: number;
      product_id: string;
      package_id: string;
      state_id: string;
      order_contact: Contact[];
      external_id: string;
      matter_number: string;
      requester_name: string;
      tax: Tax[];
      note: string;
      registered_agent: boolean;
      company: Company;
    }

    export interface State {
      id: number;
      name: string;
      code: string;
      url?: string;
      name_search_link?: string;
      filing_method?: string;
      payment_method?: string;
      standard_processing_time?: string;
      express_processing_time?: string;
      user_name?: string;
      password?: string;
      forms_link?: string;
      filing_notes?: string;
      organizer?: string;
      created_at?: Date;
      updated_at?: Date;
      check_payable_to?: null;
      country?: string;
    }

    export interface Company {
      name: string;
      company_type: string;
      client_id: string;
      registered_state_id: string;
      domestic_state_id?: string;
      fiscal_year: string;
      entity_number?: string;
      formation_on?: string;
      purpose?: string;
      address: Address;
      contacts: Contact[];
      ein?: string;
      state?: State;
    }

    export interface Address {
      address: string;
      address2?: string;
      city: string;
      country: string;
      state: string;
      zip: string;
      id?: string;
      address_type?: string;
    }

    export interface Contact {
      business_type: string;
      first_name: string;
      last_name: string;
      contact_type: string;
      addresses?: Address[];
      emails?: Email[];
      phones?: Phone[];
      percentage?: string;
      capital_contribution?: string;
    }

    export interface Email {
      email: string;
      email_type: string;
    }

    export interface Phone {
      number: string;
      phone_type: string;
    }

    export interface Tax {
      first_name: string;
      last_name: string;
      contact_type: string;
      ssn: string;
      addresses: Address[];
      emails?: Email[];
      phones: Phone[];
    }

    export interface Payment {
      amount: number;
      payer_id: string | number;
      order_id: number;
    }

    export enum OrderStatus {
      NONE = 0,
      NEW = 1,
      NAME_CHECK = 2,
      DOC_PREP = 3,
      STATE_PREP = 4,
      AT_STATE = 5,
      FULFILLMENT = 6,
      REVIEW = 7,
      SHIPPING = 8,
      COMPLETED = 9,
      CANCELLED = 10,
      EXCEPTION = 11,
      HOLD = 12,
      OG_COPY = 13,
      IRS = 19,
      PRE_ORDER = 20,
    }
  }
  export namespace Data {
    export interface Order {
      id?: number;
      description?: string;
      client_id?: number;
      status_id?: number;
      exception_id?: null;
      exception_date?: null;
      exception_follow_up_date?: null;
      irs_date?: null;
      start_date?: null;
      irs_follow_up_date?: null;
      cancelled_id?: null;
      cancelled_date?: null;
      package_price?: number;
      total_price?: number;
      affiliate_id?: number;
      affiliate_source_code?: string;
      parent_company_id?: number;
      orderable_id?: number;
      orderable_type?: string;
      service_level_id?: number;
      state_id?: number;
      foreign_id?: null;
      expected_date?: Date;
      used_retail_price?: number;
      expected_shipment_date?: Date;
      shipped_date?: null;
      shipment_confirmation_sent?: number;
      assigned_to?: null;
      placed_by?: number;
      account_manager_id?: null;
      sales_rep_id?: number;
      status_updated_at?: null;
      created_at?: Date;
      updated_at?: Date;
      external_id?: string;
      requester_name?: string;
      matter_number?: null;
      sr_number?: null;
      order_source?: string;
      parent_order_id?: null;
      company?: Company;
      client?: Client;
      creator?: Creator;
      status?: Status;
      service_level?: ServiceLevel;
      shipment?: any[];
      order_item?: OrderItem[];
      payment?: Payment[];
      user?: null;
      exception?: null;
      contacts?: Contact[];
      notes?: Note[];
      document?: any[];
      history?: History[];
      company_history?: CompanyHistory[];
      newStartDate?: Date;
      payable_to?: null;
      invoice?: any[];
      pending_backend_tasks?: any[];
      client_notes?: any[];
      net_paid?: number;
      total?: number;
    }

    export interface Client {
      id?: number;
      distributor_id?: number;
      name?: string;
      foreign_id?: string;
      account_type?: string;
      physical_copy_required?: number;
      sales_rep_id?: number;
      account_manager_id?: null;
      clio_token?: null;
      clio_account_id?: null;
      clio_updated_since?: null;
      lead_source?: string;
      payment_terms?: string;
      billing_type?: string;
      pay_initial?: string;
      pay_recurring?: string;
      auto_transfer?: string;
      arf_pay_recurring?: string;
      preferred_payment_method?: string;
      logo?: string;
      order_review_flag?: number;
      order_confirmation?: string;
      shipment_confirmation?: string;
      stripe_connect_account?: null;
      created_at?: Date;
      updated_at?: Date;
      hiveage_connection_key?: null;
      sf_account_id?: string;
      entity_size?: null;
      segment?: string;
      acs_foreign_id?: null;
      mail_forwarding_types?: string;
      mail_forward_to?: string;
      hubspot_id?: null;
      lob_renewal_invoices?: number;
      contacts?: Contact[];
      distributor?: Distributor;
      sales_rep?: Creator;
      account_manager?: null;
      parent_client?: any[];
    }

    export interface Contact {
      id?: number;
      first_name?: string;
      last_name?: string;
      ssn?: null | string;
      created_at?: Date;
      updated_at?: Date;
      business_type?: string;
      deleted_at?: null;
      is_manager?: boolean;
      relation_type?: string;
      contact_type?: string;
      share_acquired_date?: string;
      relationship_id?: number;
      relationship_type_id?: number;
      contactable_type?: string;
      contactable_id?: number;
      reference_id?: null;
      percentage?: null;
      units?: null;
      capital_contribution?: null;
      share_amount?: null;
      consideration_amount?: null;
      title?: null | string;
      sort_order?: number;
      member_type?: string;
      pivot?: Pivot;
      addresses?: Address[];
      emails?: Email[];
      phones?: Phone[];
    }

    export interface Address {
      id?: number;
      address_type?: string;
      addressable_id?: number;
      addressable_type?: string;
      address?: string;
      address2?: string;
      city?: string;
      state?: string;
      zip?: string;
      country?: string;
      created_at?: Date;
      updated_at?: Date;
      deleted_at?: null;
    }

    export interface Email {
      id?: number;
      email_type?: string;
      emailable_id?: number;
      emailable_type?: string;
      email?: string;
      created_at?: Date;
      updated_at?: Date;
      deleted_at?: null;
    }

    export interface Phone {
      id?: number;
      phone_type?: string;
      phoneable_id?: number;
      phoneable_type?: string;
      number?: string;
      extention?: null | string;
      created_at?: Date;
      updated_at?: Date;
      deleted_at?: null;
    }

    export interface Pivot {
      relationshipable_id?: number;
      contact_id?: number;
    }

    export interface Distributor {
      id?: number;
      name?: string;
      created_at?: Date;
      updated_at?: Date;
    }

    export interface Creator {
      id?: number;
      userable_type?: string;
      userable_id?: number;
      name?: string;
      email?: string;
      role_id?: number | null;
      status_id?: number;
      acceptance_terms?: Date;
      is_connected_to_quickbooks?: number;
      created_at?: Date;
      updated_at?: Date;
      type?: string;
      tag?: string;
      role?: null;
      notification_on?: number;
      on_crc_login?: number;
      activation_key?: string;
      onboarding_metadata?: null;
      access_level?: null;
      printer_id?: null;
      auth0_user_id?: string;
      deleted_at?: null;
      last_logged_in?: Date | null;
      is_active?: boolean;
      status?: string;
    }

    export interface Company {
      id?: number;
      name?: string;
      description?: null;
      company_type?: string;
      client_id?: number;
      source_client_id?: null;
      agent_id?: null;
      parent_id?: number;
      subscribed_company_id?: number;
      registered_state_id?: number;
      domestic_state_id?: number;
      foreign_id?: null;
      status_id?: number;
      state_status_id?: number;
      clio_company_id?: null;
      entity_number?: null;
      legacy_company_id?: number;
      formation_on?: null;
      fiscal_year?: string;
      purpose?: number;
      purpose_description?: null;
      shares?: null;
      par?: null;
      gross_assets?: null;
      issued_shares?: null;
      managed_by?: null;
      activity?: null;
      ein?: string;
      pay_initial?: string;
      pay_recurring?: string;
      is_legalinc_ra_sos?: string;
      arf_pay_recurring?: string;
      arf_review_status?: string;
      auto_renew?: number;
      auto_renew_company_id?: null;
      created_by?: number;
      metadata?: null;
      filing_combined_group_report?: null;
      is_tiered_partnership?: null;
      is_professional?: string;
      annualized_total_revenue?: null;
      created_at?: Date;
      updated_at?: Date;
      external_id?: string;
      tax_payer_id_number?: null;
      web_file_number?: null;
      deleted_at?: null;
      managed_by_alias?: string;
      contacts?: Contact[];
      addresses?: Address[];
      domestic_state?: State;
      state?: State;
      agent?: null;
      encrypted_id?: string;
      arf_enrollment?: boolean;
      arf_enrollment_details?: string;
    }

    export interface State {
      id?: number;
      name?: string;
      code?: string;
      url?: string;
      name_search_link?: string;
      filing_method?: string;
      payment_method?: string;
      standard_processing_time?: string;
      express_processing_time?: string;
      user_name?: string;
      password?: string;
      forms_link?: string;
      filing_notes?: string;
      organizer?: string;
      created_at?: Date;
      updated_at?: Date;
      check_payable_to?: null;
      country?: string;
      phones?: Phone[];
      addresses?: Address[];
      processing_time?: any[];
    }

    export interface CompanyHistory {
      user?: string;
      created_at?: string;
      field_display_name?: string;
      old_value?: string;
      new_value?: string;
    }

    export interface History {
      created_at?: string;
      old_status?: string;
      user?: string;
      new_status?: string;
      old_status_id?: number;
      description?: string;
      new_status_id?: Date;
    }

    export interface Note {
      id?: number;
      note?: string;
      added_by?: number;
      notable_id?: number;
      notable_type?: string;
      is_acknowledged?: number;
      note_type?: null;
      public_note?: number;
      created_at?: Date;
      updated_at?: Date;
      deleted_at?: null;
      parent_id?: number;
      user?: Creator;
    }

    export interface OrderItem {
      id?: number;
      order_id?: number;
      order_item_type?: string;
      company_id?: number;
      product_id?: number;
      description?: string;
      price?: number;
      retail_price?: number;
      revenue_rate?: number;
      rate_id?: number;
      service_level_id?: number;
      fee_id?: null;
      state_id?: number;
      county_id?: null;
      is_check_amount?: number;
      is_complete?: null;
      deleted_at?: null;
      created_at?: Date;
      updated_at?: Date;
      invoice_number?: null;
      is_legacy_renewal_net30?: null;
      company?: Company;
      fee?: null;
      state?: State;
      product?: Product;
      rate?: Rate;
      service_level?: ServiceLevel;
    }

    export interface Product {
      id?: number;
      name?: string;
      description?: string;
      type?: string;
      processing_required?: number;
      is_state_specific?: null;
      validation_rules?: string;
      besp_price?: number;
      term?: number;
      autorenew_starting_event?: number;
      created_at?: Date;
      updated_at?: Date;
      deleted_at?: null;
    }

    export interface Rate {
      id?: number;
      name?: string;
      description?: string;
      client_id?: number;
      price?: number;
      default_price?: number;
      revenue_rate?: number;
      type?: string;
      addon_type?: null;
      count?: number;
      included?: number;
      product_id?: number;
      service_level_id?: number;
      package_id?: number;
      deleted_at?: null;
      created_at?: Date;
      updated_at?: Date;
    }

    export interface ServiceLevel {
      id?: number;
      name?: string;
      description?: string;
      hours?: number;
      created_at?: Date;
      updated_at?: Date;
    }

    export interface Payment {
      id?: number;
      order_id?: number;
      type?: string;
      payment_method_id?: number;
      payment_number?: number;
      charge_id?: string;
      amount_paid?: number;
      payment_amount?: number;
      application_fee?: null;
      approval_code?: string;
      merchant_id?: number;
      created_at?: Date;
      updated_at?: Date;
      void_check?: number;
      payment_type?: string;
      payable_to?: null;
      stripe_payment_type?: null;
    }

    export interface Status {
      id?: number;
      status?: string;
      type?: string;
      status_order?: number;
      created_at?: Date;
      updated_at?: Date;
      action?: string;
      customer_status?: string;
      description?: string;
    }
    export enum StateId {
      'ALASKA' = 1,
      'ALABAMA' = 2,
      'ARKANSAS' = 3,
      'ARIZONA' = 4,
      'CALIFORNIA' = 5,
      'COLORADO' = 6,
      'CONNECTICUT' = 7,
      'DISTRICT OF COLUMBIA' = 8,
      'DELAWARE' = 9,
      'FLORIDA' = 10,
      'GEORGIA' = 11,
      'HAWAII' = 12,
      'IOWA' = 13,
      'IDAHO' = 14,
      'ILLINOIS' = 15,
      'INDIANA' = 16,
      'KANSAS' = 17,
      'KENTUCKY' = 18,
      'LOUISIANA' = 19,
      'MASSACHUSETTS' = 20,
      'MARYLAND' = 21,
      'MAINE' = 22,
      'MICHIGAN' = 23,
      'MINNESOTA' = 24,
      'MISSOURI' = 25,
      'MISSISSIPPI' = 26,
      'MONTANA' = 27,
      'NORTH CAROLINA' = 28,
      'NORTH DAKOTA' = 29,
      'NEBRASKA' = 30,
      'NEW HAMPSHIRE' = 31,
      'NEW JERSEY' = 32,
      'NEW MEXICO' = 33,
      'NEVADA' = 34,
      'NEW YORK' = 35,
      'OHIO' = 36,
      'OKLAHOMA' = 37,
      'OREGON' = 38,
      'PENNSYLVANIA' = 39,
      'RHODE ISLAND' = 40,
      'SOUTH CAROLINA' = 41,
      'SOUTH DAKOTA' = 42,
      'TENNESSEE' = 43,
      'TEXAS' = 44,
      'UTAH' = 45,
      'VIRGINIA' = 46,
      'VERMONT' = 47,
      'WASHINGTON' = 48,
      'WISCONSIN' = 49,
      'WEST VIRGINIA' = 50,
      'WYOMING' = 51,
      'AK' = 1,
      'AL' = 2,
      'AR' = 3,
      'AZ' = 4,
      'CA' = 5,
      'CO' = 6,
      'CT' = 7,
      'DC' = 8,
      'DE' = 9,
      'FL' = 10,
      'GA' = 11,
      'HI' = 12,
      'IA' = 13,
      'ID' = 14,
      'IL' = 15,
      'IN' = 16,
      'KS' = 17,
      'KY' = 18,
      'LA' = 19,
      'MA' = 20,
      'MD' = 21,
      'ME' = 22,
      'MI' = 23,
      'MN' = 24,
      'MO' = 25,
      'MS' = 26,
      'MT' = 27,
      'NC' = 28,
      'ND' = 29,
      'NE' = 30,
      'NH' = 31,
      'NJ' = 32,
      'NM' = 33,
      'NV' = 34,
      'NY' = 35,
      'OH' = 36,
      'OK' = 37,
      'OR' = 38,
      'PA' = 39,
      'RI' = 40,
      'SC' = 41,
      'SD' = 42,
      'TN' = 43,
      'TX' = 44,
      'UT' = 45,
      'VA' = 46,
      'VT' = 47,
      'WA' = 48,
      'WI' = 49,
      'WV' = 50,
      'WY' = 51,
    }
  }
}
