import {OpenApiSpec} from '@loopback/openapi-v3';

const APISpec: OpenApiSpec = {
  openapi: '3.0.0',
  info: {
    title: 'Glassboard API Reference',
    description:
      '<aside class="warning">Note:  This API specification is currently beta.  The schema definitions as described are not subject to change.  However new fields or schemas may be added.  Please contact <a href="mailto:support@glassboardtech.com">support@glassboardtech.com</a> for questions or feedback.</aside>\n\nThe Glassboard API is organized around [REST](http://en.wikipedia.org/wiki/Representational_State_Transfer).  Our API has predictable resource-oriented URLs, accepts and returns [JSON-encoded](http://www.json.org/) request and responses bodies, and uses standard HTTP response codes, authentication, and verbs.\n\nThe Glassboard API may differ for every account as we release new versions and tailor functionality based on customer needs.  The API specification below is universally available.\n\n## Cross-Origin Resource Sharing\nThis API features Cross-Origin Resource Sharing (CORS) implemented in compliance with  [W3C spec](https://www.w3.org/TR/cors/). And that allows cross-domain communication from the browser.\n\nAll responses have a wildcard same-origin which makes them completely public and accessible to everyone, including any code on any site.\n## Request Basics\n### Headers\nCurrently, this API only communicates with JSON, so in some situations you may need to set `Accept` or `Content-Type` to `application/json`.\nGlassboard Technology enables platforms and applications for a variety of organizations and users. To ensure an orderly separation of resource ownership this API requires a tenant identification in all requests.  In the request header, set the value `x-tenant-id` to your id. All requests without this value will be rejected with a 401 response.\n',
    version: '1',
    termsOfService: 'https://glassboardtech.com',
    contact: {
      name: 'API Support',
      url: 'https://glassboardtech.com',
      email: 'info@glassboardtech.com',
    },
  },
  paths: {},
  components: {
    securitySchemes: {
      oAuth2: {
        type: 'oauth2',
        description:
          'This API only supports [OAuth 2.0](https://oauth.net/2/) for authentication and authorization. Specifically, it supports two options for OAuth2 grants, Client Credential and Authorization Code\nOAuth 2.0 is the industry-standard protocol for authorization. OAuth 2.0 focuses on client developer simplicity while providing specific authorization flows for web applications, desktop applications, mobile phones, and living room devices. This list describes the components of an OAuth2 grant flow:\n<ul> <li>Client Application — the application that needs to access the protected resource.</li> <li>Resource Owner — the owner of a protected resource, this is typically a user.  A protected resource could be a bank account.</li> <li>Authorization Endpoint(s) — the API service that issues auth tokens.</li> <li>Resource Endpoint(s) — the API service that returns the requested protected resource.</li> </ul>\n\nThe Client Credentials grant type is used by clients to obtain an access token outside of the context of a user.  This is typically used by clients to access resources about themselves rather than to access a user\'s resources.\n<img src="./images/spec/oauth2-client-credentials.png" />\nThe process to obtain a token:\n<ol> <li>The Client Application sends an request to the Authorization Endpoing (`/oauth2/token`) with the client id and client secret in the request body. This request may include requested scopes.</li> <li>In the response, the Authorization Endpoint sends an Access Token (JWT).</li> <li>The Client Application uses the Access Token to call Resource Endpoints.</li> </ol>\n\nThe Authorization Code grant type is used by confidential and public clients to exchange an authorization code for an access token.  After the user returns to the client via the redirect URL, the application will get the authorization code from the URL and use it to request an access token.\n<img src="./images/spec/oauth2-auth-code.png" />\n<ol> <li>The Client initiates the flow by redirecting the User Agent of the Resource Owner to the Authorization Server. The Client includes its Client ID, requested scopes, and the redirect URI.</li> <li>Resource Owner authorizes the Client by granting permissions requested by the Client.</li> <li>Authorization Server redirects the User Agent back to the Client (using redirect URI from point 1). The redirect URI includes a temporary Authorization Code (as a query param).</li> <li>Client requests an Access Token from the Authorization Server. The request includes Client ID, Client Secret, and Authorization Code received in the previous step.</li> <li>If everything is valid, the Authorization Server returns Access Token and, optionally, a Refresh Token.</li> <li>The client uses the Access Token to call the Resource Server on behalf of the Resource Owner.</li> </ol>\n',
        flows: {
          clientCredentials: {
            tokenUrl: '/oauth2/token',
            refreshUrl: '/oauth2/refresh',
            scopes: {
              read: 'Grant read-only access to all your data except for the account and user info',
              write:
                'Grant write-only access to all your data except for the account and user info',
            },
          },
          authorizationCode: {
            authorizationUrl:
              '/auth/realms/gbt/.well-known/openid-configuration',
            tokenUrl: '/auth/realms/gbt/.well-known/openid-configuration',
            refreshUrl: '/auth/realms/gbt/.well-known/openid-configuration',
            scopes: {},
          },
        },
      },
    },
  },
  security: [
    {
      oAuth2: [],
    },
  ],
  tags: [
    {
      name: 'OAuth2',
      description:
        'This set of authorization endpoints describe how to securely communicate with the API.',
    },
    {
      name: 'Accounts',
      description: 'Everything about accounts',
    },
    {
      name: 'Assets',
      description:
        'Assets represent the investment vehicle for a deal.  An asset can take one of many forms, such as, shares of common stock in a company, real estate, crypocurrency, or even fractional ownership in a private jet.  The object specification is general enough to include a complete description of the asset, supporting documentation, and profiles of the key team members.',
    },
    {
      name: 'Banking',
      description: 'Everything about banking',
    },
    {
      name: 'Closes',
      description:
        'A Close groups Subscriptions together for completion of the investment process. A close can represent the close of the entire deal or just a subset of investments (i.e. a tranche).',
    },
    {
      name: 'Companies',
      description:
        'This object can store general information about a Company for a variety of purposes.  Some examples include fund managers, portfolio companies, or registered agents.',
    },
    {
      name: 'Deals',
      description:
        'A deal is a fundamental object that represents the opportunity into which user make an investment.',
    },
    {
      name: 'Entities',
      description:
        'An entity (typically a fund) is the legal entity that gets generated during the deal creation process.',
    },
    {
      name: 'Files',
      description:
        'Files of all kinds that can be attached to other objects or stand alone.',
    },
    {
      name: 'Invites',
      description:
        'Used for onboarding new users. The invite object can conveniently create all the objects required when a new user is created.',
    },
    {
      name: 'Legal',
      description: 'Formations, filings and compliance.',
    },
    {
      name: 'Ping',
      description: 'System ping and other health checks',
    },
    {
      name: 'Product',
      description: 'Products configure operating costs and allowances.',
    },
    {
      name: 'Profiles',
      description:
        'A Profile contains all the details for a individual, trust, joint account, etc. ' +
        ' These profiles can be defined and used for a variety of purposes, such as: organizing deals,' +
        ' making investments, or creating assets. A profile is used to complete tasks such as generating documents ' +
        'and performing KYC/KYB',
    },
    {
      name: 'Subscriptions',
      description: 'A Subscription is the investment into a Deal.',
    },
    {
      name: 'Users',
      description:
        'User objects are a generic type that includes all users in the system',
    },
    {
      name: 'Webhooks',
      summary: 'Webhooks allow your system to subscribe to updates',
      description: `Webhooks allow your system to subscribe to updates via an HTTPS POST request
       from our system to yours. 
       
       You can selectively subscribe to different resources and events`,
    },
    {
      name: 'Workflows',
      description: 'Workflows group together common operations',
    },
    {
      name: 'Identity',
      description: 'Operations related to validating individual identities',
    },
    {
      name: 'KYC',
      description: 'Background and Monitored Lists checks for KYC',
    },
    {
      name: 'Accreditation',
      description: 'Accreditations and Accreditation Providers',
    },
  ],
};
export default APISpec;
