import {model, property} from '@loopback/repository';
import GenericEntity from './GenericEntity';

@model({
  settings: {
    hiddenProperties: ['providerMeta'],
  },
})
export default abstract class ProviderEntity extends GenericEntity {
  @property({
    type: 'object',
    required: false,
    jsonSchema: {
      type: 'object',
      additionalProperties: true,
      required: ['typeId'],
      properties: {
        typeId: {
          type: 'string',
        },
      },
    },
  })
  providerMeta?: {
    typeId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}
