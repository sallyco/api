import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    hiddenProperties: ['ownerId', 'tenantId', 'providerMeta'],
  },
})
export default abstract class GenericEntity extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    jsonSchema: {
      title: 'ID',
    },
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerId: string;

  @property({type: 'string'})
  tenantId: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt: Date;
}
