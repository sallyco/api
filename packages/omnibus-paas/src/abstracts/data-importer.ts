/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  MODEL_PROPERTIES_KEY,
  PropertyDefinition,
  repository,
  Model,
} from '@loopback/repository';
import {
  IMPORTABLE_DECORATOR_KEY,
  ImportableMetadata,
} from '../decorators/importable-model-decorator';
import {MetadataInspector} from '@loopback/context';
import neatCsv from 'neat-csv';
import {MinIOService} from '../services';
import {MinIOServiceBindings} from '../keys';
import {inject} from '@loopback/core';
import {
  Asset,
  Close,
  Company,
  Deal,
  Entities,
  File,
  Profile,
  Subscription,
} from '../models';
import {v4 as uuidv4} from 'uuid';
import * as fs from 'fs';
import {
  AssetRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  ProfileRepository,
  SubscriptionRepository,
} from '../repositories';
import {CompanyRepository} from '../repositories/company.repository';
import _ from 'lodash';
import * as XLSX from 'xlsx';
import {getJsonSchema} from '@loopback/repository-json-schema';

export enum IMPORTERS {
  GBT_LEGACY = 'GBT_LEGACY',
}

export type AllowedModels = Deal | Entities | Asset | Company | Profile;

interface FlatProperty {
  path: string;
  displayName?: string;
  description?: string;
}

export abstract class DataImporterService<Model> {
  public abstract type: IMPORTERS;
  public allowedModels: typeof Model[] = [
    Deal,
    Entities,
    Asset,
    Company,
    Profile,
    Subscription,
    Close,
    File,
  ];
  private allowedFields: string[];
  private allowedModelsRepositories: {
    [p: string]:
      | DealRepository
      | EntitiesRepository
      | AssetRepository
      | CompanyRepository
      | ProfileRepository
      | SubscriptionRepository
      | CloseRepository
      | FileRepository;
  };
  private minio: MinIOService;

  constructor(
    @repository(DealRepository) private dealRepository: DealRepository,
    @repository(EntitiesRepository)
    private entitiesRepository: EntitiesRepository,
    @repository(AssetRepository) private assetRepository: AssetRepository,
    @repository(CompanyRepository) private companyRepository: CompanyRepository,
    @repository(ProfileRepository) private profileRepository: ProfileRepository,
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
    @repository(CloseRepository) private closeRepository: CloseRepository,
    @repository(FileRepository) private fileRepository: FileRepository,
    @inject(MinIOServiceBindings.MINIO_SERVICE) minio?: MinIOService,
  ) {
    this.allowedModelsRepositories = {
      [Deal.modelName]: dealRepository,
      [Entities.modelName]: entitiesRepository,
      [Asset.modelName]: assetRepository,
      [Company.modelName]: companyRepository,
      [Profile.modelName]: profileRepository,
      [Subscription.modelName]: subscriptionRepository,
      [Close.modelName]: closeRepository,
      [File.modelName]: fileRepository,
    };
    if (minio) this.minio = minio;
  }

  abstract convertDataToModel(data: object[]): Partial<Model>[];

  public import(data: object[], model: typeof Model) {
    this.allowedFields = this.importableModelPropertiesList(model);
    return this.convertDataToModel(data);
  }

  public getFlatSchemas(models: typeof Model[] = this.allowedModels) {
    return models.map(model => this.getFlatSchema(model));
  }

  public getFlatSchema(
    model: typeof Model,
  ): (Partial<ImportableMetadata> & Partial<PropertyDefinition>)[] {
    const importableProperties = this.importableModelPropertiesData(model);
    const flattenedSchema: (Partial<ImportableMetadata> &
      Partial<PropertyDefinition>)[] = [];
    for (const property of importableProperties) {
      const propertyMeta = this.getModelPropertyMeta(property.name, model);
      const propertyData: Partial<ImportableMetadata> &
        Partial<PropertyDefinition> = {...propertyMeta, ...property};
      flattenedSchema.push(propertyData);
    }
    return flattenedSchema;
  }

  public convertFlatSchemaToCSVFormat(
    schemas: (Partial<ImportableMetadata> &
      Partial<PropertyDefinition> &
      Partial<{
        modelName: string;
        name: string;
      }>)[][],
  ): string {
    const fields: string[] = [];
    for (const schema of schemas) {
      for (const property of schema) {
        fields.push(...this.flattenProperty(property, property.modelName));
      }
    }
    return fields.join(',');
  }

  public convertFlatSchemaToXLSXFormat(
    schemas: (Partial<ImportableMetadata> &
      Partial<PropertyDefinition> &
      Partial<{
        modelName: string;
        name: string;
      }>)[][],
  ): Buffer {
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    for (const schema of schemas) {
      const fields: FlatProperty[] = [];
      for (const property of schema) {
        fields.push(
          ...this.flattenProperty(property, property.modelName).map(field => {
            return {
              path: field,
              description: property?.description,
            };
          }),
        );
      }
      const workSheet: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet([
        fields.map(field => field.path),
        fields.map(field => field.description),
      ]);
      XLSX.utils.book_append_sheet(workBook, workSheet, schema[0].modelName);
    }

    return XLSX.write(workBook, {
      type: 'buffer',
      bookType: 'xlsx',
      bookSST: false,
    });
  }

  async importFile(
    fileType: 'CSV' | 'XLSX',
    filePath: string,
    ownerId?: string,
    tenantId?: string,
  ) {
    let Data;
    if (fileType === 'CSV') {
      const stream = fs.createReadStream(filePath);
      Data = await neatCsv(stream);
    } else if (fileType === 'XLSX') {
      const XLSXData = XLSX.readFile(filePath);
      Data = [];
      XLSXData.SheetNames.forEach(Sheet => {
        Data.push(...XLSX.utils.sheet_to_json<any>(XLSXData.Sheets[Sheet]));
      });
    }

    for (const row of Data) {
      const modelObjects = {};
      let modelObject: Partial<AllowedModels>;
      for (const paramKey of Object.keys(row)) {
        const ModelObjectType = this.allowedModels.find(
          allowedModel => allowedModel.modelName === paramKey.split('.')[0],
        );
        if (!ModelObjectType) {
          throw new ImportError(
            `Model '${paramKey.split('.')[0]}' not found for column ${
              row[paramKey]
            }`,
          );
        }
        if (!Object.keys(modelObjects).includes(ModelObjectType.modelName)) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          modelObject = new ModelObjectType();
          if (
            ownerId &&
            modelObject.constructor.name !== 'Subscription' &&
            Object.keys(ModelObjectType.definition.properties).includes(
              'ownerId',
            )
          ) {
            modelObject.ownerId = ownerId;
          }
          if (
            tenantId &&
            Object.keys(ModelObjectType.definition.properties).includes(
              'tenantId',
            )
          ) {
            modelObject.tenantId = tenantId;
          }
        } else {
          modelObject = modelObjects[ModelObjectType.modelName];
        }

        if (row[paramKey] !== '') {
          let modelType = 'string';
          const modelKey = (() => {
            const key = paramKey.replace(/^[a-zA-z]+\./g, '');
            const filteredKey = key.replace(/\[[0-9]+]$/g, '');
            if (filteredKey !== key) {
              modelType = 'array';
            }
            return filteredKey;
          })();

          if (modelType === 'array') {
            if (!Array.isArray(_.get(modelObject, modelKey))) {
              _.set(modelObject, modelKey, []);
            }
            _.update(modelObject, modelKey, value => {
              return [...value, ...row[paramKey].split(',')];
            });
          } else {
            _.set(modelObject, modelKey, row[paramKey]);
          }
          if (modelObject.constructor.name === 'File' && modelKey === 'key') {
            const fileUUID = uuidv4();
            const filetype = await this.minio.downloadAndPutObject(
              modelObject['key'],
              fileUUID,
              tenantId ?? 'gbt',
            );
            _.set(modelObject, modelKey, fileUUID);
            _.set(modelObject, 'type', filetype);
          }
        }
        modelObjects[ModelObjectType.name] = modelObject;
      }

      for (const model of Object.keys(modelObjects)) {
        try {
          await this.allowedModelsRepositories[model].create(
            modelObjects[model],
          );
        } catch (e) {
          console.log(e);
        }
      }
    }
    return true;
  }

  private importableModelPropertiesList(model: typeof Model): string[] {
    const allProps = this.importableModelProperties(model);
    return Object.keys(allProps ?? {});
  }

  private importableModelPropertiesData(
    model: typeof Model,
  ): (ImportableMetadata & {name: string; modelName: string})[] {
    const allProps = this.importableModelProperties(model) ?? {};
    const fieldList = Object.keys(allProps ?? {});
    return fieldList.map(key => {
      return {
        name: key,
        modelName: model.name,
        ...allProps[key],
      };
    });
  }

  private importableModelProperties(
    model: typeof Model,
  ): ImportableMetadata | undefined {
    return MetadataInspector.getAllPropertyMetadata<ImportableMetadata>(
      IMPORTABLE_DECORATOR_KEY,
      model.prototype,
    );
  }

  private getModelPropertyMeta(
    key: string,
    model: typeof Model,
  ): PropertyDefinition | undefined {
    return MetadataInspector.getPropertyMetadata<PropertyDefinition>(
      MODEL_PROPERTIES_KEY,
      model.prototype,
      key,
    );
  }

  public flattenProperty(
    property: Partial<ImportableMetadata> & Partial<PropertyDefinition>,
    parentPath = '',
  ): string[] {
    const properties: string[] = [];
    let mergedKeys;
    let itemSchema;

    switch (property.type) {
      case 'array':
      case Array:
        // Showing a single (unfurled if needed) element of the array as an example
        parentPath = [parentPath, property?.name + '[0]'].join('.');
        if (
          property.itemType === 'string' ||
          property.itemType === 'number' ||
          property?.items?.type === 'string' ||
          property?.items?.type === 'number'
        ) {
          properties.push(parentPath);
          break;
        }
        if (
          typeof property.itemType === 'function' ||
          typeof property?.items?.type === 'function'
        ) {
          itemSchema = getJsonSchema(
            property.itemType ?? property?.items?.type,
          );
        }
        properties.push(
          ...this.flattenProperty(
            {
              type: itemSchema?.type ?? property?.itemType ?? 'object',
              jsonSchema: property.jsonSchema ?? itemSchema,
            },
            parentPath,
          ),
        );
        break;
      case 'object':
      case Object:
        mergedKeys = {
          ...property?.jsonSchema?.patternProperties,
          ...property?.jsonSchema?.properties,
          ...property?.properties,
          ...property?.patternProperties,
        };
        if (property?.name) {
          parentPath = [parentPath, property?.name].join('.');
        }
        for (const propertyKey of Object.keys(mergedKeys)) {
          const flattenedProperties = this.flattenProperty(
            {...mergedKeys[propertyKey], ...{name: propertyKey}},
            parentPath,
          );
          properties.push(...flattenedProperties);
        }
        break;
      default:
        properties.push([parentPath, property?.name].join('.'));
        break;
    }
    return properties;
  }
}

class ImportError {
  constructor(message: string) {
    const error = Error(message);
    // set immutable object properties
    Object.defineProperty(error, 'message', {
      get() {
        return message;
      },
    });
    Object.defineProperty(error, 'name', {
      get() {
        return 'ImportError';
      },
    });
    Error.captureStackTrace(error, ImportError);
    return error;
  }
}
