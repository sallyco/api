<?xml version="1.0" encoding="UTF-8"?>
    <!-- Remove unwanted attributes or/and nodes -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <!-- Copy everything -->
    <xsl:template match="@*|node()|text()|comment()|processing-instruction()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="w:rsid"/>
    <xsl:template match="@w:rsidDel"/>
    <xsl:template match="@w:rsidP"/>
    <xsl:template match="@w:rsidR"/>
    <xsl:template match="@w:rsidRDefault"/>
    <xsl:template match="@w:rsidRPr"/>
    <xsl:template match="@w:rsidSect"/>
    <xsl:template match="@w:rsidTr"/>
    <xsl:template match="w:proofErr"/>
    <xsl:template match="w:lang"/>


</xsl:stylesheet>
