import React from 'react';
import {Page, Text, View, Document, StyleSheet} from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import {PDFHelpers} from '../../../utils/documentUtils/PDFHelpers';
import {Readable} from 'stream';
import moment from 'moment';

const styles = StyleSheet.create({
  body: {
    fontFamily: 'Times-Roman',
    padding: 75,
    fontSize: 11,
  },
  title: {
    fontSize: 13,
    fontFamily: 'Times-Bold',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 12,
    textTransform: 'uppercase',
    textAlign: 'center',
    marginBottom: 10,
  },
  columnHeader: {
    fontSize: 12,
    borderTop: '2px solid #000',
    borderBottom: '1px double #000',
    padding: 5,
  },
  section: {
    marginBottom: 24,
  },
  sectionHeader: {
    padding: '5 5 5 10',
    fontSize: 12,
    borderBottom: '1px solid #555',
  },
  entry: {
    padding: '5 5 5 15',
    borderBottom: '1px solid #999',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  left: {
    //marginLeft: 'auto',
    alignSelf: 'stretch',
    textAlign: 'left',
    //flexBasis: 'auto',
  },

  proceedsRight: {
    // width: '66%', //<- working alternative
    //flexGrow: 1,
    textAlign: 'right',
    marginLeft: 'auto',
    flexBasis: 70,
  },
  expensesRight: {
    // width: '66%', //<- working alternative
    //flexGrow: 1,
    textAlign: 'right',
    marginLeft: 15,
    flexBasis: 70,
  },
});

interface StatementProps {
  // TODO: Fix this any type
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
}

const Statement: React.FC<StatementProps> = ({data}) => (
  <Document>
    <Page size="LETTER" style={styles.body}>
      <View>
        <Text style={[styles.title]}>{data.entity.name}</Text>
        <Text style={[styles.subtitle]}>
          SPV Closing Statement - {moment(data.createdAt).format('MM/DD/YYYY')}
        </Text>
      </View>
      <View style={[styles.row, styles.columnHeader]}>
        <Text style={[styles.left]}>Description</Text>
        <Text style={[styles.proceedsRight]}>Proceeds</Text>
        <Text style={[styles.expensesRight]}>Expenses</Text>
      </View>
      {data.statement.sections.map((sec, i) => (
        <View style={[styles.section]} key={`section-${i}`}>
          <View style={[styles.row, styles.sectionHeader]}>
            <Text style={[styles.left]}>{sec.title}</Text>
          </View>
          {sec.entries.map((ent, j) => (
            <View style={[styles.row, styles.entry]} key={`entry-${i}-${j}`}>
              <Text style={[styles.left]}>{ent.description}</Text>
              <Text style={[styles.proceedsRight]}>{ent.proceeds ?? ' '}</Text>
              <Text style={[styles.expensesRight]}>{ent.expenses ?? ' '}</Text>
            </View>
          ))}
        </View>
      ))}
    </Page>
  </Document>
);
// TODO: Fix this any type
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const StatementBuilder = async (data: any): Promise<Buffer> => {
  const documentStream = await ReactPDF.renderToStream(
    <Statement data={data} />,
  );
  return PDFHelpers.convertStreamToBuffer(new Readable().wrap(documentStream));
};

export default StatementBuilder;
