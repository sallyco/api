import React from 'react';
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Image,
} from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import {PDFHelpers} from '../../../utils/documentUtils/PDFHelpers';
import {Readable} from 'stream';
import moment from 'moment';
import numeral from 'numeral';

const styles = StyleSheet.create({
  body: {
    padding: 75,
    fontSize: 13,
  },
  title: {
    fontSize: 17,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 10,
  },
  section: {
    paddingTop: 4,
    marginBottom: 4,
    borderTop: '2px solid #555',
  },
  subsection: {
    paddingTop: 2,
    marginBottom: 2,
    borderTop: '1px solid #555',
  },
  subsectionstart: {
    marginBottom: 2,
  },
  sectionHeader: {
    padding: '5 5 5 10',
    fontSize: 12,
    borderBottom: '1px solid #555',
  },
  smallText: {
    fontSize: 8,
  },
  entry: {
    padding: '5 5 5 15',
    borderBottom: '1px solid #999',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  left: {
    //marginLeft: 'auto',
    textAlign: 'left',
    //flexBasis: 'auto',
  },
  right: {
    flexGrow: 1,
    textAlign: 'right',
    marginLeft: 'auto',
  },
  company: {
    paddingTop: 14,
    borderTop: '2px solid #555',
    display: 'flex',
    flexDirection: 'row',
  },
  logo: {
    maxWidth: '40%',
    textAlign: 'left',
  },
  companydetails: {
    flexGrow: 1,
    textAlign: 'right',
    marginLeft: 'auto',
  },
  footer: {
    position: 'absolute',
    bottom: 32,
    left: 0,
    right: 0,
    paddingRight: 40,
    paddingLeft: 40,
  },
});

interface StatementProps {
  // TODO: Fix this any type
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
}

const Statement: React.FC<StatementProps> = ({data}) => (
  <Document>
    <Page size="LETTER" style={styles.body}>
      <View>
        <Text style={[styles.title]}>
          {data.subscription?.deal?.entity?.name}
        </Text>
        <Text style={[styles.subtitle]}>Fund Capital Account Statement</Text>
      </View>
      <View style={[styles.section]}>
        <Text>
          {data.subscription?.profile?.name
            ? data.subscription.profile.name
            : `${data.subscription?.profile?.firstName} ${data.subscription?.profile?.lastName}`}
        </Text>
        <View style={[styles.row]}>
          <Text style={[styles.left]}>
            {data.subscription?.profile?.address?.address1}
          </Text>
          <Text style={[styles.right]}>{data.subscription?.email}</Text>
        </View>
        <View style={[styles.row]}>
          <Text style={[styles.left]}>
            {data.subscription?.profile?.address?.city},{' '}
            {data.subscription?.profile?.address?.state}{' '}
            {data.subscription?.profile?.address?.postalCode}
          </Text>
          <Text style={[styles.right]}>{data.subscription?.phone}</Text>
        </View>
        <Text>{data.subscription?.profile?.address?.country}</Text>
      </View>
      <View style={[styles.section]}>
        <View style={[styles.row, styles.subsectionstart]}>
          <Text style={[styles.left]}>Effective Date</Text>
          <Text style={[styles.right]}>
            {moment(data.subscription.createdAt).format('YYYY-MM-DD')}
          </Text>
        </View>
        <View style={[styles.row, styles.subsection]}>
          <Text style={[styles.left]}>Subscription Amount</Text>
          <Text style={[styles.right]}>
            {numeral(data.subscription.amount).format('$0,0')}
          </Text>
        </View>
        <View style={[styles.row, styles.subsection]}>
          <Text style={[styles.left]}>Ownership Percentage*</Text>
          <Text style={[styles.right]}>
            {data.subscription?.ownershipPercentageAtClose
              ? numeral(data.subscription.ownershipPercentageAtClose).format(
                  '0.0000%',
                )
              : `${numeral(
                  data.subscription.amount /
                    data.subscription.deal.targetRaiseAmount,
                ).format('0.0000%')} (estimate)`}
          </Text>
        </View>
        <Text style={[styles.smallText]}>
          *As of the effective date. Changes to the fund (including additional
          investments) may affect your ownership percentage. This percentage
          does not take into account any carry percentage (if applicable).
        </Text>
      </View>
      <View style={[styles.company]}>
        <Image src={data.tenant?.assets?.images?.logo} style={[styles.logo]} />
        <View style={[styles.companydetails]}>
          <Text>{data.tenant?.manager?.name}</Text>
          <Text>{data.tenant?.manager?.address?.address1}</Text>
          <Text>
            {data.tenant?.manager?.address?.city ?? ' '},{' '}
            {data.tenant?.manager?.address?.state ?? ' '}{' '}
            {data.tenant?.manager?.address?.postalCode ?? ' '}
          </Text>
        </View>
      </View>
      <Text style={[styles.footer, styles.smallText]}>
        IMPORTANT NOTE: This statement is an official record of your account for
        your interest in {data.subscription?.deal?.entity?.name}. This statement
        will be considered accurate and complete unless you contact{' '}
        {data.tenant?.manager?.name ?? ' '} at the provided email address within
        30 calendar days of the effective date stated above.
      </Text>
    </Page>
  </Document>
);
// TODO: Fix this any type
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const StatementBuilder = async (data: any): Promise<Buffer> => {
  const documentStream = await ReactPDF.renderToStream(
    <Statement data={data} />,
  );
  return PDFHelpers.convertStreamToBuffer(new Readable().wrap(documentStream));
};

export default StatementBuilder;
