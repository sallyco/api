import React from 'react';
import {Page, Text, View, Document, StyleSheet, Image} from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import {PDFHelpers} from '../../../utils/documentUtils/PDFHelpers';
import {Readable} from 'stream';


const styles = StyleSheet.create({
  body: {
    fontFamily: 'Times-Roman',
    paddingBottom: 100,
    paddingTop: 100,
  },
  section: {
    marginTop: 20,
    marginHorizontal: 70,
    fontSize: 11,
  },
  title: {
    fontSize: 13,
    fontFamily: 'Times-Bold',
    textTransform: 'uppercase',
    textAlign: 'center',
    lineHeight: 2.3,
  },
  header: {
    position: 'absolute',
    fontSize: 12,
    top: 20,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Times-Italic',
  },
  footer: {
    position: 'absolute',
    fontSize: 10,
    bottom: 40,
    left: 50,
    right: 50,
    paddingTop: 2,
    textAlign: 'left',
    color: 'black',
    borderTopColor: 'black',
    borderTopWidth: 0.5,
  },
  dataSection: {
    maxWidth: '250',
  },
  dataSectionRows: {
    display: 'flex',
    flexDirection: 'column',
  },
  dataSectionRow: {
    display: 'flex',
    flexDirection: 'row',
    paddingBottom: 18,
  },
  dataSectionLabels: {
    paddingRight: 5,
    flexShrink: 1,
  },
  dataSectionValues: {
    flexGrow: 1,
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
  },
  signatories: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginRight: 40,
  },
  signatory: {
    width: '50%',
    paddingRight: 20,
    paddingBottom: 20,
  },
  signatoryTitle: {
    fontFamily: 'Times-Bold',
  },
  signatorySignatureLabel: {
    fontFamily: 'Times-Italic',
    borderTopWidth: 0.5,
    borderTopColor: 'black',
    paddingTop: 3,
    marginBottom: 10,
  },
  signatorySignatureImageContainer: {
    minHeight: 50,
  },
  signatorySignatureImage: {
    maxHeight: 50,
    maxWidth: '100%',
  },
  signatoryPrintName: {
    borderTopColor: 'black',
    paddingTop: 3,
  },
  signatoryPrintNameLabel: {
    fontFamily: 'Times-Italic',
    borderTopWidth: 0.5,
    borderTopColor: 'black',
    paddingTop: 3,
  },
  expenseEstimateDisclaimer: {
    color: 'red',
    position: 'absolute',
    left: 250,
    width: 200,
    marginTop: -20,
  },
});

interface DocumentSignatory {
  printName?: string;
  signature?: string;
}

interface InvestorSAProps {
  isEstimate: boolean,
  entityName: string,
  entityStateOfFormation: string,
  date: string,
  principalAmount: string,
  expenseContribution: string,
  totalSubscriptionAmount: string,
  signatories: DocumentSignatory[]
}

const InvestorSA: React.FC<InvestorSAProps> = (
  {
    isEstimate,
    entityName,
    entityStateOfFormation,
    date,
    principalAmount,
    expenseContribution,
    totalSubscriptionAmount,
    signatories,
  },
) => (
  <Document>
    <Page size='LETTER' style={styles.body}>
      <View style={styles.header} fixed>
        <Text>Proprietary and Confidential</Text>
      </View>
      <View style={[styles.title]}>
        <Text>{entityName}, A</Text>
        <Text>{entityStateOfFormation} LIMITED LIABILITY COMPANY</Text>
        <Text>Signature Page to Subscription Agreement</Text>
        <Text>Individuals</Text>
      </View>
      <View style={styles.section}>
        <Text>
          {'\u00A0'.repeat(15)}IN WITNESS WHEREOF, the Subscriber hereby executes this Agreement as of the date set
          forth
          below.
        </Text>
      </View>
      <View style={[styles.section, styles.dataSection]}>
        <View style={[styles.dataSectionRows]}>
          <View style={[styles.dataSectionRow]}>
            <Text style={[styles.dataSectionLabels]}>Date:</Text>
            <Text style={[styles.dataSectionValues]}>{date}</Text>
          </View>
          <View style={[styles.dataSectionRow]}>
            <Text style={[styles.dataSectionLabels]}>Principle Amount:</Text>
            <Text style={[styles.dataSectionValues]}>${principalAmount}</Text>
          </View>
          <View style={[styles.dataSectionRow]}>
            <Text style={[styles.dataSectionLabels]}>Expense Contribution:</Text>
            <Text style={[styles.dataSectionValues]}>${expenseContribution}</Text>
            {isEstimate && <Text style={[styles.expenseEstimateDisclaimer]}>This amount is an estimation based on total Subscribers’
              interest. It is subject to change, and will be finalized at the time of Closing.</Text>}
          </View>
          <View style={[styles.dataSectionRow]}>
            <Text style={[styles.dataSectionLabels]}>Total Subscription Amount:</Text>
            <Text style={[styles.dataSectionValues]}>${totalSubscriptionAmount}</Text>
          </View>
        </View>
      </View>
      <View style={[styles.section, styles.signatories]}>
        {
          signatories.map((signatory, index) => (
            <View style={[styles.signatory]} key={`signatory-${index}`}>
              <Text style={styles.signatoryTitle}>
                Subscriber #{index + 1}:
              </Text>
              <View style={styles.signatorySignatureImageContainer}>
                <Image
                  style={styles.signatorySignatureImage}
                  src={signatory.signature ?? `${__dirname}/../../../utils/signingUtils/signhere.png`}
                />
              </View>
              <View style={styles.signatorySignatureLabel}>
                <Text>(Signature)</Text>
              </View>
              <View style={styles.signatoryPrintName}>
                <Text>{signatory.printName}</Text>
              </View>
              <View style={styles.signatoryPrintNameLabel}>
                <Text>(Print Name)</Text>
              </View>
            </View>
          ))
        }
      </View>
      <View style={styles.footer} fixed>
        <Text>Subscription Agreement</Text>
      </View>
    </Page>
  </Document>
);

const InvestorSABuilder = async (
  isEstimate: boolean,
  entityName: string,
  entityStateOfFormation: string,
  date: string,
  principalAmount: string,
  expenseContribution: string,
  totalSubscriptionAmount: string,
  signatories: DocumentSignatory[],
): Promise<Buffer> => {
  const documentStream = await ReactPDF.renderToStream(
    <InvestorSA
      isEstimate={isEstimate}
      entityName={entityName}
      entityStateOfFormation={entityStateOfFormation}
      date={date}
      principalAmount={principalAmount}
      expenseContribution={expenseContribution}
      totalSubscriptionAmount={totalSubscriptionAmount}
      signatories={signatories}
    />,
  );
  return PDFHelpers.convertStreamToBuffer(new Readable().wrap(documentStream));
};


export default InvestorSABuilder;
