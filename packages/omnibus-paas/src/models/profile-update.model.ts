import {belongsTo, Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Company} from './company.model';

@model({
  name: 'Update Profile',
})
export class ProfileUpdate extends Entity {
  constructor(data?: Partial<ProfileUpdate>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Owner ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Owner ID',
  })
  ownerId: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Tenant ID',
  })
  tenantId?: string;

  /**
   * The type of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['ORGANIZER', 'INVESTOR'],
    },
  })
  @importable({
    displayName: 'Profile Type',
    description: 'Type of Profile',
  })
  profileType?: 'ORGANIZER' | 'INVESTOR';

  /**
   * The state of formation of the profile
   */
  @property({type: 'string'})
  @importable({
    displayName: 'State of Formation',
    description: 'State the Profile was Formed In (If Entity)',
  })
  stateOfFormation?: string;

  /**
   * The country of formation of the profile
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Country of Formation',
    description: 'Country the Profile was Formed In (If Entity)',
  })
  countryOfFormation?: string;

  /**
   * The display name of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Display Name',
    description: 'The Display Name of the Profile',
  })
  displayName?: string;

  /**
   * The legal name of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'The Name of the Profile',
  })
  name?: string;

  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Joint Account Name',
  })
  jointAccountName?: string;

  /**
   * The type of the profile if entity
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Type of Entity',
    description: 'The Type of Entity',
  })
  typeOfEntity?:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION'
    | 'S_CORPORATION'
    | 'GENERAL_PARTNERSHIP'
    | '501_C_NONPROFIT'
    | 'FOREIGN_ENTITY';

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Type of Joint',
    description: 'The Type of Joint Profile',
  })
  jointType?:
    | 'JOINT_TENANTS_WITH_RIGHTS_OF_SURVIVORSHIP'
    | 'TENANTS_IN_COMMON'
    | 'COMMUNITY_PROPERTY'
    | 'TENANTS_BY_ENTIRETY';

  /**
   * The first name of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'First Name',
    description: 'The First Name of the Profile Owner',
  })
  firstName?: string;

  /**
   * The last name of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Last Name',
    description: 'The Last Name of the Profile Owner',
  })
  lastName?: string;

  /**
   * The phone number of the profile owner
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Phone',
    description: 'The Phone Number of the Profile Owner',
  })
  phone?: string;

  /**
   * The date of birth of the profile owner
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Date of Birth',
    description: 'The Date of Birth for the Profile Owner',
  })
  dateOfBirth?: string;

  /**
   * The date of formation of the profile (if business)
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Date of Formation',
    description: 'The Date of Formation of the Profile (if Business)',
  })
  dateOfFormation?: string;

  /**
   * The email of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^\S+@\S+$/.source,
    },
  })
  @importable({
    displayName: 'Email',
    description: 'The Email of the Profile Owner',
  })
  email?: string;

  /**
   * The passportId number of the profile owner
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Passport',
    description: 'The Passport Number of the Profile Owner',
  })
  passportId?: string;

  /**
   * The passportId number of the profile owner
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Title',
    description: 'The Title of the Profile Owner',
  })
  title?: string;

  /**
   * Whether the profile owner is US based or not
   */
  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is US Based',
    description: 'Whether the profile owner is US based or not',
  })
  isUSBased?: boolean;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is RIA',
  })
  isRIA?: boolean;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is ERA',
  })
  isERA?: boolean;

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'ERA AccreditationStatus',
  })
  ERAStatus?: string;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is Single Member LLC',
    description: 'Whether the profile is a Single Member LLC',
  })
  isSingleMemberLLC?: boolean;

  @belongsTo(() => Company)
  managerId?: string;

  @belongsTo(() => Company)
  masterEntityId?: string;

  /**
   * The date a profile was deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;
}
