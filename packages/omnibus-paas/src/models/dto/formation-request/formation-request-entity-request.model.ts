import {Model, model, property} from '@loopback/repository';
import {getJsonSchema} from '@loopback/repository-json-schema';
import {FormationRequestTaxContact} from './formation-request-tax-contact.model';

@model()
export class FormationRequestEntityRequest extends Model {
  @property({
    type: 'object',
    required: true,
    jsonSchema: getJsonSchema(FormationRequestTaxContact),
  })
  taxContact: FormationRequestTaxContact;

  @property({
    type: 'object',
    jsonSchema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        type: {
          type: 'string',
          enum: ['LLC', 'LP'],
        },
        stateOfFormation: {
          type: 'string',
        },
        address: {
          required: ['street1', 'city', 'postalCode', 'state', 'country'],
          properties: {
            street1: {
              type: 'string',
              minLength: 2,
              maxLength: 1024,
            },
            street2: {type: 'string', maxLength: 1024},
            city: {type: 'string', minLength: 2, maxLength: 1024},
            state: {type: 'string', minLength: 2, maxLength: 1024},
            postalCode: {type: 'string', minLength: 2, maxLength: 1024},
            country: {
              type: 'string',
              minLength: 2,
              maxLength: 1024,
              enum: ['United States of America'],
            },
          },
        },
        isSeries: {
          type: 'boolean',
        },
      },
      if: {
        properties: {
          isSeries: {
            const: true,
          },
        },
      },
      then: {
        required: ['masterEntity'],
        properties: {
          masterEntity: {
            required: ['name'],
            properties: {
              name: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
              },
            },
          },
        },
      },
    },
  })
  entity: {
    name: string;
    type: string;
    stateOfFormation: string;
    address?: {
      street1: string;
      street2?: string;
      city: string;
      state: string;
      postalCode: string;
      country: string;
    };
    isSeries?: boolean;
    masterEntity?: {
      name: string;
    };
  };

  constructor(data?: Partial<FormationRequestEntityRequest>) {
    super(data);
  }
}

export interface FormationRequestEntityRequestRelations {
  // describe navigational properties here
}

export type FormationRequestEntityRequestWithRelations =
  FormationRequestEntityRequest & FormationRequestEntityRequestRelations;
