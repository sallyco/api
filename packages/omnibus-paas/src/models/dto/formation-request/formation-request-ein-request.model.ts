import {Model, model, property} from '@loopback/repository';
import {getJsonSchema} from '@loopback/repository-json-schema';
import {FormationRequestTaxContact} from './formation-request-tax-contact.model';

@model()
export class FormationRequestEINRequest extends Model {
  @property({
    type: 'object',
    required: true,
    jsonSchema: getJsonSchema(FormationRequestTaxContact),
  })
  taxContact: FormationRequestTaxContact;

  @property({
    type: 'object',
    required: true,
    jsonSchema: {
      type: 'object',
      required: ['name'],
      properties: {
        name: {
          type: 'string',
          maxLength: 256,
        },
      },
    },
  })
  entity: {
    name: string;
  };

  constructor(data?: Partial<FormationRequestEINRequest>) {
    super(data);
  }
}

export interface FormationRequestRequestRelations {
  // describe navigational properties here
}

export type FormationRequestRequestWithRelations = FormationRequestEINRequest &
  FormationRequestRequestRelations;
