import {model, property} from '@loopback/repository';

@model({
  name: 'FormationRequestTaxContact',
  settings: {
    hiddenProperties: ['taxDetails'],
  },
})
export class FormationRequestTaxContact {
  @property({
    type: 'object',
    required: true,
    hidden: true,
    jsonSchema: {
      required: ['type', 'value'],
      properties: {
        type: {
          type: 'string',
          enum: ['ssn'],
        },
        value: {
          type: 'string',
          maxLength: 11,
        },
      },
    },
  })
  taxDetails: {
    type: 'ssn';
    value: string;
  };

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  phone: string;

  @property({
    type: 'object',
    required: true,
    jsonSchema: {
      required: ['street1', 'city', 'postalCode', 'state', 'country'],
      properties: {
        street1: {
          type: 'string',
          minLength: 2,
          maxLength: 1024,
          description: 'Street Name Line 1',
        },
        street2: {
          type: 'string',
          maxLength: 1024,
          description: 'Street Name Line 1',
        },
        city: {type: 'string', minLength: 2, maxLength: 1024},
        state: {type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {type: 'string', minLength: 2, maxLength: 1024},
        country: {
          type: 'string',
          minLength: 2,
          maxLength: 1024,
          enum: ['United States of America'],
        },
      },
    },
  })
  address: {
    street1: string;
    street2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };
}
