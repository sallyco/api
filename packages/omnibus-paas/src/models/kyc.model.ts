import {Entity, model, property} from '@loopback/repository';
import {Check} from 'services/kycaml/kycaml.service.types';

const AllCheckResults = <const>['CLEAR', 'CONSIDER'];
export type CheckResult = typeof AllCheckResults[number];
const AllCheckStatuses = <const>['IN_PROGRESS', 'COMPLETE', 'EXCEPTION'];
export type CheckStatus = typeof AllCheckStatuses[number];

@model({
  name: 'KYC',
})
export class Kyc extends Entity implements Check {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date | string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      type: 'string',
      enum: [...AllCheckStatuses],
    },
  })
  status: CheckStatus;

  @property({
    type: 'object',
    required: false,
    jsonSchema: {
      type: 'object',
      properties: {
        adverseMedia: {
          type: 'string',
          enum: [...AllCheckStatuses],
        },
      },
    },
  })
  resultDetail?: {
    adverseMedia?: CheckResult;
    monitoredLists?: CheckResult;
    politicallyExposedPerson?: CheckResult;
    sanction?: CheckResult;
  };

  @property({
    type: 'object',
    required: true,
  })
  providerMeta?: {
    typeId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };

  @property({
    type: 'string',
  })
  applicantId: string;

  @property({
    type: 'string',
  })
  checkId?: string;

  @property({
    type: 'string',
  })
  reason?: string;

  @property({
    type: 'string',
  })
  result?: CheckResult;

  @property({
    type: 'string',
  })
  profileId?: string;

  @property({
    type: 'string',
  })
  ownerId?: string;

  constructor(data?: Partial<Kyc>) {
    super(data);
  }
}

export interface KycRelations {
  // describe navigational properties here
}

export type KycWithRelations = Kyc & KycRelations;
