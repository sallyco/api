import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';

@model({
  name: 'Update Asset',
})
export class AssetUpdate extends Entity {
  constructor(data?: Partial<AssetUpdate>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }
  /**
   * Owner ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Owner ID',
  })
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Tenant ID',
  })
  tenantId: string;

  /**
   * Deal ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Deal ID',
  })
  dealId?: string;

  /**
   * The type of the asset
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['COMPANY', 'REAL_ESTATE', 'CRYPTOCURRENCY', 'ARTWORK'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of Asset',
  })
  type?: 'COMPANY' | 'REAL_ESTATE' | 'CRYPTOCURRENCY' | 'ARTWORK';

  /**
   * The subType of the asset
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'SubType',
    description: 'The SubType of Asset',
  })
  subType?: string;

  /**
   * The Unit Count of the asset
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Unit Count',
  })
  unitCount?: number;

  /**
   * The Allocation Amount of the asset
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Allocation Amount',
  })
  allocationAmount?: number;

  /* asset name */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Name',
    description: 'Name of the Asset',
  })
  name: string;

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Sector',
    description: 'Sector of the Asset',
  })
  sector: string;

  /**
   * The market sector of the company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: [
        'COMMUNICATION_SERVICES',
        'CONSUMER_DISCRETIONARY',
        'CONSUMER_STAPLES',
        'ENERGY',
        'FINANCIALS',
        'HEALTHCARE',
        'INDUSTRIALS',
        'INFORMATION_TECHNOLOGY',
        'MATERIALS',
        'REAL_ESTATE',
        'UTILITIES',
      ],
    },
  })
  @importable({
    displayName: 'Company Market Sector',
    description: 'The Market Sector for the Asset',
  })
  public companyMarketSector?:
    | 'COMMUNICATION_SERVICES'
    | 'CONSUMER_DISCRETIONARY'
    | 'CONSUMER_STAPLES'
    | 'ENERGY'
    | 'FINANCIALS'
    | 'HEALTHCARE'
    | 'INDUSTRIALS'
    | 'INFORMATION_TECHNOLOGY'
    | 'MATERIALS'
    | 'REAL_ESTATE'
    | 'UTILITIES';

  /* description */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Description',
    description: 'The description of the Asset',
  })
  description?: string;

  /* size */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Size',
    description: 'The Size of the Asset',
  })
  size?: string;

  /* category */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Category',
    description: 'The Category for the Asset',
  })
  category?: string;

  /* description */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Advisors',
    description: 'The Advisors on the Asset',
  })
  advisors?: string;

  /**
   * Website for the asset being purchased
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern:
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/
          .source,
    },
  })
  @importable({
    displayName: 'URL',
    description: 'The URL for the Asset',
  })
  assetUrl?: string;

  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Appraised Value',
    description: 'The Appraised Value of the Asset',
  })
  appraisedValue?: number;

  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Fractional Ownership Amount',
    description: 'The amount of Fractional Ownership for the Asset',
  })
  fractionalOwnershipAmount?: number;

  /**
   * pitchDoc
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'PITCH Document',
    description: 'PITCH Document for the Asset',
  })
  pitchDoc?: string;

  /**
   * videoURL
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Video URL',
    description: 'Video URL for the Asset',
  })
  videoURL?: string;

  /**
   * isPublic
   */
  @property({
    type: 'boolean',
    default: false,
  })
  @importable({
    displayName: 'Asset is Public',
    description: 'Asset is Public',
  })
  isPublic: boolean;

  /**
   * Whether or not the asset object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  @importable({
    displayName: 'Asset is Deleted',
    description: 'Asset is Deleted',
  })
  isDeleted: boolean;
}
