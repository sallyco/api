import {Entity, model, property, belongsTo} from '@loopback/repository';
import {BlueSkyFee} from './blue-sky-fee.model';

@model({
  title: 'BlueSkyFeeTier',
  description: 'EFD ‐ Form D Fee Schedule',
  settings: {
    postgresql: {schema: 'accounting', table: 'blue_sky_fee_tier'},
    foreignKeys: {
      fk_blueskyfeetier_blueskyfeeId: {
        name: 'fk_blueskyfeetier_blueskyfeeId',
        entity: 'BlueSkyFee',
        entityKey: 'id',
        foreignKey: 'blueskyfeeid',
        onDelete: 'CASCADE',
        onUpdate: 'SET NULL',
      },
    },
  },
})
export class BlueSkyFeeTier extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'fee',
      dataType: 'integer',
      dataScale: 0,
      nullable: 'NO',
    },
  })
  fee: number;

  @property({
    type: 'number',
    required: true,
    postgresql: {
      columnName: 'breakpoint',
      dataType: 'integer',
      dataScale: 0,
      nullable: 'NO',
    },
  })
  breakpoint: number;

  @belongsTo(() => BlueSkyFee)
  blueSkyFeeId: number;

  constructor(data?: Partial<BlueSkyFeeTier>) {
    super(data);
  }
}

export interface BlueSkyFeeTierRelations {}

export type BlueSkyFeeTierWithRelations = BlueSkyFeeTier &
  BlueSkyFeeTierRelations;
