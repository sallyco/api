import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TransactionACHStatus} from '../services';
import {Deal} from './deal.model';
import {Entities} from './entities.model';
import {Subscription} from './subscription.model';
import {Close} from './close.model';

export enum TransactionStatus {
  'WAITING' = 'WAITING',
  'QUEUED' = 'QUEUED',
  'SENT' = 'SENT',
  'PENDING' = 'PENDING',
  'PROCESSING' = 'PROCESSING',
  'CANCELED' = 'CANCELED',
  'RETURNED' = 'RETURNED',
  'TRANSACTION_EXCEPTION' = 'TRANSACTION EXCEPTION',

  'DEPRECATED_TRANSACTION_FAILURE' = 'TRANSACTION FAILURE',
  'DEPRECATED_WAITING_ON_ENTITY' = 'WAITING ON ENTITY',
}

@model()
export class Transaction extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  @property({
    type: 'string',
  })
  transactionId?: string;

  @property({type: 'string'})
  bankAccountTransactionId?: string;

  @property({type: 'string'})
  refundBankAccountTransactionId?: string;

  @property({
    type: 'object',
  })
  providerMeta?: object;

  //TODO: Rename or Remove this property
  @property({
    type: 'string',
  })
  accountID: string;

  @property({
    type: 'string',
  })
  bankAccountId: string;

  @property({
    type: 'number',
    required: true,
  })
  amount: number;

  //TODO this may not always be cp_ it is acct_ with books
  @property({
    type: 'string',
  })
  counterpartyId: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'date',
  })
  addedToNetwork?: Date;

  @property({
    type: 'boolean',
  })
  needsApproval?: boolean;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      type: 'string',
      enum: ['CREDIT', 'DEBIT'],
    },
  })
  direction: 'CREDIT' | 'DEBIT';

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      type: 'string',
      enum: ['ACH', 'WIRE', 'BOOK'],
    },
  })
  type: 'ACH' | 'WIRE' | 'BOOK';

  @property({
    type: 'string',
    required: true,
  })
  status: TransactionStatus | TransactionACHStatus | string;

  @property({
    type: 'string',
  })
  reason?: string;

  @property({
    type: 'object',
  })
  reasonDetail?: object;

  @belongsTo(() => Deal)
  dealId?: string;

  @belongsTo(() => Subscription)
  subscriptionId?: string;

  @belongsTo(() => Entities)
  entitiesId?: string;

  @property({
    type: 'string',
  })
  instructions?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @belongsTo(() => Close)
  closeId?: string;

  @property({
    type: 'string',
  })
  updateEmail?: string;

  constructor(data?: Partial<Transaction>) {
    super(data);
  }
}

export interface TransactionRelations {
  // describe navigational properties here
}

export type TransactionWithRelations = Transaction & TransactionRelations;
