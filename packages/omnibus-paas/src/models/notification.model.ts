import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Notification extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  tenantId?: string;

  @property({
    type: 'string',
  })
  userId?: string;

  @property({
    type: 'object',
    jsonSchema: {
      required: ['type', 'id'],
      patternProperties: {
        type: {type: 'string'},
        id: {type: 'string'},
      },
    },
  })
  event?: {
    type: string;
    id: string;
  };

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['DEAL_STATUS', 'INVESTMENTS', 'GENERAL', 'CLOSING_ACTIVITY'],
    },
  })
  category?: string;

  @property({
    type: 'string',
  })
  message?: string;

  @property({
    type: 'boolean',
    default: () => false,
  })
  acknowledged: boolean;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: string;

  @property({
    type: 'date',
  })
  deletedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Notification>) {
    super(data);
  }
}

export interface NotificationRelations {
  // describe navigational properties here
}

export type NotificationWithRelations = Notification & NotificationRelations;
