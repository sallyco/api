import {model, property} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - InvestorSubscription
 */
@model({name: 'InvestorSubscription'})
export class InvestorSubscription {
  @property()
  profileId: string;

  @property()
  name: string;

  @property()
  email: string;

  @property()
  investorType?: string;

  @property()
  entityName?: string;

  @property()
  address: object;

  @property()
  phone: string;

  @property()
  dealId: string;

  @property()
  dealName: string;

  @property()
  dealStatus: string;

  @property()
  subscriptionId: string;

  @property()
  subscriptionStatus: string;

  @property()
  subscribedOn: string;

  @property()
  subscriptionAmount: number;
}

export interface InvestorSubscriptionRelations {
  // describe navigational properties here
}

export type InvestorSubscriptionWithRelations = InvestorSubscription &
  InvestorSubscriptionRelations;
