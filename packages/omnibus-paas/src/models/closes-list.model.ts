/* eslint-disable @typescript-eslint/no-explicit-any */
import {model, property} from '@loopback/repository';

@model({name: 'ClosesList'})
export class ClosesList {
  constructor(data?: Partial<ClosesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Close array
   */
  @property()
  data?: any;

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;
  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface ClosesListRelations {
  // describe navigational properties here
}

export type ClosesListWithRelations = ClosesList & ClosesListRelations;
