/**
 * List of Models/Entities for Pagination
 */
import {model, property} from '@loopback/repository';

@model({
  name: 'List',
})
export class List<Entity> {
  constructor(data?: Partial<List<Entity>>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }
  @property({
    type: 'array',
    itemType: 'object',
  })
  data?: Entity[];

  @property({
    type: 'number',
  })
  page?: number;

  @property({
    type: 'number',
  })
  perPage?: number;

  @property({
    type: 'number',
  })
  totalCount?: number;
}

export default List;
