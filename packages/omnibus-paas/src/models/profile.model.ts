import {
  Entity,
  hasMany,
  model,
  property,
  belongsTo,
} from '@loopback/repository';
import {getJsonSchema} from '@loopback/repository-json-schema';
import {importable} from '../decorators/importable-model-decorator';
import {ExternalBankingUser} from './external-banking-user.model';
import {Deal} from './deal.model';
import {Individual} from './individual.model';
import {Company} from './company.model';
import {User} from './user.model';
import {Kyc} from './kyc.model';
import {Accreditation} from './accreditation.model';
import {Package} from './package.model';

/**
 * The model class is generated from OpenAPI schema - Profile
 * A representation of a profile
 */
@model({
  name: 'Profile',
  settings: {
    hiddenProperties: ['isDeleted', 'deletedAt'],
    // strict: 'filter',
  },
  scope: {
    include: ['kycAml'],
  },
  jsonSchema: {
    description: 'Profile Object',
  },
})
export class Profile extends Entity {
  constructor(data?: Partial<Profile>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Profile ID
   */
  @property({
    type: 'string',
    id: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      type: 'string',
      example: '606b6259de5b6d0012c089ea',
    },
  })
  @importable({
    displayName: 'ID',
  })
  id: string;
  /**
   * Tenant ID
   */
  @property({
    type: 'string',
    jsonSchema: {
      example: 'yourtenantid',
    },
  })
  tenantId?: string;

  /**
   * The type of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'Profile Type',
      description: 'The Type of Profile',
      enum: ['ORGANIZER', 'INVESTOR', 'FOUNDER', 'INDIVIDUAL'],
      example: 'INVESTOR',
    },
  })
  @importable({
    displayName: 'Profile Type',
    description: 'Type of Profile',
  })
  profileType?: 'ORGANIZER' | 'INVESTOR' | 'FOUNDER' | 'INDIVIDUAL';

  /**
   * The state of formation of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'State of Formation',
      description: 'State the Entity was Formed In (If Entity profile)',
      example: 'Utah',
    },
  })
  @importable({
    displayName: 'State of Formation',
    description: 'State the Profile was Formed In (If Entity)',
  })
  stateOfFormation?: string;

  /**
   * The country of formation of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'Country of Formation',
      description: 'Country the Profile was Formed In (If Entity)',
      example: 'United States of America',
    },
  })
  @importable({
    displayName: 'Country of Formation',
    description: 'Country the Profile was Formed In (If Entity)',
  })
  countryOfFormation?: string;

  /**
   * The display name of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      maxLength: 1024,
      example: 'My Investment Profile',
      title: 'Display Name',
      description: 'The Display Name of the Profile',
    },
  })
  @importable({
    displayName: 'Display Name',
    description: 'The Display Name of the Profile',
  })
  displayName?: string;

  /**
   * The legal name of the profile
   */
  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      maxLength: 1024,
      example: 'Investment Company, LLC',
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Name of Entity (if Entity Profile)',
  })
  name?: string;

  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
      deprecated: true,
      title: '[Deprecated]',
      description: '[Deprecated]',
    },
  })
  @importable({
    displayName: 'Joint Account Name',
  })
  jointAccountName?: string;

  /**
   * The type of the profile if entity
   */
  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: [
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
        'S_CORPORATION',
        'GENERAL_PARTNERSHIP',
        '501_C_NONPROFIT',
        'FOREIGN_ENTITY',
        'INDIVIDUAL',
      ],
      title: 'Type of Entity',
      description: 'The Type of Entity (If an Entity Profile)',
      example: 'LIMITED_LIABILITY_COMPANY',
    },
  })
  @importable({
    displayName: 'Type of Entity',
    description: 'The Type of Entity',
  })
  typeOfEntity?:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION'
    | 'S_CORPORATION'
    | 'GENERAL_PARTNERSHIP'
    | '501_C_NONPROFIT'
    | 'FOREIGN_ENTITY'
    | 'INDIVIDUAL';

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: [
        'JOINT_TENANTS_WITH_RIGHTS_OF_SURVIVORSHIP',
        'TENANTS_IN_COMMON',
        'COMMUNITY_PROPERTY',
        'TENANTS_BY_ENTIRETY',
      ],
      title: 'Joint Type',
      description: 'The Type of Joint Profile (If Joint Profile Type)',
      example: 'TENANTS_IN_COMMON',
    },
  })
  @importable({
    displayName: 'Type of Joint',
    description: 'The Type of Joint Profile',
  })
  jointType?:
    | 'JOINT_TENANTS_WITH_RIGHTS_OF_SURVIVORSHIP'
    | 'TENANTS_IN_COMMON'
    | 'COMMUNITY_PROPERTY'
    | 'TENANTS_BY_ENTIRETY';

  /**
   * The first name of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      maxLength: 1024,
      title: 'First Name',
      description: 'The First Name of the Profile Owner',
      example: 'John',
    },
  })
  @importable({
    displayName: 'First Name',
    description: 'The First Name of the Profile Owner',
  })
  firstName?: string;

  /**
   * The last name of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 1024,
      title: 'Last Name',
      description: 'The Last Name of the Profile Owner',
      example: 'Smith',
    },
  })
  @importable({
    displayName: 'Last Name',
    description: 'The Last Name of the Profile Owner',
  })
  lastName?: string;

  /**
   * The address of the profile
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['address1', 'city', 'postalCode', 'country'],
      properties: {
        address1: {type: 'string', minLength: 2, maxLength: 1024},
        address2: {type: 'string', maxLength: 1024},
        city: {type: 'string', minLength: 2, maxLength: 1024},
        state: {type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {type: 'string', minLength: 2, maxLength: 1024},
        country: {type: 'string', minLength: 2, maxLength: 1024},
      },
      title: 'Address',
      description:
        'The physical address of the Entity or Individual associated with this profile',
      example: {
        address1: '123 Test Street',
        address2: 'Unit #6',
        city: 'Salt Lake City',
        state: 'Utah',
        postalCode: '84121',
        country: 'United States of America',
      },
    },
  })
  @importable({
    displayName: 'Address',
    description: 'The Address of the Profile',
  })
  address?: {
    address1: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };

  /**
   * The address of the profile
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['address1', 'city', 'postalCode', 'country'],
      properties: {
        address1: {type: 'string', minLength: 2, maxLength: 1024},
        address2: {type: 'string', maxLength: 1024},
        city: {type: 'string', minLength: 2, maxLength: 1024},
        state: {type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {type: 'string', minLength: 2, maxLength: 1024},
        country: {type: 'string', minLength: 2, maxLength: 1024},
      },
      title: 'Delivery Address',
      description: 'The Delivery Address of the Profile',
      example: {
        address1: '123 Test Street',
        address2: 'Unit #6',
        city: 'Salt Lake City',
        state: 'Utah',
        postalCode: '84121',
        country: 'United States of America',
      },
    },
  })
  @importable({
    displayName: 'Delivery Address',
    description: 'The Delivery Address of the Profile',
  })
  deliveryAddress?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };

  /**
   * The phone number of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'Phone Number',
      description:
        'The contact phone number of the Profile Entity or Individual',
      example: '+1 (555)-555-5555',
    },
  })
  @importable({
    displayName: 'Phone',
    description: 'The Phone Number of the Profile Owner',
  })
  phone?: string;

  /**
   * The date of birth of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'Date of Birth',
      description:
        'The Date of Birth for the Individual associated with the Profile',
      example: '1989-02-22',
    },
  })
  @importable({
    displayName: 'Date of Birth',
    description:
      'The Date of Birth for the Individual associated with the Profile',
  })
  dateOfBirth?: string;

  /**
   * The date of formation of the profile (if business)
   */
  @property({
    type: 'string',
    jsonSchema: {
      title: 'Date of Formation',
      description: 'The Date of Formation for the Entity (If Entity Profile)',
      example: '1989-02-22',
    },
  })
  @importable({
    displayName: 'Date of Formation',
    description: 'The Date of Formation of the Profile (if Business)',
  })
  dateOfFormation?: string;

  /**
   * The email of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^\S+@\S+$/.source,
      format: 'email',
      title: 'Email',
      description: 'The contact email of the Profile Entity or Individual',
      example: 'johnsmith@test.com',
    },
  })
  @importable({
    displayName: 'Email',
    description: 'The Email of the Profile Owner',
  })
  email?: string;

  @property({
    type: 'string',
    jsonSchema: {
      deprecated: true,
      title: '[Deprecated]',
      description: '[Deprecated]',
    },
  })
  @importable({
    displayName: 'Image',
    description: 'Image',
  })
  image: string;

  // /**
  //  * The social security of the profile
  //  * conditional model validation ssn(XXX-XX-XXXX),ein(XX-XXXXXXX) based on registrationtype individual or entity
  //  */

  @property({
    type: 'object',
    jsonSchema: {
      required: ['registrationType', 'taxIdentification'],
      properties: {
        registrationType: {
          type: 'string',
          enum: ['INDIVIDUAL', 'ENTITY', 'TRUST', 'JOINT'],
          title: 'registrationType',
        },
        taxIdentification: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
              enum: ['ssn', 'giin', 'itin', 'ftin', 'ein'],
            },
            value: {type: 'string'},
          },
        },
      },
      if: {
        properties: {registrationType: {const: 'INDIVIDUAL'}},
      },
      then: {
        properties: {
          taxIdentification: {
            properties: {
              type: {enum: ['ssn', 'itin', 'ftin']},
            },
          },
        },
      },
      else: {
        properties: {
          taxIdentification: {
            properties: {
              type: {enum: ['ssn', 'giin', 'itin', 'ein', 'ftin']},
            },
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Tax Details',
    description: 'The Tax Details for the Profile',
  })
  taxDetails?: {
    registrationType?: 'INDIVIDUAL' | 'ENTITY' | 'TRUST' | 'JOINT';
    taxIdentification: {
      type: string;
      value: string;
    };
  };

  /**
   * The passportId number of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      deprecated: true,
      title: '[Deprecated]',
      description: '[Deprecated]',
    },
  })
  @importable({
    displayName: 'Passport',
    description: 'The Passport Number of the Profile Owner',
  })
  passportId?: string;

  /**
   * The passportId number of the profile owner
   */
  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      title: 'Title',
      description:
        'The Title of the Individual associated with the profile (If Individual Profile)',
      example: 'CTO',
    },
  })
  @importable({
    displayName: 'Title',
    description: 'The Title of the Profile Owner',
  })
  title?: string;

  /**
   * Whether the profile owner is US based or not
   */
  @property({
    type: 'boolean',
    jsonSchema: {
      title: 'Is U.S. Based',
      description: 'Whether the Entity is based in the U.S. or not',
      example: true,
    },
  })
  @importable({
    displayName: 'Is US Based',
    description: 'Whether the profile owner is US based or not',
  })
  isUSBased?: boolean;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is RIA',
  })
  isRIA?: boolean;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is ERA',
  })
  isERA?: boolean;

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'ERA AccreditationStatus',
  })
  ERAStatus?: string;

  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Primary Contact',
  })
  primaryContact?: Individual;

  @property({
    type: 'boolean',
  })
  @importable({
    displayName: 'Is Single Member LLC',
    description: 'Whether the profile is a Single Member LLC',
  })
  isSingleMemberLLC?: boolean;

  @property({
    type: 'object',
    jsonSchema: getJsonSchema(Individual),
  })
  @importable({
    displayName: 'Primary Signatory',
    description: 'The Primary Signatory for the Profile',
  })
  primarySignatory?: Individual;

  @property.array(Individual)
  @importable({
    displayName: 'Additional Signatories',
    description: 'The Additional Signatories for the Profile',
  })
  additionalSignatories?: Individual[];

  @property({
    type: 'object',
    jsonSchema: getJsonSchema(Individual),
  })
  @importable({
    displayName: 'Beneficial Owner Signatory',
    description: 'The Beneficial Owner Signatory for the Profile',
  })
  beneficialOwner?: Individual;

  /**
   * AccreditationStatus
   */
  @property.array(Number, {
    type: 'array',
    items: {
      type: 'number',
      example: 0,
    },
    example: [0, 1, 2],
    title: 'Investor AccreditationStatus',
    description: `Investor Status representation for Document Generation. 
    Numbers in index correspond to checkboxes in documents, in numerical order`,
  })
  investorStatus?: Number[];

  @hasMany(() => Kyc, {keyTo: 'profileId'})
  kycAml?: Kyc;

  // Relationship to Deals
  @hasMany(() => Deal, {keyTo: 'profileId'})
  deals: Deal[];

  @belongsTo(() => Company)
  managerId?: string;

  @belongsTo(() => Company)
  masterEntityId?: string;

  @belongsTo(() => User)
  ownerId: string;

  @hasMany(() => Accreditation, {keyTo: 'profileId'})
  accreditations: Accreditation[];

  @belongsTo(() => Package)
  packageId?: string;

  @property({
    type: 'object',
    jsonSchema: getJsonSchema(ExternalBankingUser),
  })
  bankingUser?: ExternalBankingUser;

  /**
   * Purchaser AccreditationStatus
   */
  @property.array(Number)
  purchaserStatus?: Number[];

  /**
   * The date the profile was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the profile was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * The date a profile was deleted
   */
  @property({
    type: 'date',
    jsonSchema: {
      deprecated: true,
      title: '[Deprecated]',
      description: '[Deprecated]',
    },
  })
  deletedAt: Date;

  /**
   * The date a profile was deleted
   * @deprecated since soft-deletes are not consistent with our system design
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  /**
   * The address of the profile
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['brokerName', 'dtcNumber', 'accountNumber'],
      properties: {
        brokerName: {type: 'string', maxLength: 128},
        dtcNumber: {type: 'string', maxLength: 4},
        accountNumber: {type: 'string', maxLength: 64},
      },
      title: 'brokerageAccount',
      description:
        '',
    },
  })
  @importable({
    displayName: 'brokerageAccount',
    description: 'The BrokerageAccount of the Profile',
  })
  brokerageAccount?: {
    brokerName: string;
    dtcNumber: string;
    accountNumber: string;
  };
}

export interface ProfileRelations {
  // describe navigational properties here
  deals: Deal[];
}

export type ProfileWithRelations = Profile & ProfileRelations;
