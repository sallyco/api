import {Entity, model, property} from '@loopback/repository';
import {getJsonSchema} from '@loopback/repository-json-schema';
import {FormationRequestTaxContact} from './dto/formation-request/formation-request-tax-contact.model';
import {webhookConfig} from '../decorators/webhook-model-name-decorator';

export enum FormationRequestStatus {
  'ORDER_PLACED' = 'ORDER_PLACED',
  'PROCESSING' = 'PROCESSING',
  'FULFILLED' = 'FULFILLED',
  'EXCEPTION' = 'EXCEPTION',
  'CANCELLED' = 'CANCELLED',
}

@webhookConfig({
  resourceId: 'legal/formation/requests',
})
@model({
  name: 'FormationRequest',
  settings: {
    hiddenProperties: ['tenantId', 'providerMeta', 'taxContact'],
  },
})
export class FormationRequest extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerId: string;

  @property({
    type: 'string',
  })
  tenantId: string;

  @property({
    type: 'string',
    required: true,
  })
  status: FormationRequestStatus;

  @property({
    type: 'string',
  })
  ein: string;

  @property({
    type: 'object',
    jsonSchema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        type: {
          type: 'string',
          enum: ['LLC', 'LP'],
        },
        stateOfFormation: {
          type: 'string',
        },
        address: {
          required: ['street1', 'city', 'postalCode', 'state', 'country'],
          properties: {
            street1: {
              type: 'string',
              minLength: 2,
              maxLength: 1024,
            },
            street2: {type: 'string', maxLength: 1024},
            city: {type: 'string', minLength: 2, maxLength: 1024},
            state: {type: 'string', minLength: 2, maxLength: 1024},
            postalCode: {type: 'string', minLength: 2, maxLength: 1024},
            country: {
              type: 'string',
              minLength: 2,
              maxLength: 1024,
              enum: ['United States of America'],
            },
          },
        },
        isSeries: {
          type: 'boolean',
        },
      },
      if: {
        properties: {
          isSeries: {
            const: true,
          },
        },
      },
      then: {
        required: ['masterEntity'],
        properties: {
          masterEntity: {
            required: ['name'],
            properties: {
              name: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
              },
            },
          },
        },
      },
    },
  })
  entity: {
    name: string;
    type: string;
    stateOfFormation: string;
    address?: {
      street1: string;
      street2?: string;
      city: string;
      state: string;
      postalCode: string;
      country: string;
    };
    isSeries?: boolean;
    masterEntity?: {
      name: string;
    };
  };

  @property({
    type: 'object',
    jsonSchema: getJsonSchema(FormationRequestTaxContact, {
      partial: true,
    }),
  })
  taxContact: Partial<FormationRequestTaxContact>;

  @property.array('string')
  documents: string[];

  @property({type: 'string'})
  orderId?: string;

  /**
   * The date the Formation Request object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the Formation Request object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'object',
  })
  providerMeta: {
    typeId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };

  constructor(data?: Partial<FormationRequest>) {
    super(data);
  }
}

export interface FormationRequestRelations {
  // describe navigational properties here
}

export type FormationRequestWithRelations = FormationRequest &
  FormationRequestRelations;
