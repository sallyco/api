import {Entity, model, property} from '@loopback/repository';
import {EmailTypes} from '../services';

@model({
  name: 'EmailTemplate',
  settings: {
    strict: false,
    hiddenProperties: ['tenantId', 'ownerId'],
  },
})
export class EmailTemplate extends Entity {
  constructor(data?: Partial<EmailTemplate>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
  })
  id: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId?: string;

  /**
   * The entity type of the portfolio company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: [EmailTypes],
    },
  })
  public key: EmailTypes;

  @property({
    type: 'object',
  })
  data?: {};

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;
}

export interface EmailTemplateRelations {
  // describe navigational properties here
}

export type EmailTemplateWithRelations = EmailTemplate & EmailTemplateRelations;
