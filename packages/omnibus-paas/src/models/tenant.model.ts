import {Entity, model, property, belongsTo, hasOne} from '@loopback/repository';
import {Company} from './company.model';
import {WhiteLabel} from './white-label.model';
import {Realm} from './realm.model';
import {BankAccount} from './bank-account.model';
import {Package} from './package.model';

export interface TieredPriceEntry {
  breakpoint: number;
  price: number;
}

@model({
  name: 'Tenant',
  settings: {
    strict: 'filter',
  },
})
export class Tenant extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  url: string;

  @property({
    type: 'boolean',
    default: () => false,
  })
  inverted: boolean;

  @property({
    type: 'string',
  })
  registeredAgent?: string;

  @property({
    type: 'object',
  })
  theme?: object;
  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        images: {
          type: 'object',
          properties: {
            logo: {type: 'string'},
            logoInverted: {type: 'string'},
          },
        },
        colors: {
          type: 'object',
          properties: {
            primaryColor: {type: 'string'},
            secondaryColor: {type: 'string'},
            backgroundColor: {type: 'string'},
          },
        },
      },
    },
  })
  assets?: {
    images?: {
      logo?: string;
      logoInverted?: string;
    };
    colors?: {
      primaryColor?: string;
      secondaryColor?: string;
      backgroundColor?: string;
    };
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        emails: {
          type: 'object',
          properties: {
            fromAddress: {type: 'string'},
            compliance: {type: 'string'},
          },
        },
        showCarryRecipientsForInvestors: {
          type: 'boolean',
          default: false,
        },
        canSelfApproveWires: {
          type: 'boolean',
          default: true,
        },
        use506cAccreditation: {
          type: 'boolean',
          default: false,
        },
        parallelMarketsClientId: {
          type: 'string',
        },
      },
    },
  })
  settings?: {
    emails?: {
      fromAddress?: string;
    };
    useOrganizerForEINOrders?: boolean;
    showCarryRecipientsForInvestors?: boolean;
    canSelfApproveWires?: boolean;
    useMultiStepSigningForCloses?: boolean;
    use506cAccreditation?: boolean;
    parallelMarketsClientId?: string;
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        name: {
          type: 'string',
        },
        email: {
          type: 'string',
        },
        phone: {
          type: 'string',
        },
      },
    },
  })
  owner?: {
    name?: string;
    email?: string;
    phone?: string;
  };

  @property({
    type: 'number',
    required: false,
    default: () => 0,
  })
  dealLimit?: number;

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        maxDealCount: {
          type: 'integer',
        },
        maxInvestorCount: {
          type: 'integer',
        },
        maxTargetRaiseAmount: {
          type: 'integer',
        },
        perDealMaxInvestorCount: {
          type: 'integer',
        },
        perDealMaxTargetRaiseAmount: {
          type: 'integer',
        },
      },
    },
  })
  dealLimits?: {
    maxDealCount?: number;
    maxInvestorCount?: number;
    maxTargetRaiseAmount?: number;
    maxExpenseReserve?: number;
    perDealMaxInvestorCount?: number;
    perDealMaxTargetRaiseAmount?: number;
    costPerSPV?: number;
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        ach: {
          type: 'object',
          properties: {
            credit: {
              type: 'object',
              properties: {
                daily: {
                  type: 'integer',
                },
                transaction: {
                  type: 'integer',
                },
              },
            },
            debit: {
              type: 'object',
              properties: {
                daily: {
                  type: 'integer',
                },
                transaction: {
                  type: 'integer',
                },
              },
            },
          },
        },
        wire: {
          type: 'object',
          properties: {
            credit: {
              type: 'object',
              properties: {
                daily: {
                  type: 'integer',
                },
                transaction: {
                  type: 'integer',
                },
              },
            },
            debit: {
              type: 'object',
              properties: {
                daily: {
                  type: 'integer',
                },
                transaction: {
                  type: 'integer',
                },
              },
            },
          },
        },
      },
    },
  })
  bankingLimits?: {
    ach?: {
      credit?: {
        daily: number;
        transaction: number;
      };
      debit?: {
        daily: number;
        transaction: number;
      };
      exposure?: number;
    };
    wire?: {
      credit?: {
        daily: number;
        transaction: number;
      };
      debit?: {
        daily: number;
        transaction: number;
      };
      exposure?: number;
    };
  };

  @belongsTo(() => Company)
  managerId?: string;

  @belongsTo(() => Company)
  masterEntityId?: string;

  @property({
    type: 'string',
    required: false,
    default: () => 0,
  })
  closedStats?: string;

  @property({
    type: 'object',
    jsonSchema: {
      required: ['address', 'city', 'postalCode', 'state'],
      properties: {
        address: {
          title: 'Address',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
          description: 'Address of Tenant',
        },
        city: {
          title: 'City',
          type: 'string',
          description: 'City of Tenant',
        },
        state: {
          title: 'State',
          type: 'string',
          description: 'State of Tenant',
        },
        postalCode: {
          title: 'PostalCode',
          type: 'string',
          minLength: 2,
          maxLength: 12,
          description: 'Postalcode of Tenant',
        },
      },
      title: 'Address',
    },
  })
  address?: {
    address?: string;
    city?: string;
    state?: string;
    postalCode?: string;
  };

  @property({
    type: 'string',

    jsonSchema: {
      title: 'StartDate',
      description: 'Contract StartDate',
    },
  })
  startDate?: string;

  @property({
    type: 'string',

    jsonSchema: {
      title: 'EndDate',
      description: 'Contract EndDate',
    },
  })
  endDate?: string;

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        sassPackageType: {
          enum: ['Bronze', 'Silver', 'Gold', 'Tiered'],
          title: 'Saas PackageType',
          description: 'SaaS Package Type (Bronze, Silver, Gold, Tiered)',
        },
        monthlyRecurringPrice: {
          type: 'number',
          title: 'Price',
          description: 'Monthly Recurring Price',
        },
        noOfContractedSPVs: {
          type: 'number',
          title: 'Contracted SPVs',
          description: 'Number of Contracted SPVs',
        },
        noOfSPVsconsumed: {
          type: 'number',
          title: 'SPVs consumed',
          description: 'Number of SPVs consumed',
        },
        spvsremaining: {
          type: 'number',
          title: 'SPVs remaining',
          description: 'Contracted SPVs remaining',
        },
      },
      title: 'Pricing Plans:',
    },
  })
  saasPackageType?: {
    saasPackageType?: 'Bronze' | 'Silver' | 'Gold' | 'Tiered';
    monthlyRecurringPrice?: number;
    noOfContractedSPVs?: number;
    noOfSPVsconsumed?: number;
    spvsremaining?: number;
    tieredPricing?: TieredPriceEntry[];
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        withholdfromdeal: {
          title: 'With hold from deal?',
          type: 'boolean',
        },
        type: {
          type: 'string',
          title: 'Type',
          enum: [
            'With hold from Deal',
            'Credit Card',
            'Cheque',
            'Wire',
            'ACH Debit',
          ],
        },
        frequency: {
          type: 'string',
          title: 'Frequency',
          enum: ['Monthly', 'Annually'],
        },
      },
      title: 'Payment Method',
      description:
        'Withhold from deal Y/N (if yes, set SPV admin fee on closing statement, if no, don’t include on closing statement)',
    },
  })
  paymentMethod?: {
    withholdfromdeal?: boolean;
    type?:
      | 'With hold from Deal'
      | 'Credit Card'
      | 'Cheque'
      | 'Wire'
      | 'ACH Debit';
    frequency?: 'Monthly' | 'Annually';
    ach?: {
      accountName?: string;
      accountNumber?: string;
      routingNumber?: string;
      typeOfAccount?: 'Checking' | 'Savings';
    };
  };

  @property({
    type: 'object',
    required: false,
  })
  services?: {
    productId?: number;
    name?: string;
    price?: number;
  };

  @property({
    type: 'array',
    itemType: 'object',
    items: {
      type: 'object',
      jsonSchema: {
        properties: {
          productId: {
            type: 'number',
          },
          name: {
            title: 'Name',
            description: 'Name of service',
            type: 'string',
          },
          price: {
            title: 'Price',
            description: 'Price of service',
            type: 'number',
          },
        },
        title: 'Services',
      },
    },
  })
  tenantServices?: {
    productId?: number;
    name?: string;
    price?: number;
  }[];

  @property({
    type: 'boolean',
    jsonSchema: {
      default: true,
      title: 'Auto create new bank account?',
      description: 'Auto create new bank account new bank account manager',
    },
  })
  createBankAccount: boolean;

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        ABA: {
          type: 'number',
          title: 'ABA Routing Number',

          description: 'ABA Routing Number',
        },
        accountnumber: {
          title: 'Account Number',
          type: 'string',
          description: 'Account Number',
        },
        bankname: {
          title: 'Bank Name',
          type: 'string',
          description: 'Name of the bank',
        },
      },
    },
  })
  bankAccountDetails?: {
    ABA?: string;
    accountnumber?: number;
    bankname?: string;
  };

  @property({
    type: 'number',
    required: false,
    default: () => 180,
  })
  maxAllowedDaysToClose?: number;

  @property.array('string')
  omdUrls?: string[];

  @property({
    type: 'string',
  })
  termsOfUse?: string;

  @property({
    type: 'string',
  })
  privacyPolicy?: string;

  @property.array('string')
  files?: string[];

  @hasOne(() => WhiteLabel)
  whiteLabel: WhiteLabel;

  @belongsTo(() => Realm, {name: 'realm', keyFrom: 'id', keyTo: 'id'})
  realmId: string;

  @property({
    type: 'string',
  })
  billingContact?: string;
  @hasOne(() => BankAccount, {keyTo: 'tenantIdForFees'})
  feesPaymentsAccount: BankAccount;

  @hasOne(() => BankAccount, {keyTo: 'tenantIdForBlueSky'})
  blueSkyPaymentsAccount: BankAccount;

  @belongsTo(() => Package)
  packageId?: string;

  constructor(data?: Partial<Tenant>) {
    super(data);
  }
}

export interface TenantRelations {
  // describe navigational properties here
}

export type TenantWithRelations = Tenant & TenantRelations;
