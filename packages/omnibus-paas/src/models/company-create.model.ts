import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Individual} from './individual.model';
import {LegalIncOrderModel} from './legalinc-order.model';

/**
 * The model class is generated from OpenAPI schema - Company
 * A representation of objects associated with the fund
 */
@model({
  name: 'Create Company',
})
export class CompanyCreate extends Entity {
  constructor(data?: Partial<CompanyCreate>) {
    super(data);

    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /* company name */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Company Name',
  })
  name: string;

  /* company website */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Website',
    description: 'The Company Website',
  })
  website?: string;

  /***** contact info *****/

  /* company email */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^\S+@\S+$/.source,
    },
  })
  @importable({
    displayName: 'Email',
    description: 'The contact email for the company',
  })
  email?: string;

  /**
   * The type of the company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['MASTER_ENTITY', 'REGISTERED_AGENT', 'MANAGER'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of company',
  })
  type?: 'MASTER_ENTITY' | 'REGISTERED_AGENT' | 'MANAGER';

  /**
   * Entity type
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Entity Type',
  })
  entityType?: string;

  /* company phone */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Phone',
    description: 'The Phone number for the company',
  })
  phone?: string;

  /**
   * Raise amount being targeted for the deal
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Next series number',
  })
  seriesNumber?: number;

  /**
   * Raise amount being targeted for the deal
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'State of Formation',
  })
  stateOfFormation?: string;

  // /* company address */
  // @property({
  //   type: 'object',
  //   jsonSchema: {
  //     required: ['address1', 'city', 'postalCode', 'country'],
  //     patternProperties: {
  //       address1: {type: 'string', minLength: 2, maxLength: 1024},
  //       address2: {type: 'string', maxLength: 1024},
  //       city: {type: 'string', minLength: 2, maxLength: 1024},
  //       state: {type: 'string', minLength: 2, maxLength: 1024},
  //       postalCode: {type: 'string', minLength: 2, maxLength: 1024},
  //       country: {type: 'string', minLength: 2, maxLength: 1024},
  //     },
  //   },
  // })
  // @importable({
  //   displayName: 'Address',
  //   description: 'The Company Address',
  // })
  // address?: {
  //   address1?: string;
  //   address2?: string;
  //   city: string;
  //   state: string;
  //   postalCode: string;
  //   country: string;
  // };

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  @property({type: 'string'})
  arbitrationCity?: string;

  @property({type: 'string'})
  arbitrationState?: string;

  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Primary Contact',
  })
  primaryContact?: Individual;

  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Tax Contact',
  })
  taxContact?: Individual;

  @property(LegalIncOrderModel)
  legalIncOrder?: Partial<LegalIncOrderModel>;
}
