import {model, property} from '@loopback/repository';

@model({name: 'FundDocuments'})
export class FundDocuments {
  constructor(data?: Partial<FundDocuments>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  @property()
  subscriptionId?: string;

  @property()
  tenantId?: string;

  @property()
  investorName?: string;

  @property()
  investorEmail?: string;

  @property()
  dealId?: string;

  @property()
  dealName?: string;

  @property()
  organizerId?: string;

  @property()
  organizerName?: string;

  @property()
  organizerFirstName?: string;

  @property()
  organizerLastName?: string;

  @property()
  entityId?: string;

  @property()
  entityDocs?: object;

  @property()
  closeId?: string;

  @property()
  organizerSigned?: boolean;

  @property()
  managerSigned?: boolean;

  @property()
  investorSigned?: boolean;

  @property()
  subscriptionAmount?: number;

  @property({
    type: 'date',
  })
  subscriptionCreatedAt: Date;
}

export interface FundDocumentsRelations {
  // describe navigational properties here
}

export type FundDocumentsWithRelations = FundDocuments & FundDocumentsRelations;
