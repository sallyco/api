import {Entity, model, property} from '@loopback/repository';

@model()
export class WhiteLabel extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'object',
  })
  images?: {
    logo?: string;
    logoInverted?: string;
    icon?: string;
  };

  @property({
    type: 'object',
  })
  theme?: {
    palette?: {
      common?: {
        black?: string;
        white?: string;
      };
      type?: 'light' | 'dark';
      primary?: {
        light?: string;
        main?: string;
        dark: string;
        contrastText?: string;
      };
      secondary?: {
        light?: string;
        main?: string;
        dark?: string;
        contrastText?: string;
      };
      error?: {
        light?: string;
        main?: string;
        dark?: string;
        contrastText?: string;
      };
      warning?: {
        light?: string;
        main?: string;
        dark?: string;
        contrastText?: string;
      };
      info?: {
        light?: string;
        main?: string;
        dark?: string;
        contrastText?: string;
      };
      success?: {
        light?: string;
        main?: string;
        dark?: string;
        contrastText?: string;
      };

      grey?: {
        '50'?: string;
        '100'?: string;
        '200'?: string;
        '300'?: string;
        '400'?: string;
        '500'?: string;
        '600'?: string;
        '700'?: string;
        '800'?: string;
        '900'?: string;
        A100?: string;
        A200?: string;
        A400?: string;
        A700?: string;
      };

      contrastThreshold?: number;
      tonalOffset?: number;
      text?: {
        primary?: string;
        secondary?: string;
        disabled?: string;
        hint?: string;
        icon?: string;
      };
      divider?: string;
      background?: {
        paper?: string;
        default?: string;
      };
      action?: {
        active?: string;
        hover?: string;
        hoverOpacity?: number;
        selected?: string;
        selectedOpacity?: number;
        disabled?: string;
        disabledBackground?: string;
        disabledOpacity?: number;
        focus?: string;
        focusOpacity?: number;
        activatedOpacity?: number;
      };
    };
    typography?: {
      htmlFontSize: number;
      fontFamily?: string;
      fontSize?: number;
      fontWeightLight?: number;
      fontWeightRegular?: number;
      fontWeightMedium?: number;
      fontWeightBold?: number;
      h1?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      h2?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      h3?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      h4?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      h5?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      h6?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      subtitle1?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      subtitle2?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      body1?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      body2?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      button?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      caption?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
      overline?: {
        fontFamily?: string;
        fontWeight?: number;
        fontSize?: string;
        lineHeight?: number;
        letterSpacing?: string;
      };
    };
    shape?: {
      borderRadius?: number;
    };
  };

  @property({
    type: 'string',
  })
  tenantId?: string;

  constructor(data?: Partial<WhiteLabel>) {
    super(data);
  }
}

export interface WhiteLabelRelations {
  // describe navigational properties here
}

export type WhiteLabelWithRelations = WhiteLabel & WhiteLabelRelations;
