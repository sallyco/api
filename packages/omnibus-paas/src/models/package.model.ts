import {getJsonSchemaRef} from '@loopback/rest';
import {
  DefaultCrudRepository,
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
  hasMany,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Product, ProductWithRelations} from './product.model';
import {PackageProduct} from './package-product.model';
import _ from 'lodash';
import {PackageAttribute} from './package-attribute.model';
import {PackagePackageAttribute} from './package-package-attribute.model';

@model({
  title: 'Package',
  description: `
Package Specification
`,
})
class PackageBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Package Name',
      description: 'Name to identify this package',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Package Description',
      description:
        'Description for additional specification or clarification of the package and its usage',
      maxLength: 1024,
    },
  })
  description: string;

  constructor(data?: Partial<Package>) {
    super(data);
  }
}

@model()
export class Package extends PackageBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id?: string;

  @hasMany(() => Product, {through: {model: () => PackageProduct}})
  products: Product[];

  @hasMany(() => PackageAttribute, {
    through: {model: () => PackagePackageAttribute},
  })
  attributes: PackageAttribute[];

  constructor(data?: Partial<Package>) {
    super(data);
  }
}

export interface PackageRelations {
  products: ProductWithRelations[];
}
export type PackageWithRelations = Package & PackageRelations;

// DTO
const DefinePackageUpdateEdit = async () => {
  // TODO(Andrew): wrap this mad science into something, service?
  const ds = new MongodbDataSource();

  const productRepository = new DefaultCrudRepository(Product, ds);
  const pro = await productRepository.find();
  const attributeRepository = new DefaultCrudRepository(PackageAttribute, ds);
  const att = await attributeRepository.find();

  return new ModelDefinition('packageCreateUpdate')
    .addProperty('products', {
      type: 'array',
      jsonSchema: {
        title: 'Products',
        description: 'Products contained in this package',
        items: {
          type: 'string',
          enum: _.map(pro, 'id'),
          enumNames: _.map(pro, 'name'),
        },
        minItems: 0,
      },
    })
    .addProperty('attributes', {
      type: 'array',
      jsonSchema: {
        title: 'Attributes',
        description: 'Attributes contained in this package',
        items: {
          type: 'string',
          enum: _.map(att, 'id'),
          enumNames: _.map(att, 'name'),
        },
        minItems: 0,
      },
    });
};

export const PackageCreate = async () =>
  defineModelClass<typeof PackageBase, {products: []; attributes: []}>(
    PackageBase,
    await DefinePackageUpdateEdit(),
  );

export const PackageUpdate = async () => {
  const dpue = await DefinePackageUpdateEdit();
  return defineModelClass<
    typeof PackageBase,
    {id: string; products: []; attributes: []}
  >(
    PackageBase,
    dpue.addProperty('id', {
      type: 'string',
      jsonSchema: {readOnly: true},
    }),
  );
};

// SCHEMA
export const PackageSchema = getJsonSchemaRef(Package);

export const PackageCreateSchema = async () =>
  getJsonSchemaRef(await PackageCreate(), {
    title: 'Create a New Package',
    exclude: ['id'],
  });

export const PackageUpdateSchema = async () =>
  getJsonSchemaRef(await PackageUpdate(), {title: 'Edit a Package'});
