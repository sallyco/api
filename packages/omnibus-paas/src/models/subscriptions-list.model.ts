import {model, property} from '@loopback/repository';
import {Subscription} from './subscription.model';

/**
 * The model class is generated from OpenAPI schema - SubscriptionsList
 * List of subscriptions
 */
@model({name: 'SubscriptionsList'})
export class SubscriptionsList {
  constructor(data?: Partial<SubscriptionsList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Subscriptions
   */
  @property.array(Subscription)
  data?: Subscription[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface SubscriptionsListRelations {
  // describe navigational properties here
}

export type SubscriptionsListWithRelations = SubscriptionsList &
  SubscriptionsListRelations;
