import {getJsonSchemaRef} from '@loopback/rest';
import {
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
} from '@loopback/repository';

@model({
  title: 'Question',
  description: `
### Questions

There are two types of questions.  A __basic__ question simply captures the response.  A __scored__ question includes additional fields to calculate a value for the given response.
`,
  settings: {
    strict: 'filter',
  },
})
export class QuestionBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Question Type',
      description:
        'Determines if a question is basic (capture response only), or scored (calculate a value from the response)',
      enum: ['Basic', 'Scored'],
      default: 'Basic',
    },
  })
  assessmentType: 'Basic' | 'Scored';

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Category Name',
      description:
        'This is the formal name of the category that appears on statements, invoices, etc.',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Category Description',
      description:
        'Category description for additional specification or clarification of the category and its usage',
      maxLength: 1024,
    },
  })
  description?: string;

  constructor(data?: Partial<QuestionBase>) {
    super(data);
  }
}

@model()
export class Question extends QuestionBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id: string;

  constructor(data?: Partial<Question>) {
    super(data);
  }
}

export interface QuestionRelations {}

export type QuestionWithRelations = Question & QuestionRelations;

// DTO
export const QuestionCreate = defineModelClass<typeof Question, {}>(
  Question,
  new ModelDefinition('questionCreate'),
);

export const QuestionUpdate = defineModelClass<typeof Question, {id: string}>(
  Question,
  new ModelDefinition('questionEdit').addProperty('id', {
    type: 'string',
    jsonSchema: {readOnly: true},
  }),
);

// SCHEMA
export const QuestionSchema = getJsonSchemaRef(Question);

export const QuestionCreateSchema = getJsonSchemaRef(QuestionCreate, {
  title: 'Create a New Question',
  exclude: ['id'],
});
export const QuestionUpdateSchema = getJsonSchemaRef(QuestionUpdate, {
  title: 'Edit a Question',
});
