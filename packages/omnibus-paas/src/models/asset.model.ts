import {belongsTo, Entity, model, property} from '@loopback/repository';
import {TeamMember} from './team-member.model';
import {importable} from '../decorators/importable-model-decorator';
import {Individual} from './individual.model';
import {Deal} from './deal.model';
import {Profile} from './profile.model';

@model({
  name: 'Asset',
  settings: {
    hiddenProperties: ['isDeleted', 'deletedAt'],
  },
})
export class Asset extends Entity {
  /**
   * Asset ID
   */
  @property({
    type: 'string',
    id: true,
  })
  @importable({
    displayName: 'ID',
  })
  id: string;
  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId?: string;
  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId: string;
  /**
   * Deal ID
   */
  @importable({
    displayName: 'Deal ID',
  })
  @belongsTo(() => Deal)
  dealId?: string;
  /**
   * The type of the asset
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['COMPANY', 'REAL_ESTATE', 'CRYPTOCURRENCY', 'ARTWORK'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of Asset',
  })
  type?: 'COMPANY' | 'REAL_ESTATE' | 'CRYPTOCURRENCY' | 'ARTWORK';
  /**
   * The subType of the asset
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'SubType',
    description: 'The SubType of Asset',
  })
  subType?: string;
  /**
   * The Unit Count of the asset
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Unit Count',
  })
  unitCount?: number;
  /**
   * The Allocation Amount of the asset
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Allocation Amount',
  })
  allocationAmount?: number;
  /* asset name */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Name',
    description: 'Name of the Asset',
  })
  name: string;
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Sector',
    description: 'Sector of the Asset',
  })
  sector: string;
  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Primary Contact',
  })
  primaryContact?: Individual;
  /**
   * The market sector of the company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: [
        'COMMUNICATION_SERVICES',
        'CONSUMER_DISCRETIONARY',
        'CONSUMER_STAPLES',
        'ENERGY',
        'FINANCIALS',
        'HEALTHCARE',
        'INDUSTRIALS',
        'INFORMATION_TECHNOLOGY',
        'MATERIALS',
        'REAL_ESTATE',
        'UTILITIES',
      ],
    },
  })
  @importable({
    displayName: 'Company Market Sector',
    description: 'The Market Sector for the Asset',
  })
  public companyMarketSector?:
    | 'COMMUNICATION_SERVICES'
    | 'CONSUMER_DISCRETIONARY'
    | 'CONSUMER_STAPLES'
    | 'ENERGY'
    | 'FINANCIALS'
    | 'HEALTHCARE'
    | 'INDUSTRIALS'
    | 'INFORMATION_TECHNOLOGY'
    | 'MATERIALS'
    | 'REAL_ESTATE'
    | 'UTILITIES';
  /* description */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Description',
    description: 'The description of the Asset',
  })
  description?: string;
  /* size */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Size',
    description: 'The Size of the Asset',
  })
  size?: string;
  /* category */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Category',
    description: 'The Category for the Asset',
  })
  category?: string;
  /* description */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Advisors',
    description: 'The Advisors on the Asset',
  })
  advisors?: string;
  /**
   * An array of details
   */
  @property.array(Object)
  @importable({
    displayName: 'Details',
    description: 'The Asset Details',
  })
  details?: {}[];
  /**
   * An array of details
   */
  @property.array(Object)
  @importable({
    displayName: 'Properties',
    description: 'The Asset Properties',
  })
  properties?: {}[];
  /**
   * Website for the asset being purchased
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern:
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/
          .source,
    },
  })
  @importable({
    displayName: 'URL',
    description: 'The URL for the Asset',
  })
  assetUrl?: string;
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Appraised Value',
    description: 'The Appraised Value of the Asset',
  })
  appraisedValue?: number;
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Fractional Ownership Amount',
    description: 'The amount of Fractional Ownership for the Asset',
  })
  fractionalOwnershipAmount?: number;
  /**
   *
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        name: {type: 'string'},
        location: {type: 'string'},
        images: {type: 'array', items: {type: 'string'}},
      },
    },
  })
  @importable({
    displayName: 'Artist',
    description: 'Artist details for the Asset',
  })
  artist?: {
    name?: string;
    location?: string;
    images?: string[];
  };
  /**
   * A contact
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        name: {type: 'string'},
        email: {type: 'string'},
        phone: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Contact',
    description: 'Contact Details for the Asset',
  })
  contact?: {
    name?: string;
    email?: string;
    phone?: string;
  };
  /**
   * A funding
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        round: {type: 'string'},
        targetRaiseAmount: {type: 'string'},
        securityType: {type: 'string'},
        preValuation: {type: 'string'},
        noOfShare: {type: 'string'},
        sharePrice: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Funding',
    description: 'Funding for the Asset',
  })
  funding?: {
    round?: string;
    targetRaiseAmount?: string;
    securityType?: string;
    preValuation?: string;
    noOfShare?: string;
    sharePrice?: string;
  };
  @property.array(TeamMember)
  @importable({
    displayName: 'Team',
    description: 'Team Members for the Asset',
  })
  team?: TeamMember[];
  @property.array('string')
  @importable({
    displayName: 'Revenue History',
    description: 'Historical Revenue for the Asset',
  })
  revenueHistory?: string[];
  @property.array('string')
  @importable({
    displayName: 'Costs History',
    description: 'Historical Costs for the Asset',
  })
  costsHistory?: string[];
  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  @importable({
    displayName: 'Invited Organizers',
    description: 'Organizers Invited to the Asset',
  })
  invitedOrganizers?: string[];
  /**
   * File
   */
  @property({
    type: 'array',
    itemType: 'string',
  })
  @importable({
    displayName: 'Files',
    description: 'Files associated with the Asset',
  })
  files?: string[];
  /**
   * Images
   */
  @property({
    type: 'array',
    itemType: 'string',
  })
  @importable({
    displayName: 'Images',
    description: 'Images associated with the Asset',
  })
  images?: string[];
  /**
   * Video
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Video',
    description: 'A Video associated with the Asset',
  })
  video?: string;
  /**
   * pitchDoc
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'PITCH Document',
    description: 'PITCH Document for the Asset',
  })
  pitchDoc?: string;
  /**
   * videoURL
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
      pattern:
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/
          .source,
    },
  })
  @importable({
    displayName: 'Video URL',
    description: 'Video URL for the Asset',
  })
  videoURL?: string;
  /**
   * isPublic
   */
  @property({
    type: 'boolean',
    default: false,
  })
  isPublic: boolean;
  /**
   * logo
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Logo',
    description: 'Logo Image for the Asset',
  })
  logo?: string;
  /**
   * banner
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Banner',
    description: 'Banner Image for the Asset',
  })
  banner?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  founderIds?: string[];

  @belongsTo(() => Profile)
  founderProfileId?: string;

  @property({
    type: 'object',
  })
  foundersAssessment?: object;

  /**
   * The date the asset object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;
  /**
   * The date the asset object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;
  /**
   * The date the asset object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;
  /**
   * Whether or not the asset object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;
  /**
   * Additional information for the asset
   */
  @property()
  additionalProperties?: {};

  constructor(data?: Partial<Asset>) {
    super(data);

    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }
}

export interface AssetRelations {
  // describe navigational properties here
}

export type AssetWithRelations = Asset & AssetRelations;
