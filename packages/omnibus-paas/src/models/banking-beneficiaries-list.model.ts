import {model, property} from '@loopback/repository';
import {BankingBeneficiary} from './banking-beneficiary.model';

/**
 * The model class is generated from OpenAPI schema - EntitiesList
 * List of entities
 */
@model({name: 'BankingBeneficiariesList'})
export class BankingBeneficiariesList {
  constructor(data?: Partial<BankingBeneficiariesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Banking Beneficiaries
   */
  @property.array(BankingBeneficiary)
  data?: Partial<BankingBeneficiary>[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface BankingBeneficiariesListRelations {
  // describe navigational properties here
}

export type BankingBeneficiariesListWithRelations = BankingBeneficiariesList &
  BankingBeneficiariesListRelations;
