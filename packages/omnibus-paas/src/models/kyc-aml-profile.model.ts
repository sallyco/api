import {model, property} from '@loopback/repository';
import {Profile} from './profile.model';

/**
 * The model class is generated from OpenAPI schema - ProfilesList
 * List of profiles
 */
@model({name: 'ProfilesList'})
export class KycAmlProfile {
  /**
   * Profile
   */
  @property()
  profile?: Profile;

  /**
   * Manager details
   */
  @property({
    type: 'object',
  })
  manager?: object;

  @property.array(Object)
  deals?: {}[];
}

export interface KycAmlProfileRelations {
  // describe navigational properties here
}

export type KycAmlProfileWithRelations = KycAmlProfile & KycAmlProfileRelations;
