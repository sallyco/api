import {model, property} from '@loopback/repository';
import {Deal} from './deal.model';

/**
 * The model class is generated from OpenAPI schema - DealsList
 * List of deals
 */
@model({name: 'DealsList'})
export class DealsList {
  constructor(data?: Partial<DealsList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Deals
   */
  @property.array(Deal)
  data?: Deal[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface DealsListRelations {
  // describe navigational properties here
}

export type DealsListWithRelations = DealsList & DealsListRelations;
