import {Entity, model, property} from '@loopback/repository';

@model()
export class PackagePackageAttribute extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  packageId?: string;

  @property({
    type: 'string',
  })
  packageAttributeId?: string;

  constructor(data?: Partial<PackagePackageAttribute>) {
    super(data);
  }
}

export interface PackagePackageAttributeRelations {
  // describe navigational properties here
}

export type PackagePackageAttributeWithRelations = PackagePackageAttribute &
  PackagePackageAttributeRelations;
