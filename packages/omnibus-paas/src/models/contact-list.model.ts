import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class ContactList extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  _id?: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerId: string;

  @property({
    required: true,
    type: 'array',
    itemType: 'object',
    jsonSchema: {
      items: {
        type: 'object',
        patternProperties: {
          name: {type: 'string'},
          email: {type: 'string'},
          phone: {type: 'string'},
        },
      },
    },
  })
  contacts: {name: string; email: string; phone: string}[];

  @property({
    type: 'date',
    required: false,
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    required: false,
    default: () => new Date(),
  })
  updatedAt?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ContactList>) {
    super(data);
  }
}

export interface ContactListRelations {
  // describe navigational properties here
}

export type ContactListWithRelations = ContactList & ContactListRelations;
