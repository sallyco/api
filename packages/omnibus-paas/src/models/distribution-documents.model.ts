import {Model, model, property} from '@loopback/repository';

@model()
export class DistributionDocuments extends Model {
  @property({
    type: 'array',
    itemType: 'string',
  })
  letterOfIntent?: string[];

  @property({
    type: 'string',
    itemType: 'string',
  })
  memorandumOfUnderstanding?: string[];

  @property({
    type: 'string',
    itemType: 'string',
  })
  definitivePurchaseAgreement?: string[];

  constructor(data?: Partial<DistributionDocuments>) {
    super(data);
  }
}

export interface DistributionDocumentsRelations {
  // describe navigational properties here
}

export type DistributionDocumentsWithRelations = DistributionDocuments &
  DistributionDocumentsRelations;
