import {model, property} from '@loopback/repository';
import {Asset} from './asset.model';

/**
 * The model class is generated from OpenAPI schema - AssetsList
 * A list of assets.
 */
@model({name: 'AssetsList'})
export class AssetsList {
  constructor(data?: Partial<AssetsList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Asset array
   */
  @property.array(Asset)
  data?: Asset[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;
  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface AssetsListRelations {
  // describe navigational properties here
}

export type AssetsListWithRelations = AssetsList & AssetsListRelations;
