import {getJsonSchemaRef} from '@loopback/rest';
import {
  DefaultCrudRepository,
  defineModelClass,
  ModelDefinition,
  Entity,
  model,
  property,
  belongsTo,
  hasMany,
  hasOne,
} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Individual} from './individual.model';
import {getJsonSchema} from '@loopback/repository-json-schema';
import {Entities} from './entities.model';
import {Profile} from './profile.model';
import {Transaction} from './transaction.model';
import {Distribution} from './distribution.model';
import {User} from './user.model';
import {Package} from './package.model';
import {MongodbDataSource} from '../datasources';
import _ from 'lodash';

@model({
  name: 'Deal',
  description: 'Deal attributes',
  settings: {
    hiddenProperties: ['isDeleted', 'deletedAt'],
    strict: 'filter',
  },
})
class DealBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
      title: 'Deal Name',
      description: 'Name used throughout to identify this deal',
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Deal Name',
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Deal Description',
      description: 'Description for this deal, can be HTML',
    },
  })
  @importable({
    displayName: 'Description',
    description: 'Deal Description',
  })
  description: string;

  @property({
    type: 'number',
    required: true,
    jsonSchema: {
      minimum: 0,
      title: 'Target Raise Amount',
      description: 'The Amount being raised for the deal, priced in US dollars',
    },
  })
  @importable({
    displayName: 'Target Raise Amount',
    description: 'The Amount being raised for the deal',
  })
  targetRaiseAmount: number;

  @property({
    type: 'boolean',
    default: () => false,
    jsonSchema: {
      title: 'Is Disabled',
      description: 'A disabled deal cannot take investments, be closed, etc.',
    },
  })
  @importable({
    displayName: 'Disabled',
    description: 'Whether this deal is disabled',
  })
  disabled: boolean;

  @property({
    type: 'date',
    jsonSchema: {
      title: 'Estimated Close Date',
      description: 'Date this deal is expected to close',
    },
  })
  @importable({
    displayName: 'Estimated Close Date',
    description: 'Estimated date this Deal will close',
  })
  estimatedCloseDate?: Date;

  constructor(data?: Partial<DealBase>) {
    super(data);
  }
}

@model({})
export class Deal extends DealBase {
  constructor(data?: Partial<Deal>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Deal ID
   */
  @property({
    type: 'string',
    id: true,
    mongodb: {dataType: 'ObjectId'},
  })
  @importable({
    displayName: 'ID',
  })
  id: string;

  /**
   * GBT Role
   * TODO (Andrew): I commented this out (10/26), it isn't used anywhere, let's remove after a while
   */
  //@property({
  //  type: 'string',
  //})
  //@importable({
  //  displayName: 'GBT Role',
  //})
  //gbtRole?: string;

  @belongsTo(() => User)
  ownerId: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * Entity ID
   */
  @importable({
    displayName: 'Entity ID',
  })
  @belongsTo(() => Entities)
  entityId?: string;

  /**
   * Asset ID
   */
  @importable({
    displayName: 'Asset ID',
  })
  @property.array('string')
  assetIds?: string[];

  /**
   * The marketing information
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        logo: {type: 'string'},
        tagline: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Marketing Information',
    description: 'Marketing Information for the Deal',
  })
  marketing?: {
    logo?: string;
    tagline?: string;
  };

  /**
   * The name of the portfolio company
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Public Portfolio Company Name',
    description: 'A Public Portfolio name for the Company',
  })
  public portfolioCompanyName: string;

  /**
   * The name of the portfolio company state of formation
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Portfolio Company State',
    description: 'The state the Portfolio company is in',
  })
  public portfolioCompanyState: string;

  /**
   * The entity type of the portfolio company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: [
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
        'S_CORPORATION',
        'GENERAL_PARTNERSHIP',
        'FOREIGN_ENTITY',
      ],
    },
  })
  @importable({
    displayName: 'Portfolio Company Entity',
    description: 'The Entity Type of the Portfolio Company',
  })
  public portfolioCompanyEntity:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION'
    | 'S_CORPORATION'
    | 'GENERAL_PARTNERSHIP'
    | 'FOREIGN_ENTITY';

  /**
   * Minimum investment amount for the deal
   */
  @property({
    type: 'number',
    required: true,
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Minimum Investment Amount',
  })
  minInvestmentAmount: number;

  /**
   * Amount previously raised for the deal
   */
  @property({
    type: 'number',
    jsonschema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Previously Raised Amount',
    description: 'The amount previously raised for this Deal',
  })
  previouslyRaisedAmount?: number;

  /**
   * Security Type
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: [
        'COMMON_STOCK',
        'PREFERRED_STOCK',
        'CONVERTIBLE_PROMISSORY_NOTE',
        'SAFE',
        'SAFT',
        'SAFE-T',
        'LLC_MEMBERSHIP_UNITS',
        'LP_MEMBERSHIP_UNITS',
        'CRYPTOCURENCY',
        'OTHER',
      ],
    },
  })
  @importable({
    displayName: 'Security Type',
    description: 'The type of security for this Deal',
  })
  securityType?:
    | 'COMMON_STOCK'
    | 'PREFERRED_STOCK'
    | 'CONVERTIBLE_PROMISSORY_NOTE'
    | 'SAFE'
    | 'SAFT'
    | 'SAFE-T'
    | 'LLC_MEMBERSHIP_UNITS'
    | 'LP_MEMBERSHIP_UNITS'
    | 'CRYPTOCURENCY'
    | 'OTHER';

  /**
   * Is Public
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  @importable({
    displayName: 'Is Public',
    description: 'Whether this deal is publicly available or not',
  })
  isPublic: boolean;

  /**
   * File
   */
  @property.array('string')
  @importable({
    displayName: 'Files',
    description: 'Investor accessible Files for this Deal',
  })
  files?: string[];

  /**
   * Organizer Files
   */
  @property.array('string')
  @importable({
    displayName: 'Files',
    description: 'Organizer Only accessible Files for this Deal',
  })
  organizerFiles?: string[];

  /**
   * The carry percentage for the organizer
   */
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
      maximum: 100,
    },
  })
  @importable({
    displayName: 'Organizer Carry Percentage',
    description: 'The Carry Percentage for the Organizer of the Deal',
  })
  organizerCarryPercentage?: number;

  /**
   * The Additional Carry Recipients for the Deal
   */
  @property.array('object', {
    jsonSchema: {
      type: 'object',
      properties: {
        individual: {
          type: 'object',
          properties: getJsonSchema(Individual).properties,
        },
        carryPercentage: {
          type: 'number',
        },
      },
    },
  })
  @importable({
    displayName: 'Additional Carry Recipients',
    description: 'Additional Carry Recipients for the Deal',
  })
  additionalCarryRecipients?: {
    individual: Individual;
    carryPercentage: number;
  }[];

  /**
   * Contact for the portfolio company
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['firstName', 'lastName', 'email'],
      patternProperties: {
        firstName: {type: 'string', minLength: 2, maxLength: 1024},
        lastName: {type: 'string', minLength: 2, maxLength: 1024},
        email: {type: 'string', minLength: 2, maxLength: 1024},
      },
    },
  })
  @importable({
    displayName: 'Portfolio Company Contact',
    description: 'The Contact for the Portfolio Company',
  })
  portfolioCompanyContact?: {
    firstName?: string;
    lastName?: string;
    email?: string;
  };

  /**
   * The status of the deals progress
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['DRAFT', 'OPEN', 'IN CLOSING', 'CLOSED'],
    },
  })
  @importable({
    displayName: 'Deal AccreditationStatus',
    description: 'The current status of the deal',
  })
  status: 'DRAFT' | 'OPEN' | 'IN CLOSING' | 'CLOSED';

  /**
   * Closes
   */
  @property.array('string')
  @importable({
    displayName: 'Closes',
    description: 'Closes for this deal',
  })
  closes?: string[];

  @property({
    type: 'boolean',
    default: () => false,
  })
  @importable({
    displayName: 'Require Qualified Purchaser',
    description: 'Whether this deal requires qualified purchasers',
  })
  requireQualifiedPurchaser: boolean;

  /**
   * The date the deal was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the deal was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * URL for the deal on owners website
   */
  @property({
    type: 'string',
  })
  ownersDealUrl?: string;

  /**
   * The date a deal was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * The date a deal was deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  @belongsTo(() => Profile)
  profileId: string;

  @hasMany(() => User, {keyTo: 'id'})
  coOwnerIds?: string[];

  @property()
  additionalProperties?: {};

  @hasOne(() => Distribution)
  distribution?: Distribution;

  @belongsTo(
    () => Package,
    {name: 'package'},
    {name: 'packageId', jsonSchema: {nullable: true}},
  )
  packageId?: string;

  @hasMany(() => Transaction)
  transactions: Transaction[];
}

export interface DealRelations {
  // describe navigational properties here
}

export type DealWithRelations = Deal & DealRelations;

// DTO
export const DealCreate = defineModelClass<typeof Deal, {}>(
  Deal,
  new ModelDefinition('dealCreate'),
);

const DefineDealEdit = async () => {
  // TODO(Andrew): wrap this mad science into something, service?
  const ds = new MongodbDataSource();
  const packageRepository = new DefaultCrudRepository(Package, ds);
  const pk = await packageRepository.find();
  return new ModelDefinition('dealUpdate')
    .addProperty('id', {
      type: 'string',
      jsonSchema: {readOnly: true},
    })
    .addProperty('packageId', {
      type: 'string',
      jsonSchema: {
        title: 'Package',
        description: 'Product and attribute package assigned to this deal',
        enum: [null, ..._.map(pk, 'id')],
        enumNames: ['<Unset>', ..._.map(pk, 'name')],
        nullable: true,
      },
    });
};
export const DealUpdate = async () =>
  defineModelClass<typeof DealBase, {id: string}>(
    DealBase,
    await DefineDealEdit(),
  );

// SCHEMA
export const DealSchema = getJsonSchemaRef(Deal);

export const DealCreateSchema = getJsonSchemaRef(DealCreate, {
  title: 'Create a New Deal',
  exclude: ['id'],
});
export const DealUpdateSchema = async () =>
  getJsonSchemaRef(await DealUpdate(), {title: 'Edit a Deal'});
