/* eslint-disable @typescript-eslint/no-explicit-any */
import {Entity, model, property, belongsTo} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Entities} from './entities.model';
import {Deal} from './deal.model';

/**
 * The model class is generated from OpenAPI schema - Close
 * The closes object represents the closing of the deal/entity.
 */
@model({
  name: 'Close',
  settings: {
    hiddenProperties: ['ownerId', 'isDeleted', 'deletedAt'],
  },
})
export class Close extends Entity {
  constructor(data?: Partial<Close>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Close ID
   */
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
  })
  @importable({
    displayName: 'ID',
  })
  id: string;

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * Organizer Signature
   */
  @property({type: 'string'})
  organizerSignature?: string;

  /**
   * Manager Signature
   */
  @property({type: 'string'})
  fundManagerSignature?: string;

  /**
   * File
   */
  @property.array('string')
  @importable({
    displayName: 'Close Files',
  })
  files?: string[];

  /**
   * The date the deal closed
   */
  @property({
    type: 'date',
  })
  @importable({
    displayName: 'Close Date',
  })
  dateClosed?: Date;

  /**
   * The Date the Funds were Wired
   */
  @property({
    type: 'date',
  })
  @importable({
    displayName: 'Funds Wired Date',
  })
  fundsWiredDate?: Date;

  /**
   * If the close is ready to counter sign
   */
  @property({type: 'boolean'})
  readyToCounterSign?: boolean;

  /**
   * If the close is ready to counter sign
   */
  @property({type: 'boolean', default: () => false})
  fundManagerSigned?: boolean;

  /**
   * If the close needs approval
   */
  @property({type: 'boolean', default: () => false})
  needsApproval: boolean;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        filedDate: {type: 'object'},
      },
    },
  })
  @importable({
    displayName: 'Blue Sky Filing Data',
    description: 'Information about the BlueSky filing',
  })
  blueSky?: {
    filedDate?: Date;
  };

  @property({
    type: 'date',
  })
  @importable({
    displayName: 'Purchase Docs Signed Date',
  })
  purchaseDocsSignedDate?: Date;

  @property({
    type: 'object',
  })
  statement?: object;

  /**
   * The date the deal object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The sequence of the close
   */
  @property({
    type: 'number',
  })
  sequence?: number;

  @property()
  status?: any;

  /**
   * The date the deal object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt: Date;

  @property({type: 'string'})
  feesPaymentTransferId?: string;

  @property({type: 'string'})
  blueSkyPaymentTransferId?: string;

  /**
   * The date the deal object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt?: Date;

  @belongsTo(() => Entities)
  entityId: string;

  @belongsTo(() => Deal)
  dealId?: string;

  /**
   * Subscription Ids for all documents in the close
   */
  //  @property.array(String)
  //  subscriptions?: string[];
  @property.array(String)
  subscriptions?: string[];

  /**
   * Whether or not the deal object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted?: boolean;
}

export interface CloseRelations {
  // describe navigational properties here
}

export type CloseWithRelations = Close & CloseRelations;
