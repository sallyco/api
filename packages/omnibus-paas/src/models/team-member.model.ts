import {Entity, model, property} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - Close
 * The closes object represents the closing of the deal/entity.
 */
@model({
  name: 'TeamMember',
})
export class TeamMember extends Entity {
  constructor(data?: Partial<TeamMember>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   *firstName
   */
  @property({type: 'string'})
  name: string;

  /**
   *Role
   */
  @property({type: 'string'})
  role: string;

  /**
   *linkedinUrl
   */
  @property({type: 'string'})
  linkedInUrl: string;

  /**
   *
   */
  @property({type: 'string'})
  image: string;
}

export interface TeamMemberRelations {
  // describe navigational properties here
}

export type TeamMemberRelationsWithRelations = TeamMember & TeamMemberRelations;
