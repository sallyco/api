/* eslint-disable @typescript-eslint/no-explicit-any */
import {Entity, model, property} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - Profile
 * A representation of a profile
 */
@model({
  name: 'InviteItem',
  settings: {
    strict: false,
  },
})
export class InviteItem extends Entity {
  constructor(data?: Partial<InviteItem>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Profile ID
   */
  @property({type: 'string', id: true, generated: true})
  _id: string;

  /**
   * The first name of the inviteItem
   */
  @property({
    type: 'string',
  })
  'id': string;

  /**
   * The last name of the inviteItem
   */
  @property({
    type: 'string',
  })
  'type': string;

  /**
   * The title of the inviteItem
   */
  @property({
    type: 'string',
  })
  'relation': string;

  [prop: string]: any;
}

export interface InviteItemRelations {
  // describe navigational properties here
}

export type InviteItemWithRelations = InviteItem & InviteItemRelations;
