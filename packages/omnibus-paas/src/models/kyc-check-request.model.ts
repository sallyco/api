import {Model, model} from '@loopback/repository';
import {getNames} from 'i18n-iso-countries';
import USregions from 'iso3166-2-db/regions/US/dispute/UN/en.json';

@model({
  settings: {
    strict: true,
  },
  jsonSchema: {
    additionalProperties: true,
    description: 'KYC Check Request Object',
    oneOf: [
      {
        title: 'Data to KYC Check',
        type: 'object',
        required: [
          'firstName',
          'lastName',
          'address',
          'identificationNumbers',
          'dateOfBirth',
        ],
        properties: {
          firstName: {
            type: 'string',
            minLength: 2,
            maxLength: 1024,
            description: "The applicant's first name.",
          },
          lastName: {
            type: 'string',
            minLength: 2,
            maxLength: 1024,
            description: "The applicant's surname.",
          },
          address: {
            description:
              'The address of the applicant\n\n' +
              'The following characters will be filtered from all fields: `!$%^*=<>`',
            required: ['street1', 'city', 'postalCode', 'region', 'country'],
            properties: {
              street1: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
                description: 'First line of address',
              },
              street2: {
                type: 'string',
                maxLength: 1024,
                description: 'Second line of address',
              },
              city: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
                description: 'City of address',
              },
              region: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
                description: 'Region or State of Address',
              },
              postalCode: {
                type: 'string',
                minLength: 2,
                maxLength: 1024,
                description:
                  "The postcode (ZIP code) of the applicant's address.\n\n" +
                  '- For UK postcodes, specify the value in the following format: `SW4 6EH`.\n' +
                  '- For US postcodes, use ZIP format: `84121`',
              },
              country: {
                description: 'The full name of the country',
                type: 'string',
                enum: [...Object.values(getNames('en'))],
              },
            },
          },

          email: {
            type: 'string',
            description: "The applicant's email address.",
          },

          dateOfBirth: {
            type: 'string',
            format: 'date',
            description: "The applicant's date of birth in YYYY-MM-DD format.",
          },

          identificationNumbers: {
            type: 'array',
            minItems: 1,
            items: {
              type: 'object',
              required: ['type', 'value'],
              properties: {
                type: {
                  type: 'string',
                  enum: [
                    'ssn',
                    'social_insurance',
                    'tax_id',
                    'identity_card',
                    'driving_license',
                  ],
                  description:
                    'Type of ID number. Valid values are `ssn`, `social_insurance` (e.g. UK National Insurance), `tax_id`, `identity_card` and `driving_licence`.',
                },
                value: {
                  type: 'string',
                  minLength: 4,
                  description:
                    'Value of `type`. \n' +
                    'Type of `ssn` supports both the full SSN or the last 4 digits. If the full SSN is provided then it must be in the format xxx-xx-xxxx.',
                },
                state: {
                  type: 'string',
                  enum: [...USregions.map(item => item.iso)],
                  description:
                    'Only required for the `driving_licence` type in United States of America',
                },
              },
            },
          },
        },
      },
      {
        title: 'Profile to KYC Check',
        type: 'object',
        required: ['profileId'],
        properties: {
          profileId: {
            type: 'string',
          },
        },
      },
    ],
  },
})
export class KycCheckRequest extends Model {
  profileId?: string;

  firstName?: string;

  lastName?: string;

  email?: string;

  address?: {
    street1: string;
    street2?: string;
    city: string;
    region: string;
    postalCode: string;
    country: string;
  };

  dateOfBirth?: string;

  identificationNumbers?: {
    type:
      | 'ssn'
      | 'social_insurance'
      | 'tax_id'
      | 'identity_card'
      | 'driving_license';
    value: string;
    state?: string;
  }[];

  constructor(data?: Partial<KycCheckRequest>) {
    super(data);
  }
}

export interface KycCheckRequestRelations {
  // describe navigational properties here
}

export type KycCheckRequestWithRelations = KycCheckRequest &
  KycCheckRequestRelations;
