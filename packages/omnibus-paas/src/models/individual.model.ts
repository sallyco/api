import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';

/**
 * The model class is generated from OpenAPI schema - Individual
 * A representation of an individual
 */
@model({
  name: 'Individual',
  jsonSchema: {
    description: 'An Individual',
  },
})
export class Individual extends Entity {
  constructor(data?: Partial<Individual>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Individual ID
   */
  @property({
    type: 'string',
    id: true,
  })
  @importable({
    displayName: 'ID',
  })
  id: string;

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId?: string;

  /**
   * The Name of the Individual Entity or Person
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  name?: string;

  /**
   * The Type of Individual
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  type?: string;

  /**
   * The title of the signer
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  title?: string;

  /**
   * Whether the signer is a us person or not
   */
  @property({type: 'boolean'})
  isUSBased?: boolean;

  /**
   * The address of the profile
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['address1', 'city', 'postalCode', 'country'],
      properties: {
        address1: {type: 'string', minLength: 2, maxLength: 1024},
        address2: {type: 'string', maxLength: 1024},
        city: {type: 'string', minLength: 2, maxLength: 1024},
        state: {type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {type: 'string', minLength: 2, maxLength: 1024},
        country: {type: 'string', minLength: 2, maxLength: 1024},
      },
    },
  })
  address?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };

  /**
   * The phone number of the signer
   */
  @property()
  phone?: string;

  /**
   * The email of the signer
   */
  @property()
  email?: string;

  /**
   * The social security of the profile owner
   */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['type', 'value'],
      properties: {
        type: {type: 'string', enum: ['ssn', 'itin', 'ftin']},
        value: {type: 'string'},
      },
    },
  })
  taxDetails?: {
    type?: string;
    value?: string;
  };

  /**
   * The date of birth of the Individual
   */
  @property({
    type: 'string',
    title: 'Date of Birth',
    description: 'The Date of Birth for this individual',
    example: '1989-02-22',
  })
  dateOfBirth?: string;

  /**
   * The stateOfFormation
   */
  @property({type: 'string'})
  stateOfFormation?: string;

  /**
   * The Country of Formation
   */
  @property({type: 'string'})
  countryOfFormation?: string;

  /**
   * The Tax ID Type
   */
  @property({type: 'string'})
  taxIdType?: string;

  /**
   * The Tax ID
   */
  @property({type: 'string'})
  taxId?: string;
}

export interface IndividualRelations {
  // describe navigational properties here
}

export type IndividualWithRelations = Individual & IndividualRelations;
