import {Entity, model, property} from '@loopback/repository';

@model()
export class PackageProduct extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  packageId?: string;

  @property({
    type: 'string',
  })
  productId?: string;

  constructor(data?: Partial<PackageProduct>) {
    super(data);
  }
}

export interface PackageProductRelations {
  // describe navigational properties here
}

export type PackageProductWithRelations = PackageProduct &
  PackageProductRelations;
