import {model, property} from '@loopback/repository';
import {Webhook} from './webhook.model';

/**
 * The model class is generated from OpenAPI schema - WebhooksList
 * A list of webhooks.
 */
@model({name: 'WebhooksList'})
export class WebhooksList {
  constructor(data?: Partial<WebhooksList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Webbhook array
   */
  @property.array(Webhook)
  data?: Webhook[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  limit?: number;

  /**
   * The total number of results
   */
  @property()
  total_count?: number;
}

export interface WebhooksListRelations {
  // describe navigational properties here
}

export type WebhooksListWithRelations = WebhooksList & WebhooksListRelations;
