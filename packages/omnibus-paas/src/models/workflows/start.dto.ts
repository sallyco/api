import {Model, model, property} from '@loopback/repository';

@model({
  title: 'StartWorkflow',
  description: 'Start a new Workflow',
})
export class Start extends Model {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Business Key',
      description: 'This identifies the unique instance of this process',
    },
  })
  businessKey: string;

  constructor(data?: Partial<Start>) {
    super(data);
  }
}
