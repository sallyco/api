import {Entity, model, property} from '@loopback/repository';

@model({
  title: 'BankAccount',
  description: 'A bank account for financial transactions',
})
export class BankAccount extends Entity {
  @property({
    type: 'string',
  })
  status?: 'OPEN' | 'PENDING' | 'CLOSED';

  constructor(data?: Partial<BankAccount>) {
    super(data);
  }
}
