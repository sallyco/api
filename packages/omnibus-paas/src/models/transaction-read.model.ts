import {Model, model, property} from '@loopback/repository';

@model({
  name: 'TransactionRead',
})
export class TransactionRead extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  date: string;

  @property({
    type: 'string',
  })
  desc: string;

  @property({
    type: 'string',
  })
  wire: string;

  @property({
    type: 'string',
  })
  ach_id: string;

  @property({
    type: 'string',
  })
  summary: string;

  @property({
    type: 'number',
    required: true,
  })
  amount: number;

  @property({
    type: 'number',
    required: true,
  })
  balance: number;

  @property({
    type: 'string',
  })
  type: string;

  @property({
    type: 'string',
  })
  transaction_data: string;

  @property({
    type: 'string',
  })
  consolidated_description: string;

  @property({
    type: 'object',
  })
  providerMeta?: object;

  @property({
    type: 'string',
  })
  transaction_type: string;

  @property({
    type: 'string',
  })
  subscription_id: string;

  @property({
    type: 'string',
  })
  investor_name: string;

  constructor(data?: Partial<TransactionRead>) {
    super(data);
  }
}

export interface TransactionReadRelations {
  // describe navigational properties here
}

export type TransactionReadWithRelations = TransactionRead &
  TransactionReadRelations;
