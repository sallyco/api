import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Realm extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  tenantId?: string;
  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Realm>) {
    super(data);
  }
}

export interface RealmRelations {
  // describe navigational properties here
}

export type RealmWithRelations = Realm & RealmRelations;
