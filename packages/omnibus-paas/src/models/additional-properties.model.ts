import {model} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - AdditionalProperties
 * Additional Properties
 */
@model({name: 'AdditionalProperties'})
export class AdditionalProperties {
  constructor(data?: Partial<AdditionalProperties>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }
}

export interface AdditionalPropertiesRelations {
  // describe navigational properties here
}

export type AdditionalPropertiesWithRelations = AdditionalProperties &
  AdditionalPropertiesRelations;
