import {model, property} from '@loopback/repository';
import {Invite} from './invite.model';

/**
 * The model class is generated from OpenAPI schema - DealsList
 * List of deals
 */
@model({name: 'InviteList'})
export class InviteList {
  constructor(data?: Partial<InviteList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Invites
   */
  @property.array(Invite)
  data?: Invite[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface InviteListRelations {
  // describe navigational properties here
}

export type InviteListWithRelations = InviteList & InviteListRelations;
