import {model, property} from '@loopback/repository';
import {Company} from './company.model';

/**
 * The model class is generated from OpenAPI schema - CompaniesList
 * List of Companies
 */
@model({name: 'CompaniesList'})
export class CompaniesList {
  constructor(data?: Partial<CompaniesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Companies
   */
  @property.array(Company)
  data?: Company[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface CompaniesListRelations {
  // describe navigational properties here
}

export type CompaniestWithRelations = CompaniesList & CompaniesListRelations;
