import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';

@model({
  name: 'Update File',
})
export class FileUpdate extends Entity {
  constructor(data?: Partial<FileUpdate>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * User ID
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Owner ID',
  })
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Tenant ID',
  })
  tenantId?: string;

  /**
   * The name of the file
   */
  @property({
    type: 'string',
    required: false,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Name of the File',
  })
  name: string;

  /**
   * The type of the file
   */
  @property({
    type: 'string',
    required: false,
  })
  @importable({
    displayName: 'Type',
  })
  type: string;

  /**
   * The S3 key for the file
   */
  @property({
    type: 'string',
    required: false,
  })
  @importable({
    displayName: 'File Key or URL',
    description: 'File Key or URL',
  })
  key: string;

  /**
   * The thumbnail
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Thumbnail of the file',
  })
  thumbnail?: string;

  /**
   * Whether or not the file object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  @importable({
    displayName: 'File is Deleted',
    description: 'File is Deleted',
  })
  isDeleted: boolean;

  /**
   * Whether or not the file has been generated
   */
  @property({
    type: 'boolean',
    required: false,
    default: () => false,
  })
  @importable({
    displayName: 'File is Generating',
    description: 'File is Generating',
  })
  generating: boolean;

  /**
   * Whether or not the file has been generated
   */
  @property({
    type: 'boolean',
    required: false,
    default: () => false,
  })
  @importable({
    displayName: 'File is Public',
    description: 'File is Public',
  })
  isPublic: boolean;
}
