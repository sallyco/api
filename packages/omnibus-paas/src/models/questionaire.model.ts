import {getJsonSchemaRef} from '@loopback/rest';
import {
  DefaultCrudRepository,
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
  hasMany,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Question, QuestionWithRelations} from './question.model';
import {QuestionaireQuestion} from './questionaire-question.model';
import _ from 'lodash';

@model({
  title: 'Questionaire',
  description: `
Questionaire Specification
`,
})
class QuestionaireBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Questionaire Name',
      description: 'Name to identify this questionaire',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Questionaire Description',
      description:
        'Description for additional specification or clarification of the questionaire and its usage',
      maxLength: 1024,
    },
  })
  description: string;

  constructor(data?: Partial<Questionaire>) {
    super(data);
  }
}

@model()
export class Questionaire extends QuestionaireBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id?: string;

  @hasMany(() => Question, {through: {model: () => QuestionaireQuestion}})
  questions: Question[];

  constructor(data?: Partial<Questionaire>) {
    super(data);
  }
}

export interface QuestionaireRelations {
  questions: QuestionWithRelations[];
}
export type QuestionaireWithRelations = Questionaire & QuestionaireRelations;

// DTO
const DefineQuestionaireUpdateEdit = async () => {
  const ds = new MongodbDataSource();

  const questionRepository = new DefaultCrudRepository(Question, ds);
  const qs = await questionRepository.find();

  return new ModelDefinition('questionaireCreateUpdate').addProperty(
    'questions',
    {
      type: 'array',
      jsonSchema: {
        title: 'Questions',
        description: 'Questions contained in this questionaire',
        items: {
          type: 'string',
          enum: _.map(qs, 'id'),
          enumNames: _.map(qs, 'name'),
        },
        minItems: 0,
      },
    },
  );
};

export const QuestionaireCreate = async () =>
  defineModelClass<typeof QuestionaireBase, {questions: []; attributes: []}>(
    QuestionaireBase,
    await DefineQuestionaireUpdateEdit(),
  );

export const QuestionaireUpdate = async () => {
  const dpue = await DefineQuestionaireUpdateEdit();
  return defineModelClass<
    typeof QuestionaireBase,
    {id: string; questions: []; attributes: []}
  >(
    QuestionaireBase,
    dpue.addProperty('id', {
      type: 'string',
      jsonSchema: {readOnly: true},
    }),
  );
};

// SCHEMA
export const QuestionaireSchema = getJsonSchemaRef(Questionaire);

export const QuestionaireCreateSchema = async () =>
  getJsonSchemaRef(await QuestionaireCreate(), {
    title: 'Create a New Questionaire',
    exclude: ['id'],
  });

export const QuestionaireUpdateSchema = async () =>
  getJsonSchemaRef(await QuestionaireUpdate(), {title: 'Edit a Questionaire'});
