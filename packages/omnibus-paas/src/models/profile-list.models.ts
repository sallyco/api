import {model, property} from '@loopback/repository';
import {Profile} from './profile.model';

/**
 * The model class is generated from OpenAPI schema - ProfileList
 * List of Profiles
 */
@model({name: 'ProfilesList'})
export class ProfilesList {
  constructor(data?: Partial<ProfilesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Profiles
   */
  @property.array(Profile)
  data?: Profile[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface ProfilesListRelations {
  // describe navigational properties here
}

export type ProfilesListWithRelations = ProfilesList & ProfilesListRelations;
