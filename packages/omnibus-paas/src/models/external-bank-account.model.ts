import {Model, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class ExternalBankAccount extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  counterpartyId: string;

  @property({
    type: 'object',
  })
  providerMeta?: {
    typeId?: string;
    accountStatus?: string;
    accountApplicationId?: string;
    accountId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;
  };

  @property({
    type: 'string',
  })
  bankName?: string;

  @property({
    type: 'string',
  })
  accountNumber?: string;

  @property({
    type: 'string',
  })
  routingNumber?: string;

  @property({
    type: 'string',
  })
  accountName?: string;

  @property({
    type: 'boolean',
    default: () => false,
  })
  isDefaultForDistributions?: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ExternalBankAccount>) {
    super(data);
  }
}

export interface ExternalBankAccountRelations {
  // describe navigational properties here
}

export type ExternalBankAccountWithRelations = ExternalBankAccount &
  ExternalBankAccountRelations;
