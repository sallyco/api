import {Model, model, property} from '@loopback/repository';

@model({
  name: 'Create Tenant',
  settings: {
    strict: 'filter',
  },
})
export class TenantCreate extends Model {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Tenant id',
      description:
        'Lowercase string to identify tenant, used in URLs and other reference',
    },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Name',
    },
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'URL',
      description: "URL to tenant's marketing or other public website",
    },
  })
  url: string;

  @property({
    type: 'object',
    jsonSchema: {
      required: ['address', 'city', 'postalCode', 'state'],
      properties: {
        address: {
          title: 'Address',
          type: 'string',
          minLength: 2,
          description: 'Address of Tenant',
        },
        city: {
          title: 'City',
          type: 'string',
          description: 'City of Tenant',
        },
        state: {
          title: 'State',
          type: 'string',
          description: 'State of Tenant',
        },
        postalCode: {
          title: 'Postal Code',
          type: 'string',
          minLength: 2,
          description: 'Postalcode of Tenant',
        },
      },
      title: 'Address',
    },
  })
  address?: {
    address?: string;
    city?: string;
    state?: string;
    postalCode?: string;
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        images: {
          type: 'object',
          properties: {
            logo: {type: 'string', description: 'url of the logo image'},
            logoInverted: {type: 'string'},
          },
          title: 'Images',
          required: ['logo', 'logoInverted'],
        },
        colors: {
          type: 'object',
          properties: {
            primaryColor: {
              title: 'Primary Color',
              type: 'string',
              description: 'Color Hex code',
            },
            secondaryColor: {
              title: 'Secondary Color',
              type: 'string',
              description: 'Color Hex code',
            },
          },
          title: 'Colors',
          required: ['primaryColor', 'secondaryColor'],
        },
      },
      title: 'Assets',
    },
  })
  assets?: {
    images?: {
      logo?: string;
      logoInverted?: string;
    };
    colors?: {
      primaryColor?: string;
      secondaryColor?: string;
    };
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        emails: {
          type: 'object',
          title: 'Email',
          properties: {
            fromAddress: {
              title: 'From Address',
              type: 'string',
              description: 'Users will receive email from this address',
            },
            compliance: {
              title: 'Compliance Address',
              type: 'string',
              description: 'For filing and compliance notifications',
            },
          },
        },
        useOrganizerForEINOrders: {
          type: 'boolean',
          title: "Use Organizer's SSN",
          description: "This will use the organizer's SSN for EIN obtainment",
        },
      },
      title: 'Settings',
    },
  })
  settings?: {
    emails?: {
      fromAddress?: string;
      compliance?: string;
    };
    useOrganizerForEINOrders?: boolean;
  };

  @property({
    type: 'number',
    required: false,
    default: () => 180,
    jsonSchema: {
      title: 'Max Allowed Days for Close',
      description: 'Max allowed days for closing a deal',
    },
  })
  maxAllowedDaysToClose?: number;

  @property({
    type: 'string',
    required: false,
    jsonSchema: {
      title: 'Billing Contact',
      description: 'Billing Contact for the Tenant',
    },
  })
  billingContact?: string;

  constructor(data?: Partial<TenantCreate>) {
    super(data);
  }
}
