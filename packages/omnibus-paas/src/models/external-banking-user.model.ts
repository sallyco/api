import {Model, model, property} from '@loopback/repository';
import {ExternalBankAccount} from './external-bank-account.model';
import {getJsonSchema} from '@loopback/repository-json-schema';

@model({settings: {strict: false}})
export class ExternalBankingUser extends Model {
  @property({
    type: 'string',
  })
  id?: string;

  @property({
    type: 'array',
    itemType: 'object',
    jsonSchema: {
      items: getJsonSchema(ExternalBankAccount),
    },
  })
  accounts?: ExternalBankAccount[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ExternalBankingUser>) {
    super(data);
  }
}

export interface ExternalBankingUserRelations {
  // describe navigational properties here
}

export type ExternalBankingUserWithRelations = ExternalBankingUser &
  ExternalBankingUserRelations;
