import {model, property} from '@loopback/repository';
import {Entities} from './entities.model';

/**
 * The model class is generated from OpenAPI schema - EntitiesList
 * List of entities
 */
@model({name: 'EntitiesList'})
export class EntitiesList {
  constructor(data?: Partial<EntitiesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Entities
   */
  @property.array(Entities)
  data?: Entities[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface EntitiesListRelations {
  // describe navigational properties here
}

export type EntitiesListWithRelations = EntitiesList & EntitiesListRelations;
