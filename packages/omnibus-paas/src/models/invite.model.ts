import {Entity, model, property} from '@loopback/repository';
import {InviteItem} from './invite-item.model';

/**
 * The model class is generated from OpenAPI schema - Invite
 * An invitation for a user to join the platform.
 */
@model({
  name: 'Invite',
  settings: {
    hiddenProperties: ['ownerId', 'tenantId', 'deletedAt'],
  },
})
export class Invite extends Entity {
  constructor(data?: Partial<Invite>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * A unique identifier for this object
   */
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
  })
  id: string;

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * Email address
   */
  @property()
  acceptableBy: string;

  /**
   *
   */
  @property()
  invitedBy: string;

  /**
   * Items included in the invitation
   */
  @property.array(InviteItem)
  invitedTo: InviteItem[];

  /**
   * Invitation role
   */
  @property()
  invitedRole?: string;

  /**
   * Navigation after accept
   */
  @property()
  redirectUrl?: string;

  /**
   * The date and time the invite was accepted
   */
  @property({
    type: 'date',
  })
  acceptedAt?: Date;

  /**
   * The date the deal was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the deal was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * The date a deal was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * The date a deal was deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;
}

export interface InviteRelations {
  // describe navigational properties here
}

export type InviteWithRelations = Invite & InviteRelations;
