import {Entity, model, property} from '@loopback/repository';
import {PurchaseDetails} from './purchase-details.model';
import {DistributionDocuments} from './distribution-documents.model';

@model({
  name: 'Distribution',
})
export class Distribution extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({type: 'string'})
  ownerId?: string;

  @property({type: 'string'})
  tenantId?: string;

  @property({
    type: 'object',
    required: true,
  })
  docs: DistributionDocuments;

  @property({
    type: 'object',
    required: true,
  })
  purchaseDetails: PurchaseDetails;

  @property({
    type: 'string',
  })
  dealId: string;

  constructor(data?: Partial<Distribution>) {
    super(data);
  }
}

export interface DistributionRelations {
  // describe navigational properties here
}

export type DistributionWithRelations = Distribution & DistributionRelations;
