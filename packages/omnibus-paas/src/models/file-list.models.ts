import {model, property} from '@loopback/repository';
import {File} from './file.model';

/**
 * The model class is generated from OpenAPI schema - FilesList
 * List of files
 */
@model({name: 'FilesList'})
export class FilesList {
  constructor(data?: Partial<FilesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Files
   */
  @property.array(File)
  data?: File[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface FilesListRelations {
  // describe navigational properties here
}

export type FilesListWithRelations = FilesList & FilesListRelations;
