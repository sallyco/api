import {Entity, model, property} from '@loopback/repository';

@model()
export class QuestionaireQuestion extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  packageId?: string;

  @property({
    type: 'string',
  })
  productId?: string;

  constructor(data?: Partial<QuestionaireQuestion>) {
    super(data);
  }
}

export interface QuestionaireQuestionRelations {
  // describe navigational properties here
}

export type QuestionaireQuestionWithRelations = QuestionaireQuestion &
  QuestionaireQuestionRelations;
