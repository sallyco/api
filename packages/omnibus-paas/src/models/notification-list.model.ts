import {model, property} from '@loopback/repository';
import {Notification} from './notification.model';

/**
 * The model class is generated from OpenAPI schema - NotificationsList
 * List of notifications
 */
@model({name: 'NotificationsList'})
export class NotificationsList {
  constructor(data?: Partial<NotificationsList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Notifications
   */
  @property.array(Notification)
  data?: Notification[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface NotificationsListRelations {
  // describe navigational properties here
}

export type NotificationsListWithRelations = NotificationsList &
  NotificationsListRelations;
