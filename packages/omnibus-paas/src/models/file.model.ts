import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';

/**
 * The model class is generated from OpenAPI schema - File
 * A representation of a file
 */
@model({
  name: 'File',
  settings: {
    hiddenProperties: ['ownerId', 'isDeleted', 'deletedAt'],
  },
})
export class File extends Entity {
  constructor(data?: Partial<File>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * File ID
   */
  @property({
    type: 'string',
    id: true,
    mongodb: {dataType: 'ObjectId'},
  })
  @importable({
    displayName: 'ID',
  })
  id: string;

  /**
   * User ID
   */
  @property({
    type: 'string',
  })
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * The name of the file
   */
  @property({
    type: 'string',
    required: false,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
  })
  name: string;

  /**
   * The size of the file
   */
  @property({
    type: 'string',
  })
  size?: string;

  /**
   * The type of the file
   */
  @property({
    type: 'string',
    required: false,
  })
  @importable({
    displayName: 'Type',
  })
  type: string;

  /**
   * The S3 key for the file
   */
  @property({
    type: 'string',
    required: false,
  })
  @importable({
    displayName: 'File Key or URL',
  })
  key: string;

  /**
   * The thumbnail
   */
  @property({
    type: 'string',
  })
  thumbnail?: string;

  /**
   * The number of milliseconds since the Unix epoch
   */
  @property({
    type: 'string',
  })
  lastModified?: string;

  /**
   * The date the file object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the file object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * The date the file object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * Whether or not the file object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  /**
   * Whether or not the file has been generated
   */
  @property({
    type: 'boolean',
    required: false,
    default: () => false,
  })
  generating: boolean;

  /**
   * Whether or not the file has been generated
   */
  @property({
    type: 'boolean',
    required: false,
    default: () => false,
  })
  isPublic: boolean;

  /**
   * Additional information for the entity
   */
  @property()
  additionalProperties?: {};
}

export interface FileRelations {
  // describe navigational properties here
}

export type FileWithRelations = File & FileRelations;
