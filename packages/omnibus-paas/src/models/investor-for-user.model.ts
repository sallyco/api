import {model, property} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - InvestorForUser
 */
@model({name: 'InvestorForUser'})
export class InvestorForUser {
  @property()
  organizerId: string;

  @property()
  investorId: string;

  @property()
  investorName: string;

  @property()
  investorEmail: string;

  @property()
  investorType?: string;

  @property()
  entityName?: string;

  @property()
  subscriptionCount: number;

  @property()
  amountSubscribed: number;
}

export interface InvestorForUserRelations {
  // describe navigational properties here
}

export type InvestorForUserWithRelations = InvestorForUser &
  InvestorForUserRelations;
