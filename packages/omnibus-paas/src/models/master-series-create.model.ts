import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Individual} from './individual.model';
@model({
  name: 'Create Master Series',
  jsonSchema: {
    definitions: {
      individual: {
        type: 'object',
        required: ['firstName', 'lastName', 'memberAddress'],
        properties: {
          firstName: {
            title: 'FirstName',
            type: 'string',
            description: 'The FirstName of individual',
          },
          lastName: {
            title: 'LastName',
            type: 'string',
            description: 'The LastName of individual',
          },
          memberAddress: {
            title: 'Member/Manager Address',
            type: 'string',
            description: 'The Member/Manager Address of individual',
          },
          isManager: {
            title: 'IsManager?',
            type: 'boolean',
            description: 'Is this owner also a manager?',
          },
          percentageOwnership: {
            title: '% Ownership of member?',
            type: 'number',
            description: '% Ownership of member',
          },
          capitalContribution: {
            title: 'Capital contribution',
            type: 'number',
            description: 'Capital contribution for Member',
          },
        },
        title: 'Individual Details',
      },
      business: {
        type: 'object',
        required: ['businessName', 'address'],
        properties: {
          businessName: {
            title: 'Business Name',
            type: 'string',
            description: 'The Name of business',
          },
          address: {
            title: 'Address',
            type: 'string',
            description: 'Member/Business Address',
          },
          isManager: {
            title: 'IsManager?',
            type: 'boolean',
            description: 'Is this owner also a manager?',
          },
          percentageOwnership: {
            title: '% Ownership of member?',
            type: 'number',
            description: '% Ownership of member',
          },
          capitalContribution: {
            title: 'Capital contribution',
            type: 'number',
            description: 'Capital contribution for Member',
          },
        },
        title: 'Business Details',
      },
    },
    dependencies: {
      individualOrBusiness: {
        oneOf: [
          {
            properties: {
              individualOrBusiness: {enum: ['Individual']},
              individual: {$ref: '#/definitions/individual'},
            },
            required: ['individual'],
          },
          {
            properties: {
              individualOrBusiness: {enum: ['Business']},
              business: {$ref: '#/definitions/business'},
            },
            required: ['business'],
          },
        ],
      },
    },
  },
})
export class MasterSeriesCreate extends Entity {
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 100,
      title: 'Name',
      description: 'Company Name',
    },
  })
  name: string;
  /**
   * The type of the company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['MASTER_ENTITY', 'REGISTERED_AGENT', 'MANAGER'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of company',
  })
  type?: 'MASTER_ENTITY' | 'REGISTERED_AGENT' | 'MANAGER';

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'State of Formation',
  })
  stateOfFormation?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Entity Type',
      description: 'The Entity Type of company',
      enum: ['LLC', 'Corporation', 'Non-Profit', 'LP', 'LLP', 'GP', 'DBA'],
    },
  })
  entitytype?:
    | 'LLC'
    | 'Corporation'
    | 'Non-Profit'
    | 'LP'
    | 'LLP'
    | 'GP'
    | 'DBA';

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Type of Business/Purpose',
      description: 'Type of Business/Purpose (Finance & Insurance)',
    },
  })
  typeOfBusiness?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Type of Business Activity',
      description: 'Type of Business Activity (Finance)',
    },
  })
  typeOfBusinessActivity?: string;

  @property({
    type: 'boolean',
    jsonSchema: {
      title: 'Is this a professional entity?',
    },
  })
  professionalEntity?: boolean;

  @property({
    type: 'object',
    jsonSchema: {
      required: ['address1', 'city', 'postalCode', 'country'],
      properties: {
        address1: {
          title: 'Address1',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
        },
        address2: {title: 'Address2', type: 'string', maxLength: 1024},
        city: {title: 'City', type: 'string', minLength: 2, maxLength: 1024},
        state: {title: 'State', type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {
          title: 'PostalCode',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
        },
        country: {
          title: 'Country',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
        },
      },
      title: 'Address:',
    },
  })
  address?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };

  @property({
    type: 'object',
    jsonSchema: {
      required: [
        'firstname',
        'lastname',
        'phone',
        'address1',
        'city',
        'postalCode',
        'country',
      ],
      properties: {
        firstname: {
          title: 'FirstName',
          type: 'string',
          minLength: 2,
          maxLength: 150,
        },
        lastname: {
          title: 'LastName',
          type: 'string',
          minLength: 2,
          maxLength: 150,
        },
        phone: {
          title: 'Phone Number',
          type: 'string',
          minLength: 12,
          maxLength: 150,
        },
        address1: {title: 'Address1', type: 'string', maxLength: 1024},
        address2: {title: 'Address2', type: 'string', maxLength: 1024},
        city: {title: 'City', type: 'string', minLength: 2, maxLength: 1024},
        state: {title: 'State', type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {
          title: 'PostalCode',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
        },
        country: {
          title: 'Country',
          type: 'string',
          minLength: 2,
          maxLength: 1024,
        },
        taxId: {title: 'TaxId', type: 'string'},
      },
      title: 'Tax contact information:',
    },
  })
  taxContact?: Individual;

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: [
        'Solely by the owner(s) (Member-managed)',
        'Some, but not all, owners (Manager-managed)',
        'By the owner(s) and other managers (Manager-managed)',
        'Solely by the manager(s) (Manager-Managed)',
      ],
      title: 'How Will Be Business Managed?',
      description:
        'How will the business be managed (can have 1-many owners/managers).',
    },
  })
  howWillBeBusinessManaged?:
    | 'Solely by the owner(s) (Member-managed)'
    | 'Some, but not all, owners (Manager-managed)'
    | 'By the owner(s) and other managers (Manager-managed)'
    | 'Solely by the manager(s) (Manager-Managed)';

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      type: 'string',
      enum: ['Individual', 'Business'],
      title: 'Business/Individual',
    },
  })
  individualOrBusiness?: 'Individual' | 'Business';

  constructor(data?: Partial<MasterSeriesCreate>) {
    super(data);
  }
}
