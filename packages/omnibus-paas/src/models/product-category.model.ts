import {getJsonSchemaRef} from '@loopback/rest';
import {
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
} from '@loopback/repository';

@model({
  title: 'Product Category',
  description: `
Generally, product categories group individual products into similar types.  Categories do have specific usage on view and reports that contain categories, e.g. fee and billing statments or invoices. 

For example (categories **_emphasized_**):

| **_SPV Expenses_** | |
|:--- | ---:|
| &nbsp; &nbsp; &nbsp; EIN Obtainment: | $49 |

| **_Tax and Accounting Fees_** | |
|:--- | ---:|
| &nbsp; &nbsp; &nbsp; Tax and K1 Processing: | included |
`,
  settings: {
    strict: 'filter',
  },
})
export class ProductCategory extends Entity {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Category Name',
      description:
        'This is the formal name of the category that appears on statements, invoices, etc.',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Category Description',
      description:
        'Category description for additional specification or clarification of the category and its usage',
      maxLength: 1024,
    },
  })
  description?: string;

  constructor(data?: Partial<ProductCategory>) {
    super(data);
  }
}

export interface ProductCategoryRelations {}

export type ProductCategoryWithRelations = ProductCategory &
  ProductCategoryRelations;

// DTO
export const ProductCategoryCreate = defineModelClass<
  typeof ProductCategory,
  {}
>(ProductCategory, new ModelDefinition('productCategoryCreate'));

export const ProductCategoryUpdate = defineModelClass<
  typeof ProductCategory,
  {id: string}
>(
  ProductCategory,
  new ModelDefinition('productCategoryEdit').addProperty('id', {
    type: 'string',
    jsonSchema: {readOnly: true},
  }),
);

// SCHEMA
export const ProductCategorySchema = getJsonSchemaRef(ProductCategory);

export const ProductCategoryCreateSchema = getJsonSchemaRef(
  ProductCategoryCreate,
  {
    title: 'Create a New Product Category',
    exclude: ['id'],
  },
);
export const ProductCategoryUpdateSchema = getJsonSchemaRef(
  ProductCategoryUpdate,
  {title: 'Edit a Product Category'},
);
