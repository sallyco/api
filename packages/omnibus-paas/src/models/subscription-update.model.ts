import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';

/**
 * The model class is generated from OpenAPI schema - Subscription
 * A representation of a file
 */
@model({
  name: 'SubscriptionUpdate',
})
export class SubscriptionUpdate extends Entity {
  /**
   * The status of the subscription
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'INVITED',
        'PENDING',
        'COMMITTED',
        'COMPLETED',
        'CLOSED',
        'CANCELLED',
        'TRANSFERRED',
        'REFUNDED',
        'FUNDING SENT',
        'FUNDS_IN_TRANSIT-MANUAL',
      ],
    },
    default: () => 'INVITED',
  })
  @importable({
    displayName: 'Status',
    description: 'AccreditationStatus of the Susbscription',
  })
  status:
    | 'INVITED'
    | 'PENDING'
    | 'COMMITTED'
    | 'COMPLETED'
    | 'CLOSED'
    | 'CANCELLED'
    | 'TRANSFERRED'
    | 'REFUNDED'
    | 'FUNDING SENT'
    | 'FUNDS_IN_TRANSIT-MANUAL';

  /**
   * The amount of the subscription
   */
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Amount',
    description: 'Amount the Subscription is for',
  })
  amount?: number;
}
