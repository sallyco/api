import {
  belongsTo,
  Entity,
  model,
  property,
  hasMany,
} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {AccountTransaction} from '../services';
import {ExternalBankAccount} from './external-bank-account.model';
import {Profile, ProfileWithRelations} from './profile.model';
import {Transaction} from './transaction.model';
import {Deal} from './deal.model';
import {User} from './user.model';

/**
 * The model class is generated from OpenAPI schema - Subscription
 * A representation of a file
 */
@model({
  name: 'Subscription',
  settings: {
    hiddenProperties: ['deletedAt'],
    strict: 'filter',
  },
})
export class Subscription extends Entity {
  constructor(data?: Partial<Subscription>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Subscription ID
   */
  @property({
    type: 'string',
    id: true,
    mongodb: {dataType: 'ObjectId'},
  })
  @importable({
    displayName: 'ID',
  })
  id: string;
  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * The id of the associated deal
   */
  @importable({
    displayName: 'Deal ID',
    description: 'ID of the Deal for this Subscription',
  })
  @belongsTo(() => Deal)
  dealId: string;

  /**
   * The name of the individual or entity on the subscription
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Name of the Subscriber',
  })
  name: string;

  /**
   * The email for the primary contact on the subscription
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      pattern: /^\S+@\S+$/.source,
    },
  })
  @importable({
    displayName: 'Email',
    description: 'Email of the Subscriber',
  })
  email: string;

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Phone',
    description: 'Phone number of the subscriber',
  })
  phone?: string;

  /**
   * The type of subscription
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['INDIVIDUAL', 'TRUST', 'ENTITY', 'JOINT'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Subscriber Type',
  })
  type: 'INDIVIDUAL' | 'TRUST' | 'ENTITY' | 'JOINT';

  /**
   * The status of the subscription
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'INVITED',
        'PENDING',
        'COMMITTED',
        'COMPLETED',
        'CLOSED',
        'CANCELLED',
        'TRANSFERRED',
        'REFUNDED',
        'FUNDING SENT',
        'FUNDS_IN_TRANSIT-MANUAL',
      ],
    },
    default: () => 'INVITED',
  })
  @importable({
    displayName: 'Status',
    description: 'AccreditationStatus of the Susbscription',
  })
  status:
    | 'INVITED'
    | 'PENDING'
    | 'COMMITTED'
    | 'COMPLETED'
    | 'CLOSED'
    | 'CANCELLED'
    | 'TRANSFERRED'
    | 'REFUNDED'
    | 'FUNDING SENT'
    | 'FUNDS_IN_TRANSIT-MANUAL';

  /**
   * The amount of the subscription
   */
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  @importable({
    displayName: 'Amount',
    description: 'Amount the Subscription is for',
  })
  amount?: number;

  /**
   * The actual subscription amount received
   */
  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
      maximum: 6000000,
    },
  })
  @importable({
    displayName: 'Reconciled Amount',
    description: 'Amount actually received for this subscription',
  })
  reconciledAmount?: number;

  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  ownershipPercentageAtClose?: number;

  @property({
    type: 'number',
    jsonSchema: {
      minimum: 0,
    },
  })
  ownershipPercentageAtDistribution?: number;

  // DEPRECATED PROPERTY (Needed for initial Migration)
  @property({type: 'string'})
  transactionId: string;

  @property.array('string')
  transactionIds?: string[];

  @property.array('string')
  bankTransactionIds?: string[];

  @property({type: 'string'})
  reverseTransactionId?: string;

  @property.array('string')
  distributionTransactionIds?: string[];

  /**
   * Whether or not the documents have been signed
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDocsSigned: boolean;

  /**
   * Whether or not the documents amount match the field amount
   */
  @property({
    type: 'boolean',
    default: () => true,
  })
  isAmountMatched: boolean;

  /**
   * Whether or the subscriber has passed a KYC/AML check
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isKycAmlPassed: boolean;

  /**
   * The ID for the signing package
   */
  @property({
    type: 'string',
  })
  packageId?: string;

  /**
   * An array of signers for the subscription
   */
  @property.array('object')
  signers?: {
    profileId?: string;
    profileData?: Profile;
    email: string;
    name: string;
    signature?: string;
    dateSigned?: Date;
  }[];

  /**
   * The bank used to send the subscription amount
   */
  @property({
    type: 'object',
  })
  bankAccount?: ExternalBankAccount;

  @property({
    type: 'object',
    jsonSchema: {
      type: 'object',
      properties: {
        bankName: {
          type: 'string',
        },
        isCustomerOfBank: {
          type: 'boolean',
        },
        isFATFBank: {
          type: 'boolean',
        },
      },
    },
  })
  fundingInfo?: {
    bankName: string;
    isCustomerOfBank: boolean;
    isFATFBank: boolean;
  };

  /**
   * The AccountTransaction. Potentially sent from investor but not yet made it into the entity holding account.   */
  @property({
    type: 'object',
  })
  accountTransaction?: AccountTransaction;

  /**
   * The documents for the subscription
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        capitalAccountStatement: {type: 'string'},
        historicalCapitalAccountStatements: {type: 'string'},
        signerOneOASigPage: {type: 'string'},
        signerTwoOASigPage: {type: 'string'},
        investorSASigPage: {type: 'string'},
        signerOneTaxDoc: {type: 'string'},
        signerTwoTaxDoc: {type: 'string'},
        mergedSignerOneOA: {type: 'string'},
        mergedSignerTwoOA: {type: 'string'},
        mergedInvestorSA: {type: 'string'},
        mergedCounterSignedOA: {type: 'string'},
        mergedCounterSignedSA: {type: 'string'},
        taxes: {
          type: 'array',
          items: {
            type: 'object',
            patternProperties: {
              year: {type: 'number'},
              fileId: {type: 'string'},
            },
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Documents',
    description: 'Subscription Documents',
  })
  documents?: {
    capitalAccountStatement?: string;
    historicalCapitalAccountStatements?: string[];
    signerOneOASigPage?: string;
    signerTwoOASigPage?: string;
    investorSASigPage?: string;
    signerOneTaxDoc?: string;
    signerTwoTaxDoc?: string;
    mergedSignerOneOA?: string;
    mergedSignerTwoOA?: string;
    mergedInvestorSA?: string;
    mergedCounterSignedOA?: string;
    mergedCounterSignedSA?: string;
    taxes?: {
      year: number;
      fileId: string;
    }[];
  };

  /**
   * File
   */
  @property.array('string')
  files?: string[];

  /**
   * Close Document
   */
  @property({
    type: 'string',
  })
  closeDoc?: string;

  /**
   * The date the file object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the file object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * Signature
   */
  @property({type: 'string'})
  signature?: string;

  /**
   * The date the file object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * Whether or not the file object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  /**
   * How the subscription was created
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['DIRECT_LINK', 'INVITATION'],
    },
  })
  acquisitionMethod?: string;

  @hasMany(() => Transaction)
  transactions: Transaction[];

  @belongsTo(() => User)
  ownerId: string;

  @belongsTo(() => Profile)
  profileId: string;

  /**
   * The side letter properties for the subscription
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        managementFee: {
          type: 'object',
          properties: {
            amount: {type: 'number'},
            duration: {type: 'number'},
            frequency: {type: 'string'},
            percent: {type: 'number'},
            type: {type: 'string'},
            feesPaymentMethod: {type: 'string'},
            isRecurring: {type: 'boolean'},
          },
        },
        carryPercent: {
          type: 'object',
          patternProperties: {
            '.*': {type: 'number'},
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Side Letter',
    description: 'Side Letter Properties',
  })
  sideLetter?: {
    managementFee?: {
      amount?: number;
      duration?: number;
      frequency?: string;
      percent?: number;
      type?: 'percent' | 'flat';
      feesPaymentMethod?: string;
      isRecurring?: boolean;
    };
    carryPercent?: {
      organizerCarryPercentage: number;
      [key: string]: number;
    };
  };

  /**
   * Additional information for the entity
   */
  @property()
  additionalProperties?: {};

  /**
   * Additional files
   */
  @property.array('string')
  additionalFiles?: string[];
}

export interface SubscriptionRelations {
  // describe navigational properties here
  profile?: ProfileWithRelations;
}

export type SubscriptionWithRelations = Subscription & SubscriptionRelations;
