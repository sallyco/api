import {model, property} from '@loopback/repository';
import ProviderEntity from '../abstracts/ProviderEntity';

@model({
  settings: {
    hiddenProperties: ['ownerId', 'tenantId', 'providerMeta'],
    strict: false,
  },
})
export class Account extends ProviderEntity {
  @property({
    type: 'string',
    required: true,
  })
  type: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Account>) {
    super(data);
  }
}

export interface AccountRelations {
  // describe navigational properties here
}

export type AccountWithRelations = Account & AccountRelations;
