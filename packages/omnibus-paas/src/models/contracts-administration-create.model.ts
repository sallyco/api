import {Entity, model, property, NumberType} from '@loopback/repository';

@model({
  jsonSchema: {
    definitions: {
      bankAccountDetails: {
        type: 'object',
        required: ['ABA', 'accountnumber', 'bankname'],
        properties: {
          ABA: {
            type: 'number',
            title: 'ABA Routing Number',

            description: 'ABA Routing Number',
          },
          accountnumber: {
            title: 'Account Number',
            type: 'string',
            description: 'Account Number',
          },
          bankname: {
            title: 'Bank Name',
            type: 'string',
            description: 'Name of the bank',
          },
        },
        title: 'Bank Account Details',
      },
    },
    dependencies: {
      createBankAccount: {
        oneOf: [
          {
            properties: {
              createBankAccount: {enum: [false]},
              bankAccountDetails: {$ref: '#/definitions/bankAccountDetails'},
            },
            required: ['bankAccountDetails'],
          },
        ],
      },
    },
  },
})
export class ContractsAdministrationCreate extends Entity {
  @property({
    type: 'date',
    required: true,
    default: () => new Date(),
    jsonSchema: {
      title: 'StartDate',
      description: 'Contract StartDate',
    },
  })
  startDate?: Date;

  @property({
    type: 'date',
    required: true,
    default: () => new Date(),
    jsonSchema: {
      title: 'EndDate',
      description: 'Contract EndDate',
    },
  })
  endDate?: Date;

  @property({
    type: 'object',
    jsonSchema: {
      required: [
        'packageType',
        'monthlyRecurringPrice',
        'noOfContractedSPVs',
        'noOfSPVsconsumed',
        'spvsremaining',
      ],
      properties: {
        packageType: {
          enum: ['Bronze', 'Silver', 'Gold'],
          title: 'Saas PackageType',
          description: 'SaaS Package Type (Bronze, Silver, Gold)',
        },
        monthlyRecurringPrice: {
          type: 'number',
          title: 'Price',
          description: 'Monthly Recurring Price',
        },
        noOfContractedSPVs: {
          type: 'number',
          title: 'Contracted SPVs',
          description: 'Number of Contracted SPVs',
        },
        noOfSPVsconsumed: {
          type: 'number',
          title: 'SPVs consumed',
          description: 'Number of SPVs consumed',
        },
        spvsremaining: {
          type: 'number',
          title: 'SPVs remaining',
          description: 'Contracted SPVs remaining',
        },
      },
      title: 'Pricing Plans:',
    },
  })
  saasPackage?: {
    packageType?: 'Bronze' | 'Silver' | 'Gold';
    monthlyRecurringPrice?: number;
    noOfContractedSPVs?: number;
    noOfSPVsconsumed?: number;
    spvsremaining?: number;
  };

  @property({
    type: 'object',
    jsonSchema: {
      required: ['type', 'frequency'],
      properties: {
        withholdfromdeal: {
          title: 'With hold from deal?',
          type: 'boolean',
        },
        type: {
          type: 'string',
          title: 'Type',
          enum: ['Credit card ', 'Cheque', 'Wire', 'ACH Debit'],
        },
        frequency: {
          type: 'string',
          title: 'Frequency',
          enum: ['Monthly', 'Anually'],
        },
      },
      title: 'Payment Method',
      description:
        'Withhold from deal Y/N (if yes, set SPV admin fee on closing statement, if no, don’t include on closing statement)',
    },
  })
  paymentMethod?: {
    withholdfromdeal?: boolean;
    type?: 'Credit card ' | 'Cheque' | 'Wire' | 'ACH Debit';
    frequency?: 'Monthly' | 'Anually';
  };

  @property({
    type: 'object',
    jsonSchema: {
      required: ['name', 'price'],
      properties: {
        name: {
          title: 'Name',
          description: 'Name of service',
          type: 'string',
        },
        price: {
          title: 'Price',
          description: 'Price of service',
          type: 'number',
        },
      },
      title: 'Services',
    },
  })
  services?: {
    name?: string;
    price?: NumberType;
  };
  @property({
    type: 'boolean',
    jsonSchema: {
      default: true,
      title: 'Auto create new bank account?',
      description: 'Auto create new bank account new bank account manager',
    },
  })
  createBankAccount: boolean;
  constructor(data?: Partial<ContractsAdministrationCreate>) {
    super(data);
  }
}
