import {Model, model, property} from '@loopback/repository';
import {ILegalInc} from '../interfaces/services/legal-inc.interface';

export enum LegalIncOrderStatus {
  ORDERPLACED = 'ORDERPLACED',
  PROCESSING = 'PROCESSING',
  COMPLETED = 'COMPLETED',
  ERROR = 'ERROR',
}

@model()
export class LegalIncOrderModel extends Model {
  @property({
    type: 'string',
    required: true,
  })
  status: string | ILegalInc.API.OrderStatus | number;

  @property({
    type: 'string',
  })
  statusDescription?: string;

  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^(0[1-9]|[1-9]\d)-\d{7}$/.source,
    },
  })
  ein?: string;

  @property({
    type: 'number',
    required: true,
  })
  orderId: number;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  orderPlacedDate?: Date;

  @property({
    type: 'date',
  })
  orderFulfilledDate?: Date;

  constructor(data?: Partial<LegalIncOrderModel>) {
    super(data);
  }
}

export interface LegalIncOrderModelRelations {
  // describe navigational properties here
}

export type LegalIncOrderModelWithRelations = LegalIncOrderModel &
  LegalIncOrderModelRelations;
