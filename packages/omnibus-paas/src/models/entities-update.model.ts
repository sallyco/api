import {Entity, model, property, belongsTo} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Deal} from './deal.model';
import {Company} from './company.model';

@model({
  name: 'Update Entity',
})
export class EntitiesUpdate extends Entity {
  constructor(data?: Partial<EntitiesUpdate>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Owner ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Owner ID',
  })
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  @importable({
    displayName: 'Tenant ID',
  })
  tenantId?: string;

  /**
   * Deal ID
   */
  @belongsTo(() => Deal, {name: 'deal', keyFrom: 'dealId', keyTo: 'id'})
  @importable({
    displayName: 'Deal ID',
  })
  dealId: string;

  /**
   * The name of the entity
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Legal Name of the Entity',
  })
  name: string;

  /**
   * The type of the entity
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
      ],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of Entity',
  })
  entityType:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION';

  /**
   * Reg type
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['REGULATION_D', 'REGULATION_S', 'REGULATION_D_S'],
    },
  })
  @importable({
    displayName: 'Regulation Type',
    description: 'The Regulation Type for the Entity',
  })
  regulationType: 'REGULATION_D' | 'REGULATION_S' | 'REGULATION_D_S';

  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Reg D Exemption',
  })
  regDExemption: 'string';

  /**
   * Minimum amount required for the Entity
   */
  @property({
    type: 'number',
    required: true,
    jsonSchema: {
      minimum: 0,
      maximum: 6000000,
    },
  })
  @importable({
    displayName: 'Minimum Investment Amount',
    description:
      'Minimum investment amount required to participate in the deal',
  })
  minInvestmentAmount: number;

  /**
   * The country the entity was formed in
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Country of Formation',
    description: 'The Country the Entity was formed in',
  })
  countryOfFormation: string;

  /**
   * The state the entity was formed in
   */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
      nullable: true,
    },
  })
  @importable({
    displayName: 'State of Formation',
    description: 'The State the Entity was formed in',
  })
  stateOfFormation?: string;

  @property({
    type: 'object',
    jsonSchema: {
      type: 'object',
      patternProperties: {
        operatingAgreement: {type: 'string'},
        privatePlacementMemorandum: {type: 'string'},
        subscriptionAgreement: {type: 'string'},
        einLetter: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Documents',
    description: 'The Legal Documents for the Entity',
  })
  entityDocuments?: {
    operatingAgreement?: string;
    privatePlacementMemorandum?: string;
    subscriptionAgreement?: string;
    einLetter?: string;
    purchaseAgreement?: string;
  };

  /**
   * The entity EIN
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^(0[1-9]|[1-9]\d)-\d{7}$/.source,
    },
  })
  @importable({
    displayName: 'EIN',
    description: 'The EIN for the Entity',
  })
  ein?: string;

  /**
   * The Entity Asset composition
   */
  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Asset Composition',
  })
  assetComposition?: string;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        city: {type: 'string'},
        state: {type: 'string'},
        country: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Arbitration',
  })
  arbitration?: {
    city: string;
    state: string;
    country: string;
  };

  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Registered Agent',
  })
  registeredAgent?: string;

  /**
   * Management Fee
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        amount: {type: 'number'},
        duration: {type: 'number'},
        frequency: {type: 'string'},
        percent: {type: 'number'},
        type: {type: 'string'},
        feesPaymentMethod: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Management Fee',
  })
  managementFee?: {
    amount?: number;
    duration?: number;
    frequency?: string;
    percent?: number;
    type?: 'percent' | 'flat';
    feesPaymentMethod?: string;
    isRecurring?: boolean;
  };

  /**
   * Expense Reserve
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        amount: {type: 'number'},
        type: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Expense Reserve',
  })
  expenseReserve?: {
    amount?: number;
    type?: 'Flat Fee' | 'Percent';
  };

  /**
   * The date the entity object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * Whether or not the entity object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  @belongsTo(() => Company, undefined, {
    required: false,
    jsonSchema: {nullable: true},
  })
  managerId?: string;

  @belongsTo(() => Company, undefined, {
    required: false,
    jsonSchema: {nullable: true},
  })
  masterEntityId?: string;
}
