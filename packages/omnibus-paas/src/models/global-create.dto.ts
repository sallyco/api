import {Model, model, property} from '@loopback/repository';

@model({
  name: 'GlobalCreate',
})
export class GlobalCreate extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  key: string;

  @property({
    type: 'string',
    required: true,
  })
  category: string;

  @property({
    type: 'string',
  })
  entry?: string;

  constructor(data?: Partial<GlobalCreate>) {
    super(data);
  }
}
