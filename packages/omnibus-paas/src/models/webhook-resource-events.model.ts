import {Model, model, property} from '@loopback/repository';
import {WebhookEventTypes} from '../services/system/webhook.service';

@model()
export class WebhookResourceEvents extends Model {
  @property({
    type: 'string',
    required: true,
  })
  id: string;

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
    jsonSchema: {
      enum: [
        WebhookEventTypes.CREATE,
        WebhookEventTypes.UPDATE,
        WebhookEventTypes.DELETE,
      ],
    },
  })
  events: WebhookEventTypes[];

  constructor(data?: Partial<WebhookResourceEvents>) {
    super(data);
  }
}

export interface WebhookResourceEventsRelations {
  // describe navigational properties here
}

export type WebhookResourceEventsWithRelations = WebhookResourceEvents &
  WebhookResourceEventsRelations;
