import {Model, model, property} from '@loopback/repository';

@model()
export class AchReverseRequest extends Model {
  @property({
    type: 'string',
  })
  routing?: string;

  @property({
    type: 'string',
  })
  account?: string;

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: ['CHECKING', 'SAVINGS'],
    },
  })
  typeOfAccount?: 'CHECKING' | 'SAVINGS';

  @property({
    type: 'string',
  })
  nameOnAccount: string;

  @property({
    type: 'number',
    required: false,
    jsonSchema: {
      type: 'number',
      minimum: 0.01,
      multipleOf: 0.01,
      description:
        'If omitted, will attempt a refund the whole subscription amount',
    },
  })
  amount: number;

  constructor(data?: Partial<AchReverseRequest>) {
    super(data);
  }
}

export interface AchReverseRequestRelations {
  // describe navigational properties here
}

export type AchReverseRequestWithRelations = AchReverseRequest &
  AchReverseRequestRelations;
