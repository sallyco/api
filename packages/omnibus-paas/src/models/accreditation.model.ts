import {model, belongsTo, property} from '@loopback/repository';
import {Profile} from './profile.model';
import ProviderEntity from '../abstracts/ProviderEntity';

export type AccreditationTypes = '506c' | 'Self' | 'QP';
export type AccreditationStatus =
  | 'PENDING'
  | 'CURRENT'
  | 'THIRD_PARTY_PENDING'
  | 'EXPIRED'
  | 'REJECTED';

@model({
  name: 'Accreditation',
  settings: {
    hiddenProperties: ['providerMeta'],
    description: 'Accreditation',
  },
})
export class Accreditation extends ProviderEntity {
  @belongsTo(() => Profile)
  profileId: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Accreditation Type',
      description: 'The Type of Accreditation',
    },
  })
  type: AccreditationTypes;

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: [
        'PENDING',
        'CURRENT',
        'THIRD_PARTY_PENDING',
        'EXPIRED',
        'REJECTED',
      ],
    },
  })
  status:
    | 'PENDING'
    | 'CURRENT'
    | 'THIRD_PARTY_PENDING'
    | 'EXPIRED'
    | 'REJECTED'
    | 'EXCEPTION';

  @property({
    type: 'array',
    itemType: 'string',
  })
  files: string[];

  @property({
    type: Date,
  })
  certificationDate?: Date;

  @property({
    type: Date,
  })
  expirationDate?: Date;

  @property({
    type: 'string',
  })
  reason?: string;

  constructor(data?: Partial<Accreditation>) {
    super(data);
  }
}

export interface AccreditationRelations {
  // describe navigational properties here
}

export type AccreditationWithRelations = Accreditation & AccreditationRelations;
