import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Close, CloseWithRelations} from './close.model';

@model()
export class FormD extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    hidden: true,
  })
  id: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Central Index Key (CIK)',
      maxLength: 64,
    },
  })
  cik?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'CIK Password',
      maxLength: 128,
    },
  })
  cikPassword?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'CIK Confirmation Code',
      maxLength: 64,
    },
  })
  ccc?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Accession Number',
      description: 'Assigned automatically to an accepted submission by EDGAR',
      maxLength: 64,
    },
  })
  accessionNumber?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'PMAC',
      description: 'PMAC is used authorize a change of password',
      maxLength: 64,
    },
  })
  pmac?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'File Number',
      maxLength: 64,
    },
  })
  fileNumber?: string;

  @property({
    type: 'date',
    jsonSchema: {
      title: 'Filed Date',
      description: 'Date this filing is complete',
      type: 'string',
      format: 'date-time',
    },
  })
  filedDate?: string;

  @property({
    type: 'date',
    jsonSchema: {
      title: 'Blue Sky Paid Date',
      description: 'Date blue sky fees were paid',
      type: 'string',
      format: 'date-time',
    },
  })
  blueSkyPaidDate?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Blue Sky Receipt Number',
      type: 'string',
    },
  })
  blueSkyReceipt?: string;

  @belongsTo(() => Close, {name: 'Close'})
  closeId: string;

  @property({
    type: 'string',
  })
  entitiesId?: string;

  constructor(data?: Partial<FormD>) {
    super(data);
  }
}

export interface FormDRelations {
  close?: CloseWithRelations;
}

export type FormDWithRelations = FormD & FormDRelations;
