import {Entity, model, property} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Individual} from './individual.model';
import {LegalIncOrderModel} from './legalinc-order.model';

@model({
  name: 'Company',
  settings: {
    strict: 'filter',
  },
})
export class Company extends Entity {
  constructor(data?: Partial<Company>) {
    super(data);

    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }
  /**
   * Company ID
   */
  @property({
    type: 'string',
    id: true,
  })
  @importable({
    displayName: 'ID',
  })
  id: string;

  /* company name */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Company Name',
  })
  name: string;

  /* company website */
  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Website',
    description: 'The Company Website',
  })
  website?: string;

  /***** contact info *****/

  /* company email */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^\S+@\S+$/.source,
    },
  })
  @importable({
    displayName: 'Email',
    description: 'The contact email for the company',
  })
  email?: string;

  /**
   * The type of the company
   */
  @property({
    type: 'string',
    jsonSchema: {
      enum: ['MASTER_ENTITY', 'REGISTERED_AGENT', 'MANAGER'],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of company',
  })
  type?: 'MASTER_ENTITY' | 'REGISTERED_AGENT' | 'MANAGER';

  /**
   * Entity type
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Entity Type',
  })
  entityType?: string;

  /* company phone */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Phone',
    description: 'The Phone number for the company',
  })
  phone?: string;

  /**
   * Raise amount being targeted for the deal
   */
  @property({
    type: 'number',
  })
  @importable({
    displayName: 'Next series number',
  })
  seriesNumber?: number;

  @property({
    type: 'string',
  })
  seriesPrefix?: string;

  /**
   * Raise amount being targeted for the deal
   */
  @property({
    type: 'string',
  })
  @importable({
    displayName: 'State of Formation',
  })
  stateOfFormation?: string;

  /* company address */
  @property({
    type: 'object',
    jsonSchema: {
      required: ['address1', 'city', 'postalCode', 'country'],
      patternProperties: {
        address1: {type: 'string', minLength: 2, maxLength: 1024},
        address2: {type: 'string', maxLength: 1024},
        city: {type: 'string', minLength: 2, maxLength: 1024},
        state: {type: 'string', minLength: 2, maxLength: 1024},
        postalCode: {type: 'string', minLength: 2, maxLength: 1024},
        country: {type: 'string', minLength: 2, maxLength: 1024},
      },
    },
  })
  @importable({
    displayName: 'Address',
    description: 'The Company Address',
  })
  address?: {
    address1: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };

  /**
   * Owner ID
   */
  @property({type: 'string'})
  ownerId?: string;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  @property({type: 'string'})
  arbitrationCity?: string;

  @property({type: 'string'})
  arbitrationState?: string;

  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Primary Contact',
  })
  primaryContact?: Individual;

  @property({
    type: 'object',
  })
  @importable({
    displayName: 'Tax Contact',
  })
  taxContact?: Individual;

  @property(LegalIncOrderModel)
  legalIncOrder?: Partial<LegalIncOrderModel>;

  /**
   * The date the company object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the company object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Entity Type',
      description: 'The Entity Type of company',
      enum: ['LLC', 'Corporation', 'Non-Profit', 'LP', 'LLP', 'GP', 'DBA'],
    },
  })
  entitytype?:
    | 'LLC'
    | 'Corporation'
    | 'Non-Profit'
    | 'LP'
    | 'LLP'
    | 'GP'
    | 'DBA';

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Type of Business/Purpose',
      description: 'Type of Business/Purpose (Finance & Insurance)',
    },
  })
  typeOfBusiness?: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Type of Business Activity',
      description: 'Type of Business Activity (Finance)',
    },
  })
  typeOfBusinessActivity?: string;

  @property({
    type: 'boolean',
    jsonSchema: {
      title: 'Is this a professional entity?',
    },
  })
  professionalEntity?: boolean;

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: [
        'Solely by the owner(s) (Member-managed)',
        'Some, but not all, owners (Manager-managed)',
        'By the owner(s) and other managers (Manager-managed)',
        'Solely by the manager(s) (Manager-Managed)',
      ],
      title: 'How Will Be Business Managed?',
      description:
        'How will the business be managed (can have 1-many owners/managers).',
    },
  })
  howWillBeBusinessManaged?:
    | 'Solely by the owner(s) (Member-managed)'
    | 'Some, but not all, owners (Manager-managed)'
    | 'By the owner(s) and other managers (Manager-managed)'
    | 'Solely by the manager(s) (Manager-Managed)';

  @property({
    type: 'string',
    jsonSchema: {
      type: 'string',
      enum: ['Individual', 'Business'],
      title: 'Business/Individual',
    },
  })
  individualOrBusiness?: 'Individual' | 'Business';

  @property({
    type: 'array',
    itemType: 'object',
    items: {
      type: 'object',
      jsonSchema: {
        required: ['firstName', 'lastName', 'memberAddress'],
        properties: {
          firstName: {
            title: 'FirstName',
            type: 'string',
            description: 'The FirstName of individual',
          },
          lastName: {
            title: 'LastName',
            type: 'string',
            description: 'The LastName of individual',
          },
          memberAddress: {
            title: 'Member/Manager Address',
            type: 'string',
            description: 'The Member/Manager Address of individual',
          },
          isManager: {
            title: 'IsManager?',
            type: 'boolean',
            description: 'Is this owner also a manager?',
          },
          percentageOwnership: {
            title: '% Ownership of member?',
            type: 'number',
            description: '% Ownership of member',
          },
          capitalContribution: {
            title: 'Capital contribution',
            type: 'number',
            description: 'Capital contribution for Member',
          },
        },
        title: 'Individual Details',
      },
    },
  })
  individuals?: {
    firstName?: string;
    lastName?: string;
    memberAddress?: string;
    isManager?: boolean;
    percentageOwnership?: number;
    capitalContribution?: number;
  }[];

  @property({
    type: 'object',
    required: false,
  })
  individual: object;

  @property({
    type: 'array',
    itemType: 'object',
    items: {
      type: 'object',
      jsonSchema: {
        required: ['businessName', 'address'],
        properties: {
          businessName: {
            title: 'Business Name',
            type: 'string',
            description: 'The Name of business',
          },
          address: {
            title: 'Address',
            type: 'string',
            description: 'Member/Business Address',
          },
          isManager: {
            title: 'IsManager?',
            type: 'boolean',
            description: 'Is this owner also a manager?',
          },
          percentageOwnership: {
            title: '% Ownership of member?',
            type: 'number',
            description: '% Ownership of member',
          },
          capitalContribution: {
            title: 'Capital contribution',
            type: 'number',
            description: 'Capital contribution for Member',
          },
        },
        title: 'Business Details',
      },
    },
  })
  businesses?: {
    businessName?: string;
    address?: string;
    isManager?: boolean;
    percentageOwnership?: number;
    capitalContribution?: number;
  }[];

  @property({
    type: 'object',
    required: false,
  })
  business: object;

  @property({
    type: 'boolean',
    title: "Use Organizer's SSN",
    description: "This will use the organizer's SSN for EIN obtainment",
  })
  useOrganizerSSNForEINOrders?: boolean;

  /**
   * The entity EIN
   */
  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^(0[1-9]|[1-9]\d)-\d{7}$/.source,
    },
  })
  ein: string;

  //TODO consolidate this with what is on the entities.bankaccount
  /**
   * The fees account
   */
  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        providerMeta: {
          type: 'object',
          patternProperties: {
            typeId: {type: 'string'},
            accountStatus: {type: 'string'},
            accountApplicationId: {type: 'string'},
            accountId: {type: 'string'},
          },
        },
        createdAt: {type: 'string', format: 'date-time'},
        updatedAt: {type: 'string', format: 'date-time'},
        bankName: {type: 'string'},
        accountNumber: {type: 'string'},
        routingNumber: {type: 'string'},
        accountName: {type: 'string'},
        bankContact: {
          type: 'object',
          patternProperties: {
            name: {type: 'string'},
            phone: {type: 'string'},
            email: {type: 'string'},
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Fees Account',
    description: 'The Fees Account associated with the Entity',
  })
  feesAccount?: {
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      [prop: string]: any;
    };
    createdAt?: Date;
    updatedAt?: Date;
    bankName?: string;
    accountNumber?: string;
    routingNumber?: string;
    accountName?: string;
    swiftCode?: string;
    bankContact?: {
      name?: string;
      phone?: string;
      email?: string;
    };
  };

  @property.array('string')
  files?: string[];

  /**
   * Additional information for the company
   */
  @property()
  additionalProperties?: {};
}

export interface CompanyRelations {
  // describe navigational properties here
}

export type CompanyWithRelations = Company & CompanyRelations;
