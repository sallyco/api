import {model, property} from '@loopback/repository';
import {getNames} from 'i18n-iso-countries';

@model()
export class Address {
  @property({
    title: 'Address 1',
    type: 'string',
    jsonSchema: {
      type: 'string',
      minLength: 2,
      maxLength: 1024,
    },
    description: 'Street Name Line 1',
    example: '123 Test St',
  })
  address1: string;

  @property({
    title: 'Address 2',
    type: 'string',
    jsonSchema: {
      type: 'string',
      maxLength: 1024,
    },
    description: 'Street Name Line 2',
    example: 'Unit 6',
  })
  address2: string;

  @property({
    title: 'City',
    description: 'City of Address',
    type: 'string',
    jsonSchema: {
      type: 'string',
      minLength: 2,
      maxLength: 1024,
    },
    example: 'Salt Lake City',
  })
  city: string;

  @property({
    title: 'Region',
    description: 'Region or State of Address',
    type: 'string',
    jsonSchema: {
      type: 'string',
      minLength: 2,
      maxLength: 1024,
    },
    example: 'Utah',
  })
  region: string;

  @property({
    title: 'Postal Code',
    description: 'Postal (or ZIP) Code of Address',
    type: 'string',
    jsonSchema: {
      type: 'string',
      minLength: 2,
      maxLength: 1024,
    },
    example: '84101',
  })
  postalCode: string;

  @property({
    title: 'Country',
    type: 'string',
    description: 'ISO 3166-1 Full Country name in ISO-639-1 (EN) Format',
    jsonSchema: {
      type: 'string',
      minLength: 2,
      maxLength: 1024,
      enum: [...Object.values(getNames('en'))],
    },
    example: 'United States of America',
  })
  country: string;
}
