import {getJsonSchemaRef} from '@loopback/rest';
import {
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
} from '@loopback/repository';

@model({
  title: 'Package Attribute',
  description: `
Package attributes belong in packages. A package attribute can be reused in multiple packages.

\`package-attributes\` <--> \`package\`

They govern configuration, limits and attributes of a variety of objects including deals, organizers and tenants.  A group of package-attributes included in a package can determine what a user can and can't do.
`,
  jsonSchema: {
    definitions: {
      fields: {
        title: 'Fields',
        type: 'string',
        anyOf: [
          {
            type: 'string',
            enum: ['perDealMaxInvestorCount'],
            title: 'Maximum Investors per Deal',
          },
          {
            type: 'string',
            enum: ['perDealMaxTargetRaiseAmount'],
            title: 'Maximum Target Raise per Deal',
          },
        ],
      },
    },
  },
  settings: {
    strict: 'filter',
  },
})
class PackageAttributeBase extends Entity {
  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
    jsonSchema: {
      title: 'Attribute Name',
      description:
        'Readable identifier of the attribute; should be unique to avoid confusion',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Attribute Description',
      maxLength: 1024,
    },
  })
  description?: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      $ref: '#/definitions/fields',
      title: 'Effective Field Name',
      description:
        'The field where the specified value takes effect.  Available fields are currently statically defined.',
    },
  })
  effectiveField?: string;

  @property({
    type: 'number',
    required: true,
    jsonSchema: {
      title: 'Effective Field Value',
      description:
        'The value that is applied to the specified effective field.',
    },
  })
  value?: number;

  constructor(data?: Partial<PackageAttribute>) {
    super(data);
  }
}

export class PackageAttribute extends PackageAttributeBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id: string;

  constructor(data?: Partial<PackageAttribute>) {
    super(data);
  }
}

export interface PackageAttributeRelations {}

export type PackageAttributeWithRelations = PackageAttribute &
  PackageAttributeRelations;

// DTO
export const PackageAttributeCreate = defineModelClass<
  typeof PackageAttribute,
  {}
>(PackageAttribute, new ModelDefinition('packageAttributeCreate'));

export const PackageAttributeUpdate = defineModelClass<
  typeof PackageAttribute,
  {id: string}
>(
  PackageAttribute,
  new ModelDefinition('packageAttributeEdit').addProperty('id', {
    type: 'string',
    jsonSchema: {readOnly: true},
  }),
);

// SCHEMA
export const PackageAttributeSchema = getJsonSchemaRef(PackageAttribute);

export const PackageAttributeCreateSchema = getJsonSchemaRef(
  PackageAttributeCreate,
  {
    title: 'Create a New Package Attribute',
    exclude: ['id'],
  },
);
export const PackageAttributeUpdateSchema = getJsonSchemaRef(
  PackageAttributeUpdate,
  {title: 'Edit a Package Attribute'},
);
