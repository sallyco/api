import {Entity, model, property} from '@loopback/repository';

@model()
export class BankingBeneficiary extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerId: string;

  @property({
    type: 'string',
    required: true,
  })
  tenantId: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  phone?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  serviceObjectId?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: string;

  @property({
    type: 'object',
  })
  providerMeta?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };

  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted?: boolean;

  constructor(data?: Partial<BankingBeneficiary>) {
    super(data);
  }
}

export interface BankingBeneficiaryRelations {
  // describe navigational properties here
}

export type BankingBeneficiaryWithRelations = BankingBeneficiary &
  BankingBeneficiaryRelations;
