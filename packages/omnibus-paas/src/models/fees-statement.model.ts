import {Model, model, property} from '@loopback/repository';

@model()
export class FeesStatementEntry extends Model {
  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'string',
  })
  proceeds?: string;

  @property({
    type: 'string',
  })
  expenses?: string;

  @property({
    type: 'boolean',
  })
  isInvestorRow?: boolean;

  @property({
    type: 'boolean',
  })
  bold?: boolean;

  constructor(data?: Partial<FeesStatementEntry>) {
    super(data);
  }
}

export class FeesStatementSection extends Model {
  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  subTitle?: string;

  @property.array(FeesStatementEntry)
  entries: FeesStatementEntry[];

  @property({
    type: 'number',
  })
  subTotal?: number;

  constructor(data?: Partial<FeesStatementSection>) {
    super(data);
  }
}

export class FeesStatement extends Model {
  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  subTitle?: string;

  @property.array(FeesStatementSection)
  sections: FeesStatementSection[];

  @property({
    type: 'number',
    default: () => 0,
    required: true,
  })
  totalProceeds?: number;

  @property({
    type: 'number',
    default: () => 0,
    required: true,
  })
  totalFees: number;

  @property({
    type: 'number',
    default: () => 0,
    required: true,
  })
  proceedsToSend?: number;

  @property({
    type: 'number',
    default: () => 0,
    required: true,
  })
  spvFee?: number;

  @property({
    type: 'array',
    itemType: 'object',
  })
  investors?: {
    name: string;
    state: string;
    amount: string;
  }[];

  @property({
    type: 'date',
    required: true,
  })
  createdAt: string;

  constructor(data?: Partial<FeesStatement>) {
    super(data);
  }
}

export const mockFeeStatement = new FeesStatement({
  title: 'Statement Title',
  subTitle: 'Statement Subtitle',
  sections: [
    new FeesStatementSection({
      title: 'Section Title',
      subTitle: 'Section Subtitle',
      entries: [
        new FeesStatementEntry({
          description: 'Entry1 proceeds description',
          proceeds: '500000.00',
        }),
        new FeesStatementEntry({
          description: 'Entry1 expenses description',
          expenses: '10000.00',
        }),
      ],
      subTotal: 490000.0, // calculated from entries?
    }),
    new FeesStatementSection({
      title: 'Section2 Title',
      subTitle: 'Section2 Subtitle',
      entries: [
        new FeesStatementEntry({
          description: 'Entry1 expenses description',
          expenses: '100.0',
        }),
        new FeesStatementEntry({
          description: 'Entry1 expenses description',
          expenses: 'included',
        }),
      ],
      subTotal: 100.0,
    }),
  ],
  totalFees: 0,
  createdAt: new Date().toUTCString(),
});
