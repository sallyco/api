import {Entity, model, property} from '@loopback/repository';
import {getJsonSchema} from '@loopback/rest';
import {WebhookResourceEvents} from './webhook-resource-events.model';

/**
 * The model class is generated from OpenAPI schema - Webhook
 * A representation of a webhook
 */
@model({name: 'Webhook'})
export class Webhook extends Entity {
  constructor(data?: Partial<Webhook>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * A unique identifier for this object
   */
  @property({
    id: true,
  })
  _id?: string;

  /**
   * The url to listen to notifications
   */
  @property({
    type: 'string',
    required: true,
  })
  url: string;

  /**
   * Determines if the webhook should be active.
   */
  @property({
    type: 'boolean',
    default: true,
  })
  enabled: boolean;

  /**
   * Tenant ID
   */
  @property({type: 'string'})
  tenantId?: string;

  /**
   * The events that should be published to the webhook.
   */
  @property.array(WebhookResourceEvents, {
    jsonSchema: getJsonSchema(WebhookResourceEvents),
  })
  resources: WebhookResourceEvents[];

  /**
   * The webhook token used to verify the request.
   */
  @property({
    type: 'string',
    required: true,
  })
  token: string;
}

export interface WebhookRelations {
  // describe navigational properties here
}

export type WebhookWithRelations = Webhook & WebhookRelations;
