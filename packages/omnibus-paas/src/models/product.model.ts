import {getJsonSchemaRef} from '@loopback/rest';
import {
  DefaultCrudRepository,
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
  belongsTo,
} from '@loopback/repository';
import {
  ProductCategory,
  ProductCategoryWithRelations,
} from './product-category.model';
import {MongodbDataSource} from '../datasources';
import _ from 'lodash';

@model({
  title: 'Product',
  description: `
Product Specification

For example (products **_emphasized_**):

| SPV Expenses | |
|:--- | ---:|
| &nbsp; &nbsp; &nbsp; **_EIN Obtainment_**: | $49 |

| **Tax and Accounting Fees** | |
|:--- | ---:|
| &nbsp; &nbsp; &nbsp; **_Tax and K1 Processing_**: | included |
`,
  jsonSchema: {
    definitions: {
      priceAmount: {
        type: 'number',
        title: 'Price Amount',
        minimum: 0,
        description: 'Fixed price amount',
      },
      priceVariableBase: {
        title: 'Calculation Basis',
        type: 'string',
        description: 'Basis to use for price calculation',
        default: null,
        enum: ['Deal total raise', 'Deal investor count'],
      },
      priceVariablePercent: {
        title: 'Variable Percentage',
        type: 'number',
        description:
          'Variable percentage to apply to basis to calculate price (enter as decimal)',
        minimum: 0,
        maximum: 1,
        default: 0,
      },
      priceTier: {
        required: ['amount', 'breakpoint'],
        properties: {
          amount: {
            title: 'Price Tier Amount',
            type: 'number',
            description: 'The price amount for this tier',
            minimum: 0,
            default: 0,
          },
          breakpoint: {
            title: 'Tier Breakpoint',
            type: 'number',
            description:
              'Top point of the tier that breaks to the next level (0 indicates no maximum, i.e. top-tier)',
            minimum: 0,
            default: 0,
          },
        },
      },
    },
    dependencies: {
      priceType: {
        oneOf: [
          {
            properties: {
              priceType: {enum: ['Fixed']},
              priceAmount: {$ref: '#/definitions/priceAmount'},
            },
            required: ['priceAmount'],
          },
          {
            properties: {
              priceType: {enum: ['Variable']},
              priceVariableBase: {$ref: '#/definitions/priceVariableBase'},
              priceVariablePercent: {
                $ref: '#/definitions/priceVariablePercent',
              },
            },
            required: ['priceVariableBase', 'priceVariablePercent'],
          },
          {
            properties: {
              priceType: {enum: ['Tiered']},
              priceVariableBase: {$ref: '#/definitions/priceVariableBase'},
              priceTiers: {
                title: 'Price Tiers',
                description: 'Price tier that determines the price',
                type: 'array',
                items: {$ref: '#/definitions/priceTier'},
                default: [],
                minItems: 1,
              },
            },
            required: ['priceVariableBase', 'priceTiers'],
          },
        ],
      },
    },
  },
  settings: {
    strict: 'filter',
  },
})
class ProductBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Product Name',
      description:
        'This is the formal name of the product that appears on statements, invoices, etc.',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Product Description',
      description:
        'Description for additional specification or clarification of the product and its usage',
      maxLength: 1024,
    },
  })
  description: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['Fixed', 'Variable', 'Tiered'],
      title: 'Price Type',
      description: 'Determines how the product price should be calculated',
    },
  })
  priceType: 'Fixed' | 'Variable' | 'Tiered';

  constructor(data?: Partial<Product>) {
    super(data);
  }
}

@model()
export class Product extends ProductBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id?: string;

  @property({
    type: 'number',
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  priceAmount?: number;

  @property({
    type: 'string',
    jsonSchema: {
      default: null,
      nullable: true,
    },
  })
  priceVariableBase?: string;

  @property({
    type: 'number',
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  priceVariablePercent?: number;

  @property({
    type: 'array',
    itemType: 'object',
  })
  priceTiers?: {
    amount: number;
    breakpoint: number;
  }[];

  @belongsTo(() => ProductCategory)
  productCategoryId: string;

  constructor(data?: Partial<Product>) {
    super(data);
  }
}

export interface ProductRelations {
  productCategory?: ProductCategoryWithRelations;
}
export type ProductWithRelations = Product & ProductRelations;

// DTO
const DefineProductCreate = async () => {
  // TODO(Andrew): wrap this mad science into something, service?
  const ds = new MongodbDataSource();
  const productCategoryRepository = new DefaultCrudRepository(
    ProductCategory,
    ds,
  );
  const pc = await productCategoryRepository.find();
  return new ModelDefinition('productCreate').addProperty('productCategoryId', {
    type: 'string',
    jsonSchema: {
      title: 'Product Category',
      description: 'The category this product belongs to',
      enum: _.map(pc, 'id'),
      enumNames: _.map(pc, 'name'),
    },
  });
};
export const ProductCreate = async () =>
  defineModelClass<typeof ProductBase, {productCategoryId: string}>(
    ProductBase,
    await DefineProductCreate(),
  );

const DefineProductEdit = async () => {
  // TODO(Andrew): wrap this mad science into something, service?
  const ds = new MongodbDataSource();
  const productCategoryRepository = new DefaultCrudRepository(
    ProductCategory,
    ds,
  );
  const pc = await productCategoryRepository.find();
  return new ModelDefinition('productEdit')
    .addProperty('id', {
      type: 'string',
      jsonSchema: {readOnly: true},
    })
    .addProperty('productCategoryId', {
      type: 'string',
      jsonSchema: {
        title: 'Product Category',
        description: 'The category this product belongs to',
        enum: _.map(pc, 'id'),
        enumNames: _.map(pc, 'name'),
      },
    });
};
export const ProductUpdate = async () =>
  defineModelClass<typeof ProductBase, {id: string; productCategoryId: string}>(
    ProductBase,
    await DefineProductEdit(),
  );

// SCHEMA
export const ProductSchema = getJsonSchemaRef(Product);

export const ProductCreateSchema = async () =>
  getJsonSchemaRef(await ProductCreate(), {
    title: 'Create a New Product',
    exclude: ['id'],
  });
export const ProductUpdateSchema = async () =>
  getJsonSchemaRef(await ProductUpdate(), {title: 'Edit a Product'});
