import {Model, model, property} from '@loopback/repository';

@model()
export class PurchaseDetails extends Model {
  @property({
    type: 'string',
  })
  assetId?: string;

  @property({
    type: 'string',
    required: true,
  })
  purchaseUnit: string;

  @property({
    type: 'number',
    required: true,
  })
  purchaseAmount: number;

  @property({
    type: 'array',
    itemType: 'object',
  })
  proceeds?: object[];

  @property({
    type: 'array',
    itemType: 'object',
  })
  expenses?: object[];

  constructor(data?: Partial<PurchaseDetails>) {
    super(data);
  }
}

export interface PurchaseDetailsRelations {
  // describe navigational properties here
}

export type PurchaseDetailsWithRelations = PurchaseDetails &
  PurchaseDetailsRelations;
