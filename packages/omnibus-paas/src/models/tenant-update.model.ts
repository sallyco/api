import {Model, model, property} from '@loopback/repository';
import {Tenant, TieredPriceEntry} from './tenant.model';

@model({
  name: 'Update Tenant',
  description: 'Tenant settings and permissions',
})
export class TenantUpdate extends Model implements Partial<Tenant> {
  @property({
    type: 'string',
    id: true,
    jsonSchema: {
      title: 'Tenant id',
      readonly: true,
    },
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Name',
    },
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'URL',
      description: "URL to tenant's marketing or other public website",
    },
  })
  url: string;

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        emails: {
          type: 'object',
          title: 'Email',
          properties: {
            fromAddress: {
              title: 'From Address',
              type: 'string',
              description: 'Users will receive email from this address',
            },
            compliance: {
              title: 'Compliance Address',
              type: 'string',
              description: 'For filing and compliance notifications',
            },
          },
        },
        useOrganizerForEINOrders: {
          type: 'boolean',
          title: "Use Organizer's SSN",
          description: "This will use the organizer's SSN for EIN obtainment",
        },
        showCarryRecipientsForInvestors: {
          type: 'boolean',
          title: "Show Carry Recipients in Investors' View",
          description: "This will show carry recipient's in investors' view.",
          default: false,
        },
        canSelfApproveWires: {
          type: 'boolean',
          title: 'Can Self-approve Wire transactions',
          description: 'Can Self-approve Wire transactions',
        },
        useMultiStepSigningForCloses: {
          type: 'boolean',
          title: 'Uses Multi-Step Close Signing',
          description: 'e.g. Separate Organizer and Fund Manager',
        },
        use506cAccreditation: {
          type: 'boolean',
          title: 'Has 506c Accreditation Available',
          description: 'Allows tenant to use external 506c Accreditation',
        },
      },
      dependencies: {
        use506cAccreditation: {
          properties: {
            parallelMarketsClientId: {
              type: 'string',
              title: 'Parallel Markets Client ID',
              description:
                'Client ID to use for Parallel Markets configuration',
            },
          },
        },
      },
      title: 'Settings',
    },
  })
  settings?: {
    emails?: {
      fromAddress?: string;
      compliance?: string;
    };
    useOrganizerForEINOrders?: boolean;
    showCarryRecipientsForInvestors?: boolean;
    canSelfApproveWires?: boolean;
    useMultiStepSigningForCloses?: boolean;
  };

  @property({
    type: 'number',
    required: false,
    default: () => 180,
    jsonSchema: {
      title: 'Max Allowed Days for Close',
      description: 'Maximum allowed days for closing a deal',
    },
  })
  maxAllowedDaysToClose?: number;

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        maxDealCount: {
          type: 'integer',
          title: 'Max Deal Count',
          description: 'Total number of deals a tenant can create',
        },
        maxInvestorCount: {
          type: 'integer',
          title: 'Max Investor Count',
          description: 'Total number of investors allowed across all deals',
        },
        maxTargetRaiseAmount: {
          type: 'integer',
          title: 'Max Target Raise Count',
          description: 'Greatest amount allowed across all deals',
        },
        perDealMaxInvestorCount: {
          type: 'integer',
          title: 'Max Investor Count per Deal',
          description: 'Total number of investors allowed per deal',
        },
        perDealMaxTargetRaiseAmount: {
          type: 'integer',
          title: 'Max Target Raise Count per Deal',
          description: 'Greatest amount allowed to raise per deal',
        },
        maxExpenseReserve: {
          type: 'integer',
          title: 'Max Expense Reserve',
          description: 'Highest % Allowed for expense reserve',
        },
        costPerSPV: {
          type: 'integer',
          title: 'Cost Per SPV',
          description: 'Tenant cost per deal/SPV',
        },
      },
    },
  })
  dealLimits?: {
    maxDealCount?: number;
    maxInvestorCount?: number;
    maxTargetRaiseAmount?: number;
    maxExpenseReserve?: number;
    perDealMaxInvestorCount?: number;
    perDealMaxTargetRaiseAmount?: number;
    costPerSPV?: number;
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        saasPackageType: {
          type: 'string',
          enum: ['Bronze', 'Silver', 'Gold', `Tiered`],
          title: 'Saas PackageType',
          description: 'SaaS Package Type (Bronze, Silver, Gold)',
        },
        tieredPricing: {
          title: 'Tiered Pricing',
          description: 'Tiered Breakpoints for SPV Fee',
          type: 'array',
          items: {
            type: 'object',
            required: ['breakpoint', 'price'],
            properties: {
              breakpoint: {
                title: 'Breakpoint',
                type: 'number',
                multipleOf: 0.01,
                minimum: 0.0,
              },
              price: {
                title: 'Price Per SPV at Breakpoint',
                type: 'number',
                multipleOf: 0.01,
                minimum: 0.0,
              },
            },
          },
        },
      },
      title: 'Pricing Plans:',
    },
  })
  saasPackageType?: {
    sassPackageType?: 'Bronze' | 'Silver' | 'Gold' | 'Tiered';
    monthlyRecurringPrice?: number;
    noOfContractedSPVs?: number;
    noOfSPVsconsumed?: number;
    spvsremaining?: number;
    tieredPricing?: TieredPriceEntry[];
  };

  @property({
    type: 'string',
    required: false,
    jsonSchema: {
      title: 'Billing Contact',
      description: 'Billing Contact for the Tenant',
    },
  })
  billingContact?: string;

  constructor(data?: Partial<TenantUpdate>) {
    super(data);
  }
}
