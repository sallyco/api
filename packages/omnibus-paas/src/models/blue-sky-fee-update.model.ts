import {Model, model, property} from '@loopback/repository';

@model({
  title: 'Update Blue Sky Fee',
  description: 'EFD ‐ Form D Fee Schedule',
  jsonSchema: {
    definitions: {
      fee: {
        type: 'number',
        title: 'Fee Amount',
        minimum: 0,
        description: 'Fixed amount regardless of offering size',
      },
      variableBase: {
        title: 'Calculation Basis',
        type: 'string',
        description: 'Offering basis to use for fee calculation',
        default: null,
        enum: ['', 'State Offering', 'EDGAR Offering'],
      },
      variablePercent: {
        title: 'Variable Percentage',
        type: 'number',
        description:
          'Variable percentage to apply to basis to calculate fee (enter as decimal)',
        minimum: 0,
        maximum: 1,
        default: 0,
      },
      variableMin: {
        title: 'Minimum Fee',
        type: 'number',
        description: 'Minimum Fee possible for this state (can be 0)',
        minimum: 0,
        default: 0,
      },
      variableMax: {
        title: 'Maximum Fee',
        type: 'number',
        description: 'Maximum Fee possible for this state',
        minimum: 0,
        default: 0,
      },
      blueSkyFeeTier: {
        type: 'object',
        required: ['fee', 'breakpoint'],
        properties: {
          fee: {
            title: 'Tier Fee',
            type: 'number',
            description: 'The fee amount for this tier',
            minimum: 0,
            default: 0,
          },
          breakpoint: {
            title: 'Tier Breakpoint',
            type: 'number',
            description:
              'Top point of the tier that breaks to the next level (0 indicates no maximum, i.e. top-tier)',
            minimum: 0,
            default: 0,
          },
        },
      },
    },
    dependencies: {
      type: {
        oneOf: [
          {
            properties: {
              type: {enum: ['Fixed']},
              fee: {$ref: '#/definitions/fee'},
            },
            required: ['fee'],
          },
          {
            properties: {
              type: {enum: ['Variable']},
              variableBase: {$ref: '#/definitions/variableBase'},
              variablePercent: {$ref: '#/definitions/variablePercent'},
              variableMin: {$ref: '#/definitions/variableMin'},
              variableMax: {$ref: '#/definitions/variableMax'},
            },
            required: [
              'variableBase',
              'variablePercent',
              'variableMin',
              'variableMax',
            ],
          },
          {
            properties: {
              type: {enum: ['Fixed & Variable']},
              fee: {
                $ref: '#/definitions/fee',
                title: 'Fixed Amount',
                description: 'Amount of the fixed component for the total fee',
              },
              variableBase: {$ref: '#/definitions/variableBase'},
              variablePercent: {$ref: '#/definitions/variablePercent'},
              variableMin: {$ref: '#/definitions/variableMin'},
              variableMax: {
                $ref: '#/definitions/variableMax',
                description:
                  'Combined Maximum Fee for this state (fixed + variable)',
              },
            },
            required: [
              'fee',
              'variableBase',
              'variablePercent',
              'variableMin',
              'variableMax',
            ],
          },
          {
            properties: {
              type: {enum: ['Tiered']},
              variableBase: {$ref: '#/definitions/variableBase'},
              blueSkyFeeTiers: {
                title: 'Fee Tiers',
                description: 'Fee tier that determines the fee',
                type: 'array',
                items: {$ref: '#/definitions/blueSkyFeeTier'},
                default: [],
                minItems: 1,
              },
            },
            required: ['variableBase', 'blueSkyFeeTiers'],
          },
          {
            properties: {
              type: {
                enum: ['No Fee', 'No File'],
              },
            },
            required: [],
          },
        ],
      },
    },
  },
})
export class BlueSkyFeeUpdate extends Model {
  @property({
    type: 'number',
    id: true,
    jsonSchema: {
      readOnly: true,
    },
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      readOnly: true,
      title: 'State',
    },
  })
  state: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      readOnly: true,
      enum: ['b', 'c'],
      title: 'Exemptions Type',
      description: 'Exemption type for Rule 506 of Regulation D',
    },
  })
  exemption: 'b' | 'c';

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['Electronic', 'Check'],
      title: 'Payment Method',
    },
  })
  paymentMethod: 'Electronic' | 'Check';

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'Fixed',
        'Variable',
        'Fixed & Variable',
        'Tiered',
        'No Fee',
        'No File',
      ],
      title: 'Fee Type',
      description: 'Determines how the fee should be calculated',
    },
  })
  type:
    | 'Fixed'
    | 'Variable'
    | 'Fixed & Variable'
    | 'Tiered'
    | 'No Fee'
    | 'No File';

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Late Fee',
      description: 'Description of late fee assessment',
      nullable: true,
    },
  })
  lateFee?: string;

  constructor(data?: Partial<BlueSkyFeeUpdate>) {
    super(data);
  }
}
