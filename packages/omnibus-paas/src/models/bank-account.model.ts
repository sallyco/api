import {Entity, model, property} from '@loopback/repository';
import {webhookConfig} from '../decorators/webhook-model-name-decorator';

@webhookConfig({
  resourceId: 'banking/account',
})
@model({
  settings: {
    strict: false,
  },
  hiddenProperties: ['providerMeta', 'tenantIdForFees', 'tenantIdForBlueSky'],
})
export class BankAccount extends Entity {
  @property({
    type: 'string',
    id: true,
    mongodb: {dataType: 'ObjectId'},
    generated: true,
  })
  id?: string;

  @property({type: 'string'})
  ownerId?: string;

  @property({
    type: 'string',
  })
  status?: 'OPEN' | 'PENDING' | 'CLOSED';

  @property({type: 'string'})
  tenantId?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'string',
  })
  accountNumber?: string;

  @property({
    type: 'string',
  })
  routingNumber?: string;

  @property({
    type: 'string',
  })
  accountName?: string;

  @property({
    type: 'string',
  })
  bankName?: string;

  @property({
    type: 'string',
  })
  bankAddress?: string;

  @property({
    type: 'object',
  })
  bankContact?: {
    name?: string;
    phone?: string;
    email?: string;
  };

  @property({
    type: 'string',
  })
  swiftCode?: string;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        providerMeta: {
          type: 'object',
          patternProperties: {
            typeId: {type: 'string'},
            accountStatus: {type: 'string'},
            accountApplicationId: {type: 'string'},
            accountId: {type: 'string'},
          },
        },
        createdAt: {type: 'string', format: 'date-time'},
        updatedAt: {type: 'string', format: 'date-time'},
        bankName: {type: 'string'},
        accountNumber: {type: 'string'},
        routingNumber: {type: 'string'},
        accountName: {type: 'string'},
      },
    },
  })
  providerMeta?: {
    typeId?: string;
    accountStatus?: string;
    accountApplicationId?: string;
    accountId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [prop: string]: any;
  };

  @property({
    type: 'string',
  })
  tenantIdForFees?: string;

  @property({
    type: 'string',
  })
  tenantIdForBlueSky?: string;
  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<BankAccount>) {
    super(data);
  }
}

export interface BankAccountRelations {
  // describe navigational properties here
}

export type BankAccountWithRelations = BankAccount & BankAccountRelations;
