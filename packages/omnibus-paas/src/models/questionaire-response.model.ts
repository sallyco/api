import {getJsonSchemaRef} from '@loopback/rest';
import {
  Entity,
  model,
  property,
  defineModelClass,
  ModelDefinition,
} from '@loopback/repository';

@model({
  title: 'QuestionaireResponse',
  description: `
Questions have answers
`,
  settings: {
    strict: 'filter',
  },
})
export class QuestionaireResponseBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'Category Name',
      description:
        'This is the formal name of the category that appears on statements, invoices, etc.',
      maxLength: 128,
    },
  })
  name: string;

  @property({
    type: 'string',
    jsonSchema: {
      title: 'Category Description',
      description:
        'Category description for additional specification or clarification of the category and its usage',
      maxLength: 1024,
    },
  })
  description?: string;
  constructor(data?: Partial<QuestionaireResponseBase>) {
    super(data);
  }
}

@model()
export class QuestionaireResponse extends QuestionaireResponseBase {
  @property({
    id: true,
    type: 'string',
    generated: true,
    mongodb: {dataType: 'ObjectId'},
    jsonSchema: {
      readOnly: true,
    },
  })
  id: string;

  constructor(data?: Partial<QuestionaireResponse>) {
    super(data);
  }
}

export interface QuestionaireResponseRelations {}

export type QuestionaireResponseWithRelations = QuestionaireResponse &
  QuestionaireResponseRelations;

// DTO
export const QuestionaireResponseCreate = defineModelClass<
  typeof QuestionaireResponse,
  {}
>(QuestionaireResponse, new ModelDefinition('questionaireResponseCreate'));

export const QuestionaireResponseUpdate = defineModelClass<
  typeof QuestionaireResponse,
  {id: string}
>(
  QuestionaireResponse,
  new ModelDefinition('questionaireResponseEdit').addProperty('id', {
    type: 'string',
    jsonSchema: {readOnly: true},
  }),
);

// SCHEMA
export const QuestionaireResponseSchema =
  getJsonSchemaRef(QuestionaireResponse);

export const QuestionaireResponseCreateSchema = getJsonSchemaRef(
  QuestionaireResponseCreate,
  {
    title: 'Create a QuestionaireResponse',
    exclude: ['id'],
  },
);
export const QuestionaireResponseUpdateSchema = getJsonSchemaRef(
  QuestionaireResponseUpdate,
  {
    title: 'Edit a QuestionaireResponse',
  },
);
