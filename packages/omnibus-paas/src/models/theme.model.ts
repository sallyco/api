import {Entity, model, property} from '@loopback/repository';

/**
 * The model class is generated from OpenAPI schema - Theme
 * A representation of a theme
 */
@model({
  name: 'Theme',
  settings: {
    hiddenProperties: ['ownerId', 'isDeleted', 'deletedAt'],
  },
})
export class Theme extends Entity {
  constructor(data?: Partial<Theme>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Theme ID
   */
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectId'},
  })
  id: string;

  /**
   * User ID
   */
  @property({
    type: 'string',
  })
  tenantId: string;

  /**
   * The name of the theme
   */
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  name: string;

  /**
   * The S3 key for the theme
   */
  @property({
    type: 'string',
    required: true,
  })
  key: string;

  /**
   * The number of milliseconds since the Unix epoch
   */
  @property({
    type: 'string',
  })
  lastModified?: string;

  /**
   * The date the theme object was created
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  /**
   * The date the theme object was updated
   */
  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  /**
   * The date the theme object was deleted
   */
  @property({
    type: 'date',
  })
  deletedAt: Date;

  /**
   * Whether or not the theme object has been deleted
   */
  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  /**
   * Additional information for the entity
   */
  @property()
  additionalProperties?: {};
}

export interface ThemeRelations {
  // describe navigational properties here
}

export type ThemeWithRelations = Theme & ThemeRelations;
