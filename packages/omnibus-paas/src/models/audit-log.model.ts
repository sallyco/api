import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

export enum Action {
  INSERT_ONE = 'INSERT_ONE',
  INSERT_MANY = 'INSERT_MANY',
  UPDATE_ONE = 'UPDATE_ONE',
  UPDATE_MANY = 'UPDATE_MANY',
  DELETE_ONE = 'DELETE_ONE',
  DELETE_MANY = 'DELETE_MANY',
}

@model({
  name: 'AuditLog',
  settings: {
    description: 'Essentially a record of events and changes',
    strict: false,
  },
})
export class AuditLog extends Entity {
  constructor(data?: Partial<AuditLog>) {
    super(data);
  }

  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  action: Action;

  @property({
    name: 'actedAt',
    type: 'date',
    required: true,
  })
  actedAt: Date;

  @property({
    name: 'actedOn',
    type: 'string',
  })
  actedOn?: string;

  @property({
    name: 'actionKey',
    type: 'string',
    required: true,
  })
  actionKey: string;

  @property({
    name: 'tenantId',
    type: 'string',
  })
  tenantId?: string;

  @property({
    name: 'entityId',
    type: 'string',
    required: true,
  })
  entityId: string;

  @property({
    type: 'object',
  })
  before?: object;

  @belongsTo(() => User, {name: 'actionActor', keyTo: 'id'})
  actor: string;

  @property({
    type: 'object',
  })
  after?: object;
}
