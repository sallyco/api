import {model, property} from '@loopback/repository';
import {Invite} from './invite.model';

/**
 * The model class is generated from OpenAPI schema - InvitesList
 * List of invites
 */
@model({name: 'InvitesList'})
export class InvitesList {
  constructor(data?: Partial<InvitesList>) {
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  /**
   * Invites
   */
  @property.array(Invite)
  data?: Invite[];

  /**
   * The current page of results
   */
  @property()
  page?: number;

  /**
   * The number or results per page
   */
  @property()
  perPage?: number;

  /**
   * The total number of results
   */
  @property()
  totalCount?: number;
}

export interface InvitesListRelations {
  // describe navigational properties here
}

export type InvitesListWithRelations = InvitesList & InvitesListRelations;
