import {getJsonSchemaRef} from '@loopback/rest';
import {
  //  DefaultCrudRepository,
  defineModelClass,
  ModelDefinition,
  belongsTo,
  Entity,
  model,
  property,
  hasOne,
} from '@loopback/repository';
import {importable} from '../decorators/importable-model-decorator';
import {Deal} from './deal.model';
import {LegalIncOrderModel} from './legalinc-order.model';
import {Company} from './company.model';
import {FormD} from './form-d.model';
import {User} from './user.model';
//import {MongodbDataSource} from '../datasources';
//import _ from 'lodash';

@model()
class EntitiesBase extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      title: 'yeah',
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Name',
    description: 'Legal Name of the Entity',
  })
  name: string;

  constructor(data?: Partial<Entities>) {
    super(data);
  }
}

@model({
  name: 'Entities',
  settings: {
    hiddenProperties: ['isDeleted', 'deletedAt'],
    strict: 'filter',
  },
})
export class Entities extends EntitiesBase {
  constructor(data?: Partial<Entities>) {
    super(data);
    if (data != null && typeof data === 'object') {
      Object.assign(this, data);
    }
  }

  @property({
    type: 'string',
    id: true,
  })
  @importable({
    displayName: 'ID',
  })
  id: string;
  @property({type: 'string'})
  tenantId?: string;

  @belongsTo(() => Deal, {name: 'deal', keyFrom: 'dealId', keyTo: 'id'})
  @importable({
    displayName: 'Deal ID',
  })
  dealId: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
      ],
    },
  })
  @importable({
    displayName: 'Type',
    description: 'The Type of Entity',
  })
  entityType:
    | 'LIMITED_LIABILITY_COMPANY'
    | 'LIMITED_PARTNERSHIP'
    | 'C_CORPORATION';

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['REGULATION_D', 'REGULATION_S', 'REGULATION_D_S'],
    },
  })
  @importable({
    displayName: 'Regulation Type',
    description: 'The Regulation Type for the Entity',
  })
  regulationType: 'REGULATION_D' | 'REGULATION_S' | 'REGULATION_D_S';

  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Reg D Exemption',
  })
  regDExemption: string;

  @property({
    type: 'number',
    required: true,
    jsonSchema: {
      minimum: 0,
      maximum: 6000000,
    },
  })
  @importable({
    displayName: 'Minimum Investment Amount',
    description:
      'Minimum investment amount required to participate in the deal',
  })
  minInvestmentAmount: number;

  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
    },
  })
  @importable({
    displayName: 'Country of Formation',
    description: 'The Country the Entity was formed in',
  })
  countryOfFormation: string;

  @property({
    type: 'string',
    jsonSchema: {
      minLength: 2,
      maxLength: 1024,
      nullable: true,
    },
  })
  @importable({
    displayName: 'State of Formation',
    description: 'The State the Entity was formed in',
  })
  stateOfFormation?: string;

  @property({
    type: 'object',
    jsonSchema: {
      type: 'object',
      patternProperties: {
        operatingAgreement: {type: 'string'},
        privatePlacementMemorandum: {type: 'string'},
        subscriptionAgreement: {type: 'string'},
        einLetter: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Documents',
    description: 'The Legal Documents for the Entity',
  })
  entityDocuments?: {
    operatingAgreement?: string;
    privatePlacementMemorandum?: string;
    subscriptionAgreement?: string;
    einLetter?: string;
    purchaseAgreement?: string[];
  };

  @property.array('string')
  @importable({
    displayName: 'Files',
    description:
      'Additional Investor-accessible Files associated with this entity',
  })
  files?: string[];

  @property({
    type: 'string',
    jsonSchema: {
      pattern: /^(0[1-9]|[1-9]\d)-\d{7}$/.source,
    },
  })
  @importable({
    displayName: 'EIN',
    description: 'The EIN for the Entity',
  })
  ein?: string;

  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Asset Composition',
  })
  assetComposition?: string;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        city: {type: 'string'},
        state: {type: 'string'},
        country: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Arbitration',
  })
  arbitration?: {
    city: string;
    state: string;
    country: string;
  };

  @property({
    type: 'string',
    jsonSchema: {nullable: true},
  })
  @importable({
    displayName: 'Registered Agent',
  })
  registeredAgent?: string;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        providerMeta: {
          type: 'object',
          patternProperties: {
            typeId: {type: 'string'},
            accountStatus: {type: 'string'},
            accountApplicationId: {type: 'string'},
            accountId: {type: 'string'},
          },
        },
        createdAt: {type: 'string', format: 'date-time'},
        updatedAt: {type: 'string', format: 'date-time'},
        bankName: {type: 'string'},
        accountNumber: {type: 'string'},
        routingNumber: {type: 'string'},
        accountName: {type: 'string'},
        bankContact: {
          type: 'object',
          patternProperties: {
            name: {type: 'string'},
            phone: {type: 'string'},
            email: {type: 'string'},
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Bank Account',
    description: 'The Bank Account associated with the Entity',
  })
  bankAccount?: {
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      [prop: string]: any;
    };
    createdAt?: Date;
    updatedAt?: Date;
    bankName?: string;
    accountNumber?: string;
    routingNumber?: string;
    accountName?: string;
    swiftCode?: string;
    bankContact?: {
      name?: string;
      phone?: string;
      email?: string;
    };
  };

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        amount: {type: 'number'},
        duration: {type: 'number'},
        frequency: {type: 'string'},
        percent: {type: 'number'},
        type: {type: 'string'},
        feesPaymentMethod: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Management Fee',
  })
  managementFee?: {
    amount?: number;
    duration?: number;
    frequency?: string;
    percent?: number;
    type?: 'percent' | 'flat';
    feesPaymentMethod?: string;
    isRecurring?: boolean;
  };

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        amount: {type: 'number'},
        type: {type: 'string'},
      },
      nullable: true,
    },
  })
  @importable({
    displayName: 'Expense Reserve',
  })
  expenseReserve?: {
    amount?: number;
    type?: 'Flat Fee' | 'Percent';
  };

  @property(LegalIncOrderModel)
  legalIncOrder?: Partial<LegalIncOrderModel>;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'date',
  })
  deletedAt: Date;

  @property({
    type: 'boolean',
    default: () => false,
  })
  isDeleted: boolean;

  @belongsTo(() => Company, undefined, {
    required: false,
    jsonSchema: {nullable: true},
  })
  managerId?: string;

  @belongsTo(() => Company, undefined, {
    required: false,
    jsonSchema: {nullable: true},
  })
  masterEntityId?: string;

  @hasOne(() => FormD)
  formD: FormD;

  @belongsTo(() => User)
  ownerId: string;

  @property({required: false, jsonSchema: {nullable: true}})
  additionalProperties?: {};
}

export interface EntitiesRelations {
  // describe navigational properties here
}

export type EntitiesWithRelations = Entities & EntitiesRelations;

// DTO
export const EntitiesCreate = defineModelClass<typeof Entities, {}>(
  Entities,
  new ModelDefinition('entitiesCreate'),
);

const DefineEntitiesEdit = async () => {
  // TODO(Andrew): wrap this mad science into something, service?
  //const ds = new MongodbDataSource();
  //const productCategoryRepository = new DefaultCrudRepository(Entities, ds);
  //const pc = await productCategoryRepository.find();
  return new ModelDefinition('entitiesUpdate').addProperty('id', {
    type: 'string',
    jsonSchema: {readOnly: true},
  });
};
export const EntitiesUpdate = async () =>
  defineModelClass<typeof EntitiesBase, {id: string}>(
    EntitiesBase,
    await DefineEntitiesEdit(),
  );

// SCHEMA
export const EntitiesSchema = getJsonSchemaRef(Entities);

export const EntitiesCreateSchema = getJsonSchemaRef(EntitiesCreate, {
  title: 'Create a New Entity',
  exclude: ['id'],
});
export const EntitiesUpdateSchema = async () =>
  getJsonSchemaRef(await EntitiesUpdate(), {title: 'Edit an Entity'});
