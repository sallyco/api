import {Entity, model, property, hasMany} from '@loopback/repository';
import {BlueSkyFeeTier} from './blue-sky-fee-tier.model';

@model({
  title: 'BlueSkyFee',
  description: 'EFD ‐ Form D Fee Schedule',
  settings: {
    postgresql: {schema: 'accounting', table: 'blue_sky_fee'},
  },
})
export class BlueSkyFee extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'state',
      dataType: 'text',
      nullable: 'NO',
    },
  })
  state: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'exemption',
      dataType: 'text',
      nullable: 'NO',
    },
    jsonSchema: {
      enum: ['b', 'c'],
    },
  })
  exemption: 'b' | 'c';

  @property({
    type: 'string',
    default: 'Electronic',
    postgresql: {
      columnName: 'payment_method',
      dataType: 'text',
      nullable: 'NO',
      default: 'Electronic',
    },
    jsonSchema: {
      enum: ['Electronic', 'Check'],
    },
  })
  paymentMethod: 'Electronic' | 'Check';

  @property({
    type: 'string',
    required: true,
    postgresql: {
      columnName: 'type',
      dataType: 'text',
      nullable: 'NO',
    },
    jsonSchema: {
      enum: [
        'Fixed',
        'Variable',
        'Fixed & Variable',
        'Tiered',
        'No Fee',
        'No File',
      ],
    },
  })
  type:
    | 'Fixed'
    | 'Variable'
    | 'Fixed & Variable'
    | 'Tiered'
    | 'No Fee'
    | 'No File';

  @property({
    type: 'number',
    postgresql: {
      columnName: 'fee',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'YES',
    },
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  fee?: number;

  @property({
    type: 'string',
    postgresql: {
      columnName: 'variable_base',
      dataType: 'text',
      nullable: 'YES',
    },
    jsonSchema: {
      default: null,
      nullable: true,
      enum: ['', 'State Offering', 'EDGAR Offering'],
    },
  })
  variableBase?: '' | 'State Offering' | 'EDGAR Offering';

  @property({
    type: 'number',
    postgresql: {
      columnName: 'variable_percent',
      dataType: 'decimal',
      nullable: 'YES',
    },
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  variablePercent?: number;

  @property({
    type: 'number',
    postgresql: {
      columnName: 'variable_min',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'YES',
    },
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  variableMin?: number;

  @property({
    type: 'number',
    postgresql: {
      columnName: 'variable_max',
      dataType: 'integer',
      dataLength: null,
      dataPrecision: null,
      dataScale: 0,
      nullable: 'YES',
    },
    jsonSchema: {
      default: 0,
      nullable: true,
    },
  })
  variableMax?: number;

  @property({
    type: 'string',
    postgresql: {
      columnName: 'late_fee',
      dataType: 'text',
      nullable: 'YES',
    },
    jsonSchema: {
      nullable: true,
    },
  })
  lateFee?: string;

  @hasMany(() => BlueSkyFeeTier, {keyTo: 'blueSkyFeeId'})
  blueSkyFeeTiers?: BlueSkyFeeTier[];

  constructor(data?: Partial<BlueSkyFee>) {
    super(data);
  }
}

export interface BlueSkyFeeRelations {
  // describe navigational properties here
}

export type BlueSkyFeeWithRelations = BlueSkyFee & BlueSkyFeeRelations;
