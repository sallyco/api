import {BindingKey} from '@loopback/core';
import {MultiTenancyAction, Tenant} from './types';

export namespace MultiTenancyBindings {
  export const ACTION = BindingKey.create<MultiTenancyAction>(
    'sequence.actions.multi-tenancy',
  );

  export const CURRENT_TENANT = BindingKey.create<Tenant>(
    'multi-tenancy.currentTenant',
  );
}

export const MULTI_TENANCY_STRATEGIES = 'multi-tenancy.strategies';
