import {getService} from '@loopback/service-proxy';
import {inject, Provider} from '@loopback/core';
import {CamundaDataSource} from '../../datasources';

export interface Camunda {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  [methodName: string]: (...args: any[]) => Promise<any>;
  getRestAPIVersion(): Promise<object>;
  getProcessEngineNames(): Promise<[]>;
  getProcessDefinitions(): Promise<[]>;
}

export class CamundaProvider implements Provider<Camunda> {
  constructor(
    // camunda must match the name property in the datasource json file
    @inject('datasources.camunda')
    protected dataSource: CamundaDataSource = new CamundaDataSource(),
  ) {}

  value(): Promise<Camunda> {
    return getService(this.dataSource);
  }
}
