import {config, ContextTags, Getter, Provider} from '@loopback/context';
import {extensionPoint, extensions} from '@loopback/core';
import {HttpErrors, RequestContext} from '@loopback/rest';
import debugFactory from 'debug';
import {MultiTenancyBindings, MULTI_TENANCY_STRATEGIES} from '../keys';
import * as Sentry from '@sentry/node';
import {
  MultiTenancyAction,
  MultiTenancyActionOptions,
  MultiTenancyStrategy,
} from '../types';
const debug = debugFactory('loopback:multi-tenancy:action');
/**
 * Provides the multi-tenancy action for a sequence
 */
@extensionPoint(MULTI_TENANCY_STRATEGIES, {
  tags: {
    [ContextTags.KEY]: MultiTenancyBindings.ACTION,
  },
})
export class MultiTenancyActionProvider
  implements Provider<MultiTenancyAction>
{
  constructor(
    @extensions()
    private readonly getMultiTenancyStrategies: Getter<MultiTenancyStrategy[]>,
    @config()
    private options: MultiTenancyActionOptions = {strategyNames: ['header']},
  ) {}

  /**
   * @returns MultiTenancyStrategyFactory
   */
  value(): MultiTenancyAction {
    return this.action.bind(this);
  }

  /**
   * The implementation of authenticate() sequence action.
   * @param request - The incoming request provided by the REST layer
   */
  async action(requestCtx: RequestContext) {
    debug('Identifying tenant for request %s', requestCtx.basePath);
    const tenancy = await this.identifyTenancy(requestCtx);
    if (tenancy == null) return;
    debug(
      'Tenant identified by strategy %s',
      tenancy.strategy.name,
      tenancy.tenant,
    );
    debug('Binding resources for tenant', tenancy.tenant);
    Sentry.configureScope(scope => {
      scope.setTag('tenant', tenancy.tenant.id);
    });
    requestCtx.bind(MultiTenancyBindings.CURRENT_TENANT).to(tenancy.tenant);
    await tenancy.strategy.bindResources(requestCtx, tenancy.tenant);
    return tenancy.tenant;
  }

  private async identifyTenancy(requestCtx: RequestContext) {
    debug('Tenancy action is configured with', this.options);
    const strategyNames = this.options.strategyNames;
    let strategies = await this.getMultiTenancyStrategies();
    strategies = strategies
      .filter(s => strategyNames.includes(s.name))
      .sort((a, b) => {
        return strategyNames.indexOf(a.name) - strategyNames.indexOf(b.name);
      });
    if (debug.enabled) {
      debug(
        'Tenancy strategies',
        strategies.map(s => s.name),
      );
    }
    for (const strategy of strategies) {
      debug('Trying tenancy strategy %s', strategy.name);
      const tenant = await strategy.identifyTenant(requestCtx);
      if (tenant != null) {
        debug('Tenant is now identified by strategy %s', strategy.name, tenant);
        return {tenant, strategy};
      }
      //CALL
    }
    debug('No tenant is identified');
    throw new HttpErrors.Unauthorized(`Tenant ID Not specified for request`);
  }
}
