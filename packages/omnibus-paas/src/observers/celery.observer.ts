import {
  Application,
  CoreBindings,
  inject,
  lifeCycleObserver,
  LifeCycleObserver,
  service,
} from '@loopback/core';
import {CeleryWorkerService} from '../services';

/**
 * This class will be bound to the application as a `LifeCycleObserver` during
 * `boot`
 */
@lifeCycleObserver('daemons')
export class CeleryObserver implements LifeCycleObserver {
  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE) private app: Application,
    @service(CeleryWorkerService) private celeryWorker: CeleryWorkerService,
  ) {}

  /**
   * This method will be invoked when the application initializes. It will be
   * called at most once for a given application instance.
   */
  async init(): Promise<void> {}

  /**
   * This method will be invoked when the application starts.
   */
  async start(): Promise<void> {
    await this.celeryWorker.start();
  }

  /**
   * This method will be invoked when the application stops.
   */
  async stop(): Promise<void> {
    // This isn't currently implemented in celery-node
    // eslint-disable-next-line no-void
    // void this.celeryWorker.start();
  }
}
