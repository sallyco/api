/* eslint-disable @typescript-eslint/no-explicit-any */
declare module 'synapsenode' {
  import {AxiosResponse} from 'axios';

  export interface Options {
    fingerprint?: string;
    ip_address?: string;
    full_dehydrate?: string;
    idempotency_key?: string;
  }

  export interface Document {
    email: string;
    phone_number: string;
    ip: string;
    name: string;
    alias?: string;
    entity_type: string;
    entity_scope: string;
    day: number;
    month: number;
    year: number;
    address_street: string;
    address_city: string;
    address_subdivision: string;
    address_postal_code: string;
    address_country_code: string;
    virtual_docs: AlDoc[];
    physical_docs: AlDoc[];
    social_docs: AlDoc[];
  }

  export interface AlDoc {
    document_value: string;
    document_type: string;
  }

  export interface Extra {
    supp_id?: string;
    cip_tag?: number;
    is_business?: boolean;
    other?: object;
  }

  export interface Login {
    email: string;
  }

  export enum NodeTypes {
    'ACH-US' = 'ACH-US', //	Link w/ Bank Login or Link w/ Account/Routing
    'CARD-US' = 'CARD-US', //[Deprecated]
    'CHECK-US' = 'CHECK-US', //Create Check Recipient
    'CLEARING-US' = 'CLEARING-US', //Create Clearing Account
    'CRYPTO-US' = 'CRYPTO-US', //Create Crypto Wallet
    'CUSTODY-US' = 'CUSTODY-US', //Create Custody Account
    'DEPOSIT-US' = 'DEPOSIT-US', //Create Deposit Account
    'EXTERNAL-US' = 'EXTERNAL-US', //Refers to transactions initiated by external parties (i.e. not by Synapse). Examples include incoming wires, incoming ACH credits or debits, use of our issued cards at points of sale (POS), and ATM card withdrawals."IB-DEPOSIT-US"="IB-DEPOSIT-US",	//Create IB Account
    'IB-SUBACCOUNT-US' = 'IB-SUBACCOUNT-US', //Create IB Account
    'IC-DEPOSIT-US' = 'IC-DEPOSIT-US', //Create Interest Carrying Deposit Account
    'INTERCHANGE-US' = 'INTERCHANGE-US', //Link Card
    'IOU' = 'IOU', //Create IOU Account
    'LOAN-US' = 'LOAN-US', //Apply for One-Time Loan or Apply for Revolving Loan
    'RESERVE-US' = 'RESERVE-US', //For platform use
    'SUBCARD-US' = 'SUBCARD-US', //[Deprecated]
    'SYNAPSE-US' = 'SYNAPSE-US', //[Deprecated]
    'TRIUMPH-SUBACCOUNT-US' = 'TRIUMPH-SUBACCOUNT-US', //[Deprecated]
    'WIRE-US' = 'WIRE-US', //Link Wire Account (Domestic)
    'WIRE-INT' = 'WIRE-INT', //Link Wire Account (International)
  }

  export interface NodeCreateResponse {
    error_code: string;
    http_code: string;
    limit: number;
    node_count: number;
    nodes: Node[];
    page_count: number;
    success: boolean;
  }

  export interface Node {
    _id?: string;
    _links?: Links;
    allowed?: string;
    client?: Client;
    extra?: Extra;
    info: NodeInfo;
    is_active?: boolean;
    timeline?: Timeline[];
    type: string;
    user_id?: string;
  }

  export interface Links {
    self: SelfLink;
  }

  export interface SelfLink {
    href: string;
  }

  export interface NodeInfo {
    balance?: NodeBalance;
    document_id?: string;
    monthly_withdrawals_remaining?: null;
    name_on_account?: string;
    nickname?: string;
    account_num?: string | number;
    routing_num?: string | number;
    class?: string;
    type?: string;
  }

  export interface NodeBalance {
    amount: number;
    currency: string;
  }

  export interface Timeline {
    date: number;
    note: string;
  }

  export class Client {
    id: string;
    name: string;

    constructor(options: {
      client_id: string;
      client_secret: string;
      fingerprint: string;
      ip_address: string;
      isProduction: boolean;
    });

    createUser(
      bodyParams: {
        logins: Login[];
        phone_numbers: string[];
        legal_names: string[];
        documents: Document[];
        extra?: Extra;
      },
      ip_address: string,
      options?: Options,
    ): Promise<SynapseResponse<UserResponse>>;

    getUser(user_id: string, options?: Options): Promise<User>;
  }

  export class User implements UserResponse {
    _id: string;
    _links: LinksResponse;
    client: Client;
    documents: DocumentResponse[];
    emails: string[];
    extra: ExtraResponse;
    is_hidden: boolean;
    legal_names: string[];
    logins: LoginResponse[];
    permission: string;
    phone_numbers: string[];
    photos: any[];
    refresh_token: string;
    watchlists: string;

    constructor({client}: {client: Client});

    createNode(nodeData: Node): Promise<AxiosResponse<NodeResponse>>;
  }

  export interface SynapseResponse<BodyType> {
    id: string;
    host: string;
    ip_address: string;
    fingerprint: string;
    body: BodyType;
  }

  export interface NodeResponse {
    node_count: number;
    nodes: Node[];
  }

  export interface UserResponse {
    _id: string;
    _links: LinksResponse;
    client: Client;
    documents: DocumentResponse[];
    emails: string[];
    extra: ExtraResponse;
    is_hidden: boolean;
    legal_names: string[];
    logins: LoginResponse[];
    permission: string;
    phone_numbers: string[];
    photos: any[];
    refresh_token: string;
    watchlists: string;
  }

  export interface LinksResponse {
    self: SelfResponse;
  }

  export interface SelfResponse {
    href: string;
  }

  export interface DocumentResponse {
    entity_scope: string;
    entity_type: string;
    id: string;
    id_score: number;
    name: string;
    permission_scope: string;
    physical_docs: any[];
    social_docs: SocialDocResponse[];
    virtual_docs: any[];
    watchlists: string;
  }

  export interface SocialDocResponse {
    document_type: string;
    id: string;
    last_updated: number;
    status: string;
  }

  export interface ExtraResponse {
    cip_tag: number;
    date_joined: number;
    extra_security: boolean;
    is_business: boolean;
    is_trusted: boolean;
    last_updated: number;
    public_note: null;
    supp_id: string;
  }

  export interface LoginResponse {
    email: string;
    scope: string;
  }
}
