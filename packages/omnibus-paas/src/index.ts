import {MongodbDataSource} from './datasources';
export * from './application';

if ((process.env.ENVIRONMENT ?? 'local') !== 'development') {
  require('dotenv').config({
    debug: true,
    path: '/vault/secrets/omnibus-paas',
  });
}

import tracer from 'dd-trace';
if ((process.env.ENVIRONMENT ?? 'local') !== 'local') {
  tracer.init();
}
export default tracer;

import {ApplicationConfig} from '@loopback/core';
import {MetricsComponent} from '@loopback/extension-metrics';
import * as Sentry from '@sentry/node';
import {OmnibusPaasApplication} from './application';
import mainSeeder from './dataSeeders';
Sentry.init({
  dsn:
    process.env.SENTRY_DSN ??
    'https://d67c0f37d3bf430298254d829615b7ef@sentry.gbt.sh/2',
  environment: process.env.ENVIRONMENT ?? 'local',
});

export {OmnibusPaasApplication};
export async function main(
  options: ApplicationConfig = {
    primaryDatabase: MongodbDataSource.dataSourceName,
  },
) {
  const app = new OmnibusPaasApplication(options);
  await app.boot();
  await app.migrateSchema({
    // Sets loading order for models
    // Used for managing Foreign Key dependencies
    models: ['BlueSkyFee', 'BlueSkyFeeTier'],
  });
  app.component(MetricsComponent);
  await app.start();

  const url = app.restServer.url;
  const start = String.fromCodePoint(0x2705);
  await mainSeeder(app);
  console.log(`${start} omnibus-paas is running at ${url}`);

  return app;
}

if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      port: +(process.env.OMNIBUS_PORT ?? process.env.PORT ?? 3001),
      host: process.env.HOST ?? '0.0.0.0',
      gracePeriodForClose: 15000,
      openApiSpec: {
        servers: [{url: 'https://api.sandbox.glassboardtech.com/v1'}],
        setServersFromRequest: false,
        endpointMapping: {
          '/openapi.json': {version: '3.0.0', format: 'json'},
          '/openapi.yaml': {version: '3.0.0', format: 'yaml'},
        },
      },
      cors: {
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 204,
        maxAge: 86400,
        credentials: true,
      },
    },
    primaryDatabase: 'mongodb',
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
export {PDFHelpers} from './utils/documentUtils/PDFHelpers';
