/* eslint-disable @typescript-eslint/no-explicit-any */
import {MixinTarget} from '@loopback/core';
import {
  Count,
  DataObject,
  Entity,
  EntityCrudRepository,
  FilterBuilder,
  Options,
  Where,
} from '@loopback/repository';
import _, {keyBy} from 'lodash';

import {Action, AuditLog} from '../models';
import {AuditLogRepository} from '../repositories';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../multi-tenancy';
import {IAuditMixin, IAuditMixinOptions} from '../types';
import {WebhookEventTypes, WebhookService} from '../services';
import {Getter} from '@loopback/context';
import {Filter} from '@loopback/filter';
import legacy from 'loopback-datasource-juggler';

export function AuditRepositoryMixin<
  M extends Entity,
  ID,
  Relations extends object | {},
  R extends MixinTarget<EntityCrudRepository<M, ID, Relations>>,
>(superClass: R, opts: IAuditMixinOptions) {
  class MixedRepository extends superClass implements IAuditMixin {
    getAuditLogRepository: () => Promise<AuditLogRepository>;
    profile?: UserProfile;
    tenant?: Tenant;
    getWebhookService?: Getter<WebhookService>;

    normalizeFilter(filter?: Filter<any>): legacy.Filter | undefined {
      const filterBuilder = new FilterBuilder(filter);
      if (this.tenant && this.tenant.id !== 'master') {
        filterBuilder.impose({
          tenantId: this.tenant.id,
        });
      }
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return super.normalizeFilter(filterBuilder.filter) as legacy.Filter;
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async create(dataObject: DataObject<M>, options?: Options): Promise<M> {
      const created = await super.create(dataObject, options);
      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const audit = new AuditLog({
          actedAt: new Date(),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          actor: (user?.id as any).toString() ?? '0',
          action: Action.INSERT_ONE,
          after: created.toJSON(),
          entityId: created.getId(),
          actedOn: this.entityClass.modelName,
          actionKey: opts.actionKey,
          tenantId: this.tenant?.id,
          ...extras,
        });
        auditRepo.create(audit).catch(() => {
          console.error(
            `Audit failed for data => ${JSON.stringify(audit.toJSON())}`,
          );
        });
      }
      await this.processWebhookEvent(created, WebhookEventTypes.CREATE);
      return created;
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async createAll(
      dataObjects: DataObject<M>[],
      options?: Options,
    ): Promise<M[]> {
      const created = await super.createAll(dataObjects, options);
      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const audits = created.map(
          data =>
            new AuditLog({
              actedAt: new Date(),
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              actor: (user?.id as any).toString() ?? '0',
              action: Action.INSERT_MANY,
              after: data.toJSON(),
              entityId: data.getId(),
              actedOn: this.entityClass.modelName,
              actionKey: opts.actionKey,
              tenantId: this.tenant?.id,
              ...extras,
            }),
        );
        auditRepo.createAll(audits).catch(() => {
          const auditsJson = audits.map(a => a.toJSON());
          console.error(
            `Audit failed for data => ${JSON.stringify(auditsJson)}`,
          );
        });
      }
      await this.processWebhookEvent(created, WebhookEventTypes.CREATE);
      return created;
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async updateAll(
      dataObject: DataObject<M>,
      where?: Where<M>,
      options?: Options,
    ): Promise<Count> {
      const toUpdate = await this.find({where});
      const beforeMap = keyBy(toUpdate, d => d.getId());
      const updatedCount = await super.updateAll(dataObject, where, options);
      const updated = await this.find({where});

      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const audits = updated.map(
          data =>
            new AuditLog({
              actedAt: new Date(),
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              actor: (user?.id as any).toString() ?? '0',
              action: Action.UPDATE_MANY,
              before: (beforeMap[data.getId()] as Entity).toJSON(),
              after: data.toJSON(),
              entityId: data.getId(),
              actedOn: this.entityClass.modelName,
              actionKey: opts.actionKey,
              tenantId: this.tenant?.id,
              ...extras,
            }),
        );
        auditRepo.createAll(audits).catch(() => {
          const auditsJson = audits.map(a => a.toJSON());
          console.error(
            `Audit failed for data => ${JSON.stringify(auditsJson)}`,
          );
        });
      }

      return updatedCount;
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async deleteAll(where?: Where<M>, options?: Options): Promise<Count> {
      const toDelete = await this.find({where});
      const beforeMap = keyBy(toDelete, d => d.getId());
      const deletedCount = await super.deleteAll(where, options);

      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const audits = toDelete.map(
          data =>
            new AuditLog({
              actedAt: new Date(),
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              actor: (user?.id as any).toString() ?? '0',
              action: Action.DELETE_MANY,
              before: (beforeMap[data.getId()] as Entity).toJSON(),
              entityId: data.getId(),
              actedOn: this.entityClass.modelName,
              actionKey: opts.actionKey,
              tenantId: this.tenant?.id,
              ...extras,
            }),
        );
        auditRepo.createAll(audits).catch(() => {
          const auditsJson = audits.map(a => a.toJSON());
          console.error(
            `Audit failed for data => ${JSON.stringify(auditsJson)}`,
          );
        });
      }

      return deletedCount;
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async updateById(
      id: ID,
      data: DataObject<M>,
      options?: Options,
    ): Promise<void> {
      const before = await this.findById(id);
      await super.updateById(id, data, options);
      const after = await this.findById(id);

      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const auditLog = new AuditLog({
          actedAt: new Date(),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          actor: (user?.id as any).toString() ?? '0',
          action: Action.UPDATE_ONE,
          before: before.toJSON(),
          after: after.toJSON(),
          entityId: before.getId(),
          actedOn: this.entityClass.modelName,
          actionKey: opts.actionKey,
          tenantId: this.tenant?.id,
          ...extras,
        });

        auditRepo.create(auditLog).catch(() => {
          console.error(
            `Audit failed for data => ${JSON.stringify(auditLog.toJSON())}`,
          );
        });
      }
      if (!_.isEqual(before, after)) {
        await this.processWebhookEvent(after, WebhookEventTypes.UPDATE);
      }
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async replaceById(
      id: ID,
      data: DataObject<M>,
      options?: Options,
    ): Promise<void> {
      const before = await this.findById(id);
      await super.replaceById(id, data, options);
      const after = await this.findById(id);

      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const auditLog = new AuditLog({
          actedAt: new Date(),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          actor: (user?.id as any).toString() ?? '0',
          action: Action.UPDATE_ONE,
          before: before.toJSON(),
          after: after.toJSON(),
          entityId: before.getId(),
          actedOn: this.entityClass.modelName,
          actionKey: opts.actionKey,
          tenantId: this.tenant?.id,
          ...extras,
        });

        auditRepo.create(auditLog).catch(() => {
          console.error(
            `Audit failed for data => ${JSON.stringify(auditLog.toJSON())}`,
          );
        });
      }
      await this.processWebhookEvent(after, WebhookEventTypes.UPDATE);
    }

    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    async deleteById(id: ID, options?: Options): Promise<void> {
      const before = await this.findById(id);
      await super.deleteById(id, options);

      if (this.profile && this.tenant) {
        const user = this.profile;
        const auditRepo = await this.getAuditLogRepository();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const extras: any = Object.assign({}, opts);
        delete extras.actionKey;
        const auditLog = new AuditLog({
          actedAt: new Date(),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          actor: (user?.id as any).toString() ?? '0',
          action: Action.DELETE_ONE,
          before: before.toJSON(),
          entityId: before.getId(),
          actedOn: this.entityClass.modelName,
          actionKey: opts.actionKey,
          tenantId: this.tenant?.id,
          ...extras,
        });

        auditRepo.create(auditLog).catch(() => {
          console.error(
            `Audit failed for data => ${JSON.stringify(auditLog.toJSON())}`,
          );
        });
      }
      await this.processWebhookEvent(before, WebhookEventTypes.DELETE);
    }

    async processWebhookEvent(object: any, eventType: WebhookEventTypes) {
      if (!this.getWebhookService) {
        return;
      }
      const ws = await this.getWebhookService();
      await ws.processWebhookUpdateForObject(object, eventType);
    }
  }

  return MixedRepository;
}
