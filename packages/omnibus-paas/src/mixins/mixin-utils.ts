import {DefaultCrudRepository} from '@loopback/repository';
import {Constructor} from '@loopback/core';
import {AuditRepositoryMixin} from './audit.mixin';
import {IAuditMixinOptions} from '../types';

// TODO: Needs typing work to fix
//  Should be able to be passed multiple mixins and build a correct typing / constructor
// export function RepositoryMixinBuilder<
//   Model extends Entity,
//   ModelRelations extends object = {},
// >(mixins: ((...any) => MixinTarget<any>)[], options = {}): any {
//   let finalType = DefaultCrudRepository;
//   for (const mixin of mixins) {
//     // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//     // @ts-ignore
//     finalType = mixin<
//       DefaultCrudRepository<
//         Model,
//         typeof Model.prototype.id,
//         ModelRelations
//       >
//     >(
//       finalType,
//       options,
//     );
//   }
//   return finalType as unknown as DefaultCrudRepository<Model, unknown>;
// }

export function AuditedDefaultCrudRepository<ModelRelations extends object>(
  Model,
  options: IAuditMixinOptions,
) {
  return AuditRepositoryMixin<
    typeof Model,
    typeof Model.prototype.id,
    ModelRelations,
    Constructor<
      DefaultCrudRepository<
        typeof Model,
        typeof Model.prototype.id,
        ModelRelations
      >
    >
  >(DefaultCrudRepository, options);
}
