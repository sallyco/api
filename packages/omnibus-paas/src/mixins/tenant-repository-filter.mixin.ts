/* eslint-disable @typescript-eslint/no-explicit-any */
// import {MixinTarget} from '@loopback/core';
// import {
//   EntityCrudRepository, FilterBuilder,
// } from '@loopback/repository';
// import {Tenant} from '../multi-tenancy';
// import {ITenantFilterMixin} from '../types';
// import {Filter} from '@loopback/filter';
// import legacy from 'loopback-datasource-juggler';
//
// export function TenantRepositoryFilterMixin<T extends MixinTarget<EntityCrudRepository<any, any, any>>, M = void> (superClass: T, ...M) {
//   class MixedRepository extends superClass implements ITenantFilterMixin {
//     tenant?: Tenant;
//
//     normalizeFilter(
//       filter?: Filter<any>,
//     ): legacy.Filter | undefined {
//       const filterBuilder = new FilterBuilder(filter);
//       if (this.tenant && this.tenant.id !== 'master') {
//         filterBuilder.impose({
//           // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//           // @ts-ignore
//           tenantId: this.tenant.id,
//         });
//       }
//       // eslint-disable-next-line @typescript-eslint/ban-ts-comment
//       // @ts-ignore
//       return super.normalizeFilter(filterBuilder.filter) as legacy.Filter;
//     }
//   }
//   return MixedRepository;
// }
