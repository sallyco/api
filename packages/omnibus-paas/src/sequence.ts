import {inject} from '@loopback/context';
import {
  FindRoute,
  InvokeMethod,
  ParseParams,
  Reject,
  RequestContext,
  RestBindings,
  Send,
  SequenceHandler,
  InvokeMiddleware,
} from '@loopback/rest';
import {MultiTenancyAction, MultiTenancyBindings} from './multi-tenancy';
import {LogFn, LoggerBindings} from './logger';
import * as Sentry from '@sentry/node';
import {
  AuthenticationBindings,
  AuthenticateFn,
  AUTHENTICATION_STRATEGY_NOT_FOUND,
  USER_PROFILE_NOT_FOUND,
} from '@loopback/authentication';
import {service} from '@loopback/core';
import {Unleash, UnleashProvider} from './services';

const SequenceActions = RestBindings.SequenceActions;

export class MySequence implements SequenceHandler {
  @inject(SequenceActions.INVOKE_MIDDLEWARE, {optional: true})
  protected invokeMiddleware: InvokeMiddleware = () => false;

  constructor(
    @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
    @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
    @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
    @inject(SequenceActions.SEND) public send: Send,
    @inject(SequenceActions.REJECT) public reject: Reject,

    @inject(LoggerBindings.LOG_ACTION)
    protected logger: LogFn,

    @inject(MultiTenancyBindings.ACTION)
    public multiTenancy: MultiTenancyAction,

    @inject(AuthenticationBindings.AUTH_ACTION)
    protected authenticateRequest: AuthenticateFn,

    /*
      Required to initialize the service before the controller grabs the service.
      Gives Unleash time to grab the latest feature flags
      TODO: Move this to Application startup
      Previous attempts moving to application won't keep the cached version when called
    */
    @service(UnleashProvider) unleash: Unleash,
  ) {}

  async handle(context: RequestContext): Promise<void> {
    const {request, response} = context;

    const finished = await this.invokeMiddleware(context);
    if (finished) return;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let args: any = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let result: any;

    try {
      const route = this.findRoute(request);
      if (
        !route.spec.tags?.includes('TenantNotRequired') &&
        !['/', '/metrics'].includes(route.path)
      ) {
        await this.multiTenancy(context);
      }
      await this.authenticateRequest(request);
      args = await this.parseParams(request, route);
      result = await this.invoke(route, args);
      this.send(response, result);

      await this.logger(request, args, result);
    } catch (err) {
      console.log(err);
      if (
        err.code === AUTHENTICATION_STRATEGY_NOT_FOUND ||
        err.code === USER_PROFILE_NOT_FOUND ||
        err.name === 'UnauthorizedError'
      ) {
        Object.assign(err, {statusCode: 401 /* Unauthorized */});
      }
      this.reject(context, err);
      Sentry.captureException(err);
      await this.logger(request, args, result);
    }
  }
}
