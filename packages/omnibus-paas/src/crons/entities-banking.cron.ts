/* eslint-disable @typescript-eslint/no-invalid-this */
import {config, Provider, ValueOrPromise} from '@loopback/context';
import {service} from '@loopback/core';
import {CronJob, cronJob, CronJobConfig} from '@loopback/cron';
import {repository, WhereBuilder} from '@loopback/repository';
import debugFactory from 'debug';
import moment from 'moment';
import {BankAccount, Entities} from '../models';
import {BankAccountRepository, EntitiesRepository} from '../repositories';
import {TreasuryPrimeService, TreasuryPrimeServiceProvider} from '../services';

const debug = debugFactory('omnibus-paas:crons:entities-banking');

@cronJob()
export class EntitiesBankingCron implements Provider<CronJob> {
  constructor(
    @config()
    private readonly jobConfig: CronJobConfig = {},
    @repository(EntitiesRepository)
    public entitiesRepository: EntitiesRepository,
    @repository(BankAccountRepository)
    public bankAccountRepository: BankAccountRepository,
    @service(TreasuryPrimeServiceProvider)
    private readonly treasuryPrimeService: TreasuryPrimeService,
  ) {}
  // In minutes
  private SYNC_INTERVAL = process.env.ENTITY_BANKING_SYNC_INTERVAL ?? 60;
  value(): ValueOrPromise<CronJob> {
    return new CronJob({
      name: 'entities-banking-sync',
      onTick: this.syncEntitiesBanking,
      cronTime: '*/10 * * * * *',
      start: true,
    });
  }

  syncEntitiesBanking = async () => {
    const entitiesWhere = new WhereBuilder();
    entitiesWhere
      .exists('bankAccount.providerMeta.accountStatus')
      .neq('bankAccount.providerMeta.accountStatus', 'approved');
    // eslint-disable-next-line @typescript-eslint/no-invalid-this
    const entities: Entities[] = await this.entitiesRepository.find({
      where: entitiesWhere.build(),
    });
    for (const entity of entities) {
      if (
        entity.bankAccount?.providerMeta?.typeId === 'TREASURYPRIME' &&
        entity.bankAccount.providerMeta?.accountApplicationId
      ) {
        if (
          entity.bankAccount.updatedAt &&
          moment(entity.bankAccount.updatedAt).isAfter(
            moment().subtract(this.SYNC_INTERVAL, 'seconds'),
          )
        ) {
          continue;
        }
        try {
          const accountApplication =
            await this.treasuryPrimeService.getAccountApplication(
              entity.bankAccount.providerMeta.accountApplicationId,
            );
          entity.bankAccount.providerMeta = {
            ...entity.bankAccount.providerMeta,
            ...{
              accountStatus: accountApplication.body.status,
            },
          };
          entity.bankAccount = {
            ...entity.bankAccount,
            ...{
              updatedAt: new Date(),
            },
          };
          if (
            accountApplication.body.status === 'approved' &&
            accountApplication.body.account_id
          ) {
            const account = await this.treasuryPrimeService.getAccount(
              accountApplication.body.account_id,
            );
            entity.bankAccount.providerMeta = {
              ...entity.bankAccount.providerMeta,
              ...{
                accountId: accountApplication.body.account_id,
              },
            };
            entity.bankAccount = {
              ...entity.bankAccount,
              ...{
                bankContact:
                  account.body.bank_id === 'bank_pvbc'
                    ? {
                        name: 'Sara Spaulding',
                        phone: '978-225-1402',
                        email: 'sspaulding@bankprov.com',
                      }
                    : {
                        name: '',
                        phone: '',
                        email: '',
                      },
                bankName:
                  account.body.bank_id === 'bank_pvbc'
                    ? 'The Provident Bank'
                    : account.body.bank_id,
                swiftCode:
                  account.body.bank_id === 'bank_pvbc' ? 'PRDIUS33' : undefined,
                bankAddress:
                  account.body.bank_id === 'bank_pvbc'
                    ? '5 Market Street Amesbury, MA 01913'
                    : '',
                beneficiaryAddress:
                  account.body.bank_id === 'bank_pvbc'
                    ? '6510 Millrock Drive, Ste 400, Salt Lake City, Utah 84121'
                    : '',
                beneficiaryPhone:
                  account.body.bank_id === 'bank_pvbc' ? '801-419-0677 x2' : '',
                accountNumber: account.body.account_number,
                accountName: entity.name,
                routingNumber: account.body.routing_number,
              },
            };
            debug('Successfully synced Banking for Entity ID:', entity.id);
          }
          await this.entitiesRepository.update(entity);
        } catch (e) {
          debug(
            `Issue updating Banking Status for Entity ID: ${entity.id}`,
            e.message,
          );
        }
      }
    }
    await this.syncBankAccounts();
  };

  async syncBankAccounts() {
    const bankingWhere = new WhereBuilder();
    bankingWhere.exists('status').eq('status', 'PENDING');
    const bankAccounts: BankAccount[] = await this.bankAccountRepository.find({
      where: bankingWhere.build(),
    });
    for (let bankAccount of bankAccounts) {
      if (
        bankAccount?.providerMeta?.typeId === 'TREASURYPRIME' &&
        bankAccount.providerMeta?.accountApplicationId
      ) {
        if (
          bankAccount.updatedAt &&
          moment(bankAccount.updatedAt).isAfter(
            moment().subtract(this.SYNC_INTERVAL, 'seconds'),
          )
        ) {
          continue;
        }
        try {
          const accountApplication =
            await this.treasuryPrimeService.getAccountApplication(
              bankAccount.providerMeta.accountApplicationId,
            );
          bankAccount.providerMeta = {
            ...bankAccount.providerMeta,
            ...{
              accountStatus: accountApplication.body.status,
            },
          };
          bankAccount = new BankAccount({
            ...bankAccount,
            ...{
              updatedAt: new Date(),
            },
          });
          if (
            accountApplication.body.status === 'approved' &&
            accountApplication.body.account_id
          ) {
            const account = await this.treasuryPrimeService.getAccount(
              accountApplication.body.account_id,
            );
            bankAccount.providerMeta = {
              ...bankAccount.providerMeta,
              ...{
                accountId: accountApplication.body.account_id,
              },
            };
            bankAccount = new BankAccount({
              ...bankAccount,
              status: 'OPEN',
              ...{
                bankContact:
                  account.body.bank_id === 'bank_pvbc'
                    ? {
                        name: 'Sara Spaulding',
                        phone: '978-225-1402',
                        email: 'sspaulding@bankprov.com',
                      }
                    : {
                        name: '',
                        phone: '',
                        email: '',
                      },
                bankName:
                  account.body.bank_id === 'bank_pvbc'
                    ? 'The Provident Bank'
                    : account.body.bank_id,
                swiftCode:
                  account.body.bank_id === 'bank_pvbc' ? 'PRDIUS33' : undefined,
                bankAddress:
                  account.body.bank_id === 'bank_pvbc'
                    ? '5 Market Street Amesbury, MA 01913'
                    : '',
                beneficiaryAddress:
                  account.body.bank_id === 'bank_pvbc'
                    ? '6510 Millrock Drive, Ste 400, Salt Lake City, Utah 84121'
                    : '',
                beneficiaryPhone:
                  account.body.bank_id === 'bank_pvbc' ? '801-419-0677 x2' : '',
                accountNumber: account.body.account_number,
                routingNumber: account.body.routing_number,
              },
            });
            debug(
              'Successfully synced Banking for Bank Account ID:',
              bankAccount.id,
            );
          }
          await this.bankAccountRepository.update(bankAccount);
        } catch (e) {
          debug(
            `Issue updating Banking Status for Bank Account ID: ${bankAccount.id}`,
            e.message,
          );
        }
      }
    }
  }
}
