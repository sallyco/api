import {ClassDecoratorFactory} from '@loopback/metadata';

export interface WebhookConfigDecoratorMetadata {
  resourceId?: string;
}

export const WEBHOOK_CONFIG_DECORATOR_KEY =
  'metadata-key-for-webhook-config-decorator';

export function webhookConfig(
  spec?: WebhookConfigDecoratorMetadata,
): ClassDecorator {
  return ClassDecoratorFactory.createDecorator<
    WebhookConfigDecoratorMetadata | undefined
  >(WEBHOOK_CONFIG_DECORATOR_KEY, spec);
}
