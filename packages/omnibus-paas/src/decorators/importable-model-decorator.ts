import {PropertyDecoratorFactory} from '@loopback/metadata';

export interface ImportableMetadata {
  displayName?: string;
  description?: string;
}

export const IMPORTABLE_DECORATOR_KEY =
  'metadata-key-for-importable-model-property';

export function importable(spec?: ImportableMetadata): PropertyDecorator {
  return PropertyDecoratorFactory.createDecorator<
    ImportableMetadata | undefined
  >(IMPORTABLE_DECORATOR_KEY, spec);
}
