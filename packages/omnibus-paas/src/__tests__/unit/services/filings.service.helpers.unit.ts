import {expect} from '@loopback/testlab';
import {ILegalInc} from 'interfaces/services/legal-inc.interface';
import {Company, Profile} from 'models';
import {Individual} from 'models/individual.model';
import {getTaxContactFromProfileOrManager} from '../../../services/filings/filings.service.helpers';
const testLocalAddress = {
  address1: 'add1',
  address2: 'add2',
  city: 'city',
  state: 'state',
  postalCode: 'code',
  country: 'country',
};
const testLegalIncAddress: ILegalInc.API.Address = {
  address: testLocalAddress.address1,
  address2: testLocalAddress.address2,
  city: testLocalAddress.city,
  state: testLocalAddress.state,
  zip: testLocalAddress.postalCode,
  country: testLocalAddress.country,
  address_type: 'billing',
};

describe('Manager / Entity Resolution Tests', () => {
  it('builds a tax contact from a manager', () => {
    const testManager = new Company({
      id: '234',
      name: 'Company Name',
      taxContact: new Individual({
        name: 'FirstName LastName',
        taxIdType: 'SSN',
        taxId: '123123123',
        phone: '111 111 1111',
        address: testLocalAddress,
      }),
    });

    const expectedContact = {
      first_name: 'FirstName',
      last_name: 'LastName',
      contact_type: 'tax',
      ssn: '123123123',
      addresses: [testLegalIncAddress],
      phones: [
        {
          number: '111 111 1111',
          phone_type: 'tax',
        },
      ],
    };
    expect(getTaxContactFromProfileOrManager(testManager)).to.deepEqual(
      expectedContact,
    );
  });

  it('builds a tax contact from an individual organizer profile', () => {
    const organizerProfile = new Profile({
      firstName: 'FirstName',
      lastName: 'LastName',
      phone: '111 111 1111',
      taxDetails: {
        taxIdentification: {
          type: 'ssn',
          value: '123123123',
        },
      },
      address: testLocalAddress,
    });

    const expectedContact = {
      first_name: 'FirstName',
      last_name: 'LastName',
      contact_type: 'tax',
      ssn: '123123123',
      addresses: [testLegalIncAddress],
      phones: [
        {
          number: '111 111 1111',
          phone_type: 'tax',
        },
      ],
    };
    expect(getTaxContactFromProfileOrManager(organizerProfile)).to.deepEqual(
      expectedContact,
    );
  });

  it('builds a tax contact from an entity organizer profile', () => {
    const organizerProfile = new Profile({
      profileType: 'ORGANIZER',
      stateOfFormation: 'Nevada',
      countryOfFormation: 'United States of America',
      name: 'Test Entity',
      firstName: 'FirstName',
      lastName: 'LastName',
      address: testLocalAddress,
      phone: '(555) 555 5555',
      taxDetails: {
        registrationType: 'ENTITY',
        taxIdentification: {
          type: 'ssn',
          value: '111-111-1111',
        },
      },
    });

    const expectedContact = {
      first_name: 'FirstName',
      last_name: 'LastName',
      contact_type: 'tax',
      ssn: '111-111-1111',
      addresses: [testLegalIncAddress],
      phones: [
        {
          number: '(555) 555 5555',
          phone_type: 'tax',
        },
      ],
    };
    expect(getTaxContactFromProfileOrManager(organizerProfile)).to.deepEqual(
      expectedContact,
    );
  });
  it('builds a tax contact from an entity manager company', () => {
    const organizerProfile = new Company({
      name: 'Test, LLC',
      type: 'MANAGER',
      email: 'test@test.com',
      address: testLocalAddress,
      taxContact: new Individual({
        taxIdType: 'SSN',
        taxId: '111-11-1111',
        name: 'Test Person',
        address: testLocalAddress,
        phone: '(555) 555 5555',
      }),
      arbitrationCity: 'Encinitas',
      arbitrationState: 'California',
      phone: '(222) 222 2222',
    });

    const expectedContact = {
      first_name: 'Test',
      last_name: 'Person',
      contact_type: 'tax',
      ssn: '111-11-1111',
      addresses: [testLegalIncAddress],
      phones: [
        {
          number: '(555) 555 5555',
          phone_type: 'tax',
        },
      ],
    };
    expect(getTaxContactFromProfileOrManager(organizerProfile)).to.deepEqual(
      expectedContact,
    );
  });
  it('throws an error if ssn not present', () => {
    const organizerProfile = new Company({
      name: 'Test, LLC',
      type: 'MANAGER',
      email: 'test@test.com',
      address: testLocalAddress,
      taxContact: new Individual({
        taxIdType: 'ITIN',
        taxId: '111-11-1111',
        name: 'Test Person',
        address: testLocalAddress,
        phone: '(555) 555 5555',
      }),
      arbitrationCity: 'Encinitas',
      arbitrationState: 'California',
      phone: '(222) 222 2222',
    });

    expect(() =>
      getTaxContactFromProfileOrManager(organizerProfile),
    ).to.throw();
  });
  it('throws an error if address not present', () => {
    const organizerProfile = new Company({
      name: 'Test, LLC',
      type: 'MANAGER',
      email: 'test@test.com',
      taxContact: new Individual({
        taxIdType: 'SSN',
        taxId: '111-11-1111',
        name: 'Test Person',
        phone: '(555) 555 5555',
      }),
      arbitrationCity: 'Encinitas',
      arbitrationState: 'California',
      phone: '(222) 222 2222',
    });

    expect(() => {
      getTaxContactFromProfileOrManager(organizerProfile);
    }).to.throw();
  });
});
