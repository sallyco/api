import {ClosesService, DocType, MinIOService} from '../../../../services';
import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {createStubInstance, sinon} from '@loopback/testlab/dist/sinon';
import {Context} from '@loopback/core';
import {
  TestCloseRepository,
  TestCompanyRepository,
  TestDealRepository,
  TestEntityRepository,
  TestFileRepository,
  TestProfileRepository,
  TestSubscriptionRepository,
  TestTenantRepository,
} from '../../../helpers/RepositoryHelper';
import {TestEntityData} from '../../../helpers/entitiesHelper';
import {TestDealData} from '../../../helpers/dealsHelper';
import {
  TestIndividualOrganizerProfileData,
  TestLLCOrganizerProfileData,
} from '../../../helpers/profilesHelper';
import moment from 'moment';
import {
  TestManagerCompanyData,
  TestMasterCompanyData,
} from '../../../helpers/companiesHelper';
import {File, Subscription} from '../../../../models';

let service: ClosesService;
const minioService: StubbedInstanceWithSinonAccessor<MinIOService> =
  createStubInstance(MinIOService);
const context = new Context();

beforeEach(async () => {
  service = new ClosesService(
    TestCloseRepository,
    TestEntityRepository,
    TestTenantRepository,
    TestCompanyRepository,
    TestSubscriptionRepository,
    TestFileRepository,
    TestDealRepository,
    TestProfileRepository,
    minioService,
    context,
  );
});

describe('Closes Tests', () => {
  describe('Tenant Organizer / Fund Manager Tests', () => {
    it('Should detect a GBT Master correctly', async () => {
      const gbtEntity = await TestEntityRepository.create({
        name: 'Series 001, a series of Glassboard Master II, LLC',
        entityType: 'LIMITED_LIABILITY_COMPANY',
        minInvestmentAmount: 10000,
      });
      const otherMasterEntity = await TestEntityRepository.create({
        name: 'Series 001, a series of Other Cool Company, LLC',
        entityType: 'LIMITED_LIABILITY_COMPANY',
        minInvestmentAmount: 10000,
      });
      await expect(service.isGBTMaster(gbtEntity)).to.be.fulfilledWith(true);
      await expect(service.isGBTMaster(otherMasterEntity)).to.be.fulfilledWith(
        false,
      );
    });
    it('Should get multi step close signing setting correctly', async () => {
      let tenantMultiStep = await TestTenantRepository.create({
        id: 'gbt',
        name: 'Glassboard Test',
        url: 'https://glassboardtech.com',
        settings: {
          useMultiStepSigningForCloses: true,
        },
      });
      await expect(
        service.isMultiStepSigningTenant(tenantMultiStep),
      ).to.be.fulfilledWith(true);
      tenantMultiStep.settings = {
        ...tenantMultiStep.settings,
        useMultiStepSigningForCloses: false,
      };
      tenantMultiStep = await TestTenantRepository.save(tenantMultiStep);
      await expect(
        service.isMultiStepSigningTenant(tenantMultiStep),
      ).to.be.fulfilledWith(false);
    });
  });

  describe('Document Resolver Tests', () => {
    it('Should resolve valid document names to DocTypes', async () => {
      await expect(
        service.resolveFileNameToDocType('Test - Operating Agreement.pdf'),
      ).to.be.fulfilledWith(DocType.OperatingAgreement);
      await expect(
        service.resolveFileNameToDocType('Test - Subscription Agreement.pdf'),
      ).to.be.fulfilledWith(DocType.SubscriptionAgreement);
      await expect(
        service.resolveFileNameToDocType(
          'Test - Private Placement Memorandum.pdf',
        ),
      ).to.be.fulfilledWith(DocType.PrivatePlacementMemorandum);
      await expect(
        service.resolveFileNameToDocType('Test - W9.pdf'),
      ).to.be.fulfilledWith(DocType.W9);
      await expect(
        service.resolveFileNameToDocType('Test - W-9.pdf'),
      ).to.be.fulfilledWith(DocType.W9);
      await expect(
        service.resolveFileNameToDocType('Test - W8BEN.pdf'),
      ).to.be.fulfilledWith(DocType.W8BEN);
      await expect(
        service.resolveFileNameToDocType('Test - W8-BEN.pdf'),
      ).to.be.fulfilledWith(DocType.W8BEN);
      await expect(
        service.resolveFileNameToDocType('Weird File Name.pdf'),
      ).to.be.fulfilledWith(undefined);
    });

    it('Should build an array of files from File Ids', async () => {
      const stubDocFunction = sinon.stub(service, 'getDocumentEntry');
      stubDocFunction.returns(
        new Promise(resolve => {
          resolve({
            documentType: DocType.Unknown,
            file: new File(),
            data: new Buffer(''),
          });
        }),
      );
      service.getDocumentEntry = stubDocFunction;
      const files = await service.getSubscriptionDocuments(
        new Subscription({
          id: 'Test',
          files: ['test'],
        }),
      );
      expect(files.length).to.be.above(0);
    });

    it('Should throw an error if subscription is missing all files', async () => {
      const stubDocFunction = sinon.stub(service, 'getDocumentEntry');
      stubDocFunction.returns(
        new Promise(resolve => {
          resolve({
            documentType: DocType.Unknown,
            file: new File(),
            data: new Buffer(''),
          });
        }),
      );
      service.getDocumentEntry = stubDocFunction;
      const files_function = service.getSubscriptionDocuments(
        new Subscription({
          id: 'Test',
        }),
      );
      await expect(files_function).be.rejectedWith(
        'Subscription Test is missing Documents',
      );
    });

    it('Should skip a file if unable to get it', async () => {
      const stubDocFunction = sinon.stub(service, 'getDocumentEntry');
      stubDocFunction.returns(
        new Promise((resolve, reject) => {
          reject('Unable to download');
        }),
      );
      service.getDocumentEntry = stubDocFunction;
      const files = await service.getSubscriptionDocuments(
        new Subscription({
          id: 'Test',
          files: ['test'],
        }),
      );
      expect(files.length).to.be.equal(0);
    });
  });

  describe('Manager Information Resolution Tests', () => {
    it('Should resolve a GBT Master deal information for Individual Organizer', async () => {
      const gbtProfile = await TestProfileRepository.create(
        TestIndividualOrganizerProfileData,
      );
      const gbtDeal = await TestDealRepository.create({
        ...TestDealData,
        profileId: gbtProfile.id,
      });
      const gbtEntity = await TestEntityRepository.create({
        ...TestEntityData,
        dealId: gbtDeal.id,
      });
      const dateClosed = new Date();
      const gbtClose = await TestCloseRepository.create({
        dateClosed: dateClosed,
        entityId: gbtEntity.id,
      });
      await expect(
        service.resolveFillableData(
          gbtClose,
          '2/22/22',
          'testOrganizerSignature',
        ),
      ).to.be.fulfilledWith({
        organizerName: {
          type: 'text',
          value: `Individual Organizer`,
        },
        organizerSignaturePrint: {
          type: 'text',
          value: `Individual Organizer`,
        },
        organizerSignatureTitle: {
          type: 'text',
          value: 'Organizer',
        },
        organizerInfoBlock: {
          type: 'text',
          value: `a Utah Individual`,
        },
        organizerSignature: {
          type: 'signature',
          value: 'testOrganizerSignature',
        },
        managerName: {
          type: 'text',
          value: 'Glassboard Management II, LLC',
        },
        managerSignaturePrint: {
          type: 'text',
          value: 'Glassboard Management II, LLC',
        },
        managerSignatureTitle: {
          type: 'text',
          value: 'Manager',
        },
        managerInfoBlock: {
          type: 'text',
          value: 'a Utah Limited Liability Company',
        },
        managerSignature: {
          type: 'signature',
          value: undefined,
        },
        fundManagerName: {
          type: 'text',
          value: 'Jeremy Neilson',
        },
        fundManagerSignaturePrint: {
          type: 'text',
          value: 'Jeremy Neilson',
        },
        fundManagerSignatureTitle: {
          type: 'text',
          value: 'Manager',
        },
        fundManagerInfoBlock: {
          type: 'text',
        },
        fundManagerSignature: {
          type: 'signature',
          value: undefined,
        },
        date: {
          type: 'text',
          value: moment(dateClosed).format('MM/DD/YY'),
        },
        agreementTitle: {
          type: 'text',
          value: 'LIMITED LIABILITY COMPANY OPERATING AGREEMENT',
        },
        entityLegalName: {
          type: 'text',
          value: 'Series 001, a series of Glassboard Master II LLC',
          textAlignment: 'center',
        },
        fundEntityInfoBlock: {
          type: 'text',
          value: 'a Delaware Limited Liability Company',
        },
      });
    });

    it('Should resolve a GBT Master deal information for Entity Organizer', async () => {
      const gbtProfile = await TestProfileRepository.create(
        TestLLCOrganizerProfileData,
      );
      const gbtDeal = await TestDealRepository.create({
        ...TestDealData,
        profileId: gbtProfile.id,
      });
      const gbtEntity = await TestEntityRepository.create({
        ...TestEntityData,
        dealId: gbtDeal.id,
      });
      const dateClosed = new Date();
      const gbtClose = await TestCloseRepository.create({
        dateClosed: dateClosed,
        entityId: gbtEntity.id,
      });
      await expect(
        service.resolveFillableData(
          gbtClose,
          '2/2/22',
          'testOrganizerSignature',
          'testFundManagerSignature',
        ),
      ).to.be.fulfilledWith({
        organizerName: {
          type: 'text',
          value: `Test Entity, LLC`,
        },
        organizerSignaturePrint: {
          type: 'text',
          value: `Organizer Organizer`,
        },
        organizerSignatureTitle: {
          type: 'text',
          value: 'Organizer',
        },
        organizerInfoBlock: {
          type: 'text',
          value: `a Utah Entity`,
        },
        organizerSignature: {
          type: 'signature',
          value: 'testOrganizerSignature',
        },
        managerName: {
          type: 'text',
          value: 'Glassboard Management II, LLC',
        },
        managerSignaturePrint: {
          type: 'text',
          value: 'Glassboard Management II, LLC',
        },
        managerSignatureTitle: {
          type: 'text',
          value: 'Manager',
        },
        managerInfoBlock: {
          type: 'text',
          value: 'a Utah Limited Liability Company',
        },
        managerSignature: {
          type: 'signature',
          value: 'testFundManagerSignature',
        },
        fundManagerName: {
          type: 'text',
          value: 'Jeremy Neilson',
        },
        fundManagerSignaturePrint: {
          type: 'text',
          value: 'Jeremy Neilson',
        },
        fundManagerSignatureTitle: {
          type: 'text',
          value: 'Manager',
        },
        fundManagerInfoBlock: {
          type: 'text',
        },
        fundManagerSignature: {
          type: 'signature',
          value: 'testFundManagerSignature',
        },
        date: {
          type: 'text',
          value: moment(dateClosed).format('MM/DD/YY'),
        },
        agreementTitle: {
          type: 'text',
          value: 'LIMITED LIABILITY COMPANY OPERATING AGREEMENT',
        },
        entityLegalName: {
          type: 'text',
          value: 'Series 001, a series of Glassboard Master II LLC',
          textAlignment: 'center',
        },
        fundEntityInfoBlock: {
          type: 'text',
          value: 'a Delaware Limited Liability Company',
        },
      });
    });

    it('Should resolve a non-GBT Master deal information', async () => {
      /* eslint-disable  @typescript-eslint/no-unused-vars */
      const tenant = await TestTenantRepository.findById('gbt');
      /* eslint-enable  @typescript-eslint/no-unused-vars */
      const masterCompany = await TestCompanyRepository.create(
        TestMasterCompanyData,
      );
      const managerCompany = await TestCompanyRepository.create(
        TestManagerCompanyData,
      );
      const gbtProfile = await TestProfileRepository.create(
        TestLLCOrganizerProfileData,
      );
      const gbtDeal = await TestDealRepository.create({
        ...TestDealData,
        profileId: gbtProfile.id,
      });
      const gbtEntity = await TestEntityRepository.create({
        ...TestEntityData,
        name: 'Series 002, a Series of Test Company LLC',
        managerId: managerCompany.id,
        masterEntityId: masterCompany.id,
        dealId: gbtDeal.id,
      });
      const dateClosed = new Date();
      const gbtClose = await TestCloseRepository.create({
        dateClosed: dateClosed,
        entityId: gbtEntity.id,
      });
      await expect(
        service.resolveFillableData(
          gbtClose,
          '2/2/22',
          undefined,
          'testFundManagerSignature',
        ),
      ).to.be.fulfilledWith({
        organizerName: {
          type: 'text',
          value: `Test Entity, LLC`,
        },
        organizerSignaturePrint: {
          type: 'text',
          value: `Organizer Organizer`,
        },
        organizerSignatureTitle: {
          type: 'text',
          value: 'Organizer',
        },
        organizerInfoBlock: {
          type: 'text',
          value: `a Utah Entity`,
        },
        organizerSignature: {
          type: 'signature',
          value: undefined,
        },
        managerName: {
          type: 'text',
          value: 'Elizabeth Wallace',
        },
        managerSignaturePrint: {
          type: 'text',
          value: 'Elizabeth Wallace',
        },
        managerSignatureTitle: {
          type: 'text',
          value: 'Manager',
        },
        managerInfoBlock: {
          type: 'text',
          value: 'a Delaware LLC',
        },
        managerSignature: {
          type: 'signature',
          value: 'testFundManagerSignature',
        },
        fundManagerName: {
          type: 'text',
          value: 'Elizabeth Wallace',
        },
        fundManagerSignaturePrint: {
          type: 'text',
          value: 'Elizabeth Wallace',
        },
        fundManagerSignatureTitle: {
          type: 'text',
          value: 'Fund Manager',
        },
        fundManagerInfoBlock: {
          type: 'text',
        },
        fundManagerSignature: {
          type: 'signature',
          value: 'testFundManagerSignature',
        },
        date: {
          type: 'text',
          value: moment(dateClosed).format('MM/DD/YY'),
        },
        agreementTitle: {
          type: 'text',
          value: 'LIMITED LIABILITY COMPANY OPERATING AGREEMENT',
        },
        entityLegalName: {
          type: 'text',
          value: 'Series 002, a Series of Test Company LLC',
          textAlignment: 'center',
        },
        fundEntityInfoBlock: {
          type: 'text',
          value: 'a Delaware Limited Liability Company',
        },
      });
    });
  });
});
