import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {Deal} from 'models';
import {DealRepository} from 'repositories';
import {TestRepository} from '__tests__/helpers/globalHelpers';
import {
  CloseService,
  DealService,
  SubscriptionService,
} from '../../../services';

describe('DealService (unit)', () => {
  const dealRepository = TestRepository(DealRepository);
  let subscriptionService: StubbedInstanceWithSinonAccessor<SubscriptionService>;
  let dealService: DealService;
  let closeService: StubbedInstanceWithSinonAccessor<CloseService>;

  beforeEach(async () => {
    subscriptionService = createStubInstance(SubscriptionService);
    closeService = createStubInstance(CloseService);
    dealService = new DealService(
      dealRepository,
      subscriptionService,
      closeService,
    );
    await dealRepository.deleteAll();
  });

  it('deals service applied deal amounts to performance summary', async () => {
    // Set up mock data for the call
    const deal = new Deal({
      tenantId: 'gbt',
      name: 'Test Deal',
      description: 'desc',
      portfolioCompanyName: 'compname',
      portfolioCompanyState: 'Utah',
      portfolioCompanyEntity: 'LIMITED_LIABILITY_COMPANY',
      entityId: 'entityId',
      targetRaiseAmount: 1000,
      minInvestmentAmount: 10,
      previouslyRaisedAmount: 100,
      isPublic: false,
      status: 'OPEN',
      requireQualifiedPurchaser: false,
      deletedAt: undefined,
      isDeleted: false,
      profileId: 'profid',
    });
    const testDeal = await dealRepository.save(deal);

    // Assume one closed subscription for $10 exists
    subscriptionService.stubs.getSubscriptionPerformanceSummary.resolves({
      targetAmount: 0,
      committedAmount: 10,
      raisedAmount: 10,
    });

    // Expectations
    // $100 (previouslyRaisedAmount) should be added to committed and raised
    const summary = await dealService.getDealPerformanceSummary(
      testDeal.id,
      {},
    );
    expect(summary.targetAmount).to.equal(testDeal.targetRaiseAmount);
    expect(summary.committedAmount).to.equal(
      10 + testDeal.previouslyRaisedAmount,
    );
    expect(summary.raisedAmount).to.equal(10 + testDeal.previouslyRaisedAmount);
  });
});
