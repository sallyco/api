import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {Entities, Subscription, Tenant} from 'models';
import {
  BlueSkyFeeRepository,
  DealRepository,
  EntitiesRepository,
  ProductRepository,
  SubscriptionRepository,
  TenantRepository,
} from 'repositories';
import {
  BankingTreasuryPrimeService,
  BlueSkyFeeService,
  FilingsService,
} from '../../../services';

import {FeesService} from '../../../services';

describe('FeesService (unit)', () => {
  let feesService: FeesService;
  let subscriptionRepository: StubbedInstanceWithSinonAccessor<SubscriptionRepository>;
  let dealRepository: StubbedInstanceWithSinonAccessor<DealRepository>;
  let entityRepository: StubbedInstanceWithSinonAccessor<EntitiesRepository>;
  let tenantRepository: StubbedInstanceWithSinonAccessor<TenantRepository>;
  let productRepository: StubbedInstanceWithSinonAccessor<ProductRepository>;
  let blueSkyRepository: StubbedInstanceWithSinonAccessor<BlueSkyFeeRepository>;
  let tpBanking: StubbedInstanceWithSinonAccessor<BankingTreasuryPrimeService>;
  let filings: StubbedInstanceWithSinonAccessor<FilingsService>;
  let blueSkyFeeSerice: StubbedInstanceWithSinonAccessor<BlueSkyFeeService>;
  const feesTenant = new Tenant({
    saasPackageType: {
      saasPackageType: 'Tiered',
      tieredPricing: [
        {
          breakpoint: 49900,
          price: 4000,
        },
        {
          breakpoint: 50000,
          price: 4500,
        },
        {
          breakpoint: 100000,
          price: 5500,
        },
        {
          breakpoint: 175000,
          price: 6500,
        },
        {
          breakpoint: 250000,
          price: 7750,
        },
        {
          breakpoint: 350000,
          price: 10000,
        },
        {
          breakpoint: 500000,
          price: 12500,
        },
      ],
    },
  });
  const spvCostTenant = new Tenant({
    saasPackageType: {
      saasPackageType: 'Gold',
    },
    dealLimits: {
      costPerSPV: 1000,
    },
  });

  beforeEach(async () => {
    feesService = new FeesService(
      subscriptionRepository,
      dealRepository,
      entityRepository,
      tenantRepository,
      blueSkyRepository,
      tpBanking,
      filings,
      productRepository,
      blueSkyFeeSerice,
    );
  });

  it('Fees calculate percentage correctly, recurring and nonrecuring', () => {
    const proceedsAmount = 100;
    const managementConfig: Entities['managementFee'] = {
      amount: 10,
      type: 'percent',
      isRecurring: false,
    };
    const percentNonRecurring = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
    );
    expect(percentNonRecurring).to.equal(10);

    managementConfig.isRecurring = true;
    const percentRecurring = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
    );
    expect(percentRecurring).to.equal(70);
  });

  /**
   * ASSUMES KNOWLEDGE OF 7 YEARS
   * FLAKY, make configurable???
   */
  it('Fees calculate flat fee correctly, recurring and nonrecuring', () => {
    const proceedsAmount = 100;
    const managementConfig: Entities['managementFee'] = {
      amount: 1000,
      type: 'flat',
      isRecurring: false,
    };
    const flatNonRecurring = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
    );
    expect(flatNonRecurring).to.equal(1000);

    managementConfig.isRecurring = true;
    const flatRecurring = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
    );
    expect(flatRecurring).to.equal(7000);
  });

  it('Calculates Reserve Fees correctly', () => {
    const proceeds = 100;
    const expenseReserve: Entities['expenseReserve'] = {
      amount: 10,
      type: 'Flat Fee',
    };
    const flatFee = feesService.calculateReserveFeeAmount(
      proceeds,
      expenseReserve,
    );
    expect(flatFee).to.equal(10);

    expenseReserve.type = 'Percent';
    const percentFee = feesService.calculateReserveFeeAmount(
      proceeds,
      expenseReserve,
    );
    expect(percentFee).to.equal(10);
  });

  it('Should select the correct tier pricing', async () => {
    await expect(
      feesService.getCostPerSpv(feesTenant, 49900.0),
    ).to.be.fulfilledWith(4000);
    await expect(
      feesService.getCostPerSpv(feesTenant, 49900.01),
    ).to.be.fulfilledWith(4000);
    await expect(
      feesService.getCostPerSpv(feesTenant, 50000.0),
    ).to.be.fulfilledWith(4500);
    await expect(
      feesService.getCostPerSpv(feesTenant, 499999.99),
    ).to.be.fulfilledWith(10000);
    await expect(
      feesService.getCostPerSpv(feesTenant, 500000.0),
    ).to.be.fulfilledWith(12500);
  });

  it('Should fallback to Per SPV Field', async () => {
    await expect(
      feesService.getCostPerSpv(spvCostTenant, 49900.0),
    ).to.be.fulfilledWith(1000);
  });

  it('Fees calculate using side letter correctly', () => {
    const subscriptions = [
      new Subscription({
        tenantId: 'test-tenent-id',
        name: 'Test User',
        email: 'test@test.com',
        status: 'COMPLETED',
        amount: 5000,
        dealId: 'test-deal-id',
        profileId: 'test-profile-id',
        ownerId: 'test-owner-id',
        sideLetter: {
          managementFee: {
            amount: 5,
            type: 'percent',
            isRecurring: false,
          },
        },
      }),
      new Subscription({
        tenantId: 'test-tenent-id',
        name: 'Test User',
        email: 'test@test.com',
        status: 'COMPLETED',
        amount: 5000,
        dealId: 'test-deal-id',
        profileId: 'test-profile-id',
        ownerId: 'test-owner-id',
        sideLetter: {
          managementFee: {
            amount: 100,
            type: 'flat',
            isRecurring: false,
          },
        },
      }),
    ];

    const proceedsAmount = 100;
    const managementConfig: Entities['managementFee'] = {
      amount: 10,
      type: 'percent',
      isRecurring: false,
    };
    let calculatedFee = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
      [subscriptions[0]],
    );
    expect(calculatedFee).to.equal(250);

    calculatedFee = feesService.calculateManagementFeeAmount(
      managementConfig,
      proceedsAmount,
      [subscriptions[1]],
    );
    expect(calculatedFee).to.equal(100);
  });
});
