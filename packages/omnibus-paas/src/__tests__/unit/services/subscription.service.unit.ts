import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {Close, Subscription} from 'models';
import {ClosesList} from 'models/closes-list.model';
import {SubscriptionRepository} from 'repositories';
import {TestRepository} from '__tests__/helpers/globalHelpers';
import {CloseService, SubscriptionService} from '../../../services';

describe('Subscription Service (unit)', () => {
  const subscriptionRepository = TestRepository(SubscriptionRepository);
  let subscriptionService: SubscriptionService;
  let closeService: StubbedInstanceWithSinonAccessor<CloseService>;

  beforeEach(async () => {
    closeService = createStubInstance(CloseService);
    subscriptionService = new SubscriptionService(
      subscriptionRepository,
      closeService,
    );
    await subscriptionRepository.deleteAll();
  });

  it('Calculates subscription performance summary', async () => {
    const DEALID = '123';
    // Included, docs are signed, amount is positive
    const testSub1 = new Subscription({
      ownerId: 'owner',
      name: 'subname',
      email: 'email',
      type: 'INDIVIDUAL',
      status: 'COMPLETED',
      transactionId: 'fdsa',
      dealId: DEALID,
      isDocsSigned: true,
      isKycAmlPassed: true,
      amount: 10,
      isDeleted: false,
      deletedAt: undefined,
    });
    const savedSub1 = await subscriptionRepository.save(testSub1);

    // Included, docs are signed, amount is positive
    await subscriptionRepository.save(
      new Subscription({
        ownerId: 'owner',
        name: 'subname',
        email: 'email',
        type: 'INDIVIDUAL',
        status: 'COMPLETED',
        transactionId: 'fdsa',
        dealId: DEALID,
        isDocsSigned: true,
        isKycAmlPassed: true,
        amount: 20,
        isDeleted: false,
        deletedAt: undefined,
      }),
    );

    // Docs not signed, not committed or raised
    await subscriptionRepository.save(
      new Subscription({
        ownerId: 'owner',
        name: 'subname',
        email: 'email',
        type: 'INDIVIDUAL',
        status: 'COMPLETED',
        transactionId: 'fdsa',
        dealId: DEALID,
        isDocsSigned: false,
        isKycAmlPassed: true,
        amount: 30,
        isDeleted: false,
        deletedAt: undefined,
      }),
    );

    const excludeStatuses: Subscription['status'][] = [
      'CANCELLED',
      'TRANSFERRED',
      'REFUNDED',
    ];
    for (let i = 0; i < excludeStatuses.length; i++) {
      await subscriptionRepository.save(
        new Subscription({
          ownerId: 'owner',
          name: 'subname',
          email: 'email',
          type: 'INDIVIDUAL',
          status: excludeStatuses[i], // EXCLUDED because of status
          transactionId: 'fdsa',
          dealId: DEALID,
          isDocsSigned: true, // Should be included
          isKycAmlPassed: true,
          amount: 30, // Amount is positive, should be included
          isDeleted: false,
          deletedAt: undefined,
        }),
      );
    }

    // Include only sub number 1 in the close
    closeService.stubs.getClosesByDeal.resolves(
      new ClosesList({
        data: [new Close({subscriptions: [savedSub1.id]})],
      }),
    );

    const result = await subscriptionService.getSubscriptionPerformanceSummary(
      DEALID,
      {
        closePageNumber: 0,
        closePerPage: 100,
      },
    );

    // Saved subs that are a positive amount and signed
    expect(result.committedAmount).to.equal(30);

    // Saved subs that are included in a close
    expect(result.raisedAmount).to.equal(10);

    // Target is just part of the interface
    expect(result.targetAmount).to.equal(0);
  });
});
