import {expect} from '@loopback/testlab';
import {GbtLegacyDataImporterService} from '../../../services';
import {
  AssetRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  ProfileRepository,
  SubscriptionRepository,
} from '../../../repositories';
import {TestRepository} from '../../helpers/globalHelpers';
import {CompanyRepository} from '../../../repositories/company.repository';
import {
  Entity,
  model,
  property,
  PropertyDefinition,
} from '@loopback/repository';
import {
  importable,
  ImportableMetadata,
} from '../../../decorators/importable-model-decorator';

@model()
class TestModel extends Entity {
  @property({
    type: 'string',
  })
  ignoredProperty: string;

  @property({
    type: 'string',
  })
  @importable({
    displayName: 'Test Property',
    description: 'Test Property Description',
  })
  testProperty: string;

  @property({
    type: 'object',
    jsonSchema: {
      patternProperties: {
        nestedProperty1: {type: 'string'},
        nestedProperty2: {type: 'string'},
      },
    },
  })
  @importable({
    displayName: 'Nested Property',
    description: 'Nested Property Description',
  })
  testNestedProperty?: {
    nestedProperty1?: string;
    nestedProperty2?: string;
  };

  @property({
    type: 'object',
    jsonSchema: {
      properties: {
        nested: {
          type: 'object',
          properties: {
            nested: {
              type: 'object',
              properties: {
                nested: {
                  type: 'object',
                  properties: {
                    nested: {
                      type: 'object',
                      properties: {
                        nested: {
                          type: 'object',
                          properties: {
                            nested: {
                              type: 'object',
                              properties: {
                                nested: {
                                  type: 'string',
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  })
  @importable({
    displayName: 'Deep Nested Property',
    description: 'Deep Nested Property Description',
  })
  testDeepNestedProperty?: {
    nested?: {
      nested?: {
        nested?: {
          nested?: {
            nested?: {
              nested?: {
                nested?: string;
              };
            };
          };
        };
      };
    };
  };
}

const flattenedSchema: Partial<ImportableMetadata> &
  Partial<PropertyDefinition> = [
  {
    type: 'string',
    name: 'testProperty',
    modelName: 'TestModel',
    displayName: 'Test Property',
    description: 'Test Property Description',
  },
  {
    type: 'object',
    jsonSchema: {
      patternProperties: {
        nestedProperty1: {
          type: 'string',
        },
        nestedProperty2: {
          type: 'string',
        },
      },
    },
    name: 'testNestedProperty',
    modelName: 'TestModel',
    displayName: 'Nested Property',
    description: 'Nested Property Description',
  },
  {
    name: 'testDeepNestedProperty',
    modelName: 'TestModel',
    displayName: 'Deep Nested Property',
    description: 'Deep Nested Property Description',
    type: 'object',
    jsonSchema: {
      properties: {
        nested: {
          type: 'object',
          properties: {
            nested: {
              type: 'object',
              properties: {
                nested: {
                  type: 'object',
                  properties: {
                    nested: {
                      type: 'object',
                      properties: {
                        nested: {
                          type: 'object',
                          properties: {
                            nested: {
                              type: 'object',
                              properties: {
                                nested: {
                                  type: 'string',
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
];

const importerService = new GbtLegacyDataImporterService(
  TestRepository(DealRepository),
  TestRepository(EntitiesRepository),
  TestRepository(AssetRepository),
  TestRepository(CompanyRepository),
  TestRepository(ProfileRepository),
  TestRepository(SubscriptionRepository),
  TestRepository(CloseRepository),
  TestRepository(FileRepository),
);

describe('GBT Legacy Data Importer Service Unit Tests', () => {
  it('Should create a flat array from a Model schema', async () => {
    const flatSchema = importerService.getFlatSchema(TestModel);
    expect(flatSchema.length).to.be.equal(
      3,
      'Schema was converted to array with only @importable properties',
    );

    expect(flatSchema[0].type).to.be.equal('string');
    expect(flatSchema[0].name).to.be.equal('testProperty');
    expect(flatSchema[0].displayName).to.be.equal('Test Property');
    expect(flatSchema[0].description).to.be.equal('Test Property Description');

    expect(flatSchema[1].type).to.be.equal('object');
    expect(flatSchema[1].name).to.be.equal('testNestedProperty');
    expect(flatSchema[1].displayName).to.be.equal('Nested Property');

    expect(flatSchema[0]).to.deepEqual(flattenedSchema[0]);
    expect(flatSchema[1]).to.deepEqual(flattenedSchema[1]);
  });

  it('Should flatten a single string property to a single header', async () => {
    const flattenedProperty = importerService.flattenProperty(
      flattenedSchema[0],
      TestModel.modelName,
    );
    expect(flattenedProperty.length).to.equal(
      1,
      'Single string property resolves to single flattened item',
    );
    expect(flattenedProperty[0]).to.equal(
      'TestModel.testProperty',
      'Single string property resolves to [ModelName].[PropertyName]',
    );
  });

  it('Should flatten an object property to multiple items', async () => {
    const flattenedProperty = importerService.flattenProperty(
      flattenedSchema[1],
      TestModel.modelName,
    );
    expect(flattenedProperty.length).to.equal(
      2,
      'Object property resolves to multiple flattened items',
    );
    expect(flattenedProperty[0]).to.equal(
      'TestModel.testNestedProperty.nestedProperty1',
      'Object nested string property resolves to [ModelName].[TopLevelPropertyName].[SubPropertyName]',
    );
    expect(flattenedProperty[1]).to.equal(
      'TestModel.testNestedProperty.nestedProperty2',
      'Object nested string property resolves to [ModelName].[TopLevelPropertyName].[SubPropertyName]',
    );
  });

  it('Should flatten a deep nested object property with a single property to one item', async () => {
    const flattenedProperty = importerService.flattenProperty(
      flattenedSchema[2],
      TestModel.modelName,
    );
    expect(flattenedProperty.length).to.equal(
      1,
      'Deep nested Object property with a single property resolves to a single flattened item',
    );
    expect(flattenedProperty[0]).to.equal(
      'TestModel.testDeepNestedProperty.nested.nested.nested.nested.nested.nested.nested',
      'Object nested string property resolves to [ModelName].[TopLevelPropertyName].[SubPropertyName]...[SubPropertyName]',
    );
  });
});
