import {
  AccountTransaction,
  BankingTreasuryPrimeService,
  EmailsService,
  SystemNotificationsService,
  TenantService,
  TransactionACHDetail,
  TransactionService,
  TransactionServiceErrors,
  TransactionWireDetail,
} from '../../../services';
import {givenStubBankingService} from '../../helpers/bankingHelper';
import {
  BankAccountRepository,
  BankingBeneficiaryRepository,
} from '../../../repositories';
import {TestRepository} from '../../helpers/globalHelpers';
import {
  ExternalBankAccount,
  BankingBeneficiary,
  Deal,
  Entities as Entity,
  Profile,
  Subscription,
  Transaction,
  TransactionStatus,
  BankAccount,
} from '../../../models';
import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import moment from 'moment';
import {AccountTransactionResponse} from '../../../interfaces/services/treasury-prime.interface';
import {
  TestDealRepository,
  TestEntityRepository,
  TestProfileRepository,
  TestSubscriptionRepository,
  TestTransactionRepository,
} from '../../helpers/RepositoryHelper';
// import {sinon} from '@loopback/testlab/dist/sinon';

describe('Transaction Service Unit Tests', () => {
  let transactionService: TransactionService;
  const transactionRepository = TestTransactionRepository;
  const subscriptionRepository = TestSubscriptionRepository;
  const entityRepository = TestEntityRepository;
  const dealRepository = TestDealRepository;
  const profileRepository = TestProfileRepository;
  const bankingBeneficiaryRepository = TestRepository(
    BankingBeneficiaryRepository,
  );
  const bankAccountRepository = TestRepository(BankAccountRepository);
  let bankingService: StubbedInstanceWithSinonAccessor<BankingTreasuryPrimeService>;
  let emailService: StubbedInstanceWithSinonAccessor<EmailsService>;
  let tenantService: StubbedInstanceWithSinonAccessor<TenantService>;
  let systemNotificationService: StubbedInstanceWithSinonAccessor<SystemNotificationsService>;
  let entity: Entity;
  let deal: Deal;
  let subscription: Subscription;
  let profile: Profile;
  let bankingBeneficiary: BankingBeneficiary;
  let bankAccount: BankAccount;

  const saveAllModels = async () => {
    entity = await entityRepository.save(entity);
    subscription = await subscriptionRepository.save(subscription);
    // Removing to prevent navigational issues with Profile model
    delete profile.kycAml;
    profile = await profileRepository.save(profile);
    deal = await dealRepository.save(deal);
    bankingBeneficiary = await bankingBeneficiaryRepository.save(
      bankingBeneficiary,
    );
    bankAccount = await bankAccountRepository.save(bankAccount);
  };

  beforeEach(async () => {
    const {bankingTreasuryPrimeService} = givenStubBankingService();
    bankingService = bankingTreasuryPrimeService;
    // sinon.restore();
    emailService = createStubInstance(EmailsService);
    tenantService = createStubInstance(TenantService);
    systemNotificationService = createStubInstance(SystemNotificationsService);
    transactionService = new TransactionService(
      bankingService,
      transactionRepository,
      subscriptionRepository,
      dealRepository,
      entityRepository,
      bankingBeneficiaryRepository,
      emailService,
      tenantService,
      profileRepository,
      bankAccountRepository,
      systemNotificationService,
    );
    await transactionRepository.deleteAll();
    await subscriptionRepository.deleteAll();
    await dealRepository.deleteAll();
    await entityRepository.deleteAll();
    await bankingBeneficiaryRepository.deleteAll();
    await profileRepository.deleteAll();

    entity = await entityRepository.save(
      new Entity({
        tenantId: 'gbt',
        name: 'Test Entity',
        minInvestmentAmount: 10,
        entityType: 'LIMITED_LIABILITY_COMPANY',
        bankAccount: {
          providerMeta: {
            accountStatus: 'active',
            accountId: 'acct_test123',
          },
        },
      }),
    );
    bankAccount = await bankAccountRepository.save(
      new BankAccount({
        ownerId: 'df5e52f6-3db8-4830-87a1-a07ed40fb38d',
        status: 'OPEN',
        tenantId: 'gbt',
        createdAt: new Date(),
        updatedAt: new Date(),
        accountName: 'Test, LLC',
        providerMeta: {
          typeId: 'TREASURYPRIME',
          ein: '123-23-1234',
          accountStatus: 'approved',
          accountApplicationId: 'aact_11gbjjnt2kx41a',
          accountId: 'acct_11gbjjnz2kx424',
        },
        availableBalance: 0,
        currentBalance: 0,
        accountNumber: '420357114',
        bankName: 'The Provident Bank',
        routingNumber: '211374020',
        swiftCode: 'PRDIUS33',
        bankAddress: '5 Market Street Amesbury, MA 01913',
        bankContact: {
          name: 'Sara Spaulding',
          phone: '978-225-1402',
          email: 'sspaulding@bankprov.com',
        },
        beneficiaryAddress:
          '6510 Millrock Drive, Ste 400, Salt Lake City, Utah 84121',
        beneficiaryPhone: '801-419-0677 x2',
      }),
    );
    deal = await dealRepository.save(
      new Deal({
        tenantId: 'gbt',
        name: 'Test Deal',
        entityId: entity.id,
        targetRaiseAmount: 10,
        minInvestmentAmount: 10,
        status: 'OPEN',
      }),
    );
    profile = await profileRepository.save(
      new Profile({
        firstName: 'Richard',
        lastName: 'Austin',
        email: 'raustin@test.com',
        profileType: 'INVESTOR',
        name: 'Richard Austin',
        stateOfFormation: 'UT',
      }),
    );
    subscription = await subscriptionRepository.save(
      new Subscription({
        tenantId: 'gbt',
        name: 'Test User',
        email: 'test@test.com',
        status: 'COMMITTED',
        amount: 5000,
        dealId: deal.id,
        bankAccount: new ExternalBankAccount({
          counterpartyId: 'cp_test123',
        }),
        profileId: profile.id,
      }),
    );
    bankingBeneficiary = await bankingBeneficiaryRepository.save(
      new BankingBeneficiary({
        name: 'Test Beneficiary',
        tenantId: 'gbt',
        serviceObjectId: 'cp_1234test',
        ownerId: 'test_owner',
        providerMeta: {
          type: 'TREASURYPRIME',
          account_number: '1234345',
          routing_number: '12312312',
          id: 'cp_1234test',
          bank_name: 'Bank Of America',
        },
      }),
    );
  });

  describe('Validation Function Tests', () => {
    it('Should throw an error if Banking is missing from Entity', async () => {
      delete entity.bankAccount;
      await saveAllModels();
      await expect(
        transactionService.validateEntityBankingReady(entity),
      ).to.be.rejectedWith(TransactionServiceErrors.ENTITY_MISSING_BANKING);
    });
  });

  describe('Entity Bank Account ACH Transaction Tests', () => {
    it('Should create a valid ACH Transaction with valid Entity Bank Account', async () => {
      const transaction =
        await transactionService.createACHTransactionForEntity(
          entity.id!,
          'CREDIT',
          10,
          'cp_12345',
        );
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.QUEUED);
    });

    it('Should process a valid ACH Transaction with valid Entity Bank Account', async () => {
      let transaction = await transactionService.createACHTransactionForEntity(
        entity.id!,
        'CREDIT',
        10,
        'cp_12345',
        undefined,
        false,
      );
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.QUEUED);
      const creditTransactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'ach_test123',
        status: 'QUEUED',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: 'testentityId',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(
        creditTransactionResponse,
      );

      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(bankingService.stubs.createACHTransaction.calledOnce).to.be.true();
      expect(
        bankingService.stubs.createACHTransaction.getCall(0).args,
      ).to.deepEqual([
        'acct_test123',
        'cp_12345',
        '10.00',
        'CREDIT',
        String(transaction.amount) +
          transaction.counterpartyId +
          entity?.bankAccount?.providerMeta?.accountId +
          transaction.direction +
          new Date().getDay(),
        `${entity.id.substr(-4)} ACH`,
        {
          entityId: entity.id,
        },
      ]);
    });

    it('Should fail to create a valid ACH Transaction with invalid Entity', async () => {
      const transactionPromise =
        transactionService.createACHTransactionForEntity(
          'test',
          'CREDIT',
          10,
          'cp_12345',
        );
      await expect(transactionPromise).to.be.rejected();
    });
    it('Should fail to create a CREDIT ACH Transaction with invalid Entity Bank Account', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountId: 'test',
          accountStatus: 'closed',
        },
      };
      await saveAllModels();
      const transactionPromise =
        transactionService.createACHTransactionForEntity(
          entity.id!,
          'CREDIT',
          10,
          'cp_12345',
        );
      await expect(transactionPromise).to.be.rejectedWith(
        'Entity Bank Account is not ready',
      );
    });
  });

  describe('Bank Account Transaction Creation Tests', () => {
    it('Should throw an error if Banking is missing accountId from Entity', async () => {
      delete entity.bankAccount!.providerMeta!.accountId;
      await saveAllModels();
      await expect(
        transactionService.validateEntityBankingReady(entity),
      ).to.be.rejectedWith(TransactionServiceErrors.ENTITY_MISSING_BANKING);
    });

    it('Should create a WAITING Transaction from a Bank Account without banking', async () => {
      bankAccount = new BankAccount({
        ...bankAccount,
        status: 'PENDING',
        providerMeta: {
          ...bankAccount.providerMeta,
          accountStatus: 'pending',
        },
      });
      await saveAllModels();
      const transaction = await transactionService.createACHTransaction(
        bankAccount.id!,
        'DEBIT',
        10,
        'cp_12345',
      );
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.be.equal(
        TransactionServiceErrors.BANK_ACCOUNT_NOT_READY,
      );
    });

    it('Should create a QUEUED Transaction from a Bank Account with valid status', async () => {
      const transaction = await transactionService.createACHTransaction(
        bankAccount.id!,
        'DEBIT',
        10,
        'cp_12345',
      );
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.QUEUED);
    });
  });

  describe('Subscription Transaction Creation Tests', () => {
    it('Should throw an error if Counterparty ID is missing', async () => {
      delete subscription.bankAccount;
      await saveAllModels();
      await expect(
        transactionService.createSubscriptionTransaction(subscription.id),
      ).to.be.rejectedWith(TransactionServiceErrors.COUNTERPARTY_MISSING);
    });

    it('Should throw an error if Amount is invalid', async () => {
      subscription.amount = -1000;
      await saveAllModels();
      await expect(
        transactionService.createSubscriptionTransaction(subscription.id),
      ).to.be.rejectedWith(TransactionServiceErrors.AMOUNT_INVALID);
    });

    it('Should create a WAITING Transaction from an Entity without banking', async () => {
      delete entity.bankAccount;
      await saveAllModels();
      const transaction =
        await transactionService.createSubscriptionTransaction(subscription.id);
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.be.equal(
        TransactionServiceErrors.ENTITY_MISSING_BANKING,
      );
    });

    it('Should create a WAITING Transaction from an Entity with not ready banking', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      const transaction =
        await transactionService.createSubscriptionTransaction(subscription.id);
      expect(transaction).to.not.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.be.equal(
        TransactionServiceErrors.ENTITY_BANKING_NOT_READY,
      );
    });

    it('Should create a Queued Transaction from a Valid Subscription', async () => {
      const transaction =
        await transactionService.createSubscriptionTransaction(subscription.id);
      expect(transaction).to.not.be.undefined();
      expect(transaction.reason).to.be.undefined();
      expect(transaction.status).to.be.equal(TransactionStatus.QUEUED);
    });
  });

  describe('Process Waiting Transactions Tests', () => {
    it('Should not change status of transaction if Entity is not ready', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.WAITING,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.ENTITY_BANKING_NOT_READY,
      );
    });

    it('Should not QUEUE Transaction from WAITING if Bank Account is not ready', async () => {
      bankAccount = new BankAccount({
        ...bankAccount,
        status: 'PENDING',
        providerMeta: {
          ...bankAccount.providerMeta,
          accountStatus: 'pending',
        },
      });
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.WAITING,
          bankAccountId: bankAccount.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.BANK_ACCOUNT_NOT_READY,
      );
    });

    it('Should not change status of transaction if Entity does not have banking', async () => {
      delete entity.bankAccount;
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.WAITING,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.ENTITY_MISSING_BANKING,
      );
    });

    it('Should only re-queue transaction if Transaction Exception reason is "Too Many Requests"', async () => {
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.TRANSACTION_EXCEPTION,
          reason: TransactionServiceErrors.TOO_MANY_REQUESTS,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      let transaction_other_reason = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.TRANSACTION_EXCEPTION,
          reason: TransactionServiceErrors.SUBSCRIPTION_NOT_READY,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      transaction_other_reason = await transactionRepository.findById(
        transaction_other_reason.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(transaction.reason).to.be.undefined();
      expect(transaction_other_reason.status).to.equal(
        TransactionStatus.TRANSACTION_EXCEPTION,
      );
      expect(transaction_other_reason.reason).to.equal(
        TransactionServiceErrors.SUBSCRIPTION_NOT_READY,
      );
    });

    it('Should queue transaction if Entity is ready', async () => {
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.WAITING,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(transaction.reason).to.be.undefined();
    });
  });

  describe('Processing ACH Transactions', () => {
    it('Should grab the next queued transaction', async () => {
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.be.not.null();
    });
    it('Should not grab the next queued transaction if createdAt date is in the future', async () => {
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: moment().add('1', 'day').toDate(),
        }),
      );
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.be.null();
    });

    it('Should not grab the next queued transaction if needsApproval is true', async () => {
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2021-10-06T17:24:40.251Z'),
          needsApproval: true,
        }),
      );
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.be.null();
    });

    it('Should return null if no valid queued transaction', async () => {
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.be.null();
    });
    it('Should skip the next queued transaction if already processed in current batch', async () => {
      const transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      const nextTransaction = await transactionService.getNextQueuedTransaction(
        'ACH',
        [String(transaction.id)],
      );
      expect(nextTransaction).to.be.null();
    });
    it('Should get the full ACH Debit/Credit Limit if no transactions exist', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.getACHCreditLimit.resolves(10000);
      const {debitLimit, creditLimit} =
        await transactionService.getCurrentACHLimits();
      expect(debitLimit).type('number');
      expect(debitLimit).to.equal(40000);
      expect(creditLimit).type('number');
      expect(creditLimit).to.equal(10000);
    });

    it('Should get an adjusted ACH Debit / Credit Limit based on transactions from the last 24 hours', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.getACHCreditLimit.resolves(10000);
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 500,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: moment().subtract(25, 'hours').toDate(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.WAITING,
          reason: 'Entity Bank Account is not ready',
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );

      const {debitLimit, creditLimit} =
        await transactionService.getCurrentACHLimits();
      expect(debitLimit).type('number');
      expect(debitLimit).to.equal(30000);
      expect(creditLimit).type('number');
      expect(creditLimit).to.equal(4500);
    });

    it('Should process transactions from oldest to newest', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      const transaction_second = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
          createdAt: new Date('2021-10-06T17:24:40.251Z'),
        }),
      );
      const transaction_first = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
        }),
      );
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.not.be.null();
      expect(nextTransaction?.id).to.not.equal(transaction_second.id);
      expect(nextTransaction?.id).to.equal(transaction_first.id);
    });

    it('Should get the adjusted ACH limit', async () => {
      let limitReached = transactionService.achLimitReached(
        {
          amount: 50000,
          direction: 'DEBIT',
        },
        5000,
        5000,
      );
      expect(limitReached).to.equal(true);
      limitReached = transactionService.achLimitReached(
        {
          amount: 50000,
          direction: 'CREDIT',
        },
        5000,
        5000,
      );
      expect(limitReached).to.equal(true);
      limitReached = transactionService.achLimitReached(
        {
          amount: 50000,
          direction: 'DEBIT',
        },
        50000,
        50000,
      );
      expect(limitReached).to.equal(false);
      limitReached = transactionService.achLimitReached(
        {
          amount: 50000,
          direction: 'CREDIT',
        },
        50000,
        50000,
      );
      expect(limitReached).to.equal(false);
    });

    it('Should process valid Debit Transactions if ACH Limit Allows', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.getACHCreditLimit.resolves(0);
      const debitTransactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'debit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(
        debitTransactionResponse,
      );
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );

      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(bankingService.stubs.createACHTransaction.calledOnce).to.be.true();
      expect(
        bankingService.stubs.createACHTransaction.getCall(0).args,
      ).to.deepEqual([
        'acct_test123',
        'cp_test123',
        '5000.00',
        'DEBIT',
        transaction!.amount! + subscription.id + new Date().getDay(),
        `${subscription.id.substr(-4)}:${deal.id.substr(-4)}`,
        {
          subscriptionId: subscription.id,
        },
      ]);
    });

    it('Should only update AccreditationStatus and ProviderMeta when provider transaction created', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.getACHCreditLimit.resolves(0);
      const debitTransactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: 10000,
        counterpartyId: 'new_one',
        createdAt: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '10000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'debit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(
        debitTransactionResponse,
      );
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );

      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(5000);
      expect(transaction.counterpartyId).to.equal(
        subscription.bankAccount?.counterpartyId,
      );
    });

    it('Should not process transaction if has invalid amount', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.getACHCreditLimit.resolves(0);
      subscription.amount = -1000;
      await saveAllModels();
      const transaction_process =
        transactionService.createSubscriptionTransaction(subscription.id);
      await expect(transaction_process).to.be.rejectedWith(
        TransactionServiceErrors.AMOUNT_INVALID,
      );
    });

    it('Should process valid Credit Transactions if ACH Limit Allows', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(0);
      bankingService.stubs.getACHCreditLimit.resolves(5000);
      subscription.status = 'COMPLETED';
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      const outgoing_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [outgoing_transaction.id ?? ''];
      await saveAllModels();
      let transaction =
        await transactionService.createReverseSubscriptionTransaction(
          subscription.id,
        );
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(subscription.reverseTransactionId).to.equal(transaction.id);
      expect(subscription.status).to.equal('REFUNDED');
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(subscription.amount);
      expect(transaction.direction).to.equal('CREDIT');
    });

    it('Should process valid Credit Transactions from Bank Accounts if ACH Limit Allows', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(0);
      bankingService.stubs.getACHCreditLimit.resolves(5000);
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: String(bankAccount?.providerMeta?.accountId),
        amount: 1000.0,
        counterpartyId: 'cp_12345',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '1000.00',
            service: 'standard',
            counterparty_id: 'cp_12345',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: String(bankAccount?.providerMeta?.accountId),
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      let transaction = await transactionService.createACHTransaction(
        bankAccount.id!,
        'CREDIT',
        5000,
        'cp_12345',
      );
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(subscription.amount);
      expect(transaction.direction).to.equal('CREDIT');
    });

    it('Should process valid Debit Transactions from Bank Accounts if ACH Limit Allows', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(5000);
      bankingService.stubs.getACHCreditLimit.resolves(0);
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: String(bankAccount?.providerMeta?.accountId),
        amount: 1000.0,
        counterpartyId: 'cp_12345',
        createdAt: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '1000.00',
            service: 'standard',
            counterparty_id: 'cp_12345',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: String(bankAccount?.providerMeta?.accountId),
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'debit',
            created_at: '2021-03-31T16:04:08Z',
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      let transaction = await transactionService.createACHTransaction(
        bankAccount.id!,
        'DEBIT',
        5000,
        'cp_12345',
      );
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(subscription.amount);
      expect(transaction.direction).to.equal('DEBIT');
    });

    it('Should not process valid Transactions if Debit ACH Limit is too low', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(4000);
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
    });

    it('Should not process valid transaction if Subscription has invalid status', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      subscription.status = 'PENDING';
      await saveAllModels();
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(
        TransactionStatus.TRANSACTION_EXCEPTION,
      );
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.SUBSCRIPTION_NOT_READY,
      );
    });

    it('Should send system alert when transaction moves to EXCEPTION', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      subscription.status = 'PENDING';
      await saveAllModels();
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      systemNotificationService.stubs.Notify_Transaction_Exception.resolves();
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(
        TransactionStatus.TRANSACTION_EXCEPTION,
      );
      expect(
        systemNotificationService.stubs.Notify_Transaction_Exception.calledOnce,
      ).to.be.true();
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.SUBSCRIPTION_NOT_READY,
      );
    });

    it('Should process valid transaction if Subscription has transaction already', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      subscription.transactionIds = ['ach_test321'];
      subscription.status = 'COMMITTED';
      await saveAllModels();
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.reason).to.be.undefined();
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
    });

    it('Should leave transaction in Exception state if error is thrown while processing', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      bankingService.stubs.createACHTransaction.rejects('ACH Limit Reached');
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(
        TransactionStatus.TRANSACTION_EXCEPTION,
      );
    });
  });

  describe('Deprecated Processing Tests', () => {
    it('Should update status of transaction if using Deprecated Waiting status and Entity is not ready', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.DEPRECATED_WAITING_ON_ENTITY,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.ENTITY_BANKING_NOT_READY,
      );
    });

    it('Should queue transaction if using Deprecated Waiting status and Entity is ready', async () => {
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.DEPRECATED_WAITING_ON_ENTITY,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
    });

    it('Should queue transaction if using Deprecated Transaction Failure status and Entity is ready', async () => {
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.DEPRECATED_TRANSACTION_FAILURE,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
    });

    it('Should update transaction status if using Deprecated Transaction Failure status and Entity is not ready', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.DEPRECATED_TRANSACTION_FAILURE,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
    });

    it('Should process valid Transactions with Subscription in FUNDING SENT status', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      subscription.status = 'FUNDING SENT';
      await saveAllModels();
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'debit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      let transaction = await transactionService.createSubscriptionTransaction(
        subscription.id,
      );
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(subscription.amount);
      expect(transaction.direction).to.equal('DEBIT');
    });
  });

  describe('Reverse Transactions', () => {
    it('Should Create a CREDIT Transaction when createReverseSubscriptionTransaction called on a validated subscription', async () => {
      subscription.status = 'COMPLETED';
      const transactionResponse: TransactionACHDetail = {
        type: 'ACH',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'ach_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'ach_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              subscriptionId: 'testsubscriptionid',
            },
          },
        },
      };
      bankingService.stubs.createACHTransaction.resolves(transactionResponse);
      const outgoing_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [outgoing_transaction.id ?? ''];
      await saveAllModels();
      let transaction =
        await transactionService.createReverseSubscriptionTransaction(
          subscription.id,
          outgoing_transaction.amount,
        );
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(subscription.reverseTransactionId).to.equal(transaction.id);
      expect(subscription.status).to.equal('REFUNDED');
      await transactionService.processACHQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(subscription.amount);
      expect(transaction.direction).to.equal('CREDIT');
    });

    it('Should only refund the amount from the previous outgoing transactions', async () => {
      subscription.status = 'COMPLETED';
      subscription.amount = 10000;
      const outgoing_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [outgoing_transaction.id ?? ''];
      await saveAllModels();
      const transaction =
        await transactionService.createReverseSubscriptionTransaction(
          subscription.id,
          5000,
        );
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
      expect(transaction.amount).to.equal(outgoing_transaction.amount);
      expect(subscription.reverseTransactionId).to.equal(transaction.id);
      expect(subscription.status).to.equal('PENDING');
    });

    it("Should throw an error when the previous transactions aren't ready for reversal", async () => {
      subscription.status = 'FUNDING SENT';
      const outgoing_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [outgoing_transaction.id ?? ''];
      await saveAllModels();
      const transaction_action =
        transactionService.createReverseSubscriptionTransaction(
          subscription.id,
          outgoing_transaction.amount,
        );
      await expect(transaction_action).to.be.rejectedWith(
        TransactionServiceErrors.SUBSCRIPTION_EXISTING_TRANSACTION_NOT_RESOLVED,
      );
    });

    it('Should throw an error when the subscription has been closed', async () => {
      subscription.status = 'CLOSED';
      const outgoing_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [outgoing_transaction.id ?? ''];
      await saveAllModels();
      const transaction_action =
        transactionService.createReverseSubscriptionTransaction(
          subscription.id,
          outgoing_transaction.amount,
        );
      await expect(transaction_action).to.be.rejectedWith(
        TransactionServiceErrors.SUBSCRIPTION_CLOSED,
      );
    });

    it('Should process CREDIT ACH transactions from oldest to newest', async () => {
      bankingService.stubs.getACHDebitLimit.resolves(40000);
      const transaction_second = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2021-10-06T17:24:40.251Z'),
        }),
      );
      const transaction_first = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
        }),
      );
      const nextTransaction =
        await transactionService.getNextQueuedTransaction();
      expect(nextTransaction).to.not.be.null();
      expect(nextTransaction?.id).to.not.equal(transaction_second.id);
      expect(nextTransaction?.id).to.equal(transaction_first.id);
      bankingService.stubs.getACHDebitLimit.restore();
    });

    it("Should throw an error when validating a subscription that isn't ready to be processed", async () => {
      const transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.QUEUED,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
        }),
      );
      subscription.status = 'INVITED';
      await subscriptionRepository.save(subscription);
      let status_validation =
        transactionService.validateSubscriptionReadyForTransactionProcessing(
          subscription,
          transaction,
        );
      await expect(status_validation).to.be.rejectedWith(
        TransactionServiceErrors.SUBSCRIPTION_NOT_READY,
      );
      subscription.reverseTransactionId = transaction.id;
      subscription.status = 'CLOSED';
      await subscriptionRepository.save(subscription);
      status_validation =
        transactionService.validateSubscriptionReadyForTransactionProcessing(
          subscription,
          transaction,
        );
      await expect(status_validation).to.not.be.rejected();
      subscription.status = 'REFUNDED';
      subscription.transactionIds = ['initial_transaction'];
      subscription.reverseTransactionId = 'test_other_transaction';
      await subscriptionRepository.save(subscription);
      const duplication_validation =
        transactionService.validateSubscriptionReadyForTransactionProcessing(
          subscription,
          transaction,
        );
      await expect(duplication_validation).to.be.rejectedWith(
        TransactionServiceErrors.SUBSCRIPTION_DUPLICATE_TRANSACTION,
      );
    });
  });

  describe('ACH Transactions Synchronization', () => {
    it('Should update ACH Transaction AccreditationStatus only if changed on bank side', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_test_transaction',
        }),
      );
      bankingService.stubs.getACHTransactionDetail.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'ACH',
      });
      await transactionService.syncACHTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.SENT);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
      bankingService.stubs.getACHTransactionDetail.restore();
    });
    it('Should only update ACH Transaction AccreditationStatus and Provider Meta', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_test_transaction',
        }),
      );
      bankingService.stubs.getACHTransactionDetail.resolves({
        accountID: 'test_account',
        amount: 10000,
        counterpartyId: 'new_one',
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'ACH',
      });
      await transactionService.syncACHTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.SENT);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
      expect(transaction.createdAt).to.not.equal(transaction.updatedAt);
      expect(original_transaction.providerMeta).to.not.equal(
        transaction.providerMeta,
      );
      expect(transaction.amount).to.equal(5000);
      expect(transaction.counterpartyId).to.equal('cp_test123');
      bankingService.stubs.getACHTransactionDetail.restore();
    });
    it('Should not update ACH Transaction AccreditationStatus if not changed on bank side', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_test_transaction',
        }),
      );
      bankingService.stubs.getACHTransactionDetail.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.PENDING,
        type: 'ACH',
      });
      await transactionService.syncACHTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
      bankingService.stubs.getACHTransactionDetail.restore();
    });

    it('Should not update if transactionId is missing', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
        }),
      );
      bankingService.stubs.getACHTransactionDetail.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'ACH',
      });
      await transactionService.syncACHTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
      bankingService.stubs.getACHTransactionDetail.restore();
    });

    it("Should update Subscription status once marked 'SENT' if in 'FUNDING SENT' state", async () => {
      subscription.status = 'FUNDING SENT';

      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_test_transaction',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getACHTransactionDetail.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'ACH',
      });
      await transactionService.syncACHTransactionsBanking();
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(subscription.status).to.equal('COMPLETED');
      bankingService.stubs.getACHTransactionDetail.restore();
    });

    it('Should not crash if encountering an error with updating a subscription status', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_test_transaction',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getACHTransactionDetail.rejects('Test Error');
      const sync_task = transactionService.syncACHTransactionsBanking();
      await expect(sync_task).to.be.fulfilled();
      bankingService.stubs.getACHTransactionDetail.restore();
    });
  });

  describe('Account Transactions Synchronization', () => {
    it('Should extract ACH Transaction ID from Transaction (Old format)', async () => {
      const valid_ach_transaction_with_id: AccountTransaction = {
        amount: 500,
        balance: 500,
        fingerprint: 'asdfasdfasdfsdf',
        date: new Date().toISOString(),
        id: 'ach_test_transaction',
        summary: 'Transaction',
        type: 'deposit',
        ach_id: 'ach_11g6yw4ha312ra',
      };
      expect(
        transactionService.hasACHId(valid_ach_transaction_with_id),
      ).to.equal('ach_11g6yw4ha312ra');
      const valid_ach_transaction: AccountTransaction = {
        amount: 500,
        balance: 500,
        fingerprint: 'asdfasdfasdfsdf',
        date: new Date().toISOString(),
        id: 'ach_test_transaction',
        type: 'deposit',
        summary: 'Glassboard Tech 5405:7143 ID NBR: 11g6yw4ha312ra',
      };
      expect(transactionService.hasACHId(valid_ach_transaction)).to.equal(
        'ach_11g6yw4ha312ra',
      );
      const invalid_ach_transaction: AccountTransaction = {
        amount: 500,
        balance: 500,
        fingerprint: 'asdfasdfasdfsdf',
        date: new Date().toISOString(),
        id: 'ach_test_transaction',
        type: 'deposit',
        summary: 'Glassboard Tech 5405:7143 ID NBR: NONE',
      };
      const invalid_ach_transaction2: AccountTransaction = {
        amount: 500,
        balance: 500,
        fingerprint: 'asdfasdfasdfsdf',
        date: new Date().toISOString(),
        id: 'ach_test_transaction',
        type: 'deposit',
        summary: '',
      };
      expect(
        transactionService.hasACHId(invalid_ach_transaction),
      ).to.be.undefined();
      expect(
        transactionService.hasACHId(invalid_ach_transaction2),
      ).to.be.undefined();
    });

    it('Should extract ACH Transaction ID from Transaction (New Format)', async () => {
      const transactionsMissingACHIDProperty: {
        ach_id: string | undefined;
        transaction: AccountTransactionResponse;
      }[] = [
        {
          ach_id: 'ach_11ghv559crzadr',
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 12000.0,
            date: '2021-08-20',
            wire_id: undefined,
            desc: 'Glassboard Tech - 5b34:a706 - 11ghv559crzadr',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 137500.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4402efct2sx0',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: '3ef8375071bf822a5dc8aeadb4231dd92bf2de7f',
            check_number: undefined,
          },
        },
        {
          ach_id: 'ach_11ghvgwbcs2c5t',
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 8000.0,
            date: '2021-08-20',
            wire_id: undefined,
            desc: 'Glassboard Tech - ce88:a706 - 11ghvgwbcs2c5t',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 125500.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4402eect2sx1',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: '75d2b6b8b962aab586972181d8d1e40bf78ba16f',
            check_number: undefined,
          },
        },
        {
          ach_id: 'ach_11ghxa0vcsmrmh',
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 20000.0,
            date: '2021-08-20',
            wire_id: undefined,
            desc: 'Glassboard Tech - db3b:a706 - 11ghxa0vcsmrmh',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 117500.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4402edct2sx2',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: '2fe02dd54543d7c8984b205864fb1ca86682c6c2',
            check_number: undefined,
          },
        },
        {
          ach_id: undefined,
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 10500.0,
            date: '2021-08-19',
            wire_id: undefined,
            desc: 'WIRE IN NARAYAN K AGRAWAL +',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 97500.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4302eccsjd63',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: '0c0debce269fe740866050c22fd31558998236a1',
            check_number: undefined,
          },
        },
        {
          ach_id: 'ach_11ghrkzxcr4vst',
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 14000.0,
            date: '2021-08-19',
            wire_id: undefined,
            desc: 'Glassboard Tech - 37d5:a706 - 11ghrkzxcr4vst',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 87000.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4302ebcs8s3q',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: '937d0f4339e1a5c43901779a41127e710599e527',
            check_number: undefined,
          },
        },
        {
          ach_id: 'ach_11ghpd2ncqcg8z',
          transaction: {
            check_id: undefined,
            type_source: undefined,
            amount: 10000.0,
            date: '2021-08-18',
            wire_id: undefined,
            desc: 'Glassboard Tech - fcc0:a706 - 11ghpd2ncqcg8z',
            wire: undefined,
            book_id: undefined,
            type: 'deposit',
            incoming_wire: undefined,
            summary: '',
            balance: 73000.0,
            billpay_payment_id: undefined,
            id: 'ctx_1k8s4202eacrd4xp',
            trace_id: undefined,
            extended_timestamp: undefined,
            ach_id: undefined,
            fingerprint: 'cce0b2fa0462433704fe037e5dbc3741dc9941dc',
            check_number: undefined,
          },
        },
      ];
      const valid_ach_transaction = {
        accountID: 'test_account',
        amount: 500,
        balance: 500,
        counterpartyId: 'cp_1200',
        fingerprint: 'asdfasdfasdfsdf',
        createdAt: new Date().toISOString(),
        date: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'ach_test_transaction',
        status: TransactionStatus.SENT,
        type: 'ACH',
        summary: 'Glassboard Tech - a0da:761a - 11ghr2pncqyr1a',
      };
      expect(transactionService.hasACHId(valid_ach_transaction)).to.equal(
        'ach_11ghr2pncqyr1a',
      );
      for (const item of transactionsMissingACHIDProperty) {
        expect(transactionService.hasACHId(item.transaction)).to.equal(
          item.ach_id,
        );
      }
    });

    it('Should attach ACH Debit transaction ID from Bank Account Transaction to Transaction Object', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          accountID: 'acct_test',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_11g6yw4ha312ra',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getDepositAccountTransactions.resolves([
        {
          amount: 5000.0,
          date: '2021-04-13',
          desc: '',
          summary: 'Glassboard Tech 96d1:7143 ID NBR: 11g6yw4ha312ra',
          balance: 10000.3,
          id: 'ctx_testqd02f0a676pe',
          type: 'deposit',
        },
      ]);
      await transactionService.syncAccountTransactionBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.bankAccountTransactionId).to.equal(
        'ctx_testqd02f0a676pe',
      );
    });

    it('Should not attach ACH Debit transaction ID from Bank Account Transaction to Transaction Object if match not found', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          accountID: 'acct_test',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_11g6yw4ha312ra',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getDepositAccountTransactions.resolves([
        {
          amount: 5000.0,
          date: '2021-04-13',
          desc: '',
          summary: 'Glassboard Tech 96d1:7143 ID NBR: 11g6yw4ha312rb',
          balance: 10000.3,
          id: 'ctx_testqd02f0a676pe',
          type: 'deposit',
        },
      ]);
      await transactionService.syncAccountTransactionBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.bankAccountTransactionId).to.be.undefined();
    });

    it("Should not attach ACH Debit transaction ID from Bank Account Transaction to Transaction Object if doesn't include ID", async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          accountID: 'acct_test',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_11g6yw4ha312ra',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getDepositAccountTransactions.resolves([
        {
          amount: 5000.0,
          date: '2021-04-13',
          desc: '',
          summary: 'Glassboard Tech 96d1:7143 ID',
          balance: 10000.3,
          id: 'ctx_testqd02f0a676pe',
          type: 'deposit',
        },
      ]);
      await transactionService.syncAccountTransactionBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.bankAccountTransactionId).to.be.undefined();
    });

    it('Should attach ACH Credit transaction ID from Bank Account Transaction to Transaction Object', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          accountID: 'acct_test',
          direction: 'CREDIT',
          type: 'ACH',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'ach_11g6yw4ha312ra',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getDepositAccountTransactions.resolves([
        {
          amount: 5000.0,
          date: '2021-04-13',
          desc: '',
          summary: 'Glassboard Tech 96d1:7143 ID NBR: 11g6yw4ha312ra',
          balance: 10000.3,
          id: 'ctx_testqd02f0a676pe',
          type: 'deposit',
        },
      ]);
      await transactionService.syncAccountTransactionBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.refundBankAccountTransactionId).to.equal(
        'ctx_testqd02f0a676pe',
      );
    });
  });
  describe('Wire Transaction Tests', () => {
    it('Should create a wire transaction for an entity', async () => {
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const transactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: 'entityId',
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(transactionResponse);
      const transaction =
        await transactionService.createWireTransactionForEntity(
          entity.id,
          'CREDIT',
          1000,
          bankingBeneficiary.id!,
          'Dealio Investor Tester',
        );
      expect(bankingService.stubs.createWireTransfer.called).to.be.false();
      expect(transaction.id).to.be.not.null();
    });
    it('Should throw an error if not provided a valid entity id', async () => {
      const createTransaction =
        transactionService.createWireTransactionForEntity(
          'test',
          'CREDIT',
          1000,
          bankingBeneficiary.id!,
          'Dealio Investor Tester',
        );
      await expect(createTransaction).to.be.rejected();
    });
    it('Should throw an error if not provided a valid beneficiary id', async () => {
      const createTransaction =
        transactionService.createWireTransactionForEntity(
          entity.id,
          'CREDIT',
          1000,
          'test',
          'Dealio Investor Tester',
        );
      await expect(createTransaction).to.be.rejected();
    });
    it('Should throw an error if not provided a valid beneficiary object with serviceObjectId', async () => {
      delete bankingBeneficiary.serviceObjectId;
      await saveAllModels();
      const createTransaction =
        transactionService.createWireTransactionForEntity(
          entity.id,
          'CREDIT',
          1000,
          bankingBeneficiary.id!,
          'Dealio Investor Tester',
        );
      await expect(createTransaction).to.be.rejected();
    });

    it('Should create a wire transaction for standalone banking', async () => {
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const transactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: 'entityId',
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(transactionResponse);
      const transaction = await transactionService.createWireTransaction(
        bankAccount.id!,
        'CREDIT',
        1000,
        bankingBeneficiary.id!,
        'Dealio Investor Tester',
      );
      expect(bankingService.stubs.createWireTransfer.called).to.be.false();
      expect(transaction.id).to.be.not.null();
    });

    it("Should throw an error if a wire transaction for standalone banking doesn't have enough funds", async () => {
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 0,
      });
      const transactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity.bankAccount?.providerMeta?.accountId ?? '',
        amount: subscription.amount ?? 5000,
        counterpartyId: subscription.bankAccount?.counterpartyId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: 'cp_test123',
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: 'entityId',
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(transactionResponse);
      const transaction = transactionService.createWireTransaction(
        bankAccount.id!,
        'CREDIT',
        1000,
        bankingBeneficiary.id!,
        'Dealio Investor Tester',
      );
      await expect(transaction).to.be.rejectedWith(
        TransactionServiceErrors.INSUFFICIENT_FUNDS,
      );
      expect(bankingService.stubs.createWireTransfer.called).to.be.false();
    });

    it('Should process valid Credit Transactions if Wire Limit Allows', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const debitTransactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity!.bankAccount!.providerMeta!.accountId!,
        amount: 5000,
        counterpartyId: bankingBeneficiary.serviceObjectId!,
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: bankingBeneficiary.serviceObjectId,
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: entity.id,
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(
        debitTransactionResponse,
      );
      let transaction = await transactionService.createWireTransactionForEntity(
        entity.id,
        'CREDIT',
        5000,
        bankingBeneficiary.id!,
        'Transfer the money',
      );
      transaction.needsApproval = false;
      await transactionRepository.save(transaction);

      await transactionService.processWireQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
    });
    it('Should only update AccreditationStatus and ProviderMeta when provider transaction created', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const debitTransactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity!.bankAccount!.providerMeta!.accountId!,
        amount: 100000,
        counterpartyId: 'new_one',
        createdAt: new Date().toISOString(),
        direction: 'DEBIT',
        id: 'wire_test123_duplicate',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '10000.00',
            service: 'standard',
            counterparty_id: bankingBeneficiary.serviceObjectId,
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123_duplicate',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: entity.id,
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(
        debitTransactionResponse,
      );
      let transaction = await transactionService.createWireTransactionForEntity(
        entity.id,
        'CREDIT',
        5000,
        bankingBeneficiary.id!,
        'Transfer the money',
      );
      transaction.needsApproval = false;
      await transactionRepository.save(transaction);

      await transactionService.processWireQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.amount).to.equal(5000);
      expect(transaction.counterpartyId).to.equal(
        bankingBeneficiary.serviceObjectId!,
      );
      expect(transaction.transactionId).to.equal('wire_test123_duplicate');
      expect(transaction.providerMeta).to.not.equal(null);
    });

    it('Should not process valid Credit Transactions if Wire Limit exceeded', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(0);
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const debitTransactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity!.bankAccount!.providerMeta!.accountId!,
        amount: 5000,
        counterpartyId: bankingBeneficiary.serviceObjectId!,
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: bankingBeneficiary.serviceObjectId,
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: entity.id,
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(
        debitTransactionResponse,
      );
      let transaction = await transactionService.createWireTransactionForEntity(
        entity.id,
        'CREDIT',
        5000,
        bankingBeneficiary.id!,
        'Transfer the money',
      );

      await transactionService.processWireQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.QUEUED);
    });

    it('Should catch Wire Transfer exceptions on Transaction object', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      bankingService.stubs.createWireTransfer.rejects(
        new Error('Wire Limit Reached'),
      );
      let transaction = await transactionService.createWireTransactionForEntity(
        entity.id,
        'CREDIT',
        5000,
        bankingBeneficiary.id!,
        'Transfer the money',
      );
      transaction.needsApproval = false;
      await transactionRepository.save(transaction);

      await transactionService.processWireQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(
        TransactionStatus.TRANSACTION_EXCEPTION,
      );
      expect(transaction.reason).to.equal('Wire Limit Reached');
    });

    it('Should get the full Wire Credit Limit if no transactions exist', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      const {creditLimit} = await transactionService.getCurrentWireLimits();
      expect(creditLimit).type('number');
      expect(creditLimit).to.equal(40000);
    });

    it('Should get an adjusted Wire Credit Limit based on transactions from the last 24 hours', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 500,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date(),
        }),
      );
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 500,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          addedToNetwork: new Date('02-21-1999'),
        }),
      );
      const {creditLimit} = await transactionService.getCurrentWireLimits();
      expect(creditLimit).type('number');
      expect(creditLimit).to.equal(34500);
    });

    it('Should not change status of transaction if Entity is not ready', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.WAITING,
          entitiesId: entity.id,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions('WIRE');
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.ENTITY_BANKING_NOT_READY,
      );
    });

    it('Should capture an exception if Entity link is not present', async () => {
      entity.bankAccount = {
        providerMeta: {
          accountStatus: 'pending',
          accountId: 'acct_test123',
        },
      };
      await saveAllModels();
      let transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.WAITING,
          dealId: deal.id,
        }),
      );
      await transactionService.processWaitingTransactions('WIRE');
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.WAITING);
      expect(transaction.reason).to.equal(
        TransactionServiceErrors.ENTITY_BANKING_NOT_READY,
      );
    });

    it('Should throw an error if a wire is attempted without the funds available', async () => {
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 100,
      });
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      const debitTransactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity!.bankAccount!.providerMeta!.accountId!,
        amount: 5000,
        counterpartyId: bankingBeneficiary.serviceObjectId!,
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: bankingBeneficiary.serviceObjectId,
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: entity.id,
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(
        debitTransactionResponse,
      );
      const transaction_action =
        transactionService.createWireTransactionForEntity(
          entity.id,
          'CREDIT',
          5000,
          bankingBeneficiary.id!,
          'Transfer the money',
        );

      await expect(transaction_action).to.be.rejectedWith(
        TransactionServiceErrors.INSUFFICIENT_FUNDS,
      );
    });

    it('Should set transaction for immediate processing if auto-approval is false', async () => {
      bankingService.stubs.getWireCreditLimit.resolves(40000);
      bankingService.stubs.getDepositAccountDetails.resolves({
        currentBalance: 40000,
      });
      const debitTransactionResponse: TransactionWireDetail = {
        type: 'WIRE',
        accountID: entity?.bankAccount?.providerMeta?.accountId ?? '',
        amount: 5000,
        counterpartyId: bankingBeneficiary.serviceObjectId ?? '',
        createdAt: new Date().toISOString(),
        direction: 'CREDIT',
        id: 'wire_test123',
        status: 'PENDING',
        providerMeta: {
          typeId: BankingTreasuryPrimeService.typeId,
          ...{
            description: null,
            amount: '5000.00',
            service: 'standard',
            counterparty_id: bankingBeneficiary.serviceObjectId,
            bankdata: null,
            bank_id: 'bank_pvbc',
            account_id: 'acct_test123',
            addenda: [],
            org_id: 'org_glassboard',
            batch_key: null,
            effective_date: '2021-03-31',
            updated_at: '2021-03-31T16:04:08Z',
            status: 'pending',
            id: 'wire_test123',
            error: null,
            sec_code: 'web',
            direction: 'credit',
            created_at: '2021-03-31T16:04:08Z',
            userdata: {
              entityId: entity.id,
            },
          },
        },
      };
      bankingService.stubs.createWireTransfer.resolves(
        debitTransactionResponse,
      );

      const needsApproval = false;
      let transaction = await transactionService.createWireTransactionForEntity(
        entity.id,
        'CREDIT',
        5000,
        bankingBeneficiary.id ?? '',
        'Transfer the money',
        undefined,
        undefined,
        needsApproval,
      );

      expect(transaction.needsApproval).to.equal(false);

      await transactionService.processWireQueuedTransactions();
      transaction = await transactionRepository.findById(transaction.id);
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
    });
  });

  describe('Wire Transactions Synchronization', () => {
    it('Should update Wire Transaction AccreditationStatus only if changed on bank side', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'wire_test_transaction',
        }),
      );
      bankingService.stubs.getWireTransfer.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'wire_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'WIRE',
      });
      await transactionService.syncWireTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.SENT);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
    });
    it('Should send Notification email for SENT status change if updateEmail exists on transaction', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'wire_test_transaction',
          updateEmail: 'test@test.com',
          entitiesId: entity.id,
        }),
      );
      emailService.stubs.sendWireNotificationEmail.resolves();
      bankingService.stubs.getWireTransfer.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'wire_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'WIRE',
      });
      await transactionService.syncWireTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.SENT);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
      expect(emailService.stubs.sendWireNotificationEmail.called).to.be.true();
    });
    it('Should not update Wire Transaction AccreditationStatus if not changed on bank side', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'wire_test_transaction',
        }),
      );
      bankingService.stubs.getWireTransfer.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'wire_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.PENDING,
        type: 'WIRE',
      });
      await transactionService.syncWireTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
    });

    it('Should not update if transactionId is missing', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          entitiesId: entity.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
        }),
      );
      bankingService.stubs.getWireTransfer.resolves({
        accountID: 'test_account',
        amount: original_transaction.amount,
        counterpartyId: original_transaction.counterpartyId,
        createdAt: original_transaction.createdAt!.toISOString(),
        direction: original_transaction.direction,
        id: 'ach_test_transaction',
        providerMeta: {
          typeId: 'TREASURYPRIME',
        },
        status: TransactionStatus.SENT,
        type: 'WIRE',
      });
      await transactionService.syncACHTransactionsBanking();
      const transaction = await transactionRepository.findById(
        original_transaction.id,
      );
      expect(transaction.status).to.equal(TransactionStatus.PENDING);
      expect(transaction.updatedAt).to.not.equal(
        original_transaction.updatedAt,
      );
    });

    it('Should not crash if encountering an error with updating a wire status', async () => {
      const original_transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.PENDING,
          subscriptionId: subscription.id,
          dealId: deal.id,
          createdAt: new Date('2020-10-06T17:24:40.251Z'),
          updatedAt: new Date('2020-10-06T17:24:40.251Z'),
          transactionId: 'wire_test_transaction',
        }),
      );
      subscription.transactionIds = [original_transaction.id ?? ''];
      await subscriptionRepository.save(subscription);
      bankingService.stubs.getWireTransfer.rejects('Test Error');
      const sync_task = transactionService.syncWireTransactionsBanking();
      await expect(sync_task).to.be.fulfilled();
    });
  });
  describe('Wire Matching tests', () => {
    it('Should return correct values for Levenshtein substrings', async () => {
      const test_cases = [
        ['Uluru', '', 5],
        ['Machu Picchu', 'Machu Picchu', 0],
        ['Machu Picchu', 'Where is Machu Picchu?', 0],
        ['Machu Picchu', 'Where is Machu Pichu?', 1],
        ['Machu Picchu', 'Where is Macchu Picchu?', 1],
        ['Goon, LLC', 'WIRE: GOON, LLC', 0],
        ['Goon, LLC', 'WIRE: GOON, LL', 4],
        ['Tom Bradley', 'WIRE: TOM N BRADLEY', 0],
        ['', '', 0],
        ['', 'Machu Picchu', 12],
        ['u', 'Machu Picchu', 12],
        ['z', 'Machu Picchu', 12],
        ['Vinit Chopade', 'WIRE IN VINIT SUDHAKAR CHOPADE, SONAL VINIT', 0],
        ['Rich August', 'WIRE IN TAY COFF CHUA ONG', 8],
      ];

      test_cases.forEach(([needle, haystack, distance]) => {
        expect(
          transactionService.levenshtein_substring_distance(
            String(needle),
            String(haystack),
          ),
        ).to.equal(distance);
      });
    });

    it('Should match a wire transaction to a profile if the name matches exactly', async () => {
      const transaction: AccountTransaction = {
        amount: Number(subscription.amount),
        date: moment(subscription.createdAt)
          .add('1', 'day')
          .format('YYYY-MM-DD'),
        desc: 'WIRE IN RICHARD AUSTIN CHUA ONG',
        type: 'deposit',
        summary: '',
        balance: 48000.0,
        id: 'ctx_1k8fqm02e67cp49y',
        fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
        providerMeta: {
          amount: subscription.amount,
          date: moment().format('YYYY-MM-DD'),
          wire_id: null,
          desc: 'WIRE IN RICHARD AUSTIN CHUA ONG',
          wire: null,
          book_id: null,
          type: 'deposit',
          summary: '',
          balance: '48000.00',
          billpay_payment_id: null,
          id: 'ctx_1k8fqm02e67cp49y',
          trace_id: null,
          ach_id: null,
          fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
          check_number: null,
        },
      };
      const match_found = await transactionService.checkForWireLink(
        transaction,
        entity,
      );
      expect(match_found).to.be.true();
    });

    it('Should match a wire transaction to a profile if the name almost matche', async () => {
      const transaction: AccountTransaction = {
        amount: Number(subscription.amount),
        date: moment(subscription.createdAt)
          .add('1', 'day')
          .format('YYYY-MM-DD'),
        desc: 'WIRE IN RICHARD AUSTI CHUA ONG',
        type: 'deposit',
        summary: '',
        balance: 48000.0,
        id: 'ctx_1k8fqm02e67cp49y',
        fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
        providerMeta: {
          amount: subscription.amount,
          date: moment().format('YYYY-MM-DD'),
          wire_id: null,
          desc: 'WIRE IN RICHARD AUSTI CHUA ONG',
          wire: null,
          book_id: null,
          type: 'deposit',
          summary: '',
          balance: '48000.00',
          billpay_payment_id: null,
          id: 'ctx_1k8fqm02e67cp49y',
          trace_id: null,
          ach_id: null,
          fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
          check_number: null,
        },
      };
      const match_found = await transactionService.checkForWireLink(
        transaction,
        entity,
      );
      expect(match_found).to.be.true();
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(subscription.bankTransactionIds).to.containEql(transaction.id);
    });

    it("Should not match a wire transaction to a profile if the name isn't close", async () => {
      const transaction: AccountTransaction = {
        amount: Number(subscription.amount),
        date: moment(subscription.createdAt)
          .add('1', 'day')
          .format('YYYY-MM-DD'),
        desc: 'WIRE IN TAY COFF CHUA ONG',
        type: 'deposit',
        summary: '',
        balance: 48000.0,
        id: 'ctx_1k8fqm02e67cp49y',
        fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
        providerMeta: {
          amount: subscription.amount,
          date: moment().format('YYYY-MM-DD'),
          wire_id: null,
          desc: 'WIRE IN TAY COFF CHUA ONG',
          wire: null,
          book_id: null,
          type: 'deposit',
          summary: '',
          balance: '48000.00',
          billpay_payment_id: null,
          id: 'ctx_1k8fqm02e67cp49y',
          trace_id: null,
          ach_id: null,
          fingerprint: 'b44ff527c0fc820a7bc54d349bac18fb367bf8f6',
          check_number: null,
        },
      };
      const match_found = await transactionService.checkForWireLink(
        transaction,
        entity,
      );
      expect(match_found).to.be.false();
      expect(subscription.bankTransactionIds).to.not.containEql(transaction.id);
    });
  });
});
