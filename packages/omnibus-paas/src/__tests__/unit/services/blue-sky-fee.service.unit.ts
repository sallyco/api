import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {
  BlueSkyFeeRepository,
  CloseRepository,
  SubscriptionRepository,
} from '../../../repositories';
import {BlueSkyFeeService} from '../../../services';

const close = [
  {
    statement: {
      sections: [
        {
          title: 'Closing Statement Summary',
          entries: [
            {
              description: 'Bank Balance',
              proceeds: '0',
            },
            {
              description: 'Proceeds Collected From the Investors',
              proceeds: '$2.00',
              isInvestorRow: true,
            },
            {
              description: 'Total Amount Witheld From Investor Proceeds',
              expenses: '$10,009.00',
            },
            {
              description: 'Amount of Proceeds to be sent to Portfolio Company',
              proceeds: '$-10,007.00',
              bold: true,
            },
          ],
        },
        {
          title: 'SPV Expenses',
          subTitle: 'Entity Management & Maintenance Fees',
          entries: [
            {
              description: 'EIN Obtainment',
              expenses: '$49.00',
            },
          ],
        },
        {
          title: 'Administration Fees',
          entries: [
            {
              description: 'SPV Administration Fixed Cost per SPV',
              expenses: '$9,500.00',
            },
            {
              description: 'Management Fees',
              expenses: '$0.00',
            },
            {
              description: 'Expense Reserve',
              expenses: '$0.00',
            },
          ],
        },
        {
          title: 'Tax and Accounting Fees',
          entries: [
            {
              description: 'Tax and K1 Processing',
              expenses: 'included',
            },
          ],
        },
        {
          title: 'Regulatory Filings Fees',
          entries: [
            {
              description: 'NASAA EFD System Fee',
              expenses: '$160.00',
            },
            {
              description: 'Blue Sky - California',
              expenses: '$300.00',
            },
          ],
        },
        {
          title: 'TOTALS',
          entries: [
            {
              description: '',
              expenses: '$10,009.00',
            },
          ],
        },
      ],
    },
  },
];

describe('BlueSkyFeeService (unit)', () => {
  let blueSkyFeeService: BlueSkyFeeService;
  let subscriptionRepository: StubbedInstanceWithSinonAccessor<SubscriptionRepository>;
  let blueSkyFeeRepository: StubbedInstanceWithSinonAccessor<BlueSkyFeeRepository>;
  let closeRepository: StubbedInstanceWithSinonAccessor<CloseRepository>;

  beforeEach(async () => {
    blueSkyFeeService = new BlueSkyFeeService(
      blueSkyFeeRepository,
      subscriptionRepository,
      closeRepository,
    );
  });

  it('Should calculate total Blue Sky fees per state from close', async () => {
    let state = 'California';
    let totalBlueSkyFees = await blueSkyFeeService.getBlueSkyFeesPerState(
      close,
      state,
    );
    expect(totalBlueSkyFees).to.be.equal(300);

    state = 'Utah';
    totalBlueSkyFees = await blueSkyFeeService.getBlueSkyFeesPerState(
      close,
      state,
    );
    expect(totalBlueSkyFees).to.be.equal(0);
  });

  it('Should calculate total NASAA Sky fees from close', async () => {
    const totalNASAAFee = await blueSkyFeeService.getTotalNASAAFee(close);
    expect(totalNASAAFee).to.be.equal(160);
  });
});
