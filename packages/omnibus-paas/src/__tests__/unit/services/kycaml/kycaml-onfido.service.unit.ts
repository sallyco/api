import {ApplicantRequest} from '../../../../services/kycaml/kycaml.service.types';
import {ISOHelper} from '../../../../interfaces/datatypes/ISO';
import {KycAmlOnfidoService, Onfido} from '../../../../services';
import {StubbedInstance, stubInterface} from 'ts-sinon';
import {expect} from '@loopback/testlab';

let TestApplicant: ApplicantRequest;

let onfidoService: StubbedInstance<Onfido>;
let service: KycAmlOnfidoService;

beforeEach(async () => {
  onfidoService = stubInterface<Onfido>();
  service = new KycAmlOnfidoService(onfidoService);
  TestApplicant = {
    address: {
      street1: '123 Test',
      street2: 'Unit 1',
      city: 'Provo',
      region: 'Utah',
      country: ISOHelper.convertCountryNameToISO_2('United States of America'),
      postalCode: '84005',
    },
    dob: new Date('02-21-1995').toUTCString(),
    email: 'test@test.com',
    firstName: 'John',
    identification: [
      {
        type: 'ssn',
        value: '123-23-1234',
      },
    ],
    lastName: 'Doe',
    idNumbers: [
      {
        type: 'itin',
        value: '123-12-1234',
      },
    ],
  };
});

describe('KYC / AML Onfido Service Tests', () => {
  it('Should convert base Applicant into the correct format for Onfido', async () => {
    onfidoService.create_applicant.resolves({
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      dob: '1995-02-21',
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
    });
    const applicant = await service.createApplicant(TestApplicant);
    expect(applicant).to.be.not.null();
    expect(applicant.providerMeta?.typeId).to.equal(service.typeId);
  });

  it('Should create an applicant if DOB is missing', async () => {
    onfidoService.create_applicant.resolves({
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
    });
    delete TestApplicant.dob;
    const applicant = await service.createApplicant(TestApplicant);
    expect(applicant).to.be.not.null();
    expect(applicant.providerMeta?.typeId).to.equal(service.typeId);
    expect(applicant.dob).to.be.undefined();
  });

  it('Should create an applicant if street2 is missing', async () => {
    onfidoService.create_applicant.resolves({
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
    });
    delete TestApplicant.address?.street2;
    const applicant = await service.createApplicant(TestApplicant);
    expect(applicant).to.be.not.null();
    expect(applicant.providerMeta?.typeId).to.equal(service.typeId);
    expect(applicant.address?.street2).to.be.undefined();
  });

  it('Should collect an applicant by id if it exists', async () => {
    onfidoService.find_applicant.resolves({
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      dob: '1995-02-21',
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
    });
    const applicant = await service.getApplicant(
      '0a001844-eead-4c20-acd1-b627e6ded4ad',
    );
    expect(applicant).to.be.not.null();
    expect(applicant.dob).to.equal('1995-02-21');
  });

  it("Should throw an error if the applicant doesn't exist", async () => {
    onfidoService.find_applicant.throwsException(() => {
      return new Error('Not Found');
    });
    const applicant_request = service.getApplicant(
      '0a001844-eead-4c20-acd1-b627e6ded4ad',
    );
    await expect(applicant_request).to.be.rejectedWith('Not Found');
  });

  it('Should convert base Check request into correct format for Onfido', async () => {
    onfidoService.find_applicant.resolves({
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      dob: '1995-02-21',
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
    });
    onfidoService.create_check.resolves({
      applicant_id: '<APPLICANT_ID>',
      applicant_provides_data: false,
      created_at: '2021-03-22T17:13:12Z',
      form_uri: null,
      href: '/v3.1/checks/<CHECK_ID>',
      id: '<CHECK_ID>',
      paused: false,
      privacy_notices_read_consent_given: true,
      redirect_uri: null,
      report_ids: ['<REPORT_ID>'],
      result: null,
      results_uri: '<RESULTS_URI>',
      sandbox: true,
      status: 'in_progress',
      tags: [],
    });
    const check = await service.createCheck({
      applicantId: '<APPLICANT_ID>',
    });
    expect(check.providerMeta?.typeId).to.equal(service.typeId);
    expect(check.status).to.equal('IN_PROGRESS');
  });

  it('Should collect a Check object by Id', async () => {
    onfidoService.find_report.resolves({
      breakdown: {},
      check_id: '<CHECK_ID>',
      documents: [],
      name: 'Test Report',
      properties: {},
      status: 'complete',
      sub_result: 'clear',
      created_at: '2021-03-22T17:13:12Z',
      href: '/v3.1/reports/<REPORT_ID>',
      id: '<REPORT_ID>',
      result: 'clear',
    });
    onfidoService.find_check.resolves({
      applicant_id: '<APPLICANT_ID>',
      applicant_provides_data: false,
      created_at: '2021-03-22T17:13:12Z',
      form_uri: null,
      href: '/v3.1/checks/<CHECK_ID>',
      id: '<CHECK_ID>',
      paused: false,
      privacy_notices_read_consent_given: true,
      redirect_uri: null,
      report_ids: ['<REPORT_ID>', '<RESPORT_ID2>'],
      result: null,
      results_uri: '<RESULTS_URI>',
      sandbox: true,
      status: 'in_progress',
      tags: [],
    });
    const check = await service.getCheck('<CHECK_ID>');
    expect(onfidoService.find_check.calledOnce).to.equal(true);
    expect(onfidoService.find_report.calledTwice).to.equal(true);
    expect(check.providerMeta?.typeId).to.equal(service.typeId);
    expect(check.status).to.equal('IN_PROGRESS');
  });

  it('Should get the correct enabled reports based on available properties on the applicant', async () => {
    onfidoService.find_applicant.resolves({
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      dob: '1995-02-21',
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
    });
    const allowed_reports = await service.getAllowedReportsByApplicantId(
      '0a001844-eead-4c20-acd1-b627e6ded4ad',
    );
    expect(allowed_reports).to.deepEqual([
      'watchlist_enhanced',
      'identity_enhanced',
    ]);
  });

  it('Should restrict run reports based on available properties on the applicant', async () => {
    onfidoService.find_applicant.resolves({
      address: {
        country: 'USA',
        line1: '123 Test',
        line2: 'Unit 1',
        postcode: '84005',
        state: 'UT',
      },
      created_at: '2021-05-13T19:25:57Z',
      delete_at: null,
      email: 'test@test.com',
      first_name: 'John',
      href: '/v3.1/applicants/0a001844-eead-4c20-acd1-b627e6ded4ad',
      id: '0a001844-eead-4c20-acd1-b627e6ded4ad',
      id_numbers: [],
      last_name: 'Doe',
    });
    const allowed_reports = await service.getAllowedReportsByApplicantId(
      '0a001844-eead-4c20-acd1-b627e6ded4ad',
    );
    expect(allowed_reports).to.deepEqual(['watchlist_enhanced']);
  });

  it('identity_enhanced failure other than "clear" should result in "CONSIDER" status on check', async () => {
    onfidoService.find_report.onFirstCall().resolves({
      breakdown: {
        adverse_media: {
          result: 'consider',
        },
        monitored_lists: {
          result: 'consider',
        },
        politically_exposed_person: {
          result: 'consider',
        },
        sanction: {
          result: 'clear',
        },
      },
      check_id: 'e215c3d2-0493-4c5b-890e-a785754f7322',
      created_at: '2021-09-15T22:19:16Z',
      documents: [],
      href: '/v3.1/reports/59e72232-b61b-415b-83a4-e3d5c7fc5a04',
      id: '59e72232-b61b-415b-83a4-e3d5c7fc5a04',
      name: 'watchlist_enhanced',
      properties: {
        records: [
          {
            address: [
              {
                country: 'USA',
                locator_type: 'BIRTH',
                state_province: 'New York',
                town: 'New York',
              },
              {
                address_line1: 'Some Street',
                country: 'GBR',
                postal_code: 'AA0A 0AA',
                state_province: 'England',
                town: 'London',
              },
              {
                address_line1: 'Another Street',
                country: 'GBR',
                postal_code: 'BB0 0BB',
                state_province: 'England',
                town: 'London',
              },
            ],
            alias: [
              {
                alias_name: 'Taylor Consider',
                alias_type: 'AKA',
              },
            ],
            associate: [
              {
                entity_name: 'name 1',
                relationship_direction: 'FROM',
                relationship_type: 'GRANDFATHER',
              },
              {
                entity_name: 'name 2',
                relationship_direction: 'FROM',
                relationship_type: 'STEP_FATHER',
              },
              {
                entity_name: 'name 3',
                relationship_direction: 'FROM',
                relationship_type: 'FRIEND',
              },
              {
                entity_name: 'name 4',
                relationship_direction: 'FROM',
                relationship_type: 'FRIEND',
              },
              {
                entity_name: 'name 5',
                relationship_direction: 'FROM',
                relationship_type: 'FRIEND',
              },
              {
                entity_name: 'name 6',
                relationship_direction: 'FROM',
                relationship_type: 'GRANDFATHER',
              },
              {
                entity_name: 'name 7',
                relationship_direction: 'FROM',
                relationship_type: 'GRANDFATHER',
              },
              {
                entity_name: 'name 8',
                relationship_direction: 'FROM',
                relationship_type: 'STEP_DAUGHTER',
              },
              {
                entity_name: 'name 9',
                relationship_direction: 'FROM',
                relationship_type: 'WIFE',
              },
              {
                entity_name: 'name 10',
                relationship_direction: 'FROM',
                relationship_type: 'DAUGHTER',
              },
              {
                entity_name: 'name 11',
                relationship_direction: 'FROM',
                relationship_type: 'FATHER_IN_LAW',
              },
              {
                entity_name: 'name 12',
                relationship_direction: 'FROM',
                relationship_type: 'FATHER',
              },
              {
                entity_name: 'name 13',
                relationship_direction: 'FROM',
                relationship_type: 'MOTHER',
              },
              {
                entity_name: 'name 14',
                relationship_direction: 'FROM',
                relationship_type: 'DAUGHTER',
              },
              {
                entity_name: 'name 15',
                relationship_direction: 'FROM',
                relationship_type: 'SON',
              },
              {
                entity_name: 'name 16',
                relationship_direction: 'FROM',
                relationship_type: 'ASSOCIATE',
              },
            ],
            attribute: [
              {
                attribute_type: 'Image URL',
                attribute_value: 'https://some.website.com/photos/00000.jpg',
              },
              {
                attribute_type: 'Sex',
                attribute_value: '',
              },
              {
                attribute_type: 'PEP Type',
                attribute_value: 'MUN:L1',
              },
              {
                attribute_type: 'PEP Type',
                attribute_value: 'POL:L2',
              },
              {
                attribute_type: 'PEP Type',
                attribute_value: 'CAB:L1',
              },
              {
                attribute_type: 'PEP Type',
                attribute_value: 'FAM',
              },
              {
                attribute_type: 'PEP Type',
                attribute_value: 'LEG:L1',
              },
            ],
            date_of_birth: ['1995-02-21', '1995'],
            event: [
              {
                category: 'Person Political',
                event_date: '2018-01-01',
                event_description: 'On List [PEP Connect - UK]',
                source: {
                  source_date: '2018-01-11',
                  source_format: 'PEP_Connect',
                  source_name: 'PEP Connect - UK',
                },
                sub_category: 'Associated with, Seen with',
              },
              {
                category: 'Bribery, Graft, Kickbacks, Political Corruption',
                event_date: '2008-01-01',
                event_description: 'allegations of corruption',
                source: {
                  source_date: '2008-01-01',
                  source_format: 'Media',
                  source_headline: 'Consider features the news',
                  source_name: 'Factiva Article - GRDN000000000000a00a000aa',
                },
                sub_category: 'Allege',
              },
            ],
            full_name: 'Taylor Consider',
            position: [
              'Position 1 - Role, Country (12/31/2016 - )',
              'Position 2 - Role, Country (01/-/2015 - )',
              'Position 3 - Role, Country (01/-/2008 - 01/-/2016)',
            ],
            source: [
              {
                source_headline:
                  'This document provides the position of Taylor Consider',
                source_name:
                  'A newspaper, 01/10/2018, Headline – some description',
                source_url: 'https://a.newspapper.com/news/0000000/article/',
              },
              {
                source_headline:
                  'This document provides the position of Taylor Consider',
                source_name: 'Gov website',
                source_url:
                  'https://a.gov.website/government/people/Taylor-Consider',
              },
              {
                source_headline:
                  'This document provides the date of birth of Taylor Consider',
                source_name: 'A Radio - Headline, Profile - Taylor Consider',
                source_url: 'http://a.radio.website/Taylor-Consider-000000',
              },
            ],
          },
          {
            address: [
              {
                country: 'GBR',
              },
            ],
            alias: null,
            associate: null,
            attribute: [
              {
                attribute_type: 'Entity URL',
                attribute_value:
                  'https://a.website.gov/library/publications/world-leaders/UK.html',
              },
            ],
            date_of_birth: ['1995'],
            event: [
              {
                category: 'Person Political',
                event_date: '2016-01-01',
                event_description: 'On List [World Leaders]',
                source: {
                  source_date: '2017-01-01',
                  source_format: 'Web',
                  source_name: 'World Leaders',
                  source_url:
                    'https://a.website.gov/library/publications/world-leaders/index.html',
                },
                sub_category: 'Government Official',
              },
            ],
            full_name: 'Taylor Consider',
            position: ['Position'],
            source: [
              {
                source_headline: 'World Leaders',
                source_name: 'World Leaders',
                source_url:
                  'https://a.website.gov/library/publications/world-leaders/index.html',
              },
            ],
          },
        ],
      },
      result: 'consider',
      status: 'complete',
      sub_result: null,
    });
    onfidoService.find_report.onSecondCall().resolves({
      breakdown: {
        address: {
          breakdown: {
            address_matched: {
              properties: {},
              result: 'consider',
            },
          },
          result: 'unidentified',
        },
        date_of_birth: {
          breakdown: {
            date_of_birth_matched: {
              properties: {},
              result: 'unidentified',
            },
          },
          result: 'unidentified',
        },
        ssn1: {
          breakdown: {
            full_match: {
              properties: {},
              result: 'consider',
            },
            last_4_digits_match: {
              properties: {},
              result: 'consider',
            },
          },
          result: 'consider',
        },
      },
      check_id: 'e215c3d2-0493-4c5b-890e-a785754f7322',
      created_at: '2021-09-15T22:19:16Z',
      documents: [],
      href: '/v3.1/reports/1dce66f8-4fda-4b3d-ac17-2409d0caf150',
      id: '1dce66f8-4fda-4b3d-ac17-2409d0caf150',
      name: 'identity_enhanced',
      properties: {},
      result: 'unidentified',
      status: 'complete',
      sub_result: null,
    });
    onfidoService.find_check.resolves({
      applicant_id: '<APPLICANT_ID>',
      applicant_provides_data: false,
      created_at: '2021-03-22T17:13:12Z',
      form_uri: null,
      href: '/v3.1/checks/<CHECK_ID>',
      id: '<CHECK_ID>',
      paused: false,
      privacy_notices_read_consent_given: true,
      redirect_uri: null,
      report_ids: ['<REPORT_ID>', '<RESPORT_ID2>'],
      result: 'clear',
      results_uri: '<RESULTS_URI>',
      sandbox: true,
      status: 'complete',
      tags: [],
    });
    const check = await service.getCheck('<CHECK_ID>');
    expect(onfidoService.find_check.calledOnce).to.equal(true);
    expect(onfidoService.find_report.calledTwice).to.equal(true);
    expect(check.providerMeta?.typeId).to.equal(service.typeId);
    expect(check.status).to.equal('COMPLETE');
    expect(check.result).to.equal('CONSIDER');
  });
});
