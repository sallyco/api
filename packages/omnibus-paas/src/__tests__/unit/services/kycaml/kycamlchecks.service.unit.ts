import {
  KycAmlChecksService,
  KycAmlOnfidoService,
  SystemNotificationsService,
} from '../../../../services';
import {KycRepository, ProfileRepository} from '../../../../repositories';
import {StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {createStubInstance} from '@loopback/testlab/dist/sinon';
import {Kyc, Profile} from '../../../../models';
import {expect} from '@loopback/testlab';
import {ISO_Alpha_2} from '../../../../interfaces/datatypes/ISO';
import {
  TestKycRepository,
  TestProfileRepository,
} from '../../../helpers/RepositoryHelper';

let kycAmlService: StubbedInstanceWithSinonAccessor<KycAmlOnfidoService>;
let service: KycAmlChecksService;
let profileRepository: ProfileRepository;
let kycRepository: KycRepository;
let testProfile: Profile;
let testKycAml: Kyc;

beforeEach(async () => {
  kycAmlService = createStubInstance(KycAmlOnfidoService);
  kycRepository = TestKycRepository;
  profileRepository = TestProfileRepository;
  service = new KycAmlChecksService(
    kycAmlService,
    profileRepository,
    kycRepository,
    createStubInstance(SystemNotificationsService),
  );
  await profileRepository.deleteAll();
  await kycRepository.deleteAll();
  testProfile = await profileRepository.save(
    new Profile({
      ownerId: 'de2c9869-eb41-47f3-80d6-9f3b9d034e39',
      tenantId: 'gbt',
      profileType: 'INVESTOR',
      stateOfFormation: 'Utah',
      countryOfFormation: 'United States of America',
      firstName: 'John',
      lastName: 'Doe',
      address: {
        address1: '6510 Millrock Drive',
        city: 'Holladay',
        state: 'Utah',
        postalCode: '84121',
        country: 'United States of America',
      },
      phone: '(555) 555 5555',
      email: 'test@test.com',
      taxDetails: {
        registrationType: 'INDIVIDUAL',
        taxIdentification: {
          type: 'ssn',
          value: '123-12-3123',
        },
      },
      isUSBased: true,
      investorStatus: [0],
      purchaserStatus: [0],
      createdAt: new Date('2021-05-14T22:18:12.083Z'),
      updatedAt: new Date('2021-05-14T22:18:12.083Z'),
      isDeleted: false,
    }),
  );
  testProfile = await profileRepository.findById(testProfile.id, {
    include: ['kycAml'],
  });
  testKycAml = await kycRepository.save(
    new Kyc({
      applicantId: '0241e48f-bf6f-4351-81af-15d3251e114e',
      status: 'IN_PROGRESS',
      profileId: testProfile.id,
      providerMeta: {
        typeId: 'ONFIDO',
        applicant_id: '0241e48f-bf6f-4351-81af-15d3251e114e',
        applicant_provides_data: false,
        created_at: '2021-05-17T19:23:03Z',
        form_uri: null,
        href: '/v3.1/checks/858a7418-b10e-4c88-8797-a4dd662afee8',
        id: '858a7418-b10e-4c88-8797-a4dd662afee8',
        paused: false,
        privacy_notices_read_consent_given: null,
        redirect_uri: null,
        report_ids: ['edca8c91-d7bc-43bd-8d1b-ebebcda0c732'],
        result: 'clear',
        results_uri:
          'https://dashboard.onfido.com/checks/858a7418-b10e-4c88-8797-a4dd662afee8',
        sandbox: true,
        status: 'complete',
        tags: [],
        version: '3.1',
        webhook_ids: null,
        reports: [
          {
            breakdown: {
              adverse_media: {
                result: 'consider',
              },
              monitored_lists: {
                result: 'consider',
              },
              politically_exposed_person: {
                result: 'clear',
              },
              sanction: {
                result: 'clear',
              },
            },
            name: 'watchlist_enhanced',
          },
        ],
      },
      result: 'CLEAR',
      checkId: '858a7418-b10e-4c88-8797-a4dd662afee8',
    }),
  );
});

describe('KycAml Check Updater Service Tests', () => {
  it('Should update a Profile check to COMPLETE', async () => {
    await kycRepository.save(testKycAml);
    kycAmlService.stubs.getCheck.resolves({
      applicantId: '',
      createdAt: new Date().toISOString(),
      providerMeta: {},
      result: 'CLEAR',
      status: 'COMPLETE',
    });
    await service.synchronizeProfiles();
    testProfile = await profileRepository.findById(testProfile.id, {
      include: ['kycAml'],
    });
    expect(testProfile.kycAml?.reason).to.equal(undefined);
    expect(testProfile.kycAml?.status).to.equal('COMPLETE');
  });

  it("Should set status of KYC/AML Check to 'EXCEPTION' if unable to run", async () => {
    kycAmlService.stubs.getCheck.throwsException(() => {
      return new Error("Couldn't get Check");
    });
    await service.synchronizeProfiles();
    testProfile = await profileRepository.findById(testProfile.id, {
      include: ['kycAml'],
    });
    expect(testProfile.kycAml?.status).to.equal('EXCEPTION');
    expect(testProfile.kycAml?.reason).to.equal("Couldn't get Check");
  });

  it('Should not get checks for updating in Exception state', async () => {
    testKycAml.status = 'EXCEPTION';
    await kycRepository.save(testKycAml);
    const updatableProfiles = await service.getProfilesToUpdate();
    expect(updatableProfiles).to.be.empty();
  });

  it('Should not get checks for updating in Completed state', async () => {
    testKycAml.status = 'COMPLETE';
    await kycRepository.save(testKycAml);
    const updatableProfiles = await service.getProfilesToUpdate();
    expect(updatableProfiles).to.be.empty();
  });

  it('Should get checks for updating in In Progress state', async () => {
    testKycAml.status = 'IN_PROGRESS';
    await kycRepository.save(testKycAml);
    const updatableProfiles = await service.getProfilesToUpdate();
    expect(updatableProfiles.length).to.equal(1);
  });

  it('Should get a Check ID if profile has one', async () => {
    kycAmlService.stubs.createApplicant.resolves({
      id: 'test',
      createdAt: new Date().toISOString(),
      providerMeta: {
        id: 'test',
      },
      address: {
        street1: '',
        postalCode: '',
        city: '',
        region: '',
        country: 'US' as unknown as ISO_Alpha_2,
      },
      dob: '',
      email: '',
      firstName: '',
      identification: [],
      lastName: '',
    });
    kycAmlService.stubs.getCheck.resolves({
      applicantId: '',
      createdAt: '',
      lastDateChecked: undefined,
      providerMeta: {},
      result: 'CLEAR',
      status: 'IN_PROGRESS',
    });
    const {profile, checkId} = await service.getCheckIdForProfile(testProfile);
    expect(profile).to.be.not.null();
    expect(checkId).to.equal('858a7418-b10e-4c88-8797-a4dd662afee8');
  });
  it('Should create a Check ID if profile is missing one', async () => {
    await kycRepository.delete(testKycAml);
    kycAmlService.stubs.createCheck.resolves({
      applicantId: '',
      createdAt: new Date().toISOString(),
      providerMeta: {
        id: '858a7418-b10e-4c88-8797-a4dd662afee8',
      },
      status: 'IN_PROGRESS',
    });
    kycAmlService.stubs.createApplicant.resolves({
      id: 'test',
      createdAt: new Date().toISOString(),
      providerMeta: {
        id: 'test',
      },
      address: {
        street1: '',
        postalCode: '',
        city: '',
        region: '',
        country: 'US' as unknown as ISO_Alpha_2,
      },
      dob: '',
      email: '',
      firstName: '',
      identification: [],
      lastName: '',
    });
    const {profile, checkId} = await service.getCheckIdForProfile(testProfile);

    expect(profile).to.be.not.null();
    expect(checkId).to.equal('858a7418-b10e-4c88-8797-a4dd662afee8');
  });

  it('Should return breakdown object with Suspected data', async () => {
    const breakdown = await service.getBreakdownFromCheckResults(testKycAml);
    expect(breakdown).to.be.not.null();
    expect(breakdown.monitored_lists['result']).to.equal('consider');
  });
});
