import {KycAmlOnfidoService, KycAmlProvider, KycAml} from 'services';
import {createStubInstance} from '@loopback/testlab/dist/sinon';
import {expect} from '@loopback/testlab';
import {Profile} from 'models';
import {Individual} from '../../../../models/individual.model';

let service: KycAmlProvider;
let individualProfile: Profile;
let trustProfile: Profile;
let entityProfile: Profile;

beforeEach(async () => {
  service = new KycAmlProvider(createStubInstance(KycAmlOnfidoService));
  individualProfile = new Profile({
    profileType: 'ORGANIZER',
    taxDetails: {
      registrationType: 'INDIVIDUAL',
      taxIdentification: {
        type: 'ssn',
        value: '123-12-4567',
      },
    },
    stateOfFormation: 'Texas',
    countryOfFormation: 'United States of America',
    name: 'Legal Name One',
    typeOfEntity: 'C_CORPORATION',
    firstName: 'Mick',
    lastName: 'Williams',
    address: {
      address1: '234 Redwing',
      address2: 'Apt 45',
      city: 'Spokane',
      state: 'Washington',
      country: 'United States of America',
      postalCode: '98898',
    },
    phone: '3602346654',
    email: 'mwilliams@test.com',
    passportId: '',
    isUSBased: true,
    dateOfBirth: '02-21-1995',
  });

  trustProfile = new Profile({
    ownerId: 'de2c9869-eb41-47f3-80d6-9f3b9d034e39',
    tenantId: 'gbt',
    profileType: 'INVESTOR',
    stateOfFormation: 'Connecticut',
    countryOfFormation: 'United States of America',
    name: 'Test Testerton2',
    address: {
      address1: '6510 Millrock Drive',
      city: 'Holladay',
      state: 'Connecticut',
      postalCode: '84121',
      country: 'United States of America',
    },
    phone: '(877) 492 7555',
    email: 'test@test.com',
    taxDetails: {
      registrationType: 'TRUST',
      taxIdentification: {
        type: 'ein',
        value: '12-3123123',
      },
    },
    isUSBased: true,
    primarySignatory: new Individual({
      title: 'CEO',
      name: 'Test TesterOne',
      isUSBased: true,
      taxDetails: {
        type: 'ssn',
        value: '123-12-3123',
      },
      address: {
        address2: '',
        state: 'Arkansas',
        postalCode: '84121',
        country: 'United States of America',
        address1: '6510 Millrock Drive',
        city: 'Holladay',
      },
    }),
    investorStatus: [9],
    purchaserStatus: [0],
    isDeleted: false,
  });

  entityProfile = new Profile({
    tenantId: 'gbt',
    profileType: 'ORGANIZER',
    stateOfFormation: 'Utah',
    countryOfFormation: 'United States of America',
    name: 'A New Company',
    firstName: 'John',
    lastName: 'Doe',
    address: {
      address1: '6510 Millrock Drive',
      city: 'Holladay',
      state: 'Utah',
      postalCode: '84121',
      country: 'United States of America',
    },
    phone: '(877) 492 7555',
    email: 'test@test.com',
    taxDetails: {
      registrationType: 'ENTITY',
      taxIdentification: {
        type: 'ein',
        value: '123-12-3123',
      },
    },
    isUSBased: true,
    primarySignatory: new Individual({
      name: 'Test Testerton',
      taxDetails: {
        type: 'ssn',
        value: '123-12-1234',
      },
      address: {
        address1: '6510 Millrock Drive',
        city: 'Holladay',
        state: 'Utah',
        postalCode: '84121',
        country: 'United States of America',
      },
    }),
  });
});

describe('KYC/AML Provider Tests', () => {
  describe('Converting Profile to KYC/AML Application tests', () => {
    describe('Data Conversion Tests', () => {
      it('Should return an instance of KYC/AML provider based on the top-level class', async () => {
        const KYC_AMLInstance = service.value();
        expect(KYC_AMLInstance).to.be.instanceOf(KycAml);
      });

      it('Should resolve a name from Individual Profile', async () => {
        const name_data = await KycAmlProvider.resolveName(individualProfile);
        expect(name_data).to.deepEqual({
          firstName: 'Mick',
          lastName: 'Williams',
        });
      });

      it('Should resolve a name from an Entity Profile', async () => {
        const name_data = await KycAmlProvider.resolveName(entityProfile);
        expect(name_data).to.deepEqual({
          firstName: 'Test',
          lastName: 'Testerton',
        });
      });

      it('Should resolve a name from an Entity Profile with multiple spaces between name of Primary Signatory', async () => {
        entityProfile.primarySignatory!.name = 'Test    Testerton';
        const name_data = await KycAmlProvider.resolveName(entityProfile);
        expect(name_data).to.deepEqual({
          firstName: 'Test',
          lastName: 'Testerton',
        });
      });

      it('Should resolve a name from a Trust Profile', async () => {
        const name_data = await KycAmlProvider.resolveName(entityProfile);
        expect(name_data).to.deepEqual({
          firstName: 'Test',
          lastName: 'Testerton',
        });
      });

      it('Should resolve a poorly-formatted name from an Entity Profile', async () => {
        const name_data = await KycAmlProvider.resolveName(
          new Profile({
            ...entityProfile,
            primarySignatory: new Individual({
              ...entityProfile.primarySignatory,
              name: 'TestTesterton',
            }),
          }),
        );
        expect(name_data).to.deepEqual({
          firstName: 'Test',
          lastName: 'Testerton',
        });
      });

      it('Should return whether a profile uses a signatory or not', async () => {
        const individual_profile = await KycAmlProvider.usesSignatory(
          individualProfile,
        );
        expect(individual_profile).to.be.false();
        const entity_profile = await KycAmlProvider.usesSignatory(
          entityProfile,
        );
        expect(entity_profile).to.be.true();
        const trust_profile = await KycAmlProvider.usesSignatory(entityProfile);
        expect(trust_profile).to.be.true();
      });

      it('Should resolve taxIds from all profile types', async () => {
        const individual_tax_ids = await KycAmlProvider.resolveTaxIds(
          individualProfile,
        );
        expect(individual_tax_ids).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-4567',
          },
        ]);
        const entity_tax_ids = await KycAmlProvider.resolveTaxIds(
          entityProfile,
        );
        expect(entity_tax_ids).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-1234',
          },
        ]);
        const itin_entity_tax_ids = await KycAmlProvider.resolveTaxIds(
          new Profile({
            ...entityProfile,
            primarySignatory: new Individual({
              ...entityProfile.primarySignatory,
              taxDetails: {
                ...entityProfile.primarySignatory?.taxDetails,
                type: 'itin',
              },
            }),
          }),
        );
        expect(itin_entity_tax_ids).to.deepEqual([
          {
            type: 'itin',
            value: '123-12-1234',
          },
        ]);
        const trust_tax_ids = await KycAmlProvider.resolveTaxIds(entityProfile);
        expect(trust_tax_ids).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-1234',
          },
        ]);
      });

      it('Should convert an Individual Profile to a KYC/AML Application', async () => {
        const application = await KycAmlProvider.createApplicationFromProfile(
          individualProfile,
        );
        expect(application.firstName).to.equal(individualProfile.firstName);
        expect(application.lastName).to.equal(individualProfile.lastName);
        expect(application.email).to.equal(individualProfile.email);
        expect(application.address).to.deepEqual({
          street1: '234 Redwing',
          street2: 'Apt 45',
          city: 'Spokane',
          postalCode: '98898',
          region: 'Washington',
          country: 'US',
        });
        expect(application.dob).to.equal(
          new Date(String(individualProfile.dateOfBirth)).toUTCString(),
        );
        expect(application.idNumbers).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-4567',
          },
        ]);
      });

      it('Should convert an Individual Profile without a DOB to a KYC/AML Application', async () => {
        delete individualProfile.dateOfBirth;
        const application = await KycAmlProvider.createApplicationFromProfile(
          individualProfile,
        );
        expect(application.firstName).to.equal(individualProfile.firstName);
        expect(application.lastName).to.equal(individualProfile.lastName);
        expect(application.email).to.equal(individualProfile.email);
        expect(application.address).to.deepEqual({
          street1: individualProfile.address?.address1,
          street2: individualProfile.address?.address2,
          city: individualProfile.address?.city,
          postalCode: individualProfile.address?.postalCode,
          region: individualProfile.address?.state,
          country: 'US',
        });
        expect(application.dob).to.be.undefined();
        expect(application.idNumbers).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-4567',
          },
        ]);
      });

      it('Should convert a Trust Profile to a KYC/AML Application using the Primary Signatory', async () => {
        const application = await KycAmlProvider.createApplicationFromProfile(
          trustProfile,
        );
        expect(application.firstName).to.equal(
          trustProfile.primarySignatory?.name?.split(' ')[0],
        );
        expect(application.lastName).to.equal(
          trustProfile.primarySignatory?.name?.split(' ')[1],
        );
        expect(application.email).to.equal(trustProfile.email);
        expect(application.address).to.deepEqual({
          street1: trustProfile.primarySignatory?.address?.address1,
          street2: undefined,
          city: trustProfile.primarySignatory?.address?.city,
          postalCode: trustProfile.primarySignatory?.address?.postalCode,
          region: trustProfile.primarySignatory?.address?.state,
          country: 'US',
        });
        expect(application.idNumbers).to.deepEqual([
          {
            type: 'ssn',
            value: '123-12-3123',
          },
        ]);
      });

      it('Should convert an Entity Profile to a KYC/AML Application using regular fields', async () => {
        const application = await KycAmlProvider.createApplicationFromProfile(
          entityProfile,
        );
        expect(application.firstName).to.equal('Test');
        expect(application.lastName).to.equal('Testerton');
        expect(application.email).to.equal(entityProfile.email);
        expect(application.idNumbers).to.deepEqual([
          {
            type: 'ssn',
            value: entityProfile.primarySignatory?.taxDetails?.value,
          },
        ]);
        expect(application.address).to.deepEqual({
          street1: entityProfile.address?.address1,
          street2: entityProfile.address?.address2,
          city: entityProfile.address?.city,
          postalCode: entityProfile.address?.postalCode,
          region: entityProfile?.address?.state,
          country: 'US',
        });
      });

      describe('Data Exception Tests', () => {
        it('Should throw an error if First Name is invalid', async () => {
          delete individualProfile.firstName;
          const application_creation =
            KycAmlProvider.createApplicationFromProfile(individualProfile);
          await expect(application_creation).to.be.rejectedWith(
            'First Name is Invalid',
          );
        });

        it('Should throw an error if Last Name is invalid', async () => {
          delete individualProfile.lastName;
          const application_creation =
            KycAmlProvider.createApplicationFromProfile(individualProfile);
          await expect(application_creation).to.be.rejectedWith(
            'Last Name is Invalid',
          );
        });

        it('Should throw an error if Address is unable to be converted to ISO_2', async () => {
          if (individualProfile.address?.country) {
            individualProfile.address.country = 'Not a Country';
          }
          const application_creation =
            KycAmlProvider.createApplicationFromProfile(individualProfile);
          await expect(application_creation).to.be.rejectedWith(
            'Could not resolve country name to ISO_2',
          );
        });
      });
    });
  });
});
