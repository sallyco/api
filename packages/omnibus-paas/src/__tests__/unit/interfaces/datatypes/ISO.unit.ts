import {expect} from '@loopback/testlab';
import {ISOHelper} from '../../../../interfaces/datatypes/ISO';

describe('ISO conversion tests', () => {
  it('Should convert country name into ISO_2', () => {
    expect(
      ISOHelper.convertCountryNameToISO_2('United States of America'),
    ).to.equal('US');
    expect(
      ISOHelper.convertCountryNameToISO_2('united states of america'),
    ).to.equal('US');
  });
  it('Should throw an error if not able to convert country name to ISO_2', () => {
    expect(() => ISOHelper.convertCountryNameToISO_2('United')).to.throwError(
      'Could not resolve country name to ISO_2',
    );
  });
  it('Should convert country name into ISO_3', () => {
    expect(
      ISOHelper.convertCountryNameToISO_3('United States of America'),
    ).to.equal('USA');
    expect(
      ISOHelper.convertCountryNameToISO_3('united states of america'),
    ).to.equal('USA');
  });
  it('Should throw an error if not able to convert country name to ISO_3', () => {
    expect(() => ISOHelper.convertCountryNameToISO_3('United')).to.throwError(
      'Could not resolve country name to ISO_3',
    );
  });
});
