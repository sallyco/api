import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {SubscriptionsController} from '../../../controllers/subscriptions.controller';
import {Deal, mockFeeStatement, Subscription, Tenant} from '../../../models';

import {
  DealRepository,
  CloseRepository,
  TenantRepository,
  EntitiesRepository,
  FileRepository,
  SubscriptionRepository,
  TransactionRepository,
  ProfileRepository,
} from '../../../repositories';
import {
  BankingTreasuryPrimeService,
  CeleryClientService,
  DocumentsService,
  FeesService,
  TenantService,
} from '../../../services';
import {DocumentsGeneratorService} from '../../../services/documents/documents-generator.service';
import {securityId} from '@loopback/security';

// Some helpful mock data
const mockSub = new Subscription({
  id: 'id',
  dealId: 'dealId',
  status: 'COMPLETED',
  ownerId: 'ownerId',
  name: 'name',
  email: 'email',
  type: 'INDIVIDUAL',
  transactionId: 'transid',
  isDocsSigned: false,
  isKycAmlPassed: false,
  deletedAt: new Date(),
  isDeleted: false,
  transactions: [],
  profileId: 'profileId',
});

const mockTenant = new Tenant({
  dealLimits: {
    perDealMaxInvestorCount: 30,
  },
  id: 'tenantId',
  name: 'name',
  url: 'url',
  inverted: true,
});

const mockDeal = new Deal({
  id: 'id',
  profileId: 'profileId',
  name: 'name',
  description: 'description',
  portfolioCompanyName: 'companname',
  portfolioCompanyState: 'UT',
  portfolioCompanyEntity: 'LIMITED_LIABILITY_COMPANY',
  targetRaiseAmount: 100000,
  minInvestmentAmount: 1000,
  isPublic: true,
  status: 'OPEN',
  requireQualifiedPurchaser: false,
  deletedAt: new Date(),
  isDeleted: false,
});

describe('SubscriptionsController (unit)', () => {
  // Repositories used by controller
  let dealRepo: StubbedInstanceWithSinonAccessor<DealRepository>;
  let entityRepo: StubbedInstanceWithSinonAccessor<EntitiesRepository>;
  let fileRepo: StubbedInstanceWithSinonAccessor<FileRepository>;
  let subRepo: StubbedInstanceWithSinonAccessor<SubscriptionRepository>;
  let closeRepo: StubbedInstanceWithSinonAccessor<CloseRepository>;
  let tenantRepo: StubbedInstanceWithSinonAccessor<TenantRepository>;
  let transRepo: StubbedInstanceWithSinonAccessor<TransactionRepository>;
  let profileRepo: StubbedInstanceWithSinonAccessor<ProfileRepository>;
  let controller: SubscriptionsController;

  // Services used by controlelr
  let tenantService: StubbedInstanceWithSinonAccessor<TenantService>;
  let bankingService: StubbedInstanceWithSinonAccessor<BankingTreasuryPrimeService>;
  let celeryClientService: StubbedInstanceWithSinonAccessor<CeleryClientService>;
  let documentsService: StubbedInstanceWithSinonAccessor<DocumentsService>;
  let feeService: StubbedInstanceWithSinonAccessor<FeesService>;
  let documentsGeneratorService: StubbedInstanceWithSinonAccessor<DocumentsGeneratorService>;

  function givenStubbedRepository() {
    dealRepo = createStubInstance(DealRepository);
    entityRepo = createStubInstance(EntitiesRepository);
    fileRepo = createStubInstance(FileRepository);
    subRepo = createStubInstance(SubscriptionRepository);
    transRepo = createStubInstance(TransactionRepository);
    profileRepo = createStubInstance(ProfileRepository);
  }

  function givenMockedServices() {
    tenantService = createStubInstance(TenantService);
    bankingService = createStubInstance(BankingTreasuryPrimeService);
    celeryClientService = createStubInstance(CeleryClientService);
    documentsService = createStubInstance(DocumentsService);
    feeService = createStubInstance(FeesService);
  }

  function buildController() {
    controller = new SubscriptionsController(
      subRepo,
      fileRepo,
      dealRepo,
      entityRepo,
      profileRepo,
      transRepo,
      tenantRepo,
      closeRepo,
      documentsService,
      bankingService,
      celeryClientService,
      tenantService,
      feeService,
      documentsGeneratorService,
    );
  }

  beforeEach(() => {
    givenStubbedRepository();
    givenMockedServices();
    buildController();

    feeService.stubs.getFeesData.resolves(mockFeeStatement);
  });

  // Controller test setup data

  // Start the tests
  it('is part of the deal when COMPLETED status', async () => {
    const testSub = new Subscription({
      ...mockSub,
      status: 'COMPLETED',
    });
    // set up expected returns
    dealRepo.stubs.findById.resolves(mockDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub, testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(mockTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    expect(result.meta.includedInDeal).to.equal(true);
  });

  it('is NOT part of the deal when INVITED status', async () => {
    const testSub = new Subscription({
      ...mockSub,
      status: 'INVITED',
    });
    // set up expected returns
    dealRepo.stubs.findById.resolves(mockDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub, testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(mockTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    expect(result.meta.includedInDeal).to.equal(false);
  });

  it('has reached the investor limit', async () => {
    const testSub = new Subscription({
      ...mockSub,
    });

    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: {
        perDealMaxInvestorCount: 1,
      },
    });
    // set up expected returns
    dealRepo.stubs.findById.resolves(mockDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 1 subscriptions are in the deal, max is 1
    expect(result.meta.investorLimitReached).to.equal(true);
  });

  it('has no limit if there is no config', async () => {
    const testSub = new Subscription({
      ...mockSub,
    });

    // Remove the dealLimits config
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: undefined,
    });

    // set up expected returns
    dealRepo.stubs.findById.resolves(mockDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    //200 subscriptions are in the deal, max limitless
    expect(result.meta.investorLimitReached).to.equal(false);
  });

  it('returns remaining commitment amount (based on funded subs) ', async () => {
    const testSub = new Subscription({
      ...mockSub,
      amount: 1000,
    });

    // Set a max of 2000
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: {
        perDealMaxTargetRaiseAmount: 3000,
      },
    });

    // set up expected returns
    dealRepo.stubs.findById.resolves(mockDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub, testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 2 sub of 1000 each (2000), and max of 3000.  1000 is left
    expect(result.meta.maxCommitmentAmount).to.equal(1000);
  });

  it('uses targetRaiseAmount of deal if tenant is not configured', async () => {
    const testSub = new Subscription({
      ...mockSub,
      amount: 1000,
    });

    // Remove the dealLimits config
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: undefined,
    });

    const testDeal = new Deal({
      ...mockDeal,
      targetRaiseAmount: 2000,
    });

    // set up expected returns
    dealRepo.stubs.findById.resolves(testDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 1000 raised of a 2000 target, so 1000 is left
    expect(result.meta.maxCommitmentAmount).to.equal(1000);
  });

  it('uses deal config when tenant perDealMaxTargetRaiseAmount is higher than deal targetRaiseAmount', async () => {
    const testSub = new Subscription({
      ...mockSub,
      amount: 1000,
    });

    // Remove the dealLimits config
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: {
        perDealMaxTargetRaiseAmount: 3000,
      },
    });

    const testDeal = new Deal({
      ...mockDeal,
      targetRaiseAmount: 2000,
    });

    // set up expected returns
    dealRepo.stubs.findById.resolves(testDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 1000 raised of a 2000 target, so 1000 is left
    expect(result.meta.maxCommitmentAmount).to.equal(1000);
  });

  it('EDGE CASE, uses tenant config if deal happens to be set higher than tenant', async () => {
    const testSub = new Subscription({
      ...mockSub,
      amount: 500,
    });

    // Remove the dealLimits config
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: {
        perDealMaxTargetRaiseAmount: 1000,
      },
    });

    const testDeal = new Deal({
      ...mockDeal,
      targetRaiseAmount: 2000,
    });

    // set up expected returns
    dealRepo.stubs.findById.resolves(testDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves([testSub]);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 500 raised of a 1000 on tenant target, so 500 is left (even though deal wants more)
    expect(result.meta.maxCommitmentAmount).to.equal(500);
  });

  it('returns QP OVERRIDE for investor count if deal is QP', async () => {
    const testSub = new Subscription({
      ...mockSub,
      amount: 1000,
    });

    // Remove the dealLimits config
    const testTenant = new Tenant({
      ...mockTenant,
      dealLimits: {
        maxInvestorCount: 99,
      },
    });

    const testDeal = new Deal({
      ...mockDeal,
      requireQualifiedPurchaser: true,
    });

    const testSubs: Subscription[] = [];
    for (let i = 0; i < 110; i++) {
      testSubs.push(testSub);
    }

    // set up expected returns
    dealRepo.stubs.findById.resolves(testDeal);
    subRepo.stubs.findById.resolves(testSub);
    subRepo.stubs.find.resolves(testSubs);
    tenantService.stubs.getCurrentHydratedTenant.resolves(testTenant);

    const result = await controller.getSubscriptionDealMeta(testSub.id, {
      email: 'email',
      name: 'name',
      [securityId]: 'string',
    });

    // 99 is configured but QP overrides it
    expect(result.meta.investorLimitReached).to.equal(false);
  });
});
