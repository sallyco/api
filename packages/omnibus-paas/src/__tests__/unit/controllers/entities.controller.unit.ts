import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {EntitiesController} from '../../../controllers/entities.controller';
import {
  BankingBeneficiary,
  Entities,
  Tenant,
  Transaction,
} from '../../../models';
import {Tenant as TNT} from '../../../multi-tenancy';

import {
  CompanyRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  GlobalRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
  TransactionRepository,
  BankingBeneficiaryRepository,
} from '../../../repositories';
import {
  BlueSkyFeeService,
  SystemNotificationsService,
  TransactionService,
} from '../../../services';
import {UserProfile} from '@loopback/security';
import {TestRepository} from '../../helpers/globalHelpers';

describe('Entity Controller (unit)', () => {
  // Repositories used by controller
  let dealRepo: StubbedInstanceWithSinonAccessor<DealRepository>;
  let fileRepo: StubbedInstanceWithSinonAccessor<FileRepository>;
  let entityRepo: StubbedInstanceWithSinonAccessor<EntitiesRepository>;
  let transRepo: StubbedInstanceWithSinonAccessor<TransactionRepository>;
  let companyRepo: StubbedInstanceWithSinonAccessor<CompanyRepository>;
  let closeRepo: StubbedInstanceWithSinonAccessor<CloseRepository>;
  let profileRepo: StubbedInstanceWithSinonAccessor<ProfileRepository>;
  let subscriptionRepo: StubbedInstanceWithSinonAccessor<SubscriptionRepository>;
  let globalRepo: StubbedInstanceWithSinonAccessor<GlobalRepository>;
  let tenantRepo: StubbedInstanceWithSinonAccessor<TenantRepository>;
  let beneficiaryRepo: BankingBeneficiaryRepository;
  let controller: EntitiesController;

  // Services used by controlelr
  let transactionService: StubbedInstanceWithSinonAccessor<TransactionService>;
  let blueSkyService: StubbedInstanceWithSinonAccessor<BlueSkyFeeService>;
  let systemNotificationService: StubbedInstanceWithSinonAccessor<SystemNotificationsService>;

  function givenStubbedRepository() {
    dealRepo = createStubInstance(DealRepository);
    entityRepo = createStubInstance(EntitiesRepository);
    fileRepo = createStubInstance(FileRepository);
    transRepo = TestRepository(TransactionRepository);
    companyRepo = createStubInstance(CompanyRepository);
    profileRepo = createStubInstance(ProfileRepository);
    subscriptionRepo = createStubInstance(SubscriptionRepository);
    globalRepo = createStubInstance(GlobalRepository);
    tenantRepo = createStubInstance(TenantRepository);
    beneficiaryRepo = TestRepository(BankingBeneficiaryRepository);
    systemNotificationService = createStubInstance(SystemNotificationsService);
  }

  function givenMockedServices() {
    transactionService = createStubInstance(TransactionService);
    blueSkyService = createStubInstance(BlueSkyFeeService);
  }

  function buildController() {
    controller = new EntitiesController(
      entityRepo,
      companyRepo,
      closeRepo,
      dealRepo,
      transRepo,
      profileRepo,
      subscriptionRepo,
      fileRepo,
      globalRepo,
      transactionService,
      tenantRepo,
      blueSkyService,
      beneficiaryRepo,
      systemNotificationService,
    );
  }

  beforeEach(() => {
    givenStubbedRepository();
    givenMockedServices();
    buildController();
  });

  // Start the tests
  it('call the init ACH Transfer Success', async () => {
    const mockTenant: Tenant = new Tenant({
      dealLimits: {
        perDealMaxInvestorCount: 30,
      },
      id: 'tenantId',
      name: 'name',
      url: 'url',
      inverted: true,
    });
    const tenant: TNT = Object.assign({
      name: 'Test',
    });

    const userProfile: UserProfile = Object.assign({
      name: 'dev',
      id: '12345',
      roles: {
        account: {
          roles: ['manage-account', 'manage-account-links', 'view-profile'],
        },
      },
      tenantId: mockTenant.id,
    });
    const mockEntities = new Entities({
      id: 'test',
      name: 'Entity I',
      entityType: 'LIMITED_PARTNERSHIP',
      countryOfFormation: 'United States of America',
      stateOfFormation: 'Delaware',
      minInvestmentAmount: 1000,
      ownerId: userProfile.id,
      bankAccount: {
        accountNumber: '9999',
        providerMeta: {
          accountStatus: 'open',
          accountId: 'test',
        },
      },
    });
    const mockTransaction = new Transaction({
      id: 'asdf',
      amount: 100,
    });

    const beneficiary = await beneficiaryRepo.save(
      new BankingBeneficiary({
        ownerId: '8d1c1e35-dc6a-4ecb-8d43-a14902ed550c',
        tenantId: 'gbt',
        name: 'Dave Test',
        phone: '+1 650 924 5555',
        email: 'test@gmail.com',
        serviceObjectId: 'cp_qweqwe',
        providerMeta: {
          ach: {
            account_number: '00000000',
            account_type: 'savings',
            routing_number: '1234567',
          },
          wire: null,
          name_on_account: 'Dave Wilkes',
          id: 'cp_test',
          userdata: null,
          typeId: 'TREASURYPRIME',
        },
        isDeleted: false,
      }),
    );

    // set up expected returns
    entityRepo.stubs.findById.resolves(mockEntities);
    transactionService.stubs.createACHTransactionForEntity.resolves(
      mockTransaction,
    );
    systemNotificationService.stubs.Notify_Transaction_Approval_Needed.resolves();
    const resultPromise = controller.postACHTransfer(
      tenant,
      userProfile,
      mockEntities.id,
      {
        amount: 100,
        beneficiaryId: String(beneficiary?.id),
      },
    );

    const result = await expect(resultPromise).to.be.fulfilled();
    expect(result).to.equal(true);

    expect(
      transactionService.stubs.createACHTransactionForEntity.calledOnce,
    ).to.be.true();
    expect(
      transactionService.stubs.createACHTransactionForEntity.getCall(0).args,
    ).to.deepEqual([
      mockEntities.id,
      'CREDIT',
      100,
      beneficiary?.providerMeta?.id,
      undefined,
      true,
    ]);
  });
});
