import {KycController} from '../../../../controllers/identity/kyc.controller';
import {KycRepository, ProfileRepository} from '../../../../repositories';
import {StubbedInstance, stubInterface} from 'ts-sinon';
import {
  AvailableReportIds,
  KycAmlOnfidoService,
  Onfido,
} from '../../../../services';
import {expect} from '@loopback/testlab';
import {KycCheckRequest, Profile} from '../../../../models';
import {
  TestKycRepository,
  TestProfileRepository,
  TestRepositoryCleanup,
} from '../../../helpers/RepositoryHelper';

describe('KYC Controller (unit)', () => {
  let controller: KycController;
  let profileRepository: ProfileRepository;
  let kycRepository: KycRepository;
  let onfido: StubbedInstance<Onfido>;
  let kycAmlService: KycAmlOnfidoService;

  const buildController = () => {
    onfido = stubInterface<Onfido>();
    kycAmlService = new KycAmlOnfidoService(onfido);
    profileRepository = TestProfileRepository;
    kycRepository = TestKycRepository;
    controller = new KycController(
      kycRepository,
      profileRepository,
      kycAmlService,
    );
  };

  beforeEach(async () => {
    await TestRepositoryCleanup();
    buildController();
    onfido.find_report.reset();
    onfido.create_check.reset();
    onfido.create_applicant.reset();
    onfido.find_applicant.reset();
    onfido.create_applicant.resolves({
      address: undefined,
      created_at: new Date().toISOString(),
      delete_at: new Date().toISOString(),
      dob: '',
      email: '',
      first_name: 'Test',
      href: '',
      id: '1test',
      id_numbers: [],
      last_name: 'Testerton',
    });
    onfido.find_applicant.resolves({
      address: undefined,
      created_at: new Date().toISOString(),
      delete_at: new Date().toISOString(),
      dob: '',
      email: '',
      first_name: 'Test',
      href: '',
      id: '1test',
      id_numbers: [],
      last_name: 'Testerton',
    });
    onfido.find_report.resolves({
      breakdown: {},
      check_id: '',
      created_at: '',
      documents: [],
      href: '',
      name: '',
      properties: null,
      status: '',
      sub_result: 'clear',
      id: '1reporttest',
      result: 'clear',
    });
    onfido.create_check.resolves({
      applicant_id: '1test',
      applicant_provides_data: false,
      href: '',
      id: '',
      paused: false,
      privacy_notices_read_consent_given: false,
      report_ids: ['1reporttest'],
      reports: [
        {
          id: AvailableReportIds.watchlist_enhanced,
          check_id: 'test',
          href: '',
          name: AvailableReportIds.watchlist_enhanced,
          properties: {},
          breakdown: {},
          status: 'CLEAR',
          result: 'CLEAR',
          documents: [],
          created_at: new Date().toISOString(),
          sub_result: 'CLEAR',
        },
      ],
      results_uri: '',
      sandbox: false,
      result: null,
      tags: [],
      form_uri: null,
      redirect_uri: null,
      created_at: new Date().toISOString(),
      status: 'IN_PROGRESS',
    });
  });

  it('Should convert a valid profileId to a KYC/AML Model', async () => {
    const profile = await profileRepository.create(
      new Profile({
        firstName: 'Test',
        lastName: 'Testerton',
        taxDetails: {
          taxIdentification: {
            type: 'ssn',
            value: '123-45-1234',
          },
        },
        email: 'test@test.com',
        address: {
          address1: '123 E Street',
          address2: 'Unit 1',
          city: 'Eagle Mountain',
          state: 'UT',
          country: 'United States of America',
          postalCode: '84005',
        },
        dateOfBirth: '05/21/1990',
      }),
    );

    const kyc = await controller.create(
      new KycCheckRequest({
        profileId: profile.id,
      }),
    );
    expect(kyc).to.be.type('object', 'KYC Creation should succeed');
    expect(await kycRepository.find()).to.be.length(1);
  });

  it('Should convert a valid raw request to a KYC/AML Model', async () => {
    const kyc = await controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        dateOfBirth: '1995-02-21',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        identificationNumbers: [
          {
            type: 'ssn',
            value: '222-22-222',
          },
        ],
      }),
    );
    expect(kyc).to.be.type('object', 'KYC Creation should succeed');
  });

  it('Should throw an error if address is missing', async () => {
    const kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        dateOfBirth: '1995-02-21',
      }),
    );
    await expect(kyc).to.be.rejected();
  });
  it('Should throw an error if firstName is missing', async () => {
    const kyc = controller.create(
      new KycCheckRequest({
        lastName: 'Testerton',
        email: 'test@test.com',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        dateOfBirth: '1995-02-21',
        identificationNumbers: [
          {
            type: 'ssn',
            value: '222-22-222',
          },
        ],
      }),
    );
    await expect(kyc).to.be.rejected();
  });
  it('Should throw an error if lastName is missing', async () => {
    const kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        email: 'test@test.com',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        dateOfBirth: '1995-02-21',
        identificationNumbers: [
          {
            type: 'ssn',
            value: '222-22-222',
          },
        ],
      }),
    );
    await expect(kyc).to.be.rejected();
  });
  it('Should throw an error if identificationNumbers is missing or empty', async () => {
    let kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        dateOfBirth: '1995-02-21',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        identificationNumbers: [],
      }),
    );
    await expect(kyc).to.be.rejected();
    kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        dateOfBirth: '1995-02-21',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
      }),
    );
    await expect(kyc).to.be.rejected();
    kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        dateOfBirth: '1995-02-21',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        identificationNumbers: [
          {
            type: 'ssn',
            value: '',
          },
        ],
      }),
    );
    await expect(kyc).to.be.rejected();
  });

  it('Should throw an error if dateOfBirth is missing or empty', async () => {
    const kyc = controller.create(
      new KycCheckRequest({
        firstName: 'Test',
        lastName: 'Testerton',
        email: 'test@test.com',
        address: {
          street1: '123 E St',
          street2: 'Unit 6',
          city: 'Provo',
          region: 'Utah',
          country: 'United States of America',
          postalCode: '84005',
        },
        identificationNumbers: [
          {
            type: 'ssn',
            value: '212-22-1123',
          },
        ],
      }),
    );
    await expect(kyc).to.be.rejected();
  });
});
