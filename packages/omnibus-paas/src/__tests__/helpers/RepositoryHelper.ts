import {
  CloseRepository,
  CompanyRepository,
  DealRepository,
  DistributionRepository,
  EntitiesRepository,
  FileRepository,
  KycRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
  TransactionRepository,
  UserRepository,
} from '../../repositories';
import {TestRepository} from './globalHelpers';

export const TestCloseRepository: CloseRepository =
  TestRepository(CloseRepository);
export const TestTenantRepository: TenantRepository =
  TestRepository(TenantRepository);
export const TestEntityRepository: EntitiesRepository =
  TestRepository(EntitiesRepository);
export const TestCompanyRepository: CompanyRepository =
  TestRepository(CompanyRepository);
export const TestUserRepository: UserRepository =
  TestRepository(UserRepository);
export const TestKycRepository: KycRepository = TestRepository(KycRepository);
export const TestDealRepository: DealRepository = TestRepository(
  DealRepository,
  async () => TestEntityRepository,
  async () => TestProfileRepository,
  async () => TestDistributionRepository,
);
export const TestProfileRepository: ProfileRepository = TestRepository(
  ProfileRepository,
  async () => TestCompanyRepository,
  async () => TestUserRepository,
  async () => TestKycRepository,
);
export const TestSubscriptionRepository: SubscriptionRepository =
  TestRepository(
    SubscriptionRepository,
    async () => TestProfileRepository,
    async () => TestTransactionRepository,
    async () => TestDealRepository,
    async () => TestUserRepository,
  );
export const TestTransactionRepository: TransactionRepository = TestRepository(
  TransactionRepository,
);
export const TestFileRepository: FileRepository =
  TestRepository(FileRepository);
export const TestDistributionRepository: DistributionRepository =
  TestRepository(DistributionRepository);

export const TestRepositoryCleanup = async () => {
  const Repos = [
    TestProfileRepository,
    TestKycRepository,
    TestTransactionRepository,
    TestDealRepository,
    TestEntityRepository,
    TestFileRepository,
    TestSubscriptionRepository,
    TestCompanyRepository,
    TestCloseRepository,
  ];
  for (const repo of Repos) {
    await repo.deleteAll();
  }
};
