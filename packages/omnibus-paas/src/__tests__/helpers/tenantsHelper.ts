import {Tenant} from '../../multi-tenancy';

export const givenTestTenant = () => {
  return {
    id: 'test',
    name: 'Test',
  } as Tenant;
};
