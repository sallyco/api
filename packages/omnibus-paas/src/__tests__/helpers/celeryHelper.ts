import {createStubInstance} from '@loopback/testlab';
import {CeleryClientService, CeleryWorkerService} from '../../services';

const givenStubCeleryService = () => {
  const celeryClientService = createStubInstance(CeleryClientService);
  const celeryWorkerService = createStubInstance(CeleryWorkerService);
  return {celeryClientService, celeryWorkerService};
};

export {givenStubCeleryService};
