import {juggler} from '@loopback/repository';
import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {
  DealRepository,
  EntitiesRepository,
  TransactionRepository,
  SubscriptionRepository,
  DistributionRepository,
  UserRepository,
} from '../../repositories';
import {Getter} from '@loopback/context/dist/inject';
import {TestRepository} from './globalHelpers';
import {TestProfileRepository} from './RepositoryHelper';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  const subscriptionRepo: SubscriptionRepository = new SubscriptionRepository(
    testdb,
    Getter.fromValue(profileRepository),
    Getter.fromValue(transactionRepository),
    Getter.fromValue(dealRepository),
    Getter.fromValue(userRepository),
  );
  await subscriptionRepo.deleteAll();
}

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

let userRepository: UserRepository;
let entitiesRepository: EntitiesRepository;
export const profileRepository = TestProfileRepository;
const distributionRepository = TestRepository(DistributionRepository);
export const dealRepository = TestRepository(
  DealRepository,
  async () => entitiesRepository,
  async () => profileRepository,
  async () => distributionRepository,
);
export const transactionRepository = TestRepository(TransactionRepository);
