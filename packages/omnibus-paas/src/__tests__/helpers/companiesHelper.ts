import {juggler} from '@loopback/repository';
import {Getter} from '@loopback/context';
import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {CompanyRepository} from '../../repositories/company.repository';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../../multi-tenancy';
import {UserRepository, AuditLogRepository} from '../../repositories';
import {Company} from '../../models';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });
  const userRepository = new UserRepository(testdb);
  const auditLogRepository = new AuditLogRepository(
    testdb,
    Getter.fromValue(userRepository),
  );

  const companyRepo: CompanyRepository = new CompanyRepository(
    testdb,
    profile,
    tenant,
    Getter.fromValue(auditLogRepository),
  );
  await companyRepo.deleteAll();
}

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

export const TestMasterCompanyData = new Company({
  type: 'MASTER_ENTITY',
  name: 'Test Manager, LLC',
  address: {
    address1: '123 Main St. 200 S',
    address2: undefined,
    city: 'Salt Lake City',
    state: 'Utah',
    postalCode: '84101',
    country: 'United States of America',
  },
  email: 'ewallace@email.com',
  phone: '+1 801-567-3456',
});
export const TestManagerCompanyData = new Company({
  type: 'MANAGER',
  name: 'Elizabeth Wallace',
  address: {
    address1: '123 Main St. 200 S',
    address2: undefined,
    city: 'Salt Lake City',
    state: 'Utah',
    postalCode: '84101',
    country: 'United States of America',
  },
  email: 'ewallace@email.com',
  phone: '+1 801-567-3456',
  stateOfFormation: 'Delaware',
  entityType: 'LLC',
});
