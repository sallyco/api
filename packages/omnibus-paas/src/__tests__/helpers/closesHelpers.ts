import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {CloseRepository} from '../../repositories';
import {EntitiesRepository} from '../../repositories';
import {DealRepository} from '../../repositories/deal.repository';
import {TestRepository} from './globalHelpers';
import {TestDealRepository} from './RepositoryHelper';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  const entitiesRepository = TestRepository(EntitiesRepository);
  const dealRepo: DealRepository = TestDealRepository;
  const closesRepo: CloseRepository = TestRepository(
    CloseRepository,
    async () => entitiesRepository,
    async () => dealRepo,
  );
  await closesRepo.deleteAll();
}
