import {UserProfile} from '@loopback/security';

export const givenTestUser = () => {
  const userProfile: UserProfile = Object.assign({
    name: 'Test',
    email: 'test@test.com',
    id: '12345',
    roles: {
      account: {
        roles: [
          'manage-account',
          'manage-account-links',
          'view-profile',
          'investor',
          'organizer',
        ],
      },
    },
  });
  return userProfile;
};
