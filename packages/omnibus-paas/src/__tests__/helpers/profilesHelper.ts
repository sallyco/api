import {juggler} from '@loopback/repository';
import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {Profile} from '../../models';
import {TestRepositoryCleanup} from './RepositoryHelper';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  await TestRepositoryCleanup();
}

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

export const TestIndividualOrganizerProfileData = new Profile({
  ownerId: 'de2c9869-eb41-47f3-80d6-9f3b9d034e39',
  tenantId: 'gbt',
  profileType: 'ORGANIZER',
  stateOfFormation: 'Utah',
  countryOfFormation: 'United States of America',
  firstName: 'Individual',
  lastName: 'Organizer',
  address: {
    address1: '6510 Millrock Drive',
    city: 'Holladay',
    state: 'Utah',
    postalCode: '84121',
    country: 'United States of America',
  },
  phone: '(877) 492 7555',
  email: 'test@test.com',
  taxDetails: {
    registrationType: 'INDIVIDUAL',
    taxIdentification: {
      type: 'ssn',
      value: '123-13-2132',
    },
  },
  isUSBased: true,
  investorStatus: [],
  createdAt: new Date('2021-04-20T21:54:01.740Z'),
  updatedAt: new Date('2021-05-14T22:18:21.804Z'),
  isDeleted: false,
});

export const TestLLCOrganizerProfileData = new Profile({
  ownerId: 'd809c38b-f47f-40e9-8698-7e759c090a45',
  tenantId: 'gbt',
  profileType: 'ORGANIZER',
  stateOfFormation: 'Utah',
  countryOfFormation: 'United States of America',
  name: 'Test Entity, LLC',
  firstName: 'Organizer',
  lastName: 'Organizer',
  address: {
    address1: '296 Fay Loaf',
    city: 'Ashlynnburgh',
    state: 'Utah',
    postalCode: '84005',
    country: 'United States of America',
  },
  phone: '(123) 123 1232',
  email: 'organizer@test.com',
  taxDetails: {
    registrationType: 'ENTITY',
    taxIdentification: {
      type: 'ssn',
      value: '123-12-3123',
    },
  },
  isUSBased: true,
  investorStatus: [],
  purchaserStatus: [],
  createdAt: new Date('2021-06-10T16:16:29.340Z'),
  updatedAt: new Date('2021-06-10T16:16:29.340Z'),
  isDeleted: false,
});
