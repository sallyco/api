import {juggler} from '@loopback/repository';
import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {Deal} from '../../models';
import {TestDealRepository} from './RepositoryHelper';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  await TestDealRepository.deleteAll();
}

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

export const TestDealData = new Deal({
  ownerId: 'de2c9869-eb41-47f3-80d6-9f3b9d034e39',
  tenantId: 'gbt',
  name: 'Dealio',
  description: '<p>Deal</p>',
  targetRaiseAmount: 100000,
  minInvestmentAmount: 10,
  previouslyRaisedAmount: 10,
  isPublic: false,
  estimatedCloseDate: new Date('2022-02-21T00:00:00.000Z'),
  organizerCarryPercentage: 10,
  status: 'OPEN',
  createdAt: new Date('2021-04-20T21:54:35.093Z'),
  updatedAt: new Date('2021-05-12T05:03:05.952Z'),
  isDeleted: false,
  entityId: '607f4d9bc508cb0d380551ed',
  profileId: '607f4d79c508cb0d380551ea',
  requireQualifiedPurchaser: false,
  files: ['609b5deedfb2d41cc3181be2'],
  organizerFiles: ['609b604d15ef82699d565db5', '609b618915ef82699d565db6'],
});
