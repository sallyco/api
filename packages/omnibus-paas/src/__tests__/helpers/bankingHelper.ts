import {
  BankingProvider,
  BankingTreasuryPrimeService,
  PlaidService,
} from '../../services';
import {createStubInstance} from '@loopback/testlab';

const givenStubBankingService = () => {
  const bankingTreasuryPrimeService = createStubInstance(
    BankingTreasuryPrimeService,
  );
  const bankingServiceProvider = new BankingProvider(
    bankingTreasuryPrimeService,
  );
  const plaidService = createStubInstance(PlaidService);
  return {
    bankingTreasuryPrimeService,
    bankingServiceProvider,
    plaidService,
  };
};

export {givenStubBankingService};
