import {createStubInstance} from '@loopback/testlab';
import {
  AuditLogRepository,
  AccountRepository,
  CompanyRepository,
  DealRepository,
  GlobalRepository,
  ProfileRepository,
  FileRepository,
  EntitiesRepository,
  SubscriptionRepository,
  TransactionRepository,
  FormDRepository,
  DistributionRepository,
  UserRepository,
} from '../../repositories';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../../multi-tenancy';
import {WebhookService} from '../../services';

import {juggler} from '@loopback/repository';
import {TestRepository} from './globalHelpers';

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

export async function givenEmptyDatabase() {
  // Relationships Docs show how to include proper injection resolution for relations
  // https://loopback.io/doc/en/lb4/Testing-your-application.html
  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });
  const tenant: Tenant = Object.assign({
    name: 'Test',
  });
  const webHookService = createStubInstance(WebhookService);

  let auditLogRepository: AuditLogRepository;
  let entitiesRepository: EntitiesRepository;
  const companyRepository = TestRepository(CompanyRepository);
  let formDRepository: FormDRepository;
  let userRepository: UserRepository;
  const distributionRepository = TestRepository(DistributionRepository);
  const profileRepository = TestRepository(
    ProfileRepository,
    async () => companyRepository,
  );
  const dealRepository = TestRepository(
    DealRepository,
    async () => entitiesRepository,
    async () => profileRepository,
    async () => distributionRepository,
  );
  const transactionRepository = TestRepository(TransactionRepository);
  const globalRepository = TestRepository(GlobalRepository);

  await new AccountRepository(testdb).deleteAll();
  await dealRepository.deleteAll();
  await globalRepository.deleteAll();
  await profileRepository.deleteAll();
  await new FileRepository(testdb).deleteAll();
  await new EntitiesRepository(
    testdb,
    async () => dealRepository,
    async () => companyRepository,
    async () => formDRepository,
    async () => userRepository,
    profile,
    tenant,
    async () => webHookService,
    async () => auditLogRepository,
  ).deleteAll();
  await new SubscriptionRepository(
    testdb,
    async () => profileRepository,
    async () => transactionRepository,
    async () => dealRepository,
    async () => userRepository,
  ).deleteAll();
}
