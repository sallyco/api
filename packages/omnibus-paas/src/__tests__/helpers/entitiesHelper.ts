import {createStubInstance} from '@loopback/testlab';
import {juggler} from '@loopback/repository';
import {givenHttpServerConfig} from '@loopback/testlab';
import {OmnibusPaasApplication} from '../../application';
import {
  CompanyRepository,
  EntitiesRepository,
  FormDRepository,
  AuditLogRepository,
} from '../../repositories';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../../multi-tenancy';
import {Getter} from '@loopback/context/dist/inject';
import {Entities} from '../../models';
import {WebhookService} from '../../services';
import {
  TestUserRepository,
  TestDealRepository,
  TestProfileRepository,
} from './RepositoryHelper';

export async function givenRunningApplicationWithCustomConfiguration() {
  const app = new OmnibusPaasApplication({
    rest: givenHttpServerConfig(),
  });

  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });

  await app.boot();

  /**
   * Override default config for DataSource for testing so we don't write
   * test data to file when using the memory connector.
   */

  // Start Application
  await app.start();
  return app;
}

export async function givenEmptyDatabase() {
  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });

  const auditLogRepository = new AuditLogRepository(
    testdb,
    Getter.fromValue(userRepository),
  );
  const webHookService = createStubInstance(WebhookService);

  const entityRepo: EntitiesRepository = new EntitiesRepository(
    testdb,
    Getter.fromValue(dealRepository),
    Getter.fromValue(companyRepository),
    Getter.fromValue(formDRepository),
    Getter.fromValue(userRepository),
    profile,
    tenant,
    Getter.fromValue(webHookService),
    Getter.fromValue(auditLogRepository),
  );
  await entityRepo.deleteAll();
}

export const testdb: juggler.DataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
});

const profile: UserProfile = Object.assign({
  name: 'dev',
  id: '12345',
  roles: {
    account: {
      roles: ['manage-account', 'manage-account-links', 'view-profile'],
    },
  },
});

let formDRepository: FormDRepository;
export const profileRepository = TestProfileRepository;
export const dealRepository = TestDealRepository;
export const userRepository = TestUserRepository;

const tenant: Tenant = Object.assign({
  name: 'Test',
});

const auditLogRepository = new AuditLogRepository(
  testdb,
  Getter.fromValue(userRepository),
);

export const companyRepository = new CompanyRepository(
  testdb,
  profile,
  tenant,
  Getter.fromValue(auditLogRepository),
);

export const TestEntityData = new Entities({
  ownerId: 'de2c9869-eb41-47f3-80d6-9f3b9d034e39',
  tenantId: 'gbt',
  dealId: '607f4d9bc508cb0d380551ec',
  name: 'Series 001, a series of Glassboard Master II LLC',
  entityType: 'LIMITED_LIABILITY_COMPANY',
  regulationType: 'REGULATION_D',
  minInvestmentAmount: 10,
  countryOfFormation: 'US',
  createdAt: new Date('2021-04-20T21:54:35.581Z'),
  updatedAt: new Date('2021-05-12T18:03:07.090Z'),
  isDeleted: false,
  bankAccount: {
    providerMeta: {
      typeId: 'TREASURYPRIME',
      accountStatus: 'approved',
      accountApplicationId: 'aact_11g6ydgz27yc86',
      accountId: 'acct_11g6ydh827yc9q',
    },
    createdAt: new Date('2021-04-20T21:55:27.477Z'),
    updatedAt: new Date('2021-04-20T22:32:40.897Z'),
    bankContact: {
      name: 'Sara Spaulding',
      phone: '978-225-1402',
      email: 'sspaulding@bankprov.com',
    },
    bankName: 'The Provident Bank',
    //bankAddress: {
    //  address1: '5 Market Street',
    //  address2: '',
    //  city: 'Amesbury',
    //  postalCode: '01913',
    //  state: 'MA',
    //  country: 'United States of America',
    //},
    //beneficiaryAddress:
    //  '6510 Millrock Drive, Ste 400, Salt Lake City, Utah 84121',
    //beneficiaryPhone: '801-419-0677 x2',
    accountNumber: '420355051',
    accountName: 'Series 001, a series of Glassboard Master II LLC',
    routingNumber: '211374020',
    swiftCode: 'PRDIUS33',
  },
  ein: '12-3467137',
  entityDocuments: {
    operatingAgreement: '607f4d9dc508cb0d380551ee',
    privatePlacementMemorandum: '607f4d9dc508cb0d380551ef',
    subscriptionAgreement: '607f4d9ec508cb0d380551f0',
  },
  legalIncOrder: {
    status: 1,
    orderId: 957658,
  },
  managementFee: {
    type: 'percent',
    amount: 2,
    frequency: 'Annual',
    isRecurring: true,
  },
});
