import {UserProfile} from '@loopback/security';
import {createStubInstance, expect, sinon} from '@loopback/testlab';
import {NotificationsController} from '../../controllers';
import {Tenant} from '../../multi-tenancy';
import {NotificationRepository, ProfileRepository} from '../../repositories';

describe('Notifications_Controller', () => {
  let tenant: Tenant;
  let notificationRepository;
  let profileRepository;
  let profile: UserProfile;
  before(async () => {
    tenant = Object.assign({
      name: 'Glassboard Technology',
      id: 'gbt',
    });
    profile = Object.assign({
      id: '6212b5f7-e511-4fab-80e4-c1e84af703fe',
    });
  });

  beforeEach(async () => {
    notificationRepository = createStubInstance(NotificationRepository);
    profileRepository = createStubInstance(ProfileRepository);
  });

  describe('GET /notifications/count', () => {
    it('Gets the count of all notifications', async () => {
      const controller = new NotificationsController(
        notificationRepository,
        profileRepository,
        profile,
        tenant,
      );
      notificationRepository.stubs.count.resolves({count: 2});

      const notifications = await controller.count();

      expect(notifications).to.containEql({count: 2});
      sinon.assert.calledWithMatch(notificationRepository.stubs.count);
    });
  });

  describe('GET /notifications', () => {
    it('Gets all notifications', async () => {
      const controller = new NotificationsController(
        notificationRepository,
        profileRepository,
        profile,
        tenant,
      );
      notificationRepository.stubs.find.resolves([
        {id: '112345', tenantId: 'asdfasdf'},
      ]);

      const notifications = await controller.find(10, 0);

      expect(notifications.data).to.containEql({
        id: '112345',
        tenantId: 'asdfasdf',
      });
      sinon.assert.calledWithMatch(notificationRepository.stubs.find);
    });
  });

  describe('GET /notifications/{id}', () => {
    it('Gets one specific notification', async () => {
      const controller = new NotificationsController(
        notificationRepository,
        profileRepository,
        profile,
        tenant,
      );
      notificationRepository.stubs.findById.resolves({
        id: '112345',
        tenantId: 'asdfasdf',
      });

      const notifications = await controller.findById('112345');

      expect(notifications).to.containEql({id: '112345', tenantId: 'asdfasdf'});
      sinon.assert.calledWithMatch(notificationRepository.stubs.findById);
    });
  });
});
