// import {UserProfile} from '@loopback/security';
// import {expect} from '@loopback/testlab';
// import {CompaniesController} from '../../controllers/companies.controller';
// import {Company} from '../../models/company.model';
// import {CompanyRepository} from '../../repositories/company.repository';
// import {givenEmptyDatabase, testdb} from '../helpers/companiesHelper';

// describe('company controller integration tests', () => {
//   let repo: CompanyRepository;
//   let controller: CompaniesController;

//   before(async () => {
//     repo = new CompanyRepository(testdb);
//     controller = new CompaniesController(repo);
//   });

//   after(givenEmptyDatabase);

//   beforeEach(async () => {
//     const companies = [
//       new Company({
//         type: 'FUND_MANAGER',
//         name: 'Samantha Smith',
//         address: {
//           address1: '123 Main St.',
//           address2: 'Suite 200',
//           city: 'San Diego',
//           state: 'California',
//           postalCode: '12345',
//         },
//         title: 'Ms.',
//         email: 'samanthasmith@email.com',
//         phone: '+1 123-123-1234',
//         isEntity: true,
//         entityInfo: {
//           name: 'My Company, LLC',
//           stateOfFormation: 'Delaware',
//           countryOfFormation: 'United States of America',
//         },
//       }),
//       new Company({
//         type: 'REGISTERED_AGENT',
//         name: 'Lawrence Jones',
//         address: {
//           address1: '17th Street 5th Avenue',
//           city: 'New York City',
//           state: 'New York',
//           country: 'United States of America',
//         },
//         title: 'Mr.',
//       }),
//     ];

//     await controller.createCompany(profile, companies[0]);
//     await controller.createCompany(profile, companies[1]);
//   });

//   afterEach(givenEmptyDatabase);

//   const profile: UserProfile = Object.assign({
//     name: 'dev',
//     id: '12345',
//     roles: {
//       account: {
//         roles: ['manage-account', 'manage-account-links', 'view-profile'],
//       },
//     },
//   });

//   const companyData = new Company({
//     type: 'FUND_MANAGER',
//     name: 'Elizabeth Wallace',
//     address: {
//       address1: '123 Main St. 200 S',
//       city: 'Salt Lake City',
//       state: 'Utah',
//       postalCode: '84101',
//     },
//     title: 'Ms.',
//     email: 'ewallace@email.com',
//     phone: '+1 801-567-3456',
//   });

//   it('should get all the companies in the database', async () => {
//     const companies = await controller.readCompanies(2, 0, profile);

//     expect(companies.totalCount).to.equal(2);
//   });

//   it('should get an company by id', async () => {
//     const companies = await controller.readCompanies(2, 0, profile);
//     let id;
//     let name;
//     if (companies.data) {
//       id = companies.data[0].id;
//       name = companies.data[0].name;
//     }
//     const companies1 = await controller.readCompanyById(
//       id,
//       profile,
//     );
//     expect(companies1.name).equal(name);
//   });

//   it('should create an company successfully when given valid data', async () => {
//     const savedCompany = await controller.createCompany(profile, companyData);

//     expect(savedCompany.id).to.not.be.undefined();
//     expect(savedCompany.name).equal(companyData.name);
//     expect(savedCompany.address.address1).equal(companyData.address.address1);
//     expect(savedCompany.address.city).equal(companyData.address.city);
//     expect(savedCompany.address.state).equal(companyData.address.state);
//     expect(savedCompany.address.country).equal(companyData.address.country);
//     expect(savedCompany.address.postalCode).equal(
//       companyData.address.postalCode,
//     );
//     expect(savedCompany.title).equal(companyData.title);
//     expect(savedCompany.email).equal(companyData.email);
//     expect(savedCompany.phone).equal(companyData.phone);
//     expect(savedCompany.isEntity).equal(false);
//     expect(savedCompany.createdAt).to.not.be.undefined();
//     expect(savedCompany.updatedAt).to.not.be.undefined();
//     expect(savedCompany.isDeleted).equal(false);
//   });

//   it('should update an company by ID', async () => {
//     const companies = await controller.readCompanies(2, 0, profile);
//     let id;
//     let company;
//     if (companies.data) {
//       id = companies.data[1].id;
//       company = companies.data[1];
//     }
//     company.name = 'Lawrence Jones';

//     await controller.updateCompanyById(profile, id, company);
//     const updatedCompanies = await controller.readCompanies(2, 0, profile);
//     let updatedName;
//     if (updatedCompanies.data) {
//       updatedName = updatedCompanies.data[1].name;
//     }

//     expect(updatedName).equal('Lawrence Jones');
//   });

//   it('should delete an company by ID', async () => {
//     const companies = await controller.readCompanies(100, 0, profile);
//     expect(companies.data).to.have.length(2);
//     let id;
//     if (companies.data) {
//       id = companies.data[0].id;
//     }

//     await controller.deleteCompanyById(id, profile);
//     const updatedCompanies = await controller.readCompanies(100, 0, profile);

//     expect(updatedCompanies.data).to.have.length(1);
//   });
// });
