import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {TestRepository} from '__tests__/helpers/globalHelpers';
import {CloseController} from '../../controllers';
import {Close, Deal, Entities, FormD, Profile} from '../../models';
import {
  CloseRepository,
  FileRepository,
  DealRepository,
  TenantRepository,
  FormDRepository,
  EntitiesRepository,
  ProfileRepository,
} from '../../repositories';
import {
  CloseAuditsService,
  CloseService,
  DocumentsGeneratorService,
  DocumentsService,
  TransactionService,
} from '../../services';

describe('Close controller integration tests', () => {
  let closeRepository: CloseRepository;
  let fileRepository: FileRepository;
  let dealRepository: DealRepository;
  let tenantRepository: TenantRepository;
  let formDRepository: FormDRepository;
  let entitiesRepository: EntitiesRepository;
  let profileRepository: ProfileRepository;
  let controller: CloseController;
  let documentsService: StubbedInstanceWithSinonAccessor<DocumentsService>;
  let closeService: StubbedInstanceWithSinonAccessor<CloseService>;
  let closeAuditsService: StubbedInstanceWithSinonAccessor<CloseAuditsService>;
  let transactionService: StubbedInstanceWithSinonAccessor<TransactionService>;
  let documentsGeneratorService: StubbedInstanceWithSinonAccessor<DocumentsGeneratorService>;

  const formD = new FormD({
    cik: '123456',
    cikPassword: '654321',
    ccc: '1234567',
    accessionNumber: '87654321',
    fileNumber: '0987654321',
    filedDate: '2021-09-30T14:10:00.000+05:30',
    blueSkyPaidDate: '2021-09-30T14:11:00.000+05:30',
    blueSkyReceipt: '1234567',
    entitiesId: '61556734a947165c891bd674',
  });

  const close = new Close({
    organizerSignature: '',
    statement: {},
    entityId: '61556734a947165c891bd674',
    dealId: '61556734a947165c891bd673',
  });

  const entity = new Entities({
    tenantId: 'gbt',
    name: 'Series 009, a series of Glassboard Master II LLC',
    entityType: 'LIMITED_LIABILITY_COMPANY',
    regulationType: 'REGULATION_D',
    minInvestmentAmount: 1,
    countryOfFormation: 'US',
    managementFee: {
      amount: 0,
      type: 'percent',
    },
    expenseReserve: {
      amount: 0,
      type: 'Flat Fee',
    },
    createdAt: new Date('2021-09-30T12:58:52.818+05:30'),
    updatedAt: new Date('2021-09-30T12:59:07.877+05:30'),
    isDeleted: false,
    ownerId: '26956525-9e4a-4a67-b796-6b5964873763',
    entityDocuments: {
      operatingAgreement: '61556736a947165c891bd675',
      privatePlacementMemorandum: '61556737a947165c891bd676',
      subscriptionAgreement: '6155673ca947165c891bd677',
    },
    ein: '12-3765640',
    legalIncOrder: {
      status: 1,
      orderId: 1121320,
    },
  });

  const deal = new Deal({
    ownerId: '26956525-9e4a-4a67-b796-6b5964873763',
    tenantId: 'gbt',
    name: 'Test Seed 5.4.5',
    targetRaiseAmount: 545000,
    minInvestmentAmount: 1,
    previouslyRaisedAmount: 54500,
    isPublic: false,
    disabled: false,
    estimatedCloseDate: new Date('2022-01-01T05:30:00.000+05:30'),
    organizerCarryPercentage: 10,
    status: 'OPEN',
    requireQualifiedPurchaser: false,
    createdAt: new Date('2021-09-30T12:58:52.514+05:30'),
    updatedAt: new Date('2021-09-30T12:59:17.209+05:30'),
    isDeleted: false,
  });

  const profile = new Profile({
    tenantId: 'gbt',
    profileType: 'INVESTOR',
    stateOfFormation: 'California',
    countryOfFormation: 'United States of America',
    displayName: 'DN TI7-2',
    firstName: 'Test',
    lastName: 'Investor Seven',
    address: {
      address1: 'Address 7.1',
      address2: 'Address 7.2',
      city: 'City 7',
      state: 'California',
      postalCode: '00000',
      country: 'United States of America',
    },
    phone: '(000) 000 0000',
    dateOfBirth: '2000-01-01',
    email: 'ti7@t.t',
    taxDetails: {
      registrationType: 'INDIVIDUAL',
      taxIdentification: {
        type: 'ssn',
        value: '987-65-4321',
      },
    },
    isUSBased: true,
    investorStatus: [0],
    ownerId: '22706841-7901-4ea9-98f7-35ac0ad4319e',
    purchaserStatus: [],
    createdAt: new Date('2021-09-30T13:01:45.767+05:30'),
    updatedAt: new Date('2021-09-30T13:01:45.767+05:30'),
    isDeleted: false,
  });

  beforeEach(async () => {
    documentsService = createStubInstance(DocumentsService);
    closeService = createStubInstance(CloseService);
    documentsGeneratorService = createStubInstance(DocumentsGeneratorService);
    closeAuditsService = createStubInstance(CloseAuditsService);
    closeRepository = TestRepository(CloseRepository);
    fileRepository = TestRepository(FileRepository);
    dealRepository = TestRepository(DealRepository);
    tenantRepository = TestRepository(TenantRepository);
    formDRepository = TestRepository(FormDRepository);
    entitiesRepository = TestRepository(EntitiesRepository);
    profileRepository = TestRepository(ProfileRepository);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    controller = new CloseController(
      closeRepository,
      fileRepository,
      dealRepository,
      tenantRepository,
      formDRepository,
      entitiesRepository,
      documentsService,
      closeService,
      transactionService,
      documentsGeneratorService,
      closeAuditsService,
    );

    await dealRepository.deleteAll();
    await entitiesRepository.deleteAll();
    await closeRepository.deleteAll();
    await formDRepository.deleteAll();
    await profileRepository.deleteAll();

    const newProfile = await profileRepository.save(profile);
    deal.profileId = newProfile.id;
    const newDeal = await dealRepository.save(deal);
    entity.dealId = newDeal.id;
    close.dealId = newDeal.id;
    const newEntity = await entitiesRepository.save(entity);
    close.entityId = newEntity.id;
    formD.entitiesId = newEntity.id;
    await closeRepository.save(close);
    await formDRepository.save(formD);
  });

  xit('should get list of closed deals', async () => {
    const result = await controller.getClosedDeals();
    expect(result).to.be.not.null();
    expect(result?.length).to.be.equal(1);
  });
});
