import {Request} from '@loopback/rest';
import {UserProfile} from '@loopback/security';
import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {TestRepository} from '../../../helpers/globalHelpers';
import {PlaidController} from '../../../../controllers/banking/plaid.controller';
import {Tenant} from '../../../../multi-tenancy';
import {AccountRepository} from '../../../../repositories';
import {PlaidService} from '../../../../services';

describe('Plaid controller integration tests', () => {
  let controller: PlaidController;
  let accountRepository: AccountRepository;
  let plaidService: StubbedInstanceWithSinonAccessor<PlaidService>;

  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });

  const request: Request = Object.assign({});

  const testRequestData = {
    institution: {
      name: 'Test Bank',
      institution_id: 'ins_test',
    },
    account: {
      id: 'test_sub_account_id',
      name: 'Plaid Saving',
      type: 'depository',
      subtype: 'savings',
      mask: '1111',
    },
    account_id: 'test_sub_account_id',
    accounts: [
      {
        id: 'test_sub_account_id',
        name: 'Plaid Saving',
        mask: '1111',
        type: 'depository',
        subtype: 'savings',
      },
    ],
    link_session_id: '12345678-test-link-data-session12345',
    public_token: 'public-sandbox-test-data',
  };

  const testTokenResponse = {
    request_id: 'test_request_id',
    access_token: 'test_access_token',
    item_id: 'test_sub_account_id',
  };

  const testAuthResponse = {
    request_id: 'test_request_id',
    accounts: [],
    item: {
      available_products: [],
      billed_products: [],
      error: null,
      institution_id: '',
      item_id: 'test_sub_account_id',
      webhook: '',
      consent_expiration_time: null,
    },
    numbers: {
      ach: [
        {
          account: '1234567890',
          account_id: 'test_sub_account_id',
          routing: '',
          wire_routing: '',
        },
      ],
      eft: [],
      international: [],
      bacs: [],
    },
  };

  const testResetLoginResponse: {reset_login: true; request_id: string} = {
    reset_login: true,
    request_id: '',
  };

  const testLinkTokenResponse = {
    link_token: 'link-sandbox-b324710b-bacc-4229-9a36-6bb8ffcc5348',
    expiration: '',
    request_id: '',
  };

  beforeEach(async () => {
    plaidService = createStubInstance(PlaidService);
    plaidService.typeId = 'PLAID';

    accountRepository = TestRepository(AccountRepository);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    controller = new PlaidController(
      request,
      plaidService,
      tenant,
      profile,
      accountRepository,
    );

    await accountRepository.deleteAll();
  });

  it('should create a new account via Plaid', async () => {
    plaidService.stubs.exchangePublicToken.resolves(testTokenResponse);
    plaidService.stubs.getAuth.resolves(testAuthResponse);

    const result = await controller.convertPublicTokenToAccessToken(
      testRequestData,
      profile,
      tenant,
    );
    expect(result).to.be.not.null();
    expect(result?.type).to.equal('PLAID');
    expect(result?.plaidData?.accounts[0].mask).to.equal('1111');
  });

  it('should save account with proper mask even if request data have invalid characters in mask', async () => {
    const dataWithInvalidMask = {...testRequestData};
    dataWithInvalidMask.accounts[0].mask = 'abcd';
    plaidService.stubs.exchangePublicToken.resolves(testTokenResponse);
    plaidService.stubs.getAuth.resolves(testAuthResponse);

    const result = await controller.convertPublicTokenToAccessToken(
      dataWithInvalidMask,
      profile,
      tenant,
    );
    expect(result?.type).to.equal('PLAID');
    expect(result?.plaidData?.accounts[0].mask).to.equal('7890');
  });

  it('should return true if provided plaid credentials is valid', async () => {
    plaidService.stubs.getAuth.resolves(testAuthResponse);

    const result = await controller.checkIfVaildPlaidLink(testRequestData);
    expect(result).to.be.not.null();
    expect(result?.isError).to.equal(false);
  });

  it('should return false if provided plaid public token is invalid', async () => {
    plaidService.stubs.getAuth.rejects();

    const result = await controller.checkIfVaildPlaidLink(testRequestData);
    expect(result).to.be.not.null();
    expect(result?.isError).to.equal(true);
  });

  it('should return update token', async () => {
    plaidService.stubs.createUpdateLinkToken.resolves(testLinkTokenResponse);

    const result = await controller.getUpdateLinkToken(testRequestData);
    expect(result).to.be.not.null();
    expect(result).to.equal(testLinkTokenResponse.link_token);
  });

  it('should return true if credentials are reset', async () => {
    const testAccessToken =
      'access-sandbox-afda67b1-75c8-40f6-aa49-881b3c86a29d';
    plaidService.stubs.resetLogin.resolves(testResetLoginResponse);

    const result = await controller.resetLogin({accessToken: testAccessToken});
    expect(result).to.be.not.null();
    expect(result).to.equal(true);
  });

  it('should return false if credentials are not reset', async () => {
    const testAccessToken =
      'access-sandbox-afda67b1-75c8-40f6-aa49-881b3c86a29d';
    plaidService.stubs.resetLogin.rejects();

    const result = await controller.resetLogin({accessToken: testAccessToken});
    expect(result).to.be.not.null();
    expect(result).to.equal(false);
  });
});
