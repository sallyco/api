import {BankAccountController} from '../../../../controllers/banking/bank-account.controller';
import {UserProfile} from '@loopback/security';
import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {
  Banking,
  SystemNotificationsService,
  TransactionService,
} from '../../../../services';
import {givenStubBankingService} from '../../../helpers/bankingHelper';
import {
  AccountRepository,
  BankAccountRepository,
} from '../../../../repositories';
import {TestRepository} from '../../../helpers/globalHelpers';
import {Tenant} from 'multi-tenancy';
import {Transaction, TransactionStatus} from 'models';

describe('Bank Account controller integration tests', () => {
  let controller: BankAccountController;
  let bankingService: StubbedInstanceWithSinonAccessor<Banking>;
  let systemNotificationService: StubbedInstanceWithSinonAccessor<SystemNotificationsService>;
  let transactionService: StubbedInstanceWithSinonAccessor<TransactionService>;
  let bankAccountRepository: BankAccountRepository;
  let accountRepository: AccountRepository;

  const tenant: Tenant = {
    id: 'test',
    name: 'Test',
  };

  const userProfile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
    tenantId: tenant.id,
  });

  const testData = {
    amount: 100,
    routing: 'test',
    account: 'test',
    typeOfAccount: 'SAVINGS',
    nameOnAccount: 'Test Account',
    addressOnAccount: {
      address1: 'test',
      address2: 'test',
      region: 'test',
      city: 'test',
      postalCode: '00000',
      country: 'Unites States of America',
    },
    bankAddress: {
      address1: 'test',
      address2: 'test',
      region: 'test',
      city: 'test',
      postalCode: '00000',
      country: 'Unites States of America',
    },
    bankName: 'Test Bank',
    direction: 'CREDIT',
  };

  beforeEach(() => {
    const {bankingTreasuryPrimeService, plaidService} =
      givenStubBankingService();
    bankingService = bankingTreasuryPrimeService;
    bankAccountRepository = TestRepository(BankAccountRepository);
    accountRepository = TestRepository(AccountRepository);
    systemNotificationService = createStubInstance(SystemNotificationsService);
    transactionService = createStubInstance(TransactionService);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    controller = new BankAccountController(
      bankingService,
      bankAccountRepository,
      accountRepository,
      plaidService,
      systemNotificationService,
    );
  });

  describe('Wire Transaction Tests', () => {
    it('Should Create a Wire Transaction when passed valid data', async () => {
      bankingService.stubs.createWireCounterParty.resolves({
        id: '1',
      });
      transactionService.stubs.createWireTransaction.resolves(
        new Transaction({
          tenantId: 'gbt',
          amount: 100,
          counterpartyId: '1',
          direction: 'CREDIT',
          type: 'WIRE',
          status: TransactionStatus.QUEUED,
          subscriptionId: '1',
          dealId: '1',
          createdAt: new Date('2021-01-01T00:00:00.001Z'),
          needsApproval: true,
        }),
      );
      systemNotificationService.stubs.Notify_Transaction_Approval_Needed.resolves();
      const newTransaction = await controller.createWireTransaction(
        userProfile,
        tenant,
        transactionService,
        '1',
        {...testData, direction: 'CREDIT'},
      );
      expect(newTransaction.amount).to.be.equal(100);
    });
  });
});
