import {BankingBeneficiariesController} from '../../../../controllers/banking/banking-beneficiaries.controller';
import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {BankingBeneficiaryRepository} from '../../../../repositories/banking-beneficiary.repository';
import {Banking} from '../../../../services/banking/banking.service';
import {TestRepository} from '../../../helpers/globalHelpers';
import {UserProfile} from '@loopback/security';
import {Tenant} from '../../../../multi-tenancy';
import {BankingBeneficiary} from '../../../../models';

describe('Banking Beneficiaries controller integration tests', () => {
  let controller: BankingBeneficiariesController;
  let bankingBeneficiaryRepository: BankingBeneficiaryRepository;
  let bankingService: StubbedInstanceWithSinonAccessor<Banking>;

  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    id: 'Test',
    name: 'Test',
  });

  beforeEach(async () => {
    bankingBeneficiaryRepository = TestRepository(BankingBeneficiaryRepository);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    controller = new BankingBeneficiariesController(
      bankingBeneficiaryRepository,
      bankingService,
    );

    await bankingBeneficiaryRepository.deleteAll();

    await bankingBeneficiaryRepository.save(
      new BankingBeneficiary({
        tenantId: tenant.id,
        ownerId: profile.id,
        createdAt: '2021-01-01T00:00:01.000Z',
        email: 't@t.t',
        isDeleted: false,
        name: 'George Washington',
        phone: '(000) 000 0123',
        providerMeta: {
          name_on_account: 'George Washington',
          typeId: 'TREASURYPRIME',
          updated_at: '2021-01-01T00:00:01.000Z',
          wire: {
            account_number: '64141601',
            routing_number: '021001208',
            bank_name: 'Treasury Prime',
            bank_address: {
              city: 'Seattle',
              postal_code: '98102',
              state: 'AL',
              street_line_1: '123 Street',
              street_line_2: '123 Street',
            },
            address_on_account: {
              city: 'Duluth',
              postal_code: '55812',
              state: 'MN',
              street_line_1: '456 Street',
              street_line_2: '456 Street',
            },
          },
        },
        serviceObjectId: 'cp_11gh1h292x5m7z',
        updatedAt: '2021-01-01T00:00:01.000Z',
      }),
    );
  });

  afterEach(async () => {});

  it('should get a banking beneficiary by id', async () => {
    const beneficiaries = await controller.listBankingBeneficiaries(
      2,
      0,
      profile,
      tenant,
    );
    let id;
    let name;
    if (beneficiaries.data) {
      id = beneficiaries.data[0].id;
      name = beneficiaries.data[0].name;
    }
    const bankingBeneficiary = await controller.getBankingBeneficiaryById(
      id,
      profile,
      tenant,
    );
    expect(bankingBeneficiary?.name).equal(name);
  });

  it('should get a banking beneficiary by serviceObjectId', async () => {
    const beneficiaries = await controller.listBankingBeneficiaries(
      2,
      0,
      profile,
      tenant,
    );
    let name;
    let serviceObjectId;
    if (beneficiaries.data) {
      name = beneficiaries.data[0].name;
      serviceObjectId = beneficiaries.data[0].serviceObjectId;
    }
    const bankingBeneficiary =
      await controller.getBankingBeneficiaryByServiceObjectId(
        serviceObjectId,
        profile,
        tenant,
      );
    expect(bankingBeneficiary?.name).equal(name);
  });
});
