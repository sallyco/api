import {expect} from '@loopback/testlab';
import {TestRepository} from '../helpers/globalHelpers';
import {FundDocumentsController} from '../../controllers';
import {Close, Deal, Entities, Profile, Subscription} from '../../models';
import {
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  ProfileRepository,
  SubscriptionRepository,
} from '../../repositories';
import {
  TestDealRepository,
  TestEntityRepository,
  TestProfileRepository,
  TestRepositoryCleanup,
  TestSubscriptionRepository,
} from '../helpers/RepositoryHelper';

describe('Fund documents controller integration tests', () => {
  let closeRepository: CloseRepository;
  let subscriptionRepository: SubscriptionRepository;
  let dealRepository: DealRepository;
  let profileRepository: ProfileRepository;
  let entitiesRepository: EntitiesRepository;
  let controller: FundDocumentsController;

  const tenant = {
    id: 'test',
    name: 'Test',
  };
  const userProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
    tenantId: tenant.id,
  });

  const close = new Close({
    organizerSignature: 'test',
    statement: {},
    entityId: '61556734a947165c891bd674',
    dealId: '61556734a947165c891bd673',
  });

  const entity = new Entities({
    tenantId: 'gbt',
    name: 'Series 009, a series of Glassboard Master II LLC',
    entityType: 'LIMITED_LIABILITY_COMPANY',
    regulationType: 'REGULATION_D',
    minInvestmentAmount: 1,
    countryOfFormation: 'US',
    managementFee: {
      amount: 0,
      type: 'percent',
    },
    expenseReserve: {
      amount: 0,
      type: 'Flat Fee',
    },
    createdAt: new Date('2021-09-30T12:58:52.818+05:30'),
    updatedAt: new Date('2021-09-30T12:59:07.877+05:30'),
    isDeleted: false,
    ownerId: '26956525-9e4a-4a67-b796-6b5964873763',
    entityDocuments: {
      operatingAgreement: '61556736a947165c891bd675',
      privatePlacementMemorandum: '61556737a947165c891bd676',
      subscriptionAgreement: '6155673ca947165c891bd677',
    },
    ein: '12-3765640',
    legalIncOrder: {
      status: 1,
      orderId: 1121320,
    },
  });

  const deal = new Deal({
    ownerId: '26956525-9e4a-4a67-b796-6b5964873763',
    tenantId: 'gbt',
    name: 'Test Seed 5.4.5',
    targetRaiseAmount: 545000,
    minInvestmentAmount: 1,
    previouslyRaisedAmount: 54500,
    isPublic: false,
    disabled: false,
    estimatedCloseDate: new Date('2022-01-01T05:30:00.000+05:30'),
    organizerCarryPercentage: 10,
    status: 'OPEN',
    requireQualifiedPurchaser: false,
    createdAt: new Date('2021-09-30T12:58:52.514+05:30'),
    updatedAt: new Date('2021-09-30T12:59:17.209+05:30'),
    isDeleted: false,
  });

  const subscription = new Subscription({
    tenantId: tenant.id,
    name: 'Test User',
    email: 'test@test.com',
    status: 'COMPLETED',
    amount: 5000,
    dealId: deal.id,
    ownerId: userProfile.id,
    isDeleted: false,
  });

  const profile = new Profile({
    tenantId: 'gbt',
    profileType: 'INVESTOR',
    stateOfFormation: 'California',
    countryOfFormation: 'United States of America',
    displayName: 'DN TI7-2',
    firstName: 'Test',
    lastName: 'Investor Seven',
    address: {
      address1: 'Address 7.1',
      address2: 'Address 7.2',
      city: 'City 7',
      state: 'California',
      postalCode: '00000',
      country: 'United States of America',
    },
    phone: '(000) 000 0000',
    dateOfBirth: '2000-01-01',
    email: 'ti7@t.t',
    taxDetails: {
      registrationType: 'INDIVIDUAL',
      taxIdentification: {
        type: 'ssn',
        value: '987-65-4321',
      },
    },
    isUSBased: true,
    investorStatus: [0],
    ownerId: '22706841-7901-4ea9-98f7-35ac0ad4319e',
    purchaserStatus: [],
    createdAt: new Date('2021-09-30T13:01:45.767+05:30'),
    updatedAt: new Date('2021-09-30T13:01:45.767+05:30'),
    isDeleted: false,
  });

  beforeEach(async () => {
    closeRepository = TestRepository(CloseRepository);
    subscriptionRepository = TestSubscriptionRepository;
    profileRepository = TestProfileRepository;
    dealRepository = TestDealRepository;
    entitiesRepository = TestEntityRepository;

    controller = new FundDocumentsController(
      dealRepository,
      subscriptionRepository,
      closeRepository,
    );

    await dealRepository.deleteAll();
    await subscriptionRepository.deleteAll();
    await closeRepository.deleteAll();
    await profileRepository.deleteAll();
    await subscriptionRepository.deleteAll();

    const newProfile = await profileRepository.save(profile);
    deal.profileId = newProfile.id;
    const newEntity = await entitiesRepository.save(entity);
    deal.entityId = newEntity.id;
    const newDeal = await dealRepository.save(deal);
    subscription.dealId = newDeal.id;
    close.dealId = newDeal.id;
    close.entityId = newEntity.id;
    const newSub = await subscriptionRepository.save(subscription);
    close.subscriptions = [newSub.id];
    await closeRepository.save(close);
  });

  afterEach(async () => {
    await TestRepositoryCleanup();
  });

  it('should get list of fund documents', async () => {
    const result = await controller.find(userProfile, tenant);
    expect(result).to.be.not.null();
    expect(result?.length).to.be.equal(1);
    expect(result?.[0]?.subscriptionAmount).to.be.equal(subscription.amount);
    expect(result?.[0]?.dealName).to.be.equal(deal.name);
    expect(result?.[0]?.organizerName).to.be.equal(profile.name);
    expect(result?.[0]?.organizerSigned).to.be.equal(true);
  });
});
