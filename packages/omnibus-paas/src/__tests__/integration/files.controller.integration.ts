import {UserProfile} from '@loopback/security';
import {expect, stubHandlerContext} from '@loopback/testlab';
import {FilesController} from '../../controllers/files.controller';
import {File} from '../../models/file.model';
import {Tenant} from '../../multi-tenancy';
import {FileRepository} from '../../repositories/file.repository';
import {givenEmptyDatabase, testdb} from '../helpers/filesHelper';

describe('file controller integration tests', () => {
  let repo: FileRepository;
  let controller: FilesController;

  before(async () => {
    repo = new FileRepository(testdb);
    const context = stubHandlerContext({
      method: 'GET',
      url: '/files',
    });
    controller = new FilesController(repo, context.response);
  });

  after(givenEmptyDatabase);

  beforeEach(async () => {
    const files = [
      new File({
        name: 'File I',
        size: '600000',
        type: 'pdf',
        key: '234sdfe4543wrfsf43ts',
        lastModified: '1588140000000',
      }),
      new File({
        name: 'File II',
        size: '20000',
        type: 'pdf',
        key: '234sdfe4543wrfsf43ts',
        lastModified: '1588140000005',
      }),
    ];

    await controller.createFile(profile, tenant, files[0]);
    await controller.createFile(profile, tenant, files[1]);
  });

  afterEach(givenEmptyDatabase);

  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });

  const fileData = new File({
    name: 'File III',
    size: '500',
    type: 'pdf',
    key: '234sdfe4543wrfsf43ts',
    lastModified: '1588140000010',
  });

  it('should get all the files in the database', async () => {
    const getFiles = await controller.listFiles(2, 0, profile, tenant);

    expect(getFiles.totalCount).to.equal(2);
  });

  it('should get an file by id', async () => {
    const files = await controller.listFiles(2, 0, profile, tenant);
    let id;
    let name;
    if (files.data) {
      id = files.data[0].id;
      name = files.data[0].name;
    }

    const file1 = await controller.showFileById(id, profile, tenant);
    expect(file1?.name).equal(name);
  });

  it('should create a file successfully when given valid data', async () => {
    const savedFile = await controller.createFile(profile, tenant, fileData);

    expect(savedFile.id).to.not.be.undefined();
    expect(savedFile.name).equal(fileData.name);
    expect(savedFile.size).equal(fileData.size);
    expect(savedFile.type).equal(fileData.type);
    expect(savedFile.key).equal(fileData.key);
    expect(savedFile.lastModified).equal(fileData.lastModified);
    expect(savedFile.createdAt).to.not.be.undefined();
    expect(savedFile.updatedAt).to.not.be.undefined();
    expect(savedFile.isDeleted).equal(false);
  });

  it('should update a file by ID', async () => {
    const files = await controller.listFiles(2, 0, profile, tenant);
    let id;
    let file;
    if (files.data) {
      id = files.data[1].id;
      file = files.data[1];
    }
    file.name = 'My File';

    await controller.updateFileById(profile, tenant, id, file);
    const updatedFiles = await controller.listFiles(2, 0, profile, tenant);
    let updatedName;
    if (updatedFiles.data) {
      updatedName = updatedFiles.data[1].name;
    }

    expect(updatedName).equal('My File');
  });

  it('should delete a file by ID', async () => {
    const files = await controller.listFiles(100, 0, profile, tenant);
    expect(files.data).to.have.length(2);
    let id;
    if (files.data) {
      id = files.data[0].id;
    }

    await controller.softDeleteById(id, profile, tenant);
    const updatedFiles = await controller.listFiles(100, 0, profile, tenant);

    expect(updatedFiles.data).to.have.length(1);
  });
});
