import {Getter} from '@loopback/context/dist/inject';
import {Context} from '@loopback/core';
import {UserProfile} from '@loopback/security';
import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {SubscriptionsController} from '../../controllers/subscriptions.controller';
import {Subscription} from '../../models/subscription.model';
import {Tenant} from '../../multi-tenancy';
import {
  UserRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  SubscriptionRepository,
  TenantRepository,
  TransactionRepository,
  AuditLogRepository,
  ProfileRepository,
} from '../../repositories';
import {
  Banking,
  CeleryClientService,
  DocumentsService,
  FeesService,
  TenantService,
  TransactionService,
} from '../../services';
import {DocumentsGeneratorService} from '../../services/documents/documents-generator.service';
import {TestRepository} from '../helpers/globalHelpers';
import {givenStubBankingService} from '../helpers/bankingHelper';
import {givenStubCeleryService} from '../helpers/celeryHelper';
import {
  ExternalBankAccount,
  Deal,
  Entities as Entity,
  Entities,
  Transaction,
  TransactionStatus,
  AchReverseRequest,
} from '../../models';
import {HttpErrors} from '@loopback/rest';
import {createStubInstance} from '@loopback/testlab/dist/sinon';
import {
  TestUserRepository,
  TestDealRepository,
  TestEntityRepository,
  TestFileRepository,
  TestProfileRepository,
  TestRepositoryCleanup,
  TestSubscriptionRepository,
} from '../helpers/RepositoryHelper';

describe('Subscriptions controller integration tests', () => {
  let subscriptionRepository: SubscriptionRepository;
  let closeRepository: CloseRepository;
  let tenantRepository: TenantRepository;
  let fileRepository: FileRepository;
  let dealRepository: DealRepository;
  let userRepository: UserRepository;
  let entityRepository: EntitiesRepository;
  let profileRepository: ProfileRepository;
  let transactionRepository: TransactionRepository;
  let controller: SubscriptionsController;
  let documentsService: DocumentsService;
  let celeryClientService: StubbedInstanceWithSinonAccessor<CeleryClientService>;
  let transactionService: StubbedInstanceWithSinonAccessor<TransactionService>;
  let documentsGeneratorService: StubbedInstanceWithSinonAccessor<DocumentsGeneratorService>;
  let bankingService: StubbedInstanceWithSinonAccessor<Banking>;
  let ctx: Context;
  let subscription: Subscription;
  let deal: Deal;
  let entity: Entities;
  let profile: UserProfile;
  let tenant: Tenant;
  let tenantService: TenantService;
  let feeService: FeesService;

  afterEach(async () => {
    await TestRepositoryCleanup();
  });

  beforeEach(async () => {
    tenant = {
      id: 'test',
      name: 'Test',
    };
    profile = Object.assign({
      name: 'dev',
      id: '12345',
      roles: {
        account: {
          roles: ['manage-account', 'manage-account-links', 'view-profile'],
        },
      },
      tenantId: tenant.id,
    });

    const {bankingTreasuryPrimeService} = givenStubBankingService();
    bankingService = bankingTreasuryPrimeService;
    const {celeryClientService: celeryService} = givenStubCeleryService();
    celeryClientService = celeryService;
    userRepository = TestUserRepository;
    const auditLogRepository = TestRepository(
      AuditLogRepository,
      Getter.fromValue(userRepository),
    );
    subscriptionRepository = TestSubscriptionRepository;
    profileRepository = TestProfileRepository;
    fileRepository = TestFileRepository;
    dealRepository = TestDealRepository;
    entityRepository = TestEntityRepository;
    transactionRepository = TestRepository(
      TransactionRepository,
      profile,
      tenant,
      Getter.fromValue(auditLogRepository),
      ctx,
    );
    tenantService = createStubInstance(TenantService);
    feeService = createStubInstance(FeesService);

    transactionService = createStubInstance(TransactionService);
    controller = new SubscriptionsController(
      subscriptionRepository,
      fileRepository,
      dealRepository,
      entityRepository,
      profileRepository,
      transactionRepository,
      tenantRepository,
      closeRepository,
      documentsService,
      bankingService,
      celeryClientService,
      tenantService,
      feeService,
      documentsGeneratorService,
    );
    await subscriptionRepository.deleteAll();
    await fileRepository.deleteAll();
    await dealRepository.deleteAll();
    await entityRepository.deleteAll();
    await transactionRepository.deleteAll();

    entity = new Entity({
      tenantId: 'gbt',
      name: 'Test Entity',
      minInvestmentAmount: 10,
      entityType: 'LIMITED_LIABILITY_COMPANY',
      bankAccount: {
        providerMeta: {
          accountStatus: 'active',
          accountId: 'acct_test123',
        },
      },
      ownerId: profile.id,
    });
    entity = await entityRepository.save(entity);
    deal = new Deal({
      tenantId: tenant.id,
      name: 'Test Deal',
      entityId: entity.id,
      targetRaiseAmount: 10,
      minInvestmentAmount: 10,
      status: 'OPEN',
      ownerId: profile.id,
    });
    deal = await dealRepository.save(deal);
    subscription = await subscriptionRepository.save(
      new Subscription({
        tenantId: tenant.id,
        name: 'Test User',
        email: 'test@test.com',
        status: 'COMMITTED',
        amount: 5000,
        dealId: deal.id,
        bankAccount: new ExternalBankAccount({
          counterpartyId: 'cp_test123',
        }),
        ownerId: profile.id,
      }),
    );

    const subscriptions = [
      new Subscription({
        dealId: '1234',
        name: 'Subscription 1',
        type: 'INDIVIDUAL',
        email: 'samanthasmith@email.com',
        status: 'INVITED',
        amount: 10000,
        reconciledAmount: 20000,
        transactionIds: ['Test'],
        isDocsSigned: false,
        isKycAmlPassed: false,
        documents: {
          capitalAccountStatement: 'string',
          historicalCapitalAccountStatements: ['Test1', 'Test2'],
          signerOneOASigPage: 'string',
          signerTwoOASigPage: 'string',
          investorSASigPage: 'string',
          signerOneTaxDoc: 'string',
          signerTwoTaxDoc: 'string',
          mergedSignerOneOA: 'string',
          mergedSignerTwoOA: 'string',
          mergedInvestorSA: 'string',
          mergedCounterSignedOA: 'string',
          mergedCounterSignedSA: 'string',
        },
      }),
      new Subscription({
        dealId: '1234567',
        name: 'Subscription 3',
        type: 'INDIVIDUAL',
        email: 'samanthasmith1@email.com',
        status: 'INVITED',
        amount: 20000,
        reconciledAmount: 30000,
        transactionIds: ['Test1'],
        isDocsSigned: false,
        isKycAmlPassed: false,
        documents: {
          capitalAccountStatement: 'string',
          historicalCapitalAccountStatements: ['Test1', 'Test2'],
          signerOneOASigPage: 'string',
          signerTwoOASigPage: 'string',
          investorSASigPage: 'string',
          signerOneTaxDoc: 'string',
          signerTwoTaxDoc: 'string',
          mergedSignerOneOA: 'string',
          mergedSignerTwoOA: 'string',
          mergedInvestorSA: 'string',
          mergedCounterSignedOA: 'string',
          mergedCounterSignedSA: 'string',
        },
      }),
    ];

    await controller.createSubscription(profile, tenant, subscriptions[0]);
    await controller.createSubscription(profile, tenant, subscriptions[1]);
  });

  afterEach(async () => {});

  const subscriptionsData = new Subscription({
    dealId: '12345678',
    name: 'Subscription 3',
    type: 'INDIVIDUAL',
    email: 'samanthasmith3@email.com',
    status: 'INVITED',
    amount: 20000,
    reconciledAmount: 30000,
    transactionIds: ['Test1'],
    isDocsSigned: false,
    isKycAmlPassed: false,
    documents: {
      capitalAccountStatement: 'string',
      historicalCapitalAccountStatements: ['Test1', 'Test2'],
      signerOneOASigPage: 'string',
      signerTwoOASigPage: 'string',
      investorSASigPage: 'string',
      signerOneTaxDoc: 'string',
      signerTwoTaxDoc: 'string',
      mergedSignerOneOA: 'string',
      mergedSignerTwoOA: 'string',
      mergedInvestorSA: 'string',
      mergedCounterSignedOA: 'string',
      mergedCounterSignedSA: 'string',
    },
  });

  it('should get all the subscriptions in the database', async () => {
    const companies = await controller.listSubscriptions(2, 0, profile, tenant);

    expect(companies.totalCount).to.equal(3);
  });

  it('should get a subscription by id', async () => {
    const subscriptions = await controller.listSubscriptions(
      2,
      0,
      profile,
      tenant,
    );
    let id;
    let name;
    if (subscriptions.data) {
      id = subscriptions.data[0].id;
      name = subscriptions.data[0].name;
    }
    const companies1 = await controller.readSubscriptionById(id, profile);
    expect(companies1.name).equal(name);
  });

  it('should create an company successfully when given valid data', async () => {
    const savedSubscription = await controller.createSubscription(
      profile,
      tenant,
      subscriptionsData,
    );

    expect(savedSubscription.id).to.not.be.undefined();
    expect(savedSubscription.name).equal(subscriptionsData.name);
    expect(savedSubscription.dealId).equal(subscriptionsData.dealId);
    expect(savedSubscription.type).equal(subscriptionsData.type);
    expect(savedSubscription.email).equal(subscriptionsData.email);
    expect(savedSubscription.status).equal(subscriptionsData.status);
    expect(savedSubscription.amount).equal(subscriptionsData.amount);
    expect(savedSubscription.reconciledAmount).equal(
      subscriptionsData.reconciledAmount,
    );
    expect(savedSubscription.documents).equal(subscriptionsData.documents);
  });

  it('should update a subscription by ID', async () => {
    const subscriptions = await controller.listSubscriptions(
      2,
      0,
      profile,
      tenant,
    );
    let id;
    let subscription_test;
    if (subscriptions.data) {
      id = subscriptions.data[1].id;
      subscription_test = subscriptions.data[1];
    }
    subscription_test.name = 'Test Suscription';

    await controller.updateSubscriptionById(
      profile,
      tenant,
      id,
      subscription_test,
    );
    const updatedSubscriptions = await controller.listSubscriptions(
      2,
      0,
      profile,
      tenant,
    );
    let updatedName;
    if (updatedSubscriptions.data) {
      updatedName = updatedSubscriptions.data[1].name;
    }

    expect(updatedName).equal('Test Suscription');
  });

  it('should delete a subscription by ID', async () => {
    const subscriptions = await controller.listSubscriptions(
      100,
      0,
      profile,
      tenant,
    );
    expect(subscriptions.data).to.have.length(3);
    let id;
    if (subscriptions.data) {
      id = subscriptions.data[0].id;
    }

    await controller.deleteSubscriptionById(id, profile, tenant);
    const updatedSubscriptions = await controller.listSubscriptions(
      100,
      0,
      profile,
      tenant,
    );

    expect(updatedSubscriptions.data).to.have.length(2);
  });

  describe('Subscription Transaction Initiation', () => {
    it('Should queue a valid subscription transaction', async () => {
      const reverseAction = controller.initTransfer(
        subscription.id,
        transactionService,
      );
      await expect(reverseAction).to.be.fulfilled();
      expect(
        transactionService.stubs.createSubscriptionTransaction.calledOnce,
      ).to.equal(true);
      subscription = await subscriptionRepository.findById(subscription.id);
      expect(subscription.status).to.equal('FUNDING SENT');
    });

    it('Should allow subscription with transactionIds already set', async () => {
      subscription.transactionIds = ['test'];
      await subscriptionRepository.save(subscription);
      const reverseAction = controller.initTransfer(
        subscription.id,
        transactionService,
      );
      await expect(reverseAction).to.be.fulfilled();
      expect(
        transactionService.stubs.createSubscriptionTransaction.calledOnce,
      ).to.equal(true);
    });

    it('Should not allow subscription without banking', async () => {
      delete subscription.bankAccount;
      await subscriptionRepository.save(subscription);
      const reverseAction = controller.initTransfer(
        subscription.id,
        transactionService,
      );
      await expect(reverseAction).to.be.rejectedWith(
        new HttpErrors.FailedDependency(
          'Subscription must have a valid bank account attached',
        ),
      );
      expect(
        transactionService.stubs.createSubscriptionTransaction.called,
      ).to.equal(false);
    });

    it("Should not allow subscription that's not in 'COMMITTED' or 'COMPLETED' status ", async () => {
      subscription.status = 'PENDING';
      await subscriptionRepository.save(subscription);
      const reverseAction = controller.initTransfer(
        subscription.id,
        transactionService,
      );
      await expect(reverseAction).to.be.rejectedWith(
        new HttpErrors.FailedDependency(
          'Subscription must be in one of the following statuses before initiating transaction. (COMMITTED, COMPLETED, FUNDS IN TRANSIT - MANUAL, NEEDS FUNDING)',
        ),
      );
      expect(
        transactionService.stubs.createSubscriptionTransaction.called,
      ).to.equal(false);
    });

    it('Should not allow subscription without an amount set ', async () => {
      delete subscription.amount;
      await subscriptionRepository.save(subscription);
      const reverseAction = controller.initTransfer(
        subscription.id,
        transactionService,
      );
      await expect(reverseAction).to.be.rejectedWith(
        new HttpErrors.FailedDependency('Subscription amount must be set'),
      );
      expect(
        transactionService.stubs.createSubscriptionTransaction.called,
      ).to.equal(false);
    });
  });

  describe('Subscription Transaction Reversal', () => {
    it('Should queue a valid subscription for reversal', async () => {
      const transaction = await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );
      subscription.transactionIds = [transaction.id ?? ''];
      subscription.ownerId = profile.id;
      await subscriptionRepository.save(subscription);
      const reverseAction = controller.reverseTransfer(
        subscription.id,
        profile,
        tenant,
        transactionService,
      );
      await expect(reverseAction).to.be.fulfilled();
      expect(
        transactionService.stubs.createReverseSubscriptionTransaction
          .calledOnce,
      ).to.equal(true);
    });
  });
  it('Should allow the Deal Organizer to initiate the reversal', async () => {
    const transaction = await transactionRepository.save(
      new Transaction({
        tenantId: 'gbt',
        amount: 5000,
        counterpartyId: 'cp_test123',
        direction: 'DEBIT',
        type: 'ACH',
        status: TransactionStatus.SENT,
        subscriptionId: subscription.id,
        dealId: deal.id,
      }),
    );
    subscription.transactionIds = [transaction.id ?? ''];
    subscription.ownerId = 'investor';
    deal.ownerId = 'organizer';
    profile.id = 'organizer';
    await subscriptionRepository.save(subscription);
    await dealRepository.save(deal);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.fulfilled();
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.calledOnce,
    ).to.equal(true);
  });

  it('Should not allow an unrelated person to initiate the reversal', async () => {
    const transaction = await transactionRepository.save(
      new Transaction({
        tenantId: 'gbt',
        amount: 5000,
        counterpartyId: 'cp_test123',
        direction: 'DEBIT',
        type: 'ACH',
        status: TransactionStatus.SENT,
        subscriptionId: subscription.id,
        dealId: deal.id,
      }),
    );
    subscription.transactionIds = [transaction.id ?? ''];
    subscription.ownerId = 'investor';
    deal.ownerId = 'organizer';
    profile.id = 'person';
    await subscriptionRepository.save(subscription);
    await dealRepository.save(deal);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.rejectedWith(new HttpErrors.Forbidden());
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.called,
    ).to.equal(false);
  });

  it('Should allow an unrelated person in the master realm to initiate the reversal', async () => {
    const transaction = await transactionRepository.save(
      new Transaction({
        tenantId: 'gbt',
        amount: 5000,
        counterpartyId: 'cp_test123',
        direction: 'DEBIT',
        type: 'ACH',
        status: TransactionStatus.SENT,
        subscriptionId: subscription.id,
        dealId: deal.id,
      }),
    );
    subscription.transactionIds = [transaction.id ?? ''];
    subscription.ownerId = 'investor';
    deal.ownerId = 'organizer';
    tenant.id = 'master';
    profile.id = 'person';
    await subscriptionRepository.save(subscription);
    await dealRepository.save(deal);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.fulfilled();
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.calledOnce,
    ).to.equal(true);
  });

  it('Should not allow the transaction reversal if subscription already has a reversed transaction', async () => {
    subscription.transactionIds = ['testTransaction123'];
    subscription.reverseTransactionId = 'testReverseTransaction123';
    await subscriptionRepository.save(subscription);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.rejectedWith(
      new HttpErrors.Conflict('Refund already initiated for this subscription'),
    );
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.called,
    ).to.equal(false);
  });

  it("Should not allow the transaction reversal if subscription is in 'REFUNDED' status", async () => {
    subscription.status = 'REFUNDED';
    await subscriptionRepository.save(subscription);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.rejectedWith(
      new HttpErrors.Conflict('Refund already initiated for this subscription'),
    );
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.called,
    ).to.equal(false);
  });

  it('Should not allow the transaction reversal if subscription does not have transactionId and ACH Credentials are not supplied', async () => {
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.rejectedWith(
      'Off-Platform Funded Subscription Requires ACH Details to create Refund',
    );
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.called,
    ).to.equal(false);
  });

  it('Should still allow the transaction reversal if subscription does not have transactionId and ACH Credentials are supplied', async () => {
    delete subscription.transactionIds;
    delete subscription.bankAccount;
    await subscriptionRepository.save(subscription);
    bankingService.stubs.createACHCounterParty.resolves({id: 'test'});
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
      new AchReverseRequest({
        routing: '123123123',
        account: '123123123',
        nameOnAccount: 'Test Testerton',
        typeOfAccount: 'CHECKING',
      }),
    );
    await expect(reverseAction).to.be.fulfilled();
    expect(
      transactionService.stubs.createReverseSubscriptionTransaction.called,
    ).to.equal(true);
  });

  it('Should not allow the transaction reversal if previous transaction not sent', async () => {
    const transaction = await transactionRepository.save(
      new Transaction({
        tenantId: 'gbt',
        amount: 5000,
        counterpartyId: 'cp_test123',
        direction: 'DEBIT',
        type: 'ACH',
        status: TransactionStatus.PENDING,
        subscriptionId: subscription.id,
        dealId: deal.id,
      }),
    );
    subscription.transactionIds = [transaction.id ?? ''];
    await subscriptionRepository.save(subscription);
    await dealRepository.save(deal);
    const reverseAction = controller.reverseTransfer(
      subscription.id,
      profile,
      tenant,
      transactionService,
    );
    await expect(reverseAction).to.be.rejectedWith(
      new HttpErrors.FailedDependency(
        `Subscription is not ready to be refunded, transaction(s) are not settled into account. Transaction id ${transaction.id} status is ${TransactionStatus.PENDING}`,
      ),
    );
    expect(
      transactionService.stubs.createSubscriptionTransaction.called,
    ).to.equal(false);
  });

  describe('Subscription Cancellation Tests', () => {
    it('should allow a subscription to be cancelled (fully deleted) before funding', async () => {
      await controller.cancelSubscription(subscription.id, profile, tenant);
      await expect(
        subscriptionRepository.findById(subscription.id),
      ).to.be.rejectedWith(Error);
    });

    it('should cancel a subscription instead of delete if docs are signed already', async () => {
      subscription.isDocsSigned = true;
      subscription = await subscriptionRepository.save(subscription);
      await controller.cancelSubscription(subscription.id, profile, tenant);

      const result = await subscriptionRepository.findById(subscription.id);
      expect(result.status).to.equal('CANCELLED');
    });

    it('should not allow a subscription to be deleted/cancelled after funding', async () => {
      // if transactions exist it won't be deleted, but potentially cancelled instead
      await transactionRepository.save(
        new Transaction({
          tenantId: 'gbt',
          amount: 5000,
          counterpartyId: 'cp_test123',
          direction: 'DEBIT',
          type: 'ACH',
          status: TransactionStatus.SENT,
          subscriptionId: subscription.id,
          dealId: deal.id,
        }),
      );

      // Completed is TOO FAR along to be cancelled

      subscription.status = 'COMPLETED';
      await subscriptionRepository.save(subscription);
      const subscription_cancellation = controller.cancelSubscription(
        subscription.id,
        profile,
        tenant,
      );
      await expect(subscription_cancellation).to.be.rejectedWith(
        'Subscription cannot be cancelled without refund',
      );
    });

    it('should not allow a subscription to be cancelled if not allowed ownership', async () => {
      subscription.ownerId = 'TestUserOther';
      deal.ownerId = 'TestOrganizer';
      subscription = await subscriptionRepository.save(subscription);
      deal = await dealRepository.save(deal);
      const subscription_cancellation = controller.cancelSubscription(
        subscription.id,
        profile,
        tenant,
      );
      await expect(subscription_cancellation).to.be.rejected();
    });
  });
});
