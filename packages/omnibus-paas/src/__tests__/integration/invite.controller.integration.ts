import {Getter} from '@loopback/context/dist/inject';
import {UserProfile} from '@loopback/security';
import {expect} from '@loopback/testlab';
import {InvitesController} from '../../controllers/invite.controller';
import {InviteItem} from '../../models/invite-item.model';
import {Invite} from '../../models/invite.model';
import {Tenant} from '../../multi-tenancy';
import {DealRepository} from '../../repositories/deal.repository';
import {ProfileRepository} from '../../repositories/profile.repository';
import {InviteRepository} from '../../repositories/invite.repository';
import {SubscriptionRepository} from '../../repositories/subscription.repository';
import {UserRepository} from '../../repositories/user.repository';
import {dealRepository} from '../helpers/entitiesHelper';
import {givenEmptyDatabase, testdb} from '../helpers/inviteHelpers';
import {transactionRepository} from '../helpers/subscriptionsHelper';

describe('Invite controller integration tests', () => {
  let repo: InviteRepository;
  let repo2: SubscriptionRepository;
  let repo3: DealRepository;
  let profileRepository: ProfileRepository;
  let userRepository: UserRepository;
  let controller: InvitesController;

  before(async () => {
    repo = new InviteRepository(testdb);
    repo2 = new SubscriptionRepository(
      testdb,
      Getter.fromValue(profileRepository),
      Getter.fromValue(transactionRepository),
      Getter.fromValue(dealRepository),
      Getter.fromValue(userRepository),
    );
    repo3 = dealRepository;
    controller = new InvitesController(repo, repo2, repo3, profileRepository);
  });

  after(givenEmptyDatabase);
  const inviteItems = [
    new InviteItem({
      id: '123',
      type: 'Test Type',
      relation: 'Test Relation',
    }),
    new InviteItem({
      id: '1234',
      type: 'Test Type1',
      relation: 'Test Relation1',
    }),
  ];

  beforeEach(async () => {
    const invites = [
      new Invite({
        acceptableBy: '12345',
        invitedBy: '54321',
        invitedTo: inviteItems,
        invitedRole: 'Test',
        acceptedAt: new Date(),
      }),
      new Invite({
        acceptableBy: '1234567',
        invitedBy: '7654321',
        invitedTo: inviteItems,
        invitedRole: 'Test',
        acceptedAt: new Date(),
      }),
    ];

    await controller.createInvite(profile, tenant, invites[0]);
    await controller.createInvite(profile, tenant, invites[1]);
  });

  afterEach(givenEmptyDatabase);

  const profile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });

  const inviteData = new Invite({
    acceptableBy: '1234567',
    invitedBy: '7654321',
    invitedTo: inviteItems,
    invitedRole: 'Test',
    acceptedAt: new Date(),
  });

  it('should get all the invites in the database', async () => {
    const invites = await controller.listInvites(2, 0, profile, tenant);

    expect(invites.totalCount).to.equal(2);
  });

  it('should get an invite by id', async () => {
    const invites = await controller.listInvites(2, 0, profile, tenant);
    let id;
    let acceptableBy;
    if (invites.data) {
      id = invites.data[0].id;
      acceptableBy = invites.data[0].acceptableBy;
    }
    const invites1 = await controller.readInviteById(id);
    expect(invites1.acceptableBy).equal(acceptableBy);
  });

  it('should create an invite successfully when given valid data', async () => {
    const savedInvite = await controller.createInvite(
      profile,
      tenant,
      inviteData,
    );

    expect(savedInvite.id).to.not.be.undefined();
    expect(savedInvite.acceptableBy).equal(inviteData.acceptableBy);
    expect(savedInvite.invitedBy).equal(inviteData.invitedBy);
    expect(savedInvite.isDeleted).equal(false);
  });

  it('should update an invite by ID', async () => {
    const invites = await controller.listInvites(2, 0, profile, tenant);
    let id;
    let invite;
    if (invites.data) {
      id = invites.data[1].id;
      invite = invites.data[1];
    }
    invite.acceptableBy = '567';

    await controller.updateInviteById(profile, tenant, id, invite);
    const updatedInvites = await controller.listInvites(2, 0, profile, tenant);
    let acceptableBy;
    if (updatedInvites.data) {
      acceptableBy = updatedInvites.data[1].acceptableBy;
    }

    expect(acceptableBy).equal('567');
  });

  it('should delete an invite by ID', async () => {
    const invites = await controller.listInvites(100, 0, profile, tenant);
    expect(invites.data).to.have.length(2);
    let id;
    if (invites.data) {
      id = invites.data[0].id;
    }

    await controller.softDeleteById(id, profile, tenant);
    const updatedInvites = await controller.listInvites(
      100,
      0,
      profile,
      tenant,
    );

    expect(updatedInvites.data).to.have.length(1);
  });
});
