import {UserProfile} from '@loopback/security'; // provides the user details
import {expect, StubbedInstanceWithSinonAccessor} from '@loopback/testlab';
import {ProfilesController} from '../../controllers';
import {Profile} from '../../models';
import {Tenant} from '../../multi-tenancy';
import {ProfileRepository} from '../../repositories';
import {KycAmlOnfidoService} from '../../services';
import {givenEmptyDatabase} from '../helpers/profilesHelper';
import {AccountRepository} from '../../repositories';
import {createStubInstance} from '@loopback/testlab/dist/sinon';
import {KycAml} from '../../services/kycaml/kycaml.service.types';
import {
  TestKycRepository,
  TestProfileRepository,
  TestRepositoryCleanup,
} from '../helpers/RepositoryHelper';
import {Individual} from '../../models/individual.model';
import {ISO_Alpha_2} from '../../interfaces/datatypes/ISO';

describe('ProfileRepository', () => {
  let repo: ProfileRepository;
  let kycAmlService: StubbedInstanceWithSinonAccessor<KycAml>;
  let controller: ProfilesController;
  let accountRepository: AccountRepository;
  let profiles: Profile[];

  before(async () => {
    repo = TestProfileRepository;
    kycAmlService = createStubInstance(KycAmlOnfidoService);
    controller = new ProfilesController(
      repo,
      accountRepository,
      kycAmlService,
      TestKycRepository,
    );
  });

  after(givenEmptyDatabase);

  beforeEach(async () => {
    await TestRepositoryCleanup();
    profiles = [
      await repo.save(
        new Profile({
          profileType: 'ORGANIZER',
          taxDetails: {
            registrationType: 'TRUST',
            taxIdentification: {
              type: 'ssn',
              value: '123-12-4567',
            },
          },
          stateOfFormation: 'Texas',
          countryOfFormation: 'United States of America',
          name: 'Legal Name One',
          typeOfEntity: 'C_CORPORATION',
          primarySignatory: new Individual({
            name: 'Mick Williams',
            address: {
              address1: '234 Redwing',
              address2: 'Apt 45',
              city: 'Seattle',
              postalCode: '98898',
              state: 'Washington',
              country: 'United States of America',
            },
          }),
          address: {
            address1: '234 Redwing',
            address2: 'Apt 45',
            city: 'Seattle',
            postalCode: '98898',
            state: 'Washington',
            country: 'United States of America',
          },
          phone: '3602346654',
          passportId: '',
          isUSBased: true,
          ownerId: userProfile.id,
          tenantId: tenant.id,
        }),
      ),
      await repo.save(
        new Profile({
          profileType: 'INVESTOR',
          taxDetails: {
            registrationType: 'INDIVIDUAL',
            taxIdentification: {
              type: 'ssn',
              value: '123-12-4567',
            },
          },
          stateOfFormation: 'Texas',
          countryOfFormation: 'United States of America',
          name: 'Legal Name One',
          typeOfEntity: 'C_CORPORATION',
          firstName: 'Mick',
          lastName: 'Williams',
          address: {
            address1: '234 Redwing',
            address2: 'Apt 45',
            city: 'Spokane',
            state: 'Washington',
            country: 'United States of America',
            postalCode: '98898',
          },
          phone: '3602346654',
          passportId: '',
          isUSBased: true,
          ownerId: userProfile.id,
          tenantId: tenant.id,
        }),
      ),
    ];
  });

  afterEach(givenEmptyDatabase);

  const profileData = new Profile({
    profileType: 'ORGANIZER',
    taxDetails: {
      registrationType: 'ENTITY',
      taxIdentification: {
        type: 'ssn',
        value: '123-12-4567',
      },
    },
    stateOfFormation: 'Texas',
    countryOfFormation: 'United States of America',
    name: 'Legal Name One',
    typeOfEntity: 'C_CORPORATION',
    firstName: 'Mick',
    lastName: 'Williams',
    address: {
      address1: '234 Redwing',
      address2: 'Apt 45',
      city: 'Tacoma',
      postalCode: '98898',
      state: 'Washington',
      country: 'United States of America',
    },
    phone: '3602346654',
    passportId: '',
    isUSBased: true,
  });

  const userProfile: UserProfile = Object.assign({
    name: 'dev',
    id: '12345',
    roles: {
      account: {
        roles: ['manage-account', 'manage-account-links', 'view-profile'],
      },
    },
  });

  const tenant: Tenant = Object.assign({
    name: 'Test',
  });

  it('should get all the profiles in the database', async () => {
    const getProfiles = await controller.listProfiles(
      userProfile,
      tenant,
      2,
      0,
    );
    expect(getProfiles.totalCount).to.equal(2);
  });

  it('should get a profile by id', async () => {
    const profile1 = await controller.showProfileById(
      userProfile,
      tenant,
      profiles[0].id,
    );
    expect(profile1.name).equal(profiles[0].name);
  });

  it('should create a profile successfully when given valid data', async () => {
    kycAmlService.stubs.createApplicant.resolves({
      id: 'test',
      createdAt: new Date().toISOString(),
      providerMeta: {
        typeId: 'test',
        id: 'test',
      },
      address: {
        street1: '',
        postalCode: '',
        city: '',
        region: '',
        country: 'US' as unknown as ISO_Alpha_2,
      },
      dob: '',
      email: '',
      firstName: '',
      identification: [],
      lastName: '',
    });
    kycAmlService.stubs.createCheck.resolves({
      applicantId: '',
      createdAt: new Date().toISOString(),
      providerMeta: {
        typeId: 'test',
        id: 'test',
      },
      status: 'IN_PROGRESS',
    });
    const savedProfile = await controller.createProfile(
      userProfile,
      tenant,
      profileData,
    );

    expect(savedProfile.id).to.not.be.undefined();
    expect(savedProfile.name).equal(profileData.name);
    expect(savedProfile.profileType).equal(profileData.profileType);
    expect(savedProfile.taxDetails?.registrationType).equal(
      profileData.taxDetails?.registrationType,
    );
    expect(savedProfile.stateOfFormation).equal(profileData.stateOfFormation);
    expect(savedProfile.countryOfFormation).equal(
      profileData.countryOfFormation,
    );
    expect(savedProfile.address?.state).equal(profileData.address?.state);
    expect(savedProfile.address?.country).equal(profileData.address?.country);
    expect(savedProfile.typeOfEntity).equal(profileData.typeOfEntity);
    expect(savedProfile.firstName).equal(profileData.firstName);
    expect(savedProfile.lastName).equal(profileData.lastName);
    expect(savedProfile.address?.address1).equal(profileData.address?.address1);
    expect(savedProfile.address?.address2).equal(profileData.address?.address2);
    expect(savedProfile.address?.postalCode).equal(
      profileData.address?.postalCode,
    );
    expect(savedProfile.taxDetails?.taxIdentification?.type).equal(
      profileData.taxDetails?.taxIdentification?.type,
    );
    expect(savedProfile.taxDetails?.taxIdentification?.value).equal(
      profileData.taxDetails?.taxIdentification?.value,
    );
    expect(savedProfile.passportId).equal(profileData.passportId);
    expect(savedProfile.isUSBased).equal(profileData.isUSBased);
  });

  it('should update a profile by ID', async () => {
    await controller.updateProfileById(
      userProfile,
      tenant,
      profiles[0].id,
      profiles[0],
    );
    const updatedProfiles = await controller.listProfiles(
      userProfile,
      tenant,
      2,
      0,
    );
    let updatedName;
    if (updatedProfiles.data) {
      updatedName = updatedProfiles.data[1].name;
    }

    expect(updatedName).equal('Legal Name One');
  });

  it('should delete a profile by ID', async () => {
    await controller.deleteProfileById(userProfile, tenant, profiles[0].id);
    const updatedProfiles = await controller.listProfiles(
      userProfile,
      tenant,
      2,
      0,
    );

    expect(updatedProfiles.data).to.have.length(1);
  });

  // model portion

  it('should have correct enum values', async () => {
    const profilesList = await controller.listProfiles(
      userProfile,
      tenant,
      2,
      0,
    );
    if (profilesList.data) {
      expect(profilesList.data[0].typeOfEntity).to.oneOf([
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
        'S_CORPORATION',
        'GENERAL_PARTNERSHIP',
        'FOREIGN_ENTITY',
      ]);

      expect(profilesList.data[1].typeOfEntity).to.oneOf([
        'LIMITED_LIABILITY_COMPANY',
        'LIMITED_PARTNERSHIP',
        'C_CORPORATION',
        'S_CORPORATION',
        'GENERAL_PARTNERSHIP',
        'FOREIGN_ENTITY',
      ]);

      expect(profilesList.data[0].profileType).to.oneOf([
        'ORGANIZER',
        'INVESTOR',
      ]);

      expect(profilesList.data[1].profileType).to.oneOf([
        'ORGANIZER',
        'INVESTOR',
      ]);

      expect(profilesList.data[0].taxDetails?.registrationType).to.oneOf([
        'ENTITY',
        'INDIVIDUAL',
        'TRUST',
        'JOINT',
      ]);

      expect(profilesList.data[1].taxDetails?.registrationType).to.oneOf([
        'ENTITY',
        'INDIVIDUAL',
        'TRUST',
        'JOINT',
      ]);
    }
  });
});
