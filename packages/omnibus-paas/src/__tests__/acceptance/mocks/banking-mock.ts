// Mocks
import {Banking} from '../../../services';
import {stubInterface} from 'ts-sinon';
import {Provider} from '@loopback/core';

const BankingMock = stubInterface<Banking>();
class BankingProviderMock implements Provider<Banking> {
  value(): Banking {
    return BankingMock;
  }
}

export {BankingMock, BankingProviderMock};
