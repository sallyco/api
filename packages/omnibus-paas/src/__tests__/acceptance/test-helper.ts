import {OmnibusPaasApplication} from '../..';
import {
  createRestAppClient,
  givenHttpServerConfig,
  Client,
} from '@loopback/testlab';
import {TestDataSource} from '../../datasources';
import {CeleryWorkerService} from '../../services';
import {createStubInstance} from '@loopback/testlab/dist/sinon';

export async function setupApplication(): Promise<AppWithClient> {
  const restConfig = givenHttpServerConfig();

  const app = new OmnibusPaasApplication({
    rest: restConfig,
    primaryDatabase: TestDataSource.dataSourceName,
  });

  await app.boot();
  app.bind('datasources.config.mongodb').to({
    name: 'db',
    connector: 'memory',
  });
  app.bind('datasources.config.accounting').to({
    name: 'accounting',
    connector: 'memory',
  });
  app.service(CeleryWorkerService).to(createStubInstance(CeleryWorkerService));

  try {
    await app.start();
  } catch (e) {
    console.error(e);
  }

  const client = createRestAppClient(app);

  return {app, client};
}

export interface AppWithClient {
  app: OmnibusPaasApplication;
  client: Client;
}
