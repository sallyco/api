import {BindingKey} from '@loopback/context';
import {TimerFn, LogFn, LogWriterFn} from './types';

export namespace LoggerBindings {
  export const APP_LOG_LEVEL = BindingKey.create<LOG_LEVEL>('logger.level');
  export const TIMER = BindingKey.create<TimerFn>('logger.timer');
  export const LOGGER = BindingKey.create<LogWriterFn>('logger.logger');
  export const LOG_ACTION = BindingKey.create<LogFn>('logger.action');
}

export const LOGGER_METADATA = 'logger.metadata';

export enum LOG_LEVEL {
  DEBUG,
  INFO,
  WARN,
  ERROR,
  OFF,
}
