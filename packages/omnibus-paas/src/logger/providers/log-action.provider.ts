import {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Constructor,
  Getter,
  inject,
  Provider,
  CoreBindings,
} from '@loopback/core';
import {OperationArgs, Request} from '@loopback/rest';
import chalk from 'chalk';
import {getLogMetadata} from '../decorators';
import {LoggerBindings, LOG_LEVEL} from '../keys';
import {
  HighResTime,
  LevelMetadata,
  LogFn,
  LogWriterFn,
  TimerFn,
} from '../types';

class StaticController {}

export class LogActionProvider implements Provider<LogFn> {
  // LogWriteFn is an optional dependency and it falls back to `logToConsole`
  @inject(LoggerBindings.LOGGER, {optional: true})
  writeLog: LogWriterFn = logToConsole;

  @inject(LoggerBindings.APP_LOG_LEVEL, {optional: true})
  logLevel: LOG_LEVEL = LOG_LEVEL[process.env.LOG_LEVEL ?? 'INFO'];

  constructor(
    @inject.getter(CoreBindings.CONTROLLER_CLASS, {optional: true})
    private readonly getController: Getter<Constructor<{}>>,
    @inject.getter(CoreBindings.CONTROLLER_METHOD_NAME, {optional: true})
    private readonly getMethod: Getter<string>,
    @inject(LoggerBindings.TIMER) public timer: TimerFn,
  ) {}

  value(): LogFn {
    const fn = <LogFn>((
      req: Request,
      args: OperationArgs,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      result: any,
      start?: HighResTime,
    ) => {
      return this.action(req, args, result, start);
    });

    fn.startTimer = () => {
      return this.timer();
    };

    return fn;
  }

  private async action(
    req: Request,
    args: OperationArgs,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    result: any,
    start?: HighResTime,
  ): Promise<void> {
    let controllerClass = this.getController && (await this.getController());
    let methodName: string = this.getMethod && (await this.getMethod());
    controllerClass =
      typeof controllerClass === 'undefined'
        ? StaticController
        : controllerClass;
    methodName = typeof methodName === 'undefined' ? 'static' : methodName;

    const metadata: LevelMetadata = getLogMetadata(controllerClass, methodName);
    const level: number | undefined = metadata ? metadata.level : undefined;

    if (
      level !== undefined &&
      this.logLevel !== LOG_LEVEL.OFF &&
      level >= this.logLevel &&
      level !== LOG_LEVEL.OFF
    ) {
      if (!args) args = [];
      let msg = `[${new Date().toISOString()}] ${req.method} ${req.url} :: ${
        controllerClass.name
      }.`;
      msg += `${methodName}(${args.join(', ')}) => `;

      // filter certain types for logging
      function replacer(name, val) {
        if (
          val?.toString()?.startsWith('data:image/') &&
          val?.toString()?.length > 64
        ) {
          return val.toString().substring(0, 64) + '[...]';
        } else if (name === 'ssn') {
          //
          return undefined;
        } else {
          return val;
        }
      }

      if (typeof result === 'object') msg += JSON.stringify(result, replacer);
      else msg += result;

      if (start) {
        const timeDiff: HighResTime = this.timer(start);
        const time: number =
          timeDiff[0] * 1000 + Math.round(timeDiff[1] * 1e-4) / 100;
        msg = `${time}ms: ${msg}`;
      }

      this.writeLog(msg, level);
    }
  }
}

function logToConsole(msg: string, level: number) {
  let output;
  switch (level) {
    case LOG_LEVEL.DEBUG:
      output = chalk.cyan(`DEBUG: ${msg}`);
      break;
    case LOG_LEVEL.INFO:
      output = chalk.green(`INFO: ${msg}`);
      break;
    case LOG_LEVEL.WARN:
      output = chalk.yellow(`WARN: ${msg}`);
      break;
    case LOG_LEVEL.ERROR:
      output = chalk.red(`ERROR: ${msg}`);
      break;
  }
  if (output) console.log(output);
}
