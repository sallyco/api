import {Component, ProviderMap} from '@loopback/core';
import {LoggerBindings} from './keys';
import {LogActionProvider, TimerProvider} from './providers';

export class LogComponent implements Component {
  providers?: ProviderMap = {
    [LoggerBindings.TIMER.key]: TimerProvider,
    [LoggerBindings.LOG_ACTION.key]: LogActionProvider,
  };
}
