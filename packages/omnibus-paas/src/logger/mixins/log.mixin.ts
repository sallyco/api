import {MixinTarget, Application} from '@loopback/core';
import {LogComponent} from '../component';
import {LoggerBindings, LOG_LEVEL} from '../keys';

export function LogMixin<T extends MixinTarget<Application>>(superClass: T) {
  return class extends superClass {
    // A mixin class has to take in a type any[] argument!
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args: any[]) {
      super(...args);
      if (this.options?.logLevel) {
        this.logLevel(this.options.logLevel);
      }
      this.component(LogComponent);
    }

    logLevel(level: LOG_LEVEL) {
      this.bind(LoggerBindings.APP_LOG_LEVEL).to(level);
    }
  };
}
