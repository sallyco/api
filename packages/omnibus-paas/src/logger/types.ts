import {Request, OperationArgs} from '@loopback/rest';

export interface LogFn {
  (
    req: Request,
    args: OperationArgs,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    result: any,
    startTime?: HighResTime,
  ): Promise<void>;

  startTimer(): HighResTime;
}

export type LevelMetadata = {level: number};

export type HighResTime = [number, number]; // [seconds, nanoseconds]
export type LogWriterFn = (msg: string, level: number) => void;
export type TimerFn = (start?: HighResTime) => HighResTime;
