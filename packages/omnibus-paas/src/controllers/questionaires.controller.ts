import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  put,
  del,
  requestBody,
  response,
  tags,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/core';
import debugFactory from 'debug';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {Questionaire, QuestionaireWithRelations} from '../models';
import {QuestionaireRepository} from '../repositories';

const debug = debugFactory('omnibus-paas:controllers:questionaires');

@tags('Questions')
@visibility(OperationVisibility.UNDOCUMENTED)
export class QuestionairesController {
  constructor(
    @repository(QuestionaireRepository)
    public questionaireRepository: QuestionaireRepository,
  ) {}

  @authenticate('jwt')
  @post('/questions/questionaires')
  @response(200, {
    description: 'Questionaire model instance',
  })
  async create(
    @requestBody() questionQuestionaire: Omit<Questionaire, 'id'>,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Questionaire> {
    const {questions, ...rest} = questionQuestionaire;
    const pack = await this.questionaireRepository.create(rest);

    //questions
    if (questions) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newpro = questions.map((entry: any) => {
        return this.questionaireRepository.questions(pack.id).link(entry);
      });

      await Promise.all(newpro);
    }

    return pack;
  }

  @get('/questions/questionaires/count')
  @response(200, {
    description: 'Questionaire model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Questionaire) where?: Where<Questionaire>,
  ): Promise<Count> {
    return this.questionaireRepository.count(where);
  }

  @get('/questions/questionaires')
  @response(200, {
    description: 'Array of Questionaire model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Questionaire, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Questionaire) filter?: Filter<Questionaire>,
  ): Promise<QuestionaireWithRelations[]> {
    return this.questionaireRepository.find(filter);
  }

  @get('/questions/questionaires/{id}')
  @response(200, {
    description: 'Questionaire model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Questionaire, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Questionaire, {exclude: 'where'})
    filter?: FilterExcludingWhere<Questionaire>,
  ): Promise<QuestionaireWithRelations> {
    return this.questionaireRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @put('/questions/questionaires/{id}')
  @response(204, {
    description: 'Questionaire PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() questionQuestionaire: QuestionaireWithRelations,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    try {
      await this.questionaireRepository
        .questions(questionQuestionaire.id)
        .unlinkAll();
    } catch {
      debug("can't unlink on undefined hasMany ", questionQuestionaire.id);
    }

    let pack;
    //questions
    if (questionQuestionaire.questions) {
      const {questions, ...rest} = questionQuestionaire;
      pack = rest;

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newpro = questions.map((entry: any) => {
        return this.questionaireRepository
          .questions(questionQuestionaire.id)
          .link(entry);
      });

      await Promise.all(newpro);
    } else {
      pack = questionQuestionaire;
    }

    await this.questionaireRepository.replaceById(id, pack);
  }

  @authenticate('jwt')
  @del('/questions/questionaires/{id}')
  @response(204, {
    description: 'Questionaire DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.questionaireRepository.questions(id).unlinkAll();

    await this.questionaireRepository.deleteById(id);
  }
}
