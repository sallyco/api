import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Transaction, Subscription} from '../models';
import {TransactionRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class TransactionSubscriptionController {
  constructor(
    @repository(TransactionRepository)
    public transactionRepository: TransactionRepository,
  ) {}

  @get('/transactions/{id}/subscription', {
    responses: {
      '200': {
        description: 'Subscription belonging to Transaction',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Subscription)},
          },
        },
      },
    },
  })
  async getSubscription(
    @param.path.string('id') id: typeof Transaction.prototype.id,
  ): Promise<Subscription> {
    return this.transactionRepository.subscription(id);
  }
}
