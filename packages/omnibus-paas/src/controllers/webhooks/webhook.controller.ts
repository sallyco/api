import {repository} from '@loopback/repository';
import {
  getModelSchemaRef,
  operation,
  param,
  requestBody,
  tags,
} from '@loopback/rest';
import {Webhook} from '../../models';
import {WebhookRepository} from '../../repositories';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {inject} from '@loopback/core';
import {createHash} from 'crypto';
import {authenticate} from '@loopback/authentication';
import List from '../../models/list.model';

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by Webhooks
 * Webhooks
 */
@tags('Webhooks')
export class WebhooksController {
  constructor(
    @repository(WebhookRepository)
    public webhookRepository: WebhookRepository,
  ) {}

  /**
   * Process a webhook
   *

   * @returns Expected response to a valid request
   * @param webhook
   */
  @authenticate('jwt')
  @operation('post', '/webhooks', {
    operationId: 'createWebhook',
    summary: 'Create Webhook',
    description: `You can create a webhook to subscribe to event updates on
     various resources and events using a POST call to an endpoint you specify.
     
     When creating webhooks, you can supply the \`token\` value to validate that the calls you are receiving
     are valid. POST calls from our system will contain a header \`X-Webhook-Token\` that will be set to the \`SHA256\`
     value of the token.
     
     Currently, only the following resource IDs are supported: 
     
     - legal/formation/requests
     - banking/account
     
     If you need an additional resource available via Webhook, please contact support`,
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected Response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Webhook, {exclude: ['_id']}),
            examples: {
              'example-1': {
                description: 'Example Creation for Webhook',
                value: {
                  id: '614a10cc0bad65fde41e0670',
                  token: 'z4DNiu1ILV0VJ9fccvzv+E5jJlkoSER9LcCw6H38mpA=',
                  url: 'https://example.com/incoming-webhook',
                  enabled: true,
                  resources: [
                    {
                      events: ['CREATE', 'UPDATE', 'DELETE'],
                      id: 'legal/formation/requests',
                    },
                    {
                      events: ['CREATE', 'UPDATE'],
                      id: 'banking/account',
                    },
                  ],
                },
              },
            },
          },
        },
      },
    },
  })
  async newWebhook(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Webhook),
          examples: {
            'example-1': {
              description: 'Example Webhook Creation Request',
              value: {
                url: 'https://example.com/incoming-webhook',
                token: 'MySecretToken1234%!',
                enabled: true,
                resources: [
                  {
                    events: ['CREATE', 'UPDATE', 'DELETE'],
                    id: 'legal/formation/requests',
                  },
                  {
                    events: ['CREATE', 'UPDATE'],
                    id: 'banking/account',
                  },
                ],
              },
            },
          },
        },
      },
    })
    webhook: Webhook,
  ): Promise<Webhook> {
    const hashedToken = createHash('sha256')
      .update(webhook.token)
      .digest('base64');
    return this.webhookRepository.create({
      ...webhook,
      token: hashedToken,
    });
  }

  /**
   * Get all webhooks
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/webhooks', {
    operationId: 'getWebhooks',
    summary: 'List Webhooks',
    description: 'List Webhooks for the current account',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected Response to a valid request',
        content: {
          'application/json': {
            schema: {
              ...getModelSchemaRef(List),
              ...{
                properties: {
                  data: {
                    type: 'array',
                    itemType: getModelSchemaRef(Webhook),
                  },
                },
              },
            },
            examples: {
              'example-1': {
                description: 'Example Listing for Webhooks',
                value: [
                  {
                    id: '614a10cc0bad65fde41e0670',
                    url: 'https://example.com/incoming-webhook',
                    enabled: true,
                    resources: [
                      {
                        events: ['CREATE', 'UPDATE', 'DELETE'],
                        id: 'legal/formation/requests',
                      },
                      {
                        events: ['CREATE', 'UPDATE'],
                        id: 'banking/account',
                      },
                    ],
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async listWebhooks(
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    tenant: Tenant,
  ): Promise<Webhook[]> {
    return this.webhookRepository.find({
      where: {
        tenantId: tenant.id,
      },
    });
  }

  /**
   * Get single webhook by Id
   * @param id
   */
  @authenticate('jwt')
  @operation('get', '/webhooks/{id}', {
    operationId: 'getWebhook',
    summary: 'Get Webhook By Id',
    description: `Returns a stored webhook. Note that the \`token\` will be returned as the \`SHA-256\` hashed value`,
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected Response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Webhook, {exclude: ['_id']}),
            examples: {
              'example-1': {
                description: 'Example Response for Webhook',
                value: {
                  id: '614a10cc0bad65fde41e0670',
                  token: 'z4DNiu1ILV0VJ9fccvzv+E5jJlkoSER9LcCw6H38mpA=',
                  url: 'https://example.com/incoming-webhook',
                  enabled: true,
                  resources: [
                    {
                      events: ['CREATE', 'UPDATE', 'DELETE'],
                      id: 'legal/formation/requests',
                    },
                    {
                      events: ['CREATE', 'UPDATE'],
                      id: 'banking/account',
                    },
                  ],
                },
              },
            },
          },
        },
      },
      '404': {
        description: 'Webhook ID not found',
      },
    },
  })
  async getWebhookById(
    @param({name: 'id', in: 'path'}) id: string,
  ): Promise<Webhook> {
    return this.webhookRepository.findById(id);
  }

  /**
   * Delete single webhook by Id
   * @param id
   */
  @authenticate('jwt')
  @operation('delete', '/webhooks/{id}', {
    operationId: 'deleteWebhook',
    summary: 'Delete Webhook By Id',
    description: `Deletes the specified webhook`,
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '204': {
        description: 'Expected Response to a valid request',
      },
      '404': {
        description: 'Webhook ID not found',
      },
    },
  })
  async deleteWebhookById(
    @param({name: 'id', in: 'path'}) id: string,
  ): Promise<void> {
    await this.webhookRepository.deleteById(id);
  }
}
