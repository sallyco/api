import {Context, inject, service} from '@loopback/core';
import {
  oas,
  operation,
  visibility,
  OperationVisibility,
  requestBody,
  RequestContext,
} from '@loopback/rest';
import {repository} from '@loopback/repository';
import {
  DealRepository,
  EntitiesRepository,
  FileRepository,
} from '../repositories';
import {Entities, File} from '../models';
import {Banking, BankingProvider, LegalInc, MinIOService} from '../services';
import {ILegalInc} from '../interfaces/services/legal-inc.interface';
import {v4 as uuidv4} from 'uuid';
import axios from 'axios';
import {PassThrough} from 'stream';
import debugFactory from 'debug';
import {MultiTenancyBindings} from '../multi-tenancy';

const debug = debugFactory('omnibus-paas:webhooks:controller');

@visibility(OperationVisibility.UNDOCUMENTED)
export class IncomingWebhookController {
  constructor(
    @repository(EntitiesRepository)
    public entityRepository: EntitiesRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @inject.context()
    public context: RequestContext,
  ) {}

  @oas.tags('TenantNotRequired')
  @operation('PUT', '/incoming-webhook/legalinc/order')
  async updateLegalIncEntityOrder(
    @inject('services.LegalInc') legalInc: LegalInc,
    @service(BankingProvider) bankingService: Banking,
    @requestBody() request: ILegalInc.WebHooks.Order,
  ): Promise<boolean> {
    // TODO: Fix this, needs to deep search entity models for orderID
    // TODO: HUGE potential performance hit here once we get going
    try {
      const entities = await this.entityRepository.find({});
      const entity = entities.find((item: Entities) => {
        return item.legalIncOrder?.orderId === Number(request.id);
      });
      if (entity?.legalIncOrder) {
        entity.legalIncOrder.status =
          ILegalInc.API.OrderStatus[request.status_id];
        if (
          ILegalInc.API.OrderStatus[entity.legalIncOrder.status] ===
          ILegalInc.API.OrderStatus.COMPLETED
        ) {
          const legalIncTokenResponse = await legalInc.getToken(
            process.env.LEGALINC_CLIENT_ID ?? '51270',
            process.env.LEGALINC_CLIENT_SECRET ?? 'xHtIgBxVzuKkUo',
            process.env.LEGALINC_USERNAME ?? 'tcoffelt@glassboardtech.com',
            process.env.LEGALINC_PASSWORD ?? 'yN2!jK1}kC',
          );

          const {response} = await legalInc.getOrder(
            legalIncTokenResponse.access_token,
            request.id,
          );
          const order = JSON.parse(response);
          if (order.company.ein) {
            entity.ein = order.company.ein;
          }
          await this.entityRepository.update(entity);
          entity.bankAccount = await bankingService.createEntityDepositAccount(
            entity,
          );
          await this.entityRepository.update(entity);
        }
      }
      debug(`legalinc order ${request.id} updated`);
    } catch (e) {
      debug('legalinc order error', e);
    }
    return true;
  }

  @oas.tags('TenantNotRequired')
  @operation('POST', '/incoming-webhook/legalinc/documents')
  async updateDealDocuments(
    @inject('services.LegalInc') legalInc: LegalInc,
    @service(MinIOService) minIOService: MinIOService,
    @requestBody() request: ILegalInc.WebHooks.Document,
    @inject.context() ctx: Context,
  ): Promise<boolean> {
    try {
      const entities = await this.entityRepository.find({});
      const entity = entities.find((item: Entities) => {
        return item.legalIncOrder?.orderId === Number(request.order_id);
      });
      if (entity?.legalIncOrder) {
        const deal = await this.dealRepository.findById(entity.dealId);
        ctx
          .bind(MultiTenancyBindings.CURRENT_TENANT)
          .to({id: String(entity.tenantId)});
        const passThroughStream = new PassThrough();
        const responseStream = await axios.get(request.file_url, {
          responseType: 'stream',
        });
        const fileKey = uuidv4();
        const MinioPromise = minIOService.putObject(
          fileKey,
          passThroughStream,
          {
            'Content-Type': responseStream.headers['content-type'],
            'Content-Disposition': 'attachment; filename=' + request.file_name,
          },
        );
        responseStream.data.pipe(passThroughStream);
        await MinioPromise;
        let file = new File({
          name: request.file_name,
          type: request.file_name.split('.').pop(),
          ownerId: deal.ownerId,
          key: fileKey,
          lastModified: Date.now().toString(),
        });
        file = await this.fileRepository.save(file);
        deal.files = deal.files ?? [];
        deal.files.push(file.id);
        await this.dealRepository.updateById(deal.id, deal);
      }
      debug(`legalinc order document for order ${request.order_id} added`);
    } catch (e) {
      debug('legalinc document error', e);
    }
    return true;
  }
}
