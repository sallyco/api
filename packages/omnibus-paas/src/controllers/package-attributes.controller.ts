import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  tags,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {PackageAttribute} from '../models';
import {PackageAttributeRepository} from '../repositories';

@tags('Products')
@visibility(OperationVisibility.UNDOCUMENTED)
export class PackageAttributesController {
  constructor(
    @repository(PackageAttributeRepository)
    public packageAttributeRepository: PackageAttributeRepository,
  ) {}

  @authenticate('jwt')
  @post('/products/packages/attributes')
  @response(200, {
    description: 'PackageAttribute model instance',
    content: {
      'application/json': {schema: getModelSchemaRef(PackageAttribute)},
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(PackageAttribute, {
            title: 'NewPackageAttribute',
            exclude: ['id'],
          }),
        },
      },
    })
    packageAttribute: Omit<PackageAttribute, 'id'>,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<PackageAttribute> {
    return this.packageAttributeRepository.create(packageAttribute);
  }

  @get('/products/packages/attributes/count')
  @response(200, {
    description: 'PackageAttribute model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(PackageAttribute) where?: Where<PackageAttribute>,
  ): Promise<Count> {
    return this.packageAttributeRepository.count(where);
  }

  @get('/products/packages/attributes')
  @response(200, {
    description: 'Array of PackageAttribute model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(PackageAttribute, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(PackageAttribute) filter?: Filter<PackageAttribute>,
  ): Promise<PackageAttribute[]> {
    return this.packageAttributeRepository.find(filter);
  }

  @get('/products/packages/attributes/{id}')
  @response(200, {
    description: 'PackageAttribute model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(PackageAttribute, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(PackageAttribute, {exclude: 'where'})
    filter?: FilterExcludingWhere<PackageAttribute>,
  ): Promise<PackageAttribute> {
    return this.packageAttributeRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @patch('/products/packages/attributes/{id}')
  @response(204, {
    description: 'PackageAttribute PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(PackageAttribute, {partial: true}),
        },
      },
    })
    packageAttribute: PackageAttribute,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.packageAttributeRepository.updateById(id, packageAttribute);
  }

  @authenticate('jwt')
  @put('/products/packages/attributes/{id}')
  @response(204, {
    description: 'PackageAttribute PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() packageAttribute: PackageAttribute,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.packageAttributeRepository.replaceById(id, packageAttribute);
  }

  @authenticate('jwt')
  @del('/products/packages/attributes/{id}')
  @response(204, {
    description: 'PackageAttribute DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.packageAttributeRepository.deleteById(id);
  }
}
