/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  getModelSchemaRef,
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  requestBody,
  tags,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import debugFactory from 'debug';
import KcAdminClient from 'keycloak-admin';
import {Invite, Subscription} from '../models';
import {InvitesList} from '../models/invites-list.model';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  DealRepository,
  InviteRepository,
  ProfileRepository,
  SubscriptionRepository,
} from '../repositories';

const debug = debugFactory('omnibus-paas:controllers:invite');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by Invites
 * Everything about your invites
 */
@visibility(OperationVisibility.UNDOCUMENTED)
@tags('Invites')
export class InvitesController {
  constructor(
    @repository(InviteRepository)
    public inviteRepository: InviteRepository,
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository, //    public user: UsersController,
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
  ) {}

  /**
   * List current user's invtes to which the authentication method has access.
   *

   * @param limit A limit on the number of objects to be returned.  Limit can range between 1 and 100, the default is 10.
   * @param startingIndex A cursor for use in pagination. `startingIndex` is an object ID that define your place in the list.
   * @returns Successful request, returns a paged array of invites
  */
  @authenticate('jwt')
  @operation('get', '/invites')
  async listInvites(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<InvitesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const countObj = await this.inviteRepository.count({
      and: [{isDeleted: false}, {ownerId: profile.id}, {tenantId: tenant.id}],
    });

    return new InvitesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.inviteRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: {
          and: [
            {isDeleted: false},
            {ownerId: profile.id},
            {tenantId: tenant.id},
          ],
        },
      }),
    });
  }

  @authenticate('jwt')
  @operation('get', '/invites-by-deal/{dealId}')
  async listInvitesByDeal(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<InvitesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const invites = await this.inviteRepository.find({
      where: {
        and: [
          {isDeleted: false},
          {ownerId: profile.id},
          {tenantId: tenant.id},
          {
            acceptedAt: {
              exists: false,
            },
          },
        ],
      },
    });

    const filteredInvites = invites.filter(element =>
      element.invitedTo.some(subElement => subElement.id === dealId),
    );

    const countObj = await this.inviteRepository.count({
      and: [
        {isDeleted: false},
        {ownerId: profile.id},
        {tenantId: tenant.id},
        {
          acceptedAt: {
            exists: false,
          },
        },
      ],
    });

    return new InvitesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: filteredInvites,
    });
  }

  /**
   * Create a single account by its id
   *

  */
  @visibility(OperationVisibility.DOCUMENTED)
  @authenticate('jwt')
  @operation('post', '/invites', {
    operationId: 'createInvite',
    summary: 'Create Invite Object',
    description: 'Create Invite Object',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Invite),
            examples: {
              'example-1': {
                description: 'Example Invite Object',
                value: [
                  {
                    id: '60381e064b79321012eac5b7',
                    ownerId: '483f401f-cfae-4972-b62c-e3c9d7472585',
                    tenantId: 'gbt',
                    acceptableBy: 'john@example.com',
                    invitedTo: [
                      {
                        id: '60381e06fb79320012e8c5b3',
                        type: 'deal',
                        relation: 'subscription',
                      },
                    ],
                    invitedRole: 'investor',
                    createdAt: '2021-02-25T22:03:47.727Z',
                    updatedAt: '2021-02-25T22:15:29.276Z',
                    isDeleted: false,
                    acceptedAt: '2021-02-25T22:15:29.276Z',
                    redirectUrl: '/subscriptions/60382181fb79320012e8c5b6',
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async createInvite(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invite, {
            title: 'NewInvite',
            exclude: [
              'id',
              'ownerId',
              'tenantId',
              'createdAt',
              'updatedAt',
              'acceptedAt',
              'deletedAt',
              'isDeleted',
            ],
          }),
          examples: {
            'example-1': {
              description: 'Example Invite Object',
              value: [
                {
                  acceptableBy: 'john@example.com',
                  invitedTo: [
                    {
                      id: '60381e06fb79320012e8c5b3',
                      type: 'deal',
                      relation: 'subscription',
                    },
                  ],
                  invitedRole: 'investor',
                  redirectUrl: '/subscriptions/60382181fb79320012e8c5b6',
                },
              ],
            },
          },
        },
      },
    })
    invite: Invite,
  ): Promise<Invite> {
    if (tenant.id !== 'master') {
      invite.tenantId = tenant.id;
    }
    invite.ownerId = profile.id;
    if (invite.invitedTo[0].type === 'user') {
      invite.invitedTo[0].id = profile.id;
    }
    return this.inviteRepository.create(invite);
  }

  /**
   * Get a single account by its id
   *

   * @param inviteId Unique identifier of the invite to retrieve
   * @returns Expected response to a valid request
   */
  @operation('get', '/invites/{id}')
  async readInviteById(
    //@inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'id', in: 'path'}) inviteId: string,
  ): Promise<Invite> {
    const invite = await this.inviteRepository.findById(inviteId);
    //  if (!invite.isDeleted && invite.ownerId === profile.id) {
    return invite;
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The invite you are trying to find cannot be found.',
    //  );
    //}
  }

  /**
    * Update a single invite by its id
    *

    * @param inviteId Unique identifier of the invite to retrieve
    * @returns Expected response to a valid request
  */
  @authenticate('jwt')
  @operation('put', '/invites/{inviteId}')
  async updateInviteById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'inviteId', in: 'path'}) inviteId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
      },
    })
    invite: Invite,
  ): Promise<Invite> {
    const retrievedInvite = await this.inviteRepository.findById(inviteId);

    if (
      !retrievedInvite.isDeleted &&
      invite.ownerId === profile.id &&
      invite.tenantId === tenant.id
    ) {
      invite.updatedAt = new Date();
      await this.inviteRepository.updateById(inviteId, invite);
      return this.inviteRepository.findById(inviteId);
    } else {
      throw new HttpErrors.NotFound(
        'The invite you are trying to update cannot be found.',
      );
    }
  }

  /**
    * Delete a single invite by its id
    *

    * @param inviteId Unique identifier of the invite to retrieve
    * @returns Expected response to a valid request
  */
  @authenticate('jwt')
  @operation('delete', '/invites/{inviteId}')
  async softDeleteById(
    @param({name: 'inviteId', in: 'path'}) inviteId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const invite = await this.inviteRepository.findById(inviteId);

    if (
      invite.isDeleted ||
      (invite.ownerId !== profile.id && invite.tenantId !== tenant.id)
    ) {
      throw new HttpErrors.NotFound(
        'The invite you are trying to delete cannot be found.',
      );
    } else {
      await this.inviteRepository.updateById(inviteId, {
        deletedAt: new Date(),
        isDeleted: true,
      });
    }
  }

  /**
   * Update a single invite by its id with the acceptor
   * @param inviteId Unique identifier of the invite to retrieve
   * @param userId
   * @param tenant
   * @param tenantId
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.DOCUMENTED)
  @operation('post', '/invites/{inviteId}/accept', {
    operationId: 'inviteAccept',
    summary: 'Accept Invite Object',
    description: 'Accept Invite Object',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Invite),
            examples: {
              'example-1': {
                description: 'Example Invite Object',
                value: [
                  {
                    id: '60381e064b79321012eac5b7',
                    ownerId: '483f401f-cfae-4972-b62c-e3c9d7472585',
                    tenantId: 'gbt',
                    acceptableBy: 'john@example.com',
                    invitedTo: [
                      {
                        id: '60381e06fb79320012e8c5b3',
                        type: 'deal',
                        relation: 'subscription',
                      },
                    ],
                    invitedRole: 'investor',
                    createdAt: '2021-02-25T22:03:47.727Z',
                    updatedAt: '2021-02-25T22:15:29.276Z',
                    isDeleted: false,
                    acceptedAt: '2021-02-25T22:15:29.276Z',
                    redirectUrl: '/subscriptions/60382181fb79320012e8c5b6',
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async acceptInviteById(
    @param({
      name: 'inviteId',
      in: 'path',
      description: 'Invite Object ID',
      required: true,
    })
    inviteId: string,
    @param({
      name: 'userId',
      in: 'query',
      description: 'User ID of acceptor',
      required: true,
    })
    userId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({
      in: 'header',
      name: 'x-tenant-id',
      required: true,
      description: 'Your Tenant ID',
      schema: {
        type: 'string',
      },
    })
    tenantId: string,
  ): Promise<Invite> {
    const invite = await this.inviteRepository.findById(inviteId);
    await this.inviteRepository.updateById(inviteId, {
      updatedAt: new Date(),
      acceptedAt: new Date(),
    });

    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
    });

    debug('userId', userId);
    const profile = await kcAdminClient.users.findOne({
      id: userId,
      realm: tenant.id,
    });

    if (!invite.isDeleted) {
      switch (invite.invitedTo[0].type) {
        case 'deal': {
          let subscription: Subscription | null =
            await this.subscriptionRepository.findOne({
              where: {
                and: [
                  {isDeleted: false},
                  {ownerId: userId},
                  {dealId: invite.invitedTo[0].id},
                ],
              },
            });

          if (!subscription) {
            const subscriptionData = new Subscription({
              email: profile.username,
              name: `${profile.firstName} ${profile.lastName}`,
              ownerId: profile.id,
              dealId: invite.invitedTo[0].id,
              tenantId: tenant.id,
              acquisitionMethod: 'INVITATION',
            });
            subscription = await this.subscriptionRepository.create(
              subscriptionData,
            );
          }
          await this.inviteRepository.updateById(inviteId, {
            redirectUrl: `/subscriptions/${subscription.id}`,
          });
          break;
        }
        case 'user': {
          break;
        }
        case 'profile': {
          // Purpose - Update the multi-signer profile's additionalSignatories to contain the ownerId

          // Get multi-sign profile and signer email from invite information
          const multiSignersProfileId = invite.invitedTo[0].id;
          const signerEmail = invite.acceptableBy;

          let multiSignerProfile;
          try {
            multiSignerProfile = await this.profileRepository.findById(
              multiSignersProfileId,
            );
          } catch (err) {
            throw new HttpErrors.NotFound(
              'MultiSign Investor Profile was not found',
            );
          }

          if (
            multiSignerProfile?.additionalSignatories === undefined ||
            multiSignerProfile.additionalSignatories.length <= 0
          ) {
            throw new HttpErrors.NotFound(
              'No additional Signatories were found on this profile',
            );
          }

          // Connect using the provided email
          const individual = multiSignerProfile.additionalSignatories.findIndex(
            signer => signer.email === signerEmail,
          );

          // Throw error if we dont' find this signer
          if (individual === -1 || individual === undefined) {
            throw new HttpErrors.NotFound(
              'Additional signatory does not exist with the provided email',
            );
          }

          multiSignerProfile.additionalSignatories[individual]['ownerId'] =
            userId;

          try {
            await this.profileRepository.updateById(multiSignersProfileId, {
              additionalSignatories: multiSignerProfile.additionalSignatories,
            });
          } catch (err) {
            throw new HttpErrors.InternalServerError('Signatory update failed');
          }
          break;
        }
        default:
          break;
      }

      return this.inviteRepository.findById(inviteId);
    } else {
      throw new HttpErrors.NotFound(
        'The invite you are trying to accept cannot be found.',
      );
    }
  }
}
