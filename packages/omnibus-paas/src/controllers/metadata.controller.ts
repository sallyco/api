import {EntityNotFoundError} from '@loopback/repository';
import {
  getJsonSchemaRef,
  operation,
  param,
  visibility,
  OperationVisibility,
} from '@loopback/rest';

import * as Models from '../models';

function findAction(
  action: string,
  key: string,
  createModel,
  readModel,
  updateModel,
) {
  switch (action) {
    case 'create':
      return typeof createModel === 'function'
        ? getJsonSchemaRef(createModel)
        : createModel;
    case 'read':
      return typeof readModel === 'function'
        ? getJsonSchemaRef(readModel)
        : readModel;
    case 'update':
      return typeof updateModel === 'function'
        ? getJsonSchemaRef(updateModel)
        : updateModel;
    default:
      throw new EntityNotFoundError('metadata', {key, action});
  }
}
@visibility(OperationVisibility.UNDOCUMENTED)
export class MetadataController {
  constructor() {}

  @operation('get', '/metadata/{key}/ui')
  async findUiByKey(@param.path.string('key') key: string): Promise<object> {
    return {};
  }
  @operation('get', '/metadata/{key}/{action}/ui')
  async findUiByKeyAndAction(
    @param.path.string('key') key: string,
    @param.path.string('action') action: string,
  ): Promise<object> {
    return {};
  }

  @operation('get', '/metadata/{key}/describe')
  async findByKey(@param.path.string('key') key: string): Promise<object> {
    switch (key) {
      case 'tenant':
        return getJsonSchemaRef(Models.Tenant);
      case 'deal':
        return getJsonSchemaRef(Models.Deal);
      case 'entitiy':
        return Models.EntitiesSchema;
      case 'profile':
        return getJsonSchemaRef(Models.Profile);
      case 'global':
        return getJsonSchemaRef(Models.Global);
      case 'question':
        return getJsonSchemaRef(Models.Question);
      case 'questionaire':
        return getJsonSchemaRef(Models.Questionaire);
      case 'questionaire-response':
        return getJsonSchemaRef(Models.QuestionaireResponse);
      case 'fees-statement':
        return getJsonSchemaRef(Models.FeesStatement);
      case 'product':
        return Models.ProductSchema;
      case 'package':
        return Models.PackageSchema;
      case 'product-category':
        return Models.ProductCategorySchema;
      case 'package-attribute':
        return Models.PackageAttributeSchema;
      case 'blue-sky-fee':
        return getJsonSchemaRef(Models.BlueSkyFee);
      case 'form-d':
        return getJsonSchemaRef(Models.FormD);
      case 'white-label':
        return getJsonSchemaRef(Models.WhiteLabel);
      default:
        throw new EntityNotFoundError('metadata', key);
    }
  }

  @operation('get', '/metadata/{key}/{action}/describe')
  async findByKeyAndAction(
    @param.path.string('key') key: string,
    @param.path.string('action') action: string,
  ): Promise<object> {
    switch (key) {
      case 'global':
        return findAction(
          action,
          key,
          Models.GlobalCreate, //createModel
          Models.Global, //readModel
          Models.Global, //updateModel
        );
      case 'question':
        return findAction(
          action,
          key,
          Models.QuestionCreateSchema,
          Models.QuestionSchema,
          Models.QuestionUpdateSchema,
        );
      case 'questionaire':
        return findAction(
          action,
          key,
          await Models.QuestionaireCreateSchema(),
          Models.QuestionaireSchema,
          await Models.QuestionaireUpdateSchema(),
        );
      case 'questionaire-response':
        return findAction(
          action,
          key,
          Models.QuestionaireResponseCreateSchema,
          Models.QuestionaireResponseSchema,
          Models.QuestionaireResponseUpdateSchema,
        );
      case 'product':
        return findAction(
          action,
          key,
          await Models.ProductCreateSchema(),
          Models.ProductSchema,
          await Models.ProductUpdateSchema(),
        );
      case 'package':
        return findAction(
          action,
          key,
          await Models.PackageCreateSchema(),
          Models.PackageSchema,
          await Models.PackageUpdateSchema(),
        );
      case 'product-category':
        return findAction(
          action,
          key,
          Models.ProductCategoryCreateSchema,
          Models.ProductCategorySchema,
          Models.ProductCategoryUpdateSchema,
        );
      case 'package-attribute':
        return findAction(
          action,
          key,
          Models.PackageAttributeCreateSchema,
          Models.PackageAttributeSchema,
          Models.PackageAttributeUpdateSchema,
        );
      case 'company':
        return findAction(
          action,
          key,
          Models.CompanyCreate,
          Models.Company,
          Models.Company,
        );
      case 'deal':
        return findAction(
          action,
          key,
          Models.DealCreateSchema,
          Models.DealSchema,
          await Models.DealUpdateSchema(),
        );
      case 'subscription':
        return findAction(
          action,
          key,
          Models.Subscription,
          Models.Subscription,
          Models.SubscriptionUpdate,
        );
      case 'blue-sky-fee':
        return findAction(
          action,
          key,
          Models.BlueSkyFee,
          Models.BlueSkyFee,
          Models.BlueSkyFeeUpdate,
        );
      case 'form-d':
        return findAction(
          action,
          key,
          Models.FormD,
          Models.FormD,
          Models.FormD,
        );
      case 'white-label':
        return findAction(
          action,
          key,
          Models.WhiteLabel,
          Models.WhiteLabel,
          Models.WhiteLabelUpdate,
        );
      case 'tenant':
        return findAction(
          action,
          key,
          Models.Tenant,
          Models.Tenant,
          Models.TenantUpdate,
        );
      case 'tenant-tab-1':
        return findAction(
          action,
          key,
          Models.TenantCreate,
          Models.Tenant,
          Models.Tenant,
        );
      case 'tenant-tab-2':
        return findAction(
          action,
          key,
          Models.ContractsAdministrationCreate,
          Models.Tenant,
          Models.Tenant,
        );
      case 'tenant-tab-3':
        return findAction(
          action,
          key,
          Models.MasterSeriesCreate,
          Models.Company,
          Models.Company,
        );
      case 'entity':
        return findAction(
          action,
          key,
          Models.EntitiesCreateSchema,
          Models.EntitiesSchema,
          await Models.EntitiesUpdateSchema(),
        );
      case 'file':
        return findAction(
          action,
          key,
          Models.File,
          Models.File,
          Models.FileUpdate,
        );
      case 'asset':
        return findAction(
          action,
          key,
          Models.Asset,
          Models.Asset,
          Models.AssetUpdate,
        );
      case 'profile':
        return findAction(
          action,
          key,
          Models.Profile,
          Models.Profile,
          Models.ProfileUpdate,
        );
      default:
        throw new EntityNotFoundError('metadata', key);
    }
  }
}
