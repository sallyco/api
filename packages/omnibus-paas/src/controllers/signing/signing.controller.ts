import {inject, service} from '@loopback/core';
import {authenticate} from '@loopback/authentication';
import {
  getModelSchemaRef,
  operation,
  OperationVisibility,
  param,
  visibility,
} from '@loopback/rest';
import {SubscriptionsList} from '../../models';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {repository} from '@loopback/repository';
import {SubscriptionRepository} from '../../repositories';
import {KeycloakService} from '../../services';

@visibility(OperationVisibility.UNDOCUMENTED)
export class SigningController {
  constructor(
    @repository(SubscriptionRepository)
    private readonly subscriptionRepository: SubscriptionRepository,
  ) {}

  @authenticate('jwt')
  @operation('GET', '/signing/requests', {
    operationId: 'listSubscriptions',
    summary: 'List Subscriptions',
    description: 'List the current users subscriptions.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    parameters: [
      {
        name: 'perPage',
        in: 'query',
        required: false,
        schema: {
          type: 'number',
          minimum: 1,
        },
      },
      {
        name: 'page',
        in: 'query',
        required: false,
        schema: {
          type: 'number',
          minimum: 0,
        },
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SubscriptionsList),
          },
        },
      },
    },
  })
  async listSubscriptionsBySigningRequest(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(KeycloakService) keycloak: KeycloakService,
    @param({name: 'perPage', in: 'query'}) perPage = 10,
    @param({name: 'page', in: 'query'}) page = 0,
  ): Promise<SubscriptionsList> {
    const user = await (
      await keycloak.getClient()
    ).users.findOne({
      id: userProfile.id,
      realm: tenant.id,
    });

    let countAndStatement = {};
    let listFindStatement = {};
    countAndStatement = {
      and: [
        {isDeleted: false},
        {tenantId: tenant.id},
        {
          'signers.email': user.email,
        },
      ],
    };
    listFindStatement = {
      limit: perPage,
      skip: page * perPage,
      where: {
        ...countAndStatement,
      },
    };

    const countObj = await this.subscriptionRepository.count(countAndStatement);

    return new SubscriptionsList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.subscriptionRepository.find(listFindStatement),
    });
  }
}
