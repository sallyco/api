/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  HttpErrors,
  tags,
  operation,
  Request,
  requestBody,
  Response,
  RestBindings,
  getModelSchemaRef,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import debugFactory from 'debug';
import {File} from '../models';
import {FileRepository} from '../repositories';

import {FILE_UPLOAD_SERVICE} from '../keys';
import {FileUploadHandler} from '../types';
const debug = debugFactory('omnibus-paas:upload:controller');

///TMP
import {DocumentsService} from '../services';
/////
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

/**
 * A controller to handle file uploads using multipart/form-data media type
 */
@tags('Files')
export class FileUploadController {
  /**
   * Constructor
   * @param handler - Inject an express request handler to deal with the request
   * @param documentsService
   * @param fileRepository
   */
  constructor(
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @service(DocumentsService)
    private documentsService: DocumentsService,
    @repository(FileRepository)
    public fileRepository: FileRepository,
  ) {}

  @authenticate('jwt')
  @operation('post', '/files/upload', {
    operationId: 'fileUpload',
    summary: 'Upload a File',
    description: 'Upload a file',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(File, {partial: true}),
            },
          },
        },
      },
    },
  })
  async fileUpload(
    @requestBody.file()
    request: Request,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<object> {
    const files: any = await new Promise<object>((resolve, reject) => {
      this.handler(request, response, (err: unknown) => {
        if (err) reject(err);
        else {
          resolve(FileUploadController.getFilesAndFields(request));
        }
      });
    });

    const invalidFileName =
      // eslint-disable-next-line no-control-regex
      files.files.filter(f => /[^\u0000-\u00ff]/.test(f.originalname));
    if (invalidFileName?.length) {
      throw new HttpErrors.UnprocessableEntity(
        `File name contains unicode characters.`,
      );
    }

    const outFiles: any = [];
    for (const i in files.files) {
      debug('file', files.files[i]);
      const file: any = files.files[i];
      const workingFilePath = '/tmp/' + file.originalname;
      const outputFileName = file.originalname;
      const s3Object = await this.documentsService.uploadFileToBucket(
        workingFilePath,
        outputFileName,
        file.mimetype,
      );

      let tenantId = tenant.id;
      if (request?.header('x-file-upload-tenant-id')) {
        tenantId = request.header('x-file-upload-tenant-id')!;
      }

      const fileObj = new File({
        name: outputFileName,
        ownerId: profile.id,
        type: file.mimetype,
        key: s3Object,
        lastModified: Date.now().toString(),
        tenantId,
      });

      const fileRef = await this.fileRepository.create(fileObj);
      outFiles.push(fileRef);
    }

    return outFiles;
  }

  static getFilesAndFields(request: any) {
    const uploadedFiles = request.files;
    const mapper = (f: globalThis.Express.Multer.File) => ({
      fieldname: f.fieldname,
      originalname: f.originalname,
      encoding: f.encoding,
      mimetype: f.mimetype,
      size: f.size,
    });
    let files: object[] = [];
    if (Array.isArray(uploadedFiles)) {
      files = uploadedFiles.map(mapper);
    } else {
      for (const filename in uploadedFiles) {
        files.push(...uploadedFiles[filename].map(mapper));
      }
    }
    return {files, fields: request.body};
  }
}
