/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {
  param,
  operation,
  requestBody,
  Request,
  RestBindings,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Camunda} from '../services';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

@visibility(OperationVisibility.UNDOCUMENTED)
export class DecisionsController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @inject('services.Camunda') protected camunda: Camunda,
  ) {}

  ////////////////////////////
  ///// DECISION
  ///////////////////////////
  @authenticate('jwt')
  @operation('get', '/decisions')
  async getDecisionDefinitions(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDecisionDefinitions({
      latestVersion: true,
    });
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/decisions/count')
  async getDecisionDefinitionsCount(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDecisionDefinitionsCount(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/decisions/{key}')
  async getDecisionDefinitionByKey(
    @param({name: 'key', in: 'path'}) decisionDefinitionKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDecisionDefinitionByKey(
      decisionDefinitionKey,
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/decisions/{key}/diagram')
  async getDecisionDefinitionDiagramByKey(
    @param({name: 'key', in: 'path'}) decisionDefinitionKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDecisionDefinitionDiagramByKey(
      decisionDefinitionKey,
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/decisions/{key}/xml')
  async getDecisionDefinitionDmnXmlByKey(
    @param({name: 'key', in: 'path'}) decisionDefinitionKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDecisionDefinitionDmnXmlByKey(
      decisionDefinitionKey,
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('post', '/decisions/{key}/evaluate')
  async evaluateDecisionByKey(
    @param({name: 'key', in: 'path'}) decisionDefinitionKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody() values: object,
  ): Promise<any> {
    const resp = await this.camunda.evaluateDecisionByKey(
      decisionDefinitionKey,
      values,
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }
  //"getDecisionDefinitions",
  //"getDecisionDefinitionsCount",
  //"getDecisionDefinitionByKey",
  //"getDecisionDefinitionDiagramByKey",
  //"evaluateDecisionByKey",
  //
  //"updateHistoryTimeToLiveByDecisionDefinitionKey",
  //"getDecisionDefinitionByKeyAndTenantId",
  //"getDecisionDefinitionDiagramByKeyAndTenant",
  //"evaluateDecisionByKeyAndTenant",
  //"updateHistoryTimeToLiveByDecisionDefinitionKeyAndTenant",
  //
  //"getDecisionDefinitionDmnXmlByKeyAndTenant",
  //"getDecisionDefinitionDmnXmlByKey",
  //"getDecisionDefinitionById",
  //"getDecisionDefinitionDiagram",
  //"evaluateDecisionById",
  //
  //"updateHistoryTimeToLiveByDecisionDefinitionId",
  //"getDecisionDefinitionDmnXmlById",
}
