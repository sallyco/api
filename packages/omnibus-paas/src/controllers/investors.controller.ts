import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  OperationVisibility,
  param,
  response,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {InvestorForUser, InvestorSubscription} from '../models';
import {ProfileRepository} from '../repositories';
import {ObjectId} from 'bson';

@visibility(OperationVisibility.UNDOCUMENTED)
export class InvestorsController {
  constructor(
    @repository(ProfileRepository)
    protected profileRepository: ProfileRepository,
  ) {}

  @authenticate('jwt')
  @get('/investors-for-user/{id}')
  @response(200, {
    description: 'Array of Fund document model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(InvestorForUser, {includeRelations: true}),
        },
      },
    },
  })
  async findInvestorsForUser(
    @param.path.string('id') ownerId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<InvestorForUser[]> {
    const profileCollection =
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (this.profileRepository.dataSource.connector as any).collection(
        'Profile',
      );

    const data = await profileCollection
      .aggregate([
        {$match: {profileType: 'ORGANIZER', ownerId: ownerId}},
        {
          $lookup: {
            from: 'Deal',
            localField: '_id',
            foreignField: 'profileId',
            as: 'dealData',
          },
        },
        {
          $unwind: {
            path: '$dealData',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $redact: {
            $cond: [
              {$eq: ['$dealData.tenantId', '$tenantId']},
              '$$KEEP',
              '$$PRUNE',
            ],
          },
        },
        {
          $lookup: {
            from: 'Subscription',
            localField: 'dealData._id',
            foreignField: 'dealId',
            as: 'subscriptionData',
          },
        },
        {
          $unwind: {
            path: '$subscriptionData',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $redact: {
            $cond: [
              {
                $and: [
                  {$eq: ['$subscriptionData.tenantId', '$tenantId']},
                  {$ne: ['$subscriptionData.status', 'INVITED']},
                ],
              },
              '$$KEEP',
              '$$PRUNE',
            ],
          },
        },
        {
          $lookup: {
            from: 'Profile',
            localField: 'subscriptionData.profileId',
            foreignField: '_id',
            as: 'investorData',
          },
        },
        {
          $unwind: {
            path: '$investorData',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: {
              ownerId: '$ownerId',
              investorId: '$investorData._id',
              investorName: '$subscriptionData.name',
              investorEmail: '$subscriptionData.email',
              investorType: '$investorData.taxDetails.registrationType',
              entityName: {
                $cond: {
                  if: {
                    $eq: [
                      '$investorData.taxDetails.registrationType',
                      'ENTITY',
                    ],
                  },
                  then: '$investorData.name',
                  else: '',
                },
              },
            },
            subscriptionCount: {$sum: 1},
            amountSubscribed: {$sum: '$subscriptionData.amount'},
          },
        },
        {$sort: {'_id.investorName': 1}},
        {
          $project: {
            _id: -1,
            ownerId: '$_id.ownerId',
            investorId: '$_id.investorId',
            investorName: '$_id.investorName',
            investorEmail: '$_id.investorEmail',
            investorType: '$_id.investorType',
            entityName: '$_id.entityName',
            subscriptionCount: 1,
            amountSubscribed: 1,
          },
        },
      ])
      .get();

    return data.filter(
      d => d.investorType && d.investorType !== '' && d.amountSubscribed > 0,
    );
  }

  @authenticate('jwt')
  @get('/investors/{ownerId}/{id}')
  @response(200, {
    description: 'Array of Fund document model instances',
    content: {
      'application/json': {
        schema: getModelSchemaRef(InvestorSubscription),
      },
    },
  })
  async findInvestorsById(
    @param.path.string('ownerId') ownerId: string,
    @param.path.string('id') profileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<InvestorSubscription> {
    console.log('findInvestorsById');
    const profileCollection =
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (this.profileRepository.dataSource.connector as any).collection(
        'Profile',
      );

    const data = await profileCollection
      .aggregate([
        {
          $redact: {
            $cond: [
              {$eq: ['$_id', new ObjectId(profileId)]},
              '$$KEEP',
              '$$PRUNE',
            ],
          },
        },
        {
          $lookup: {
            from: 'Subscription',
            localField: '_id',
            foreignField: 'profileId',
            as: 'subscriptionData',
          },
        },
        {
          $unwind: {
            path: '$subscriptionData',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $redact: {
            $cond: [
              {$eq: ['$subscriptionData.tenantId', '$tenantId']},
              '$$KEEP',
              '$$PRUNE',
            ],
          },
        },
        {
          $lookup: {
            from: 'Deal',
            localField: 'subscriptionData.dealId',
            foreignField: '_id',
            as: 'dealData',
          },
        },
        {
          $unwind: {
            path: '$dealData',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $redact: {
            $cond: [
              {
                $and: [
                  {$eq: ['$dealData.tenantId', '$tenantId']},
                  {$eq: ['$dealData.ownerId', ownerId]},
                ],
              },
              '$$KEEP',
              '$$PRUNE',
            ],
          },
        },
        {
          $project: {
            profileId: '$_id',
            name: '$subscriptionData.name',
            email: '$subscriptionData.email',
            investorType: '$taxDetails.registrationType',
            entityName: {
              $cond: {
                if: {$eq: ['$taxDetails.registrationType', 'ENTITY']},
                then: '$name',
                else: '',
              },
            },
            address: 1,
            phone: 1,
            dealId: '$dealData._id',
            dealName: '$dealData.name',
            dealStatus: '$dealData.status',
            subscriptionId: '$subscriptionData._id',
            subscriptionAmount: '$subscriptionData.amount',
            subscriptionStatus: '$subscriptionData.status',
            subscribedOn: '$subscriptionData.createdAt',
          },
        },
      ])
      .get();

    return data;
  }
}
