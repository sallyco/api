/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {
  Filter,
  FilterBuilder,
  repository,
  WhereBuilder,
} from '@loopback/repository';
import {
  tags,
  getModelSchemaRef,
  HttpErrors,
  operation,
  param,
  requestBody,
  RestBindings,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {
  Request,
  Response
} from '@loopback/express';
import {SecurityBindings, UserProfile} from '@loopback/security';
import debugFactory from 'debug';
import KcAdminClient from 'keycloak-admin';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {Asset, AssetsList, File} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  AssetRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  SubscriptionRepository,
} from '../repositories';
import {DocumentsService} from '../services';
import {FileUploadHandler} from '../types';

const debug = debugFactory('omnibus-paas:assets:controller');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by assets
 *
 */
@tags('Assets')
export class AssetsController {
  constructor(
    @repository(AssetRepository)
    public assetRepository: AssetRepository,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @service(DocumentsService)
    private documentsService: DocumentsService,
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(EntitiesRepository)
    public entitiesRepository: EntitiesRepository,
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
  ) {}

  /**
   * This endpoint lists all the assets
   *

   * @returns A paged array of assets
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   * @param filter
   * @param request
   */
  @authenticate('jwt')
  @operation('get', '/assets', {
    operationId: 'readAssets',
    summary: 'List Assets',
    description:
      "List current user's assets to which the authentication method has access.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AssetsList),
          },
        },
      },
    },
  })
  async readAssets(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(RestBindings.Http.REQUEST) request: Request,
    @param.filter(Asset) filter?: Filter<Asset>,
  ): Promise<AssetsList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const userEmail = profile?.email;

    const query = new WhereBuilder<any>();
    if (tenant.id !== 'master') {
      if (profile?.roles?.account?.roles.includes('organizer')) {
        query.impose({
          and: [{tenantId: tenant.id}],
        });
      } else {
        query.impose({
          and: [
            {tenantId: tenant.id},
            {
              or: [
                {ownerId: profile.id},
                ...(userEmail
                  ? [
                      {
                        founderIds: userEmail.toLowerCase(),
                      },
                    ]
                  : [{}]),
              ],
            },
          ],
        });
      }
    }

    const countObj = await this.assetRepository.count(query.build());

    return new AssetsList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.assetRepository.find({
        limit: perPage,
        skip: page * perPage,
        ...filter,
        where: query.build(),
      }),
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/assets', {
    operationId: 'createAsset',
    summary: 'Create an Asset',
    description: 'Create an asset for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async createAsset(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {
            title: 'NewAsset',
            exclude: ['id'],
          }),
        },
      },
    })
    asset: Asset,
  ): Promise<Asset> {
    asset.ownerId = profile.id;
    asset.tenantId = tenant.id;
    // if (asset.type === 'FUND_MANAGER' && asset.isEntity && asset.entityInfo) {
    return this.assetRepository.create(asset);
    // } else if (asset.type === 'FUND_MANAGER' && !asset.isEntity && asset.email && asset.phone) {
    //   return this.assetRepository.create(asset);
    // } else if (asset.type === 'REGISTERED_AGENT') {
    //   return this.assetRepository.create(asset);
    // } else {
    //   throw new HttpErrors.UnprocessableEntity('The request body is invalid.')
    // }
  }

  /**
   *
   *

   * @param assetId The id of the asset to retrieve
   * @param profile
   * @param tenant
   * @param filter
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/assets/{assetId}', {
    operationId: 'readAssetById',
    summary: 'Get an Asset by Id',
    description: 'Get an Asset for the current user by Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async readAssetById(
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Asset) filter?: Filter<Asset>,
  ): Promise<Asset> {
    const query = new FilterBuilder<any>(filter);
    query.where({
      id: assetId,
    });
    if (tenant.id !== 'master') {
      if (profile?.roles?.account?.roles.includes('organizer')) {
        query.impose({
          and: [{tenantId: tenant.id}],
        });
      } else {
        query.impose({
          and: [
            {tenantId: tenant.id},
            {
              or: [
                {
                  ownerId: profile.id,
                },
                {
                  founderIds: profile.email?.toLowerCase(),
                },
              ],
            },
          ],
        });
      }
    }

    const asset = await this.assetRepository.findOne(query.build());

    if (asset) {
      return asset;
    } else {
      throw new HttpErrors.NotFound(
        'The asset you are trying to find cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param profile
   * @param tenant
   * @param assetId The id of the asset to retrieve
   * @param asset
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/assets/{assetId}', {
    operationId: 'updateAssetById',
    summary: 'Update an Asset by Id',
    description: 'Update an asset for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async updateAssetById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {partial: true}),
        },
      },
    })
    asset: Asset,
  ): Promise<Asset> {
    const retrievedAsset = await this.assetRepository.findById(assetId);
    if (
      !retrievedAsset.isDeleted &&
      (tenant.id === 'master' ||
        // (retrievedAsset.ownerId === profile.id &&
        retrievedAsset.tenantId === tenant.id)
    ) {
      asset.updatedAt = new Date();
      //TODO: Make this the correct replacement call
      await this.assetRepository.updateById(assetId, asset);
      return this.assetRepository.findById(assetId);
    } else {
      throw new HttpErrors.NotFound(
        'The asset you are trying to update cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param profile
   * @param tenant
   * @param assetId The id of the asset to retrieve
   * @param asset
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('patch', '/assets/{assetId}', {
    operationId: 'patchAssetById',
    summary: 'Update an Asset by Id',
    description: 'Patch an asset for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async patchAssetById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {partial: true}),
        },
      },
    })
    asset: Asset,
  ): Promise<Asset> {
    const retrievedAsset = await this.assetRepository.findById(assetId);
    if (tenant.id === 'master' || retrievedAsset.tenantId === tenant.id) {
      asset.updatedAt = new Date();
      await this.assetRepository.updateById(assetId, asset);
      return this.assetRepository.findById(assetId);
    } else {
      throw new HttpErrors.NotFound(
        'The asset you are trying to update cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param assetId The id of the asset to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/assets/{assetId}', {
    operationId: 'deleteAssetById',
    summary: 'Delete an Asset',
    description: 'Delete an asset for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async deleteAssetById(
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const asset = await this.assetRepository.findById(assetId);

    if (
      asset.isDeleted ||
      (asset.ownerId !== profile.id &&
        asset.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The asset you are trying to delete cannot be found.',
      );
    } else {
      await this.assetRepository.updateById(assetId, {
        deletedAt: new Date(),
        isDeleted: true,
      });
    }
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/assets/search', {
    operationId: 'searchAssets',
    summary: 'Search Assets',
    description: 'Search assets',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AssetsList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number
          * StartingIndex(page) is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async search(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              perPage: {
                type: 'number',
              },
              page: {
                type: 'number',
              },
              search: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
              sort: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    direction: {
                      type: 'string',
                      enum: ['ASCENDING', 'DESCENDING'],
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    searchObj: object,
  ): Promise<AssetsList> {
    let perPage = searchObj['perPage'];
    let page = searchObj['page'];
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const searchParamsList = [] as any;
    const sortParamList = [] as any;
    if (searchObj['search'] !== undefined && searchObj['search'].length > 0) {
      for (let index = 0; index < searchObj['search'].length; index++) {
        const queryParam = {};
        queryParam[searchObj['search'][index].field] =
          searchObj['search'][index].value;
        searchParamsList.push(queryParam);
      }
    }

    if (searchObj['sort'] !== undefined && searchObj['sort'].length > 0) {
      for (let index = 0; index < searchObj['sort'].length; index++) {
        let direction = 'ASC';
        if (searchObj['sort'][index].direction === 'DESCENDING')
          direction = 'DESC';
        const queryParam = searchObj['sort'][index].field + ' ' + direction;
        sortParamList.push(queryParam);
      }
    }

    searchParamsList.push(
      {isDeleted: false},
      {ownerId: profile.id},
      {tenantId: tenant.id},
    );
    const count = await this.assetRepository.count({and: [{isDeleted: false}]});

    return new AssetsList({
      totalCount: count.count,
      perPage: perPage,
      page: page,
      data: await this.assetRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: {and: searchParamsList},
        order: sortParamList,
      }),
    });
  }

  @authenticate('jwt')
  @operation('put', '/assets/documents-upload/{assetId}', {
    operationId: 'uploadAssetDocuments',
    summary: 'Add Documents to an Asset',
    description: 'Upload documents for the specified asset',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
      '404': {
        description:
          'Not Found. The asset you are trying to find cannot be found.',
      },
    },
  })
  async fileUpload(
    @requestBody.file()
    request: Request,
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Asset> {
    const documents: string[] = [];
    const asset = await this.assetRepository.findById(assetId);
    if (asset.isDeleted) {
      throw new HttpErrors.NotFound(
        'The companny you are trying to find cannot be found.',
      );
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const files: any = await new Promise<object>((resolve, reject) => {
      this.handler(request, response, (err: unknown) => {
        if (err) reject(err);
        else {
          resolve(request);
        }
      });
    });
    if(!request?.files){
      throw new HttpErrors.BadRequest("Files are required");
    }
    for (const fileKey in Object.keys(request.files)) {
      const file = request.files[fileKey];

      debug('file', file);
      const workingFilePath = '/tmp/' + file.originalname;
      const outputFileName = file.originalname;
      const s3Object = await this.documentsService.uploadFileToBucket(
        workingFilePath,
        outputFileName,
        file.mimetype,
      );
      debug('Uploaded to S3', s3Object);

      const fileObj = new File({
        name: outputFileName,
        ownerId: profile.id,
        tenantId: tenant.id,
        type: file.mimetype,
        key: s3Object,
        lastModified: Date.now().toString(),
      });

      const fileRef = await this.fileRepository.create(fileObj);
      switch (file.fieldname) {
        case 'video':
          asset.video = fileRef.id;
          break;
        case 'pitchDoc':
          asset.pitchDoc = fileRef.id;
          break;
        case 'documents':
          documents.push(fileRef.id);
          break;
        default:
          asset.videoURL = file.fieldname;
          break;
      }
    }

    asset.files = documents.length === 0 ? asset.files : documents;
    await this.assetRepository.updateById(assetId, asset);
    return this.assetRepository.findById(assetId);
  }

  @authenticate('jwt')
  @operation('put', '/assets/{assetId}/update-documents/{fileId}', {
    operationId: 'updateAssetDocuments',
    summary: 'Update Documents for an Asset',
    description: 'Update documents on an asset',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async updatedocuments(
    @requestBody.file()
    request: Request,
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @param({name: 'fileId', in: 'path'}) fileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Asset> {
    const asset = await this.assetRepository.findById(assetId);
    if (asset.isDeleted) {
      throw new HttpErrors.NotFound(
        'The companny you are trying to find cannot be found.',
      );
    }

    const fileObj = await this.fileRepository.findById(fileId);
    if (
      fileObj.isDeleted ||
      (fileObj.ownerId !== profile.id && fileObj.tenantId !== tenant.id)
    ) {
      throw new HttpErrors.NotFound(
        'The file you are trying to delete cannot be found.',
      );
    } else {
      await this.fileRepository.updateById(fileId, {
        deletedAt: new Date(),
        updatedAt: new Date(),
        isDeleted: true,
      });
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const files: any = await new Promise<object>((resolve, reject) => {
      this.handler(request, response, (err: unknown) => {
        if (err) reject(err);
        else {
          resolve(request);
        }
      });
    });
    if (request.files) {
      const file = request.files[0];
      debug('file', file);
      const workingFilePath = '/tmp/' + file.originalname;
      const outputFileName = file.originalname;
      const s3Object = await this.documentsService.uploadFileToBucket(
        workingFilePath,
        outputFileName,
        file.mimetype,
      );
      debug('Uploaded to S3', s3Object);

      const newFileObj = new File({
        name: outputFileName,
        ownerId: profile.id,
        tenantId: tenant.id,
        type: file.mimetype,
        key: s3Object,
        lastModified: Date.now().toString(),
      });
      const fileRef = await this.fileRepository.create(newFileObj);
      const index =
        asset.files?.indexOf(fileId) !== undefined
          ? asset.files?.indexOf(fileId)
          : -1;
      asset.files?.splice(index, 1);
      asset.files?.push(fileRef.id);
    }
    await this.assetRepository.updateById(assetId, asset);
    return this.assetRepository.findById(assetId);
  }

  @authenticate('jwt')
  @operation('delete', '/assets/{assetId}/delete-documents/{fileId}', {
    operationId: 'deleteAssetDocument',
    summary: 'Delete an Asset Document',
    description: 'Delete a Document for an Asset',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async deletedocuments(
    @param({name: 'assetId', in: 'path'}) assetId: string,
    @param({name: 'fileId', in: 'path'}) fileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Asset> {
    const asset = await this.assetRepository.findById(assetId);
    if (asset.isDeleted) {
      throw new HttpErrors.NotFound(
        'The companny you are trying to find cannot be found.',
      );
    }

    const fileObj = await this.fileRepository.findById(fileId);
    if (
      fileObj.isDeleted ||
      (fileObj.ownerId !== profile.id && fileObj.tenantId !== tenant.id)
    ) {
      throw new HttpErrors.NotFound(
        'The file you are trying to delete cannot be found.',
      );
    } else {
      await this.fileRepository.updateById(fileId, {
        deletedAt: new Date(),
        updatedAt: new Date(),
        isDeleted: true,
      });
    }
    const index =
      asset.files?.indexOf(fileId) !== undefined
        ? asset.files?.indexOf(fileId)
        : -1;
    asset.files?.splice(index, 1);
    await this.assetRepository.updateById(assetId, asset);
    return this.assetRepository.findById(assetId);
  }

  /**
   * // TODO: This doesn't handle multi-assets
   * @param dealId The id of the deal to retrieve the asset for
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/assets/by-deal-id/{dealId}', {
    operationId: 'getAssetByDealId',
    summary: 'Return an Asset ',
    description: 'Create an asset for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset),
          },
        },
      },
    },
  })
  async readAssetByDealId(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Asset> {
    const deal = await this.dealRepository.findById(dealId);
    const assetsWhere = new WhereBuilder();
    assetsWhere.eq('dealId', dealId);

    const assetList = await this.assetRepository.find({
      where: assetsWhere.build(),
    });
    let asset;
    if (assetList.length) {
      asset = assetList[0];
    } else {
      return asset;
    }

    const subscriptions = await this.subscriptionRepository.find({
      where: {dealId},
    });

    const clientAuth = async function () {
      const kcAdminClient = new KcAdminClient({
        baseUrl: process.env.KC_BASE_URL,
        realmName: 'master',
      });

      await kcAdminClient.auth({
        username: process.env.KC_REALM_USER
          ? process.env.KC_REALM_USER
          : 'development',
        password: process.env.KC_REALM_PASS
          ? process.env.KC_REALM_PASS
          : 'development',
        grantType: 'password',
        clientId: 'admin-cli',
      });

      return kcAdminClient;
    };
    const kcAdminClient = await clientAuth();
    const user = await kcAdminClient.users.findOne({
      id: profile.id,
      realm: tenant.id,
    });

    if (
      asset &&
      !asset.isDeleted &&
      (asset.ownerId === profile.id ||
        (user?.email && asset.invitedOrganizers?.includes(user.email)) ||
        deal.profileId === profile.id ||
        subscriptions.find(sub => sub.ownerId === profile.id))
    ) {
      return asset;
    } else {
      return {} as Asset;
    }
  }
}
