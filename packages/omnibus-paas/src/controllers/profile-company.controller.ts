import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Profile, Company} from '../models';
import {ProfileRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class ProfileCompanyController {
  constructor(
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
  ) {}

  @get('/profiles/{id}/company', {
    responses: {
      '200': {
        description: 'Company belonging to Profile',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Company)},
          },
        },
      },
    },
  })
  async getCompany(
    @param.path.string('id') id: typeof Profile.prototype.id,
  ): Promise<Company> {
    return this.profileRepository.masterEntity(id);
  }
}
