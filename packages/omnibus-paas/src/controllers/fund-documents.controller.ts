import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {log, LOG_LEVEL} from '../logger';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  get,
  getModelSchemaRef,
  OperationVisibility,
  response,
  visibility,
} from '@loopback/rest';
import {FundDocuments} from '../models';
import {
  CloseRepository,
  DealRepository,
  SubscriptionRepository,
} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class FundDocumentsController {
  constructor(
    @repository(DealRepository) protected dealRepository: DealRepository,
    @repository(SubscriptionRepository)
    protected subscriptionRepository: SubscriptionRepository,
    @repository(CloseRepository) protected closeRepository: CloseRepository,
  ) {}

  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @get('/fund-docs')
  @response(200, {
    description: 'Array of Fund document model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(FundDocuments, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<FundDocuments[]> {
    const dataset: FundDocuments[] = await this.subscriptionRepository
      .find({
        where: {
          isDeleted: false,
        },
        include: [
          {
            relation: 'deal',
            scope: {
              fields: ['id', 'name', 'profileId', 'entityId', 'isDeleted'],
              where: {
                isDeleted: false,
              },
              include: [
                {
                  relation: 'entity',
                  scope: {
                    fields: ['id', 'entityDocuments'],
                  },
                },
                {
                  relation: 'profile',
                  scope: {
                    fields: ['id', 'name', 'firstName', 'lastName'],
                  },
                },
              ],
            },
          },
        ],
      })
      .then(resultSubscriptions =>
        resultSubscriptions.map(subscription => {
          return {
            subscriptionId: subscription.id,
            tenantId: subscription.tenantId,
            investorName: subscription.name,
            investorEmail: subscription.email,
            dealId: subscription.dealId,
            dealName: subscription?.['deal']?.['name'],
            targetRaiseAmount: subscription?.['deal']?.['targetRaiseAmount'],
            organizerId: subscription?.['deal']?.['profileId'],
            organizerName: subscription?.['deal']?.['profile']?.['name'],
            organizerFirstName:
              subscription?.['deal']?.['profile']?.['firstName'],
            organizerLastName:
              subscription?.['deal']?.['profile']?.['lastName'],
            entityId: subscription?.['deal']?.['entityId'],
            entityDocs: subscription?.['deal']?.['entity']?.['entityDocuments'],
            investorSigned: subscription.isDocsSigned,
            subscriptionAmount: subscription.amount,
            subscriptionCreatedAt: new Date(subscription.createdAt ?? ''),
          };
        }),
      );

    const dataWithCloseDetail: FundDocuments[] = [];
    for (const data of dataset) {
      const closeDataForDeal = await this.closeRepository.find({
        where: {and: [{dealId: data.dealId}, {isDeleted: false}]},
      });

      if (closeDataForDeal && closeDataForDeal.length > 0) {
        const subscriptionId = data.subscriptionId?.toString();

        closeDataForDeal
          .filter(
            c => subscriptionId && c.subscriptions?.includes(subscriptionId),
          )
          .forEach(close => {
            const result = {
              ...data,
              closeId: close.id,
              organizerSigned: close.organizerSignature ? true : false,
              managerSigned: close.fundManagerSigned,
            };

            dataWithCloseDetail.push(result);
          });
      } else {
        dataWithCloseDetail.push({...data});
      }
    }
    return dataWithCloseDetail;
  }
}
