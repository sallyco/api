import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Entities, Company} from '../models';
import {EntitiesRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class EntitiesCompanyController {
  constructor(
    @repository(EntitiesRepository)
    public entitiesRepository: EntitiesRepository,
  ) {}

  @get('/entities/{id}/company', {
    responses: {
      '200': {
        description: 'Company belonging to Entities',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Company)},
          },
        },
      },
    },
  })
  async getCompany(
    @param.path.string('id') id: typeof Entities.prototype.id,
  ): Promise<Company> {
    return this.entitiesRepository.masterEntity(id);
  }
}
