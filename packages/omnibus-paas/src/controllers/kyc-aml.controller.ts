import {authenticate} from '@loopback/authentication';
import {authorize} from '../authorization';
import {repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  OperationVisibility,
  param,
  patch,
  requestBody,
  response,
  visibility,
} from '@loopback/rest';
import {service} from '@loopback/core';
import moment from 'moment';
import _ from 'lodash';
import {KycAmlProfile} from '../models';
import {
  DealRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
} from '../repositories';
import {KycAmlChecksService} from '../services/kycaml/kycamlchecks.service';

@visibility(OperationVisibility.UNDOCUMENTED)
export class KycAmlController {
  constructor(
    @repository(ProfileRepository) public profileRepository: ProfileRepository,
    @repository(TenantRepository) protected tenantRepository: TenantRepository,
    @repository(SubscriptionRepository)
    protected subscriptionRepository: SubscriptionRepository,
    @repository(DealRepository) protected dealRepository: DealRepository,
  ) {}

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/kyc-aml')
  @response(200, {
    description: 'Array of KYC/AML Profile model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(KycAmlProfile, {includeRelations: true}),
        },
      },
    },
  })
  async find(): Promise<KycAmlProfile[]> {
    const kycAmlProfiles: KycAmlProfile[] = [];
    const profiles = await this.profileRepository.find({
      where: {isDeleted: false},
    });

    for (const profile of profiles) {
      const kycAmlProfile: KycAmlProfile = {};
      kycAmlProfile.profile = profile;

      const manager = await this.profileRepository.manager(profile.id);
      kycAmlProfile.manager = {
        managerName: manager?.name ?? 'Glassboard Management II, LLC',
        managerEntityType: manager?.entityType ?? 'Limited Liability Company',
      };

      if (profile.profileType === 'INVESTOR') {
        const subscriptions = await this.subscriptionRepository.find({
          where: {profileId: profile.id},
        });
        if (subscriptions && subscriptions.length > 0) {
          for (const subscription of subscriptions) {
            const deal = await this.dealRepository.findById(
              subscription.dealId,
            );
            if (deal) {
              kycAmlProfile.deals = [
                {
                  dealId: subscription.dealId,
                  subscriptionId: subscription.id,
                  dealName: deal.name,
                },
              ];
            }
            kycAmlProfiles.push(kycAmlProfile);
          }
        } else {
          kycAmlProfiles.push(kycAmlProfile);
        }
      } else if (profile.profileType === 'ORGANIZER') {
        const deals = await this.dealRepository.find({
          where: {profileId: profile.id},
        });
        if (deals && deals.length > 0) {
          deals.forEach(deal => {
            kycAmlProfile.deals = [
              {
                dealId: deal.id,
                dealName: deal.name,
              },
            ];
            kycAmlProfiles.push(kycAmlProfile);
          });
        } else {
          kycAmlProfiles.push(kycAmlProfile);
        }
      }
    }

    return kycAmlProfiles;
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/kyc-aml/chart/{interval}/{utcOffset}')
  @response(200, {
    description: 'Array of KYC/AML Profile model instances',
  })
  async chart(
    @param.path.string('utcOffset') utcOffset: number,
    @param.path.string('interval')
    interval: moment.unitOfTime.DurationConstructor,
    @service(KycAmlChecksService) kycAmlChecksService: KycAmlChecksService,
  ): Promise<{data: (string | number)[][]; name: string}[]> {
    const startDate = moment().subtract(1, interval).startOf('day');
    const endDate = moment().add(1, 'day').startOf('day');

    const resultSet = await this.profileRepository
      .find({
        where: {isDeleted: false},
        include: ['kycAml'],
      })
      .then(data =>
        data.filter(
          d =>
            d.kycAml?.updatedAt &&
            moment(d.kycAml.updatedAt).isBetween(
              startDate,
              endDate,
              undefined,
              '[]',
            ),
        ),
      )
      .then(data => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const dataset: any[] = [];
        for (const profile of data) {
          if (!profile.kycAml?.updatedAt) {
            continue;
          }
          if (profile.kycAml?.result === 'CLEAR') {
            dataset.push({
              date: moment(profile.kycAml?.updatedAt),
              status: 'Clear',
            });
            continue;
          }

          if (profile.kycAml?.status === 'EXCEPTION') {
            dataset.push({
              date: moment(profile.kycAml?.updatedAt).utc(true),
              status: 'Exception',
            });
            continue;
          }

          const breakdown = kycAmlChecksService.getBreakdownFromCheckResults(
            profile.kycAml,
          );

          if (
            breakdown &&
            ((breakdown['sanction'] &&
              breakdown['sanction'].result.toLowerCase() === 'consider') ||
              (breakdown['monitored_lists'] &&
                breakdown['monitored_lists'].result.toLowerCase() ===
                  'consider'))
          ) {
            dataset.push({
              date: moment(profile.kycAml?.updatedAt).utc(true),
              status: 'Suspected',
            });
            continue;
          }

          if (
            breakdown?.['identity']?.result &&
            breakdown['identity'].result.toLowerCase() === 'consider'
          ) {
            dataset.push({
              date: moment(profile.kycAml?.updatedAt).utc(true),
              status: 'Identity',
            });
            continue;
          }
        }

        const recentClearedDataSeries = _(
          dataset.filter(d => d.status === 'Clear'),
        )
          .groupBy(entry => {
            return moment(entry.date)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const recentExceptionDataSeries = _(
          dataset.filter(d => d.status === 'Exception'),
        )
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const recentSuspectedDataSeries = _(
          dataset.filter(d => d.status === 'Suspected'),
        )
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const recentIdentityDataSeries = _(
          dataset.filter(d => d.status === 'Identity'),
        )
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        return [
          {
            name: 'Clear',
            data: _.sortBy(recentClearedDataSeries, [
              entry => new Date(entry[0]),
            ]),
          },
          {
            name: 'Exception',
            data: _.sortBy(recentExceptionDataSeries, [
              entry => new Date(entry[0]),
            ]),
          },
          {
            name: 'Suspected',
            data: _.sortBy(recentSuspectedDataSeries, [
              entry => new Date(entry[0]),
            ]),
          },
          {
            name: 'Identity',
            data: _.sortBy(recentIdentityDataSeries, [
              entry => new Date(entry[0]),
            ]),
          },
        ];
      });

    return resultSet;
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/kyc-aml/{id}')
  @response(200, {
    description: 'A single KYC/AML Profile model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(KycAmlProfile, {includeRelations: true}),
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<KycAmlProfile> {
    const profile = await this.profileRepository.findById(id);

    const kycAmlProfile: KycAmlProfile = {};
    kycAmlProfile.profile = profile;

    const manager = await this.profileRepository.manager(profile.id);
    kycAmlProfile.manager = {
      managerName: manager?.name ?? 'Glassboard Management II, LLC',
      managerEntityType: manager?.entityType ?? 'Limited Liability Company',
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const dealsForProfile: any[] = [];
    if (profile.profileType === 'INVESTOR') {
      const subscriptions = await this.subscriptionRepository.find({
        where: {profileId: profile.id},
      });
      if (subscriptions && subscriptions.length > 0) {
        for (const subscription of subscriptions) {
          const deal = await this.dealRepository.findById(subscription.dealId);
          if (deal) {
            dealsForProfile.push({
              dealId: subscription.dealId,
              subscriptionId: subscription.id,
              dealName: deal.name,
            });
          }
        }
      }
    } else if (profile.profileType === 'ORGANIZER') {
      const deals = await this.dealRepository.find({
        where: {profileId: profile.id},
      });
      if (deals && deals.length > 0) {
        deals.forEach(deal => {
          dealsForProfile.push({
            dealId: deal.id,
            dealName: deal.name,
          });
        });
      }
    }
    kycAmlProfile.deals = dealsForProfile;
    return kycAmlProfile;
  }

  @patch('/kyc-aml/{id}')
  @response(204, {
    description: 'Profile PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    @requestBody() kycAmlProfile: any,
  ): Promise<void> {
    await this.profileRepository.updateById(id, kycAmlProfile);
  }
}
