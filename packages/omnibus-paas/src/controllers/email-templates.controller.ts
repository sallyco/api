import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {Filter, FilterBuilder, repository} from '@loopback/repository';
import {
  getModelSchemaRef,
  operation,
  param,
  requestBody,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {EmailTemplate, Notification} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {EmailTemplateRepository} from '../repositories';
import {EmailTypes} from '../services';

@visibility(OperationVisibility.UNDOCUMENTED)
export class EmailTemplatesController {
  constructor(
    @repository(EmailTemplateRepository)
    public emailTemplateRepository: EmailTemplateRepository,
    @inject(SecurityBindings.USER)
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT)
    public tenant: Tenant,
  ) {}

  scopeFilterToUser(filter?: Filter<Notification>): Filter<Notification> {
    return new FilterBuilder<Notification>(filter)
      .impose({where: {userId: this.profile.id, tenantId: this.tenant.id}})
      .build();
  }

  /**
   *
   * @param id
   * @param filter
   */
  @authenticate('jwt')
  @operation('get', '/emailtemplates/{key}')
  async findByKey(
    @param.path.string('key') key: EmailTypes,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<EmailTemplate> {
    const template = await this.emailTemplateRepository.findOne({
      where: {key: key, tenantId: tenant.id},
    });
    if (template) {
      return template;
    } else {
      const defaultData = await this.emailTemplateRepository.findOne({
        where: {key: key, tenantId: 'global'},
      });
      return this.emailTemplateRepository.create({
        tenantId: tenant.id,
        ownerId: profile.id,
        key: key,
        data: defaultData?.data ?? {},
      });
    }
  }

  /**
   *
   * @param id
   * @param notification
   */
  @authenticate('jwt')
  @operation('put', '/emailtemplates/{id}')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(EmailTemplate, {partial: true}),
        },
      },
    })
    emailTemplate: EmailTemplate,
  ): Promise<EmailTemplate> {
    emailTemplate.updatedAt = new Date().toString();
    await this.emailTemplateRepository.updateById(id, emailTemplate);
    return this.emailTemplateRepository.findById(id);
  }
}
