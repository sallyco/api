import {authenticate} from '@loopback/authentication';
import {authorize} from '../authorization';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Global, GlobalCreate} from '../models';
import {GlobalRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class GlobalsController {
  constructor(
    @repository(GlobalRepository)
    public globalRepository: GlobalRepository,
  ) {}

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @post('/globals')
  @response(200, {
    description: 'Global model instance',
    content: {'application/json': {schema: getModelSchemaRef(Global)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalCreate, {
            title: 'NewGlobal',
          }),
        },
      },
    })
    global: GlobalCreate,
  ): Promise<Global> {
    const newDef = new Global({
      ...global,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
    return this.globalRepository.create(newDef);
  }

  @get('/globals')
  @response(200, {
    description: 'Array of Global model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Global, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Global) filter?: Filter<Global>): Promise<Global[]> {
    return this.globalRepository.find(filter);
  }

  @get('/globals/{id}')
  @response(200, {
    description: 'Global model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Global, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Global, {exclude: 'where'})
    filter?: FilterExcludingWhere<Global>,
  ): Promise<Global> {
    return this.globalRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @patch('/globals/{id}')
  @response(204, {
    description: 'Global PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Global, {partial: true}),
        },
      },
    })
    global: Global,
  ): Promise<void> {
    await this.globalRepository.updateById(id, global);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @put('/globals/{id}')
  @response(204, {
    description: 'Global PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() global: Global,
  ): Promise<void> {
    await this.globalRepository.replaceById(id, global);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @del('/globals/{id}')
  @response(204, {
    description: 'Global DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.globalRepository.deleteById(id);
  }
}
