import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  post,
  put,
  requestBody,
  response,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {ContactList} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {ContactListRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class ContactListController {
  constructor(
    @repository(ContactListRepository)
    public contactListRepository: ContactListRepository,
    @inject(SecurityBindings.USER) private profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ) {}

  @post('/contact-lists')
  @response(200, {
    description: 'ContactList model instance',
    content: {'application/json': {schema: getModelSchemaRef(ContactList)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContactList, {
            title: 'NewContactList',
            exclude: ['_id'],
          }),
        },
      },
    })
    contactList: Omit<ContactList, '_id'>,
  ): Promise<ContactList> {
    return this.contactListRepository.create(contactList);
  }

  @get('/contact-lists/count')
  @response(200, {
    description: 'ContactList model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ContactList) where?: Where<ContactList>,
  ): Promise<Count> {
    return this.contactListRepository.count(where);
  }

  @get('/contact-lists/by-owner/{ownerId}')
  @response(200, {
    description: 'Array of ContactList model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ContactList, {includeRelations: true}),
        },
      },
    },
  })
  async findByOwnerId(
    @param.path.string('ownerId') ownerId: string,
  ): Promise<ContactList | null> {
    if (ownerId !== this.profile?.id) {
      throw new HttpErrors.NotFound('Access restricted');
    }

    const filter = {
      where: {
        ownerId,
      },
    };
    return this.contactListRepository.findOne(filter);
  }

  @get('/contact-lists/{id}')
  @response(200, {
    description: 'ContactList model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ContactList, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ContactList, {exclude: 'where'})
    filter?: FilterExcludingWhere<ContactList>,
  ): Promise<ContactList> {
    const list = await this.contactListRepository.findById(id, filter);
    if (list.ownerId !== this.profile?.id) {
      throw new HttpErrors.NotFound('Access restricted');
    }

    return list;
  }

  @put('/contact-lists/{id}')
  @response(204, {
    description: 'ContactList PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() contactList: ContactList,
  ): Promise<void> {
    contactList.updatedAt = new Date();
    await this.contactListRepository.replaceById(id, contactList);
  }

  @del('/contact-lists/{id}')
  @response(204, {
    description: 'ContactList DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.contactListRepository.deleteById(id);
  }
}
