import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  OperationVisibility,
  param,
  patch,
  post,
  requestBody,
  visibility,
} from '@loopback/rest';
import {Deal, Distribution} from '../models';
import {DealRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class DealDistributionController {
  constructor(
    @repository(DealRepository) protected dealRepository: DealRepository,
  ) {}

  @get('/deals/{id}/distribution', {
    responses: {
      '200': {
        description: 'Deal has one Distribution',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Distribution),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Distribution>,
  ): Promise<Distribution> {
    return this.dealRepository.distribution(id).get(filter);
  }

  @post('/deals/{id}/distribution', {
    responses: {
      '200': {
        description: 'Deal model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(Distribution)},
        },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Deal.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Distribution, {
            title: 'NewDistributionInDeal',
            exclude: ['id'],
            optional: ['dealId'],
          }),
        },
      },
    })
    distribution: Omit<Distribution, 'id'>,
  ): Promise<Distribution> {
    return this.dealRepository.distribution(id).create(distribution);
  }

  @patch('/deals/{id}/distribution', {
    responses: {
      '200': {
        description: 'Deal.Distribution PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Distribution, {partial: true}),
        },
      },
    })
    distribution: Partial<Distribution>,
    @param.query.object('where', getWhereSchemaFor(Distribution))
    where?: Where<Distribution>,
  ): Promise<Count> {
    return this.dealRepository.distribution(id).patch(distribution, where);
  }

  @del('/deals/{id}/distribution', {
    responses: {
      '200': {
        description: 'Deal.Distribution DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Distribution))
    where?: Where<Distribution>,
  ): Promise<Count> {
    return this.dealRepository.distribution(id).delete(where);
  }
}
