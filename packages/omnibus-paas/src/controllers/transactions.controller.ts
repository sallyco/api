/* eslint-disable @typescript-eslint/no-explicit-any */
import {authorize} from '../authorization';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
  WhereBuilder,
} from '@loopback/repository';
import {
  //post,
  param,
  get,
  getModelSchemaRef,
  //patch,
  put,
  //del,
  requestBody,
  response,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import moment from 'moment';
import _ from 'lodash';
import {Transaction} from '../models';
import {
  BankingBeneficiaryRepository,
  TransactionRepository,
} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class TransactionsController {
  constructor(
    @repository(TransactionRepository)
    public transactionRepository: TransactionRepository,
    @repository(BankingBeneficiaryRepository)
    public bankingBeneficiaryRepository: BankingBeneficiaryRepository,
  ) {}

  /**
  @post('/transactions')
  @response(200, {
    description: 'Transaction model instance',
    content: {'application/json': {schema: getModelSchemaRef(Transaction)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Transaction, {
            title: 'NewTransaction',
            exclude: ['id'],
          }),
        },
      },
    })
    transaction: Omit<Transaction, 'id'>,
  ): Promise<Transaction> {
    return this.transactionRepository.create(transaction);
  }
**/

  @authenticate('jwt')
  @get('/transactions/count')
  @response(200, {
    description: 'Transaction model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Transaction) where?: Where<Transaction>,
  ): Promise<Count> {
    return this.transactionRepository.count(where);
  }

  @authenticate('jwt')
  @get('/transactions/charts')
  @response(200, {
    description: 'Reconsiliation Transaction Chart',
    content: {
      'application/json': {
        schema: {
          category: [],
          series: [],
          status: {},
        },
      },
    },
  })
  async charts(
    @param.query.number('startDate') startDate: number,
    @param.query.number('endDate') endDate: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    //@param.filter(Transaction) filter?: Filter<Transaction>,
  ): Promise<any> {
    const diffDays = Math.round(
      Math.abs((startDate - endDate) / (24 * 60 * 60 * 1000)),
    );
    const start = new Date(startDate);
    const end = new Date(endDate);
    const diffMonths =
      end.getMonth() -
      start.getMonth() +
      12 * (end.getFullYear() - start.getFullYear());
    const monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];

    const collection = await (
      this.transactionRepository.dataSource.connector as any
    ).collection(Transaction.modelName);
    const builder = new WhereBuilder({
      bankAccountTransactionId: {$exists: true},
      updatedAt: {$gte: new Date(startDate), $lte: new Date(endDate)},
    });
    const match = builder.impose(profile.where).build();

    const result = await collection
      .aggregate([
        {
          $match: match,
        },
        {
          $group: {
            _id: {
              month: {$substr: ['$updatedAt', 0, diffDays < 32 ? 10 : 7]},
              status: '$status',
            },
            count: {$sum: 1},
            amount: {$sum: '$amount'},
          },
        },
      ])
      .toArray();

    const res = {};
    const status = {}; // as Array<{count:number, amount:number}>;
    result.forEach(e => {
      if (res[e._id.status] === undefined) {
        res[e._id.status] = {};
      }
      res[e._id.status][e._id.month] = e.amount;
      status[e._id.status] = status[e._id.status]
        ? {
            count: status[e._id.status].count + e.count,
            amount: status[e._id.status].amount + e.amount,
          }
        : {count: e.count, amount: e.amount};
    });
    if (diffDays <= 31) {
      const dateList = {};
      for (let i = start; i <= end; i.setDate(i.getDate() + 1))
        dateList[
          `${i.getFullYear()}-${i.getMonth() < 10 ? 0 : ''}${
            i.getMonth() + 1
          }-${i.getDate() < 10 ? 0 : ''}${i.getDate()}`
        ] = `${i.getDate()} ${monthNames[i.getMonth()]}`;
      const final = [] as Array<{}>;
      for (const k in res) {
        final.push({
          name: k,
          data: Object.keys(dateList).map(m => (res[k][m] ? res[k][m] : 0)),
        });
      }
      return {
        series: final,
        categories: Object.keys(dateList).map(s => dateList[s]),
        status,
      };
    } else {
      const monthsList = {};
      for (let i = 0; i <= diffMonths; i++) {
        const m = new Date(startDate).getMonth() + i;
        const y = Math.floor(m / 12) + new Date(startDate).getFullYear();
        monthsList[`${y}-${m % 12 < 9 ? 0 : ''}${(m % 12) + 1}`] = `${
          monthNames[m % 12]
        } ${y}`;
      }

      const final = [] as Array<{}>;
      for (const e in res) {
        final.push({
          name: e,
          data: Object.keys(monthsList).map(m => (res[e][m] ? res[e][m] : 0)),
        });
      }
      return {
        series: final,
        categories: Object.keys(monthsList).map(m => monthsList[m]),
        status,
      };
    }
  }

  @authenticate('jwt')
  @get('/transactions')
  @response(200, {
    description: 'Array of Transaction model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Transaction, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Transaction) filter?: Filter<Transaction>,
  ): Promise<Transaction[]> {
    return this.transactionRepository.find(filter);
  }

  @authenticate('jwt')
  @get('/transactions/{id}')
  @response(200, {
    description: 'Transaction model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Transaction, {includeRelations: true}),
      },
    },
  })
  async findById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('id') id: string,
    @param.filter(Transaction, {exclude: 'where'})
    filter?: FilterExcludingWhere<Transaction>,
  ): Promise<Transaction> {
    return this.transactionRepository.findById(id, filter);
  }

  /**
  @patch('/transactions')
  @response(200, {
    description: 'Transaction PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Transaction, {partial: true}),
        },
      },
    })
    transaction: Transaction,
    @param.where(Transaction) where?: Where<Transaction>,
  ): Promise<Count> {
    return this.transactionRepository.updateAll(transaction, where);
  }
**/

  @authenticate('jwt')
  @authorize({allowedTenants: ['master', 'assure']})
  @put('/transactions/{id}')
  @response(204, {
    description: 'Transaction PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Transaction, {partial: true}),
        },
      },
    })
    transaction: Transaction,
  ): Promise<void> {
    await this.transactionRepository.updateById(id, transaction);
  }
  /**

  @put('/transactions/{id}')
  @response(204, {
    description: 'Transaction PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() transaction: Transaction,
  ): Promise<void> {
    await this.transactionRepository.replaceById(id, transaction);
  }

  @del('/transactions/{id}')
  @response(204, {
    description: 'Transaction DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.transactionRepository.deleteById(id);
  }
**/

  @authenticate('jwt')
  @get('/transactions-all')
  @response(200, {
    description: 'Array of Transaction model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Transaction, {includeRelations: true}),
        },
      },
    },
  })
  async findTransactionWithRelations(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Transaction[]> {
    return this.transactionRepository.find({
      include: [
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name', 'profileId'],
            include: [
              {
                relation: 'profile',
                scope: {
                  fields: ['id', 'name', 'firstName', 'lastName'],
                },
              },
            ],
          },
        },
        {
          relation: 'subscription',
          scope: {
            fields: ['id', 'profileId'],
            include: [
              {
                relation: 'profile',
                scope: {
                  fields: ['id', 'name', 'firstName', 'lastName'],
                },
              },
            ],
          },
        },
      ],
    });
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/transactions-all/chart/{interval}/{utcOffset}')
  @response(200, {
    description: 'Array of KYC/AML Profile model instances',
  })
  async getTransactionsChartData(
    @param.path.string('utcOffset') utcOffset: number,
    @param.path.string('interval')
    interval: moment.unitOfTime.DurationConstructor,
  ): Promise<{
    recents: {
      name: string;
      data: (string | number)[][];
    }[];
    counts: {
      name: string;
      data: (string | number)[][];
    }[];
  }> {
    const startDate = moment().subtract(1, interval).startOf('day');
    const endDate = moment().add(1, 'day').startOf('day');

    const resultSet = this.transactionRepository
      .find({
        where: {
          createdAt: {
            between: [startDate.toDate(), endDate.toDate()],
          },
        },
        include: [
          {
            relation: 'deal',
            scope: {
              fields: ['id', 'name', 'profileId'],
              include: [
                {
                  relation: 'profile',
                  scope: {
                    fields: ['id', 'name', 'firstName', 'lastName'],
                  },
                },
              ],
            },
          },
          {
            relation: 'subscription',
            scope: {
              fields: ['id', 'profileId'],
              include: [
                {
                  relation: 'profile',
                  scope: {
                    fields: ['id', 'name', 'firstName', 'lastName'],
                  },
                },
              ],
            },
          },
        ],
      })
      .then(data => {
        const dataset: any[] = [];
        for (const transaction of data) {
          if (!transaction?.createdAt) {
            continue;
          }

          if (
            transaction.type === 'ACH' &&
            ![
              'RETURNED',
              'TRANSACTION_EXCEPTION',
              'TRANSACTION EXCEPTION',
              'ERROR',
            ].includes(transaction.status)
          ) {
            dataset.push({
              date: moment(transaction.createdAt),
              status: 'ACH',
              amount: transaction.amount,
            });
            continue;
          }

          if (
            transaction.type === 'WIRE' &&
            ![
              'RETURNED',
              'TRANSACTION_EXCEPTION',
              'TRANSACTION EXCEPTION',
              'ERROR',
            ].includes(transaction.status)
          ) {
            dataset.push({
              date: moment(transaction.createdAt),
              status: 'Wire',
              amount: transaction.amount,
            });
            continue;
          }

          if (transaction.status === 'RETURNED') {
            dataset.push({
              date: moment(transaction.createdAt),
              status: 'Returned',
              amount: transaction.amount,
            });
            continue;
          }

          if (
            transaction.status === 'TRANSACTION_EXCEPTION' ||
            transaction.status === 'TRANSACTION EXCEPTION' ||
            transaction.status === 'ERROR'
          ) {
            dataset.push({
              date: moment(transaction.createdAt),
              status: 'Failed',
              amount: transaction.amount,
            });
            continue;
          }
        }

        const achDataSeries = _(dataset.filter(d => d.status === 'ACH'))
          .groupBy(entry => {
            return moment(entry.date)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const wireDataSeries = _(dataset.filter(d => d.status === 'Wire'))
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const returnedDataSeries = _(
          dataset.filter(d => d.status === 'Returned'),
        )
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const failedDataSeries = _(dataset.filter(d => d.status === 'Failed'))
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, en => 1)])
          .value();

        const recentDataSeries = _(dataset)
          .groupBy(entry => {
            return moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSSZ', true)
              .utc()
              .utcOffset(0 - utcOffset)
              .startOf('day');
          })
          .map((entry, id) => [id, _.sumBy(entry, 'amount')])
          .value();

        const countDataForChart = [
          {
            name: 'ACH',
            data: _.sortBy(achDataSeries, [entry => new Date(entry[0])]),
          },
          {
            name: 'Wire',
            data: _.sortBy(wireDataSeries, [entry => new Date(entry[0])]),
          },
          {
            name: 'Failed',
            data: _.sortBy(failedDataSeries, [entry => new Date(entry[0])]),
          },
          {
            name: 'Returned',
            data: _.sortBy(returnedDataSeries, [entry => new Date(entry[0])]),
          },
        ];

        const recentDataForChart = [
          {
            name: 'Amount',
            data: _.sortBy(recentDataSeries, [entry => new Date(entry[0])]),
          },
        ];

        return {
          recents: recentDataForChart,
          counts: countDataForChart,
        };
      });

    return resultSet;
  }

  @authenticate('jwt')
  @get('/transactions-log/{ownerId}')
  @response(200, {
    description: 'Array of Transaction model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Transaction, {includeRelations: true}),
        },
      },
    },
  })
  async findTransactionLog(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('ownerId') ownerId: string,
  ): Promise<Transaction[]> {
    const wires = await this.transactionRepository
      .find({
        where: {
          tenantId: tenant.id,
          direction: 'CREDIT',
          type: 'WIRE',
          or: [{subscriptionId: {exists: false}}, {subscriptionId: undefined}],
        },
        include: [
          {
            relation: 'entities',
            scope: {
              fields: ['id', 'name', 'dealId', 'ownerId'],
              where: {
                ownerId: ownerId,
              },
              include: [
                {
                  relation: 'deal',
                  scope: {
                    fields: ['id', 'name'],
                  },
                },
              ],
            },
          },
        ],
      })
      .then(data => {
        return data.filter(d => d['entities']);
      });

    const wireDataset: any[] = [];

    for (const transaction of wires) {
      const beneficiary = await this.bankingBeneficiaryRepository.findOne({
        where: {
          serviceObjectId: transaction.counterpartyId,
          ownerId,
        },
      });
      wireDataset.push({
        ...transaction,
        beneficiary: {
          name: beneficiary?.name,
          accountNumber: (
            beneficiary?.providerMeta!['wire'] ??
            beneficiary?.providerMeta!['ach']
          )?.account_number,
        },
      });
    }

    const combinedDataset = [...wireDataset];

    const achDataset = await this.transactionRepository.find({
      where: {
        tenantId: tenant.id,
        direction: 'CREDIT',
        type: 'ACH',
        counterpartyId: {exists: true},
        or: [{subscriptionId: {exists: false}}, {subscriptionId: undefined}],
      },
    });

    for (const transaction of achDataset) {
      const beneficiary = await this.bankingBeneficiaryRepository.findOne({
        where: {
          serviceObjectId: transaction.counterpartyId,
          ownerId,
        },
      });
      if (beneficiary) {
        combinedDataset.push({
          ...transaction,
          beneficiary: {
            name: beneficiary?.name,
            accountNumber: (
              beneficiary?.providerMeta!['wire'] ??
              beneficiary?.providerMeta!['ach']
            )?.account_number,
          },
        });
      }
    }

    return combinedDataset.sort((a, b) => {
      const aDate = new Date(a.createdAt as Date);
      const bDate = new Date(b.createdAt as Date);
      if (aDate > bDate) return -1;
      if (aDate < bDate) return 1;
      return 0;
    });
  }
}
