import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Subscription, Deal} from '../models';
import {SubscriptionRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class SubscriptionDealController {
  constructor(
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
  ) {}

  @get('/subscriptions/{id}/deal', {
    responses: {
      '200': {
        description: 'Deal belonging to Subscription',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Deal)},
          },
        },
      },
    },
  })
  async getDeal(
    @param.path.string('id') id: typeof Subscription.prototype.id,
  ): Promise<Deal> {
    return this.subscriptionRepository.deal(id);
  }
}
