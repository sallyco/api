/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import {
  getModelSchemaRef,
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  Request,
  requestBody,
  RestBindings,
  tags,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {
  ExternalBankAccount,
  ExternalBankingUser,
  Kyc,
  Profile,
} from '../models';
import {ProfilesList} from '../models/profile-list.models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  AccountRepository,
  KycRepository,
  ProfileRepository,
} from '../repositories';
import {
  Banking,
  BankingProvider,
  KycAmlProvider,
  PlaidService,
} from '../services';
import {KycAml} from '../services/kycaml/kycaml.service.types';

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by profiles
 *
 */
@tags('Profiles')
export class ProfilesController {
  constructor(
    @repository(ProfileRepository) public profileRepository: ProfileRepository,
    @repository(AccountRepository) public accountRepository: AccountRepository,
    @service(KycAmlProvider) protected kycaml: KycAml,
    @repository(KycRepository) protected kycRepository: KycRepository,
  ) {}

  /**
   * This endpoint lists all the profiles
   *

   * @returns Successful request, returns a paged array of profiles
   * @param profile
   * @param tenant
   * @param perPage
   * @param page
   * @param filter
   */
  @authenticate('jwt')
  @operation('get', '/profiles', {
    operationId: 'listProfiles',
    summary: 'List Profiles',
    description: "List current user's profiles.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProfilesList),
          },
        },
      },
    },
  })
  async listProfiles(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @param.filter(Profile) filter?: Filter<Profile>,
  ): Promise<ProfilesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const builder = new WhereBuilder(filter?.where);
    const countObj = await this.profileRepository.count(
      builder.impose(profile.where ?? {}).build(),
    );

    return new ProfilesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.profileRepository.find({
        include: ['kycAml', ...(filter?.include ?? [])],
        skip: page * perPage,
        where: builder
          .impose(profile.where ?? {})
          .impose({
            isDeleted: {
              neq: true,
            },
          })
          .build(),
      }),
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/profiles', {
    operationId: 'createProfile',
    summary: 'Create a Profile',
    description: 'Creates a new profile for the current user.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Profile),
          },
        },
      },
    },
  })
  async createProfile(
    @inject(SecurityBindings.USER) user: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Profile, {
            exclude: ['id'],
          }),
        },
      },
    })
    newProfile: Profile,
  ): Promise<Profile> {
    newProfile.ownerId = user.id;
    newProfile.tenantId = tenant.id;
    return this.profileRepository.create(newProfile);
  }

  /**
   *
   *

   * @param profileId The id of the profile to retrieve
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/profiles/{profileId}', {
    operationId: 'showProfileById',
    summary: 'Get a Profile by Id',
    description: 'Returns a specific profile for the current user.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Profile),
          },
        },
      },
    },
  })
  async showProfileById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @param.filter(Profile) filter?: Filter<Profile>,
  ): Promise<any> {
    const foundProfile = await this.profileRepository.findById(profileId, {
      ...filter,
      include: ['kycAml', ...(filter?.include ?? [])],
    });
    if (!foundProfile.isDeleted || foundProfile.tenantId !== tenant.id) {
      return foundProfile;
    } else {
      throw new HttpErrors.NotFound('This profile cannot be found');
    }
  }

  /**
   *
   *

   * @param profileId The id of the profile to retrieve
   * @param profile The id of the profile to retrieve
   */
  @authenticate('jwt')
  @operation('put', '/profiles/{profileId}', {
    operationId: 'updateProfileById',
    summary: 'Update a profile by ID',
    description: 'Update a profile with the provided information.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Profile),
          },
        },
      },
    },
  })
  async updateProfileById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Profile, {partial: true}),
        },
      },
    })
    updatedProfile: Profile,
  ): Promise<Profile> {
    const retrivedProfile = await this.profileRepository.findById(profileId);
    if (
      !retrivedProfile.isDeleted &&
      (tenant.id === 'master' ||
        (retrivedProfile.ownerId === profile.id &&
          retrivedProfile.tenantId === tenant.id))
    ) {
      updatedProfile.updatedAt = new Date();
      await this.profileRepository.updateById(profileId, updatedProfile);
      return this.profileRepository.findById(profileId);
    } else {
      throw new HttpErrors.NotFound('The profile cannot be found.');
    }
  }

  /**
   *
   *
   * @param profile
   * @param tenant
   * @param profileId The id of the profile to retrieve
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/profiles/{profileId}', {
    operationId: 'deleteProfileById',
    summary: 'Delete a Profile',
    description: 'Deletes a specific profile by id.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async deleteProfileById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'profileId', in: 'path'}) profileId: string,
  ): Promise<void> {
    const foundProfile = await this.profileRepository.findById(profileId);

    if (
      foundProfile.isDeleted ||
      (foundProfile.ownerId !== profile.id &&
        foundProfile.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound('The profile cannot be found.');
    } else {
      await this.profileRepository.deleteById(profileId);
    }
  }

  /**
   *
   *
   * @param userProfile
   * @param profileId The id of the profile to retrieve
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('post', '/profiles/{profileId}/createKycAmlApplicant', {
    operationId: 'createKycAmlApplicant',
    summary: 'Create KYC/AML Applicant from Profile',
    description:
      'Creates a new KYC/AML Check with Applicant from Profile Object.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Profile with kycAml sub-object',
        'application/json': {
          schema: getModelSchemaRef(Profile),
        },
      },
    },
  })
  async createKycAmlApplicant(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @param({name: 'profileId', in: 'path'}) profileId: string,
  ): Promise<any> {
    const profile = await this.profileRepository.findById(profileId, {
      include: ['kycAml'],
    });
    if (profile?.kycAml?.status === 'IN_PROGRESS') {
      return 'Check already in progress';
    }

    const applicant = await this.kycaml.createApplicant(
      await KycAmlProvider.createApplicationFromProfile(profile),
    );

    const check = await this.kycaml.createCheck({
      applicantId: String(applicant.providerMeta!.id),
    });

    await this.kycRepository.save(
      new Kyc({
        id: profile.kycAml?.id,
        ...check,
        profileId: profileId,
        tenantId: profile?.tenantId,
        ownerId: profile?.ownerId,
        updatedAt: new Date(),
      }),
    );

    return this.profileRepository.findById(profileId, {
      include: ['kycAml'],
    });
  }

  @authenticate('jwt')
  @operation('post', '/profiles/{profileId}/addBankingUser', {
    operationId: 'addBankingUser',
    summary: 'Add a banking user',
    description: 'Add a banking user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '204': {
        description: 'POST Add Profile Bank Account User Success',
      },
    },
  })
  async addBankingUser(
    @service(BankingProvider) banking: Banking,
    @param({name: 'profileId', in: 'path'}) profileId: string,
    // @requestBody() request_body: {
    //   GOVT_ID?: string;
    //   entityDocumentation?: {
    //     EIN_DOC?: string;
    //     BYLAWS_DOC?: string;
    //     AOI?: string;
    //   },
    //   primarySignatory?: {
    //     GOVT_ID?: string;
    //   },
    // },
    @inject(RestBindings.Http.REQUEST) request: Request,
  ): Promise<void> {
    const profile = await this.profileRepository.findById(profileId);
    if (profile.bankingUser) {
      throw new HttpErrors.Conflict(
        'Profile already has a banking user. Please add a banking account instead',
      );
    }
    // const ensureDefined = <T>(value: T, name: string): NonNullable<T> => {
    //   if (value === undefined || value === null) {
    //     throw new HttpErrors.UnprocessableEntity(`${name} is required`);
    //   }
    //   return value!;
    // };
    // ------------------------------------
    try {
      profile.bankingUser = new ExternalBankingUser({
        accounts: [],
      });
      await this.profileRepository.update(profile);
    } catch (e) {
      if (e instanceof HttpErrors.UnprocessableEntity) {
        throw e;
      }
      throw new HttpErrors.InternalServerError(e.message);
    }
  }

  @authenticate('jwt')
  @operation(
    'post',
    '/profiles/{profileId}/addBankAccount/{accountId}/{subAccountId}',
    {
      operationId: 'addBankingAccount',
      summary: 'Add a banking account to a profile',
      description: 'Add a banking account to a profile',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '204': {
          description: 'POST Add Profile Bank Account Success',
        },
      },
    },
  )
  async addBankingAccount(
    @service(BankingProvider) bankingService: Banking,
    @service(PlaidService) plaidService: PlaidService,
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @param({name: 'accountId', in: 'path'}) accountId: string,
    @param({name: 'subAccountId', in: 'path'}) subAccountId: string,
  ) {
    const profile = await this.profileRepository.findById(profileId);
    if (!profile.bankingUser) {
      profile.bankingUser = new ExternalBankingUser({});
      // throw new HttpErrors.ExpectationFailed('Banking User must be created on profile first');
    }
    if (
      profile.bankingUser.accounts &&
      profile.bankingUser.accounts.length > 0
    ) {
      const existingAccount = profile.bankingUser.accounts.find(
        acc =>
          acc.plaidAccountId === accountId &&
          acc.plaidSubAccountId === subAccountId,
      );
      if (existingAccount) {
        throw new HttpErrors.Conflict(
          'Selected account is already linked with this profile',
        );
      }
    }

    const account = await this.accountRepository.findById(accountId);
    const subAccount = account.plaidData.accounts.find(subAccountObject => {
      return subAccountObject.id === subAccountId;
    });

    const plaidAuthObject = await plaidService.getAuth(
      account.accessToken,
      subAccountId,
    );
    const accountType =
      plaidAuthObject.accounts[0].subtype === 'checking'
        ? 'CHECKING'
        : 'SAVINGS';

    const counterParty = await bankingService.createACHCounterParty({
      nameOnAccount: plaidAuthObject.accounts[0].name,
      accountNumber: plaidAuthObject.numbers.ach[0].account,
      routingNumber: plaidAuthObject.numbers.ach[0].routing,
      accountType: accountType,
    });

    const bankAccount = new ExternalBankAccount({
      counterpartyId: counterParty.id,
      accountName:
        account.plaidData.institution.name + ' ending in ' + subAccount.mask,
      plaidAccountId: accountId,
      plaidSubAccountId: subAccountId,
    });

    if (!profile.bankingUser.accounts) {
      profile.bankingUser.accounts = [];
    }

    // TODO: Figure out handling of adding multiple of the same account to the profile
    // if(profile.bankingUser.accounts.findIndex(synapseAccount=>{
    //   return synapseAccount.nodeId === synapseAccountId;
    // }) === -1){
    profile.bankingUser.accounts.push(bankAccount);
    // }

    await this.profileRepository.update(profile);
    return bankAccount;
  }

  /**
*
*

*/
  @authenticate('jwt')
  @operation('post', '/profiles/search', {
    operationId: 'searchProfiles',
    summary: 'Search for the current users profiles',
    description: 'Search for the users profiles given a searchObj.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProfilesList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number
          * StartingIndex(page) is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async search(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              perPage: {
                type: 'number',
              },
              page: {
                type: 'number',
              },
              search: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
              sort: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    direction: {
                      type: 'string',
                      enum: ['ASCENDING', 'DESCENDING'],
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    searchObj: object,
  ): Promise<ProfilesList> {
    let perPage = searchObj['perPage'];
    let page = searchObj['page'];
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const searchParamsList = [] as any;
    const sortParamList = [] as any;
    if (searchObj['search'] !== undefined && searchObj['search'].length > 0) {
      for (let index = 0; index < searchObj['search'].length; index++) {
        const queryParam = {};
        const pattern = new RegExp(
          '.*' + searchObj['search'][index].value + '.*',
          'i',
        );
        queryParam[searchObj['search'][index].field] = {like: pattern};
        searchParamsList.push(queryParam);
      }
    }

    if (searchObj['sort'] !== undefined && searchObj['sort'].length > 0) {
      for (let index = 0; index < searchObj['sort'].length; index++) {
        let direction = 'ASC';
        if (searchObj['sort'][index].direction === 'DESCENDING')
          direction = 'DESC';
        const queryParam = searchObj['sort'][index].field + ' ' + direction;
        sortParamList.push(queryParam);
      }
    }

    searchParamsList.push(
      {isDeleted: false},
      {ownerId: profile.id},
      {tenantId: tenant.id},
    );
    const count = await this.profileRepository.count({
      and: [{isDeleted: false}, {tenantId: tenant.id}],
    });

    return new ProfilesList({
      totalCount: count.count,
      perPage: perPage,
      page: page,
      data: await this.profileRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: {and: searchParamsList},
        order: sortParamList,
      }),
    });
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('post', '/profilevalidate')
  async validateprofilemodel(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Profile, {
            title: 'newprofile',
            exclude: ['id'],
          }),
        },
      },
    })
    testProfile: Profile,
  ): Promise<any> {
    //return model, only if profile model validated successfully
    return testProfile;
  }

  @authenticate('jwt')
  @operation(
    'delete',
    '/profiles/{profileId}/removeBankAccount/{accountId}/{subAccountId}',
    {
      operationId: 'removeBankingAccount',
      summary: 'Remove a banking account from a profile',
      description: 'Remove a banking account from a profile',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '204': {
          description: 'DELETE Remove Profile Bank Account Success',
        },
      },
    },
  )
  async removeBankingAccount(
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @param({name: 'accountId', in: 'path'}) accountId: string,
    @param({name: 'subAccountId', in: 'path'}) subAccountId: string,
  ) {
    const profile = await this.profileRepository.findById(profileId);
    if (!profile.bankingUser) {
      throw new HttpErrors.ExpectationFailed(
        'Banking User must be created on profile first',
      );
    }
    if (
      profile.bankingUser.accounts &&
      profile.bankingUser.accounts.length > 0
    ) {
      profile.bankingUser.accounts = profile.bankingUser.accounts.filter(
        account =>
          account.plaidAccountId !== accountId ||
          account.plaidSubAccountId !== subAccountId,
      );

      await this.profileRepository.update(profile);
    }
    return true;
  }

  /**
   * Returns list of investor profiles for the current user
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/investorprofiles-of-user', {
    operationId: 'getInvestorProfilesForUser',
    summary: 'Get list of Profiles by user',
    description: 'Returns list of investor profiles for the current user.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Profile),
          },
        },
      },
    },
  })
  async getInvestorProfilesForUser(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Profile[]> {
    const profilesWithAccounts = await this.profileRepository
      .find({
        where: {
          ownerId: profile.id,
          tenantId: tenant.id,
          profileType: 'INVESTOR',
          isDeleted: false,
          bankingUser: {exists: true},
        },
      })
      .then(data => {
        return data.filter(
          d => d.bankingUser?.accounts && d.bankingUser.accounts.length > 0,
        );
      });

    return profilesWithAccounts;
  }
}
