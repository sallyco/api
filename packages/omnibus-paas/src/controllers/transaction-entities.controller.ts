import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Transaction, Entities} from '../models';
import {TransactionRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class TransactionEntitiesController {
  constructor(
    @repository(TransactionRepository)
    public transactionRepository: TransactionRepository,
  ) {}

  @get('/transactions/{id}/entities', {
    responses: {
      '200': {
        description: 'Entities belonging to Transaction',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Entities)},
          },
        },
      },
    },
  })
  async getEntities(
    @param.path.string('id') id: typeof Transaction.prototype.id,
  ): Promise<Entities> {
    return this.transactionRepository.entities(id);
  }
}
