/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {
  param,
  operation,
  requestBody,
  Request,
  RestBindings,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Camunda} from '../services';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

@visibility(OperationVisibility.UNDOCUMENTED)
export class WorkflowsController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @inject('services.Camunda') protected camunda: Camunda,
  ) {}

  ////////////////////////////
  ///// ENGINE
  ///////////////////////////
  @authenticate('jwt')
  @operation('get', '/workflows/engine')
  async getProcessEngineNames(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessEngineNames();
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/engine/metrics')
  async getMetrics(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getMetrics(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/engine/api')
  async getRestAPIVersion(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getRestAPIVersion();
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ ENGINES
"getProcessEngineNames",

$$$$$ METRIC
"interval",
"getMetrics",

$$$$$ VERSION
"getRestAPIVersion",
**/

  ////////////////////////////
  ///// PROCESS DEFINITIONS
  ///////////////////////////
  @authenticate('jwt')
  @operation('get', '/workflows/processes')
  async getProcessDefinitions(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessDefinitions({
      latestVersion: true,
    });
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/processes/{id}')
  async getProcessDefinition(
    @param({name: 'id', in: 'path'}) processId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessDefinition(processId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/processes/{id}/xml')
  async getProcessDefinitionBpmn20Xml(
    @param({name: 'id', in: 'path'}) processId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessDefinitionBpmn20Xml(processId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/processes/{id}/variables')
  async getStartFormVariables(
    @param({name: 'id', in: 'path'}) processId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getStartFormVariables(processId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('post', '/workflows/processes/{id}/start')
  async startProcessInstance(
    @param({name: 'id', in: 'path'}) processId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody() bodyObject: object,
  ): Promise<any> {
    const resp = await this.camunda.startProcessInstance(
      processId,
      bodyObject,
      {
        withVariablesInReturn: true,
      },
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('post', '/workflows/processes-by-key/{key}/start')
  async startProcessInstanceByKey(
    @param({name: 'key', in: 'path'}) processKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody() bodyObject: object,
  ): Promise<any> {
    const resp = await this.camunda.startProcessInstanceByKey(
      processKey,
      bodyObject,
    );
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }
  /**
$$$$$ PROCESS DEFINITION 
"getProcessDefinitions",
"getProcessDefinitionsCount",
"getProcessDefinitionBpmn20Xml",

"getDeployedStartForm",
"getStartFormVariables",
"getRenderedStartForm",
"getStartForm",

"startProcessInstance",
"restartProcessInstance",

"startProcessInstanceByKey",
"deleteProcessDefinitionsByKey",
"getProcessDefinitionByKey",
"getDeployedStartFormByKey",
"getProcessDefinitionDiagramByKey",
"getStartFormVariablesByKey",
"updateHistoryTimeToLiveByProcessDefinitionKey",
"getRenderedStartFormByKey",
"getStartFormByKey",
"getActivityStatisticsByProcessDefinitionKey",
"submitFormByKey",
"updateProcessDefinitionSuspensionStateByKey",
"deleteProcessDefinitionsByKeyAndTenantId",
"getLatestProcessDefinitionByTenantId",
"getDeployedStartFormByKeyAndTenantId",
"getProcessDefinitionDiagramByKeyAndTenantId",
"getStartFormVariablesByKeyAndTenantId",
"updateHistoryTimeToLiveByProcessDefinitionKeyAndTenantId",
"getRenderedStartFormByKeyAndTenantId",
"startProcessInstanceByKeyAndTenantId",
"getStartFormByKeyAndTenantId",
"getActivityStatisticsByProcessDefinitionKeyAndTenantId",
"submitFormByKeyAndTenantId",
"updateProcessDefinitionSuspensionStateByKeyAndTenantId",
"getProcessDefinitionBpmn20XmlByKeyAndTenantId",
"getProcessDefinitionBpmn20XmlByKey",
"getProcessDefinitionStatistics",
"updateProcessDefinitionSuspensionState",
"deleteProcessDefinition",
"getProcessDefinition",
"getProcessDefinitionDiagram",
"updateHistoryTimeToLiveByProcessDefinitionId",
"restartProcessInstanceAsyncOperation",
"getActivityStatistics",
"submitForm",
"updateProcessDefinitionSuspensionStateById",
**/

  ////////////////////////////
  ///// PROCESS INSTANCES
  ///////////////////////////

  @authenticate('jwt')
  @operation('get', '/workflows/instances')
  async getProcessInstances(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessInstances();
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/instances-by-key/{key}')
  async getProcessInstancesByKey(
    @param({name: 'key', in: 'path'}) processKey: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessInstances({
      processDefinitionKey: processKey,
    });
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/instances/{id}')
  async getProcessInstance(
    @param({name: 'id', in: 'path'}) instanceId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessInstance(instanceId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/instances/{id}/activity-instances')
  async getActivityInstanceTree(
    @param({name: 'id', in: 'path'}) instanceId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getActivityInstanceTree(instanceId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/instances/{id}/variables')
  async getProcessInstanceVariables(
    @param({name: 'id', in: 'path'}) instanceId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getProcessInstanceVariables(instanceId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('delete', '/workflows/instances/{id}')
  async deleteProcessInstance(
    @param({name: 'id', in: 'path'}) instanceId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.deleteProcessInstance(instanceId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ PROCESS INSTANCE
"getProcessInstances",
"queryProcessInstances",
"getProcessInstancesCount",
"queryProcessInstancesCount",
"getProcessInstance",
"getActivityInstanceTree",
"deleteProcessInstance",

"deleteProcessInstancesAsyncOperation",
"deleteAsyncHistoricQueryBased",
"setRetriesByProcess",
"setRetriesByProcessHistoricQueryBased",
"updateSuspensionState",
"updateSuspensionStateAsyncOperation",
"setVariablesAsyncOperation",
"modifyProcessInstance",
"modifyProcessInstanceAsyncOperation",
"updateSuspensionStateById",
"getProcessInstanceVariables",
"modifyProcessInstanceVariables",
"deleteProcessInstanceVariable",
"getProcessInstanceVariable",
"setProcessInstanceVariable",
"getProcessInstanceVariableBinary",
"setProcessInstanceVariableBinary",

**/
  /////
  /////
  /////
  /////

  ////////////////////////////
  ///// TASKS
  ///////////////////////////
  @authenticate('jwt')
  @operation('get', '/workflows/tasks')
  async getTasks(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getTasks(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('post', '/workflows/tasks')
  async queryTasks(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody() bodyObject: object,
  ): Promise<any> {
    const resp = await this.camunda.queryTasks(0, 50, bodyObject);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/tasks/count')
  async getTasksCount(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getTasksCount(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/tasks/{id}')
  async getTask(
    @param({name: 'id', in: 'path'}) taskId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getTask(taskId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/tasks/{id}/variables')
  async getTaskVariables(
    @param({name: 'id', in: 'path'}) taskId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getTaskVariables(taskId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/tasks/{id}/form-variables')
  async getFormVariables(
    @param({name: 'id', in: 'path'}) taskId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getFormVariables(taskId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ TASK
"getTasks",
"getTask",
"queryTasks",
"getTasksCount",
"getFormVariables",

"queryTasksCount",
"getTaskLocalVariables",
"modifyTaskLocalVariables",
"deleteTaskLocalVariable",
"getTaskLocalVariable",
"putTaskLocalVariable",
"getTaskLocalVariableBinary",
"setBinaryTaskLocalVariable",
"getTaskVariables",
"modifyTaskVariables",
"deleteTaskVariable",
"getTaskVariable",
"putTaskVariable",
"getTaskVariableBinary",
"setBinaryTaskVariable",

"createTask",
"deleteTask",
"updateTask",
"setAssignee",
"getAttachments",
"addAttachment",
"deleteAttachment",
"getAttachment",
"getAttachmentData",
"handleBpmnError",
"handleEscalation",

"claim",
"getComments",
"createComment",
"getComment",
"complete",
"delegateTask",
"getDeployedForm",
"getForm",
"getIdentityLinks",
"addIdentityLink",
"deleteIdentityLink",
"getRenderedForm",
"resolve",
"submit",
"unclaim",
**/
  /////
  /////
  /////
  /////
  /////

  @authenticate('jwt')
  @operation('get', '/workflows/external-tasks')
  async getExternalTasks(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getExternalTasks(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /////

  //@authenticate('jwt')
  //@operation('get', '/workflows/admin/tenants')
  //async getTenants(
  //  @inject(SecurityBindings.USER) profile: UserProfile,
  //  @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  //): Promise<any> {
  //  const resp = await this.camunda.getTenants(null);
  //  const json = JSON.parse(JSON.stringify(resp));
  //  return json.body;
  //}

  /////
  ////////////////////////////
  ///// USERS
  ///////////////////////////

  @authenticate('jwt')
  @operation('get', '/workflows/admin/users')
  async getUsers(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getUsers(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  @authenticate('jwt')
  @operation('get', '/workflows/users/{id}/profile')
  async getUserProfile(
    @param({name: 'id', in: 'path'}) userId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getUserProfile(userId);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ USERS
"getUsers",
"availableOperations",
"getUserCount",

"createUser",
"deleteUser",
"availableUserOperations",
"updateCredentials",
"getUserProfile",
"unlockUser",
**/

  ////////////////////////////
  ///// INCIDENTS
  ///////////////////////////

  @authenticate('jwt')
  @operation('get', '/workflows/incidents')
  async getIncidents(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getIncidents(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ INCIDENT
"getIncidents",
"getIncidentsCount",

"resolveIncident",
"getIncident",
**/

  ////////////////////////////
  ///// DEPLOYMENTS
  ///////////////////////////

  @authenticate('jwt')
  @operation('get', '/workflows/deployments')
  async getDeployments(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getDeployments(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /**
$$$$$ DEPLOYMENT
>> "getDeployments",
"getDeploymentsCount",

"createDeployment",
"deleteDeployment",
"getDeployment",
"redeploy",
"getDeploymentResources",
"getDeploymentResource",
"getDeploymentResourceData",
**/

  /////

  @authenticate('jwt')
  @operation('get', '/workflows/event-subscriptions')
  async getEventSubscriptions(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getEventSubscriptions(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  // HISTORY
  /////

  @authenticate('jwt')
  @operation('get', '/workflows/history/activity-instances')
  async getHistoricActivityInstances(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getHistoricActivityInstances(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /////

  @authenticate('jwt')
  @operation('get', '/workflows/history/process-instances')
  async getHistoricProcessInstances(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getHistoricProcessInstances(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }

  /////

  @authenticate('jwt')
  @operation('get', '/workflows/schema/logs')
  async getSchemaLog(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const resp = await this.camunda.getSchemaLog(null);
    const json = JSON.parse(JSON.stringify(resp));
    return json.body;
  }
}

/**
 *
 *
  @operation('post', 'workflows/create-deal')
  async startWorkflow(): Promise<object> {
    const tmp = await this.camunda.startProcessInstanceByKey(PROCESS_KEY, {});
    const json = JSON.parse(JSON.stringify(tmp));
    return {
      date: new Date(),
      url: this.req.url,
      body: json.body,
    };
  }

  @operation('get', 'workflows/businessrules')
  async submitbusinessrules(
    @param({name: 'processinsatnceId', in: 'query'}) processinsatnceId: string,
    @param({name: 'isnew', in: 'query'}) isnew: boolean,
    @param({name: 'isindividual', in: 'query'}) isindividual: boolean,
  ): Promise<object> {
    const _data = await this.gettask(processinsatnceId);
    const _json = JSON.parse(JSON.stringify(_data));

    const data = {
      variables: {
        IsNew: {
          value: isnew,
          type: 'Boolean',
        },
        IsIndividual: {
          value: isindividual,
          type: 'Boolean',
        },
      },
      businessKey: 'myBusinessKey',
    };

    const taskid = _json.body[0].id;
    const tmp = await this.camunda.complete(taskid, data);
    const json = JSON.parse(JSON.stringify(tmp));
    return {
      taskid: json.body,
    };
  }

  @operation('get', 'workflows/get-task')
  async gettask(
    @param({name: 'processinstanceId', in: 'query'}) processinstanceId: string,
  ): Promise<object> {
    console.log('ok');
    const tmp = await this.camunda.getTasks(processinstanceId);
    const json = JSON.parse(JSON.stringify(tmp));
    return {
      taskid: json.body,
    };
  }
 *
 **/

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//
//
/**
Condition
Deployment
Engine
Event Subscription
External Task
Incident
Message
Metrics
Process Instance
Process Definition
Signal
Schema Log
Task
Telemetry
User
Version
Historic Activity Instance
Historic Process Instance
**/
//
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

/**
 *
 * FUNCTIONS
 *
 
$$$$$ CONDITION
"evaluateCondition",

$$$$$ EVENT SUBSCRIPTION
>> "getEventSubscriptions",
"getEventSubscriptionsCount",

$$$$$ EXTERNAL TASK
>> "getExternalTasks",
"queryExternalTasks",
"getExternalTasksCount",
"queryExternalTasksCount",

"fetchAndLock",
"setExternalTaskRetries",
"setExternalTaskRetriesAsyncOperation",
"getTopicNames",
"getExternalTask",
"handleExternalTaskBpmnError",
"completeExternalTaskResource",
"getExternalTaskErrorDetails",
"extendLock",
"handleFailure",
"setExternalTaskResourcePriority",
"setExternalTaskResourceRetries",
"unlock",

$$$$$ HISTORIC ACTIVITY INSTANCE
>> "getHistoricActivityInstances",
"queryHistoricActivityInstances",
"getHistoricActivityInstancesCount",
"queryHistoricActivityInstancesCount",

"getHistoricActivityInstance",

$$$$$ HISTORIC PROCESS INSTANCE
"getHistoricProcessInstances",
"queryHistoricProcessInstances",
"getHistoricProcessInstancesCount",
"queryHistoricProcessInstancesCount",

"deleteHistoricProcessInstancesAsync",
"getHistoricProcessInstanceDurationReport",
"setRemovalTimeAsync",
"deleteHistoricProcessInstance",
"getHistoricProcessInstance",
"deleteHistoricVariableInstancesOfHistoricProcessInstance",


$$$$$ MESSAGE
"deliverMessage",

$$$$$ SCHEMA LOG
"getSchemaLog",
"querySchemaLog",

$$$$$ SIGNAL
"throwSignal",

$$$$$ TELEMERY
"getTelemetryConfiguration",
"configureTelemetry",

**/
