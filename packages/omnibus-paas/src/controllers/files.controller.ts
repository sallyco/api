import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  tags,
  getModelSchemaRef,
  HttpErrors,
  operation,
  param,
  requestBody,
  RestBindings,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {ServerResponse} from 'http';
import {FilesList} from '../models/file-list.models';
import {File} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {FileRepository} from '../repositories';

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by files
 *
 */
@tags('Files')
export class FilesController {
  constructor(
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @inject(RestBindings.Http.RESPONSE)
    private response: ServerResponse,
  ) {}

  /**
   * This endpoint lists all the files
   *

   * @returns A paged array of files
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   */
  @authenticate('jwt')
  @operation('get', '/files', {
    operationId: 'getFile',
    summary: 'Get Files for the current User',
    description: 'Get a list of Files for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(FilesList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number.
          * StartingIndex(page) is not a natural number.
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async listFiles(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<FilesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const countObj = await this.fileRepository.count({
      and: [
        {isDeleted: false},
        ...(tenant.id !== 'master'
          ? [{ownerId: profile.id}, {tenantId: tenant.id}]
          : []),
      ],
    });

    const query =
      tenant.id !== 'master'
        ? {
            limit: perPage,
            skip: page * perPage,
            where: {
              and: [
                {isDeleted: false},
                {ownerId: profile.id},
                {tenantId: tenant.id},
              ],
            },
            fields: {tenantId: false},
          }
        : {
            limit: perPage,
            skip: page * perPage,
            where: {
              and: [{isDeleted: false}],
            },
          };

    const files = await this.fileRepository.find(query);

    return new FilesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: files,
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/files', {
    operationId: 'createFile',
    summary: 'Create a File',
    description: 'Create a File record',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(File),
          },
        },
      },
    },
  })
  async createFile(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(File, {
            title: 'NewFile',
            exclude: ['id'],
          }),
        },
      },
    })
    file: File,
  ): Promise<File> {
    file.ownerId = profile.id;
    file.tenantId = tenant.id;
    return this.fileRepository.create(file);
  }

  /**
   *
   *

   * @param fileId The id of the file to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/files/{fileId}', {
    operationId: 'getFile',
    summary: 'Get a File',
    description: 'Returns a File with the specified Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(File),
          },
        },
      },
      '404': {
        description:
          'Not Found. The file you are trying to find cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async showFileById(
    @param({name: 'fileId', in: 'path'}) fileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<File | undefined> {
    const file =
      tenant.id !== 'master'
        ? await this.fileRepository.findById(fileId, {
            fields: {tenantId: false},
          })
        : await this.fileRepository.findById(fileId);
    this.response.setHeader(
      'Cache-Control',
      'max-age=500000, stale-while-revalidate=60000',
    );
    if (file?.generating) {
      this.response.setHeader(
        'Cache-Control',
        'max-age=3, stale-while-revalidate=1',
      );
      return;
    }
    if (!file.isDeleted) {
      return file;
    } else {
      throw new HttpErrors.NotFound(
        'The file you are trying to find cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param profile
   * @param tenant
   * @param fileId The id of the file to retrieve
   * @param file
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/files/{fileId}', {
    operationId: 'updateFile',
    summary: 'Update a File',
    description: 'Update a File with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(File),
          },
        },
      },
      '404': {
        description:
          'Not Found. The file you are trying to update cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async updateFileById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'fileId', in: 'path'}) fileId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(File, {partial: true}),
        },
      },
    })
    file: File,
  ): Promise<File> {
    const retrievedFile =
      tenant.id !== 'master'
        ? await this.fileRepository.findOne({
            where: {
              and: [{id: fileId}, {ownerId: profile.id}, {tenantId: tenant.id}],
            },
            fields: {tenantId: false},
          })
        : await this.fileRepository.findOne({
            where: {
              and: [{id: fileId}],
            },
          });

    if (retrievedFile && !retrievedFile.isDeleted) {
      file.updatedAt = new Date();
      await this.fileRepository.updateById(fileId, file);
      return this.fileRepository.findById(fileId);
    } else {
      throw new HttpErrors.NotFound(
        'The file you are trying to update cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param fileId The id of the file to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/files/{fileId}', {
    operationId: 'deleteFile',
    summary: 'Delete a File',
    description: 'Deletes the File with the specified Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      '404': {
        description:
          'Not Found. The file you are trying to delete cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async softDeleteById(
    @param({name: 'fileId', in: 'path'}) fileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const file = await this.fileRepository.findById(fileId);

    if (
      file.isDeleted ||
      (file.ownerId !== profile.id &&
        file.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The file you are trying to delete cannot be found.',
      );
    } else {
      await this.fileRepository.deleteById(fileId);
    }
  }
}
