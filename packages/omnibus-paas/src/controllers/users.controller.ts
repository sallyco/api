/* eslint-disable @typescript-eslint/no-explicit-any */
import {authorize} from '../authorization';
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  Request,
  requestBody,
  RestBindings,
  tags,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import axios from 'axios';
import debugFactory from 'debug';
import KcAdminClient from 'keycloak-admin';
import {log, LOG_LEVEL} from '../logger';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  InviteRepository,
  KycRepository,
  SubscriptionRepository,
  UserRepository,
} from '../repositories';
import {KycAml, KycAmlProvider} from '../services';
import {Profile} from '../models';
import _ from 'lodash';

const querystring = require('querystring');

const debug = debugFactory('omnibus-paas:users:controller');

@tags('Users')
export class UsersController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
    @repository(InviteRepository) public inviteRepository: InviteRepository,
    @inject(RestBindings.Http.REQUEST) public request: Request,
    @repository(KycRepository) public kycRepository: KycRepository,
    @service(KycAmlProvider) public kycaml: KycAml,
  ) {}

  async clientAuth() {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
    });

    return kcAdminClient;
  }

  /**
   * This endpoint lists all the users
   *
   * @param profile
   * @param tenant
   * @param limit A limit on the number of objects to be returned.  Limit can range between 1 and 100, the default is 10.
   * @returns A paged array of users
   */
  @authenticate('jwt')
  @authorize({allowedTenants: ['master'], allowedRoles: ['admin']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/users')
  async readUsers(
    @param({name: 'limit', in: 'query'}) limit: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    return this.userRepository.find({where: {tenantId: tenant.id}});
  }

  @operation('post', '/users/request-password', {
    operationId: 'requestPassword',
    summary: 'Request a password reset',
    description: 'Use to request a password reset for a user.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: 'string',
      },
      '404': {
        description: 'Not Found. No user exists with specified email address',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async requestPassword(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              email: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    email: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const kcAdminClient = await this.clientAuth();
    const users = await kcAdminClient.users.find({
      email: email.email,
      realm: tenant.id,
    });

    if (users.length === 0) {
      throw new HttpErrors.NotFound(
        'No user exists with specified email address',
      );
    } else {
      const buff = new Buffer(email.email);
      const base64data = buff.toString('base64');
      return base64data;
    }
  }

  @log(LOG_LEVEL.INFO)
  @operation('post', '/users/verify-email', {
    operationId: 'verifyEmail',
    summary: 'Verify an email address',
    description: 'This will verify that a user email exists in the system.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async verifyEmail(
    @requestBody() data: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    debug('email:verify', data.email);
    const username = data.email;
    const kcAdminClient = await this.clientAuth();
    try {
      const user = await kcAdminClient.users.find({username, realm: tenant.id});
      debug('user:verify', user);
      const id = user[0].id as string;
      return await kcAdminClient.users.update(
        {id: id, realm: tenant.id},
        {emailVerified: true, requiredActions: []},
      );
    } catch (err) {
      debug('err : verify', err);
      throw new HttpErrors[err.response.status](err.response.data.errorMessage);
    }
  }

  @operation('post', '/users/reset-password', {
    operationId: 'resetPassword',
    summary: 'Attempt to reset a password',
    description: "This will attempt to reset a user's password.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async resetPassword(
    @requestBody() data: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const buff = new Buffer(data.token, 'base64');
    const email = buff.toString('ascii');

    const kcAdminClient = await this.clientAuth();

    try {
      const user = await kcAdminClient.users.find({email, realm: tenant.id});
      debug('user', user);
      const id = user[0].id as string;

      const response = await kcAdminClient.users.resetPassword({
        id: id,
        credential: {
          temporary: false,
          type: 'password',
          value: data.password,
        },
        realm: tenant.id,
      });

      return response;
    } catch (err) {
      debug('err : password reset', err);
      throw new HttpErrors[err.response.status](err.response.data.errorMessage);
    }
  }

  /**
   *
   *

   */
  @operation('post', '/users', {
    operationId: 'createUser',
    summary: 'Create a user',
    description:
      "This will create a user that can sign in to the system, and return the user's id",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
      '422': {
        description:
          'Unprocessable Entity. Phone number is required to create an account',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async createUser(
    @requestBody() user: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    if (!user.phone) {
      debug('err : users', 'Phone number missing from user object');
      throw new HttpErrors.UnprocessableEntity(
        'Phone number is required to create an account',
      );
    }
    const kcAdminClient = await this.clientAuth();

    try {
      const clients = await kcAdminClient.clients.find({
        realm: tenant.id,
      });
      const accountClient = clients.find(
        element => element.clientId === 'account',
      );
      // TODO What is this for?
      const accountClientId = accountClient?.id ?? '123';
      debug('accountClientId', accountClientId);

      const currentUser = await kcAdminClient.users.create({
        realm: tenant.id,
        username: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        attributes: {
          phone: user.phone,
          termsOfService: new Date(),
          tenantId: tenant.id,
          accountAdminId: user.accountAdminid,
        },
        credentials: [{value: user.password, type: 'password'}],
        enabled: true,
      });

      if (user.role) {
        const roles = await kcAdminClient.users.listAvailableClientRoleMappings(
          {
            realm: tenant.id,
            id: currentUser.id,
            clientUniqueId: accountClientId,
          },
        );

        const userroles = user.role.split(',');
        for (const i in userroles) {
          const role = roles.find(element => element.name === userroles[i]);
          if (role?.id) {
            const roleId = role.id;
            await kcAdminClient.users.addClientRoleMappings({
              id: currentUser.id,
              realm: tenant.id,
              clientUniqueId: accountClientId,

              // at least id and name should appear
              roles: [
                {
                  id: roleId,
                  name: userroles[i],
                },
              ],
            });
          }
        }
      }

      //Consolidate Invitations
      const invites = await this.inviteRepository.find({
        where: {
          and: [
            {isDeleted: false},
            {acceptableBy: user.email},
            {tenantId: tenant.id},
          ],
        },
      });

      invites
        .filter(invite => invite.invitedTo.find(invTo => invTo.type !== 'user'))
        .map(async invite => {
          const subscription = {
            email: user.email,
            name: `${user.firstName} ${user.lastName}`,
            ownerId: currentUser.id,
            dealId: invite.invitedTo[0].id,
            tenantId: tenant.id,
          };
          const sub = await this.subscriptionRepository.create(subscription);
          await this.inviteRepository.updateById(invite.id, {
            updatedAt: new Date(),
            acceptedAt: new Date(),
            redirectUrl: `/subscriptions/${sub.id}`,
          });
        });

      // Create KYC/AML Check for this User based on submitted information
      if (user.country) {
        const profileApplicantData =
          await KycAmlProvider.createApplicationFromProfile(
            new Profile({
              address: user.address,
              dateOfBirth: user.dateOfBirth,
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              taxDetails: {
                registrationType: 'INDIVIDUAL',
                taxIdentification: {
                  type: user.taxIdType,
                  value: user.taxId,
                },
              },
            }),
          );
        const profileApplicant = await this.kycaml.createApplicant(
          profileApplicantData,
        );
        const profileCheck = await this.kycaml.createCheck({
          applicantId: String(profileApplicant.providerMeta?.id),
        });
        await this.kycRepository.create({
          ...profileCheck,
          ownerId: currentUser.id,
          tenantId: tenant.id,
        });
      }
      return currentUser;
    } catch (err) {
      debug('err : users', err);
      throw new HttpErrors[err.response.status](err.response.data.errorMessage);
    }
  }

  /**
   *
   *

   * @param userId The id of the user to retrieve
   * @param tenant
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/users/{userId}')
  async readUserById(
    @param({name: 'userId', in: 'path'}) userId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    return this.userRepository.findById(userId);
  }

  /**
   *
   *

   * @returns Expected response to a valid request
   * @param email
   * @param tenant
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/users-by-email/{email}')
  async readUserByEmail(
    @param({name: 'email', in: 'path'}) email: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const kcAdminClient = await this.clientAuth();

    const users = await kcAdminClient.users.find({email, realm: tenant.id});

    debug('user', users);

    if (users.length === 0) {
      throw new HttpErrors.NotFound(
        'No user exists with specified email address',
      );
    } else {
      return users[0];
    }
  }

  /**
   *
   *

   * @returns Expected response to a valid request
   * @param profile
   * @param tenant
   */
  @authenticate('jwt')
  @operation('get', '/users/me', {
    operationId: 'readUserSelf',
    summary: 'Gets the current logged in user',
    deprecated: true,
    description: 'This will return the logged in user information',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: 'object',
      },
    },
  })
  async readUserSelf(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const kcAdminClient = await this.clientAuth();
    const user = await kcAdminClient.users.findOne({
      id: profile.id,
      realm: tenant.id,
    });

    if (!user) {
      throw new HttpErrors.NotFound(
        'No user exists with specified email address',
      );
    } else {
      return user;
    }
  }

  @authenticate('jwt')
  @operation('get', '/users/self', {
    operationId: 'readUserInfo',
    summary: 'Gets the current logged in user',
    description: 'This will return the logged in user information',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: 'object',
      },
    },
  })
  async readUserInfo(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const uri =
      process.env.KC_BASE_URL +
      '/realms/' +
      tenant.id +
      '/protocol/openid-connect/userinfo';

    try {
      const res = await axios.get(uri, {
        headers: {
          Authorization: this.request.headers.authorization,
        },
      });

      return res.data;
    } catch (error) {
      debug('err', error);
    }
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @operation('put', '/users/{userId}', {
    operationId: 'updateUserById',
    summary: 'Update a user',
    description: 'This will update a user with the information you provide.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
    },
  })
  async updateUserById(
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'userId', in: 'path'}) userId: string,
    @requestBody() userData: any,
  ): Promise<any> {
    const kcAdminClient = await this.clientAuth();
    return kcAdminClient.users.update({id: userId, realm: tenant.id}, userData);
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @operation('delete', '/users/{userId}')
  async deleteUser(
    @param({name: 'userId', in: 'path'}) userId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const kcAdminClient = await this.clientAuth();
    await kcAdminClient.users.update(
      {id: userId, realm: tenant.id},
      {enabled: false},
    );
  }

  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authorize({allowedRoles: ['admin']})
  @operation('post', '/users/{realmId}/{userId}/impersonate')
  async impersonateUserById(
    @param({name: 'userId', in: 'path'}) userId: string,
    @param({name: 'realmId', in: 'path'}) realmId: string,
  ): Promise<any> {
    const kcAdminClient = await this.clientAuth();
    const uri =
      process.env.KC_BASE_URL +
      '/admin/realms/' +
      realmId +
      '/users/' +
      userId +
      '/impersonation';
    const uri2 =
      process.env.KC_BASE_URL +
      '/realms/' +
      realmId +
      '/protocol/openid-connect/auth';

    try {
      const res = await axios.post(
        uri,
        {
          realm: realmId,
          user: userId,
        },
        {
          headers: {
            Authorization: `Bearer ${kcAdminClient.accessToken}`,
            'Content-Type': 'application/json',
          },
        },
      );

      let resUri = '';
      try {
        await axios.get(
          uri2 +
            '?response_mode=fragment&response_type=token&client_id=account',
          {
            headers: {
              Cookie: res.headers['set-cookie'],
              Authorization: `Bearer ${kcAdminClient.accessToken}`,
            },
          },
        );
      } catch (error2) {
        resUri = error2.request.res.responseUrl;
      }
      debug('resUri', resUri);
      const qs = resUri.split('#')[1];
      const queryObject = querystring.parse(qs);

      debug('token', queryObject);
      return queryObject;
    } catch (error) {
      debug('err', error);
    }
  }

  @log(LOG_LEVEL.INFO)
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/users/{userId}/role-mappings')
  async readUserRoles(
    @param({name: 'userId', in: 'path'}) userId: string,
  ): Promise<any> {
    const user = await this.userRepository.findById(userId);
    if (!user) {
      throw new HttpErrors.NotFound('No user exists with specified id');
    }
    const kcAdminClient = await this.clientAuth();
    const clients = await kcAdminClient.clients.find({realm: user.tenantId});

    const clientId: any = _.find(clients, {clientId: 'account'});
    if (!clientId) {
      throw new HttpErrors.NotFound('No role-mappings found');
    }
    return {
      roleMappings: await kcAdminClient.users.listClientRoleMappings({
        id: userId,
        clientUniqueId: clientId.id,
        realm: user.tenantId,
      }),
      available: await kcAdminClient.users.listAvailableClientRoleMappings({
        id: userId,
        clientUniqueId: clientId.id,
        realm: user.tenantId,
      }),
      composite: await kcAdminClient.users.listCompositeClientRoleMappings({
        id: userId,
        clientUniqueId: clientId.id,
        realm: user.tenantId,
      }),
    };
  }

  @authenticate('jwt')
  @log(LOG_LEVEL.INFO)
  @authorize({allowedRoles: ['admin']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('post', '/users/{userId}/role-mappings')
  async addUserRoles(
    @param({name: 'userId', in: 'path'}) userId: string,
    @requestBody() roles: any,
  ): Promise<any> {
    const user = await this.userRepository.findById(userId);
    if (!user) {
      throw new HttpErrors.NotFound('No user exists with specified id');
    }
    const kcAdminClient = await this.clientAuth();
    const clients = await kcAdminClient.clients.find({realm: user.tenantId});

    const clientId: any = _.find(clients, {clientId: 'account'});
    if (!clientId) {
      throw new HttpErrors.NotFound('No role-mappings found');
    }

    await kcAdminClient.users.addClientRoleMappings({
      id: userId,
      clientUniqueId: clientId.id,
      realm: user.tenantId,
      roles: roles.roles,
    });

    return;
  }

  @authenticate('jwt')
  @log(LOG_LEVEL.INFO)
  @authorize({allowedRoles: ['admin']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('delete', '/users/{userId}/role-mappings')
  async removeUserRoles(
    @param({name: 'userId', in: 'path'}) userId: string,
    @requestBody() roles: any,
  ): Promise<any> {
    const user = await this.userRepository.findById(userId);
    if (!user) {
      throw new HttpErrors.NotFound('No user exists with specified id');
    }
    const kcAdminClient = await this.clientAuth();
    const clients = await kcAdminClient.clients.find({realm: user.tenantId});

    const clientId: any = _.find(clients, {clientId: 'account'});
    if (!clientId) {
      throw new HttpErrors.NotFound('No role-mappings found');
    }

    await kcAdminClient.users.delClientRoleMappings({
      id: userId,
      clientUniqueId: clientId.id,
      realm: user.tenantId,
      roles: roles.roles,
    });

    return;
  }
}
