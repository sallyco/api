/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {Context, inject, service} from '@loopback/core';
import {EntityNotFoundError, repository} from '@loopback/repository';
import {tags, get, param, Response, RestBindings} from '@loopback/rest';
import {FileRepository} from '../repositories';
import {DocumentsService} from '../services';

/**
 * A controller to handle file downloads using multipart/form-data media type
 */
@tags('Files')
export class FileDownloadController {
  constructor(
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @service(DocumentsService)
    private documentsService: DocumentsService,
  ) {}

  @authenticate('jwt')
  @get('/files/{fileId}/download', {
    operationId: 'downloadFile',
    summary: 'Download a File',
    description: 'Download a File',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200. File is attched with response.',
      },
      '404': {
        description: 'Not Found',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async downloadFile(
    @param.path.string('fileId') fileId: string,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @inject.context() ctx: Context,
  ) {
    try {
      const file = await this.fileRepository.findById(fileId);
      const fileObjectStats = await this.documentsService.getDocumentStats(
        file.key,
      );
      const fileObject = await this.documentsService.getDocumentFromBucket(
        file.key,
      );
      response.setHeader('Content-Length', fileObjectStats.size);

      response.attachment(file.name);
      await new Promise(resolve => {
        const stream = fileObject.pipe(response);
        stream.on('finish', resolve);
      });
    } catch {
      throw new EntityNotFoundError('Document', fileId);
    }
  }
}
