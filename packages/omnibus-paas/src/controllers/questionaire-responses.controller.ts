import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  tags,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {QuestionaireResponse} from '../models';
import {QuestionaireResponseRepository} from '../repositories';

@tags('Questions')
@visibility(OperationVisibility.UNDOCUMENTED)
export class QuestionaireResponsesController {
  constructor(
    @repository(QuestionaireResponseRepository)
    public questionaireResponseRepository: QuestionaireResponseRepository,
  ) {}

  @authenticate('jwt')
  @post('/questions/responses')
  @response(200, {
    description: 'QuestionaireResponse model instance',
    content: {
      'application/json': {schema: getModelSchemaRef(QuestionaireResponse)},
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(QuestionaireResponse, {
            title: 'NewQuestionaireResponse',
            exclude: ['id'],
          }),
        },
      },
    })
    questionaireResponse: Omit<QuestionaireResponse, 'id'>,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<QuestionaireResponse> {
    return this.questionaireResponseRepository.create(questionaireResponse);
  }

  @get('/questions/responses/count')
  @response(200, {
    description: 'QuestionaireResponse model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(QuestionaireResponse) where?: Where<QuestionaireResponse>,
  ): Promise<Count> {
    return this.questionaireResponseRepository.count(where);
  }

  @get('/questions/responses')
  @response(200, {
    description: 'Array of QuestionaireResponse model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(QuestionaireResponse, {
            includeRelations: true,
          }),
        },
      },
    },
  })
  async find(
    @param.filter(QuestionaireResponse) filter?: Filter<QuestionaireResponse>,
  ): Promise<QuestionaireResponse[]> {
    return this.questionaireResponseRepository.find(filter);
  }

  @get('/questions/responses/{id}')
  @response(200, {
    description: 'QuestionaireResponse model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(QuestionaireResponse, {
          includeRelations: true,
        }),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(QuestionaireResponse, {exclude: 'where'})
    filter?: FilterExcludingWhere<QuestionaireResponse>,
  ): Promise<QuestionaireResponse> {
    return this.questionaireResponseRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @patch('/questions/responses/{id}')
  @response(204, {
    description: 'QuestionaireResponse PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(QuestionaireResponse, {partial: true}),
        },
      },
    })
    questionaireResponse: QuestionaireResponse,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.questionaireResponseRepository.updateById(
      id,
      questionaireResponse,
    );
  }

  @authenticate('jwt')
  @put('/questions/responses/{id}')
  @response(204, {
    description: 'QuestionaireResponse PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() questionaireResponse: QuestionaireResponse,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.questionaireResponseRepository.replaceById(
      id,
      questionaireResponse,
    );
  }

  @authenticate('jwt')
  @del('/questions/responses/{id}')
  @response(204, {
    description: 'QuestionaireResponse DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.questionaireResponseRepository.deleteById(id);
  }
}
