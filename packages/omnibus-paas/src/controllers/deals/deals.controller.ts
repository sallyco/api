/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {authorize} from '../../authorization';
import {inject, service} from '@loopback/core';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  requestBody,
  response as responseReq,
  RestBindings,
  tags,
  visibility,
} from '@loopback/rest';
import {ServerResponse} from 'http';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {DealService, FeesService} from '../../services';
import {log, LOG_LEVEL} from '../../logger';
import {Deal, DealsList, FeesStatement} from '../../models';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {
  DealRepository,
  SubscriptionRepository,
  TenantRepository,
} from '../../repositories';
import {ProfilesList} from '../../models/profile-list.models';

const _ = require('lodash');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by Deals
 * Everything about your Deals
 */
@tags('Deals')
export class DealsController {
  constructor(
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(TenantRepository)
    public tenantRepository: TenantRepository,
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
    @inject(RestBindings.Http.RESPONSE)
    private response: ServerResponse,
    @service(FeesService)
    private feeService: FeesService,
    @service(DealService)
    public dealService: DealService,
  ) {}

  /**
   * List current user's deals to which the authentication method has access.
   *
   * @returns Successful request, returns a paged array of deals
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   */
  @log(LOG_LEVEL.DEBUG)
  @authenticate('jwt')
  @operation('get', '/deals', {
    operationId: 'readDeal',
    summary: 'List Deals',
    description:
      "List current user's deals to which the authentication method has access.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DealsList),
          },
        },
      },
    },
  })
  async readDeals(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<DealsList> {
    let listFindStatement = {};
    const builder = new WhereBuilder({isDeleted: false});
    if (tenant.id !== 'master') {
      listFindStatement = {
        where: builder.impose(profile.where).build(),
        order: 'createdAt DESC',
        fields: {tenantId: false, ownerId: false},
        include: [
          {
            relation: 'entity',
            scope: {
              fields: ['id', 'name', 'entityType'],
            },
          },
          {
            relation: 'profile',
            scope: {
              fields: ['id', 'firstName', 'lastName'],
            },
          },
        ],
      };
    } else {
      listFindStatement = {
        where: {and: [{isDeleted: false}]},
        include: [
          {
            relation: 'entity',
            scope: {
              fields: ['id', 'name', 'entityType'],
            },
          },
          {
            relation: 'profile',
            scope: {
              fields: ['id', 'firstName', 'lastName'],
            },
          },
        ],
      };
    }

    const countObj = await this.dealRepository.getDealCount(tenant, profile);

    return new DealsList({
      totalCount: countObj.count,
      ...(perPage ? {perPage: perPage} : {}),
      ...(page === undefined ? {} : {page: page}),
      data: await this.dealRepository.find(listFindStatement),
    });
  }

  /**
   * Return false if a new deal can be created
   * Return true if the limit has been reached
   */
  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/deals/at-limit')
  async getNumberOfDeals(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<boolean> {
    let results;
    try {
      results = await Promise.all([
        this.dealRepository.getDealCount(tenant, profile),
        this.tenantRepository.findById(tenant.id),
      ]);
    } catch (err) {
      // If an error occurs assume new deals shouldn't be created
      console.error('deals/atLimit failed to query deal and tenant', err);
      return true;
    }

    const countObj = results[0];
    const tenantObj = results[1];
    const limit = tenantObj?.dealLimits?.maxDealCount || 0;

    /** Zero or less means unlimited
     *  Otherwise check the actual limit
     */
    if (limit <= 0 || limit > countObj.count) {
      return false;
    }

    return true;
  }

  @authenticate('jwt')
  @get('/deals/charts')
  @responseReq(200, {
    description: 'Deals Chart',
    content: {
      'application/json': {
        schema: {
          category: [],
          series: [],
          status: {},
        },
      },
    },
  })
  async charts(
    @param.query.number('startDate') startDate: number,
    @param.query.number('endDate') endDate: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    // @param.filter(Transaction) filter?: Filter<Transaction>,
  ): Promise<any> {
    // const diffDays = Math.round(Math.abs((startDate - endDate) / (24 * 60 * 60 * 1000));
    const start = new Date(startDate);
    const end = new Date(endDate);
    const diffMonths =
      end.getMonth() -
      start.getMonth() +
      12 * (end.getFullYear() - start.getFullYear());

    const monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    const monthsList = {};
    for (let i = 0; i <= diffMonths; i++) {
      const m = new Date(startDate).getMonth() + i;
      const y = Math.floor(m / 12) + new Date(startDate).getFullYear();
      monthsList[`${y}-${m % 12 < 9 ? 0 : ''}${(m % 12) + 1}`] = `${
        monthNames[m % 12]
      } ${y}`;
    }

    const collection = await (
      this.dealRepository.dataSource.connector as any
    ).collection(Deal.modelName);
    const result = await collection
      .aggregate([
        {
          $match: {
            updatedAt: {$gte: new Date(startDate), $lte: new Date(endDate)},
            ...(tenant.id === 'master' ? {} : {tenantId: tenant.id}),
          },
        },
        {
          $group: {
            _id: {
              month: {$substr: ['$updatedAt', 0, 7]},
              status: '$status',
            },
            count: {$sum: 1},
            amount: {$sum: '$previouslyRaisedAmount'},
            // minAmount:{$sum: '$minInvestmentAmount'},
            // prAmount:{$sum:'$previouslyRaisedAmount'}
          },
        },
      ])
      .toArray();

    const res = {};
    const status = {}; // as Array<{count:number, amount:number}>;
    result.forEach(e => {
      if (res[e._id.status] === undefined) {
        res[e._id.status] = {};
      }
      res[e._id.status][e._id.month] = e.count;
      status[e._id.status] = status[e._id.status]
        ? {
            count: status[e._id.status].count + e.count,
            amount: status[e._id.status].amount + e.amount,
            // minAmount: status[e._id.status].minAmount + e.minAmount,
            // prAmount: status[e._id.status].prAmount + e.prAmount,
          }
        : {
            count: e.count,
            amount: e.amount,
            // minAmount:e.minAmount,prAmount:e.prAmount
          };
    });
    const final = [] as Array<{}>;
    for (const e in res) {
      final.push({
        name: e,
        data: Object.keys(monthsList).map(m => (res[e][m] ? res[e][m] : 0)),
      });
    }
    return {
      series: final,
      categories: Object.keys(monthsList).map(m => monthsList[m]),
      status,
    };
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @authorize({allowedRoles: ['admin', 'organizer', 'manage-deals']})
  @operation('post', '/deals', {
    operationId: 'createDeal',
    summary: 'Create a Deal',
    'x-badges': [
      {
        color: 'red',
        label: 'Core API',
      },
    ],
    description: 'Create a new deal for the user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Deal),
          },
        },
      },
    },
  })
  async createDeal(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Deal, {
            title: 'NewDeal',
            exclude: ['id'],
          }),
        },
      },
    })
    deal: Deal,
  ): Promise<Deal> {
    deal.ownerId = profile.id;
    deal.tenantId = tenant.id;
    return this.dealRepository.create(deal);
  }

  /**
   *
   *
   * @param dealId The id of the deal to retrieve
   * @param profile
   * @param tenant
   * @param filter
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/deals/{dealId}', {
    operationId: 'getDeal',
    summary: 'Get a Deal by Id',
    description: 'Get a Deal by Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Deal),
          },
        },
      },
    },
  })
  async readDealById(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Deal) filter?: Filter<Deal>,
  ): Promise<Deal> {
    return this.dealRepository.findById(dealId, {
      include: [
        ...(filter?.include ?? []),
        {
          relation: 'entity',
          scope: {
            fields: ['id', 'name', 'entityType'],
          },
        },
      ],
    });
  }

  /**
   *
   *
   * @param profile
   * @param dealId The id of the deal to retrieve
   * @param deal
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/deals/{dealId}', {
    operationId: 'updateDeal',
    summary: 'Update Deal Asset',
    description: 'Update a Deal with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Deal),
          },
        },
      },
    },
  })
  async updateDealById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Deal, {partial: true}),
        },
      },
    })
    deal: Deal,
  ): Promise<Deal> {
    //    const retrivedDeal = await this.dealRepository.findById(dealId);

    //    if (!retrivedDeal.isDeleted && deal.ownerId === profile.id) {
    deal.updatedAt = new Date();
    await this.dealRepository.updateById(dealId, deal);
    return this.dealRepository.findById(dealId, {
      fields: {tenantId: false, ownerId: false},
    });
    //   } else {
    //     throw new HttpErrors.NotFound(
    //       'The deal you are trying to update cannot be found.',
    //     );
    //   }
  }

  /**
   *
   *
   * @param profile
   * @param dealId The id of the deal to retrieve
   * @param deal
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('patch', '/deals/{dealId}', {
    operationId: 'patchDeal',
    summary: 'Patch Deal',
    description: 'Patch a Deal with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Deal),
          },
        },
      },
    },
  })
  async patchDealById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Deal, {partial: true}),
        },
      },
    })
    deal: Deal,
  ): Promise<Deal> {
    deal.updatedAt = new Date();
    await this.dealRepository.updateById(dealId, deal);
    return this.dealRepository.findById(dealId, {
      fields: {tenantId: false, ownerId: false},
    });
  }

  /**
   *
   *
   * @param dealId The id of the deal to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/deals/{dealId}', {
    operationId: 'deleteDeal',
    summary: 'Delete a Deal',
    description: 'Delete the specified deal',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async softDeleteById(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const deal = await this.dealRepository.findById(dealId);

    if (
      deal.isDeleted ||
      (deal.ownerId !== profile.id &&
        deal.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The deal you are trying to delete cannot be found.',
      );
    } else {
      await this.dealRepository.updateById(dealId, {
        deletedAt: new Date(),
        isDeleted: true,
      });
    }
  }

  /**
 *
 *

 */
  @authenticate('jwt')
  @operation('post', '/deals/search', {
    operationId: 'searchDeals',
    summary: 'Search for Deals',
    description: 'Search Deals for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DealsList),
          },
        },
      },
    },
  })
  async search(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              perPage: {
                type: 'number',
              },
              page: {
                type: 'number',
              },
              search: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
              sort: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    direction: {
                      type: 'string',
                      enum: ['ASCENDING', 'DESCENDING', 'ASC', 'DESC'],
                    },
                  },
                },
              },
              includes: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
            },
          },
        },
      },
    })
    searchObj: object,
  ): Promise<DealsList> {
    const searchParamsList = [] as any;
    const sortParamList = [] as any;
    if (searchObj['search'] !== undefined && searchObj['search'].length > 0) {
      for (let index = 0; index < searchObj['search'].length; index++) {
        const queryParam = {};
        if (searchObj['search'][index].field === 'profileId') {
          queryParam[searchObj['search'][index].field] = {
            eq: searchObj['search'][index].value,
          };
        } else {
          const pattern = new RegExp(
            '.*' + searchObj['search'][index].value + '.*',
            'i',
          );
          queryParam[searchObj['search'][index].field] = {like: pattern};
        }
        searchParamsList.push(queryParam);
      }
    }

    if (searchObj['sort'] !== undefined && searchObj['sort'].length > 0) {
      for (let index = 0; index < searchObj['sort'].length; index++) {
        let direction = 'ASC';
        if (['DESCENDING', 'DESC'].includes(searchObj['sort'][index].direction))
          direction = 'DESC';
        const queryParam = searchObj['sort'][index].field + ' ' + direction;
        sortParamList.push(queryParam);
      }
    }

    // Includes
    let searchIncludes = [];
    if (
      searchObj['includes'] !== undefined &&
      searchObj['includes'].length > 0
    ) {
      searchIncludes = searchIncludes.concat(searchObj['includes']);
    }

    searchParamsList.push(
      {isDeleted: false},
      {ownerId: profile.id},
      {tenantId: tenant.id},
    );
    const count = await this.dealRepository.count({
      and: [{isDeleted: false}, {ownerId: profile.id}, {tenantId: tenant.id}],
    });

    return new DealsList({
      totalCount: count.count,
      data: await this.dealRepository.find({
        fields: {tenantId: false, ownerId: false},
        where: {and: searchParamsList},
        include: searchIncludes,
        order: sortParamList.length > 0 ? sortParamList : 'createdAt DESC',
      }),
    });
  }

  /**
   *
   *
   * @param dealId The id of the deal to retrieve
   * @param profile
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/deals/{dealId}/fees-statement', {
    operationId: 'getDealCloseInformation',
    summary: 'Get a close statement for a deal by dealId',
    description:
      'Returns information regarding what costs will be associated with a deal',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {},
          },
        },
      },
    },
  })
  async getDealCloseStatement(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<FeesStatement> {
    // Allow cache usage for up to 3m
    this.response.setHeader(
      'Cache-Control',
      'max-age=120, stale-while-revalidate=60',
    );

    return this.feeService.getFeesData(dealId);
  }

  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authorize({allowedTenants: ['master', 'assure']})
  @operation('post', '/deals/{dealId}/fees-statement/override')
  async overrideDealCloseStatement(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @requestBody() payload: any,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<any> {
    const deal = await this.dealRepository.findById(dealId);

    if (!deal) {
      throw new HttpErrors.NotFound(
        'The deal you are trying to find cannot be found.',
      );
    }

    const feesOverride =
      deal.additionalProperties && 'feesOverride' in deal.additionalProperties
        ? _.keyBy(deal.additionalProperties['feesOverride'], 'field')
        : {};

    feesOverride[payload.field] = payload;
    try {
      if (deal.additionalProperties) {
        deal.additionalProperties['feesOverride'] = _.values(feesOverride);
      } else {
        deal.additionalProperties = {};
        deal.additionalProperties['feesOverride'] = _.values(feesOverride);
      }
    } catch (err) {
      throw new HttpErrors.UnprocessableEntity(err);
    }
    return this.dealRepository.updateById(dealId, deal);
  }

  /**
   *
   *
   * @param dealId The id of the deal to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/deals/{dealId}/performance-summary', {
    operationId: 'getDealPerformanceSummary',
    summary: 'Return a summary of information for the dealId',
    description:
      'Information like # of subscriptions, funds received, etc will be returned',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async getDealPerformanceSummary(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    return this.dealService.getDealPerformanceSummary(dealId, {});
  }

  /**
   *
   *
   * @param ProfileId The profileId of the deals to retrieve
   * @param profile
   * @param filter
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/deals/{profileId}/deals-meta', {
    operationId: 'readDealsByProfileId',
    summary: 'Get  deals by profileId',
    description: 'Returns deals given a specific profileId',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DealsList),
          },
        },
      },
    },
  })
  async readDealsByProfileId(
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.filter(Deal) filter?: Filter<Deal>,
  ): Promise<DealsList> {
    const listFindStatement = {
      where: {
        and: [{isDeleted: false}, {profileId: profileId}],
      },
    };
    const countObj = await this.dealRepository.count({
      isDeleted: false,
      profileId: profileId,
    });

    return new DealsList({
      totalCount: countObj.count,
      perPage: 100,
      page: 1,
      data: await this.dealRepository.find(listFindStatement),
    });
  }

  /**
   *
   *
   * @param dealId The id of the deal to retrieve
   * @param profile
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/profiles-for-deal/{dealId}', {
    operationId: 'getInvestorProfilesUsedForDeal',
    summary: 'Get investor profiles by dealId',
    description: 'Returns investorprofiles used in a specific deal',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProfilesList),
          },
        },
      },
    },
  })
  async getInvestorProfilesUsedForDeal(
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<ProfilesList> {
    const result = await this.subscriptionRepository
      .find({
        where: {
          isDeleted: false,
          dealId,
          ownerId: profile.id,
        },
        include: [
          {
            relation: 'profile',
            scope: {
              include: [
                {
                  relation: 'accreditations',
                },
              ],
            },
          },
        ],
      })
      .then(data => data.filter(d => d['profile']));

    return new ProfilesList({
      totalCount: result.length,
      data: result.map(r => r['profile']),
    });
  }
}
