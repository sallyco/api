import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  OperationVisibility,
  visibility,
} from '@loopback/rest';
import {Distribution} from '../../../models';
import {DealRepository, DistributionRepository} from '../../../repositories';
import {inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../../multi-tenancy';
import {authenticate} from '@loopback/authentication';

@visibility(OperationVisibility.UNDOCUMENTED)
export class DistributionsController {
  constructor(
    @repository(DistributionRepository)
    public distributionRepository: DistributionRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
  ) {}

  @authenticate('jwt')
  @post('/distributions')
  @response(200, {
    description: 'Distribution model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Distribution),
      },
    },
  })
  async create(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Distribution, {
            title: 'NewDistribution',
            exclude: ['id'],
          }),
        },
      },
    })
    distribution: Omit<Distribution, 'id'>,
  ): Promise<Distribution> {
    distribution.ownerId = profile.id;
    distribution.tenantId = tenant.id;
    return this.distributionRepository.create(distribution);
  }

  @authenticate('jwt')
  @get('/distributions/count')
  @response(200, {
    description: 'Distribution model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Distribution) where?: Where<Distribution>,
  ): Promise<Count> {
    return this.distributionRepository.count(where);
  }

  @authenticate('jwt')
  @get('/distributions')
  @response(200, {
    description: 'Array of Distribution model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Distribution, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Distribution) filter?: Filter<Distribution>,
  ): Promise<Distribution[]> {
    return this.distributionRepository.find(filter);
  }

  @authenticate('jwt')
  @get('/distributions/{id}')
  @response(200, {
    description: 'Distribution model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Distribution, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Distribution, {exclude: 'where'})
    filter?: FilterExcludingWhere<Distribution>,
  ): Promise<Distribution> {
    return this.distributionRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @patch('/distributions/{id}')
  @response(204, {
    description: 'Distribution PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: getModelSchemaRef(Distribution, {partial: true}),
        },
      },
    })
    distribution: Distribution,
  ): Promise<void> {
    await this.distributionRepository.updateById(id, distribution);
  }

  @authenticate('jwt')
  @put('/distributions/{id}')
  @response(204, {
    description: 'Distribution PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() distribution: Distribution,
  ): Promise<void> {
    await this.distributionRepository.replaceById(id, distribution);
  }

  @authenticate('jwt')
  @del('/distributions/{id}')
  @response(204, {
    description: 'Distribution DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.distributionRepository.deleteById(id);
  }
}
