/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  tags,
  Request,
  RestBindings,
  get,
  ResponseObject,
  oas,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/context';
import {HealthStatusService} from '../services';

/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'PingResponse',
        properties: {
          date: {type: 'string'},
          url: {type: 'string'},
          headers: {
            type: 'object',
            properties: {
              'Content-Type': {type: 'string'},
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
@visibility(OperationVisibility.UNDOCUMENTED)
@tags('Ping')
export class PingController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @inject('services.HealthStatusService')
    public healthStatus: HealthStatusService,
  ) {}

  // Map to `GET /ping`
  @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  @oas.tags('TenantNotRequired')
  async ping(): Promise<object> {
    let hs: any;
    try {
      hs = await this.healthStatus.getHealthStatus('2');
    } catch {
      hs = {healthStatuses: 404};
    }
    return {
      date: new Date(),
      url: this.req.url,
      // headers: Object.assign({}, this.req.headers),// Let's handle this a little differently
      buildVersion: process.env.BUILD_VERSION ?? 'x.x.x',
      buildName: process.env.BUILD_NAME ?? 'development',
      buildDate: process.env.BUILD_DATE ?? new Date().toISOString(),
      ...hs,
    };
  }
}
