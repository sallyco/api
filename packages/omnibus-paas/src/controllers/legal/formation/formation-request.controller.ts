import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  post,
  requestBody,
  tags,
} from '@loopback/rest';
import {
  FormationRequest,
  FormationRequestEINRequest,
  FormationRequestEntityRequest,
  FormationRequestStatus,
} from '../../../models';
import {FormationRequestRepository} from '../../../repositories';
import {inject, service} from '@loopback/core';
import {FilingsService, Unleash, UnleashProvider} from '../../../services';
import {authenticate} from '@loopback/authentication';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../../multi-tenancy';

@tags('Legal')
export class FormationRequestController {
  constructor(
    @repository(FormationRequestRepository)
    public formationRequestRepository: FormationRequestRepository,
    @service(FilingsService)
    public filings: FilingsService,
    @service(UnleashProvider)
    public unleash: Unleash,
  ) {}

  @authenticate('jwt')
  @post('/legal/formation/requests/ein-only', {
    operationId: 'createEINRequest',
    summary: 'Request an EIN',
    description: 'Request an EIN',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '422': {
        description: 'FormationRequest Body is Invalid',
      },
      '200': {
        description: 'FormationRequest instance for EIN Only order',
        content: {
          'application/json': {
            description: 'A Formation Request Object',
            schema: getModelSchemaRef(FormationRequest, {
              title: 'FormationRequest',
              exclude: ['providerMeta'],
            }),
            examples: {
              'example-2': {
                description: 'Example Formation Response for EIN',
                value: {
                  id: '53709c46-45fc-40b9-aa83-be8211b230b7',
                  entity: {
                    name: 'Test Series 1, a Series of Test Master LLC',
                    type: 'LLC',
                    stateOfFormation: 'Delaware',
                  },
                  taxContact: {
                    firstName: 'John',
                    lastName: 'Doe',
                    phone: '555-555-5555',
                    taxDetails: {
                      type: 'ssn',
                    },
                    address: {
                      street1: 'Example Street',
                      city: 'Salt Lake City',
                      postalCode: '84005',
                      state: 'Utah',
                      country: 'United States of America',
                    },
                  },
                  status: 'ORDER_PLACED',
                  ein: null,
                },
              },
            },
          },
        },
      },
    },
  })
  async createEINOnly(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: getModelSchemaRef(FormationRequestEINRequest, {
            title: 'NewFormationRequestEIN',
          }),
          examples: {
            'example-1': {
              description: 'Example Formation Request for EIN',
              value: {
                entity: {
                  name: 'Test Series 1, a Series of Test Master LLC',
                  type: 'LLC',
                  stateOfFormation: 'Delaware',
                },
                taxContact: {
                  firstName: 'John',
                  lastName: 'Doe',
                  phone: '555-555-5555',
                  taxDetails: {
                    type: 'ssn',
                    value: '111-11-1111',
                  },
                  address: {
                    street1: 'Example Street',
                    city: 'Salt Lake City',
                    postalCode: '84005',
                    state: 'Utah',
                    country: 'United States of America',
                  },
                },
              },
            },
          },
        },
      },
    })
    formationRequestEINRequest: FormationRequestEINRequest,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({
      in: 'header',
      name: 'x-tenant-id',
      required: true,
      description: 'Your Tenant ID',
      schema: {
        type: 'string',
      },
    })
    tenantId: string,
  ): Promise<FormationRequest> {
    if (
      !this.unleash.isEnabled('api_legal_formation_requests_ein_only', {
        userId: tenant.id,
      })
    ) {
      throw new HttpErrors.Forbidden();
    }
    return this.formationRequestRepository.create({
      ...(await this.filings.createEINOrder(formationRequestEINRequest)),
      ownerId: profile.id,
    });
  }

  @authenticate('jwt')
  @post('/legal/formation/requests/entity', {
    operationId: 'createEntityRequest',
    summary: 'Order an Entity',
    description: 'Request an Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '422': {
        description: 'FormationRequest Body is Invalid',
      },
      '200': {
        description: 'FormationRequest instance for Entity order',
        content: {
          'application/json': {
            description: 'A Formation Request Object',
            schema: getModelSchemaRef(FormationRequest, {
              title: 'FormationRequest',
              exclude: ['providerMeta'],
            }),
            examples: {
              'example-2': {
                description: 'Example Formation Response for EIN',
                value: {
                  id: '53709c46-45fc-40b9-aa83-be8211b230b7',
                  entity: {
                    name: 'Test Series 1, a Series of Test Master LLC',
                    type: 'LLC',
                    stateOfFormation: 'Delaware',
                  },
                  taxContact: {
                    firstName: 'John',
                    lastName: 'Doe',
                    phone: '555-555-5555',
                    taxDetails: {
                      type: 'ssn',
                    },
                    address: {
                      street1: 'Example Street',
                      city: 'Salt Lake City',
                      postalCode: '84005',
                      state: 'Utah',
                      country: 'United States of America',
                    },
                  },
                  status: 'ORDER_PLACED',
                },
              },
            },
          },
        },
      },
    },
  })
  async createEntityOrder(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: getModelSchemaRef(FormationRequestEntityRequest, {
            title: 'NewFormationRequestEntity',
          }),
        },
      },
    })
    formationRequestRequest: FormationRequestEntityRequest,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<FormationRequest> {
    if (
      !this.unleash.isEnabled('api_legal_formation_requests_entity', {
        userId: tenant.id,
      })
    ) {
      throw new HttpErrors.Forbidden();
    }

    const formationRequest = new FormationRequest({
      ...formationRequestRequest,
      ownerId: profile.id,
      status: FormationRequestStatus.PROCESSING,
      providerMeta: {
        typeId: 'LEGALINC',
      },
    });
    try {
      return await this.filings.createEntityFormationOrder(formationRequest);
    } catch (e) {
      throw new HttpErrors.BadRequest(e);
    }
  }

  @authenticate('jwt')
  @get('/legal/formation/requests/count', {
    operationId: 'formationRequestCount',
    summary: 'Get count of formation requests',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'FormationRequest count',
        content: {
          'application/json': {
            schema: CountSchema,
          },
        },
      },
    },
  })
  async count(
    @param.where(FormationRequest) where?: Where<FormationRequest>,
  ): Promise<Count> {
    return this.formationRequestRepository.count(where);
  }

  @authenticate('jwt')
  @get('/legal/formation/requests', {
    operationId: 'find',
    summary: 'List formation requests',
    description: 'Get a list of Companies for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Array of FormationRequest model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(FormationRequest, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(FormationRequest) filter?: Filter<FormationRequest>,
  ): Promise<FormationRequest[]> {
    return this.formationRequestRepository.find(filter);
  }

  @authenticate('jwt')
  @get('/legal/formation/requests/{id}', {
    operationId: 'find',
    summary: 'Get formation request by id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      description: 'FormationRequest instance by Id',
      content: {
        'application/json': {
          schema: getModelSchemaRef(FormationRequest, {includeRelations: true}),
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(FormationRequest, {exclude: 'where'})
    filter?: FilterExcludingWhere<FormationRequest>,
  ): Promise<FormationRequest> {
    return this.formationRequestRepository.findById(id, filter);
  }
}
