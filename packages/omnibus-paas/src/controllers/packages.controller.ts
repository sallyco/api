import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  put,
  del,
  requestBody,
  response,
  tags,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/core';
import debugFactory from 'debug';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {Package, PackageWithRelations} from '../models';
import {PackageRepository} from '../repositories';

const debug = debugFactory('omnibus-paas:controllers:packages');

@tags('Products')
@visibility(OperationVisibility.UNDOCUMENTED)
export class PackagesController {
  constructor(
    @repository(PackageRepository)
    public packageRepository: PackageRepository,
  ) {}

  @authenticate('jwt')
  @post('/products/packages')
  @response(200, {
    description: 'Package model instance',
  })
  async create(
    @requestBody() productPackage: Omit<Package, 'id'>,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Package> {
    const {products, attributes, ...rest} = productPackage;
    const pack = await this.packageRepository.create(rest);

    //products
    if (products) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newpro = products.map((entry: any) => {
        return this.packageRepository.products(pack.id).link(entry);
      });

      await Promise.all(newpro);
    }

    //attributes
    if (attributes) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newatt = attributes.map((entry: any) => {
        return this.packageRepository.attributes(pack.id).link(entry);
      });

      await Promise.all(newatt);
    }

    return pack;
  }

  @get('/products/packages/count')
  @response(200, {
    description: 'Package model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Package) where?: Where<Package>): Promise<Count> {
    return this.packageRepository.count(where);
  }

  @get('/products/packages')
  @response(200, {
    description: 'Array of Package model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Package, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Package) filter?: Filter<Package>,
  ): Promise<PackageWithRelations[]> {
    return this.packageRepository.find(filter);
  }

  @get('/products/packages/{id}')
  @response(200, {
    description: 'Package model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Package, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Package, {exclude: 'where'})
    filter?: FilterExcludingWhere<Package>,
  ): Promise<PackageWithRelations> {
    return this.packageRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @put('/products/packages/{id}')
  @response(204, {
    description: 'Package PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() productPackage: PackageWithRelations,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    try {
      await this.packageRepository.products(productPackage.id).unlinkAll();
      await this.packageRepository.attributes(productPackage.id).unlinkAll();
    } catch {
      debug("can't unlink on undefined hasMany ", productPackage.id);
    }

    let pack;
    //products
    if (productPackage.products) {
      const {products, ...rest} = productPackage;
      pack = rest;

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newpro = products.map((entry: any) => {
        return this.packageRepository.products(productPackage.id).link(entry);
      });

      await Promise.all(newpro);
    } else {
      pack = productPackage;
    }

    //attributes
    if (productPackage.attributes) {
      const {attributes, ...rest} = pack;
      pack = rest;

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const newatt = attributes.map((entry: any) => {
        return this.packageRepository.attributes(productPackage.id).link(entry);
      });

      await Promise.all(newatt);
    }
    await this.packageRepository.replaceById(id, pack);
  }

  @authenticate('jwt')
  @del('/products/packages/{id}')
  @response(204, {
    description: 'Package DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.packageRepository.products(id).unlinkAll();
    await this.packageRepository.attributes(id).unlinkAll();

    await this.packageRepository.deleteById(id);
  }
}
