/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import {
  getModelSchemaRef,
  get,
  response,
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  requestBody,
  tags,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import debugFactory from 'debug';
import {ExternalBankAccount, Entities, File, Global} from '../models';
import {EntitiesList} from '../models/entities-list.model';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  CompanyRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  GlobalRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
  TransactionRepository,
  BankingBeneficiaryRepository,
} from '../repositories';
import {
  Banking,
  BankingProvider,
  BlueSkyFeeService,
  BlueSkySummary,
  CeleryClientService,
  FilingsService,
  SystemNotificationsService,
  TransactionService,
  TreasuryPrimeAccountTransaction,
} from '../services';
import moment from 'moment';
import _ from 'lodash';
const debug = debugFactory('omnibus-paas:entities:controller');
/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by entities
 * Everything about your Entities
 */
@tags('Entities')
export class EntitiesController {
  constructor(
    @repository(EntitiesRepository)
    public entityRepository: EntitiesRepository,
    @repository(CompanyRepository)
    public companyRepository: CompanyRepository,
    @repository(CloseRepository)
    public closeRepository: CloseRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(TransactionRepository)
    protected transactionRepository: TransactionRepository,
    @repository(ProfileRepository)
    protected profileRepository: ProfileRepository,
    @repository(SubscriptionRepository)
    protected subscriptionRepository: SubscriptionRepository,
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @repository(GlobalRepository)
    public globalRepository: GlobalRepository,
    @service(TransactionService)
    private transactionService: TransactionService,
    @repository(TenantRepository)
    protected tenantRepository: TenantRepository,
    @service(BlueSkyFeeService)
    private blueSkyService: BlueSkyFeeService,
    @repository(BankingBeneficiaryRepository)
    private bankingBeneficiaryRepository: BankingBeneficiaryRepository,
    @service(SystemNotificationsService)
    private readonly systemNotifications: SystemNotificationsService,
  ) {}

  /**
   * This endpoint lists all the entities
   *

   * @returns A paged array of entities
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   */
  @authenticate('jwt')
  @operation('get', '/entities', {
    operationId: 'getEntities',
    summary: 'List Entities',
    description: 'List Entities for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(EntitiesList),
          },
        },
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async listEntities(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<EntitiesList> {
    let countAndStatement = {};
    let listFindStatement = {};
    if (tenant.id !== 'master') {
      const builder = new WhereBuilder({isDeleted: false});
      countAndStatement = builder.impose(profile.where ?? {}).build();
      listFindStatement = {
        where: builder.impose(profile.where ?? {}).build(),
      };
    } else {
      countAndStatement = {and: [{isDeleted: false}]};
      listFindStatement = {
        where: {and: [{isDeleted: false}]},
      };
    }

    const countObj = await this.entityRepository.count(countAndStatement);

    return new EntitiesList({
      totalCount: countObj.count,
      data: await this.entityRepository.find(listFindStatement),
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/entities', {
    operationId: 'createEntity',
    summary: 'Create an Entity',
    description: 'Create an entity with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Entities),
          },
        },
      },
    },
  })
  async createEntity(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Entities, {
            title: 'NewEntity',
            exclude: ['id'],
          }),
        },
      },
    })
    entity: Entities,
  ): Promise<Entities> {
    entity.ownerId = profile.id;
    entity.tenantId = tenant.id;

    if (!entity.managementFee) {
      entity.managementFee = {
        amount: 0,
        type: 'percent',
      };
    }
    if (!entity.expenseReserve) {
      entity.expenseReserve = {
        amount: 0,
        type: 'Flat Fee',
      };
    }
    return this.entityRepository.create(entity);
  }

  /**
   *
   *

   * @param entityId The id of the entity to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/entities/{entityId}', {
    operationId: 'getEntity',
    summary: 'Get an Entity by Id',
    description: 'Get the Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Entities),
          },
        },
      },
    },
  })
  async showEntityById(
    @param({name: 'entityId', in: 'path'}) entityId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Entities) filter?: Filter<Entities>,
  ): Promise<Entities> {
    const entity = await this.entityRepository.findById(entityId, filter);
    if (!entity?.tenantId) {
      entity.tenantId = tenant.id;
      await this.entityRepository.save(entity);
    }
    //  if (!entity.isDeleted && entity.ownerId === profile.id) {
    return entity;
    // } else {
    //   throw new HttpErrors.NotFound(
    //     'The entity you are trying to find cannot be found.',
    //   );
    // }
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/entities/current-series-name')
  async currentEntityName(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<string> {
    const pad = '000';

    //if (tenant?.masterEntity) {
    //  const ten = await this.tenantRepository.findById(tenant.url);
    //  const company = await this.companyRepository.findById(tenant.url);

    //  const value = company.seriesNumber;

    //  return `Fund ${(pad + value).slice(
    //    -pad.length,
    //  )}, a series of ${company.name}`;

    //}

    const value = await this.globalRepository.findOne({
      where: {name: 'series_number'},
    });

    debug('value', value);

    if (!value) {
      const obj = new Global({
        name: 'series_number',
        value: 1,
      });

      const globalObj = await this.globalRepository.create(obj);
      debug('globalObj', globalObj);
      return `Series ${(pad + globalObj.value).slice(
        -pad.length,
      )}, a series of Glassboard Master II LLC`;
    }

    return `Series ${(pad + value.value).slice(
      -pad.length,
    )}, a series of Glassboard Master II LLC`;
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('post', '/entities/next-series-name')
  async nextEntityName(
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<string> {
    const pad = '000';
    let value = await this.globalRepository.findOne({
      where: {name: 'series_number'},
    });

    debug('value', value);

    if (!value) {
      const obj = new Global({
        name: 'series_number',
        value: 1,
      });

      const globalObj = await this.globalRepository.create(obj);
      debug('globalObj', globalObj);
      return `Series ${(pad + globalObj.value).slice(
        -pad.length,
      )}, a series of Glassboard Master II LLC`;
    }
    const obj = new Global({
      name: 'series_number',
      value: value.value + 1,
    });

    await this.globalRepository.updateById(value.id, obj);
    value = await this.globalRepository.findOne({
      where: {name: 'series_number'},
    });
    return `Series ${(pad + obj.value).slice(
      -pad.length,
    )}, a series of Glassboard Master II LLC`;
  }

  /**
   *
   *

   * @param profile
   * @param entityId The id of the entity to retrieve
   * @param tenant
   * @param entity
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/entities/{entityId}', {
    operationId: 'updateEntity',
    summary: 'Update an Entity',
    description: 'Update an Entity with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Entities),
          },
        },
      },
    },
  })
  async updateEntityById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'entityId', in: 'path'}) entityId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Entities, {partial: true}),
        },
      },
    })
    entity: Entities,
  ): Promise<Entities> {
    //const retrivedEntity = await this.entityRepository.findById(entityId);

    //if (!retrivedEntity.isDeleted && entity.ownerId === profile.id) {
    entity.updatedAt = new Date();
    if (tenant.id !== 'master') entity.tenantId = tenant.id;
    await this.entityRepository.updateById(entityId, entity);
    return this.entityRepository.findById(entityId);
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The entity you are trying to update cannot be found.',
    //  );
    //}
  }

  /**
   *
   *

   * @param profile
   * @param entityId The id of the entity to retrieve
   * @param tenant
   * @param partialEntity
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('patch', '/entities/{entityId}')
  async patchEntityById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'entityId', in: 'path'}) entityId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Entities, {partial: true}),
        },
      },
    })
    partialEntity: Entities,
  ): Promise<Entities> {
    const retrivedEntity = await this.entityRepository.findById(entityId);

    //if (!retrivedEntity.isDeleted && entity.ownerId === profile.id) {
    partialEntity.updatedAt = new Date();
    partialEntity.tenantId = tenant.id;
    await this.entityRepository.updateById(entityId, {
      ...retrivedEntity,
      ...partialEntity,
    });
    return this.entityRepository.findById(entityId);
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The entity you are trying to update cannot be found.',
    //  );
    //}
  }

  /**
   *
   *

   * @param entityId The id of the entity to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/entities/{entityId}', {
    operationId: 'deleteEntity',
    summary: 'Delete an Entity',
    description: 'Delete the Entity with the specified Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      '404': {
        description:
          'Not Found. The entity you are trying to delete cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async softDeleteEntityById(
    @param({name: 'entityId', in: 'path'}) entityId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const entity = await this.entityRepository.findById(entityId);

    if (
      entity.isDeleted ||
      (entity.ownerId !== profile.id &&
        entity.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The entity you are trying to delete cannot be found.',
      );
    } else {
      await this.entityRepository.updateById(entityId, {
        deletedAt: new Date(),
        isDeleted: true,
      });
    }
  }

  @authenticate('jwt')
  @operation('post', '/entities/{entityId}/generate-documents/{documentType}', {
    operationId: 'generateEntityDocument',
    summary: 'Generate a document for an Entity',
    description: 'Generate the specified documentType for the Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'text/plain': {
            schema: getModelSchemaRef(File, {partial: true}),
          },
        },
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async populateTemplate(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(CeleryClientService) celeryClient: CeleryClientService,
    @param.path.string('entityId') entityId: string,
    @param.path.string('documentType') documentType: string,
    @requestBody() documentData: any,
  ): Promise<File> {
    return celeryClient.generateEntityDocument(
      entityId,
      documentType,
      documentData,
      userProfile,
      tenant,
    );
  }

  @authenticate('jwt')
  @operation('get', '/entities/{entityId}/documents/{documentType}', {
    operationId: 'getEntityDocument',
    summary: 'Get a document for an Entity',
    description: 'Get the specified documentType for the Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'text/plain': {
            schema: getModelSchemaRef(File, {partial: true}),
          },
        },
      },
      '424': {
        description: `Failed Dependency
        
          * Entity is not connected to a deal.
          * The entity's deal does not have a profile.
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async getPopulateTemplate(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(CeleryClientService) celeryClient: CeleryClientService,
    @param.path.string('entityId') entityId: string,
    @param.path.string('documentType') documentType: string,
  ): Promise<File> {
    // acquire the required data objects
    const entity = await this.entityRepository.findById(entityId);
    const deal = await this.entityRepository.deal(entity.id);
    if (!deal) {
      throw new HttpErrors.FailedDependency(
        'Entity is not connected to a deal',
      );
    }
    const profile = await this.dealRepository.profile(deal.id);
    if (!profile) {
      throw new HttpErrors.FailedDependency(
        "The entity's deal does not have a profile",
      );
    }

    let manager = await this.profileRepository.manager(profile.id);
    if (!manager) {
      manager = await this.tenantRepository.manager(tenant.id);
    }

    let masterEntity = await this.profileRepository.masterEntity(profile.id);
    if (!masterEntity) {
      masterEntity = await this.tenantRepository.masterEntity(tenant.id);
    }

    // generate derived fields
    const organizerName = prof => {
      // This handles runtime errors where async calls haven't filled in "profile" yet.
      if (!prof) {
        return '';
      }
      // Prefer 'name' before using signer's "firtstName lastName"
      const {name, firstName, lastName} = prof;

      if (name) return name;
      if (firstName && lastName) return `${firstName} ${lastName}`;
      if (lastName) return lastName;
      if (firstName) return firstName;
      return '';
    };

    const calculateCarryPercentage = dl => {
      let percentage = Number(dl?.organizerCarryPercentage ?? 0);
      if (
        Array.isArray(dl?.additionalCarryRecipients) &&
        dl?.additionalCarryRecipients?.length > 0
      ) {
        percentage += dl.additionalCarryRecipients.reduce(
          (accl, carry) => accl + (carry.carryPercentage ?? 0),
          0,
        );
      }
      return percentage;
    };

    function formatTypeOfEntity(str) {
      str = str.split('_');
      for (let i = 0, x = str.length; i < x; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].toLowerCase().substr(1);
      }
      return str.join(' ');
    }

    //
    const derivedData = {
      dealName: deal.name.trim(),
      entityType: 'LIMITED_LIABILITY_COMPANY',
      fileType: 'pdf',
      dictionaryData: {
        entityLegalName: entity.name, // VARIABLE FROM API
        // VARIABLES GATHERED FROM UI //
        organizerName: organizerName(profile), // VARIABLE FROM FORMh
        fundEntityType: 'Limited Liability Company', //data.entityType, // VARIABLE FROM FORM
        fundStateOfFormation: 'Delaware', // FIXED
        carryPercentage:
          tenant.id === 'assure'
            ? deal.organizerCarryPercentage
            : calculateCarryPercentage(deal).toString(), // VARIABLE FROM FORM
        additionalCarryRecipientsNames: deal.additionalCarryRecipients
          ? _.map(deal.additionalCarryRecipients, 'individual.name').join(', ')
          : '',
        additionalCarryRecipientsPercentages: deal.additionalCarryRecipients
          ? _.map(
              deal.additionalCarryRecipients,
              i => `${i.individual.name} (${i.carryPercentage}%)`,
            ).join(', ')
          : '',
        organizerState: profile.stateOfFormation
          ? profile.stateOfFormation
          : profile.address?.state, // VARIABLE FROM FORM
        managementFee: entity?.managementFee?.amount ? true : false, //data.managementFee, // VARIABLE FROM FORM
        //portfolioCompany: data.portfolioCompanyName, // VARIABLE FROM FORM
        //portfolioCompanyEntityType: "Limited Liability Company", //data.entityType, // VARIABLE FROM FORM (Same as Entity Name)
        organizerEntityType: profile.typeOfEntity
          ? formatTypeOfEntity(profile.typeOfEntity)
          : 'Limited Liability Company', // VARIABLE FROM FORM (Based on organizer profile, ENTITY or INDIVIDUAL)
        minimumSubscriptionAmount: `"$${entity.minInvestmentAmount}`, // VARIABLE FROM FORM

        //portfolioCompanyState: data.portfolioCompanyState, //"Delaware", // FIXED
        assureRole: 'Administrative Manager', // FIXED

        arbitrationCity: manager?.arbitrationCity ?? 'Salt Lake City', // FIXED (this was in a previous form, but was removed)
        arbitrationState: manager?.arbitrationState ?? 'Utah', // FIXED (this was in a previous form, but was removed)

        managerName: manager?.name ?? 'Glassboard Management II, LLC', // FIXED
        managerEmail: manager?.email ?? 'funds@glassboardtech.com', // FIXED
        managerFullAddress: `${manager?.address?.address1 ?? ''} ${
          manager?.address?.address2
            ? ', ' + manager.address.address2 + ' '
            : ''
        }${manager?.address?.city ?? ''}, ${manager?.address?.state ?? ''} ${
          manager?.address?.postalCode ?? ''
        }`,
        managerAddress1: `${manager?.address?.address1 ?? ''} ${
          manager?.address?.address2 ? ', ' + manager.address.address2 : ''
        }`,
        managerAddress2: `${manager?.address?.city ?? ''}, ${
          manager?.address?.state ?? ''
        } ${manager?.address?.postalCode ?? ''}`,
        managerPhone: manager?.phone ?? '',
        managerContact: manager?.name ?? 'Jeremy Neilson', // FIXED
        managerState: manager?.stateOfFormation ?? 'Utah', // FIXED
        managerEntityType: manager?.entityType ?? 'Limited Liability Company', // FIXED
        managementFeePercent: entity?.managementFee?.amount ?? '0', // FIXED
        managementFeeTerm: '7', // FIXED - Ready for other values if they show up
        managementFeeFrequency: entity?.managementFee?.isRecurring
          ? entity?.managementFee?.frequency ?? 'Annual'
          : 'Upfront',
        //master: "Glassboard Master II, LLC", // FIXED
        master: masterEntity?.name ?? 'Glassboard Master II, LLC', // FIXED
        masterStateOfFormation: masterEntity?.stateOfFormation ?? 'Delaware', //
        masterEntityType: 'Limited Liability Company', // OMITTED FOR NOW ("master" fixed to false)
        multipleClasses: false, // FIXED
        isVentureStrategy: false, // FIXED
        singleOrMulti: 'Multi', // FIXED
        feesPaidByFund: true, // FIXED
        isCrypto: false, // FIXED
        effectiveDate: Date.now() / 1000, // GENERATED
      },
    };

    return celeryClient.generateEntityDocument(
      entityId,
      documentType,
      derivedData,
      userProfile,
      tenant,
    );
  }

  @authenticate('jwt')
  @operation('post', '/entities/{entityId}/series-ein', {
    operationId: 'createEntitySeriesEIN',
    summary: 'Create an EIN for the Series Entity',
    description: 'Creates a request for an EIN on the specified Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '204': {
        description:
          'No return value. If success, the response status will be 204',
      },
      '404': {
        description:
          'Not Found. The entity you are trying to modify cannot be found.',
      },
      '422': {
        description: 'Unprocessable Entity',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async orderEINForSeries(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(FilingsService) filings: FilingsService,
    @param.path.string('entityId') entityId: string,
  ) {
    const entity = await this.entityRepository.findById(entityId);
    if (entity.isDeleted) {
      throw new HttpErrors.NotFound(
        'The entity you are trying to modify cannot be found.',
      );
    }
    const deal = await this.entityRepository.deal(entity.id);
    try {
      await filings.createEntityEINOrder(entity, deal);
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }

  @authenticate('jwt')
  @operation('post', '/entities/{entityId}/bank-account', {
    operationId: 'createEntityBankAccount',
    summary: 'Create a Bank Account for an Entity',
    description:
      'Creates a Bank Account for the Series Entity. ' +
      'An EIN for the entity is required for this request to succeed. ' +
      'Only one Bank Account is allowed per Entity',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '204': {
        description:
          'No return value. If success, the response status will be 204',
      },
      '409': {
        description: 'Conflict. Entity has existing banking account.',
      },
      '422': {
        description: 'Unprocessable Entity',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async createBankAccountForEntity(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('entityId') entityId: string,
  ): Promise<void> {
    const entity = await this.entityRepository.findById(entityId);
    if (entity.bankAccount) {
      throw new HttpErrors.Conflict('Entity has existing banking account');
    }
    try {
      entity.bankAccount = await bankingService.createEntityDepositAccount(
        entity,
      );
      await this.entityRepository.update(entity);
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }

  @authenticate('jwt')
  @operation('get', '/entities/{entityId}/bank-account/details', {
    operationId: 'getEntityBankAccountBalance',
    summary: 'Get details about the Entity Bank Account',
    description: 'Returns details about the Entity Bank Account (if present)',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ExternalBankAccount, {partial: true}),
          },
        },
      },
      '404': {
        description: 'Not Found. Entity not found.',
      },
      '424': {
        description:
          'Failed Dependency. Entity does not have an existing banking account.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async getBankAccountDetails(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('entityId') entityId: string,
  ): Promise<Partial<ExternalBankAccount>> {
    const entity = await this.entityRepository.findById(entityId);
    if (!entity?.bankAccount?.providerMeta?.accountId) {
      throw new HttpErrors.FailedDependency(
        'Entity does not have an existing banking account',
      );
    }
    if (profile.id !== entity.ownerId) {
      throw new HttpErrors.NotFound('Entity not found');
    }
    return bankingService.getDepositAccountDetails(
      entity.bankAccount.providerMeta.accountId,
    );
  }

  @authenticate('jwt')
  @operation('get', '/entities/{entityId}/bank-account/balance', {
    operationId: 'getEntityBankAccountBalance',
    summary: 'Get the balance from the Entity Bank Account',
    description:
      'Returns the balance from the Entity Bank Account (if present)',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                balance: {
                  type: 'number',
                },
                availableBalance: {
                  type: 'number',
                },
              },
            },
          },
        },
      },
      '404': {
        description: 'Not Found. Entity not found.',
      },
      '422': {
        description: 'Unprocessable Entity',
      },
      '424': {
        description:
          'Failed Dependency. Entity does not have an existing banking account.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async getBankAccountBalance(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('entityId') entityId: string,
  ): Promise<object> {
    const entity = await this.entityRepository.findById(entityId);
    if (!entity?.bankAccount?.providerMeta?.accountId) {
      throw new HttpErrors.FailedDependency(
        'Entity does not have an existing banking account',
      );
    }
    if (tenant.id !== 'master' && profile.id !== entity.ownerId) {
      throw new HttpErrors.NotFound('Entity not found');
    }
    try {
      const providerBankDetails = await bankingService.getDepositAccountDetails(
        entity.bankAccount.providerMeta.accountId,
      );

      return {
        balance: providerBankDetails.currentBalance ?? 0,
        availableBalance: providerBankDetails.availableBalance ?? 0,
      };
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }

  @authenticate('jwt')
  @operation('get', '/entities/{entityId}/bank-account/transactions', {
    operationId: 'getEntityBankAccountTransactions',
    summary: 'Get the Transactions from the Entity Bank Account',
    description:
      'Returns the Transaction Listing from the Entity Bank Account (if present)',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  amount: {
                    type: 'number',
                    minimum: 0.01,
                    multipleOf: 0.01,
                    examples: [
                      '100.02',
                      '0.05',
                      '19.95',
                      '255.50',
                      '120000.01',
                    ],
                    description:
                      'Transaction amount.  Note that transactions with type `hold` have an amount, but they do not change the balance.',
                  },
                  date: {
                    type: 'string',
                    format: 'date',
                    description: 'ISO 8601 format ("YYYY-MM-DD")',
                    examples: ['2021-02-21', '2020-03-15'],
                  },
                  desc: {
                    type: 'string',
                    description: 'Transaction description.',
                    examples: ['EXT-XFER Sq. Nbr.: 123456'],
                  },
                  book_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the Booking Transfer object that originated this transaction, if any. Otherwise null.',
                    examples: ['book_1k8s7102ehgtbk3'],
                  },
                  type: {
                    type: ['string', 'null'],
                    enum: [
                      'charge',
                      'deposit',
                      'hold',
                      'hold_release',
                      'interest',
                      'reversal',
                      'withdrawal',
                    ],
                    description:
                      'Type of transaction. One of `charge`, `deposit`, `hold`, `hold_release`, `interest`, `payment`, `reversal`, `withdrawal`, or `null`.',
                  },
                  summary: {
                    type: 'string',
                    description: 'Summary description of the transaction.',
                    examples: ['ACME CORP VENDOR PMT'],
                  },
                  balance: {
                    type: 'string',
                    minLength: 1,
                    multipleOf: 0.01,
                    examples: [
                      '100.01',
                      '0.05',
                      '19.95',
                      '255.50',
                      '120000.01',
                    ],
                    description:
                      'Account balance immediately after this transaction. Transactions of type `hold` do not affect the balance.',
                  },
                  id: {
                    type: 'string',
                    description: 'ID for this transaction.',
                    examples: ['ctx_1k8s7102eedgtbk1'],
                  },
                  ach_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the ACH object that originated this transaction, if any. Otherwise null.',
                    examples: ['ach_11gkqac4ddx61n'],
                  },
                  wire_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the Wire object that originated this transaction, if any. Otherwise null.',
                    examples: ['wire_11gkqac4ddx76b'],
                  },
                  wire: {
                    type: ['string', 'null'],
                    description:
                      'For wire transactions, the Fedwire description, if any. Otherwise null.',
                    examples: ['ACME CORP VENDOR PMT'],
                  },
                  fingerprint: {
                    type: 'string',
                    description: 'A unique fingerprint for this transaction.',
                    examples: ['2k7vxetham4i1v3ceb5vs6'],
                  },
                },
              },
            },
            examples: {
              'Incoming ACH Transaction': {
                description: 'Example of DEBIT ACH Transaction',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkqangdfdfs',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '5.00',
                    id: 'ctx_1k8s7102egddtgs',
                    ach_id: 'ach_11gkqangdfdfs',
                    fingerprint: '4ef07ba7ce76451240bce807089faf672267bbe5',
                  },
                ],
              },
              'Outgoing ACH Transaction': {
                description: 'Example of CREDIT ACH Transaction',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkqangdfdfs',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '5.00',
                    id: 'ctx_1k8s7102egdgtbk6',
                    ach_id: 'ach_11gkqangdfdfs',
                    fingerprint: '4ef07ba7ce76451240bce807089faf672267beb7',
                  },
                ],
              },
              'Multiple Transaction Types': {
                description: 'Example of Multiple Transaction Types',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '113.00',
                    date: '2021-09-17',
                    wire_id: 'wire_11gm9qnadpsdsw',
                    desc: 'WIRE OUT John Doe',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '0.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7502epdp5ws1',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: 'bbd13484e9b77e6e9e96cb571b2bf1b7c76dfdsa2',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '4.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkq9scddwdsdf',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '113.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102endse3s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9scddwdsdf',
                    fingerprint: 'b1efec6fc9d429dbe10a8794e091c47933b0dfsdf',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkq9g1dwvts',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '117.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102emdfae4s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g1dwvts',
                    fingerprint: 'b696b8dbce64ac98a7d3990779314afac3sd23sa4',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq9g0dd12d23s',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '122.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ekdsds34sd',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g0dd12d23s',
                    fingerprint: 'c7977c438c1c21451f3b664ceaeasdfa3sdf23sd',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq9g0dddws4s',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '127.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ea11ds',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g0dddws4s',
                    fingerprint: '0af22c3404976a8d7556b29ebe7f7ab6033ds23s',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq8x83sd229',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '137.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ehdgtbfs',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x83sd229',
                    fingerprint: '3fc6f1929404f2b2c4bbe04d8900adfdc233aa3se',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq8x8nsdfy',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '147.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s71dfsdftbft',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x8nsdfy',
                    fingerprint: '3c384ecf210afe3c72d8bd399b12fb213234adc8c1',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkq8x8ddwddsb',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '152.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102dgdgtbfv',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x8ddwddsb',
                    fingerprint: '4d2453eb1585981ec7141495ac846444adsded102',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '1.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Transfer from 420433323',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '162.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s2eeasd86',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: '90966551c9a79b2d03c87f477aa88434c922232397',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '1.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Transfer from 42049933',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '161.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02eddcvn87',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: '1a4998bb6d94462886f5ec1d2934ac782drd00e',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '100.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'WIRE IN JOHN DOE',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '160.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02ssbssda23eg',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: 'e411211d334a01e44fe50b413216099151d91003',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkj0dgdb4dfdw',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '60.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02ebdc7z4r',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj0dgdb4dfdw',
                    fingerprint: '6114a6969f0002424abdbde82c82383a3sdf53ed2b',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkj0dsdfkp9h',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '55.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02a3sdc7z4s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj0dsdfkp9h',
                    fingerprint: 'b26a8712ae05528a6ad09e5d3454s86f1636c81081',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkj19mmabbm299',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '45.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02e9dc7z4t',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj19mmabbm299',
                    fingerprint: '5cad515b1fec309e5f86aba36bd389a5445d8d459',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async getBankAccountTransactions(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('entityId') entityId: string,
  ): Promise<TreasuryPrimeAccountTransaction[]> {
    const entity = await this.entityRepository.findById(entityId);
    if (!entity?.bankAccount?.providerMeta?.accountId) {
      throw new HttpErrors.FailedDependency(
        'Entity does not have an existing banking account',
      );
    }
    if (profile.id !== entity.ownerId) {
      throw new HttpErrors.NotFound('Entity not found');
    }
    try {
      const transactions: TreasuryPrimeAccountTransaction[] =
        await bankingService.getDepositAccountTransactions(entity.bankAccount, {
          pageNumber: 0,
          pageSize: 500,
        });
      const ACHIDs = transactions
        .map(transaction => this.transactionService.hasACHId(transaction))
        .filter(id => id);
      const achTransactionsWhere = new WhereBuilder({
        transactionId: {
          inq: ACHIDs,
        },
      });
      const AchTransactions = await this.transactionRepository.find(
        achTransactionsWhere,
      );
      const wireTransactionsWhere = new WhereBuilder({
        bankTransactionIds: {
          exists: true,
          nin: [[], null],
        },
        dealId: entity.dealId,
      });
      const WireSubscriptions = await this.subscriptionRepository.find(
        wireTransactionsWhere,
      );
      return await Promise.all(
        transactions.map(async transaction => {
          const getInvestorNameFromProfile = async (
            profileId: string,
          ): Promise<string | undefined> => {
            const profile_names = await this.profileRepository.findOne({
              where: {
                id: profileId,
              },
              fields: {
                firstName: true,
                lastName: true,
                name: true,
              },
              include: [],
            });
            return profile_names?.firstName && profile_names?.lastName
              ? profile_names.firstName + ' ' + profile_names.lastName
              : profile_names?.name;
          };

          const matchingSubscription = WireSubscriptions.find(subscription =>
            subscription.bankTransactionIds?.includes(transaction.id),
          );
          if (matchingSubscription) {
            transaction.investor_name = await getInvestorNameFromProfile(
              matchingSubscription.profileId,
            );
          }

          transaction.ach_id = this.transactionService.hasACHId(transaction);
          clearGlassboardInfo(transaction);
          transaction.consolidated_description =
            getCombinedDescription(transaction);

          if (transaction.ach_id) {
            transaction.transaction_type = 'ACH';
            const AchTransaction = AchTransactions.find(
              achTransaction =>
                achTransaction.transactionId === transaction.ach_id,
            );
            if (AchTransaction?.subscriptionId) {
              transaction.subscription_id = AchTransaction.subscriptionId;
              // Grab profile info and add to transaction
              const subscription = await this.subscriptionRepository.findById(
                String(transaction.subscription_id),
              );
              transaction.investor_name = await getInvestorNameFromProfile(
                subscription.profileId,
              );

              // Make sure description isn't blank
              if (transaction.consolidated_description === '') {
                transaction.consolidated_description =
                  getCounterPartyDescription(
                    transaction?.transaction_type ?? '',
                    transaction,
                    subscription.bankAccount,
                  );
              }
            }
          }
          if (transactionIsWire(transaction)) {
            transaction.transaction_type = 'WIRE';
          }

          return transaction;
        }),
      );
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }

  @authenticate('jwt')
  @operation('post', '/entities/{entityId}/initACHTransfer', {
    operationId: 'postACHTransfer',
    summary: 'Send ACH out',
    description: 'This is used to send money out using a ach transfer',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'boolean (true/false)',
      },
      '403': {
        description:
          'Forbidden. You are not authorized to initiate a wire transfer for this entity.',
      },
      '404': {
        description: 'Not Found. Entity not found',
      },
      '422': {
        description:
          'Unprocessable Entity. No bank account found for the entity.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async postACHTransfer(
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.path.string('entityId') entityId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['amount', 'beneficiaryId'],
            properties: {
              amount: {
                type: 'number',
                minimum: 0.01,
              },
              beneficiaryId: {
                type: 'string',
                minLength: 1,
              },
            },
          },
        },
      },
    })
    request: {
      amount: number;
      beneficiaryId: string;
      closeId?: string;
    },
  ): Promise<void | boolean> {
    let needsApproval = true;
    const tenantData = await this.tenantRepository.findById(tenant.id);
    needsApproval = tenantData?.settings?.canSelfApproveWires ? false : true;

    const entity = await this.entityRepository.findById(entityId);
    if (!entity) {
      throw new HttpErrors.NotFound('Entity not found');
    }

    if (profile.id !== entity.ownerId) {
      throw new HttpErrors.Forbidden(
        'You are not authorized to initiate a wire transfer for this entity',
      );
    }

    const accountID = entity?.bankAccount?.providerMeta?.accountId ?? '';
    if (!accountID) {
      throw new HttpErrors.UnprocessableEntity(
        'No bank account found for the entity',
      );
    }
    const {beneficiaryId, amount} = request;
    const beneficiary = await this.bankingBeneficiaryRepository.findById(
      beneficiaryId,
    );
    const transaction =
      await this.transactionService.createACHTransactionForEntity(
        entityId,
        'CREDIT',
        amount,
        beneficiary?.providerMeta?.id,
        request.closeId ?? undefined,
        needsApproval,
      );

    if (transaction.needsApproval) {
      await this.systemNotifications.Notify_Transaction_Approval_Needed(
        transaction,
      );
    }

    return !!(transaction ?? false);
  }
  @authenticate('jwt')
  @operation('post', '/entities/{entityId}/initTransfer', {
    operationId: 'postWireTransfer',
    summary: 'Send wires out',
    description: 'This is used to send money out using a wire transfer',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'boolean (true/false)',
      },
      '400': {
        description:
          'Bad Request. Scheduled Transaction date cannot be in the past.',
      },
      '403': {
        description:
          'Forbidden. You are not authorized to initiate a wire transfer for this entity.',
      },
      '404': {
        description: 'Not Found. Entity not found.',
      },
      '422': {
        description:
          'Unprocessable Entity. No bank account found for the entity.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async postWireTransfer(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('entityId') entityId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['amount', 'beneficiaryId', 'instructions'],
            properties: {
              amount: {
                type: 'number',
                minimum: 0.01,
              },
              beneficiaryId: {
                type: 'string',
                minLength: 1,
              },
              instructions: {
                type: 'string',
                minLength: 1,
              },
              scheduledForDate: {
                type: 'string',
                format: 'date',
              },
            },
          },
        },
      },
    })
    request: {
      amount: number;
      beneficiaryId: string;
      instructions: string;
      closeId?: string;
      scheduledForDate?: string;
    },
  ): Promise<void | boolean> {
    let needsApproval = true;
    const tenantData = await this.tenantRepository.findById(tenant.id);
    needsApproval = tenantData?.settings?.canSelfApproveWires ? false : true;

    const entity = await this.entityRepository.findById(entityId);
    if (!entity) {
      throw new HttpErrors.NotFound('Entity not found');
    }

    if (profile.id !== entity.ownerId) {
      throw new HttpErrors.Forbidden(
        'You are not authorized to initiate a wire transfer for this entity',
      );
    }

    const accountID = entity?.bankAccount?.accountNumber ?? '';
    if (!accountID) {
      throw new HttpErrors.UnprocessableEntity(
        'No bank account found for the entity',
      );
    }
    if (
      request.scheduledForDate &&
      moment(request.scheduledForDate).isBefore(moment())
    ) {
      throw new HttpErrors.BadRequest(
        'Scheduled Transaction date cannot be in the past',
      );
    }

    const {beneficiaryId, amount, instructions} = request;
    try {
      const transaction =
        await this.transactionService.createWireTransactionForEntity(
          entityId,
          'CREDIT',
          amount,
          beneficiaryId,
          instructions,
          request.closeId ?? undefined,
          request.scheduledForDate
            ? moment(request.scheduledForDate).toDate()
            : undefined,
          needsApproval,
        );

      if (transaction.needsApproval) {
        await this.systemNotifications.Notify_Transaction_Approval_Needed(
          transaction,
        );
      }

      return !!(transaction ?? false);
    } catch (err) {
      throw new HttpErrors.UnprocessableEntity(err.toString());
    }
  }

  /**
   * @param entityId The id of the entity to get fees for
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/entities/{entityId}/blue-sky-amounts', {
    operationId: 'getStateInvestmentAmountsForEntity',
    summary: 'Gets the Blue Sky fees related to this entity',
    description:
      'Given a specific entity id, this will return a count of investors per state, the amount invested per state, and the date of the first investment',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Entities),
          },
        },
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async getStateInvestmentAmountsForEntity(
    @param({name: 'entityId', in: 'path'}) entityId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<BlueSkySummary> {
    const entity = await this.entityRepository.findById(entityId);
    return this.blueSkyService.getInvestmentAmountsByState(entity.dealId);
  }

  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @get('/entities-to-file')
  @response(200, {
    description: 'Get a list of entities that need to be filed',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Entities, {includeRelations: true}),
        },
      },
    },
  })
  async entitiesToFile(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    const listFindStatement = {
      include: [
        {
          relation: 'formD',
          where: {filedDate: {neq: null}},
        },
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name', 'estimatedCloseDate'],
          },
          where: {disabled: {neq: true}},
        },
      ],
      where: profile.where,
    };

    const values = await this.entityRepository.find(listFindStatement);
    const newValues = await Promise.all(
      values.map(async value => {
        const close = await this.closeRepository.findOne({
          where: {
            entityId: value.id,
          },
        });
        return {...value, close: close};
      }),
    );

    return newValues;
  }

  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @get('/entities-to-file/{entityId}')
  @response(200, {
    description: 'Get a close that needs to be filed',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Entities),
      },
    },
  })
  async entityToFile(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @repository(SubscriptionRepository)
    subscriptionRepository: SubscriptionRepository,
    @param({name: 'entityId', in: 'path'}) entityId: string,
  ): Promise<any> {
    const listFindStatement = {
      include: [
        {
          relation: 'formD',
        },
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name'],
          },
        },
        {
          relation: 'manager',
        },
        {
          relation: 'masterEntity',
        },
      ],
    };
    const entity = await this.entityRepository.findById(
      entityId,
      listFindStatement,
    );
    const close = await this.closeRepository.findOne({
      where: {
        entityId: entity.id,
      },
    });

    const subscriptions = await subscriptionRepository.find({
      where: {
        id: {inq: close?.subscriptions},
      },
    });

    return {...entity, close: close, subscriptions: subscriptions}; //, subscriptions: subscriptions};
  }
}

export function transactionIsWire(
  transaction: TreasuryPrimeAccountTransaction,
): boolean {
  // For now we derive this based on various fields
  /* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
  return Boolean(
    transaction.wire ||
      transaction.desc?.toUpperCase().indexOf('WIRE IN') !== -1 ||
      transaction.summary?.toUpperCase().indexOf('WIRE IN') !== -1,
  );
}

// If the data comes from Glassboard Tech, clear it out
export function clearGlassboardInfo(
  transaction: TreasuryPrimeAccountTransaction,
) {
  ['desc', 'summary', 'wire_description'].forEach(subField => {
    if (transaction[subField]?.indexOf('Glassboard Tech') === 0) {
      transaction.transaction_data = transaction[subField];
      transaction[subField] = '';
    }
  });
}

// combine `<desc> <summary> <wire_description>`
export function getCombinedDescription(
  transaction: TreasuryPrimeAccountTransaction,
): string {
  return `${transaction?.desc ?? ''} ${transaction?.summary ?? ''} ${
    transaction?.wire_description ?? ''
  }`.trim();
}

export function getCounterPartyDescription(
  prefix: string,
  transaction: TreasuryPrimeAccountTransaction,
  bankAccount: ExternalBankAccount | undefined,
): string {
  return `${prefix ?? ''} ${transaction?.type ?? ''}: ${
    bankAccount?.accountName ?? ''
  }`;
}
