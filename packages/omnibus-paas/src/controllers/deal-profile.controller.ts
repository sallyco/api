import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Deal, Profile} from '../models';
import {DealRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class DealProfileController {
  constructor(
    @repository(DealRepository)
    public dealRepository: DealRepository,
  ) {}

  @get('/deals/{id}/profile', {
    responses: {
      '200': {
        description: 'Profile belonging to Deal',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Profile)},
          },
        },
      },
    },
  })
  async getProfile(
    @param.path.string('id') id: typeof Deal.prototype.id,
  ): Promise<Profile> {
    return this.dealRepository.profile(id);
  }
}
