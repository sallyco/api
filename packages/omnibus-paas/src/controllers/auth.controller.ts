/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject} from '@loopback/core';
import {
  tags,
  HttpErrors,
  operation,
  requestBody,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import debugFactory from 'debug';
import KcAdminClient from 'keycloak-admin';
import {Issuer} from 'openid-client';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

const debug = debugFactory('omnibus-paas:auth:controller');

@visibility(OperationVisibility.UNDOCUMENTED)
@tags('OAuth2')
export class AuthController {
  constructor() {}

  async clientAuth() {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',

      grantType: 'password',
      // Admin-Cli can get the realm data and create users in various realms
      clientId: 'admin-cli',
    });

    return kcAdminClient;
  }

  @operation('post', 'oauth2/token', {
    operationId: 'createToken',
    summary: 'Create an Auth Token',
    description: 'Create a token for use with the API',
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async createToken(
    @requestBody() credentials: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    // Check that token.id is valid
    let keycloakIssuer;
    try {
      keycloakIssuer = await Issuer.discover(
        process.env.KC_BASE_URL + '/realms/' + tenant.id,
      );
    } catch (err) {
      debug('err', err);
      throw new HttpErrors.Unauthorized(err.error_description);
    }

    // TODO temp until OMD is update
    if (!credentials.grantType) {
      credentials.grantType = 'password';
    }
    //

    switch (credentials.grantType) {
      case 'client_credentials': {
        if (!credentials.clientId)
          throw new HttpErrors.UnprocessableEntity(
            'clientId is not defined in request',
          );
        if (!credentials.clientSecret)
          throw new HttpErrors.UnprocessableEntity(
            'clientSecret is not defined in request',
          );

        try {
          const client = new keycloakIssuer.Client({
            client_id: credentials.clientId,
            client_secret: credentials.clientSecret,
          });
          const tokenSet = await client.grant({
            grant_type: credentials.grantType,
          });
          return tokenSet;
        } catch (err) {
          debug('err', err);
          throw new HttpErrors.Unauthorized(err.error_description);
        }
      }
      case 'password': {
        // TODO check source URL block anything that isnt GBT
        try {
          const kcAdminClient = await this.clientAuth();

          kcAdminClient.setConfig({
            realmName: tenant.id,
          });
          const cli = await kcAdminClient.clients.find({clientId: 'account'});
          if (!cli || cli.length < 1 || !cli[0].id)
            throw new HttpErrors.UnprocessableEntity('Encountered an error');
          const secret = await kcAdminClient.clients.getClientSecret({
            id: cli[0].id,
          });

          const client = new keycloakIssuer.Client({
            client_id: 'account',
            client_secret: secret.value,
          });

          const tokenSet = await client.grant({
            grant_type: credentials.grantType,
            username: credentials.username,
            password: credentials.password,
          });
          return tokenSet;
        } catch (err) {
          debug('err', err);
          throw new HttpErrors.Unauthorized(err.error_description);
        }
      }
      default:
        throw new HttpErrors.UnprocessableEntity(
          'grantType is not defined in request',
        );
    }
  }

  @operation('post', 'oauth2/refresh', {
    operationId: 'refreshToken',
    summary: 'Refresh an Auth Token',
    description: 'Refresh a token',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async refreshToken(
    @requestBody() credentials: any,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<any> {
    // Check that token.id is valid
    let keycloakIssuer;
    try {
      keycloakIssuer = await Issuer.discover(
        process.env.KC_BASE_URL + '/realms/' + tenant.id,
      );
    } catch (err) {
      debug('err', err);
      throw new HttpErrors.Unauthorized(err.error_description);
    }
    if (!credentials.refresh_token)
      throw new HttpErrors.UnprocessableEntity(
        'refreshToken is not defined in request',
      );

    const kcAdminClient = await this.clientAuth();

    kcAdminClient.setConfig({
      realmName: tenant.id,
    });
    const cli = await kcAdminClient.clients.find({clientId: 'account'});
    if (!cli || cli.length < 1 || !cli[0].id)
      throw new HttpErrors.UnprocessableEntity('Encountered an error');
    const secret = await kcAdminClient.clients.getClientSecret({id: cli[0].id});

    const client = new keycloakIssuer.Client({
      client_id: 'account',
      client_secret: secret.value,
    });

    try {
      const res = client.refresh(credentials.refresh_token);
      return res;
    } catch (err) {
      debug('err', err);
      throw new HttpErrors.Unauthorized(err.error_description);
    }
  }
}
