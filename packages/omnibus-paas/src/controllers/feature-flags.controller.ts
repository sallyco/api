import {inject, service} from '@loopback/core';
import {operation, visibility, OperationVisibility} from '@loopback/rest';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {Unleash, UnleashProvider} from '../services';

@visibility(OperationVisibility.UNDOCUMENTED)
export class FeatureFlagsController {
  constructor(@service(UnleashProvider) private unleash: Unleash) {}

  @operation('get', '/feature-flags')
  async getAllFeatureFlags(
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<object> {
    const featureToggles = this.unleash.getFeatureToggleDefinitions();
    return featureToggles.map(toggle => {
      return {
        name: toggle.name,
        isEnabled: this.unleash.isEnabled(toggle.name, {userId: tenant.id}),
      };
    });
  }
}
