import {authenticate} from '@loopback/authentication';
import {
  getModelSchemaRef,
  HttpErrors,
  operation,
  param,
  requestBody,
  tags,
} from '@loopback/rest';
import {inject, service} from '@loopback/core';
import {
  Banking,
  BankingProvider,
  PlaidService,
  SystemNotificationsService,
  TransactionService,
  TransactionServiceErrors,
  TreasuryPrimeAccountTransaction,
} from '../../services';
import {BankAccount, Transaction} from '../../models';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import List from '../../models/list.model';
import {FilterBuilder, repository, WhereBuilder} from '@loopback/repository';
import {AccountRepository, BankAccountRepository} from '../../repositories';
import {Address} from '../../models/address.dto';
import {getJsonSchema} from '@loopback/repository-json-schema';

@tags('Banking')
export class BankAccountController {
  constructor(
    @service(BankingProvider) private banking: Banking,
    @repository(BankAccountRepository)
    private bankAccountRepository: BankAccountRepository,
    @repository(AccountRepository) private accountRepository: AccountRepository,
    @service(PlaidService) private plaidService: PlaidService,
    @service(SystemNotificationsService)
    private readonly systemNotifications: SystemNotificationsService,
  ) {}

  @authenticate('jwt')
  @operation('get', '/banking/account', {
    operationId: 'getBankAccounts',
    summary: 'List Bank Accounts',
    description: 'List Bank Accounts for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              ...getModelSchemaRef(List),
              ...{
                properties: {
                  data: {
                    type: 'array',
                    itemType: getModelSchemaRef(BankAccount),
                  },
                },
              },
            },
            examples: {
              'example-1': {
                description: 'Example Formation Request for EIN',
                value: [
                  {
                    id: 'd203b77d-c860-491c-a9fa-bf4110e4e8a7',
                    status: 'OPEN',
                    accountNumber: '111111111',
                    routingNumber: '912312312',
                    accountName: 'Test Account',
                    bankName: 'The Provident Bank',
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async listBankAcounts(
    @param({
      name: 'perPage',
      in: 'query',
      schema: {
        type: 'integer',
        minimum: 0,
        maximum: 100,
      },
    })
    perPage = 100,
    @param({
      name: 'page',
      in: 'query',
      schema: {
        type: 'integer',
        minimum: 0,
      },
    })
    page = 0,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<List<BankAccount>> {
    const whereStatement = new WhereBuilder<BankAccount>();
    if (tenant.id !== 'master') {
      whereStatement.eq('ownerId', userProfile.id);
    }

    const filterBuilder = new FilterBuilder<BankAccount>();
    filterBuilder
      .limit(perPage)
      .skip(page * perPage)
      .fields({providerMeta: false})
      .impose(whereStatement);

    const countObj = await this.bankAccountRepository.count(
      whereStatement.build(),
    );

    const data = await this.bankAccountRepository.find(filterBuilder.build());

    return new List({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: data,
    });
  }

  @authenticate('jwt')
  @operation('get', '/banking/account/{accountId}', {
    operationId: 'getBankAccountById',
    summary: 'Get Bank Account by ID',
    description: 'Get Bank Account by Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              ...getModelSchemaRef(BankAccount, {
                exclude: ['providerMeta'],
              }),
              ...{
                parameters: {
                  currentBalance: {
                    type: 'number',
                  },
                  availableBalance: {
                    type: 'number',
                  },
                },
              },
            },
            examples: {
              'example-1': {
                description: 'Example Get Bank Account By Id',
                value: {
                  id: 'd203b77d-c860-491c-a9fa-bf4110e4e8a7',
                  status: 'OPEN',
                  accountNumber: '111111111',
                  routingNumber: '912312312',
                  accountName: 'Test Account',
                  bankName: 'The Provident Bank',
                  currentBalance: 10000.11,
                  availableBalance: 2000.85,
                  bankContact: {
                    name: 'John Doe',
                    phone: '555-555-5555',
                    email: 'john@test.com',
                  },
                  swiftCode: 'PRDIUS33',
                  bankAddress: '5 Market Street Amesbury, MA 01913',
                  createdAt: '2021-03-29T14:45:20.140Z',
                  updatedAt: '2021-03-29T14:45:30.430Z',
                },
              },
            },
          },
        },
      },
    },
  })
  async getBankAccountById(
    @param({
      name: 'accountId',
      in: 'path',
      schema: {
        type: 'string',
      },
    })
    id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<BankAccount> {
    const bankAccount = await this.bankAccountRepository.findById(id);
    if (tenant.id !== 'master' && bankAccount?.ownerId !== userProfile.id) {
      throw new HttpErrors.NotFound();
    }
    if (bankAccount?.providerMeta?.accountId) {
      const providerBankDetails = await this.banking.getDepositAccountDetails(
        bankAccount.providerMeta.accountId,
      );
      bankAccount.currentBalance = providerBankDetails.currentBalance ?? 0;
      bankAccount.availableBalance = providerBankDetails.availableBalance ?? 0;
    }

    delete bankAccount.providerMeta;
    return bankAccount;
  }

  @authenticate('jwt')
  @operation('post', '/banking/account', {
    operationId: 'createBankAccount',
    summary: 'Create a Bank Account',
    description: 'Create a Bank Account with the supplied data',
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankAccount),
            examples: {
              'example-1': {
                description: 'Example Bank Account Creation Response',
                value: {
                  id: 'd203b77d-c860-491c-a9fa-bf4110e4e8a7',
                  status: 'PENDING',
                  accountNumber: null,
                  routingNumber: null,
                  accountName: null,
                  bankName: null,
                  bankContact: null,
                  swiftCode: null,
                  bankAddress: null,
                  createdAt: '2021-03-29T14:45:20.140Z',
                  updatedAt: '2021-03-29T14:45:30.430Z',
                },
              },
            },
          },
        },
      },
    },
  })
  async createBankAccount(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['ein', 'entityName'],
            properties: {
              ein: {
                type: 'string',
                maxLength: 11,
              },
              entityName: {
                type: 'string',
                maxLength: 36,
              },
            },
          },
          examples: {
            'example-1': {
              description: 'Example Bank Account Creation Request',
              value: {
                ein: '12-3456789',
                entityName: 'Example LLC',
              },
            },
          },
        },
      },
    })
    requestData: {
      ein: string;
      entityName: string;
    },
  ): Promise<BankAccount> {
    try {
      const BankAccountData = await this.banking.createDepositAccount({
        ein: requestData.ein,
        entityName: requestData.entityName,
      });

      const account = await this.bankAccountRepository.create(
        new BankAccount({
          ...BankAccountData,
          ...{
            ownerId: userProfile.id,
            tenantId: tenant.id,
          },
        }),
      );

      delete account.providerMeta;
      return account;
    } catch (e) {
      throw new HttpErrors[e?.status ?? 400](
        e?.response?.body ?? `Error Creating Bank Account: ${e}`,
      );
    }
  }

  @authenticate('jwt')
  @operation('post', '/banking/account/{accountId}/ach', {
    operationId: 'createACHTransaction',
    summary: 'Create an ACH Transaction',
    description: 'Create an ACH Transaction with the supplied data',
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '204': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {},
          },
        },
      },
    },
  })
  async createACHTransaction(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(TransactionService) transactionService: TransactionService,
    @param({
      name: 'accountId',
      in: 'path',
      schema: {
        type: 'string',
      },
    })
    bankAccountId: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              accountId: {
                type: 'string',
              },
              subAccountId: {
                type: 'string',
              },
              direction: {
                type: 'string',
                enum: ['CREDIT', 'DEBIT'],
              },
              nameOnAccount: {
                type: 'string',
              },
            },
            dependentSchemas: {
              accountId: {
                required: ['accountId', 'subAccountId', 'direction', 'amount'],
              },
              nameOnAccount: {
                properties: {
                  amount: {
                    type: 'number',
                    minimum: 0.01,
                    multipleOf: 0.01,
                  },
                  routing: {
                    type: 'string',
                  },
                  account: {
                    type: 'string',
                  },
                  typeOfAccount: {
                    type: 'string',
                    enum: ['CHECKING', 'SAVINGS'],
                  },
                },
                required: [
                  'amount',
                  'routing',
                  'account',
                  'typeOfAccount',
                  'direction',
                ],
              },
            },
          },
        },
      },
    })
    requestData:
      | {
          accountId: string;
          subAccountId: string;
          direction: 'CREDIT' | 'DEBIT';
          amount: number;
        }
      | {
          amount: number;
          routing: string;
          account: string;
          typeOfAccount: string;
          nameOnAccount: string;
          direction: 'CREDIT' | 'DEBIT';
        },
  ): Promise<Transaction> {
    let counterparty;
    if ('accountId' in requestData) {
      const account = await this.accountRepository.findById(
        requestData.accountId,
      );
      let plaidAuthObject;
      try {
        plaidAuthObject = await this.plaidService.getAuth(
          account.accessToken,
          requestData.subAccountId,
        );
      } catch (e) {
        throw new HttpErrors.FailedDependency(`Plaid Error: ${e.message}`);
      }
      const accountType =
        plaidAuthObject.accounts[0].subtype === 'checking'
          ? 'CHECKING'
          : 'SAVINGS';
      counterparty = await this.banking.createACHCounterParty({
        nameOnAccount: plaidAuthObject.accounts[0].name,
        accountNumber: plaidAuthObject.numbers.ach[0].account,
        routingNumber: plaidAuthObject.numbers.ach[0].routing,
        accountType: accountType,
      });
    } else if ('routing' in requestData) {
      counterparty = await this.banking.createACHCounterParty({
        nameOnAccount: requestData.nameOnAccount,
        accountNumber: requestData.account,
        routingNumber: requestData.routing,
        accountType: ['CHECKING', 'SAVINGS'][requestData?.typeOfAccount ?? ''],
      });
    } else {
      throw new HttpErrors.BadRequest('Unsupported input');
    }

    return transactionService.createACHTransaction(
      bankAccountId,
      requestData.direction,
      requestData.amount,
      counterparty.id,
    );
  }

  @authenticate('jwt')
  @operation('post', '/banking/account/{accountId}/wire', {
    operationId: 'createWireTransaction',
    summary: 'Create an Outgoing Wire Transaction',
    description: 'Create a Wire Transaction with the supplied data',
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            example: {
              id: '6151b6dbd4ad990013efdf12ed',
              transactionId: 'wire_11gn467cdzdfw1sd',
              bankAccountId: 'qwe123123',
              amount: 100.01,
              counterpartyId: 'cp_q231232',
              createdAt: '2021-03-31T16:04:08Z',
              updatedAt: '2021-03-31T16:04:08Z',
              addedToNetwork: '2021-03-31T16:04:08Z',
              direction: 'CREDIT',
              type: 'WIRE',
              status: 'SENT',
              instructions: 'Payment on Example',
            },
            schema: {
              ...getJsonSchema(Transaction, {
                title: 'Transaction',
                exclude: [
                  'providerMeta',
                  'refundBankAccountTransactionId',
                  'needsApproval',
                  'dealId',
                  'subscriptionId',
                  'description',
                  'closeId',
                  'updateEmail',
                  'tenantId',
                  'entitiesId',
                  'bankAccountTransactionId',
                  'accountID',
                ],
              }),
              description: 'Wire Transaction',
            },
          },
        },
      },
      '412': {
        description: 'Insufficient current balance to initiate the transaction',
      },
    },
  })
  async createWireTransaction(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(TransactionService) transactionService: TransactionService,
    @param({
      name: 'accountId',
      in: 'path',
      schema: {
        type: 'string',
      },
    })
    bankAccountId: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: [
              'direction',
              'nameOnAccount',
              'addressOnAccount',
              'bankAddress',
              'bankName',
              'account',
              'routing',
            ],
            properties: {
              direction: {
                title: 'Direction',
                description: `The direction of the transaction from the perspective 
                of the originating account. For Wire transactions, this can only be \`CREDIT\` `,
                type: 'string',
                enum: ['CREDIT'],
              },
              nameOnAccount: {
                title: 'Name on Account',
                description:
                  "The Account Holder's name on the receiving account",
                type: 'string',
                minLength: 1,
              },
              addressOnAccount: getModelSchemaRef(Address, {
                title: 'Address on Account',
              }),
              bankAddress: getModelSchemaRef(Address, {title: 'Bank Address'}),
              bankName: {
                title: 'Bank Name',
                description: 'The Name of the receiving Bank',
                type: 'string',
                minLength: 3,
              },
              account: {
                title: 'Account Number',
                description: 'The account number of the receiving account',
                type: 'string',
                minLength: 1,
              },
              routing: {
                title: 'Routing',
                description: 'The routing number of the receiving account',
                type: 'string',
                minLength: 1,
              },
              instructions: {
                title: 'Instructions',
                description:
                  'Additional instructions for the Wire. Limited to 140 characters.',
                type: 'string',
                maxLength: 140,
              },
            },
          },
          example: {
            description: 'Wire Transaction Example',
            value: {
              direction: 'CREDIT',
              nameOnAccount: 'John Smith',
              addressOnAccount: {
                address1: '123 Main St',
                address2: '200 S',
                city: 'Salt Lake City',
                region: 'Utah',
                postalCode: '84101',
                country: 'United States of America',
              },
              bankAddress: {
                address1: '123 Main St',
                address2: '200 S',
                city: 'Salt Lake City',
                region: 'Utah',
                postalCode: '84101',
                country: 'United States of America',
              },
              bankName: 'Example Bank',
              account: '10000000',
              routing: '021000021',
              instructions: 'Payment on Example',
            },
          },
        },
      },
    })
    requestData: {
      amount: number;
      routing: string;
      account: string;
      typeOfAccount: string;
      nameOnAccount: string;
      addressOnAccount: Address;
      bankAddress: Address;
      bankName: string;
      direction: 'CREDIT';
      instructions?: string;
    },
  ): Promise<Transaction> {
    const counterparty = await this.banking.createWireCounterParty({
      addressOnAccount: {
        street_1: requestData.addressOnAccount.address1,
        street_2: requestData.addressOnAccount.address2,
        city: requestData.addressOnAccount.city,
        postal_code: requestData.addressOnAccount.postalCode,
        region: requestData.addressOnAccount.region,
        country: requestData.addressOnAccount.country,
      },
      bankAddress: {
        street_1: requestData.bankAddress.address1,
        street_2: requestData.bankAddress.address2,
        city: requestData.bankAddress.city,
        postal_code: requestData.bankAddress.postalCode,
        region: requestData.bankAddress.region,
        country: requestData.bankAddress.country,
      },
      bankName: requestData.bankName,
      nameOnAccount: requestData.nameOnAccount,
      accountNumber: requestData.account,
      routingNumber: requestData.routing,
    });

    try {
      const newTransaction = await transactionService.createWireTransaction(
        bankAccountId,
        requestData.direction,
        requestData.amount,
        counterparty.id,
        requestData.instructions,
      );

      if (newTransaction.needsApproval) {
        await this.systemNotifications.Notify_Transaction_Approval_Needed(
          newTransaction,
        );
      }

      return newTransaction;
    } catch (e) {
      if (e.message === TransactionServiceErrors.INSUFFICIENT_FUNDS) {
        throw new HttpErrors.PreconditionFailed(e);
      }
      throw e;
    }
  }

  @authenticate('jwt')
  @operation('get', '/banking/account/{bankAccountId}/transactions', {
    operationId: 'getBankAccountTransactions',
    summary: 'List Transactions from the Bank Account',
    description: 'Returns the Transaction Listing from the Bank Account',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  amount: {
                    type: 'number',
                    minimum: 0.01,
                    multipleOf: 0.01,
                    examples: [
                      '100.02',
                      '0.05',
                      '19.95',
                      '255.50',
                      '120000.01',
                    ],
                    description:
                      'Transaction amount.  Note that transactions with type `hold` have an amount, but they do not change the balance.',
                  },
                  date: {
                    type: 'string',
                    format: 'date',
                    description: 'ISO 8601 format ("YYYY-MM-DD")',
                    examples: ['2021-02-21', '2020-03-15'],
                  },
                  desc: {
                    type: 'string',
                    description: 'Transaction description.',
                    examples: ['EXT-XFER Sq. Nbr.: 123456'],
                  },
                  book_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the Booking Transfer object that originated this transaction, if any. Otherwise null.',
                    examples: ['book_1k8s7102ehgtbk3'],
                  },
                  type: {
                    type: ['string', 'null'],
                    enum: [
                      'charge',
                      'deposit',
                      'hold',
                      'hold_release',
                      'interest',
                      'reversal',
                      'withdrawal',
                    ],
                    description:
                      'Type of transaction. One of `charge`, `deposit`, `hold`, `hold_release`, `interest`, `payment`, `reversal`, `withdrawal`, or `null`.',
                  },
                  summary: {
                    type: 'string',
                    description: 'Summary description of the transaction.',
                    examples: ['ACME CORP VENDOR PMT'],
                  },
                  balance: {
                    type: 'string',
                    minLength: 1,
                    multipleOf: 0.01,
                    examples: [
                      '100.01',
                      '0.05',
                      '19.95',
                      '255.50',
                      '120000.01',
                    ],
                    description:
                      'Account balance immediately after this transaction. Transactions of type `hold` do not affect the balance.',
                  },
                  id: {
                    type: 'string',
                    description: 'ID for this transaction.',
                    examples: ['ctx_1k8s7102eedgtbk1'],
                  },
                  ach_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the ACH object that originated this transaction, if any. Otherwise null.',
                    examples: ['ach_11gkqac4ddx61n'],
                  },
                  wire_id: {
                    type: ['string', 'null'],
                    description:
                      'The ID of the Wire object that originated this transaction, if any. Otherwise null.',
                    examples: ['wire_11gkqac4ddx76b'],
                  },
                  wire: {
                    type: ['string', 'null'],
                    description:
                      'For wire transactions, the Fedwire description, if any. Otherwise null.',
                    examples: ['ACME CORP VENDOR PMT'],
                  },
                  fingerprint: {
                    type: 'string',
                    description: 'A unique fingerprint for this transaction.',
                    examples: ['2k7vxetham4i1v3ceb5vs6'],
                  },
                },
              },
            },
            examples: {
              'Incoming ACH Transaction': {
                description: 'Example of DEBIT ACH Transaction',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkqangdfdfs',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '5.00',
                    id: 'ctx_1k8s7102egddtgs',
                    ach_id: 'ach_11gkqangdfdfs',
                    fingerprint: '4ef07ba7ce76451240bce807089faf672267bbe5',
                  },
                ],
              },
              'Outgoing ACH Transaction': {
                description: 'Example of CREDIT ACH Transaction',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkqangdfdfs',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '5.00',
                    id: 'ctx_1k8s7102egdgtbk6',
                    ach_id: 'ach_11gkqangdfdfs',
                    fingerprint: '4ef07ba7ce76451240bce807089faf672267beb7',
                  },
                ],
              },
              'Multiple Transaction Types': {
                description: 'Example of Multiple Transaction Types',
                value: [
                  {
                    check_id: null,
                    type_source: null,
                    amount: '113.00',
                    date: '2021-09-17',
                    wire_id: 'wire_11gm9qnadpsdsw',
                    desc: 'WIRE OUT John Doe',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '0.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7502epdp5ws1',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: 'bbd13484e9b77e6e9e96cb571b2bf1b7c76dfdsa2',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '4.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkq9scddwdsdf',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '113.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102endse3s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9scddwdsdf',
                    fingerprint: 'b1efec6fc9d429dbe10a8794e091c47933b0dfsdf',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - Example - 11gkq9g1dwvts',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '117.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102emdfae4s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g1dwvts',
                    fingerprint: 'b696b8dbce64ac98a7d3990779314afac3sd23sa4',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq9g0dd12d23s',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '122.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ekdsds34sd',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g0dd12d23s',
                    fingerprint: 'c7977c438c1c21451f3b664ceaeasdfa3sdf23sd',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq9g0dddws4s',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '127.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ea11ds',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq9g0dddws4s',
                    fingerprint: '0af22c3404976a8d7556b29ebe7f7ab6033ds23s',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq8x83sd229',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '137.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102ehdgtbfs',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x83sd229',
                    fingerprint: '3fc6f1929404f2b2c4bbe04d8900adfdc233aa3se',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkq8x8nsdfy',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '147.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s71dfsdftbft',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x8nsdfy',
                    fingerprint: '3c384ecf210afe3c72d8bd399b12fb213234adc8c1',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-13',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkq8x8ddwddsb',
                    wire: null,
                    book_id: null,
                    type: 'withdrawal',
                    incoming_wire: null,
                    summary: '',
                    balance: '152.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s7102dgdgtbfv',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkq8x8ddwddsb',
                    fingerprint: '4d2453eb1585981ec7141495ac846444adsded102',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '1.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Transfer from 420433323',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '162.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s2eeasd86',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: '90966551c9a79b2d03c87f477aa88434c922232397',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '1.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Transfer from 42049933',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '161.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02eddcvn87',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: '1a4998bb6d94462886f5ec1d2934ac782drd00e',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '100.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'WIRE IN JOHN DOE',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '160.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02ssbssda23eg',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: null,
                    fingerprint: 'e411211d334a01e44fe50b413216099151d91003',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkj0dgdb4dfdw',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '60.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02ebdc7z4r',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj0dgdb4dfdw',
                    fingerprint: '6114a6969f0002424abdbde82c82383a3sdf53ed2b',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '10.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH- 11gkj0dsdfkp9h',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '55.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02a3sdc7z4s',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj0dsdfkp9h',
                    fingerprint: 'b26a8712ae05528a6ad09e5d3454s86f1636c81081',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                  {
                    check_id: null,
                    type_source: null,
                    amount: '5.00',
                    date: '2021-09-09',
                    wire_id: null,
                    desc: 'Glassboard Tech - ACH - 11gkj19mmabbm299',
                    wire: null,
                    book_id: null,
                    type: 'deposit',
                    incoming_wire: null,
                    summary: '',
                    balance: '45.00',
                    billpay_payment_id: null,
                    id: 'ctx_1k8s6x02e9dc7z4t',
                    trace_id: null,
                    extended_timestamp: null,
                    ach_id: 'ach_11gkj19mmabbm299',
                    fingerprint: '5cad515b1fec309e5f86aba36bd389a5445d8d459',
                    check_number: null,
                    related_transfer_ids: [],
                  },
                ],
              },
            },
          },
        },
      },
    },
  })
  async getTransactions(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('bankAccountId') accountId: string,
  ): Promise<TreasuryPrimeAccountTransaction[]> {
    const account = await this.bankAccountRepository.findById(accountId);
    if (!account) {
      throw new HttpErrors.NotFound('bankAccountId is not valid');
    }
    try {
      return await bankingService.getDepositAccountTransactions(account, {
        pageNumber: 0,
        pageSize: 500,
      });
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }
}
