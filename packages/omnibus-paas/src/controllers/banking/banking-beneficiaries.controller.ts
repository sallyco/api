import {authenticate} from '@loopback/authentication';
import {
  tags,
  getModelSchemaRef,
  operation,
  param,
  requestBody,
  HttpErrors,
  OperationVisibility,
  visibility,
} from '@loopback/rest';
import {inject, service} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {BankingBeneficiariesList, BankingBeneficiary} from '../../models';
import {repository} from '@loopback/repository';
import {BankingBeneficiaryRepository} from '../../repositories';
import {Banking, BankingProvider, CounterPartyAccount} from '../../services';

@tags('Banking')
export class BankingBeneficiariesController {
  constructor(
    @repository(BankingBeneficiaryRepository)
    private bankingBeneficiaryRepository: BankingBeneficiaryRepository,
    @service(BankingProvider) private banking: Banking,
  ) {}

  @authenticate('jwt')
  @operation('get', '/banking/beneficiaries', {
    operationId: 'getBankingBeneficiaries',
    summary: 'List Banking Beneficiaries',
    description: 'List Banking Beneficiaries for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiariesList),
          },
        },
      },
    },
  })
  async listBankingBeneficiaries(
    @param({
      name: 'perPage',
      in: 'query',
      schema: {
        type: 'integer',
        minimum: 0,
        maximum: 100,
      },
    })
    perPage = 100,
    @param({
      name: 'page',
      in: 'query',
      schema: {
        type: 'integer',
        minimum: 0,
      },
    })
    page = 0,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<BankingBeneficiariesList> {
    const countAndStatement = {
      and: [
        {
          isDeleted: {
            neq: true,
          },
        },
        ...(tenant.id !== 'master'
          ? [{ownerId: profile.id}, {tenantId: tenant.id}]
          : []),
      ],
    };

    const listFindStatement = {
      where: {
        and: [
          {
            isDeleted: {
              neq: true,
            },
          },
          ...(tenant.id !== 'master'
            ? [{ownerId: profile.id}, {tenantId: tenant.id}]
            : []),
        ],
      },
    };

    const countObj = await this.bankingBeneficiaryRepository.count(
      countAndStatement,
    );

    const data = await this.bankingBeneficiaryRepository.find(
      listFindStatement,
    );

    return new BankingBeneficiariesList({
      totalCount: countObj.count,
      data: data.map(beneficiary => {
        const modifiedResult: Partial<
          BankingBeneficiary & {
            accountNumber: string;
            routingNumber: string;
            type: string;
          }
        > = {...beneficiary};
        delete modifiedResult.providerMeta;

        const providerMetaCounterParty =
          beneficiary.providerMeta!['wire'] ?? beneficiary.providerMeta!['ach'];
        modifiedResult.accountNumber = providerMetaCounterParty.account_number;
        modifiedResult.routingNumber = providerMetaCounterParty.routing_number;
        modifiedResult.type = beneficiary.providerMeta!['wire']
          ? 'wire'
          : 'ach';

        return modifiedResult;
      }),
    });
  }

  @authenticate('jwt')
  @operation('get', '/banking/beneficiaries/{id}', {
    operationId: 'getBankingBeneficiary',
    summary: 'Get Banking Beneficiary by Id',
    description: 'Get Banking Beneficiaries with the provided Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async getBankingBeneficiaryById(
    @param({
      name: 'id',
      in: 'path',
      schema: {
        type: 'string',
      },
    })
    id: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<BankingBeneficiary> {
    const bankingBeneficiary = await this.bankingBeneficiaryRepository.findById(
      id,
    );
    if (tenant.id !== 'master' && bankingBeneficiary?.ownerId !== profile.id) {
      throw new HttpErrors.NotFound();
    }
    return bankingBeneficiary;
  }

  @authenticate('jwt')
  @operation('post', '/banking/beneficiaries', {
    operationId: 'createBankingBeneficiary',
    summary: 'Create a Banking Beneficiary',
    description: 'Create a Banking Beneficiary with the supplied data',
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async createBankingBeneficiary(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: [
              'name',
              'routingNumber',
              'accountNumber',
              'bankName',
              'bankAddress',
              'addressOnAccount',
            ],
            properties: {
              name: {
                type: 'string',
              },
              email: {
                type: 'string',
              },
              phone: {
                type: 'string',
              },
              routingNumber: {
                type: 'string',
              },
              accountNumber: {
                type: 'string',
              },
              bankName: {
                type: 'string',
              },
              bankAddress: {
                type: 'object',
                jsonSchema: {
                  properties: {
                    street_1: {
                      type: 'string',
                    },
                    street_2: {
                      type: 'string',
                    },
                    city: {
                      type: 'string',
                    },
                    postal_code: {
                      type: 'string',
                    },
                    region: {
                      type: 'string',
                      maxLength: 2,
                    },
                    country: {
                      type: 'string',
                      maxLength: 2,
                    },
                  },
                },
              },
              addressOnAccount: {
                type: 'object',
                jsonSchema: {
                  properties: {
                    street_1: {
                      type: 'string',
                    },
                    street_2: {
                      type: 'string',
                    },
                    city: {
                      type: 'string',
                    },
                    postal_code: {
                      type: 'string',
                    },
                    region: {
                      type: 'string',
                      maxLength: 2,
                    },
                    country: {
                      type: 'string',
                      maxLength: 2,
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    requestData: {
      name: string;
      email?: string;
      phone?: string;
      routingNumber: string;
      accountNumber: string;
      bankName: string;
      nameOnAccount: string;
      addressOnAccount: {
        street_1: string;
        street_2: string;
        city: string;
        postal_code: string;
        region: string;
        country: string;
      };
      bankAddress: {
        street_1: string;
        street_2: string;
        city: string;
        postal_code: string;
        region: string;
        country: string;
      };
    },
  ): Promise<BankingBeneficiary> {
    const {name, phone, email, ...counterpartyProps} = requestData;
    const bankingBeneficiary = new BankingBeneficiary({
      name: name,
      phone: phone,
      email: email,
    });
    bankingBeneficiary.ownerId = profile.id;
    bankingBeneficiary.tenantId = tenant.id;
    let counterParty;
    // Create Counterparty from this object
    try {
      counterParty = await this.banking.createWireCounterParty({
        ...counterpartyProps,
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(e.message);
    }
    bankingBeneficiary.serviceObjectId = counterParty.id;
    bankingBeneficiary.providerMeta = {
      ...counterParty,
      typeId: this.banking.typeId,
    };
    return this.bankingBeneficiaryRepository.create(bankingBeneficiary);
  }

  @authenticate('jwt')
  @operation('post', '/banking/ACHBeneficiaries', {
    operationId: 'createACHBankingBeneficiary',
    summary: 'Create a ACH Banking Beneficiary',
    description: 'Create a ACH Banking Beneficiary with the supplied data',
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async createACHBankingBeneficiary(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: [
              'nameOnAccount',
              'accountNumber',
              'routingNumber',
              'accountType',
            ],
            properties: {
              email: {
                type: 'string',
              },
              phone: {
                type: 'string',
              },
              nameOnAccount: {
                type: 'string',
              },
              accountNumber: {
                type: 'string',
              },
              routingNumber: {
                type: 'string',
              },
              accountType: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    requestData: {
      email?: string;
      phone?: string;
      nameOnAccount: string;
      accountNumber: string;
      routingNumber: string;
      accountType: 'CHECKING' | 'SAVINGS';
    },
  ): Promise<BankingBeneficiary> {
    console.log('requestData: ', requestData);
    const {phone, email, ...counterpartyProps} = requestData;
    const bankingBeneficiary = new BankingBeneficiary({
      name: counterpartyProps.nameOnAccount.replace(
        // eslint-disable-next-line no-control-regex
        /[^\x00-\x7F]/g,
        '',
      ),
      phone: phone,
      email: email,
    });
    bankingBeneficiary.ownerId = profile.id;
    bankingBeneficiary.tenantId = tenant.id;
    // Create Counterparty from this object
    let counterParty;
    try {
      counterParty = await this.banking.createACHCounterParty({
        ...counterpartyProps,
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(e.message);
    }
    bankingBeneficiary.serviceObjectId = counterParty.id;
    bankingBeneficiary.providerMeta = {
      ...counterParty,
      typeId: this.banking.typeId,
    };
    return this.bankingBeneficiaryRepository.create(bankingBeneficiary);
  }

  @authenticate('jwt')
  @operation('put', '/banking/beneficiaries/{beneficiaryId}', {
    operationId: 'updateBeneficiaryById',
    summary: 'Update a Beneficiary',
    description: 'Update a Beneficiary with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async updateBeneficiaryById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'beneficiaryId', in: 'path'}) beneficiaryId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: [
              'name',
              'routingNumber',
              'accountNumber',
              'bankName',
              'bankAddress',
              'addressOnAccount',
              'type',
            ],
            properties: {
              id: {
                type: 'string',
              },
              ownerId: {
                type: 'string',
              },
              tenantId: {
                type: 'string',
              },
              serviceObjectId: {
                type: 'string',
              },
              name: {
                type: 'string',
              },
              email: {
                type: 'string',
              },
              phone: {
                type: 'string',
              },
              routingNumber: {
                type: 'string',
              },
              accountNumber: {
                type: 'string',
              },
              bankName: {
                type: 'string',
              },
              bankAddress: {
                type: 'object',
                jsonSchema: {
                  properties: {
                    street_1: {
                      type: 'string',
                    },
                    street_2: {
                      type: 'string',
                    },
                    city: {
                      type: 'string',
                    },
                    postal_code: {
                      type: 'string',
                    },
                    region: {
                      type: 'string',
                      maxLength: 2,
                    },
                    country: {
                      type: 'string',
                      maxLength: 2,
                    },
                  },
                },
              },
              addressOnAccount: {
                type: 'object',
                jsonSchema: {
                  properties: {
                    street_1: {
                      type: 'string',
                    },
                    street_2: {
                      type: 'string',
                    },
                    city: {
                      type: 'string',
                    },
                    postal_code: {
                      type: 'string',
                    },
                    region: {
                      type: 'string',
                      maxLength: 2,
                    },
                    country: {
                      type: 'string',
                      maxLength: 2,
                    },
                  },
                },
              },
              type: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    requestData: {
      id: string;
      ownerId: string;
      tenantId: string;
      serviceObjectId: string;
      name: string;
      email?: string;
      phone?: string;
      routingNumber: string;
      accountNumber: string;
      bankName: string;
      nameOnAccount: string;
      addressOnAccount: {
        street_1: string;
        street_2: string;
        city: string;
        postal_code: string;
        region: string;
        country: string;
      };
      bankAddress: {
        street_1: string;
        street_2: string;
        city: string;
        postal_code: string;
        region: string;
        country: string;
      };
      type: string;
    },
  ): Promise<BankingBeneficiary> {
    const {
      id,
      ownerId,
      tenantId,
      serviceObjectId,
      name,
      phone,
      email,
      ...counterpartyProps
    } = requestData;
    const bankingBeneficiary: Partial<BankingBeneficiary> = {
      id,
      ownerId,
      tenantId,
      serviceObjectId,
      name,
      phone,
      email,
    };

    let counterParty: CounterPartyAccount;
    try {
      counterParty = await this.banking.createWireCounterParty({
        ...counterpartyProps,
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(e.message);
    }

    if (counterParty && Object.keys(counterParty).length > 0) {
      bankingBeneficiary.serviceObjectId = counterParty.id;
      bankingBeneficiary.providerMeta = {
        ...counterParty,
        typeId: this.banking.typeId,
      };
    }

    await this.bankingBeneficiaryRepository.updateById(
      beneficiaryId,
      bankingBeneficiary,
    );
    return this.bankingBeneficiaryRepository.findById(beneficiaryId);
  }

  @authenticate('jwt')
  @operation('put', '/banking/ach-beneficiaries/{beneficiaryId}', {
    operationId: 'updateACHBeneficiaryById',
    summary: 'Update an ACH Beneficiary',
    description: 'Update an ACH Beneficiary with the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async updateACHBeneficiaryById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'beneficiaryId', in: 'path'}) beneficiaryId: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: [
              'nameOnAccount',
              'accountNumber',
              'routingNumber',
              'accountType',
            ],
            properties: {
              email: {
                type: 'string',
              },
              phone: {
                type: 'string',
              },
              nameOnAccount: {
                type: 'string',
              },
              accountNumber: {
                type: 'string',
              },
              routingNumber: {
                type: 'string',
              },
              accountType: {
                type: 'string',
              },
              type: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    requestData: {
      id: string;
      ownerId: string;
      tenantId: string;
      serviceObjectId: string;
      name: string;
      email?: string;
      phone?: string;
      routingNumber: string;
      accountNumber: string;
      bankName: string;
      nameOnAccount: string;
      accountType: 'CHECKING' | 'SAVINGS';
      type: string;
    },
  ): Promise<BankingBeneficiary> {
    const {
      id,
      ownerId,
      tenantId,
      serviceObjectId,
      name,
      phone,
      email,
      ...counterpartyProps
    } = requestData;
    const bankingBeneficiary: Partial<BankingBeneficiary> = {
      id,
      ownerId,
      tenantId,
      serviceObjectId,
      name,
      phone,
      email,
    };

    let counterParty: CounterPartyAccount;
    try {
      counterParty = await this.banking.createACHCounterParty({
        ...counterpartyProps,
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(e.message);
    }

    if (counterParty && Object.keys(counterParty).length > 0) {
      bankingBeneficiary.serviceObjectId = counterParty.id;
      bankingBeneficiary.providerMeta = {
        ...counterParty,
        typeId: this.banking.typeId,
      };
    }

    await this.bankingBeneficiaryRepository.updateById(
      beneficiaryId,
      bankingBeneficiary,
    );
    return this.bankingBeneficiaryRepository.findById(beneficiaryId);
  }

  @authenticate('jwt')
  @operation('delete', '/banking/beneficiaries/{beneficiaryId}', {
    operationId: 'deleteBeneficiary',
    summary: 'Delete a Beneficiary',
    description: 'Delete the specified beneficiary',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async softDeleteById(
    @param({name: 'beneficiaryId', in: 'path'}) beneficiaryId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const beneficiary = await this.bankingBeneficiaryRepository.findById(
      beneficiaryId,
    );

    if (
      beneficiary.isDeleted ||
      (beneficiary.ownerId !== profile.id &&
        beneficiary.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The beneficiary you are trying to delete cannot be found.',
      );
    } else {
      await this.bankingBeneficiaryRepository.deleteById(beneficiaryId);
    }
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/banking/beneficiary-by-serviceid/{serviceObjectId}', {
    operationId: 'getBankingBeneficiaryByServiceObjectId',
    summary: 'Get Banking Beneficiary by ServiceObjectId',
    description: 'Get Banking Beneficiaries with the provided ServiceObjectId',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BankingBeneficiary),
          },
        },
      },
    },
  })
  async getBankingBeneficiaryByServiceObjectId(
    @param({
      name: 'serviceObjectId',
      in: 'path',
      schema: {
        type: 'string',
      },
    })
    serviceObjectId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<BankingBeneficiary | null> {
    const bankingBeneficiary = await this.bankingBeneficiaryRepository.findOne({
      where: {
        serviceObjectId,
      },
    });
    if (tenant.id !== 'master' && bankingBeneficiary?.ownerId !== profile.id) {
      throw new HttpErrors.NotFound();
    }
    return bankingBeneficiary;
  }
}
