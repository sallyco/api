// import debugFactory from 'debug';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {service} from '@loopback/core';
import {get, post} from '@loopback/openapi-v3';
import {repository} from '@loopback/repository';
import {
  Request,
  requestBody,
  RestBindings,
  visibility,
  OperationVisibility,
  HttpErrors,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {Account} from '../../models';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {AccountRepository} from '../../repositories';
import {PlaidService} from '../../services';

// const debug = debugFactory('omnibus-paas:controllers:plaid');

@visibility(OperationVisibility.UNDOCUMENTED)
export class PlaidController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
    @service(PlaidService) private plaidService: PlaidService,
    @inject(MultiTenancyBindings.CURRENT_TENANT) private tenant: Tenant,
    @inject(SecurityBindings.USER) private profile: UserProfile,
    @repository(AccountRepository)
    public accountRepository: AccountRepository,
  ) {}

  @authenticate('jwt')
  @get('/plaid/get-link-token')
  async getLinkToken(): Promise<string> {
    const linkTokenResponse = await this.plaidService.createLinkToken(
      this.tenant,
      this.profile,
    );
    return linkTokenResponse.link_token;
  }

  @authenticate('jwt')
  @post('/plaid/convert-to-account')
  async convertPublicTokenToAccessToken(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              public_token: {type: 'string'},
              accounts: {type: 'array'},
              institution: {type: 'object'},
              link_session_id: {type: 'string'},
            },
          },
        },
      },
    })
    request,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Account | undefined> {
    const publicTokenResponse = await this.plaidService.exchangePublicToken(
      request.public_token,
    );

    for (const subAccount of request.accounts) {
      if (!subAccount) {
        continue;
      }
      const checkIfValidMask = /^\d{4}$/;
      if (checkIfValidMask.test(subAccount.mask)) {
        continue;
      }
      try {
        const plaidAuthObject = await this.plaidService.getAuth(
          publicTokenResponse.access_token,
          subAccount.id,
        );
        if (plaidAuthObject?.numbers?.ach?.length) {
          const accountNumber = plaidAuthObject.numbers.ach[0].account;
          if (accountNumber) {
            subAccount.mask =
              accountNumber.length >= 4
                ? accountNumber.slice(-4)
                : accountNumber;
          }
        }
      } catch (e) {
        throw new HttpErrors.FailedDependency(`Plaid Error: ${e.message}`);
      }
    }

    const account = new Account({
      type: this.plaidService.typeId,
      ownerId: profile.id,
      accessToken: publicTokenResponse.access_token,
      itemId: publicTokenResponse.item_id,
      plaidData: request,
      tenantId: tenant.id,
    });
    const newAccount = await this.accountRepository.save(account);
    return newAccount;
  }

  @authenticate('jwt')
  @post('/plaid/check-if-valid')
  async checkIfVaildPlaidLink(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              accessToken: {type: 'string'},
              subAccountId: {type: 'string'},
            },
          },
        },
      },
    })
    request,
  ): Promise<{isError: boolean; errorCode: string}> {
    try {
      const plaidAuthObject = await this.plaidService.getAuth(
        request.accessToken,
        request.subAccountId,
      );
      if (plaidAuthObject) {
        return {isError: false, errorCode: ''};
      }

      return {isError: true, errorCode: ''};
    } catch (e) {
      return {isError: true, errorCode: e?.error_code ?? ''};
    }
  }

  @authenticate('jwt')
  @post('/plaid/get-update-link-token')
  async getUpdateLinkToken(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              accessToken: {type: 'string'},
            },
          },
        },
      },
    })
    request,
  ): Promise<string> {
    const linkTokenResponse = await this.plaidService.createUpdateLinkToken(
      this.tenant,
      this.profile,
      request.accessToken,
    );
    return linkTokenResponse.link_token;
  }

  @authenticate('jwt')
  @post('/plaid/reset-login')
  async resetLogin(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              accessToken: {type: 'string'},
            },
          },
        },
      },
    })
    request,
  ): Promise<boolean> {
    try {
      const resetResponse = await this.plaidService.resetLogin(
        request.accessToken,
      );
      return resetResponse.reset_login;
    } catch (e) {
      return false;
    }
  }
}
