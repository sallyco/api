import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {tags, param, get, getModelSchemaRef} from '@loopback/rest';
import {Subscription, Profile} from '../models';
import {SubscriptionRepository} from '../repositories';

@tags('Subscriptions')
export class SubscriptionProfileController {
  constructor(
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
  ) {}

  @authenticate('jwt')
  @get('/subscriptions/{id}/profile', {
    operationId: 'getProfil',
    summary: 'Get a Profile for a subscription',
    description: 'Returns a specific profile',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Profile belonging to Subscription',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Profile)},
          },
        },
      },
    },
  })
  async getProfile(
    @param.path.string('id') id: typeof Subscription.prototype.id,
  ): Promise<Profile> {
    return this.subscriptionRepository.profile(id);
  }
}
