/* eslint-disable @typescript-eslint/no-explicit-any */

/* eslint-disable @typescript-eslint/no-unused-vars */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {log, LOG_LEVEL} from '../logger';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import {
  getJsonSchemaRef,
  getModelSchemaRef,
  HttpErrors,
  operation,
  OperationVisibility,
  param,
  requestBody,
  Response,
  RestBindings,
  tags,
  visibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import assert from 'assert';
import debugFactory from 'debug';
import {
  ExternalBankAccount,
  Subscription,
  SubscriptionsList,
  File,
  Profile,
  AchReverseRequest,
} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  ProfileRepository,
  SubscriptionRepository,
  TenantRepository,
  TransactionRepository,
} from '../repositories';
import {
  Banking,
  BankingProvider,
  CeleryClientService,
  DocumentsService,
  FeesService,
  KeycloakService,
  TenantService,
  TransactionService,
} from '../services';
import {
  w9Form,
  w8Form,
  operatingAgreement,
  subscriptionAgreement,
} from '../utils/signingUtils/dictionary-builder.util';
import {PDFDocument, StandardFonts, PageSizes, rgb} from 'pdf-lib';
import {DocumentsGeneratorService} from '../services/documents/documents-generator.service';

import moment from 'moment';

const fs = require('fs');
const path = require('path');
const {v4: uuidv4} = require('uuid');
const got = require('got');

const debug = debugFactory('omnibus-paas:controllers:subscriptions');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by subscriptions
 *
 */
@tags('Subscriptions')
export class SubscriptionsController {
  constructor(
    @repository(SubscriptionRepository)
    public subscriptionRepository: SubscriptionRepository,
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(EntitiesRepository)
    public entityRepository: EntitiesRepository,
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
    @repository(TransactionRepository)
    public transactionRepository: TransactionRepository,
    @repository(TenantRepository)
    protected tenantRepository: TenantRepository,
    @repository(CloseRepository)
    public closeRepository: CloseRepository,
    @service(DocumentsService)
    public documentsService: DocumentsService,
    @service(BankingProvider)
    public bankingService: Banking,
    @service(CeleryClientService)
    public celeryClient: CeleryClientService,
    @service(TenantService)
    public tenantService: TenantService,
    @service(FeesService)
    public feeService: FeesService,
    @service(DocumentsGeneratorService)
    public documentsGeneratorService: DocumentsGeneratorService,
  ) {}

  /**
   * This endpoint lists all the subscriptions
   *

   * @returns A paged array of subscriptions
   * @param perPage
   * @param page
   * @param profile
   */
  @authenticate('jwt')
  @log(LOG_LEVEL.INFO)
  @operation('get', '/subscriptions', {
    operationId: 'listSubscriptions',
    summary: 'List Subscriptions',
    description: 'List the current users subscriptions.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SubscriptionsList),
          },
        },
      },
    },
  })
  async listSubscriptions(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Subscription) filter?: Filter<Subscription>,
  ): Promise<SubscriptionsList> {
    let countAndStatement = {};
    let listFindStatement = {};
    if (tenant.id !== 'master') {
      const builder = new WhereBuilder({isDeleted: false});
      countAndStatement = builder.impose(profile.where ?? {}).build();
      listFindStatement = {
        order: 'createdAt DESC',
        where: builder.impose(profile.where ?? {}).build(),
      };
    } else {
      countAndStatement = {and: [{isDeleted: false}]};
      listFindStatement = {
        where: {and: [{isDeleted: false}]},
        include: [...(filter?.include ?? [])],
        order: 'createdAt DESC',
      };
    }

    const countObj = await this.subscriptionRepository.count(countAndStatement);

    return new SubscriptionsList({
      totalCount: countObj.count,
      data: await this.subscriptionRepository.find(listFindStatement),
    });
  }

  @authenticate('jwt')
  @operation('get', '/subscriptions-by-deal/{dealId}', {
    operationId: 'listSubscriptionsByDeal',
    summary: 'List Subscriptions by a deal Id',
    description: "List user's subscriptions that are part of a deal.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SubscriptionsList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit is not a natural number
          * StartingIndex is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async listSubscriptionsByDeal(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Subscription) filter?: Filter<Subscription>,
  ): Promise<SubscriptionsList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    let countAndStatement = {};
    let listFindStatement = {};

    if (tenant.id !== 'master') {
      countAndStatement = {
        and: [{isDeleted: false}, {dealId: dealId}, {tenantId: tenant.id}],
      };
      listFindStatement = {
        ...filter,
        where: {
          and: [{isDeleted: false}, {dealId: dealId}, {tenantId: tenant.id}],
        },
        include: [...(filter?.include ?? []), 'transactions'],
      };
    } else {
      countAndStatement = {and: [{isDeleted: false}]};
      listFindStatement = {
        where: {and: [{isDeleted: false}, {dealId: dealId}]},
        include: [...(filter?.include ?? []), 'transactions'],
      };
    }

    const countObj = await this.subscriptionRepository.count(countAndStatement);

    return new SubscriptionsList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.subscriptionRepository.find(listFindStatement),
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/subscriptions', {
    operationId: 'createSubscription',
    summary: 'Create a subscription',
    description:
      "Creates a subscription for a specific deal if one doesn't already exist.",
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Subscription),
          },
        },
      },
    },
  })
  async createSubscription(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, {
            title: 'NewSubscription',
            exclude: ['id'],
          }),
        },
      },
    })
    subscription: Subscription,
  ): Promise<Subscription> {
    if (!subscription.ownerId) subscription.ownerId = userProfile.id;
    subscription.tenantId = tenant.id;

    // const exists = await this.subscriptionRepository.findOne({
    //   where: {
    //     and: [
    //       {isDeleted: false},
    //       {ownerId: subscription.ownerId},
    //       {dealId: subscription.dealId},
    //     ],
    //   },
    // });
    //
    // debug('exits', exists);
    //
    // if (
    //   exists &&
    //   exists.status !== 'CANCELLED' &&
    //   exists.status !== 'REFUNDED'
    // ) {
    //   return exists;
    // }

    return this.subscriptionRepository.create(subscription);
  }

  /**
   *
   *

   * @param subscriptionId The id of the subscription to retrieve
   * @param profile
   * @param filter
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/subscriptions/{subscriptionId}', {
    operationId: 'readSubscriptionById',
    summary: 'Get a subscription by Id',
    description: 'Returns a subscription given a specific subscriptionId',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Subscription),
          },
        },
      },
    },
  })
  async readSubscriptionById(
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.filter(Subscription) filter?: Filter<Subscription>,
  ): Promise<Subscription> {
    //if (!subscription.isDeleted && subscription.ownerId === profile.id) {
    return this.subscriptionRepository.findById(subscriptionId, filter);
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The subscription you are trying to find cannot be found.',
    //  );
    //}
  }

  /**
   *
   * @param subscriptionId Get deal informatin in context of this subscription
   * @param profile
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/subscriptions/{subscriptionId}/deal-meta', {
    operationId: 'getSubscriptionMeta',
    summary: 'Get the meta information about a subscription and its deal',
    description: 'Returns a deal info in context of this subscriptionId',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              meta: {
                type: 'object',
                properties: {
                  investorLimitReached: 'boolean',
                  includedInDeal: 'boolean',
                },
              },
            },
          },
        },
      },
    },
  })
  async getSubscriptionDealMeta(
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<{
    meta: {
      investorLimitReached: boolean;
      includedInDeal: boolean;
      maxCommitmentAmount: number;
    };
  }> {
    const activeSubscriptionStatuses: Subscription['status'][] = [
      'COMPLETED',
      'CLOSED',
      'FUNDING SENT',
      'FUNDS_IN_TRANSIT-MANUAL',
      'COMPLETED',
    ];
    const activeSubOrClause = activeSubscriptionStatuses.map(status => {
      return {status: status};
    });

    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );

    // Get the funded subscriptions for this deal
    const queryParams = {
      and: [
        {dealId: subscription.dealId},
        {
          or: activeSubOrClause,
        },
      ],
    };
    const hydratedTenant = await this.tenantService.getCurrentHydratedTenant();
    const dealSubscriptions = await this.subscriptionRepository.find({
      where: queryParams,
    });
    const deal = await this.dealRepository.findById(subscription.dealId);

    const subscriptionCount = dealSubscriptions.length ?? 0;
    const fundedAmounts = dealSubscriptions.reduce((currentSum, sub) => {
      return currentSum + (sub?.amount ?? 0);
    }, 0);

    // Reached
    // config of 0 means no limit
    // assumes precalculation of number of included subscriptions
    let maxInvestors = hydratedTenant?.dealLimits?.perDealMaxInvestorCount ?? 0;

    // This is an override requested by Robb for now, if QP allow 250
    if (deal?.requireQualifiedPurchaser) {
      const QP_OVERRIDE = 250;
      maxInvestors = Math.max(QP_OVERRIDE, maxInvestors);
    }

    const reached = maxInvestors > 0 && subscriptionCount >= maxInvestors;
    const isIncludedInDeal = activeSubscriptionStatuses.includes(
      subscription.status,
    );

    // MaxCommitmentAmount
    // Assumes precalculation of fundedAmounts for deal subscriptions
    // 0 - no limit
    // -1 - means no more allowed, it's full
    const NO_LIMIT = 0;
    const FULL = -1;
    const tenantMaximum =
      hydratedTenant?.dealLimits?.perDealMaxTargetRaiseAmount ?? NO_LIMIT;

    const feeData = await this.feeService.getFeesData(subscription.dealId);
    const dealMax = deal?.targetRaiseAmount + feeData.totalFees;

    // Don't allow oversubscription of a deal
    let maxCommitmentAmount = tenantMaximum;
    // Apply the more restrictive limit
    if (tenantMaximum > NO_LIMIT) {
      maxCommitmentAmount = Math.min(dealMax, tenantMaximum);
    } else if (deal.targetRaiseAmount > 0) {
      maxCommitmentAmount = dealMax;
    }

    if (maxCommitmentAmount > 0) {
      maxCommitmentAmount =
        maxCommitmentAmount - fundedAmounts > 0
          ? maxCommitmentAmount - fundedAmounts
          : FULL;
    }

    return {
      meta: {
        investorLimitReached: reached,
        includedInDeal: isIncludedInDeal,
        maxCommitmentAmount,
      },
    };
  }

  /**
   * @param userProfile
   * @param subscriptionId The id of the subscription to retrieve
   * @param subscription
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/subscriptions/{subscriptionId}', {
    operationId: 'updateSubscriptionById',
    summary: 'Update a subscription',
    description: 'Update a subscription with new information.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Subscription),
          },
        },
      },
      '404': {
        description:
          'Not Found. The subscription you are trying to update cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async updateSubscriptionById(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, {partial: true}),
        },
      },
    })
    subscription: Subscription,
  ): Promise<Subscription> {
    const retrievedSubscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );

    if (
      !retrievedSubscription.isDeleted &&
      (retrievedSubscription.tenantId === tenant.id || tenant.id === 'master')
      //subscription.ownerId === profile.id
    ) {
      subscription.updatedAt = new Date();
      await this.subscriptionRepository.updateById(
        subscriptionId,
        subscription,
      );
      return this.subscriptionRepository.findById(subscriptionId);
    } else {
      throw new HttpErrors.NotFound(
        'The subscription you are trying to update cannot be found.',
      );
    }
  }

  /**
   * @param userProfile
   * @param tenant
   * @param subscriptionId The id of the subscription to update
   * @param subscription
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('patch', '/subscriptions/{subscriptionId}', {
    operationId: 'patchSubscriptionById',
    summary: 'Partially Update a subscription',
    description: 'Update a subscription with new information.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Subscription),
          },
        },
      },
      '404': {
        description:
          'Not Found. The subscription you are trying to update cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async patchSubscriptionById(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, {partial: true}),
        },
      },
    })
    subscription: Partial<Subscription>,
  ): Promise<Subscription> {
    const retrievedSubscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );

    if (
      !retrievedSubscription.isDeleted &&
      (retrievedSubscription.tenantId === tenant.id || tenant.id === 'master')
    ) {
      subscription.updatedAt = new Date();
      await this.subscriptionRepository.updateById(subscriptionId, {
        ...retrievedSubscription,
        ...subscription,
      });
      return this.subscriptionRepository.findById(subscriptionId);
    } else {
      throw new HttpErrors.NotFound(
        'The subscription you are trying to update cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param subscriptionId The id of the subscription to retrieve
   * @param profile
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/subscriptions/{subscriptionId}', {
    operationId: 'deleteSubscriptionById',
    summary: 'Delete a subscription',
    description: 'Delete a subscription using a subscriptionId.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async deleteSubscriptionById(
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );

    if (
      subscription.isDeleted ||
      (subscription.ownerId !== profile.id &&
        subscription.tenantId !== tenant.id &&
        tenant.id !== 'master')
    ) {
      throw new HttpErrors.NotFound(
        'The subscription you are trying to delete cannot be found.',
      );
    } else {
      await this.subscriptionRepository.updateById(subscriptionId, {
        deletedAt: new Date(),
        isDeleted: true,
      });
    }
  }

  @authenticate('jwt')
  @operation(
    'post',
    '/subscriptions/{subscriptionId}/generate-signing-document/{documentType}',
    {
      operationId: 'populateTemplate',
      summary: 'Generate a signing document',
      description:
        'Generate a signing document for a specific subscription, of the type you provide.',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '200': {
          description: 'Expected response to a valid request',
          content: {
            'text/plain': {
              schema: getModelSchemaRef(File, {partial: true}),
            },
          },
        },
      },
    },
  )
  async populateTemplate(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('subscriptionId') subscriptionId: string,
    @param.path.string('documentType') documentType: string,
    @requestBody() payloadData: any,
  ): Promise<File> {
    return this.celeryClient.generateSigningDocument(
      documentType,
      subscriptionId,
      payloadData,
      profile,
      tenant,
    );
  }

  @authenticate('jwt')
  @operation(
    'get',
    '/subscriptions/{subscriptionId}/signing-documents/{documentType}',
    {
      operationId: 'getSigningDocument',
      summary: 'Get a signing document',
      description:
        'Get a signing document for a specific subscription, of the type you provide.',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '200': {
          description: 'Expected response to a valid request',
          content: {
            'text/plain': {
              schema: getModelSchemaRef(File, {partial: true}),
            },
          },
        },
        '404': {
          description: 'Not Found. DocumentType not supported',
        },
        '424': {
          description: `Failed Dependency

            * Subscription is not connected to a profile
            * Subscription is not connected to a deal
          `,
        },
        default: {
          $ref: '#/components/responses/UnexpectedError',
        },
      },
    },
  )
  async getPopulateTemplate(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('subscriptionId') subscriptionId: string,
    @param.path.string('documentType') documentType: string,
  ): Promise<File> {
    if (!['oa', 'sub', 'sub-s', 'w8ben', 'w9'].includes(documentType)) {
      throw new HttpErrors.NotFound('documentType not supported');
    }

    const sub = await this.subscriptionRepository.findById(subscriptionId);
    const profile = await this.subscriptionRepository.profile(sub.id);
    if (!profile) {
      throw new HttpErrors.FailedDependency(
        'Subscription is not connected to a profile',
      );
    }
    const deal = await this.subscriptionRepository.deal(sub.id);
    if (!deal) {
      throw new HttpErrors.FailedDependency(
        'Subscription is not connected to a deal',
      );
    }
    const entity = await this.dealRepository.entity(deal.id);

    let payloadData: object;
    switch (documentType) {
      case 'w9':
        payloadData = w9Form(profile);
        break;
      case 'w8ben':
        payloadData = w8Form(profile);
        break;
      case 'oa':
        payloadData = operatingAgreement(sub, profile, entity);
        break;
      case 'sub':
        payloadData = subscriptionAgreement(sub, profile, entity, deal, true);
        break;
      default:
        throw new HttpErrors.NotFound('documentType not supported');
    }
    if (sub.signature) {
      payloadData = {...payloadData, signature: sub.signature};
    }

    return this.celeryClient.generateSigningDocument(
      documentType,
      subscriptionId,
      payloadData,
      userProfile,
      tenant,
    );
  }

  @authenticate('jwt')
  @operation(
    'post',
    '/subscriptions/{subscriptionId}/signing-documents/{documentType}/sign',
    {
      operationId: 'signSigningDocument',
      summary: 'Apply signature to a signing document',
      description:
        'Apply signature a signing document for a specific subscription, of the type you provide.',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '200': {
          description: 'Expected response to a valid request',
          content: {
            'text/plain': {
              schema: getModelSchemaRef(File, {partial: true}),
            },
          },
        },
        '404': {
          description: 'Not Found. DocumentType not supported',
        },
        '424': {
          description: `Failed Dependency.

            * No ProfileId present for the current signer on this subscription
            * ProfileId is not valid
            * Subscription is not connected to a deal`,
        },
        default: {
          $ref: '#/components/responses/UnexpectedError',
        },
      },
    },
  )
  async signPopulateTemplate(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('subscriptionId') subscriptionId: string,
    @param.path.string('documentType') documentType: string,
    @service(KeycloakService) keycloak: KeycloakService,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              signature: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    payload: {
      signature: string;
    },
  ): Promise<File> {
    if (!['oa', 'sub', 'sub-s', 'w8ben', 'w9'].includes(documentType)) {
      throw new HttpErrors.NotFound('documentType not supported');
    }
    const user = await (
      await keycloak.getClient()
    ).users.findOne({
      id: userProfile.id,
      realm: tenant.id,
    });

    const sub = await this.subscriptionRepository.findById(subscriptionId);
    const profileId = sub.profileId;
    if (sub?.signers && sub.signers.length > 0) {
      const signerIndex = sub.signers.findIndex(
        signer => signer?.email === user.email,
      );

      if (!sub.signers[signerIndex]?.profileId) {
        throw new HttpErrors.FailedDependency(
          'No ProfileId present for the current signer on this subscription',
        );
      }
      const subProfileId = sub.signers[signerIndex].profileId!;
      const profileData = await this.profileRepository.findById(subProfileId);

      sub.signers[signerIndex] = {
        ...sub.signers[signerIndex],
        dateSigned: new Date(),
        signature: payload.signature,
        profileId: profileId,
        profileData: profileData,
      };
    }
    if (sub?.ownerId === userProfile.id) {
      sub.signature = payload.signature;
    }

    const profile = await this.profileRepository.findById(profileId);
    if (!profile) {
      throw new HttpErrors.FailedDependency('ProfileId is not valid');
    }

    const deal = await this.subscriptionRepository.deal(sub.id);
    if (!deal) {
      throw new HttpErrors.FailedDependency(
        'Subscription is not connected to a deal',
      );
    }
    const entity = await this.dealRepository.entity(deal.id);

    let payloadData: object;
    switch (documentType) {
      case 'w9':
        payloadData = w9Form(profile, payload.signature);
        break;
      case 'w8ben':
        payloadData = w8Form(profile, payload.signature);
        break;
      case 'oa':
        payloadData = operatingAgreement(
          sub,
          profile,
          entity,
          payload.signature,
        );
        break;
      case 'sub':
        payloadData = subscriptionAgreement(
          sub,
          profile,
          entity,
          deal,
          true,
          false,
          payload.signature,
        );
        break;
      default:
        throw new HttpErrors.NotFound('documentType not supported');
    }

    await this.subscriptionRepository.save(sub);

    return this.celeryClient.generateSigningDocument(
      documentType,
      subscriptionId,
      payloadData,
      userProfile,
      tenant,
    );
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/subscriptions/search', {
    operationId: 'search',
    summary: 'Subscription search',
    description:
      'Search for a list of subscriptions with a search object of parameters that you provide.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SubscriptionsList),
          },
        },
      },
    },
  })
  async search(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              search: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
              sort: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    direction: {
                      type: 'string',
                      enum: ['ASCENDING', 'DESCENDING'],
                    },
                  },
                },
              },
              dealSearch: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    value: {
                      type: 'string',
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    searchObj: object,
  ): Promise<SubscriptionsList> {
    const searchParamsList = [] as any;
    const sortParamList = [] as any;
    if (searchObj['search'] !== undefined && searchObj['search'].length > 0) {
      for (let index = 0; index < searchObj['search'].length; index++) {
        const queryParam = {};
        if (searchObj['search'][index].field === 'profileId') {
          queryParam[searchObj['search'][index].field] = {
            eq: searchObj['search'][index].value,
          };
        } else {
          const pattern = new RegExp(
            '.*' + searchObj['search'][index].value + '.*',
            'i',
          );
          queryParam[searchObj['search'][index].field] = {like: pattern};
        }
        searchParamsList.push(queryParam);
      }
    }
    if (searchObj['sort'] !== undefined && searchObj['sort'].length > 0) {
      for (let index = 0; index < searchObj['sort'].length; index++) {
        let direction = 'ASC';
        if (searchObj['sort'][index].direction === 'DESCENDING')
          direction = 'DESC';
        const queryParam = searchObj['sort'][index].field + ' ' + direction;
        sortParamList.push(queryParam);
      }
    }

    searchParamsList.push(
      {isDeleted: false},
      {ownerId: profile.id},
      {tenantId: tenant.id},
    );
    const count = await this.subscriptionRepository.count({
      and: [{isDeleted: false}, {ownerId: profile.id}, {tenantId: tenant.id}],
    });

    return new SubscriptionsList({
      totalCount: count.count,
      data: await this.subscriptionRepository
        .find({
          include: [
            {
              relation: 'deal',
              scope: {
                fields: ['id', 'name'],
              },
            },
          ],
          where: {and: searchParamsList},
          order: sortParamList.length > 0 ? sortParamList : 'createdAt DESC',
        })
        .then(data => {
          if (
            searchObj['dealSearch'] !== undefined &&
            searchObj['dealSearch'].length > 0
          ) {
            const predicates: any = {};
            for (const dealSearchObj of searchObj['dealSearch']) {
              const field = dealSearchObj.field;
              const value = dealSearchObj.value;
              predicates[field] = sub =>
                // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
                sub['deal'][field] &&
                sub['deal'][field].toLowerCase().includes(value);
            }
            return data.filter(sub => {
              return Object.values(predicates).some((predicate: any) => {
                if (predicate(sub)) {
                  return true;
                }
                return false;
              });
            });
          }
          return data;
        }),
    });
  }

  @authenticate('jwt')
  @operation('post', '/subscriptions/{subscriptionId}/initTransfer', {
    operationId: 'createAchTransfer',
    summary: 'Create an ACH transfer',
    description: 'This places the transfer into the queue for processing.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'boolean (true/false)',
      },
      '400': {
        description: 'Scheduled Transaction date cannot be in the past',
      },
      '424': {
        description: 'The subscription or entity is not ready for the transfer',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async initTransfer(
    @param.path.string('subscriptionId') subscriptionId: string,
    @service(TransactionService) transactionService: TransactionService,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              scheduledForDate: {
                type: 'string',
                format: 'date',
              },
            },
          },
        },
      },
    })
    parameters?: {
      scheduledForDate?: string;
    },
  ): Promise<void | boolean> {
    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );
    if (
      parameters?.scheduledForDate &&
      moment(parameters.scheduledForDate).isBefore(moment())
    ) {
      throw new HttpErrors.BadRequest(
        'Scheduled Transaction date cannot be in the past',
      );
    }
    if (!subscription.bankAccount?.counterpartyId) {
      throw new HttpErrors.FailedDependency(
        'Subscription must have a valid bank account attached',
      );
    }
    if (
      !['COMMITTED', 'COMPLETED', 'FUNDS_IN_TRANSIT-MANUAL'].includes(
        subscription.status ?? '',
      )
    ) {
      throw new HttpErrors.FailedDependency(
        'Subscription must be in one of the following statuses before initiating transaction. (COMMITTED, COMPLETED, FUNDS IN TRANSIT - MANUAL, NEEDS FUNDING)',
      );
    }
    if (!subscription.amount) {
      throw new HttpErrors.FailedDependency('Subscription amount must be set');
    }

    subscription.status = 'FUNDING SENT';
    await this.subscriptionRepository.update(subscription);
    // Calculate amount from previous transactions
    const transactions = await this.transactionRepository.find(
      new WhereBuilder({
        subscriptionId: subscriptionId,
        direction: 'DEBIT',
      }),
    );
    let amount = subscription.amount;
    for (const transaction of transactions) {
      amount = amount - transaction.amount;
    }
    // ---
    await transactionService.createSubscriptionTransaction(
      subscriptionId,
      'DEBIT',
      amount,
      parameters?.scheduledForDate
        ? moment(parameters.scheduledForDate).toDate()
        : undefined,
    );
    return true;
  }

  @authenticate('jwt')
  @operation('post', '/subscriptions/{subscriptionId}/reverseTransfer', {
    operationId: 'reverseTransfer',
    summary: 'Create a Reverse ACH Transfer',
    description: `This will add a reverse transfer to the queue for processing, if conditions are met.

The following conditions are required:

* The Subscription must be in a funded state, either from an on or off-platform transaction
* The Entity of the Deal for the Subscription must have an active bank account, with the funds available for the refund
* The Subscription must not have been refunded before. Only One (1) refund is allowed per Subscription
* All On-Platform Transactions for the Subscription must be Settled (In a 'SENT' state)

The following properties must be supplied if the Subscription was funded with an off-platform method, or with multiple incoming ACH transactions:

* \`routing\`
* \`account\`
* \`typeOfAccount\`
* \`nameOnAccount\`

The \`ownerId\` of the Deal or Subscription must match the userId of the current API user

If \`amount\` is omitted, the Subscription will refund for the value of \`Subscription.amount\`
`,
    security: [
      {
        oAuth2: ['clientCredentials', 'authorizationCode'],
      },
    ],
    responses: {
      '200': {
        description: 'boolean (true/false)',
      },
      '403': {
        description:
          'Forbidden. Current user is not authorized to refund the subscription',
      },
      '409': {
        description: 'Conflict. Refund already initiated for this subscription',
      },
      '422': {
        description: 'Unprocessable Entity',
      },
      '424': {
        description:
          "Failed Dependency. Either the Subscription wasn't in a refundable state, or ACH credentials were not provided",
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async reverseTransfer(
    @param.path.string('subscriptionId') subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(TransactionService) transactionService: TransactionService,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: getModelSchemaRef(AchReverseRequest),
        },
        examples: {
          'Subscription With An Off-Platform ACH Transaction': {
            description: 'Example',
            value: {
              amount: 100.01,
              routing: '000000',
              account: '1111111111',
              nameOnAccount: 'John Doe',
              typeOfAccount: 'CHECKING',
            },
          },
        },
      },
    })
    request?: AchReverseRequest,
  ): Promise<void | boolean> {
    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );
    const deal = await this.dealRepository.findById(subscription.dealId);
    if (
      tenant.id !== 'master' &&
      profile.id !== subscription.ownerId &&
      profile.id !== deal.ownerId
    ) {
      throw new HttpErrors.Forbidden();
    }

    if (
      subscription.reverseTransactionId ||
      subscription.status === 'REFUNDED'
    ) {
      throw new HttpErrors.Conflict(
        'Refund already initiated for this subscription',
      );
    }

    if (subscription?.transactionIds) {
      if (!subscription.bankAccount?.counterpartyId) {
        throw new HttpErrors.FailedDependency(
          'Subscription must have a valid bank account attached',
        );
      }
      for (const transactionId in subscription.transactionIds) {
        const transaction = await this.transactionRepository.findById(
          subscription.transactionIds[transactionId],
        );
        if (transaction.status !== 'SENT') {
          throw new HttpErrors.FailedDependency(
            `Subscription is not ready to be refunded, transaction(s) are not settled into account. Transaction id ${transaction.id} status is ${transaction.status}`,
          );
        }
      }
    }

    if (
      !subscription?.transactionIds &&
      (!request?.account ||
        !request?.routing ||
        !request?.typeOfAccount ||
        !request?.nameOnAccount)
    ) {
      throw new HttpErrors.FailedDependency(
        'Off-Platform Funded Subscription Requires ACH Details to create Refund',
      );
    } else if (!subscription?.transactionIds) {
      // Create a counterparty ID for this refund
      try {
        const counterparty = await this.bankingService.createACHCounterParty({
          nameOnAccount: request?.nameOnAccount ?? '',
          routingNumber: request?.routing ?? '',
          accountNumber: request?.account ?? '',
          accountType: ['CHECKING', 'SAVINGS'][request?.typeOfAccount ?? ''],
        });
        subscription.bankAccount = new ExternalBankAccount({
          ...subscription.bankAccount,
          ...{
            counterpartyId: counterparty.id,
          },
        });
        await this.subscriptionRepository.save(subscription);
      } catch (e) {
        throw new HttpErrors.UnprocessableEntity(e.message);
      }
    }
    assert(subscription.amount);

    let formattedAmount = 0;
    if (request?.amount) {
      formattedAmount = request.amount;
    } else {
      formattedAmount = subscription.amount;
    }

    await transactionService.createReverseSubscriptionTransaction(
      subscriptionId,
      formattedAmount,
    );
    return true;
  }

  @authenticate('jwt')
  @operation('post', '/subscriptions/{subscriptionId}/cancel', {
    operationId: 'cancelSubscription',
    summary: 'Cancel a subscription',
    description: 'Cancel a subscription by a specific id.',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      '424': {
        description: 'Failed Dependency',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async cancelSubscription(
    @param.path.string('subscriptionId') subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const subscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );
    const deal = await this.dealRepository.findById(subscription.dealId);
    if (
      tenant.id !== 'master' &&
      subscription.ownerId !== profile.id &&
      deal.ownerId !== profile.id
    ) {
      throw new HttpErrors.Forbidden();
    }

    const subscriptionTransactionsCount =
      await this.transactionRepository.count({
        subscriptionId: subscriptionId,
      });

    // Requirements: FULLY DELETE a sub if no actions have taken place
    if (
      subscriptionTransactionsCount.count === 0 &&
      subscription.isDocsSigned === false
    ) {
      await this.subscriptionRepository.deleteById(subscriptionId);
      return;
    }

    if (
      !['INVITED', 'PENDING', 'COMMITTED'].includes(subscription.status ?? '')
    ) {
      throw new HttpErrors.FailedDependency(
        'Subscription cannot be cancelled without refund',
      );
    }
    subscription.status = 'CANCELLED';
    await this.subscriptionRepository.save(subscription);
  }

  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @operation(
    'get',
    '/subscriptions/{subscriptionId}/capital-account-statement/pdf',
    {
      operationId: 'getCapStatementPdf',
      summary: 'Generate a capital account statement for a subscription',
      description:
        'Statements in PDF format are created at request time, i.e. they are not generated during some previous event and stored.  The function generates a PDF for a capital account statement for the specified subscription id.  It can be created anytime after the subscription has been created. If the subscription is not part of a close, the values on this statement are estimates.',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '200': {
          description:
            'No return value. If success, the response status will be 200 and response contains buffer(bytes) of the PDF.',
        },
      },
    },
  )
  async getCapStatementPdf(
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenat: Tenant,
    @inject(RestBindings.Http.RESPONSE) res: Response,
  ) {
    const subscription: any = await this.subscriptionRepository.findById(
      subscriptionId,
      {
        include: [
          {relation: 'profile'},
          {
            relation: 'deal',
            scope: {
              include: ['entity'],
            },
          },
        ],
      },
    );
    const tenant: any = await this.tenantRepository.findById(
      subscription.tenantId,
      {
        include: [{relation: 'manager'}],
      },
    );
    const statementPdf = await this.documentsGeneratorService.buildCapStatement(
      {subscription, tenant},
    );

    const pdfBytes = await statementPdf.save();
    res.attachment(`${subscription.name}.pdf`);
    res.contentType('application/pdf');
    res.send(Buffer.from(pdfBytes));
  }

  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation(
    'get',
    '/subscriptions/{subscriptionId}/distribution-statement/pdf',
    {
      operationId: 'getCapStatementPdf',
      summary: 'Get a Distribution Statement Pdf',
      description: 'Get Distribution Statement Pdf',
      security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
      responses: {
        '200': {
          description: 'Expected response to a valid request',
        },
      },
    },
  )
  async getDistroStatementPdf(
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenat: Tenant,
    @inject(RestBindings.Http.RESPONSE) res: Response,
  ) {
    const subscription: any = await this.subscriptionRepository.findById(
      subscriptionId,
      {
        include: [
          {relation: 'profile'},
          {
            relation: 'deal',
            scope: {
              include: ['entity'],
            },
          },
        ],
      },
    );
    if (!subscription.ownershipPercentageAtDistribution) {
      throw new HttpErrors.FailedDependency(
        'Subscription has not been distributed',
      );
    }
    const tenant: any = await this.tenantRepository.findById(
      subscription.tenantId,
      {
        include: [{relation: 'manager'}],
      },
    );
    const statementPdf =
      await this.documentsGeneratorService.buildDistroStatement({
        subscription,
        tenant,
      });

    const pdfBytes = await statementPdf.save();
    res.attachment(`${subscription.name}.pdf`);
    res.contentType('application/pdf');
    res.send(Buffer.from(pdfBytes));
  }

  /**
   *
   *
   * @param ProfileId The profileId of the subscription to retrieve
   * @param profile
   * @param filter
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/subscriptions/{profileId}/subscriptions-meta', {
    operationId: 'readSubscriptionByProfileId',
    summary: 'Get a subscription by profileId',
    description: 'Returns a subscription given a specific profileId',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SubscriptionsList),
          },
        },
      },
    },
  })
  async readSubscriptionByProfileId(
    @param({name: 'profileId', in: 'path'}) profileId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.filter(Subscription) filter?: Filter<Subscription>,
  ): Promise<SubscriptionsList> {
    const listFindStatement = {
      where: {
        and: [{isDeleted: false}, {profileId: profileId}],
      },
    };
    const countObj = await this.subscriptionRepository.count({
      isDeleted: false,
      profileId: profileId,
    });

    return new SubscriptionsList({
      totalCount: countObj.count,
      perPage: 100,
      page: 1,
      data: await this.subscriptionRepository.find(listFindStatement),
    });
  }

  /**
   * @param userProfile
   * @param tenant
   * @param subscriptionId The id of the subscription to retrieve
   * @param keycloak
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('post', '/subscriptions/{subscriptionId}/update-signers', {
    operationId: 'updateSubscriptionSignersBySubscriptionId',
    summary: 'Update signers for a subscription',
    description: 'Update signers for a subscription',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description:
          'No return value. If success, the response status will be 200',
      },
      '404': {
        description:
          'Not Found. The subscription you are trying to update cannot be found.',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async updateSubscriptionSignersBySubscriptionId(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @service(KeycloakService) keycloak: KeycloakService,
  ): Promise<void> {
    const retrievedSubscription = await this.subscriptionRepository.findById(
      subscriptionId,
    );

    if (
      !retrievedSubscription.isDeleted &&
      (retrievedSubscription.tenantId === tenant.id || tenant.id === 'master')
      //subscription.ownerId === profile.id
    ) {
      const signers = retrievedSubscription.signers;
      if (signers) {
        const kcAdminClient = await keycloak.getClient();
        for (const signer of signers) {
          const user = await kcAdminClient.users.find({
            realm: tenant.id,
            username: signer.email,
          });
          if (user?.[0]) {
            const clients = await kcAdminClient.clients.find({
              realm: tenant.id,
            });
            const accountClient = clients.find(
              element => element.clientId === 'account',
            );
            const accountClientId = accountClient!.id!;
            const availableRoles =
              await kcAdminClient.users.listAvailableClientRoleMappings({
                realm: tenant.id,
                id: user[0].id!,
                clientUniqueId: accountClientId,
              });
            const signerRole = availableRoles.find(
              element => element.name === 'signer',
            );
            if (signerRole?.id) {
              const roleId = signerRole.id;
              // const existingRoles = await kcAdminClient.users.listClientRoleMappings({
              //   id: user[0].id!,
              //   realm: tenant.id,
              //   clientUniqueId: accountClientId
              // })
              await kcAdminClient.users.addClientRoleMappings({
                id: user[0].id!,
                realm: tenant.id,
                clientUniqueId: accountClientId,
                roles: [
                  {
                    id: roleId,
                    name: signerRole.name!,
                  },
                ],
              });
            }
          }
        }
      } else {
        throw new HttpErrors.NotFound(
          'The subscription you are trying to update cannot be found.',
        );
      }
    }
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('put', '/subscriptions/{subscriptionId}/reset')
  async resetSubscriptionById(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param({name: 'subscriptionId', in: 'path'}) subscriptionId: string,
    @service(KeycloakService) keycloak: KeycloakService,
  ): Promise<Subscription> {
    if (process.env.ENVIRONMENT === 'production') {
      throw new HttpErrors.NotFound();
    }
    const sub = await this.subscriptionRepository.findById(subscriptionId);

    await this.subscriptionRepository.replaceById(subscriptionId, {
      ...{
        status: 'INVITED',
        isDocsSigned: false,
        isAmountMatched: true,
        isKycAmlPassed: false,
        updatedAt: new Date(),
        isDeleted: false,
      },
      tenantId: sub.tenantId,
      dealId: sub.dealId,
      name: sub.name,
      email: sub.email,
      createdAt: sub.createdAt,
      acquisitionMethod: sub.acquisitionMethod,
      ownerId: sub.ownerId,
    });
    return this.subscriptionRepository.findById(subscriptionId);
  }
}
