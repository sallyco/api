import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Accreditation, Profile} from '../../models';
import {AccreditationRepository} from '../../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class AccreditationProfileController {
  constructor(
    @repository(AccreditationRepository)
    public accreditationRepository: AccreditationRepository,
  ) {}

  @get('/accreditations/{id}/profile', {
    responses: {
      '200': {
        description: 'Profile belonging to Accreditation',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Profile)},
          },
        },
      },
    },
  })
  async getProfile(
    @param.path.string('id') id: typeof Accreditation.prototype.id,
  ): Promise<Profile> {
    return this.accreditationRepository.profileModel(id);
  }
}
