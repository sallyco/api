import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  tags,
} from '@loopback/rest';
import {Accreditation} from '../../models';
import {AccreditationRepository} from '../../repositories';
import {inject, service} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import {authenticate} from '@loopback/authentication';
import {AccreditationProvider, AccreditationService} from '../../services';
import {getJsonSchema} from '@loopback/repository-json-schema';

@authenticate('jwt')
@tags('Accreditation')
export class AccreditationController {
  constructor(
    @repository(AccreditationRepository)
    public accreditationRepository: AccreditationRepository,
  ) {}

  @authenticate('jwt')
  @post('/accreditations')
  @response(200, {
    description: 'Accreditation model instance',
    content: {'application/json': {schema: getModelSchemaRef(Accreditation)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {
            title: 'NewAccreditation',
            exclude: ['id', 'ownerId', 'tenantId'],
          }),
        },
      },
    })
    accreditation: Omit<Accreditation, 'id'>,
    @inject(SecurityBindings.USER) user: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(AccreditationProvider) accreditationService: AccreditationService,
  ): Promise<Accreditation> {
    accreditation.ownerId = user.id;
    accreditation.tenantId = tenant.id;
    const accreditationObject = await this.accreditationRepository.create(
      accreditation,
    );
    const updatedAccreditation =
      await accreditationService.getAccreditationUpdate(accreditationObject);
    await this.accreditationRepository.update(updatedAccreditation);
    return updatedAccreditation;
  }

  @authenticate('jwt')
  @get('/accreditations/count')
  @response(200, {
    description: 'Accreditation model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Accreditation) where?: Where<Accreditation>,
  ): Promise<Count> {
    return this.accreditationRepository.count(where);
  }

  @authenticate('jwt')
  @get('/accreditations')
  @response(200, {
    description: 'Array of Accreditation model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Accreditation, {exclude: ['providerMeta']}),
        },
      },
    },
  })
  async find(
    @param.filter(Accreditation) filter?: Filter<Accreditation>,
  ): Promise<Accreditation[]> {
    return this.accreditationRepository.find(filter);
  }

  @authenticate('jwt')
  @patch('/accreditations')
  @response(200, {
    description: 'Accreditation PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {partial: true}),
        },
      },
    })
    accreditation: Accreditation,
    @param.where(Accreditation) where?: Where<Accreditation>,
  ): Promise<Count> {
    return this.accreditationRepository.updateAll(accreditation, where);
  }

  @authenticate('jwt')
  @get('/accreditations/{id}')
  @response(200, {
    description: 'Accreditation model instance',
    content: {
      'application/json': {
        schema: getJsonSchema(Accreditation, {
          exclude: ['providerMeta'],
          title: 'Accreditation',
        }),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Accreditation, {exclude: 'where'})
    filter?: FilterExcludingWhere<Accreditation>,
  ): Promise<Accreditation> {
    return this.accreditationRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @patch('/accreditations/{id}')
  @response(204, {
    description: 'Accreditation PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {partial: true}),
        },
      },
    })
    accreditation: Accreditation,
  ): Promise<void> {
    await this.accreditationRepository.updateById(id, accreditation);
  }

  @authenticate('jwt')
  @put('/accreditations/{id}')
  @response(204, {
    description: 'Accreditation PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {
            title: 'ReplaceAccreditation',
            exclude: ['ownerId', 'tenantId'],
          }),
        },
      },
    })
    accreditation: Accreditation,
    @inject(SecurityBindings.USER) user: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(AccreditationProvider) accreditationService: AccreditationService,
  ): Promise<void> {
    accreditation.ownerId = user.id;
    accreditation.tenantId = tenant.id;
    await this.accreditationRepository.replaceById(id, accreditation);
    const accreditationObject = await this.accreditationRepository.findById(id);
    const updatedAccreditation =
      await accreditationService.getAccreditationUpdate(accreditationObject);
    await this.accreditationRepository.update(updatedAccreditation);
  }

  @authenticate('jwt')
  @del('/accreditations/{id}')
  @response(204, {
    description: 'Accreditation DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.accreditationRepository.deleteById(id);
  }
}
