import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  put,
  post,
  requestBody,
  OperationVisibility,
  visibility,
} from '@loopback/rest';
import {Tenant, WhiteLabel} from '../models';
import {TenantRepository, WhiteLabelRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class TenantWhiteLabelController {
  constructor(
    @repository(TenantRepository) protected tenantRepository: TenantRepository,
    @repository(WhiteLabelRepository)
    protected whiteLabelRepository: WhiteLabelRepository,
  ) {}

  @get('/tenants/{id}/white-label', {
    responses: {
      '200': {
        description: 'Tenant has one WhiteLabel',
        content: {
          'application/json': {
            schema: getModelSchemaRef(WhiteLabel),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<WhiteLabel>,
  ): Promise<WhiteLabel> {
    return this.tenantRepository.whiteLabel(id).get(filter);
  }

  @post('/tenants/{id}/white-label', {
    responses: {
      '200': {
        description: 'Tenant model instance',
        content: {'application/json': {schema: getModelSchemaRef(WhiteLabel)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Tenant.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WhiteLabel, {
            title: 'NewWhiteLabelInTenant',
            exclude: ['id'],
            optional: ['tenantId'],
          }),
        },
      },
    })
    whiteLabel: Omit<WhiteLabel, 'id'>,
  ): Promise<WhiteLabel> {
    return this.tenantRepository.whiteLabel(id).create(whiteLabel);
  }

  @patch('/tenants/{id}/white-label', {
    responses: {
      '200': {
        description: 'Tenant.WhiteLabel PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WhiteLabel, {partial: true}),
        },
      },
    })
    whiteLabel: Partial<WhiteLabel>,
    @param.query.object('where', getWhereSchemaFor(WhiteLabel))
    where?: Where<WhiteLabel>,
  ): Promise<Count> {
    return this.tenantRepository.whiteLabel(id).patch(whiteLabel, where);
  }

  @put('/white-label/{id}', {
    responses: {
      '200': {
        description: 'Tenant.WhiteLabel PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async put(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WhiteLabel, {partial: true}),
        },
      },
    })
    whiteLabel: Partial<WhiteLabel>,
  ): Promise<void> {
    return this.whiteLabelRepository.updateById(id, whiteLabel);
  }

  @del('/tenants/{id}/white-label', {
    responses: {
      '200': {
        description: 'Tenant.WhiteLabel DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(WhiteLabel))
    where?: Where<WhiteLabel>,
  ): Promise<Count> {
    return this.tenantRepository.whiteLabel(id).delete(where);
  }
}
