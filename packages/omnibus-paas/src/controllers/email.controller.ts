/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  operation,
  requestBody,
  visibility,
  OperationVisibility,
  post,
} from '@loopback/rest';
import {Context, inject} from '@loopback/core';
import {EmailsService, EmailTypes} from '../services';
import {authenticate} from '@loopback/authentication';

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by email
 *
 */
@visibility(OperationVisibility.UNDOCUMENTED)
export class EmailController {
  constructor(
    @inject('services.EmailsService')
    private readonly emailsService: EmailsService,
    @inject.context()
    private readonly ctx: Context,
  ) {}

  /**
   * This endpoint sends emails for various processes (reset password, verification, etc.)
   *

   */

  /**
   * @deprecated
   * @param email
   */
  @operation('post', '/email/sendEmail')
  async sendEmailOld(@requestBody() email: any): Promise<any> {
    return this.sendEmail(email);
  }

  @post('/email/send-email')
  async sendEmail(
    @requestBody() email: {to: string; subject: string; templateId: EmailTypes},
  ): Promise<any> {
    const {to, subject, templateId} = email;
    return this.emailsService.sendEmail(templateId, to, subject, email);
  }

  @authenticate('jwt')
  @post('/email/send-mass-email')
  async sendMassEmail(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['emails', 'template', 'subject'],
            properties: {
              emails: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    email: {
                      type: 'string',
                    },
                    firstName: {
                      type: 'string',
                    },
                    lastName: {
                      type: 'string',
                    },
                    tenantId: {
                      type: 'string',
                    },
                  },
                },
              },
              template: {
                type: 'string',
              },
              subject: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    emailData: {
      emails: {
        email: string;
        firstName: string;
        lastName: string;
        tenantId: string;
        [key: string]: any;
      }[];
      subject: string;
      template: EmailTypes;
    },
  ): Promise<
    {
      type: 'success' | 'error';
      message: string;
    }[]
  > {
    const report: {type: 'success' | 'error'; message: string}[] = [];
    let totalSent = 0;
    for (const email of emailData.emails) {
      try {
        await this.emailsService.sendEmail(
          emailData.template,
          email.email,
          emailData.subject,
          email,
          email.tenantId,
        );
        totalSent++;
      } catch (e) {
        report.push({
          type: 'error',
          message: `Failed to send to ${email.email}: ${e.message}`,
        });
      }
    }
    if (totalSent === emailData.emails.length) {
      report.push({
        type: 'success',
        message: 'Sent All Emails Successfully',
      });
    } else {
      report.push({
        type: 'success',
        message: `Sent ${totalSent} Emails Successfully. ${
          emailData.emails.length - totalSent
        } Emails had errors`,
      });
    }
    console.log(report);
    return report;
  }
}
