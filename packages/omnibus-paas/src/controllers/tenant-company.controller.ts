import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Tenant, Company} from '../models';
import {TenantRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class TenantCompanyController {
  constructor(
    @repository(TenantRepository)
    public tenantRepository: TenantRepository,
  ) {}

  @get('/tenants/{id}/company', {
    responses: {
      '200': {
        description: 'Company belonging to Tenant',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Company)},
          },
        },
      },
    },
  })
  async getCompany(
    @param.path.string('id') id: typeof Tenant.prototype.id,
  ): Promise<Company> {
    return this.tenantRepository.masterEntity(id);
  }
}
