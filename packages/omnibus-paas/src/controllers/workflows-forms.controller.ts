import {EntityNotFoundError} from '@loopback/repository';
import {
  getJsonSchemaRef,
  operation,
  param,
  visibility,
  OperationVisibility,
} from '@loopback/rest';

import * as Models from '../models/workflows';

@visibility(OperationVisibility.UNDOCUMENTED)
export class WorkflowsFormsController {
  constructor() {}

  @operation('get', '/workflows/forms/{key}/describe')
  async findByKey(@param.path.string('key') key: string): Promise<object> {
    switch (key) {
      case 'start':
        return getJsonSchemaRef(Models.Start);
      case 'bank-account-business':
        return getJsonSchemaRef(Models.BankAccount);
      default:
        throw new EntityNotFoundError('metadata', key);
    }
  }
}
