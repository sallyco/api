import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  tags,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {inject} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {ProductCategory} from '../models';
import {ProductCategoryRepository} from '../repositories';

@tags('Products')
@visibility(OperationVisibility.UNDOCUMENTED)
export class ProductCategoriesController {
  constructor(
    @repository(ProductCategoryRepository)
    public productCategoryRepository: ProductCategoryRepository,
  ) {}

  @authenticate('jwt')
  @post('/products/categories')
  @response(200, {
    description: 'ProductCategory model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductCategory)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductCategory, {
            title: 'NewProductCategory',
            exclude: ['id'],
          }),
        },
      },
    })
    productCategory: Omit<ProductCategory, 'id'>,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<ProductCategory> {
    return this.productCategoryRepository.create(productCategory);
  }

  @get('/products/categories/count')
  @response(200, {
    description: 'ProductCategory model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductCategory) where?: Where<ProductCategory>,
  ): Promise<Count> {
    return this.productCategoryRepository.count(where);
  }

  @get('/products/categories')
  @response(200, {
    description: 'Array of ProductCategory model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductCategory, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductCategory) filter?: Filter<ProductCategory>,
  ): Promise<ProductCategory[]> {
    return this.productCategoryRepository.find(filter);
  }

  @get('/products/categories/{id}')
  @response(200, {
    description: 'ProductCategory model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductCategory, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ProductCategory, {exclude: 'where'})
    filter?: FilterExcludingWhere<ProductCategory>,
  ): Promise<ProductCategory> {
    return this.productCategoryRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @patch('/products/categories/{id}')
  @response(204, {
    description: 'ProductCategory PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductCategory, {partial: true}),
        },
      },
    })
    productCategory: ProductCategory,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.productCategoryRepository.updateById(id, productCategory);
  }

  @authenticate('jwt')
  @put('/products/categories/{id}')
  @response(204, {
    description: 'ProductCategory PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() productCategory: ProductCategory,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.productCategoryRepository.replaceById(id, productCategory);
  }

  @authenticate('jwt')
  @del('/products/categories/{id}')
  @response(204, {
    description: 'ProductCategory DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    await this.productCategoryRepository.deleteById(id);
  }
}
