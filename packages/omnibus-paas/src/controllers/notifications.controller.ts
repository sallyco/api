import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterBuilder,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  getModelSchemaRef,
  HttpErrors,
  operation,
  param,
  requestBody,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {unset} from 'lodash';
import {Notification, NotificationsList} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {NotificationRepository, ProfileRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class NotificationsController {
  constructor(
    @repository(NotificationRepository)
    public notificationRepository: NotificationRepository,
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
    @inject(SecurityBindings.USER)
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT)
    public tenant: Tenant,
  ) {}

  scopeFilterToUser(filter?: Filter<Notification>): Filter<Notification> {
    return new FilterBuilder<Notification>(filter)
      .impose({where: {userId: this.profile.id, tenantId: this.tenant.id}})
      .build();
  }

  @authenticate('jwt')
  @operation('post', '/notifications', {
    responses: {
      '200': {
        description: 'Notification model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(Notification)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Notification, {
            title: 'NewNotification',
            exclude: ['id'],
          }),
        },
      },
    })
    notification: Omit<Notification, 'id'>,
  ): Promise<Notification> {
    if (!notification.userId && notification.profileId) {
      const foundProfile = await this.profileRepository.findById(
        notification.profileId,
      );
      if (!foundProfile.isDeleted) {
        notification.userId = foundProfile.ownerId;
        unset(notification, 'profileId');
      } else {
        throw new HttpErrors.NotFound('Notification recipient cannot be found');
      }
    }

    return this.notificationRepository.create(notification);
  }

  /**
   *
   * @param where
   */
  @authenticate('jwt')
  @operation('get', '/notifications/count', {
    responses: {
      '200': {
        description: 'Notification model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Notification) where?: Where<Notification>,
  ): Promise<Count> {
    return this.notificationRepository.count(where);
  }

  @authenticate('jwt')
  @operation('get', '/notifications')
  async find(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @param.filter(Notification) filter?: Filter<Notification>,
  ): Promise<NotificationsList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const data = await this.notificationRepository.find(
      this.scopeFilterToUser(filter),
    );
    return new NotificationsList({
      totalCount: data.length,
      perPage,
      page,
      data,
    });
  }

  /**
   *
   * @param id
   * @param filter
   */
  @authenticate('jwt')
  @operation('get', '/notifications/{id}', {
    responses: {
      '200': {
        description: 'Notification model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Notification, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Notification, {exclude: 'where'})
    filter?: FilterExcludingWhere<Notification>,
  ): Promise<Notification> {
    return this.notificationRepository.findById(id, filter);
  }

  /**
   *
   * @param id
   * @param notification
   */
  @authenticate('jwt')
  @operation('put', '/notifications/{id}')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Notification, {partial: true}),
        },
      },
    })
    notification: Notification,
  ): Promise<Notification> {
    notification.updatedAt = new Date().toString();
    await this.notificationRepository.updateById(id, notification);
    return this.notificationRepository.findById(id);
  }

  /**
   *
   * @param id
   */
  @authenticate('jwt')
  @operation('delete', '/notifications/{id}', {
    responses: {
      '204': {
        description: 'Notification DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.notificationRepository.deleteById(id);
  }
}
