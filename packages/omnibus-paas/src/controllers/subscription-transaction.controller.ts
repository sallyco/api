import {authenticate} from '@loopback/authentication';
import {Filter, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Transaction} from '../models';
import {SubscriptionRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class SubscriptionTransactionController {
  constructor(
    @repository(SubscriptionRepository)
    protected subscriptionRepository: SubscriptionRepository,
  ) {}

  @get('/subscriptions/{id}/transactions', {
    responses: {
      '200': {
        description: 'Array of Subscription has many Transaction',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Transaction)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Transaction>,
  ): Promise<Transaction[]> {
    return this.subscriptionRepository.transactions(id).find(filter);
  }
}
