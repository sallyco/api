import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterBuilder,
  FilterExcludingWhere,
  repository,
  Where,
  WhereBuilder,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  Response,
  RestBindings,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {Account} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {AccountRepository} from '../repositories';

@authenticate('jwt')
@visibility(OperationVisibility.UNDOCUMENTED)
export class AccountsController {
  constructor(
    @repository(AccountRepository)
    public accountRepository: AccountRepository,
    @inject(SecurityBindings.USER)
    public profile: UserProfile,
    @inject(RestBindings.Http.RESPONSE)
    public response: Response,
    @inject(MultiTenancyBindings.CURRENT_TENANT)
    public tenant: Tenant,
  ) {}

  scopeFilterToUser(filter?: Filter<Account>): Filter<Account> {
    return new FilterBuilder<Account>(filter)
      .impose({where: {ownerId: this.profile.id, tenantId: this.tenant.id}})
      .build();
  }

  scopeWhereToUser(where?: Where<Account>): Where<Account> {
    return new WhereBuilder<Account>(where)
      .impose({ownerId: this.profile.id, tenantId: this.tenant.id})
      .build();
  }

  @post('/accounts', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: {'application/json': {schema: getModelSchemaRef(Account)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Account, {
            title: 'NewAccount',
            exclude: ['id'],
          }),
        },
      },
    })
    account: Omit<Account, 'id'>,
  ): Promise<Account> {
    account.ownerId = this.profile.id;
    return this.accountRepository.create(account);
  }

  @get('/accounts/count', {
    responses: {
      '200': {
        description: 'Account model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Account) where?: Where<Account>): Promise<Count> {
    return this.accountRepository.count(this.scopeWhereToUser(where));
  }

  @authenticate('jwt')
  @get('/accounts', {
    responses: {
      '200': {
        description: 'Array of Account model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Account, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.filter(Account) filter?: Filter<Account>,
  ): Promise<Account[]> {
    const count = await this.accountRepository.count(
      this.scopeWhereToUser(filter?.where),
    );
    this.response.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    this.response.setHeader('X-Total-Count', count.count);
    return this.accountRepository.find(this.scopeFilterToUser(filter));
  }

  @get('/accounts/{id}', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Account, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Account, {exclude: 'where'})
    filter?: FilterExcludingWhere<Account>,
  ): Promise<Account> {
    return this.accountRepository.findById(id, this.scopeFilterToUser(filter));
  }

  @patch('/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Account, {partial: true}),
        },
      },
    })
    account: Account,
  ): Promise<void> {
    await this.accountRepository.findById(id, this.scopeFilterToUser());
    await this.accountRepository.updateById(id, account);
  }

  @put('/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() account: Account,
  ): Promise<void> {
    await this.accountRepository.findById(id, this.scopeFilterToUser());
    await this.accountRepository.replaceById(id, account);
  }

  @del('/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.accountRepository.findById(id, this.scopeFilterToUser());
    await this.accountRepository.deleteById(id);
  }

  @del('/accounts/{id}/{subAccountId}', {
    responses: {
      '204': {
        description: 'Account DELETE success',
      },
    },
  })
  async deleteSubAccountById(
    @param.path.string('id') id: string,
    @param.path.string('subAccountId') subAccountId: string,
  ): Promise<void> {
    const account = await this.accountRepository.findById(
      id,
      this.scopeFilterToUser(),
    );

    account.plaidData.accounts = account.plaidData.accounts.filter(
      pAcc => pAcc.id !== subAccountId,
    );

    await this.accountRepository.updateById(id, {
      ...account,
    });
  }
}
