/* eslint-disable @typescript-eslint/no-explicit-any */
import {authorize} from '../authorization';
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {Filter, EntityNotFoundError, repository} from '@loopback/repository';
import {
  getModelSchemaRef,
  oas,
  operation,
  param,
  requestBody,
  Response,
  RestBindings,
  visibility,
  OperationVisibility,
  HttpErrors,
} from '@loopback/rest';
import {MinIOServiceBindings} from '../keys';
import {Tenant as TenantModel, Theme, WhiteLabel} from '../models';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {log, LOG_LEVEL} from '../logger';
import {
  DealRepository,
  TenantRepository,
  ThemeRepository,
} from '../repositories';
import {Banking, BankingProvider, MinIOService} from '../services';

import _ from 'lodash';

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by tenants
 *
 */
@visibility(OperationVisibility.UNDOCUMENTED)
export class TenantsController {
  constructor(
    @repository(ThemeRepository) public themeRepository: ThemeRepository,
    @repository(TenantRepository) public tenantRepository: TenantRepository,
    @repository(DealRepository) public dealRepository: DealRepository,
    @inject(MinIOServiceBindings.MINIO_SERVICE) private minio: MinIOService,
  ) {}

  /**
   * This endpoint lists all the tenants
   *

   /**
   *
   *

   * @param tenantId The id of the tenant to retrieve
   * @returns Expected response to a valid request
   */
  @log(LOG_LEVEL.INFO)
  @oas.tags('TenantNotRequired')
  @operation('get', '/tenants/{tenantId}')
  async readTenantById(
    @param({name: 'tenantId', in: 'path'}) tenantId: string,
    @param.filter(TenantModel) filter?: Filter<TenantModel>,
  ): Promise<TenantModel> {
    if (tenantId === 'master') {
      return new TenantModel({
        id: 'master',
        name: 'GBT Super Admin',
        assets: {
          images: {
            logo: 'https://marketing-images-gbt.s3.us-east-2.amazonaws.com/adminlogo.png',
            logoInverted:
              'https://marketing-images-gbt.s3.us-east-2.amazonaws.com/adminlogoinverted.png',
          },
        },
        whiteLabel: new WhiteLabel({
          images: {
            logo: 'https://marketing-images-gbt.s3.us-east-2.amazonaws.com/adminlogo.png',
            logoInverted:
              'https://marketing-images-gbt.s3.us-east-2.amazonaws.com/adminlogoinverted.png',
          },
        }),
      });
    }
    const tenant = await this.tenantRepository.findById(tenantId, filter);
    let closedCount = 0;
    if (tenant) {
      closedCount = (await this.dealRepository.getClosedDealCount(tenantId))
        .count;
    }
    tenant.closedStats =
      '' + closedCount + '/' + (tenant.dealLimits?.maxDealCount ?? 0);
    tenant.maxAllowedDaysToClose = tenant.maxAllowedDaysToClose ?? 0;
    return tenant;
  }

  @authenticate('jwt')
  @log(LOG_LEVEL.DEBUG)
  @operation('get', '/tenants')
  async readTenants(
    @param.filter(TenantModel) filter?: Filter<TenantModel>,
  ): Promise<TenantModel[]> {
    return this.tenantRepository.find(filter);
  }

  @log(LOG_LEVEL.INFO)
  @oas.tags('TenantNotRequired')
  @operation('get', '/tenants/by-url/{url}')
  async getTenantByUrl(
    @param({name: 'url', in: 'path'}) url: string,
  ): Promise<object> {
    //TODO this doesn't always work. I think it has something to do with how we hand ids on tennats
    //const tenant = await this.tenantRepository.find({
    //  where: {omdUrl: url},
    //});
    const tenant =
      url === 'portal.variaventures.com' || url === 'sages.variaventures.com'
        ? [{id: 'varia'}]
        : [];
    //
    if (tenant.length <= 0) {
      throw new HttpErrors.NotFound('No tenant found');
    }
    return _.pick(tenant[0], ['id']);
  }

  @operation('get', '/tenants/{tenantId}/theme')
  @oas.response.file()
  async readThemeByTenantId(
    @param({name: 'tenantId', in: 'path'}) tenantId: string,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    let fileObject;
    try {
      response.setHeader(
        'Cache-Control',
        'max-age=6000, stale-while-revalidate=3000',
      );
      let theme = await this.themeRepository.findOne({
        where: {tenantId: tenantId},
      });
      if (!theme) {
        // Allow fallback to root folder css in bucket
        theme = new Theme({
          tenantId: tenantId,
          name: tenantId,
          key: tenantId + '.theme.css',
        });
      }
      fileObject = await this.minio.getObject(theme.key);
      response.attachment(theme.name);
      await new Promise(resolve => {
        const stream = fileObject.pipe(response);
        stream.on('finish', resolve);
      });
    } catch {
      throw new EntityNotFoundError('Theme', tenantId);
    }
  }

  @authenticate('jwt')
  @operation('post', '/tenants', {
    responses: {
      '200': {
        description: 'Tenant model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(TenantModel)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TenantModel),
        },
      },
    })
    tenant: TenantModel,
  ): Promise<TenantModel> {
    return this.tenantRepository.create(tenant);
  }

  /**
   *
   * @param id
   * @param tenant
   */
  @authenticate('jwt')
  @operation('put', '/tenants/{id}')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TenantModel, {partial: true}),
        },
      },
    })
    tenant: TenantModel,
  ): Promise<TenantModel> {
    if (Object.prototype.hasOwnProperty.call(tenant, 'closedStats')) {
      delete tenant.closedStats;
    }
    await this.tenantRepository.updateById(id, tenant);
    const updatedTenant = await this.tenantRepository.findById(id);
    let closedCount = 0;
    if (updatedTenant) {
      closedCount = (await this.dealRepository.getClosedDealCount(id)).count;
    }
    updatedTenant.closedStats =
      '' + closedCount + '/' + (updatedTenant.dealLimits?.maxDealCount ?? 0);
    return updatedTenant;
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @operation('post', '/tenants/{tenantId}/bank-account', {
    operationId: 'createBankAccountForTenant',
    summary: 'Create a Bank Account for a tenant',
    description:
      'Creates a Bank Account for the Tenant. ' +
      'An EIN for the tenant manager is required for this request to succeed. ',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '204': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async createBankAccountForTenant(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @service(BankingProvider) bankingService: Banking,
    @param.path.string('tenantId') entityId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            accountPurpose: {
              type: 'string',
              required: true,
              jsonSchema: {
                enum: ['FEES', 'BLUESKY'],
              },
            },
          },
        },
      },
    })
    req: {
      accountPurpose: string;
    },
  ): Promise<void> {
    const tenant: any = await this.tenantRepository.findById(entityId, {
      include: ['manager'],
    });

    if (
      !req.accountPurpose ||
      (req.accountPurpose !== 'FEES' && req.accountPurpose !== 'BLUESKY')
    ) {
      throw new HttpErrors.UnprocessableEntity(
        'Account purpose must be FEES or BLUESKY',
      );
    }
    if (
      (req.accountPurpose === 'FEES' && tenant.feesPaymentsAccount) ||
      (req.accountPurpose === 'BLUESKY' && tenant.blueSkyPaymentsAccount)
    ) {
      throw new HttpErrors.Conflict('Tenant has existing banking account');
    }
    if (!tenant.manager) {
      throw new HttpErrors.UnprocessableEntity('Tenant must have a manager');
    }
    try {
      if (req.accountPurpose === 'FEES') {
        const acct = await bankingService.createTenantFeesAccount(
          tenant.manager,
          tenant.name.substr(0, 24) + ' FEES',
        );
        await this.tenantRepository.feesPaymentsAccount(tenant.id).create(acct);
      } else if (req.accountPurpose === 'BLUESKY') {
        const acct = await bankingService.createTenantFeesAccount(
          tenant.manager,
          tenant.name.substr(0, 24) + ' BLUESKY',
        );
        await this.tenantRepository
          .blueSkyPaymentsAccount(tenant.id)
          .create(acct);
      }
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }
}
