/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-shadow */
import {authenticate} from '@loopback/authentication';
import {inject, service} from '@loopback/core';
import {Filter, repository, WhereBuilder} from '@loopback/repository';
import {log, LOG_LEVEL} from '../logger';
import {
  tags,
  getModelSchemaRef,
  HttpErrors,
  operation,
  param,
  requestBody,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
//import debugFactory from 'debug';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {Company} from '../models';
import {CompaniesList} from '../models/company-list.model';
import {FilesList} from '../models/file-list.models';
import {FileRepository} from '../repositories';
import {CompanyRepository} from '../repositories/company.repository';
import {DocumentsService, FilingsService} from '../services';
import {FileUploadHandler} from '../types';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
//const debug = debugFactory('omnibus-paas:entities:controller');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by companies
 *
 */
@tags('Companies')
export class CompaniesController {
  constructor(
    @repository(CompanyRepository)
    public companyRepository: CompanyRepository,
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @service(DocumentsService)
    private documentsService: DocumentsService,
    @repository(FileRepository)
    public fileRepository: FileRepository,
  ) {}

  /**
   * This endpoint lists all the companies
   *

   * @returns A paged array of companies
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   */
  @authenticate('jwt')
  @operation('get', '/companies', {
    operationId: 'getCompanies',
    summary: 'List Companies',
    description: 'Get a list of Companies for the current user',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(CompaniesList),
          },
        },
      },
    },
  })
  async readCompanies(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Company) filter?: Filter<Company>,
  ): Promise<CompaniesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const builder = new WhereBuilder(filter?.where).and({id: {exists: true}});

    const countObj = await this.companyRepository.count(
      builder.impose(profile.where).build(),
    );

    return new CompaniesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: await this.companyRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: builder.impose(profile.where).build(),
      }),
    });
  }

  /**
   *
   *

   */
  @authenticate('jwt')
  @operation('post', '/companies', {
    operationId: 'createCompany',
    summary: 'Create a Company',
    description: 'Create a Company Object from the supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Company),
          },
        },
      },
    },
  })
  async createCompany(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {
            title: 'NewCompany',
            exclude: ['id'],
          }),
        },
      },
    })
    company: Company,
  ): Promise<Company> {
    company.ownerId = profile.id;
    // if (company.type === 'FUND_MANAGER' && company.isEntity && company.entityInfo) {
    return this.companyRepository.create(company);
    // } else if (company.type === 'FUND_MANAGER' && !company.isEntity && company.email && company.phone) {
    //   return this.companyRepository.create(company);
    // } else if (company.type === 'REGISTERED_AGENT') {
    //   return this.companyRepository.create(company);
    // } else {
    //   throw new HttpErrors.UnprocessableEntity('The request body is invalid.')
    // }
  }

  /**
   *
   *

   * @param companyId The id of the company to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('get', '/companies/{companyId}', {
    operationId: 'getCompanyById',
    summary: 'Get a Company by Id',
    description: 'Get a Company matching the supplied Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Company),
          },
        },
      },
    },
  })
  async readCompanyById(
    @param({name: 'companyId', in: 'path'}) companyId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Company> {
    const company = await this.companyRepository.findById(companyId);
    if (tenant.id === 'master') {
      return company;
    }
    if (
      company?.ownerId === profile.id ||
      (['MASTER_ENTITY', 'MANAGER'].includes(company.type ?? '') &&
        company.tenantId === tenant.id)
    ) {
      return company;
    } else {
      throw new HttpErrors.NotFound(
        'The company you are trying to find cannot be found.',
      );
    }
  }

  /**
   *
   *

   * @param profile
   * @param companyId The id of the company to update
   * @param company
   * @returns Expected response to a valid request
   */
  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @operation('put', '/companies/{companyId}', {
    operationId: 'updateCompanyById',
    summary: 'Update a Company',
    description: 'Update the specified Company with supplied data',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Company),
          },
        },
      },
    },
  })
  async updateCompanyById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'companyId', in: 'path'}) companyId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {partial: true}),
        },
      },
    })
    company: Company,
  ): Promise<Company> {
    const retrievedCompany = await this.companyRepository.findById(companyId);
    if (retrievedCompany) {
      company.updatedAt = new Date();
      await this.companyRepository.updateById(companyId, company);
      return this.companyRepository.findById(companyId);
    } else {
      throw new HttpErrors.NotFound(
        'The company you are trying to update cannot be found.',
      );
    }
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('POST', '/companies/{companyId}/{managerId}/master-formation')
  async companyMasterFormation(
    @param({name: 'companyId', in: 'path'}) companyId: string,
    @param({name: 'managerId', in: 'path'}) managerId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @service(FilingsService) filings: FilingsService,
  ): Promise<void> {
    const company = await this.companyRepository.findById(companyId);
    const manager = await this.companyRepository.findById(managerId);
    try {
      await filings.createMasterFormationOrder(company, manager);
    } catch (e) {
      throw new HttpErrors.UnprocessableEntity(e.message);
    }
  }

  /**
   *
   *

   * @param companyId The id of the company to retrieve
   * @param profile
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('delete', '/companies/{companyId}', {
    operationId: 'deleteCompany',
    summary: 'Delete a Company',
    description: 'Delete the Specified Company',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async deleteCompanyById(
    @param({name: 'companyId', in: 'path'}) companyId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<void> {
    const company = await this.companyRepository.findById(companyId);

    if (company.ownerId !== profile.id && tenant.id !== 'master') {
      throw new HttpErrors.NotFound(
        'The company you are trying to delete cannot be found.',
      );
    } else {
      await this.companyRepository.deleteById(companyId);
    }
  }

  /**
  *
  *

  */
  @authenticate('jwt')
  @operation('post', '/companies/search', {
    operationId: 'searchCompanies',
    summary: 'Search for a Company',
    description:
      'Search Companies for the current user by the supplied paramters',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(CompaniesList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number
          * StartingIndex(page) is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async search(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              perPage: {
                type: 'number',
              },
              page: {
                type: 'number',
              },
              search: {
                type: 'array',
                items: {
                  type: 'object',
                },
              },
              sort: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    field: {
                      type: 'string',
                    },
                    direction: {
                      type: 'string',
                      enum: ['ASCENDING', 'DESCENDING'],
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    searchObj: object,
  ): Promise<CompaniesList> {
    let perPage = searchObj['perPage'];
    let page = searchObj['page'];
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const searchParamsList = [] as any;
    const sortParamList = [] as any;
    if (searchObj['search'] !== undefined && searchObj['search'].length > 0) {
      for (let index = 0; index < searchObj['search'].length; index++) {
        const param = {};
        param[searchObj['search'][index].field] =
          searchObj['search'][index].value;
        searchParamsList.push(param);
      }
    }

    if (searchObj['sort'] !== undefined && searchObj['sort'].length > 0) {
      for (let index = 0; index < searchObj['sort'].length; index++) {
        let direction = 'ASC';
        if (searchObj['sort'][index].direction === 'DESCENDING')
          direction = 'DESC';
        const param = searchObj['sort'][index].field + ' ' + direction;
        sortParamList.push(param);
      }
    }

    searchParamsList.push({ownerId: profile.id});
    const count = await this.companyRepository.count({});

    return new CompaniesList({
      totalCount: count.count,
      perPage: perPage,
      page: page,
      data: await this.companyRepository.find({
        limit: perPage,
        skip: page * perPage,
        where: {and: searchParamsList},
        order: sortParamList,
      }),
    });
  }

  /**
   *
   *

   * @param companyId The id of the company to retrieve
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/companies/{companyId}/files', {
    operationId: 'getCompanyFiles',
    summary: 'Get files of a Company by Id',
    description: 'Get files of a Company matching the supplied Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(FilesList),
          },
        },
      },
    },
  })
  async getCompanyFiles(
    @param({name: 'companyId', in: 'path'}) companyId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<FilesList> {
    const filesPromise = await this.companyRepository
      .findById(companyId)
      .then(company => {
        return company.files?.map(async fileId => {
          return this.fileRepository.findById(fileId);
        });
      });

    if (!filesPromise || !filesPromise.length) {
      return new FilesList({
        data: [],
      });
    }
    const files = await Promise.all(filesPromise);

    if (tenant.id === 'master') {
      return new FilesList({
        data: files,
      });
    } else {
      throw new HttpErrors.NotFound(
        'The company you are trying to find cannot be found.',
      );
    }
  }
}
