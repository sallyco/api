import {authenticate} from '@loopback/authentication';
import {authorize} from '../authorization';
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  put,
  del,
  requestBody,
  response,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {BlueSkyFee, BlueSkyFeeTier, BlueSkyFeeWithRelations} from '../models';
import {BlueSkyFeeRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class BlueSkyFeesController {
  constructor(
    @repository(BlueSkyFeeRepository)
    public blueSkyFeeRepository: BlueSkyFeeRepository,
  ) {}

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @post('/blue-sky-fees')
  @response(200, {
    description: 'BlueSkyFee model instance',
    content: {'application/json': {schema: getModelSchemaRef(BlueSkyFee)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlueSkyFee, {
            title: 'NewBlueSkyFee',
            exclude: ['id'],
          }),
        },
      },
    })
    blueSkyFee: Omit<BlueSkyFee, 'id'>,
  ): Promise<BlueSkyFee> {
    return this.blueSkyFeeRepository.create(blueSkyFee);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/blue-sky-fees')
  @response(200, {
    description: 'Array of BlueSkyFee model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(BlueSkyFee, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(BlueSkyFee) filter?: Filter<BlueSkyFee>,
  ): Promise<BlueSkyFee[]> {
    return this.blueSkyFeeRepository.find(filter);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @get('/blue-sky-fees/{id}')
  @response(200, {
    description: 'BlueSkyFee model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(BlueSkyFee, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(BlueSkyFee, {exclude: 'where'})
    filter?: FilterExcludingWhere<BlueSkyFee>,
  ): Promise<BlueSkyFee> {
    return this.blueSkyFeeRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @put('/blue-sky-fees/{id}')
  @response(204, {
    description: 'BlueSkyFee PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() blueSkyFee: BlueSkyFeeWithRelations,
  ): Promise<void> {
    await this.blueSkyFeeRepository.blueSkyFeeTiers(blueSkyFee.id).delete();

    let bsf;
    if (blueSkyFee.blueSkyFeeTiers) {
      const {blueSkyFeeTiers, ...rest} = blueSkyFee;
      bsf = rest;

      const newbsft = blueSkyFeeTiers.map((entry: BlueSkyFeeTier) => {
        return this.blueSkyFeeRepository.blueSkyFeeTiers(id).create(entry);
      });

      await Promise.all(newbsft);
    } else {
      bsf = blueSkyFee;
    }
    await this.blueSkyFeeRepository.replaceById(id, bsf);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @del('/blue-sky-fees/{id}')
  @response(204, {
    description: 'BlueSkyFee DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.blueSkyFeeRepository.deleteById(id);
  }
}
