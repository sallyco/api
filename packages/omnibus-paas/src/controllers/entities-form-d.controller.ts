import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  OperationVisibility,
  param,
  patch,
  post,
  requestBody,
  visibility,
} from '@loopback/rest';
import {Entities, FormD} from '../models';
import {EntitiesRepository} from '../repositories';

@visibility(OperationVisibility.UNDOCUMENTED)
export class EntitiesFormDController {
  constructor(
    @repository(EntitiesRepository)
    protected entitiesRepository: EntitiesRepository,
  ) {}

  @get('/entities/{id}/form-d', {
    responses: {
      '200': {
        description: 'Entities has one FormD',
        content: {
          'application/json': {
            schema: getModelSchemaRef(FormD),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<FormD>,
  ): Promise<FormD> {
    return this.entitiesRepository.formD(id).get(filter);
  }

  @post('/entities/{id}/form-d', {
    responses: {
      '200': {
        description: 'Entities model instance',
        content: {'application/json': {schema: getModelSchemaRef(FormD)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Entities.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FormD, {
            title: 'NewFormDInEntities',
            exclude: ['id'],
            optional: ['entitiesId'],
          }),
        },
      },
    })
    formD: Omit<FormD, 'id'>,
  ): Promise<FormD> {
    return this.entitiesRepository.formD(id).create(formD);
  }

  @patch('/entities/{id}/form-d', {
    responses: {
      '200': {
        description: 'Entities.FormD PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FormD, {partial: true}),
        },
      },
    })
    formD: Partial<FormD>,
    @param.query.object('where', getWhereSchemaFor(FormD)) where?: Where<FormD>,
  ): Promise<Count> {
    return this.entitiesRepository.formD(id).patch(formD, where);
  }

  @del('/entities/{id}/form-d', {
    responses: {
      '200': {
        description: 'Entities.FormD DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(FormD)) where?: Where<FormD>,
  ): Promise<Count> {
    return this.entitiesRepository.formD(id).delete(where);
  }
}
