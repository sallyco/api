import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Profile, Accreditation} from '../models';
import {ProfileRepository} from '../repositories';

export class ProfileAccreditationController {
  constructor(
    @repository(ProfileRepository)
    protected profileRepository: ProfileRepository,
  ) {}

  @get('/profiles/{id}/accreditations', {
    responses: {
      '200': {
        description: 'Array of Profile has many Accreditation',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Accreditation)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Accreditation>,
  ): Promise<Accreditation[]> {
    return this.profileRepository.accreditations(id).find(filter);
  }

  @post('/profiles/{id}/accreditations', {
    responses: {
      '200': {
        description: 'Profile model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(Accreditation)},
        },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Profile.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {
            title: 'NewAccreditationInProfile',
            exclude: ['id'],
            optional: ['profileId'],
          }),
        },
      },
    })
    accreditation: Omit<Accreditation, 'id'>,
  ): Promise<Accreditation> {
    return this.profileRepository.accreditations(id).create(accreditation);
  }

  @patch('/profiles/{id}/accreditations', {
    responses: {
      '200': {
        description: 'Profile.Accreditation PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Accreditation, {partial: true}),
        },
      },
    })
    accreditation: Partial<Accreditation>,
    @param.query.object('where', getWhereSchemaFor(Accreditation))
    where?: Where<Accreditation>,
  ): Promise<Count> {
    return this.profileRepository
      .accreditations(id)
      .patch(accreditation, where);
  }

  @del('/profiles/{id}/accreditations', {
    responses: {
      '200': {
        description: 'Profile.Accreditation DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Accreditation))
    where?: Where<Accreditation>,
  ): Promise<Count> {
    return this.profileRepository.accreditations(id).delete(where);
  }
}
