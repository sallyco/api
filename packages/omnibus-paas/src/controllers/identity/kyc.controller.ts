import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  del,
  requestBody,
  response,
  tags,
  HttpErrors,
  patch,
} from '@loopback/rest';
import {Kyc, KycCheckRequest} from '../../models';
import {KycRepository, ProfileRepository} from '../../repositories';
import {service} from '@loopback/core';
import {KycAml, KycAmlProvider} from '../../services';
import {ISOHelper} from '../../interfaces/datatypes/ISO';
import {authenticate} from '@loopback/authentication';
import moment from 'moment';

@tags('Identity', 'KYC')
@authenticate('jwt')
export class KycController {
  constructor(
    @repository(KycRepository)
    public kycRepository: KycRepository,
    @repository(ProfileRepository)
    public profileRepository: ProfileRepository,
    @service(KycAmlProvider)
    public kycAmlService: KycAml,
  ) {}

  @post('/identity/kyc')
  @response(200, {
    description: 'KYC model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Kyc),
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(KycCheckRequest),
          examples: {
            'example-1': {
              description: 'Data to KYC Check',
              value: {
                firstName: 'John',
                lastName: 'Doe',
                email: 'jdoe@test.com',
                address: {
                  street1: '123 E St',
                  street2: 'Unit 2',
                  city: 'Salt Lake City',
                  region: 'Utah',
                  country: 'United States of America',
                  postalCode: '84121',
                },
                dateOfBirth: '02-21-1972',
                identificationNumbers: [
                  {
                    type: 'ssn',
                    value: '111-11-1111',
                  },
                ],
              },
            },
          },
        },
      },
    })
    kycCheckRequest: KycCheckRequest,
  ): Promise<Kyc> {
    // if (!await ajvInstance.validate(
    //   getJsonSchema(KycCheckRequest), kycCheckRequest) && ajvInstance.errors
    // ) {
    //   throw RestHttpErrors.invalidRequestBody(ajvInstance.errors.map(
    //     (e: ajv.ErrorObject): RestHttpErrors.ValidationErrorDetails => {
    //       return {
    //         path: e.dataPath,
    //         code: e.keyword,
    //         message: e.message ?? `must pass validation rule ${e.keyword}`,
    //         info: e.params,
    //       };
    //     },
    //   ));
    // }
    if ('profileId' in kycCheckRequest && kycCheckRequest.profileId) {
      const profile = await this.profileRepository.findById(
        kycCheckRequest.profileId,
      );
      const profileApplicantData =
        await KycAmlProvider.createApplicationFromProfile(profile);
      const profileApplicant = await this.kycAmlService.createApplicant(
        profileApplicantData,
      );
      const profileCheck = await this.kycAmlService.createCheck({
        applicantId: String(profileApplicant.id),
      });
      return this.kycRepository.create({
        ...profileCheck,
        profileId: kycCheckRequest.profileId,
      });
    }

    if (!kycCheckRequest.firstName) {
      throw new HttpErrors.BadRequest('First Name is Required');
    }
    if (!kycCheckRequest.lastName) {
      throw new HttpErrors.BadRequest('Last Name is Required');
    }
    if (
      !kycCheckRequest.dateOfBirth ||
      !moment(kycCheckRequest.dateOfBirth, 'YYYY-MM-DD').isValid()
    ) {
      throw new HttpErrors.BadRequest(
        'Date of Birth is Required in YYYY-MM-DD format',
      );
    }
    if (
      !kycCheckRequest.identificationNumbers ||
      kycCheckRequest.identificationNumbers.length < 1 ||
      kycCheckRequest.identificationNumbers.find(
        item => item.value.trim().length < 1,
      )
    ) {
      throw new HttpErrors.BadRequest('Identification Numbers are Required');
    }

    const applicant = await this.kycAmlService.createApplicant({
      address: {
        street1: String(kycCheckRequest.address?.street1),
        street2: kycCheckRequest.address?.street2,
        city: String(kycCheckRequest.address?.city),
        region: String(kycCheckRequest.address?.region),
        country: ISOHelper.convertCountryNameToISO_2(
          String(kycCheckRequest?.address?.country),
        ),
        postalCode: String(kycCheckRequest.address?.postalCode),
      },
      dob: String(kycCheckRequest.dateOfBirth),
      email: kycCheckRequest.email,
      firstName: String(kycCheckRequest.firstName),
      lastName: String(kycCheckRequest.lastName),
      idNumbers: [...kycCheckRequest.identificationNumbers!],
    });
    const check = await this.kycAmlService.createCheck({
      applicantId: String(applicant?.providerMeta?.id),
    });
    const kyc = await this.kycRepository.create(check);
    delete kyc.providerMeta;
    return kyc;
  }

  @get('/identity/kyc/count')
  @response(200, {
    description: 'KYC Check Count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Kyc) where?: Where<Kyc>): Promise<Count> {
    return this.kycRepository.count(where);
  }

  @get('/identity/kyc')
  @response(200, {
    description: 'Array of KYC model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Kyc, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Kyc) filter?: Filter<Kyc>): Promise<Kyc[]> {
    return this.kycRepository.find(filter);
  }

  @get('/identity/kyc/{id}')
  @response(200, {
    description: 'KYC Model Instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Kyc, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Kyc, {exclude: 'where'}) filter?: FilterExcludingWhere<Kyc>,
  ): Promise<Kyc> {
    return this.kycRepository.findById(id, filter);
  }

  @del('/identity/kyc/{id}')
  @response(204, {
    description: 'KYC DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.kycRepository.deleteById(id);
  }

  @patch('/identity/kyc/{id}')
  @response(200, {
    description: 'Array of KYC model instances',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Kyc, {includeRelations: true}),
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Kyc, {partial: true}),
          examples: {
            'example-1': {
              description: 'Data to Update',
              value: {
                result: 'CLEAR',
              },
            },
          },
        },
      },
    })
    kyc: Kyc,
  ) {
    return this.kycRepository.updateById(id, kyc);
  }
}
