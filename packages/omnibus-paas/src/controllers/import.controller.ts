import {inject, service} from '@loopback/core';
import {FileUploadHandler} from '../types';
import {FILE_UPLOAD_SERVICE} from '../keys';
import {
  HttpErrors,
  oas,
  param,
  post,
  requestBody,
  RestBindings,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {Request, Response} from 'express-serve-static-core';
import {DataImporterResolverService} from '../services';
import {IMPORTERS} from '../abstracts/data-importer';
import {get} from '@loopback/openapi-v3';

@visibility(OperationVisibility.UNDOCUMENTED)
export class ImportController {
  constructor(
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
  ) {}

  uploadAsync(request, response): Promise<void | Express.Multer.File> {
    return new Promise((resolve, reject) => {
      this.handler(request, response, (err: unknown) => {
        if (err) reject(err);
        resolve(request.files[0]);
      });
    });
  }

  @post('/import/{tenantId}/{ownerId}', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileImport(
    @requestBody.file()
    request: Request,
    @param({name: 'tenantId', in: 'path'}) tenantId: string,
    @param({name: 'ownerId', in: 'path'}) ownerId: string,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @service(DataImporterResolverService)
    dataImporterResolver: DataImporterResolverService,
  ): Promise<boolean> {
    const file = await this.uploadAsync(request, response);
    if (file) {
      const importer = dataImporterResolver.resolveImporter(
        // TODO: resolve this via parameter
        IMPORTERS[IMPORTERS.GBT_LEGACY],
      );
      //TODO: Update this to detect filetype from either extension or data
      //return importer.importFile("XLSX",request.files[0].path);
      return importer.importFile('XLSX', file.path, ownerId, tenantId);
    }
    throw new HttpErrors.BadRequest('CSV File is required');
  }

  @get('/import/schema')
  @oas.tags('TenantNotRequired')
  async fileImportSchema(
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @service(DataImporterResolverService)
    dataImporterResolver: DataImporterResolverService,
    @param.query.string('models') models: string,
  ): Promise<Buffer> {
    response.setHeader(
      'Content-Disposition',
      'attachment; filename=schema.xlsx',
    );
    response.setHeader('Content-Type', 'application/octet-stream');
    const importer = dataImporterResolver.resolveImporter(
      IMPORTERS[IMPORTERS.GBT_LEGACY],
    );
    let schema;
    if (!models) {
      schema = importer.getFlatSchemas();
    } else {
      const modelsList = models.split(',');
      schema = importer.getFlatSchemas(
        importer.allowedModels.filter(model =>
          modelsList.includes(model.modelName),
        ),
      );
    }
    return importer.convertFlatSchemaToXLSXFormat(schema);
  }
}
