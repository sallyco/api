/* eslint-disable @typescript-eslint/no-explicit-any */
import {authenticate} from '@loopback/authentication';
import {authorize} from '../authorization';

import {inject, service} from '@loopback/core';
import {Filter, repository} from '@loopback/repository';
import {
  tags,
  get,
  response,
  getModelSchemaRef,
  Response,
  RestBindings,
  HttpErrors,
  operation,
  param,
  requestBody,
  visibility,
  OperationVisibility,
} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {log, LOG_LEVEL} from '../logger';
import {Close, File, FormD} from '../models';
import {ClosesList} from '../models/closes-list.model';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {
  CloseRepository,
  DealRepository,
  FileRepository,
  TenantRepository,
  SubscriptionRepository,
  FormDRepository,
  EntitiesRepository,
} from '../repositories';
import {
  CeleryClientService,
  CloseService,
  TransactionService,
  DocumentsService,
  CloseAuditsService,
  CheckResultMap,
} from '../services';
import {DocumentsGeneratorService} from '../services/documents/documents-generator.service';
import _ from 'lodash';

const {v4: uuidv4} = require('uuid');
const fs = require('fs');
const path = require('path');

import debugFactory from 'debug';
const debug = debugFactory('omnibus-paas:closes:controller');

/**
 * The controller class is generated from OpenAPI spec with operations tagged
 * by Close
 *
 */
@tags('Closes')
export class CloseController {
  constructor(
    @repository(CloseRepository)
    public closeRepository: CloseRepository,
    @repository(FileRepository)
    public fileRepository: FileRepository,
    @repository(DealRepository)
    public dealRepository: DealRepository,
    @repository(TenantRepository)
    protected tenantRepository: TenantRepository,
    @repository(FormDRepository)
    protected formDRepository: FormDRepository,
    @repository(EntitiesRepository)
    protected entitiesRepository: EntitiesRepository,
    @service(DocumentsService)
    public documentsService: DocumentsService,
    @service(CloseService)
    public closeService: CloseService,
    @service(TransactionService)
    private transactionService: TransactionService,
    @service(DocumentsGeneratorService)
    public documentsGeneratorService: DocumentsGeneratorService,
    @service(CloseAuditsService)
    public closeAuditsService: CloseAuditsService,
  ) {}

  @authenticate('jwt')
  @operation('post', '/closes/audit-potential-close', {
    operationId: 'auditPotentialClose',
    summary: 'Audit potential close',
    description: 'Check if have potential for close.',
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                ALL_SUBSCRIPTION_DOCS_SIGNED: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
                POSITIVE_CLOSING_BALANCE: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
                INVESTOR_COUNT_LIMIT: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
                SUBSCRIPTION_COMMITMENT_AMOUNT_MATCH: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
                ALL_INVESTORS_PASSED_KYC: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
                ALL_INVESTORS_ACCREDITED: {
                  type: 'object',
                  properties: {
                    status: {
                      type: 'string',
                      enum: ['PASS', 'FAIL'],
                    },
                    failDetail: {
                      type: 'string',
                    },
                    data: {
                      type: 'object',
                    },
                  },
                },
              },
            },
          },
        },
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async auditPotentialClose(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              dealdId: {
                type: 'string',
              },
              subscriptionIds: {
                type: 'array',
                itemType: 'string',
              },
            },
          },
        },
      },
    })
    request: {
      dealId: string;
      subscriptionIds: string[];
    },
  ): Promise<CheckResultMap> {
    return this.closeAuditsService.auditPotentialClose(
      request.subscriptionIds,
      request.dealId,
    );
  }

  /**
   * List current user's closes to which the authentication method has access.
   *

   * @returns Expected response to a valid request
   * @param perPage
   * @param page
   * @param profile
   * @param tenant
   * @param filter
   */
  @authenticate('jwt')
  @operation('get', '/closes', {
    operationId: 'readCloses',
    summary: 'List closes',
    description: 'List closes to which the authentication method has access.',
    tags: ['Closes'],
    security: [
      {
        oAuth2: ['clientCredentials'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/ClosesList',
            },
          },
        },
      },
      '400': {
        description: 'Invalid Request',
      },
      '401': {
        description: 'Unauthorized',
      },
      '403': {
        description: 'Forbidden',
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number
          * StartingIndex(page) is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async readCloses(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.filter(Close) filter?: Filter<Close>,
  ): Promise<ClosesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 100;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    let countAndStatement = {};
    let listFindStatement = {};
    if (tenant.id !== 'master') {
      if (profile?.roles?.account?.roles.includes('admin')) {
        countAndStatement = {and: [{isDeleted: false}, {tenantId: tenant.id}]};
        listFindStatement = {
          limit: perPage,
          skip: page * perPage,
          where: {
            and: [
              {isDeleted: false},
              {tenantId: tenant.id},
              {...(filter?.where ?? [])},
            ],
          },
          include: [...(filter?.include ?? [])],
        };
      } else {
        countAndStatement = {
          and: [
            {isDeleted: false},
            {ownerId: profile.id},
            {tenantId: tenant.id},
          ],
        };
        listFindStatement = {
          limit: perPage,
          skip: page * perPage,
          where: {
            and: [
              {isDeleted: false},
              {ownerId: profile.id},
              {...(filter?.where ?? [])},
            ],
          },
          include: [...(filter?.include ?? [])],
        };
      }
    } else {
      countAndStatement = {and: [{isDeleted: false}]};
      listFindStatement = {
        limit: perPage,
        skip: page * perPage,
        where: {and: [{isDeleted: false}, {...(filter?.where ?? [])}]},
        include: [...(filter?.include ?? [])],
      };
    }

    const countObj = await this.closeRepository.count(countAndStatement);
    const data = await this.closeRepository.find(listFindStatement);
    _.forEach(data, entry => {
      let stat = {label: 'COMPLETE', labelColor: '#ababab'};
      if (entry.needsApproval) {
        stat = {label: 'NEEDS APPROVAL', labelColor: '#db0030'};
      } else if (!entry.fundManagerSigned) {
        stat = {label: 'NEEDS COUNTER SIGNING', labelColor: '#e400e8'};
      }
      _.set(entry, 'status', stat);
    });

    return new ClosesList({
      totalCount: countObj.count,
      perPage: perPage,
      page: page,
      data: data,
    });
  }

  @authenticate('jwt')
  @operation('get', '/closes-by-deal/{dealId}', {
    operationId: 'listClosesByDeal',
    summary: 'Get Closes by Deal Id',
    description: 'Get the Close objects associated with a Deal by Deal Id',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ClosesList),
          },
        },
      },
      '422': {
        description: `Unprocessable Entity
        
          * Limit(perPage) is not a natural number.
          * StartingIndex(page) is not a natural number
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async listClosesByDeal(
    @param({name: 'perPage', in: 'query'}) perPage: number,
    @param({name: 'page', in: 'query'}) page: number,
    @param({name: 'dealId', in: 'path'}) dealId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<ClosesList> {
    if (perPage === undefined && page === undefined) {
      perPage = 10;
      page = 0;
    } else if (!Number.isInteger(perPage) || perPage < 1) {
      throw new HttpErrors.UnprocessableEntity('limit is not a natural number');
    } else if (!Number.isInteger(page) || page < 0) {
      throw new HttpErrors.UnprocessableEntity(
        'startingIndex is not a natural number',
      );
    } else if (perPage > 100) {
      perPage = 100;
    }

    const closes = await this.closeService.getClosesByDeal(dealId, {
      page,
      perPage,
    });
    return closes;
  }

  /**
   * Create a new close for the user
   *

   */
  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @operation('post', '/closes', {
    operationId: 'createClose',
    summary: 'Create a Close',
    description: 'Create an Close object for the specified deal',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Close),
          },
        },
      },
      '424': {
        description: `Failed Dependency.
        
          * No valid subscriptions found to close.
          * All Subscriptions must be ready for closing.
        `,
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async createClose(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @repository(SubscriptionRepository)
    subscriptionRepository: SubscriptionRepository,
    @service(CeleryClientService) celeryClient: CeleryClientService,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              dealdId: {
                type: 'string',
              },
              organizerSignature: {
                type: 'string',
              },
              statement: {
                type: 'object',
              },
            },
          },
        },
      },
    })
    closeRequest: {
      dealId: string;
      organizerSignature: string;
      statement: object;
    },
  ): Promise<Close> {
    const deal = await this.dealRepository.findOne({
      where: {
        id: closeRequest.dealId,
        tenantId: tenant.id,
        ownerId: userProfile.id,
      },
    });
    let subscriptions = await subscriptionRepository.find({
      where: {and: [{dealId: closeRequest.dealId}, {isDeleted: false}]},
    });

    if (subscriptions.length < 1) {
      throw new HttpErrors.FailedDependency(
        'No valid subscriptions found to close',
      );
    }
    subscriptions = subscriptions.filter(
      subscription =>
        !['CLOSED', 'REFUNDED', 'CANCELED', 'CANCELLED', 'INVITED'].includes(
          subscription.status,
        ),
    );
    subscriptions.forEach(subscription => {
      if (subscription.status !== 'COMPLETED') {
        throw new HttpErrors.FailedDependency(
          'All Subscriptions must be ready for closing',
        );
      }
    });

    const close = new Close({
      dealId: closeRequest.dealId,
      organizerSignature: closeRequest.organizerSignature,
      statement: closeRequest.statement,
      entityId: deal?.entityId,
    });
    close.ownerId = userProfile.id;
    close.tenantId = tenant.id;
    close.subscriptions = subscriptions.map(subscription => subscription.id);
    close.dateClosed = new Date();
    close.needsApproval = true; //all closes must be approved
    const closeObject = await this.closeRepository.create(close);
    debug('closes:created', closeObject.id);
    await celeryClient.startClose(
      closeObject.id,
      closeRequest.organizerSignature,
    );
    return closeObject;
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('post', '/closes/{closeId}')
  async reprocessClose(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @repository(SubscriptionRepository)
    subscriptionRepository: SubscriptionRepository,
    @service(CeleryClientService) celeryClient: CeleryClientService,
    @param({name: 'closeId', in: 'path'}) closeId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            organizerSignature: {
              type: 'string',
            },
          },
        },
      },
    })
    closeRequest: {
      organizerSignature: string;
    },
  ): Promise<any> {
    const closeObject = await this.closeRepository.findById(closeId);
    await celeryClient.startClose(
      closeObject.id,
      closeRequest.organizerSignature,
    );
  }

  /**
   * Get a single Close by its ID
   *

   * @param closeId Unique identifier
   * @param profile
   * @param filter
   * @returns Expected response to a valid request
   */
  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @operation('get', '/closes/{closeId}', {
    operationId: 'getClose',
    summary: 'Get a Close by ID',
    description: 'Get Close by ID',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Close),
          },
        },
      },
    },
  })
  async readCloseById(
    @param({name: 'closeId', in: 'path'}) closeId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param.query.object('filter') filter?: Filter<Close>,
  ): Promise<Close> {
    return this.closeRepository.findById(closeId, filter);
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The close you are trying to find cannot be found.',
    //  );
    //}
  }

  /**
   *
   *

   * @param profile
   * @param closeId Unique identifier
   * @param close
   * @returns Expected response to a valid request
   */
  @authenticate('jwt')
  @operation('put', '/closes/{closeId}', {
    operationId: 'updateClose',
    summary: 'Update Close by ID',
    description: 'Update a close with the supplied information',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Close),
          },
        },
      },
    },
  })
  async updateCloseById(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'closeId', in: 'path'}) closeId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Close, {partial: true}),
        },
      },
    })
    close: Close,
  ): Promise<Close> {
    //if (!close.isDeleted && close.ownerId === profile.id) {
    close.updatedAt = new Date();
    await this.closeRepository.updateById(closeId, close);
    return this.closeRepository.findById(closeId);
    //} else {
    //  throw new HttpErrors.NotFound(
    //    'The close you are trying to update cannot be found.',
    //  );
    //}
  }

  /**
   *
   *

   * @param closeId Unique identifier
   * @param profile
   * @param tenant
   * @returns Expected response to a valid request
   */
  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('delete', '/closes/{closeId}', {
    operationId: 'deleteClose',
    summary: 'Delete a Close',
    description: 'Delete the Close matching the specified ID',
    responses: {
      '204': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async deleteCloseById(
    @repository(SubscriptionRepository)
    subscriptionRepository: SubscriptionRepository,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @param({name: 'closeId', in: 'path'}) closeId: string,
  ): Promise<void> {
    const close = await this.closeRepository.findById(closeId);

    if (close.subscriptions) {
      const subscriptions = await subscriptionRepository.find({
        where: {
          id: {inq: close.subscriptions},
        },
      });
      const updatedSubs = subscriptions.map(sub => {
        return subscriptionRepository.updateById(sub.id, {
          status: 'COMPLETED',
          closeDoc: undefined,
        });
      });
      await Promise.all(updatedSubs);
    }

    await this.closeRepository.deleteById(closeId);
  }

  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation(
    'post',
    '/closes/{closeId}/generate-closing-document/{documentType}',
  )
  async populateTemplate(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('closeId') dealId: string,
    @param.path.string('documentType') documentType: string,
    @requestBody() payloadData: any,
  ): Promise<any> {
    if (!['oa', 'sub'].includes(documentType)) {
      throw new HttpErrors.NotFound('documentType not supported');
    }

    const typeNames = {
      oa: 'Operating Agreement',
      sub: 'Subscription Agreement',
    };

    let fileName = '';
    if (documentType === 'oa') {
      fileName = 'manager/managerOATemplate.pdf';
    } else if (documentType === 'sub') {
      fileName = 'manager/managerSATemplate.pdf';
    }
    const deal = await this.dealRepository.findById(dealId);
    const contentDispositionName = `${deal.name} - ${typeNames[documentType]}.pdf`;

    //<deal name> - <file type>.pdf
    //merge files
    let fileId;
    if (payloadData.signature) {
      const sigFilename = path.join('/tmp', uuidv4()) + '.png';
      console.log(sigFilename);

      fs.writeFileSync(
        sigFilename,
        payloadData.signature.replace(/^data:image\/png;base64,/, ''),
        'base64',
      );

      fileId = await this.documentsService.oldPopulatePdfTemplate(
        fileName,
        payloadData,
        contentDispositionName,
        sigFilename,
      );
    } else {
      //merge files
      fileId = await this.documentsService.oldPopulatePdfTemplate(
        fileName,
        payloadData,
        contentDispositionName,
      );
    }
    const fileObj = new File({
      name: contentDispositionName,
      ownerId: profile.id,
      tenantId: tenant.id,
      type: 'pdf',
      key: fileId,
      lastModified: Date.now().toString(),
      //      thumbnail: `data:image/png;base64,${thumbnail}`,
    });
    const fileRef = await this.fileRepository.create(fileObj);
    return fileRef.id;
  }

  @authenticate('jwt')
  @operation('post', '/closes/{closeId}/fund-manager-sign', {
    operationId: 'fundManagerSign',
    summary: 'Sign a Close as the Fund Manager',
    description: 'Signs the Close documents as the Fund Manager',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Close),
          },
        },
      },
    },
  })
  async sign(
    @param.path.string('closeId') closeId: string,
    @service(CeleryClientService) celeryClient: CeleryClientService,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['fundManagerSignature'],
            properties: {
              fundManagerSignature: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    payloadData: any,
  ): Promise<Close> {
    const close = await this.closeRepository.findById(closeId);

    await celeryClient.fundManagerCountersign(
      close.id,
      payloadData.fundManagerSignature,
    );

    await this.closeRepository.updateById(close.id, {
      updatedAt: new Date(),
      fundManagerSignature: payloadData.fundManagerSignature,
    });

    return this.closeRepository.findById(closeId);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @get('/closes-to-file')
  @response(200, {
    description: 'Get a list of closes that need to be filed',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Close, {includeRelations: true}),
        },
      },
    },
  })
  async closesToFile(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
  ): Promise<Close[]> {
    const listFindStatement = {
      include: [
        {
          relation: 'formD',
          where: {filedDate: {neq: null}},
        },
        {
          relation: 'entity',
          scope: {
            fields: ['id', 'name', 'entityType'],
          },
        },
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name'],
          },
        },
      ],
    };
    return this.closeRepository.find(listFindStatement);
  }

  @authenticate('jwt')
  @authorize({allowedTenants: ['master']})
  @visibility(OperationVisibility.UNDOCUMENTED)
  @get('/closes-to-file/{closeId}')
  @response(200, {
    description: 'Get a close that needs to be filed',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Close),
      },
    },
  })
  async closeToFile(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @repository(SubscriptionRepository)
    subscriptionRepository: SubscriptionRepository,
    @param({name: 'closeId', in: 'path'}) closeId: string,
  ): Promise<any> {
    const listFindStatement = {
      include: [
        {
          relation: 'formD',
        },
        {
          relation: 'entity',
          scope: {
            fields: [
              'id',
              'name',
              'entityType',
              'minInvestmentAmount',
              'managerId',
              'masterEntityId',
            ],
            include: ['manager', 'masterEntity'],
          },
        },
        {
          relation: 'deal',
          scope: {
            fields: ['id', 'name'],
          },
        },
      ],
    };
    const close = await this.closeRepository.findById(
      closeId,
      listFindStatement,
    );
    const subscriptions = await subscriptionRepository.find({
      where: {
        id: {inq: close.subscriptions},
      },
    });

    return {...close, subscriptions: subscriptions};
  }

  @log(LOG_LEVEL.INFO)
  @authenticate('jwt')
  @visibility(OperationVisibility.UNDOCUMENTED)
  @operation('get', '/closes/{closeId}/fees-statement/pdf', {
    operationId: 'getCloseStatementPdf',
    summary: 'Get a Close Statement Pdf',
    description: 'Get Close Statement Pdf',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async getCloseStatementPdf(
    @param({name: 'closeId', in: 'path'}) closeId: string,
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenat: Tenant,
    @inject(RestBindings.Http.RESPONSE) res: Response,
  ) {
    //const tenant = await this.tenantRepository.findById('gbt');
    const close: any = await this.closeRepository.findById(closeId, {
      include: [{relation: 'entity'}, {relation: 'deal'}],
    });
    if (!close.statement) {
      throw new HttpErrors.FailedDependency('No statement found on close');
    }
    const statementPdf = await this.documentsGeneratorService.buildStatement(
      close,
    );

    const pdfBytes = await statementPdf.save();
    res.attachment(`${close.entity.name}.pdf`);
    res.contentType('application/pdf');
    res.send(Buffer.from(pdfBytes));
  }

  @log(LOG_LEVEL.INFO)
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @authorize({allowedRoles: ['admin', 'manage-closes']})
  @operation('post', '/closes/{closeId}/init-book-transfer', {
    operationId: 'postBookTransfer',
    summary: 'Send book transfer out',
    description:
      'This is used to send money out to another account with a book transfer',
    security: [{oAuth2: ['clientCredentials', 'authorizationCode']}],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
    },
  })
  async postbookTransfer(
    @inject(SecurityBindings.USER) profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT) tenant: Tenant,
    @param.path.string('closeId') closeId: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['amount', 'purpose'],
            properties: {
              amount: {
                type: 'number',
                minimum: 0.01,
              },
              purpose: {
                type: 'string',
                enum: ['FEES', 'BLUESKY'],
              },
            },
          },
        },
      },
    })
    request: {
      amount: number;
      purpose: string;
    },
  ): Promise<void | boolean> {
    const close: any = await this.closeRepository.findById(closeId, {
      include: [
        {
          relation: 'entity',
          scope: {
            include: [{relation: 'manager'}],
          },
        },
      ],
    });
    if (!close) {
      throw new HttpErrors.NotFound('Close not found');
    }

    const fromAccountId = close?.entity?.bankAccount?.accountNumber ?? '';
    if (!fromAccountId) {
      throw new HttpErrors.UnprocessableEntity(
        'No bank account found for the entity (fees source)',
      );
    }
    const toAccountId =
      close?.entity?.manager?.feesAccount?.accountNumber ?? '';
    if (!toAccountId) {
      throw new HttpErrors.UnprocessableEntity(
        'No bank account found for the manager (fees destination)',
      );
    }

    const {amount, purpose} = request;
    try {
      const transaction = await this.transactionService.createBookTransaction(
        close.entityId,
        toAccountId,
        amount,
        `For payment of ${purpose}`,
      );
      await this.closeRepository.updateById(close.id, {
        updatedAt: new Date(),
        ...(purpose === 'FEES' && {feesPaymentTransferId: transaction.id}),
        ...(purpose === 'BLUESKY' && {
          blueSkyPaymentTransferId: transaction.id,
        }),
      });

      return !!(transaction ?? false);
    } catch (err) {
      throw new HttpErrors.UnprocessableEntity(err.toString());
    }
  }

  /**
   * List current user's closes to which the authentication method has access.
   *
   * @returns Expected response to a valid request
   * @param profile
   * @param tenant
   */
  @visibility(OperationVisibility.UNDOCUMENTED)
  @authenticate('jwt')
  @operation('get', '/closed-deals', {
    operationId: 'getClosedDeals',
    summary: 'List of closed deals',
    description: 'List of deals that have been reg filed and BlueSky paid.',
    security: [
      {
        oAuth2: ['clientCredentials'],
      },
    ],
    responses: {
      '200': {
        description: 'Expected response to a valid request',
      },
      '400': {
        description: 'Invalid Request',
      },
      '401': {
        description: 'Unauthorized',
      },
      '403': {
        description: 'Forbidden',
      },
      default: {
        $ref: '#/components/responses/UnexpectedError',
      },
    },
  })
  async getClosedDeals(): Promise<{formD: FormD; close: Close}[]> {
    const result: {formD: FormD; close: Close}[] = [];

    const formDList = await this.formDRepository.find({
      where: {
        fileNumber: {exists: true},
        blueSkyReceipt: {exists: true},
      },
    });

    for (const formD of formDList) {
      const closeByEntity = await this.closeRepository.findOne({
        where: {
          entityId: formD.entitiesId,
        },
        include: [
          {
            relation: 'entity',
            scope: {
              fields: ['id', 'name', 'dealId'],
              include: [
                {
                  relation: 'deal',
                  scope: {
                    fields: ['id', 'name', 'status', 'profileId', 'updatedAt'],
                    include: [
                      {
                        relation: 'profile',
                        scope: {
                          fields: [
                            'id',
                            'name',
                            'firstName',
                            'lastName',
                            'displayName',
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      });

      if (closeByEntity) {
        result.push({
          formD: formD,
          close: closeByEntity,
        });
      }
    }

    return result;
  }
}
