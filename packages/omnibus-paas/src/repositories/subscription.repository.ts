import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  BelongsToAccessor,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {Subscription, Profile, Transaction, Deal, User} from '../models';
import {ProfileRepository} from './profile.repository';
import {TransactionRepository} from './transaction.repository';
import {DealRepository} from './deal.repository';
import {UserRepository} from './user.repository';

export class SubscriptionRepository extends DefaultCrudRepository<
  Subscription,
  typeof Subscription.prototype.id
> {
  public readonly profile: BelongsToAccessor<
    Profile,
    typeof Subscription.prototype.id
  >;

  public readonly transactions: HasManyRepositoryFactory<
    Transaction,
    typeof Subscription.prototype.id
  >;

  public readonly deal: BelongsToAccessor<
    Deal,
    typeof Subscription.prototype.id
  >;

  public readonly owner: BelongsToAccessor<
    User,
    typeof Subscription.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('ProfileRepository')
    protected profileRepositoryGetter: Getter<ProfileRepository>,
    @repository.getter('TransactionRepository')
    protected transactionRepositoryGetter: Getter<TransactionRepository>,
    @repository.getter('DealRepository')
    protected dealRepositoryGetter: Getter<DealRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Subscription, dataSource);
    this.owner = this.createBelongsToAccessorFor('owner', userRepositoryGetter);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);
    this.deal = this.createBelongsToAccessorFor('deal', dealRepositoryGetter);
    this.registerInclusionResolver('deal', this.deal.inclusionResolver);
    this.transactions = this.createHasManyRepositoryFactoryFor(
      'transactions',
      transactionRepositoryGetter,
    );

    this.profile = this.createBelongsToAccessorFor(
      'profile',
      profileRepositoryGetter,
    );

    this.registerInclusionResolver('profile', this.profile.inclusionResolver);
    this.registerInclusionResolver(
      'transactions',
      this.transactions.inclusionResolver,
    );
  }
}
