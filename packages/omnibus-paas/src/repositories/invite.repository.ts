import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Invite} from '../models';

export class InviteRepository extends DefaultCrudRepository<
  Invite,
  typeof Invite.prototype.id
> {
  constructor(@inject('datasources.mongodb') dataSource: juggler.DataSource) {
    super(Invite, dataSource);
  }
}
