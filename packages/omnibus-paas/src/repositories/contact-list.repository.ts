import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {ContactList, ContactListRelations} from '../models';

export class ContactListRepository extends DefaultCrudRepository<
  ContactList,
  typeof ContactList.prototype._id,
  ContactListRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(ContactList, dataSource);
  }
}
