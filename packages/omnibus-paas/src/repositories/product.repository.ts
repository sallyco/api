import {inject, Context, Constructor, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Product, ProductRelations, ProductCategory} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {ProductCategoryRepository} from './product-category.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'product_logs',
};

export class ProductRepository extends AuditRepositoryMixin<
  Product,
  typeof Product.prototype.id,
  ProductRelations,
  Constructor<
    DefaultCrudRepository<
      Product,
      typeof Product.prototype.id,
      ProductRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly productCategory: BelongsToAccessor<
    ProductCategory,
    typeof Product.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
    @repository.getter('ProductCategoryRepository')
    protected productCategoryRepositoryGetter: Getter<ProductCategoryRepository>,
  ) {
    super(Product, dataSource);
    this.productCategory = this.createBelongsToAccessorFor(
      'productCategory',
      productCategoryRepositoryGetter,
    );
    this.registerInclusionResolver(
      'productCategory',
      this.productCategory.inclusionResolver,
    );
  }
}
