import {Context, inject} from '@loopback/core';
import {DefaultCrudRepository, Options} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Notification, NotificationRelations, Tenant} from '../models';
import {MultiTenancyBindings} from '../multi-tenancy';

export class NotificationRepository extends DefaultCrudRepository<
  Notification,
  typeof Notification.prototype.id,
  NotificationRelations
> {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject.context() private ctx: Context,
  ) {
    super(Notification, dataSource);
  }

  async save(entity: Notification, options?: Options): Promise<Notification> {
    if (!entity.tenantId) {
      const tenant = await this.ctx.get<Tenant>(
        MultiTenancyBindings.CURRENT_TENANT,
      );
      entity.tenantId = tenant.id;
    }
    return super.save(entity, options);
  }
}
