import {repository} from '@loopback/repository';
import {inject, Getter, Constructor} from '@loopback/core';

import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Company, CompanyRelations} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'company_logs',
};

export class CompanyRepository extends AuditRepositoryMixin<
  Company,
  typeof Company.prototype.id,
  CompanyRelations,
  Constructor<
    DefaultCrudRepository<
      Company,
      typeof Company.prototype.id,
      CompanyRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
  ) {
    super(Company, dataSource);
  }
}
