import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  juggler,
  repository,
} from '@loopback/repository';
import {Asset, AssetRelations, Deal, Profile} from '../models';
import {DealRepository} from './deal.repository';
import {ProfileRepository} from './profile.repository';

export class AssetRepository extends DefaultCrudRepository<
  Asset,
  typeof Asset.prototype.id,
  AssetRelations
> {
  public readonly deal: BelongsToAccessor<Deal, typeof Asset.prototype.id>;

  public readonly founderProfile: BelongsToAccessor<
    Profile,
    typeof Asset.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('DealRepository')
    protected dealRepositoryGetter: Getter<DealRepository>,
    @repository.getter('ProfileRepository')
    protected profileRepositoryGetter: Getter<ProfileRepository>,
  ) {
    super(Asset, dataSource);
    this.deal = this.createBelongsToAccessorFor('deal', dealRepositoryGetter);
    this.founderProfile = this.createBelongsToAccessorFor(
      'founderProfile',
      profileRepositoryGetter,
    );
    this.registerInclusionResolver('deal', this.deal.inclusionResolver);
    this.registerInclusionResolver(
      'founderProfile',
      this.founderProfile.inclusionResolver,
    );
  }
}
