import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {FormD, FormDRelations} from '../models';

export class FormDRepository extends DefaultCrudRepository<
  FormD,
  typeof FormD.prototype.id,
  FormDRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(FormD, dataSource);
  }
}
