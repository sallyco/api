import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  BelongsToAccessor,
  HasOneRepositoryFactory,
} from '@loopback/repository';
import {UserProfile} from '@loopback/security';
import {Deal, Entities, Profile, Distribution, User, Package} from '../models';
import {Tenant} from '../multi-tenancy';
import {EntitiesRepository} from './entities.repository';
import {ProfileRepository} from './profile.repository';
import {DistributionRepository} from './distribution.repository';
import {UserRepository} from './user.repository';
import {PackageRepository} from './package.repository';

export class DealRepository extends DefaultCrudRepository<
  Deal,
  typeof Deal.prototype.id
> {
  public readonly profile: BelongsToAccessor<Profile, typeof Deal.prototype.id>;

  public readonly entity: BelongsToAccessor<Entities, typeof Deal.prototype.id>;

  public readonly distribution: HasOneRepositoryFactory<
    Distribution,
    typeof Deal.prototype.id
  >;

  public readonly owner: BelongsToAccessor<User, typeof Deal.prototype.id>;

  public readonly package: BelongsToAccessor<Package, typeof Deal.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('EntitiesRepository')
    protected entitiesRepositoryGetter: Getter<EntitiesRepository>,
    @repository.getter('ProfileRepository')
    protected profileRepositoryGetter: Getter<ProfileRepository>,
    @repository.getter('DistributionRepository')
    protected distributionRepositoryGetter: Getter<DistributionRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('PackageRepository')
    protected packageRepositoryGetter: Getter<PackageRepository>,
  ) {
    super(Deal, dataSource);
    this.package = this.createBelongsToAccessorFor(
      'package',
      packageRepositoryGetter,
    );
    this.registerInclusionResolver('package', this.package.inclusionResolver);
    this.owner = this.createBelongsToAccessorFor('owner', userRepositoryGetter);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);

    this.distribution = this.createHasOneRepositoryFactoryFor(
      'distribution',
      distributionRepositoryGetter,
    );
    this.registerInclusionResolver(
      'distribution',
      this.distribution.inclusionResolver,
    );
    this.entity = this.createBelongsToAccessorFor(
      'entity',
      entitiesRepositoryGetter,
    );
    this.registerInclusionResolver('entity', this.entity.inclusionResolver);
    this.profile = this.createBelongsToAccessorFor(
      'profile',
      profileRepositoryGetter,
    );
    this.registerInclusionResolver('profile', this.profile.inclusionResolver);
  }

  getDealCount(tenant: Tenant, profile: UserProfile) {
    let countAndStatement = {};
    if (tenant.id !== 'master') {
      if (profile?.roles?.account?.roles.includes('admin')) {
        countAndStatement = {
          and: [{isDeleted: false}, {tenantId: tenant.id}],
        };
      } else {
        countAndStatement = {
          and: [
            {isDeleted: false},
            {ownerId: profile.id},
            {tenantId: tenant.id},
          ],
        };
      }
    } else {
      countAndStatement = {and: [{isDeleted: false}]};
    }

    return this.count(countAndStatement);
  }

  getClosedDealCount(tenantId: string) {
    let countAndStatement = {};
    if (tenantId !== 'master') {
      countAndStatement = {
        and: [{isDeleted: false}, {tenantId: tenantId}, {status: 'CLOSED'}],
      };
    } else {
      countAndStatement = {and: [{isDeleted: false}, {status: 'CLOSED'}]};
    }
    return this.count(countAndStatement);
  }
}
