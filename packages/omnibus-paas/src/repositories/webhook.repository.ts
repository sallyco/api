import {Context, inject} from '@loopback/core';
import {
  DataObject,
  DefaultCrudRepository,
  juggler,
  Options,
} from '@loopback/repository';
import {Webhook} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

export class WebhookRepository extends DefaultCrudRepository<
  Webhook,
  typeof Webhook.prototype._id
> {
  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @inject.context() private ctx: Context,
  ) {
    super(Webhook, dataSource);
  }

  async create(
    entity: DataObject<Webhook>,
    options?: Options,
  ): Promise<Webhook> {
    if (!entity.tenantId) {
      const tenant = await this.ctx.get<Tenant>(
        MultiTenancyBindings.CURRENT_TENANT,
      );
      entity.tenantId = tenant.id;
    }
    return super.create(entity, options);
  }
}
