import {inject, Context, Constructor} from '@loopback/core';
import {DefaultCrudRepository, Getter, repository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {ProductCategory, ProductCategoryRelations} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'productCategory_logs',
};

export class ProductCategoryRepository extends AuditRepositoryMixin<
  ProductCategory,
  typeof ProductCategory.prototype.id,
  ProductCategoryRelations,
  Constructor<
    DefaultCrudRepository<
      ProductCategory,
      typeof ProductCategory.prototype.id,
      ProductCategoryRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(ProductCategory, dataSource);
  }
}
