import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {PackageProduct, PackageProductRelations} from '../models';

export class PackageProductRepository extends DefaultCrudRepository<
  PackageProduct,
  typeof PackageProduct.prototype.id,
  PackageProductRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(PackageProduct, dataSource);
  }
}
