import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';

import {AuditLog, User} from '../models';
import {UserRepository} from './user.repository';

export class AuditLogRepository extends DefaultCrudRepository<
  AuditLog,
  typeof AuditLog.prototype.id
> {
  public readonly actionActor: BelongsToAccessor<
    User,
    typeof AuditLog.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(AuditLog, dataSource);
    this.actionActor = this.createBelongsToAccessorFor(
      'actionActor',
      userRepositoryGetter,
    );
    this.registerInclusionResolver(
      'actionActor',
      this.actionActor.inclusionResolver,
    );
  }
}
