/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject} from '@loopback/core';
import {DefaultCrudRepository, Options} from '@loopback/repository';
import {FilterExcludingWhere} from '@loopback/filter';
import {KeycloakDataSource} from '../datasources';
import {User, UserRelations} from '../models';
import {HttpErrors} from '@loopback/rest';
import KcAdminClient from 'keycloak-admin';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  constructor(@inject('datasources.keycloak') dataSource: KeycloakDataSource) {
    super(User, dataSource);
  }

  async clientAuth() {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
    });

    return kcAdminClient;
  }

  async find(filter?: any, options?: Options): Promise<User[]> {
    const kcAdminClient = await this.clientAuth();
    const tenants = await kcAdminClient.realms.find();
    let results: any = [];
    const tenantId = !filter || !filter.tenantId ? 'master' : filter.tenantId;
    if (tenantId === 'master') {
      for (const t in tenants) {
        //if (tenants[t].realm === 'master') continue;
        const users = await kcAdminClient.users.find({
          realm: tenants[t].realm,
          max: 10000,
        });
        results = results.concat(
          users.map(v => ({...v, tenantId: tenants[t].realm})),
        );
      }
    } else {
      results = await kcAdminClient.users.find({realm: tenantId});
      if (results.length === 0) {
        throw new HttpErrors.NotFound('No users found');
      } else {
        return results;
      }
    }

    return results;
  }

  async findById(
    id: string,
    filter?: FilterExcludingWhere<User>,
    options?: Options,
  ): Promise<User> {
    const kcAdminClient = await this.clientAuth();
    const tenants = await kcAdminClient.realms.find();
    let user: any;

    for (const t in tenants) {
      user = await kcAdminClient.users.findOne({
        id: id,
        realm: tenants[t].realm,
      });
      if (user) return {...user, tenantId: tenants[t].realm};
    }
    throw new HttpErrors.NotFound('No user exists with specified id');
  }
}
