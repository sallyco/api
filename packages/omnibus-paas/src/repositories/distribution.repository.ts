/* eslint-disable @typescript-eslint/no-explicit-any */
import {Context, inject} from '@loopback/core';
import {Getter, repository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Distribution, DistributionRelations} from '../models';
import {IAuditMixinOptions} from '../types';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {AuditLogRepository} from './audit-log.repository';
import {AuditedDefaultCrudRepository} from '../mixins/mixin-utils';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'distribution_logs',
};

export class DistributionRepository extends AuditedDefaultCrudRepository<DistributionRelations>(
  Distribution,
  auditOpts,
) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(Distribution, dataSource);
  }
}
