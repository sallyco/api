import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Account} from '../models';

export class AccountRepository extends DefaultCrudRepository<
  Account,
  typeof Account.prototype.id
> {
  constructor(@inject('datasources.mongodb') dataSource: juggler.DataSource) {
    super(Account, dataSource);
  }
}
