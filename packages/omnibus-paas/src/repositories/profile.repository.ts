import {inject, Getter, Constructor} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  BelongsToAccessor,
  DataObject,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {
  Profile,
  Company,
  User,
  Kyc,
  ProfileRelations,
  Accreditation,
  Package,
} from '../models';
import {CompanyRepository} from './company.repository';
import {UserRepository} from './user.repository';
import {KycRepository} from './kyc.repository';
import legacy from 'loopback-datasource-juggler';
import {IAuditMixinOptions} from '../types';
import {AuditRepositoryMixin} from '../mixins';
import _ from 'lodash';
import {AccreditationRepository} from './accreditation.repository';
import {PackageRepository} from './package.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'profile_logs',
};

export class ProfileRepository extends AuditRepositoryMixin<
  Profile,
  typeof Profile.prototype.id,
  ProfileRelations,
  Constructor<DefaultCrudRepository<Profile, typeof Profile.prototype.id>>
>(DefaultCrudRepository, auditOpts) {
  public readonly manager: BelongsToAccessor<
    Company,
    typeof Profile.prototype.id
  >;

  public readonly masterEntity: BelongsToAccessor<
    Company,
    typeof Profile.prototype.id
  >;

  public readonly owner: BelongsToAccessor<User, typeof Profile.prototype.id>;

  public readonly kyc: HasManyRepositoryFactory<
    Kyc,
    typeof Profile.prototype.id
  >;

  public readonly accreditations: HasManyRepositoryFactory<
    Accreditation,
    typeof Profile.prototype.id
  >;

  public readonly package: BelongsToAccessor<
    Package,
    typeof Profile.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('KycRepository')
    protected kycRepositoryGetter: Getter<KycRepository>,
    @repository.getter('AccreditationRepository')
    protected accreditationRepositoryGetter: Getter<AccreditationRepository>,
    @repository.getter('PackageRepository')
    protected packageRepositoryGetter: Getter<PackageRepository>,
  ) {
    super(Profile, dataSource);
    this.package = this.createBelongsToAccessorFor(
      'package',
      packageRepositoryGetter,
    );
    this.registerInclusionResolver('package', this.package.inclusionResolver);
    this.accreditations = this.createHasManyRepositoryFactoryFor(
      'accreditations',
      accreditationRepositoryGetter,
    );
    this.registerInclusionResolver(
      'accreditations',
      this.accreditations.inclusionResolver,
    );
    this.owner = this.createBelongsToAccessorFor('owner', userRepositoryGetter);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);

    this.masterEntity = this.createBelongsToAccessorFor(
      'masterEntity',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver(
      'masterEntity',
      this.masterEntity.inclusionResolver,
    );
    this.manager = this.createBelongsToAccessorFor(
      'manager',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver('manager', this.manager.inclusionResolver);

    this.kyc = this.createHasManyRepositoryFactoryFor(
      'kycAml',
      kycRepositoryGetter,
    );

    this.registerInclusionResolver(
      'kycAml',
      async (entities, filter, options) => {
        for (const entity of entities) {
          if (
            entity?.kycAml &&
            typeof entity.kycAml === 'object' &&
            entity.kycAml?.status !== 'EXCEPTION'
          ) {
            try {
              delete entity.kycAml;
            } catch (e) {
              // silently fail for now
            }
          }
        }
        const data = await this.kyc.inclusionResolver(
          entities,
          filter,
          options,
        );
        data.forEach((datum, index) => {
          if (Array.isArray(datum) && datum?.length > 0) {
            data[index] = _.orderBy(datum, a => new Date(a.updatedAt), [
              'desc',
            ])[0];
          }
        });
        return data;
      },
    );
  }

  protected entityToData<R extends Profile>(
    entity: DataObject<R> | R,
    options?: {},
  ): Promise<legacy.ModelData<legacy.PersistedModel>> {
    // Prevent issues with saving data to Profile model as a navigational property
    delete entity.kycAml;
    return super.entityToData(entity, options);
  }
}
