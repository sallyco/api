import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {AccountingDataSource} from '../datasources';
import {BlueSkyFee, BlueSkyFeeRelations, BlueSkyFeeTier} from '../models';
import {BlueSkyFeeTierRepository} from './blue-sky-fee-tier.repository';

export class BlueSkyFeeRepository extends DefaultCrudRepository<
  BlueSkyFee,
  typeof BlueSkyFee.prototype.id,
  BlueSkyFeeRelations
> {
  public readonly blueSkyFeeTiers: HasManyRepositoryFactory<
    BlueSkyFeeTier,
    typeof BlueSkyFee.prototype.id
  >;

  constructor(
    @inject('datasources.accounting') dataSource: AccountingDataSource,
    @repository.getter('BlueSkyFeeTierRepository')
    protected blueSkyFeeTierRepositoryGetter: Getter<BlueSkyFeeTierRepository>,
  ) {
    super(BlueSkyFee, dataSource);
    this.blueSkyFeeTiers = this.createHasManyRepositoryFactoryFor(
      'blueSkyFeeTiers',
      blueSkyFeeTierRepositoryGetter,
    );
    this.registerInclusionResolver(
      'blueSkyFeeTiers',
      this.blueSkyFeeTiers.inclusionResolver,
    );
  }
}
