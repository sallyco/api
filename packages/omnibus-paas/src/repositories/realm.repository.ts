/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject} from '@loopback/core';
import {DefaultCrudRepository, Options} from '@loopback/repository';
import {FilterExcludingWhere} from '@loopback/filter';
import {KeycloakDataSource} from '../datasources';
import {Realm, RealmRelations} from '../models';
import {HttpErrors} from '@loopback/rest';
import KcAdminClient from 'keycloak-admin';

export class RealmRepository extends DefaultCrudRepository<
  Realm,
  typeof Realm.prototype.id,
  RealmRelations
> {
  constructor(@inject('datasources.keycloak') dataSource: KeycloakDataSource) {
    super(Realm, dataSource);
  }

  async clientAuth() {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
    });

    return kcAdminClient;
  }

  async find(filter?: any, options?: Options): Promise<Realm[]> {
    const kcAdminClient = await this.clientAuth();
    const realms: any = await kcAdminClient.realms.find();

    return realms;
  }

  async findById(
    id: string,
    filter?: FilterExcludingWhere<Realm>,
    options?: Options,
  ): Promise<Realm> {
    const kcAdminClient = await this.clientAuth();
    const realm: any = await kcAdminClient.realms.findOne({
      realm: id,
    });
    if (realm) return realm;
    throw new HttpErrors.NotFound('No realm exists with specified id');
  }
}
