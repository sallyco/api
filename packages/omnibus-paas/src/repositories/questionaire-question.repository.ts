import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {QuestionaireQuestion, QuestionaireQuestionRelations} from '../models';

export class QuestionaireQuestionRepository extends DefaultCrudRepository<
  QuestionaireQuestion,
  typeof QuestionaireQuestion.prototype.id,
  QuestionaireQuestionRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(QuestionaireQuestion, dataSource);
  }
}
