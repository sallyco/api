import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {
  PackagePackageAttribute,
  PackagePackageAttributeRelations,
} from '../models';

export class PackagePackageAttributeRepository extends DefaultCrudRepository<
  PackagePackageAttribute,
  typeof PackagePackageAttribute.prototype.id,
  PackagePackageAttributeRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(PackagePackageAttribute, dataSource);
  }
}
