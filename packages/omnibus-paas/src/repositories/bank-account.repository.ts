import {inject, Constructor, Context} from '@loopback/core';
import {
  DefaultCrudRepository,
  Getter,
  Options,
  repository,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {BankAccount, BankAccountRelations} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {AuditLogRepository} from './audit-log.repository';
const auditOpts: IAuditMixinOptions = {
  actionKey: 'bank_account_logs',
};

export class BankAccountRepository extends AuditRepositoryMixin<
  BankAccount,
  typeof BankAccount.prototype.id,
  BankAccountRelations,
  Constructor<
    DefaultCrudRepository<
      BankAccount,
      typeof BankAccount.prototype.id,
      BankAccountRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(BankAccount, dataSource);
  }

  save = async (
    entity: BankAccount,
    options?: Options,
  ): Promise<BankAccount> => {
    if (!entity.tenantId) {
      const tenant = await this.ctx.get<Tenant>(
        MultiTenancyBindings.CURRENT_TENANT,
      );
      entity.tenantId = tenant.id;
    }
    return super.save(entity, options);
  };
}
