import {Constructor, Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, repository} from '@loopback/repository';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {MongodbDataSource} from '../datasources';
import {BankingBeneficiary, BankingBeneficiaryRelations} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'bankingbeneficiary_logs',
};

export class BankingBeneficiaryRepository extends AuditRepositoryMixin<
  BankingBeneficiary,
  typeof BankingBeneficiary.prototype.id,
  BankingBeneficiaryRelations,
  Constructor<
    DefaultCrudRepository<
      BankingBeneficiary,
      typeof BankingBeneficiary.prototype.id,
      BankingBeneficiaryRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
  ) {
    super(BankingBeneficiary, dataSource);
  }
}
