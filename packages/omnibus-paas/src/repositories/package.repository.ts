import {inject, Context, Constructor, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  HasManyThroughRepositoryFactory,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {
  Package,
  PackageRelations,
  Product,
  PackageProduct,
  PackageAttribute,
  PackagePackageAttribute,
} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {PackageProductRepository} from './package-product.repository';
import {ProductRepository} from './product.repository';
import {PackagePackageAttributeRepository} from './package-package-attribute.repository';
import {PackageAttributeRepository} from './package-attribute.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'package_logs',
};

export class PackageRepository extends AuditRepositoryMixin<
  Package,
  typeof Package.prototype.id,
  PackageRelations,
  Constructor<
    DefaultCrudRepository<
      Package,
      typeof Package.prototype.id,
      PackageRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly products: HasManyThroughRepositoryFactory<
    Product,
    typeof Product.prototype.id,
    PackageProduct,
    typeof Package.prototype.id
  >;

  public readonly attributes: HasManyThroughRepositoryFactory<
    PackageAttribute,
    typeof PackageAttribute.prototype.id,
    PackagePackageAttribute,
    typeof Package.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
    @repository.getter('PackageProductRepository')
    protected packageProductRepositoryGetter: Getter<PackageProductRepository>,
    @repository.getter('ProductRepository')
    protected productRepositoryGetter: Getter<ProductRepository>,
    @repository.getter('PackagePackageAttributeRepository')
    protected packagePackageAttributeRepositoryGetter: Getter<PackagePackageAttributeRepository>,
    @repository.getter('PackageAttributeRepository')
    protected packageAttributeRepositoryGetter: Getter<PackageAttributeRepository>,
  ) {
    super(Package, dataSource);
    this.attributes = this.createHasManyThroughRepositoryFactoryFor(
      'attributes',
      packageAttributeRepositoryGetter,
      packagePackageAttributeRepositoryGetter,
    );
    this.registerInclusionResolver(
      'attributes',
      this.attributes.inclusionResolver,
    );
    this.products = this.createHasManyThroughRepositoryFactoryFor(
      'products',
      productRepositoryGetter,
      packageProductRepositoryGetter,
    );
    this.registerInclusionResolver('products', this.products.inclusionResolver);
  }
}
