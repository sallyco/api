import {repository, BelongsToAccessor} from '@loopback/repository';
import {inject, Getter, Constructor, Context} from '@loopback/core';

import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';

import {DefaultCrudRepository, juggler, Options} from '@loopback/repository';
import {
  Transaction,
  TransactionRelations,
  Subscription,
  Entities,
  Deal,
  Close,
} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {SubscriptionRepository} from './subscription.repository';
import {EntitiesRepository} from './entities.repository';
import {DealRepository} from './deal.repository';
import {CloseRepository} from './close.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'transaction_logs',
};

export class TransactionRepository extends AuditRepositoryMixin<
  Transaction,
  typeof Transaction.prototype.id,
  TransactionRelations,
  Constructor<
    DefaultCrudRepository<
      Transaction,
      typeof Transaction.prototype.id,
      TransactionRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context() private ctx: Context,
    @repository.getter('SubscriptionRepository')
    protected subscriptionRepositoryGetter: Getter<SubscriptionRepository>,
    @repository.getter('EntitiesRepository')
    protected entitiesRepositoryGetter: Getter<EntitiesRepository>,
    @repository.getter('DealRepository')
    protected dealRepositoryGetter: Getter<DealRepository>,
    @repository.getter('CloseRepository')
    protected closeRepositoryGetter: Getter<CloseRepository>,
  ) {
    super(Transaction, dataSource);
    this.close = this.createBelongsToAccessorFor(
      'close',
      closeRepositoryGetter,
    );
    this.registerInclusionResolver('close', this.close.inclusionResolver);
    this.entities = this.createBelongsToAccessorFor(
      'entities',
      entitiesRepositoryGetter,
    );
    this.registerInclusionResolver('entities', this.entities.inclusionResolver);
    this.subscription = this.createBelongsToAccessorFor(
      'subscription',
      subscriptionRepositoryGetter,
    );
    this.deal = this.createBelongsToAccessorFor('deal', dealRepositoryGetter);
    this.registerInclusionResolver(
      'subscription',
      this.subscription.inclusionResolver,
    );
    this.registerInclusionResolver('deal', this.deal.inclusionResolver);
  }

  public readonly subscription: BelongsToAccessor<
    Subscription,
    typeof Transaction.prototype.id
  >;

  public readonly entities: BelongsToAccessor<
    Entities,
    typeof Transaction.prototype.id
  >;

  public readonly deal: BelongsToAccessor<
    Deal,
    typeof Transaction.prototype.id
  >;

  public readonly close: BelongsToAccessor<
    Close,
    typeof Transaction.prototype.id
  >;
  save = async (
    entity: Transaction,
    options?: Options,
  ): Promise<Transaction> => {
    if (!entity.tenantId) {
      const tenant = await this.ctx.get<Tenant>(
        MultiTenancyBindings.CURRENT_TENANT,
      );
      entity.tenantId = tenant.id;
    }
    return super.save(entity, options);
  };
}
