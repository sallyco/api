import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {EmailTemplate} from '../models';

export class EmailTemplateRepository extends DefaultCrudRepository<
  EmailTemplate,
  typeof EmailTemplate.prototype.id
> {
  constructor(@inject('datasources.mongodb') dataSource: juggler.DataSource) {
    super(EmailTemplate, dataSource);
  }
}
