import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  juggler,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';
import {Close, Entities, Deal} from '../models';
import {EntitiesRepository} from './entities.repository';
import {DealRepository} from './deal.repository';

export class CloseRepository extends DefaultCrudRepository<
  Close,
  typeof Close.prototype.id
> {
  public readonly entity: BelongsToAccessor<
    Entities,
    typeof Close.prototype.id
  >;

  public readonly deal: BelongsToAccessor<Deal, typeof Close.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('EntitiesRepository')
    protected entitiesRepositoryGetter: Getter<EntitiesRepository>,
    @repository.getter('DealRepository')
    protected dealRepositoryGetter: Getter<DealRepository>,
  ) {
    super(Close, dataSource);
    this.deal = this.createBelongsToAccessorFor('deal', dealRepositoryGetter);
    this.registerInclusionResolver('deal', this.deal.inclusionResolver);
    this.entity = this.createBelongsToAccessorFor(
      'entity',
      entitiesRepositoryGetter,
    );
    this.registerInclusionResolver('entity', this.entity.inclusionResolver);
  }
}
