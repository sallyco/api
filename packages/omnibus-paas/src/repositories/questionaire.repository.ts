import {inject, Context, Constructor, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  HasManyThroughRepositoryFactory,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {
  Questionaire,
  QuestionaireRelations,
  Question,
  QuestionaireQuestion,
} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {QuestionaireQuestionRepository} from './questionaire-question.repository';
import {QuestionRepository} from './question.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'questionaire_logs',
};

export class QuestionaireRepository extends AuditRepositoryMixin<
  Questionaire,
  typeof Questionaire.prototype.id,
  QuestionaireRelations,
  Constructor<
    DefaultCrudRepository<
      Questionaire,
      typeof Questionaire.prototype.id,
      QuestionaireRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly questions: HasManyThroughRepositoryFactory<
    Question,
    typeof Question.prototype.id,
    QuestionaireQuestion,
    typeof Questionaire.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
    @repository.getter('QuestionaireQuestionRepository')
    protected questionaireQuestionRepositoryGetter: Getter<QuestionaireQuestionRepository>,
    @repository.getter('QuestionRepository')
    protected questionRepositoryGetter: Getter<QuestionRepository>,
  ) {
    super(Questionaire, dataSource);
  }
}
