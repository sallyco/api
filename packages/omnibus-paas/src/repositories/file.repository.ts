import {inject} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {File} from '../models';

export class FileRepository extends DefaultCrudRepository<
  File,
  typeof File.prototype.id
> {
  constructor(@inject('datasources.mongodb') dataSource: juggler.DataSource) {
    super(File, dataSource);
  }
}
