import {inject, Getter, Constructor, Context} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Accreditation, AccreditationRelations, Profile} from '../models';
import {ProfileRepository} from './profile.repository';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'accreditation_logs',
};

export class AccreditationRepository extends AuditRepositoryMixin<
  Accreditation,
  typeof Accreditation.prototype.id,
  AccreditationRelations,
  Constructor<
    DefaultCrudRepository<
      Accreditation,
      typeof Accreditation.prototype.id,
      AccreditationRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly profileModel: BelongsToAccessor<
    Profile,
    typeof Accreditation.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context() private ctx: Context,
    @repository.getter('ProfileRepository')
    protected profileRepositoryGetter: Getter<ProfileRepository>,
  ) {
    super(Accreditation, dataSource);
    this.profileModel = this.createBelongsToAccessorFor(
      'profile',
      profileRepositoryGetter,
    );
    this.registerInclusionResolver(
      'profile',
      this.profileModel.inclusionResolver,
    );
  }
}
