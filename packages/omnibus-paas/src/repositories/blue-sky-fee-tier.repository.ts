import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
} from '@loopback/repository';
import {AccountingDataSource} from '../datasources';
import {BlueSkyFeeTier, BlueSkyFeeTierRelations, BlueSkyFee} from '../models';
import {BlueSkyFeeRepository} from './blue-sky-fee.repository';

export class BlueSkyFeeTierRepository extends DefaultCrudRepository<
  BlueSkyFeeTier,
  typeof BlueSkyFeeTier.prototype.id,
  BlueSkyFeeTierRelations
> {
  public readonly blueSkyFee: BelongsToAccessor<
    BlueSkyFee,
    typeof BlueSkyFeeTier.prototype.id
  >;

  constructor(
    @inject('datasources.accounting') dataSource: AccountingDataSource,
    @repository.getter('BlueSkyFeeRepository')
    protected blueSkyFeeRepositoryGetter: Getter<BlueSkyFeeRepository>,
  ) {
    super(BlueSkyFeeTier, dataSource);
    this.blueSkyFee = this.createBelongsToAccessorFor(
      'blueSkyFee',
      blueSkyFeeRepositoryGetter,
    );
    this.registerInclusionResolver(
      'blueSkyFee',
      this.blueSkyFee.inclusionResolver,
    );
  }
}
