import {Context, inject, Constructor} from '@loopback/core';
import {
  DefaultCrudRepository,
  FilterBuilder,
  Options,
  WhereBuilder,
  repository,
  Getter,
} from '@loopback/repository';
import {Filter} from '@loopback/filter/dist/query';
import {MongodbDataSource} from '../datasources';
import {FormationRequest, FormationRequestRelations} from '../models';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {IAuditMixinOptions} from '../types';
import {AuditRepositoryMixin} from '../mixins';
import {AuditLogRepository} from './audit-log.repository';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {WebhookService} from '../services';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'formation_request_logs',
};

export class FormationRequestRepository extends AuditRepositoryMixin<
  FormationRequest,
  typeof FormationRequest.prototype.id,
  FormationRequestRelations,
  Constructor<
    DefaultCrudRepository<
      FormationRequest,
      typeof FormationRequest.prototype.id,
      FormationRequestRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(FormationRequest, dataSource);
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  protected normalizeFilter(
    filter?: Filter<FormationRequest>,
  ): Filter | undefined {
    if (this.tenant && this.tenant.id !== 'master') {
      if (!filter) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        filter = new FilterBuilder<FormationRequest>();
      }
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (!filter.where) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        filter.where = new WhereBuilder().build();
      }
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      filter.where = new WhereBuilder<FormationRequest>(filter?.where).impose({
        tenantId: this.tenant.id,
      });
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return super.normalizeFilter(filter);
  }

  save = async (
    entity: FormationRequest,
    options?: Options,
  ): Promise<FormationRequest> => {
    if (!entity.tenantId) {
      const tenant = await this.ctx.get<Tenant>(
        MultiTenancyBindings.CURRENT_TENANT,
      );
      entity.tenantId = tenant.id;
    }
    return super.save(entity, options);
  };
}
