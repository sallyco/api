import {repository} from '@loopback/repository';
import {inject, Getter, Constructor} from '@loopback/core';
import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Global} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'global_logs',
};

export class GlobalRepository extends AuditRepositoryMixin<
  Global,
  typeof Global.prototype.id,
  {},
  Constructor<DefaultCrudRepository<Global, typeof Global.prototype.id>>
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
  ) {
    super(Global, dataSource);
  }
}
