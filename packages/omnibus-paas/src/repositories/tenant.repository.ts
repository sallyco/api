import {
  repository,
  BelongsToAccessor,
  HasOneRepositoryFactory,
} from '@loopback/repository';
import {inject, Getter, Constructor} from '@loopback/core';

import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant as Ten} from '../multi-tenancy';

import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {
  Tenant,
  TenantRelations,
  Company,
  WhiteLabel,
  Realm,
  BankAccount,
  Package,
} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {CompanyRepository} from './company.repository';
import {WhiteLabelRepository} from './white-label.repository';
import {RealmRepository} from './realm.repository';
import {BankAccountRepository} from './bank-account.repository';
import {PackageRepository} from './package.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'tenant_logs',
};

export class TenantRepository extends AuditRepositoryMixin<
  Tenant,
  typeof Tenant.prototype.id,
  TenantRelations,
  Constructor<
    DefaultCrudRepository<Tenant, typeof Tenant.prototype.id, TenantRelations>
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly manager: BelongsToAccessor<
    Company,
    typeof Tenant.prototype.id
  >;

  public readonly masterEntity: BelongsToAccessor<
    Company,
    typeof Tenant.prototype.id
  >;

  public readonly whiteLabel: HasOneRepositoryFactory<
    WhiteLabel,
    typeof Tenant.prototype.id
  >;

  public readonly realm: BelongsToAccessor<Realm, typeof Tenant.prototype.id>;

  public readonly feesPaymentsAccount: HasOneRepositoryFactory<
    BankAccount,
    typeof Tenant.prototype.id
  >;

  public readonly blueSkyPaymentsAccount: HasOneRepositoryFactory<
    BankAccount,
    typeof Tenant.prototype.id
  >;

  public readonly package: BelongsToAccessor<
    Package,
    typeof Tenant.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Ten,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,
    @repository.getter('WhiteLabelRepository')
    protected whiteLabelRepositoryGetter: Getter<WhiteLabelRepository>,
    @repository.getter('RealmRepository')
    protected realmRepositoryGetter: Getter<RealmRepository>,
    @repository.getter('BankAccountRepository')
    protected bankAccountRepositoryGetter: Getter<BankAccountRepository>,
    @repository.getter('PackageRepository')
    protected packageRepositoryGetter: Getter<PackageRepository>,
  ) {
    super(Tenant, dataSource);
    this.package = this.createBelongsToAccessorFor(
      'package',
      packageRepositoryGetter,
    );
    this.registerInclusionResolver('package', this.package.inclusionResolver);
    this.blueSkyPaymentsAccount = this.createHasOneRepositoryFactoryFor(
      'blueSkyPaymentsAccount',
      bankAccountRepositoryGetter,
    );
    this.registerInclusionResolver(
      'blueSkyPaymentsAccount',
      this.blueSkyPaymentsAccount.inclusionResolver,
    );
    this.feesPaymentsAccount = this.createHasOneRepositoryFactoryFor(
      'feesPaymentsAccount',
      bankAccountRepositoryGetter,
    );
    this.registerInclusionResolver(
      'feesPaymentsAccount',
      this.feesPaymentsAccount.inclusionResolver,
    );
    this.realm = this.createBelongsToAccessorFor(
      'realm',
      realmRepositoryGetter,
    );
    this.registerInclusionResolver('realm', this.realm.inclusionResolver);
    this.whiteLabel = this.createHasOneRepositoryFactoryFor(
      'whiteLabel',
      whiteLabelRepositoryGetter,
    );
    this.registerInclusionResolver(
      'whiteLabel',
      this.whiteLabel.inclusionResolver,
    );
    this.masterEntity = this.createBelongsToAccessorFor(
      'masterEntity',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver(
      'masterEntity',
      this.masterEntity.inclusionResolver,
    );
    this.manager = this.createBelongsToAccessorFor(
      'manager',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver('manager', this.manager.inclusionResolver);
  }
}
