import {inject, Getter, Constructor} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  juggler,
  repository,
  HasOneRepositoryFactory,
} from '@loopback/repository';
import {
  Deal,
  Entities,
  EntitiesRelations,
  Company,
  FormD,
  User,
} from '../models';
import {DealRepository} from './deal.repository';
import {CompanyRepository} from './company.repository';
import {FormDRepository} from './form-d.repository';
import {UserRepository} from './user.repository';
import {AuditRepositoryMixin} from '../mixins';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {SecurityBindings, UserProfile} from '@loopback/security';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'entity_logs',
};

export class EntitiesRepository extends AuditRepositoryMixin<
  Entities,
  typeof Entities.prototype.id,
  EntitiesRelations,
  Constructor<
    DefaultCrudRepository<
      Entities,
      typeof Entities.prototype.id,
      EntitiesRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  public readonly deal: BelongsToAccessor<Deal, typeof Entities.prototype.id>;

  public readonly manager: BelongsToAccessor<
    Company,
    typeof Entities.prototype.id
  >;

  public readonly masterEntity: BelongsToAccessor<
    Company,
    typeof Entities.prototype.id
  >;

  public readonly formD: HasOneRepositoryFactory<
    FormD,
    typeof Entities.prototype.id
  >;

  public readonly owner: BelongsToAccessor<User, typeof Entities.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: juggler.DataSource,
    @repository.getter(DealRepository)
    dealRepositoryGetter: Getter<DealRepository>,
    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,
    @repository.getter('FormDRepository')
    protected formDRepositoryGetter: Getter<FormDRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
  ) {
    super(Entities, dataSource);
    this.owner = this.createBelongsToAccessorFor('owner', userRepositoryGetter);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);
    this.formD = this.createHasOneRepositoryFactoryFor(
      'formD',
      formDRepositoryGetter,
    );
    this.registerInclusionResolver('formD', this.formD.inclusionResolver);
    this.masterEntity = this.createBelongsToAccessorFor(
      'masterEntity',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver(
      'masterEntity',
      this.masterEntity.inclusionResolver,
    );
    this.manager = this.createBelongsToAccessorFor(
      'manager',
      companyRepositoryGetter,
    );
    this.registerInclusionResolver('manager', this.manager.inclusionResolver);
    this.deal = this.createBelongsToAccessorFor('deal', dealRepositoryGetter);
    this.registerInclusionResolver('deal', this.deal.inclusionResolver);
  }
}
