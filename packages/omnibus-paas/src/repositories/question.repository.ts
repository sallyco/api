import {inject, Context, Constructor} from '@loopback/core';
import {DefaultCrudRepository, Getter, repository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Question, QuestionRelations} from '../models';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {IAuditMixinOptions} from '../types';
import {AuditLogRepository} from './audit-log.repository';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'question_logs',
};

export class QuestionRepository extends AuditRepositoryMixin<
  Question,
  typeof Question.prototype.id,
  QuestionRelations,
  Constructor<
    DefaultCrudRepository<
      Question,
      typeof Question.prototype.id,
      QuestionRelations
    >
  >
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(Question, dataSource);
  }
}
