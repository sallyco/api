import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {WhiteLabel, WhiteLabelRelations} from '../models';

export class WhiteLabelRepository extends DefaultCrudRepository<
  WhiteLabel,
  typeof WhiteLabel.prototype.id,
  WhiteLabelRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(WhiteLabel, dataSource);
  }
}
