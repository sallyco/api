import {Constructor, Context, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  //  FilterBuilder,
  Getter,
  repository,
} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Kyc, KycRelations} from '../models';
import {IAuditMixinOptions} from '../types';
import {AuditRepositoryMixin} from '../mixins';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {MultiTenancyBindings, Tenant} from '../multi-tenancy';
import {WebhookService} from '../services';
import {AuditLogRepository} from './audit-log.repository';
import {Filter} from '@loopback/filter';
import legacy from 'loopback-datasource-juggler';

const auditOpts: IAuditMixinOptions = {
  actionKey: 'kyc_logs',
};

export class KycRepository extends AuditRepositoryMixin<
  Kyc,
  typeof Kyc.prototype.id,
  KycRelations,
  Constructor<DefaultCrudRepository<Kyc, typeof Kyc.prototype.id, KycRelations>>
>(DefaultCrudRepository, auditOpts) {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @inject(SecurityBindings.USER, {optional: true})
    public profile: UserProfile,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    public tenant: Tenant,
    @inject.getter('services.WebhookService')
    public getWebhookService: Getter<WebhookService>,
    @repository.getter('AuditLogRepository')
    public getAuditLogRepository: Getter<AuditLogRepository>,
    @inject.context()
    private readonly ctx: Context,
  ) {
    super(Kyc, dataSource);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  normalizeFilter(filter?: Filter<any>): legacy.Filter | undefined {
    // if (this.tenant && this.tenant.id !== 'master') {
    //   const filterBuilder = new FilterBuilder(filter);
    //   filterBuilder.fields({
    //     ...filterBuilder.build().fields,
    //     providerMeta: false,
    //   });
    //   filter = filterBuilder.build();
    // }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return super.normalizeFilter(filter) as legacy.Filter;
  }
}
