import {RequestHandler} from 'express-serve-static-core';
export type FileUploadHandler = RequestHandler;

import {AuditLogRepository} from './repositories';
import {UserProfile} from '@loopback/security';
import {Tenant} from './multi-tenancy';
import {WebhookService} from './services';
import {Getter} from '@loopback/core';

export const AuditDbSourceName = 'AuditDB';

export interface IAuditMixin {
  getAuditLogRepository: () => Promise<AuditLogRepository>;
  profile?: UserProfile;
  tenant?: Tenant;
  getWebhookService?: Getter<WebhookService>;
}

export interface ITenantFilterMixin {
  tenant?: Tenant;
}

export interface IAuditMixinOptions {
  actionKey: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}
