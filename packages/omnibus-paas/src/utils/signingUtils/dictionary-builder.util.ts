/* eslint-disable @typescript-eslint/no-explicit-any */
import {Profile} from 'models';
import moment from 'moment';

const formatMoney = amount => {
  return amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

const shouldUseW9 = profile => {
  const isIndividual = profile.taxDetails.registrationType === 'INDIVIDUAL';

  const isForeign = (() => {
    if (isIndividual) {
      return profile.taxDetails.taxIdentification?.type !== 'ssn';
    }
    return (
      profile?.taxDetails?.taxIdentification?.type === 'ein' &&
      profile?.typeOfEntity === 'FOREIGN_ENTITY'
    );
  })();

  if (isIndividual && isForeign) {
    return false;
  }
  return true;
};

export function w9Form(prof: Profile, signature: string | null = null) {
  const type = prof.taxDetails!.registrationType;
  const signIndividualPage =
    prof.taxDetails!.registrationType === 'INDIVIDUAL' ||
    prof.taxDetails!.registrationType === 'JOINT';

  const payload: any = {
    taxName1: signIndividualPage
      ? `${prof.firstName} ${prof.lastName}`
      : `${prof.primarySignatory!.name ?? ''}`,
    signedDate: moment().format('MMM Do YYYY'),
    taxBusinessName: prof.name ?? ' ',
    streetAddress1: `${prof.address!.address1} ${prof.address!.address2 ?? ''}`,
    cityStateZIP1: `${prof.address!.city}, ${prof.address!.state} ${
      prof.address!.postalCode
    }`,
    llcClassification:
      prof.typeOfEntity === 'LIMITED_LIABILITY_COMPANY' &&
      !prof.isSingleMemberLLC
        ? 'P'
        : '',
    taxLLC:
      prof.typeOfEntity === 'LIMITED_LIABILITY_COMPANY' &&
      !prof.isSingleMemberLLC
        ? 'Yes'
        : 'Off',
    taxTrust: type === 'TRUST' ? 'Yes' : 'Off',
    taxPartnership: prof.typeOfEntity === 'LIMITED_PARTNERSHIP' ? 'Yes' : 'Off',
    taxSCorp: prof.typeOfEntity === 'S_CORPORATION' ? 'Yes' : 'Off',
    'S Corporation': prof.typeOfEntity === 'S_CORPORATION' ? 'Yes' : 'Off',
    taxCCorp: prof.typeOfEntity === 'C_CORPORATION' ? 'Yes' : 'Off',
    taxIndividual:
      type === 'INDIVIDUAL' || prof.isSingleMemberLLC ? 'Yes' : 'Off',
    SSN1Signer1: '',
    SSN2Signer1: '',
    SSN3Signer1: '',
    SSN4Signer1: '',
    SSN5Signer1: '',
    SSN6Signer1: '',
    SSN7Signer1: '',
    SSN8Signer1: '',
    SSN9Signer1: '',
    EIN1Signer1: '',
    EIN2Signer1: '',
    EIN3Signer1: '',
    EIN4Signer1: '',
    EIN5Signer1: '',
    EIN6Signer1: '',
    EIN7Signer1: '',
    EIN8Signer1: '',
    EIN9Signer1: '',
    memberEntity: '',
    memberIndividual: '**memberIndividual**',
  };

  if (
    type === 'INDIVIDUAL' ||
    type === 'JOINT' ||
    (type === 'TRUST' && prof.taxDetails!.taxIdentification.type === 'ssn')
  ) {
    const ssnString =
      type === 'JOINT'
        ? prof?.primarySignatory?.taxDetails?.value ?? ''
        : prof?.taxDetails?.taxIdentification.value ?? '';

    if (ssnString === '') {
      throw Error("SSN Expected, but wasn't supplied");
    }

    payload.SSN1Signer1 = ssnString[0];
    payload.SSN2Signer1 = ssnString[1];
    payload.SSN3Signer1 = ssnString[2];
    payload.SSN4Signer1 = ssnString[4];
    payload.SSN5Signer1 = ssnString[5];
    payload.SSN6Signer1 = ssnString[7];
    payload.SSN7Signer1 = ssnString[8];
    payload.SSN8Signer1 = ssnString[9];
    payload.SSN9Signer1 = ssnString[10];
  } else {
    const EINString = prof.taxDetails!.taxIdentification.value;

    if (!EINString) {
      throw Error("EIN Expected, but wasn't supplied");
    }

    payload.EIN1Signer1 = EINString[0];
    payload.EIN2Signer1 = EINString[1];
    payload.EIN3Signer1 = EINString[3];
    payload.EIN4Signer1 = EINString[4];
    payload.EIN5Signer1 = EINString[5];
    payload.EIN6Signer1 = EINString[6];
    payload.EIN7Signer1 = EINString[7];
    payload.EIN8Signer1 = EINString[8];
    payload.EIN9Signer1 = EINString[9];
  }

  if (signature) {
    payload.signature = signature;
  }

  return payload;
}

export function operatingAgreement(
  sub,
  prof,
  ent,
  signature: string | null = null,
) {
  const signIndividualPage = prof.taxDetails.registrationType === 'INDIVIDUAL';

  const payload: any = {
    entityLegalName: ent.name.toUpperCase(),
    investmentName: signIndividualPage
      ? `${prof.firstName} ${prof.lastName}`
      : `${prof.name}`,
    memberEntity: '',
    memberIndividual: '**memberIndividual**',
    oaHeader: 'LIMITED LIABILITY COMPANY AGREEMENT',
    signedDate: moment().format('MMM Do YYYY'),
    signer1Name: signIndividualPage
      ? ''
      : `${prof.primarySignatory.name || ''}`,
    signer1Title: signIndividualPage ? '' : prof.primarySignatory.title,
  };

  if (!['INDIVIDUAL', 'JOINT'].includes(prof.taxDetails.registrationType)) {
    payload.signer1Name = prof.primarySignatory.name;
    payload.signer1Title = prof.primarySignatory.title;
  }

  if (signature) {
    payload.signature = signature;
  }

  return payload;
}

export function w8Form(prof, signature: string | null = null) {
  const payload: any = {
    taxName1: `${prof.firstName} ${prof.lastName}`,
    signedDate: moment().format('MMM Do YYYY'),
    streetAddress1: prof.address.address1,
    cityStateZIP1: `${prof.address.city}, ${prof.address.state} ${prof.address.postalCode}`,
    country1: prof.address.country,
    taxId1:
      prof.taxDetails.taxIdentification.type !== 'ftin'
        ? prof.taxDetails.taxIdentification.value
        : ' ',
    ftin1:
      prof.taxDetails.taxIdentification.type === 'ftin'
        ? prof.taxDetails.taxIdentification.value
        : ' ',
    DOB1: prof.dateOfBirth,
    taxTitle1: ' ',

    memberEntity: '',
    memberIndividual: '**memberIndividual**',
  };

  if (signature) {
    payload.signature = signature;
  }

  return payload;
}

export function subscriptionAgreement(
  sub,
  prof,
  ent,
  deal,
  isEstimate = false,
  isRegS = false,
  signature: string | null = null,
) {
  const type = prof.taxDetails.registrationType;
  const signIndividualPage =
    prof.taxDetails.registrationType === 'INDIVIDUAL' ||
    prof.taxDetails.registrationType === 'JOINT';

  let expenseAmt = 0;
  if (ent?.expenseReserve?.amount) {
    const own_percent = sub.amount / deal.targetRaiseAmount;
    if (ent?.expenseReserve?.type === 'Flat Fee') {
      expenseAmt = ent.expenseReserve.amount * own_percent;
    } else if (ent?.expenseReserve?.type === 'Percent') {
      expenseAmt =
        deal.targetRaiseAmount * ent.expenseReserve.amount * 0.01 * own_percent;
    }
  }

  const totalAmt = sub.amount - expenseAmt;

  const payload: any = {
    // Page 1
    individualDate: signIndividualPage ? moment().format('MMM Do YYYY') : ' ',
    individualInvestmentAmount: signIndividualPage
      ? formatMoney(sub.amount)
      : ' ',
    investmentAmount: formatMoney(sub.amount),
    individualExpenseContribution: signIndividualPage
      ? formatMoney(expenseAmt)
      : ' ',
    individualTotalSubscription: signIndividualPage
      ? formatMoney(totalAmt)
      : ' ',
    investmentName: signIndividualPage
      ? `${prof.firstName} ${prof.lastName}`
      : prof.name,
    entityInvestmentName: signIndividualPage ? ' ' : prof.name,
    individualSigner1Name: signIndividualPage
      ? `${prof.firstName} ${prof.lastName}`
      : ' ',
    individualSigner2Name: ' ',
    individualDisclaimer:
      signIndividualPage && isEstimate
        ? 'This amount is an estimation based on total Subscribers’ interest.  It is subject to change, and will be finalized at the time of Closing.'
        : ' ',

    saHeader1: `${ent.name.toUpperCase()} A`,
    saHeader2: 'DELAWARE LIMITED LIABILITY COMPANY',
    memberIndividual: signIndividualPage ? '**memberIndividual**' : '',
    //"signer2": "",

    // Page 2
    entityDate: signIndividualPage ? ' ' : moment().format('MMM Do YYYY'),
    entityInvestmentAmount: signIndividualPage ? ' ' : formatMoney(sub.amount),
    entityExpenseContribution: signIndividualPage
      ? ' '
      : formatMoney(expenseAmt),
    entityTotalSubscription: signIndividualPage ? ' ' : formatMoney(totalAmt),
    entitySignerName: signIndividualPage
      ? ' '
      : `${prof.primarySignatory.name || ''}`,
    entitySignerTitle: signIndividualPage ? ' ' : prof.primarySignatory.title,
    memberEntity: signIndividualPage ? '' : '**memberIndividual**',

    entityDisclaimer:
      !signIndividualPage && isEstimate
        ? 'This amount is an estimation based on total Subscribers’ interest.  It is subject to change, and will be finalized at the time of Closing.'
        : ' ',

    //// Page 3
    EIN:
      type !== 'JOINT' && prof.isUSBased
        ? prof.taxDetails.taxIdentification.value
        : ' ',
    stateOfFormation: prof?.stateOfFormation ?? '',
    address1: `${prof.address.address1} ${prof.address.address2 || ''}`,
    address2: `${prof.address.city}, ${prof.address.state} ${prof.address.postalCode} ${prof.address.country}`,
    deliveryAddress1: ' ',
    deliveryAddress2: ' ',
    phoneNumber: prof.phone,
    email: prof.email,
    LP: prof.typeOfEntity === 'LIMITED_PARTNERSHIP' ? 'Yes' : 'No',
    LLC: prof.typeOfEntity === 'LIMITED_LIABILITY_COMPANY' ? 'Yes' : 'No',
    corp:
      prof.typeOfEntity === 'C_CORPORATION' ||
      prof.typeOfEntity === 'S_CORPORATION' ||
      prof.typeOfEntity === 'GENERAL_PARTNERSHIP'
        ? 'Yes'
        : 'No',
    trust: type === 'TRUST' ? 'Yes' : 'No',

    //// Page 4
    singleIndividual: type === 'INDIVIDUAL' ? 'Yes' : 'No',
    //"jointTenants": "",
    //"tenantsInCommon": "",
    //"communityProperty": "",
    w9: shouldUseW9(prof) ? 'Yes' : 'No',
    w8ben:
      !prof.isUSBased && type !== 'ENTITY' && type !== 'TRUST' ? 'Yes' : 'No',
    w8bene: prof.typeOfEntity === 'FOREIGN_ENTITY' ? 'Yes' : 'No',

    //// Page 5
    investorStatus0: prof.investorStatus
      ? prof.investorStatus.includes(0)
        ? 'Yes'
        : 'No'
      : type === 'INDIVIDUAL'
      ? 'Yes'
      : 'No',
    investorStatus1: prof.investorStatus?.includes(1) ? 'Yes' : 'No',
    investorStatus2: prof.investorStatus?.includes(2) ? 'Yes' : 'No',
    investorStatus3: prof.investorStatus
      ? prof.investorStatus.includes(3)
        ? 'Yes'
        : 'No'
      : type === 'TRUST'
      ? 'Yes'
      : 'No',
    investorStatus4: prof.investorStatus?.includes(4) ? 'Yes' : 'No',

    //// Page 6
    investorStatus5: prof.investorStatus?.includes(5) ? 'Yes' : 'No',
    investorStatus6: prof.investorStatus?.includes(6) ? 'Yes' : 'No',
    investorStatus7: prof.investorStatus?.includes(7) ? 'Yes' : 'No',
    investorStatus8: prof.investorStatus
      ? prof.investorStatus.includes(8)
        ? 'Yes'
        : 'No'
      : type === 'ENTITY'
      ? 'Yes'
      : 'No',
    investorStatus9: prof.investorStatus?.includes(9) ? 'Yes' : 'No',
    investorStatus10: prof.investorStatus?.includes(10) ? 'Yes' : 'No',

    //// Page 11
    wiringBankName: sub?.fundingInfo?.bankName ?? '',
    wiringBankInUS: sub?.fundingInfo?.isCustomerOfBank ? 'Yes' : 'No',
    wiringBankNotInUS: !sub?.fundingInfo?.isCustomerOfBank ? 'Yes' : 'No',
    wiringBankSub: sub?.fundingInfo?.isFATFBank ? 'Yes' : 'No',
    wiringBankNotSub: !sub?.fundingInfo?.isFATFBank ? 'Yes' : 'No',
  };

  const numPurchaserStatusOptions = 11;
  for (let i = 0; i < numPurchaserStatusOptions; i++) {
    payload['purchaserStatus' + i] = prof.purchaserStatus?.includes(i)
      ? 'Yes'
      : 'No';
  }

  if (signature) {
    payload.signature = signature;
  }

  return payload;
}
