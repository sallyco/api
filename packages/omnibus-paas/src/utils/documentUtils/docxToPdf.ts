const fs = require('fs');
const docx = require('./docx-wasm/api');

import debugFactory from 'debug';
const debug = debugFactory('omnibus-paas:utils:convertToPdf');

export async function convertToPdf(
  fileName: string,
  fileType: string,
  desiredType: string,
) {
  docx
    .init({
      ENVIRONMENT: 'NODE',
      LAZY_INIT: true,
    })
    .catch(function (e) {
      console.error(e);
    });

  async function convertHelper(document, exportFct) {
    const api = await docx.engine();
    await api.load(document);
    const arrayBuffer = await api[exportFct]();
    await api.close();
    return arrayBuffer;
  }

  const enterPath = '/tmp/' + fileName + fileType;
  const outputPath = '/tmp/' + fileName + desiredType;

  debug('Converting File', enterPath);

  return new Promise((resolve, reject) => {
    convertHelper(enterPath, 'exportPDF')
      .then(arrayBuffer => {
        fs.writeFileSync(outputPath, new Uint8Array(arrayBuffer));
        resolve(fileName + desiredType);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
}
