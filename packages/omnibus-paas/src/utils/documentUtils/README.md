

    #### action: "populateData"

    Request

    ```json
    {
    "service":"merge",
    "action":"populateData",
    "inputBucket":"dm-templates-loader",
    "outputBucket":"dm-template-receiver",
    "inputKey":"OATemplate.docx",
    "outputKey":"OAfilled.docx",
    "signerRole":"",
    "delimiters":"{{}}",
    "dictionaryData": {
            "state": "Utah",
            "managerName": "John Doe",
            "managerState": "Utah",
            "managerEntityType": "Fund Management",
            "managerContact": "Johnny Cash",
            "managerTitle": "Manager",
            "organizerName": "Jane Doe",
            "organizerContact": "Janine Dow",
            "organizerTitle": "Organizer",
            "memberName": "Jack Munoz",
            "memberSignatureDate": "6 September 2019",
            "memberAuthSignName": "None",
            "memberAuthSignTitle": "None",
            "entityLegalName": "Ardua Inc",
            "fundStateOfFormation": "Utah",
            "fundEntityType": "LLC",
            "organizerState": "Florida",
            "organizerEntityType": "INC",
            "managementFeePercent": "3%",
            "managementFeeTerm": "60",
            "organizerContactTitle": "Paralegal",
            "managerContactTitle": "Fund Associate"
        }
    }
    ```

    Response

    ```json

    {
        "Service": "Merge Ok"
    }

    ```
