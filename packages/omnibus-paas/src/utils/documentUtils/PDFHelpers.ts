/* eslint-disable @typescript-eslint/no-explicit-any */
import * as pdfjslib from 'pdfjs-dist/legacy/build/pdf';
import {
  PDFDocument,
  PDFName,
  PDFTextField,
  StandardFonts,
  TextAlignment,
} from 'pdf-lib';
import {Readable} from 'stream';
import debugFactory from 'debug';
import {DocumentInitParameters, PDFDocumentProxy} from 'pdfjs-dist/types/src/display/api';
import {TypedArray} from 'type-fest';
import {PDFDataRangeTransport} from 'pdfjs-dist/legacy/build/pdf';

const debug = debugFactory('omnibus-paas:utils:pdf-helpers');

export class PDFHelpers {
  static async getPageText(pdf: PDFDocumentProxy, pageNo: number) {
    const page = await pdf.getPage(pageNo);
    const tokenizedText = await page.getTextContent();
    return tokenizedText.items.map((token: any) => token.str).join('');
  }

  static async findPageIndexWithText(
    source:
      | string
      | URL
      | TypedArray
      | PDFDataRangeTransport
      | DocumentInitParameters,
    search: string | RegExp,
  ): Promise<number> {
    const pdf = await pdfjslib.getDocument(source).promise;
    const maxPages = pdf.numPages;
    for (let pageNo = 0; pageNo < maxPages; pageNo += 1) {
      const text = await this.getPageText(pdf, pageNo + 1);
      if (text.search(search) > -1) {
        return pageNo;
      }
    }
    return -1;
  }

  static async populateTemplate(
    template: Buffer | Uint8Array,
    fillableData: any,
    finalize = false,
  ): Promise<Uint8Array> {
    const pdfDoc = await PDFDocument.load(template);
    const form = pdfDoc.getForm();
    const fields = form.getFields();
    const times = await pdfDoc.embedFont(StandardFonts.TimesRoman);
    for (const field of fields) {
      try {
        const name = field.getName();
        if (name in fillableData) {
          const fieldData = fillableData[name];
          if (fieldData.type === 'text' && fieldData.value) {
            (field as PDFTextField).setText(fieldData.value);
            if (fieldData.textAlignment) {
              const alignmentMap = {
                center: TextAlignment.Center,
                left: TextAlignment.Left,
                right: TextAlignment.Right,
              };
              (field as PDFTextField).setAlignment(
                alignmentMap[fieldData.textAlignment ?? TextAlignment.Left],
              );
            }
            (field as PDFTextField).enableMultiline();
            (field as PDFTextField).defaultUpdateAppearances(times);
            (field as PDFTextField).enableReadOnly();
          } else if (fieldData.type === 'signature' && fieldData.value) {
            const image = await pdfDoc.embedPng(fieldData.value);
            (field as PDFTextField).setImage(image);
          }
        }
      } catch (e) {
        // We can't update this field
        debug(`Issue with updating field ${name}`, e.message);
      }
    }
    form.flatten();
    let data: Uint8Array;
    try {
      data = await pdfDoc.save();
      return data;
    } catch (e) {
      debug(
        'Failed to update Field appearances, falling back to default appearance save',
      );
    }
    data = await pdfDoc.save({updateFieldAppearances: false});
    return data;
  }

  static async removePages(document: PDFDocument, from: number, to: number) {
    for (let page = to; page > from; page--) {
      document.removePage(page);
    }
  }

  static async mergeDocumentAtIndex(
    document: PDFDocument,
    donorDocument: PDFDocument,
    start: number,
  ) {
    const totalPages = donorDocument.getPageCount();
    for (let page = 0; page < totalPages; page++) {
      const [donorPage] = await document.copyPages(donorDocument, [page]);

      try {
        // donorDocument.getForm().getFields().forEach(field => {
        //   const newRef = document.context.register(field.acroField.dict);
        //   document.getForm().acroForm.addField(newRef);
        // });

        // This ensures all Annots come along when merging
        const existingAnnots = donorPage?.node?.Annots()?.asArray() ?? [];
        const annots = [...existingAnnots];
        donorPage.node.set(PDFName.of('Annots'), document.context.obj(annots));

        document.insertPage(start + page, donorPage);
      } catch (e) {
        debug('Issue with merging pages: ', e.message);
      }
    }
  }

  static async convertStreamToBuffer(readable: Readable): Promise<Buffer> {
    const chunks: any[] = [];
    for await (const chunk of readable) {
      chunks.push(chunk);
    }
    return Buffer.concat(chunks);
  }
}
