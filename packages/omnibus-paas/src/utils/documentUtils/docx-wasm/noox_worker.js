var nd_init = function (nd_init) {
  nd_init = nd_init || {};

  var Module = typeof nd_init !== 'undefined' ? nd_init : {};

  function ndResourceLookup(packagename, filename) {
    var ret = null;
    var res;
    if ((res = Module['ND_RESOURCES'])) {
      ret = res[packagename];
      if (ret) {
        if ('function' === typeof ret) {
          ret = ret(filename);
        } else {
          ret = ret[filename];
        }
        ret = new Uint8Array(ret, 0);
      }
    }
    return ret || null;
  }

  function ndLocateExternalPath(packagename, filename) {
    var nd_externals;
    var module_dir = null;
    if (
      (nd_externals = Module['ND_EXTERNALS']) &&
      (module_dir = nd_externals[packagename])
    ) {
      if ('function' === typeof module_dir) {
        module_dir = module_dir(filename);
      }
    } else if (Module['ENVIRONMENT'] === 'NODE') {
      var path = require('path');
      //var module_path = require.resolve(packagename);
      module_dir = path.dirname(__dirname) + path.sep + packagename + path.sep;
    } else {
      console.error(
        'unable to locate path for ' +
          JSON.stringify(filename) +
          ' in package ' +
          JSON.stringify(packagename),
      );
    }
    if (module_dir) {
      var file_path = module_dir + filename;
      return file_path;
    } else {
      return null;
    }
  }
  Module['locateFile'] = function (filename) {
    return ndLocateExternalPath('docx-wasm', filename);
  };
  Module['readResource'] = function (filename) {
    var ret = ndResourceLookup('fontpool', filename);
    if (ret) {
      return ret;
    } else {
      var filepath = ndLocateExternalPath('fontpool', filename);
      var filedata = filepath ? Module['readBinary'](filepath) : null;
      if (filedata) {
        return filedata;
      } else {
        throw new Error('Unable to read resource ' + JSON.stringify(filename));
      }
    }
  };
  var moduleOverrides = {};
  var key;
  for (key in Module) {
    if (Module.hasOwnProperty(key)) {
      moduleOverrides[key] = Module[key];
    }
  }
  Module['arguments'] = [];
  Module['thisProgram'] = './this.program';
  Module['quit'] = function (status, toThrow) {
    throw toThrow;
  };
  Module['preRun'] = [];
  Module['postRun'] = [];
  var ENVIRONMENT_IS_WEB = false;
  var ENVIRONMENT_IS_WORKER = false;
  var ENVIRONMENT_IS_NODE = true;
  var ENVIRONMENT_IS_SHELL = false;
  // ENVIRONMENT_IS_WEB = typeof window === 'object';
  // ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
  // ENVIRONMENT_IS_NODE =
  //   typeof process === 'object' &&
  //   typeof require === 'function' &&
  //   !ENVIRONMENT_IS_WEB &&
  //   !ENVIRONMENT_IS_WORKER;
  // ENVIRONMENT_IS_SHELL =
  //   !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;
  if (ENVIRONMENT_IS_NODE) {
    var nodeFS;
    var nodePath;
    Module['read'] = function shell_read(filename, binary) {
      var ret;
      if (!nodeFS) nodeFS = require('fs');
      if (!nodePath) nodePath = require('path');
      filename = nodePath['normalize'](filename);
      ret = nodeFS['readFileSync'](filename);
      return binary ? ret : ret.toString();
    };
    Module['readBinary'] = function readBinary(filename) {
      var ret = Module['read'](filename, true);
      if (!ret.buffer) {
        ret = new Uint8Array(ret);
      }
      assert(ret.buffer);
      return ret;
    };
    if (process['argv'].length > 1) {
      Module['thisProgram'] = process['argv'][1].replace(/\\/g, '/');
    }
    Module['arguments'] = process['argv'].slice(2);
    process['on']('uncaughtException', function (ex) {
      if (!(ex instanceof ExitStatus)) {
        throw ex;
      }
    });
    process['on']('unhandledRejection', function (reason, p) {
      process['exit'](1);
    });
    Module['quit'] = function (status) {
      process['exit'](status);
    };
    Module['inspect'] = function () {
      return '[Emscripten Module object]';
    };
  } else if (ENVIRONMENT_IS_SHELL) {
    if (typeof read != 'undefined') {
      Module['read'] = function shell_read(f) {
        return read(f);
      };
    }
    Module['readBinary'] = function readBinary(f) {
      var data;
      if (typeof readbuffer === 'function') {
        return new Uint8Array(readbuffer(f));
      }
      data = read(f, 'binary');
      assert(typeof data === 'object');
      return data;
    };
    if (typeof scriptArgs != 'undefined') {
      Module['arguments'] = scriptArgs;
    } else if (typeof arguments != 'undefined') {
      Module['arguments'] = arguments;
    }
    if (typeof quit === 'function') {
      Module['quit'] = function (status) {
        quit(status);
      };
    }
  } else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
    Module['read'] = function shell_read(url) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, false);
      xhr.send(null);
      return xhr.responseText;
    };
    if (ENVIRONMENT_IS_WORKER) {
      Module['readBinary'] = function readBinary(url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.responseType = 'arraybuffer';
        xhr.send(null);
        return new Uint8Array(xhr.response);
      };
    }
    Module['readAsync'] = function readAsync(url, onload, onerror) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.responseType = 'arraybuffer';
      xhr.onload = function xhr_onload() {
        if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) {
          onload(xhr.response);
          return;
        }
        onerror();
      };
      xhr.onerror = onerror;
      xhr.send(null);
    };
    Module['setWindowTitle'] = function (title) {
      document.title = title;
    };
  } else {
  }
  var out =
    Module['print'] ||
    (typeof console !== 'undefined'
      ? console.log.bind(console)
      : typeof print !== 'undefined'
      ? print
      : null);
  var err =
    Module['printErr'] ||
    (typeof printErr !== 'undefined'
      ? printErr
      : (typeof console !== 'undefined' && console.warn.bind(console)) || out);
  for (key in moduleOverrides) {
    if (moduleOverrides.hasOwnProperty(key)) {
      Module[key] = moduleOverrides[key];
    }
  }
  moduleOverrides = undefined;
  var STACK_ALIGN = 16;

  function staticAlloc(size) {
    var ret = STATICTOP;
    STATICTOP = (STATICTOP + size + 15) & -16;
    return ret;
  }

  function dynamicAlloc(size) {
    var ret = HEAP32[DYNAMICTOP_PTR >> 2];
    var end = (ret + size + 15) & -16;
    HEAP32[DYNAMICTOP_PTR >> 2] = end;
    if (end >= TOTAL_MEMORY) {
      var success = enlargeMemory();
      if (!success) {
        HEAP32[DYNAMICTOP_PTR >> 2] = ret;
        return 0;
      }
    }
    return ret;
  }

  function alignMemory(size, factor) {
    if (!factor) factor = STACK_ALIGN;
    var ret = (size = Math.ceil(size / factor) * factor);
    return ret;
  }

  function getNativeTypeSize(type) {
    switch (type) {
      case 'i1':
      case 'i8':
        return 1;
      case 'i16':
        return 2;
      case 'i32':
        return 4;
      case 'i64':
        return 8;
      case 'float':
        return 4;
      case 'double':
        return 8;
      default: {
        if (type[type.length - 1] === '*') {
          return 4;
        } else if (type[0] === 'i') {
          var bits = parseInt(type.substr(1));
          assert(bits % 8 === 0);
          return bits / 8;
        } else {
          return 0;
        }
      }
    }
  }
  var asm2wasmImports = {
    'f64-rem': function (x, y) {
      return x % y;
    },
    debugger: function () {
      debugger;
    },
  };
  var functionPointers = new Array(0);
  var GLOBAL_BASE = 1024;
  var ABORT = 0;
  var EXITSTATUS = 0;

  function assert(condition, text) {
    if (!condition) {
      abort('Assertion failed: ' + text);
    }
  }

  function setValue(ptr, value, type, noSafe) {
    type = type || 'i8';
    if (type.charAt(type.length - 1) === '*') type = 'i32';
    switch (type) {
      case 'i1':
        HEAP8[ptr >> 0] = value;
        break;
      case 'i8':
        HEAP8[ptr >> 0] = value;
        break;
      case 'i16':
        HEAP16[ptr >> 1] = value;
        break;
      case 'i32':
        HEAP32[ptr >> 2] = value;
        break;
      case 'i64':
        (tempI64 = [
          value >>> 0,
          ((tempDouble = value),
          +Math_abs(tempDouble) >= 1
            ? tempDouble > 0
              ? (Math_min(+Math_floor(tempDouble / 4294967296), 4294967295) |
                  0) >>>
                0
              : ~~+Math_ceil(
                  (tempDouble - +(~~tempDouble >>> 0)) / 4294967296,
                ) >>> 0
            : 0),
        ]),
          (HEAP32[ptr >> 2] = tempI64[0]),
          (HEAP32[(ptr + 4) >> 2] = tempI64[1]);
        break;
      case 'float':
        HEAPF32[ptr >> 2] = value;
        break;
      case 'double':
        HEAPF64[ptr >> 3] = value;
        break;
      default:
        abort('invalid type for setValue: ' + type);
    }
  }
  var ALLOC_NORMAL = 0;
  var ALLOC_STATIC = 2;
  var ALLOC_NONE = 4;

  function allocate(slab, types, allocator, ptr) {
    var zeroinit, size;
    if (typeof slab === 'number') {
      zeroinit = true;
      size = slab;
    } else {
      zeroinit = false;
      size = slab.length;
    }
    var singleType = typeof types === 'string' ? types : null;
    var ret;
    if (allocator == ALLOC_NONE) {
      ret = ptr;
    } else {
      ret = [
        typeof _malloc === 'function' ? _malloc : staticAlloc,
        stackAlloc,
        staticAlloc,
        dynamicAlloc,
      ][allocator === undefined ? ALLOC_STATIC : allocator](
        Math.max(size, singleType ? 1 : types.length),
      );
    }
    if (zeroinit) {
      var stop;
      ptr = ret;
      assert((ret & 3) == 0);
      stop = ret + (size & ~3);
      for (; ptr < stop; ptr += 4) {
        HEAP32[ptr >> 2] = 0;
      }
      stop = ret + size;
      while (ptr < stop) {
        HEAP8[ptr++ >> 0] = 0;
      }
      return ret;
    }
    if (singleType === 'i8') {
      if (slab.subarray || slab.slice) {
        HEAPU8.set(slab, ret);
      } else {
        HEAPU8.set(new Uint8Array(slab), ret);
      }
      return ret;
    }
    var i = 0,
      type,
      typeSize,
      previousType;
    while (i < size) {
      var curr = slab[i];
      type = singleType || types[i];
      if (type === 0) {
        i++;
        continue;
      }
      if (type == 'i64') type = 'i32';
      setValue(ret + i, curr, type);
      if (previousType !== type) {
        typeSize = getNativeTypeSize(type);
        previousType = type;
      }
      i += typeSize;
    }
    return ret;
  }

  function getMemory(size) {
    if (!staticSealed) return staticAlloc(size);
    if (!runtimeInitialized) return dynamicAlloc(size);
    return _malloc(size);
  }

  function Pointer_stringify(ptr, length) {
    if (length === 0 || !ptr) return '';
    var hasUtf = 0;
    var t;
    var i = 0;
    while (1) {
      t = HEAPU8[(ptr + i) >> 0];
      hasUtf |= t;
      if (t == 0 && !length) break;
      i++;
      if (length && i == length) break;
    }
    if (!length) length = i;
    var ret = '';
    if (hasUtf < 128) {
      var MAX_CHUNK = 1024;
      var curr;
      while (length > 0) {
        curr = String.fromCharCode.apply(
          String,
          HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)),
        );
        ret = ret ? ret + curr : curr;
        ptr += MAX_CHUNK;
        length -= MAX_CHUNK;
      }
      return ret;
    }
    return UTF8ToString(ptr);
  }
  var UTF8Decoder =
    typeof TextDecoder !== 'undefined' ? new TextDecoder('utf8') : undefined;

  function UTF8ArrayToString(u8Array, idx) {
    var endPtr = idx;
    while (u8Array[endPtr]) ++endPtr;
    if (endPtr - idx > 16 && u8Array.subarray && UTF8Decoder) {
      return UTF8Decoder.decode(u8Array.subarray(idx, endPtr));
    } else {
      var u0, u1, u2, u3, u4, u5;
      var str = '';
      while (1) {
        u0 = u8Array[idx++];
        if (!u0) return str;
        if (!(u0 & 128)) {
          str += String.fromCharCode(u0);
          continue;
        }
        u1 = u8Array[idx++] & 63;
        if ((u0 & 224) == 192) {
          str += String.fromCharCode(((u0 & 31) << 6) | u1);
          continue;
        }
        u2 = u8Array[idx++] & 63;
        if ((u0 & 240) == 224) {
          u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
        } else {
          u3 = u8Array[idx++] & 63;
          if ((u0 & 248) == 240) {
            u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | u3;
          } else {
            u4 = u8Array[idx++] & 63;
            if ((u0 & 252) == 248) {
              u0 = ((u0 & 3) << 24) | (u1 << 18) | (u2 << 12) | (u3 << 6) | u4;
            } else {
              u5 = u8Array[idx++] & 63;
              u0 =
                ((u0 & 1) << 30) |
                (u1 << 24) |
                (u2 << 18) |
                (u3 << 12) |
                (u4 << 6) |
                u5;
            }
          }
        }
        if (u0 < 65536) {
          str += String.fromCharCode(u0);
        } else {
          var ch = u0 - 65536;
          str += String.fromCharCode(55296 | (ch >> 10), 56320 | (ch & 1023));
        }
      }
    }
  }

  function UTF8ToString(ptr) {
    return UTF8ArrayToString(HEAPU8, ptr);
  }

  function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
    if (!(maxBytesToWrite > 0)) return 0;
    var startIdx = outIdx;
    var endIdx = outIdx + maxBytesToWrite - 1;
    for (var i = 0; i < str.length; ++i) {
      var u = str.charCodeAt(i);
      if (u >= 55296 && u <= 57343)
        u = (65536 + ((u & 1023) << 10)) | (str.charCodeAt(++i) & 1023);
      if (u <= 127) {
        if (outIdx >= endIdx) break;
        outU8Array[outIdx++] = u;
      } else if (u <= 2047) {
        if (outIdx + 1 >= endIdx) break;
        outU8Array[outIdx++] = 192 | (u >> 6);
        outU8Array[outIdx++] = 128 | (u & 63);
      } else if (u <= 65535) {
        if (outIdx + 2 >= endIdx) break;
        outU8Array[outIdx++] = 224 | (u >> 12);
        outU8Array[outIdx++] = 128 | ((u >> 6) & 63);
        outU8Array[outIdx++] = 128 | (u & 63);
      } else if (u <= 2097151) {
        if (outIdx + 3 >= endIdx) break;
        outU8Array[outIdx++] = 240 | (u >> 18);
        outU8Array[outIdx++] = 128 | ((u >> 12) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 6) & 63);
        outU8Array[outIdx++] = 128 | (u & 63);
      } else if (u <= 67108863) {
        if (outIdx + 4 >= endIdx) break;
        outU8Array[outIdx++] = 248 | (u >> 24);
        outU8Array[outIdx++] = 128 | ((u >> 18) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 12) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 6) & 63);
        outU8Array[outIdx++] = 128 | (u & 63);
      } else {
        if (outIdx + 5 >= endIdx) break;
        outU8Array[outIdx++] = 252 | (u >> 30);
        outU8Array[outIdx++] = 128 | ((u >> 24) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 18) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 12) & 63);
        outU8Array[outIdx++] = 128 | ((u >> 6) & 63);
        outU8Array[outIdx++] = 128 | (u & 63);
      }
    }
    outU8Array[outIdx] = 0;
    return outIdx - startIdx;
  }

  function stringToUTF8(str, outPtr, maxBytesToWrite) {
    return stringToUTF8Array(str, HEAPU8, outPtr, maxBytesToWrite);
  }

  function lengthBytesUTF8(str) {
    var len = 0;
    for (var i = 0; i < str.length; ++i) {
      var u = str.charCodeAt(i);
      if (u >= 55296 && u <= 57343)
        u = (65536 + ((u & 1023) << 10)) | (str.charCodeAt(++i) & 1023);
      if (u <= 127) {
        ++len;
      } else if (u <= 2047) {
        len += 2;
      } else if (u <= 65535) {
        len += 3;
      } else if (u <= 2097151) {
        len += 4;
      } else if (u <= 67108863) {
        len += 5;
      } else {
        len += 6;
      }
    }
    return len;
  }
  var UTF16Decoder =
    typeof TextDecoder !== 'undefined'
      ? new TextDecoder('utf-16le')
      : undefined;

  function UTF16ToString(ptr) {
    var endPtr = ptr;
    var idx = endPtr >> 1;
    while (HEAP16[idx]) ++idx;
    endPtr = idx << 1;
    if (endPtr - ptr > 32 && UTF16Decoder) {
      return UTF16Decoder.decode(HEAPU8.subarray(ptr, endPtr));
    } else {
      var i = 0;
      var str = '';
      while (1) {
        var codeUnit = HEAP16[(ptr + i * 2) >> 1];
        if (codeUnit == 0) return str;
        ++i;
        str += String.fromCharCode(codeUnit);
      }
    }
  }

  function stringToUTF16(str, outPtr, maxBytesToWrite) {
    if (maxBytesToWrite === undefined) {
      maxBytesToWrite = 2147483647;
    }
    if (maxBytesToWrite < 2) return 0;
    maxBytesToWrite -= 2;
    var startPtr = outPtr;
    var numCharsToWrite =
      maxBytesToWrite < str.length * 2 ? maxBytesToWrite / 2 : str.length;
    for (var i = 0; i < numCharsToWrite; ++i) {
      var codeUnit = str.charCodeAt(i);
      HEAP16[outPtr >> 1] = codeUnit;
      outPtr += 2;
    }
    HEAP16[outPtr >> 1] = 0;
    return outPtr - startPtr;
  }

  function allocateUTF8(str) {
    var size = lengthBytesUTF8(str) + 1;
    var ret = _malloc(size);
    if (ret) stringToUTF8Array(str, HEAP8, ret, size);
    return ret;
  }

  function allocateUTF8OnStack(str) {
    var size = lengthBytesUTF8(str) + 1;
    var ret = stackAlloc(size);
    stringToUTF8Array(str, HEAP8, ret, size);
    return ret;
  }
  var PAGE_SIZE = 16384;
  var WASM_PAGE_SIZE = 65536;
  var ASMJS_PAGE_SIZE = 16777216;
  var MIN_TOTAL_MEMORY = 16777216;

  function alignUp(x, multiple) {
    if (x % multiple > 0) {
      x += multiple - (x % multiple);
    }
    return x;
  }
  var buffer, HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;

  function updateGlobalBuffer(buf) {
    Module['buffer'] = buffer = buf;
  }

  function updateGlobalBufferViews() {
    Module['HEAP8'] = HEAP8 = new Int8Array(buffer);
    Module['HEAP16'] = HEAP16 = new Int16Array(buffer);
    Module['HEAP32'] = HEAP32 = new Int32Array(buffer);
    Module['HEAPU8'] = HEAPU8 = new Uint8Array(buffer);
    Module['HEAPU16'] = HEAPU16 = new Uint16Array(buffer);
    Module['HEAPU32'] = HEAPU32 = new Uint32Array(buffer);
    Module['HEAPF32'] = HEAPF32 = new Float32Array(buffer);
    Module['HEAPF64'] = HEAPF64 = new Float64Array(buffer);
  }
  var STATIC_BASE, STATICTOP, staticSealed;
  var STACK_BASE, STACKTOP, STACK_MAX;
  var DYNAMIC_BASE, DYNAMICTOP_PTR;
  STATIC_BASE =
    STATICTOP =
    STACK_BASE =
    STACKTOP =
    STACK_MAX =
    DYNAMIC_BASE =
    DYNAMICTOP_PTR =
      0;
  staticSealed = false;

  function abortOnCannotGrowMemory() {
    abort(
      'Cannot enlarge memory arrays. Either (1) compile with  -s TOTAL_MEMORY=X  with X higher than the current value ' +
        TOTAL_MEMORY +
        ', (2) compile with  -s ALLOW_MEMORY_GROWTH=1  which allows increasing the size at runtime, or (3) if you want malloc to return NULL (0) instead of this abort, compile with  -s ABORTING_MALLOC=0 ',
    );
  }
  if (!Module['reallocBuffer'])
    Module['reallocBuffer'] = function (size) {
      var ret;
      try {
        if (ArrayBuffer.transfer) {
          ret = ArrayBuffer.transfer(buffer, size);
        } else {
          var oldHEAP8 = HEAP8;
          ret = new ArrayBuffer(size);
          var temp = new Int8Array(ret);
          temp.set(oldHEAP8);
        }
      } catch (e) {
        return false;
      }
      var success = _emscripten_replace_memory(ret);
      if (!success) return false;
      return ret;
    };

  function enlargeMemory() {
    var PAGE_MULTIPLE = Module['usingWasm'] ? WASM_PAGE_SIZE : ASMJS_PAGE_SIZE;
    var LIMIT = 2147483648 - PAGE_MULTIPLE;
    if (HEAP32[DYNAMICTOP_PTR >> 2] > LIMIT) {
      return false;
    }
    var OLD_TOTAL_MEMORY = TOTAL_MEMORY;
    TOTAL_MEMORY = Math.max(TOTAL_MEMORY, MIN_TOTAL_MEMORY);
    while (TOTAL_MEMORY < HEAP32[DYNAMICTOP_PTR >> 2]) {
      if (TOTAL_MEMORY <= 536870912) {
        TOTAL_MEMORY = alignUp(2 * TOTAL_MEMORY, PAGE_MULTIPLE);
      } else {
        TOTAL_MEMORY = Math.min(
          alignUp((3 * TOTAL_MEMORY + 2147483648) / 4, PAGE_MULTIPLE),
          LIMIT,
        );
      }
    }
    var replacement = Module['reallocBuffer'](TOTAL_MEMORY);
    if (!replacement || replacement.byteLength != TOTAL_MEMORY) {
      TOTAL_MEMORY = OLD_TOTAL_MEMORY;
      return false;
    }
    updateGlobalBuffer(replacement);
    updateGlobalBufferViews();
    return true;
  }
  var byteLength;
  try {
    byteLength = Function.prototype.call.bind(
      Object.getOwnPropertyDescriptor(ArrayBuffer.prototype, 'byteLength').get,
    );
    byteLength(new ArrayBuffer(4));
  } catch (e) {
    byteLength = function (buffer) {
      return buffer.byteLength;
    };
  }
  var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
  var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 536870912;
  if (TOTAL_MEMORY < TOTAL_STACK)
    err(
      'TOTAL_MEMORY should be larger than TOTAL_STACK, was ' +
        TOTAL_MEMORY +
        '! (TOTAL_STACK=' +
        TOTAL_STACK +
        ')',
    );
  if (Module['buffer']) {
    buffer = Module['buffer'];
  } else {
    if (
      typeof WebAssembly === 'object' &&
      typeof WebAssembly.Memory === 'function'
    ) {
      Module['wasmMemory'] = new WebAssembly.Memory({
        initial: TOTAL_MEMORY / WASM_PAGE_SIZE,
      });
      buffer = Module['wasmMemory'].buffer;
    } else {
      buffer = new ArrayBuffer(TOTAL_MEMORY);
    }
    Module['buffer'] = buffer;
  }
  updateGlobalBufferViews();

  function getTotalMemory() {
    return TOTAL_MEMORY;
  }

  function callRuntimeCallbacks(callbacks) {
    while (callbacks.length > 0) {
      var callback = callbacks.shift();
      if (typeof callback == 'function') {
        callback();
        continue;
      }
      var func = callback.func;
      if (typeof func === 'number') {
        if (callback.arg === undefined) {
          Module['dynCall_v'](func);
        } else {
          Module['dynCall_vi'](func, callback.arg);
        }
      } else {
        func(callback.arg === undefined ? null : callback.arg);
      }
    }
  }
  var __ATPRERUN__ = [];
  var __ATINIT__ = [];
  var __ATMAIN__ = [];
  var __ATEXIT__ = [];
  var __ATPOSTRUN__ = [];
  var runtimeInitialized = false;
  var runtimeExited = false;

  function preRun() {
    if (Module['preRun']) {
      if (typeof Module['preRun'] == 'function')
        Module['preRun'] = [Module['preRun']];
      while (Module['preRun'].length) {
        addOnPreRun(Module['preRun'].shift());
      }
    }
    callRuntimeCallbacks(__ATPRERUN__);
  }

  function ensureInitRuntime() {
    if (runtimeInitialized) return;
    runtimeInitialized = true;
    callRuntimeCallbacks(__ATINIT__);
  }

  function preMain() {
    callRuntimeCallbacks(__ATMAIN__);
  }

  function exitRuntime() {
    callRuntimeCallbacks(__ATEXIT__);
    runtimeExited = true;
  }

  function postRun() {
    if (Module['postRun']) {
      if (typeof Module['postRun'] == 'function')
        Module['postRun'] = [Module['postRun']];
      while (Module['postRun'].length) {
        addOnPostRun(Module['postRun'].shift());
      }
    }
    callRuntimeCallbacks(__ATPOSTRUN__);
  }

  function addOnPreRun(cb) {
    __ATPRERUN__.unshift(cb);
  }

  function addOnPostRun(cb) {
    __ATPOSTRUN__.unshift(cb);
  }

  function writeArrayToMemory(array, buffer) {
    HEAP8.set(array, buffer);
  }

  function writeAsciiToMemory(str, buffer, dontAddNull) {
    for (var i = 0; i < str.length; ++i) {
      HEAP8[buffer++ >> 0] = str.charCodeAt(i);
    }
    if (!dontAddNull) HEAP8[buffer >> 0] = 0;
  }
  var Math_abs = Math.abs;
  var Math_ceil = Math.ceil;
  var Math_floor = Math.floor;
  var Math_min = Math.min;
  var Math_trunc = Math.trunc;
  var runDependencies = 0;
  var runDependencyWatcher = null;
  var dependenciesFulfilled = null;

  function addRunDependency(id) {
    runDependencies++;
    if (Module['monitorRunDependencies']) {
      Module['monitorRunDependencies'](runDependencies);
    }
  }

  function removeRunDependency(id) {
    runDependencies--;
    if (Module['monitorRunDependencies']) {
      Module['monitorRunDependencies'](runDependencies);
    }
    if (runDependencies == 0) {
      if (runDependencyWatcher !== null) {
        clearInterval(runDependencyWatcher);
        runDependencyWatcher = null;
      }
      if (dependenciesFulfilled) {
        var callback = dependenciesFulfilled;
        dependenciesFulfilled = null;
        callback();
      }
    }
  }
  Module['preloadedImages'] = {};
  Module['preloadedAudios'] = {};
  var dataURIPrefix = 'data:application/octet-stream;base64,';

  function isDataURI(filename) {
    return String.prototype.startsWith
      ? filename.startsWith(dataURIPrefix)
      : filename.indexOf(dataURIPrefix) === 0;
  }

  function integrateWasmJS() {
    var wasmTextFile = 'noox_worker.wast';
    var wasmBinaryFile = 'noox_worker.wasm';
    var asmjsCodeFile = 'noox_worker.temp.asm.js';
    if (typeof Module['locateFile'] === 'function') {
      if (!isDataURI(wasmTextFile)) {
        wasmTextFile = Module['locateFile'](wasmTextFile);
      }
      if (!isDataURI(wasmBinaryFile)) {
        wasmBinaryFile = Module['locateFile'](wasmBinaryFile);
      }
      if (!isDataURI(asmjsCodeFile)) {
        asmjsCodeFile = Module['locateFile'](asmjsCodeFile);
      }
    }
    var wasmPageSize = 64 * 1024;
    var info = {
      global: null,
      env: null,
      asm2wasm: asm2wasmImports,
      parent: Module,
    };
    var exports = null;

    function mergeMemory(newBuffer) {
      var oldBuffer = Module['buffer'];
      if (newBuffer.byteLength < oldBuffer.byteLength) {
        err(
          'the new buffer in mergeMemory is smaller than the previous one. in native wasm, we should grow memory here',
        );
      }
      var oldView = new Int8Array(oldBuffer);
      var newView = new Int8Array(newBuffer);
      newView.set(oldView);
      updateGlobalBuffer(newBuffer);
      updateGlobalBufferViews();
    }

    function fixImports(imports) {
      return imports;
    }

    function getBinary() {
      try {
        if (Module['wasmBinary']) {
          return new Uint8Array(Module['wasmBinary']);
        }
        if (Module['readBinary']) {
          return Module['readBinary'](wasmBinaryFile);
        } else {
          throw "on the web, we need the wasm binary to be preloaded and set on Module['wasmBinary']. emcc.py will do that for you when generating HTML (but not JS)";
        }
      } catch (err) {
        abort(err);
      }
    }

    function getBinaryPromise() {
      if (
        !Module['wasmBinary'] &&
        (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) &&
        typeof fetch === 'function'
      ) {
        return fetch(wasmBinaryFile, {
          credentials: 'same-origin',
        })
          .then(function (response) {
            if (!response['ok']) {
              throw (
                "failed to load wasm binary file at '" + wasmBinaryFile + "'"
              );
            }
            return response['arrayBuffer']();
          })
          .catch(function () {
            return getBinary();
          });
      }
      return new Promise(function (resolve, reject) {
        resolve(getBinary());
      });
    }

    function doNativeWasm(global, env, providedBuffer) {
      if (typeof WebAssembly !== 'object') {
        err('no native wasm support detected');
        return false;
      }
      if (!(Module['wasmMemory'] instanceof WebAssembly.Memory)) {
        err('no native wasm Memory in use');
        return false;
      }
      env['memory'] = Module['wasmMemory'];
      info['global'] = {
        NaN: NaN,
        Infinity: Infinity,
      };
      info['global.Math'] = Math;
      info['env'] = env;

      function receiveInstance(instance, module) {
        exports = instance.exports;
        if (exports.memory) mergeMemory(exports.memory);
        Module['asm'] = exports;
        Module['usingWasm'] = true;
        removeRunDependency('wasm-instantiate');
      }
      addRunDependency('wasm-instantiate');
      if (Module['instantiateWasm']) {
        try {
          return Module['instantiateWasm'](info, receiveInstance);
        } catch (e) {
          err('Module.instantiateWasm callback failed with error: ' + e);
          return false;
        }
      }

      function receiveInstantiatedSource(output) {
        receiveInstance(output['instance'], output['module']);
      }

      async function instantiateArrayBuffer(receiver) {
        getBinaryPromise()
          .then(function (binary) {
            return WebAssembly.instantiate(binary, info);
          })
          .then(function (value) {
            receiver(value);
          })
          .catch(function (reason) {
            err('failed to asynchronously prepare wasm: ' + reason);
            abort(reason);
          });
      }
      if (
        !Module['wasmBinary'] &&
        typeof WebAssembly.instantiateStreaming === 'function' &&
        !isDataURI(wasmBinaryFile) &&
        typeof fetch === 'function'
      ) {
        WebAssembly.instantiateStreaming(
          fetch(wasmBinaryFile, {
            credentials: 'same-origin',
          }),
          info,
        )
          .then(receiveInstantiatedSource)
          .catch(function (reason) {
            err('wasm streaming compile failed: ' + reason);
            err('falling back to ArrayBuffer instantiation');
            instantiateArrayBuffer(receiveInstantiatedSource);
          });
      } else {
        instantiateArrayBuffer(receiveInstantiatedSource);
      }
      return {};
    }
    Module['asmPreload'] = Module['asm'];
    var asmjsReallocBuffer = Module['reallocBuffer'];
    var wasmReallocBuffer = function (size) {
      var PAGE_MULTIPLE = Module['usingWasm']
        ? WASM_PAGE_SIZE
        : ASMJS_PAGE_SIZE;
      size = alignUp(size, PAGE_MULTIPLE);
      var old = Module['buffer'];
      var oldSize = old.byteLength;
      if (Module['usingWasm']) {
        try {
          var result = Module['wasmMemory'].grow(
            (size - oldSize) / wasmPageSize,
          );
          if (result !== (-1 | 0)) {
            return (Module['buffer'] = Module['wasmMemory'].buffer);
          } else {
            return null;
          }
        } catch (e) {
          return null;
        }
      }
    };
    Module['reallocBuffer'] = function (size) {
      if (finalMethod === 'asmjs') {
        return asmjsReallocBuffer(size);
      } else {
        return wasmReallocBuffer(size);
      }
    };
    var finalMethod = '';
    Module['asm'] = function (global, env, providedBuffer) {
      env = fixImports(env);
      if (!env['table']) {
        var TABLE_SIZE = Module['wasmTableSize'];
        if (TABLE_SIZE === undefined) TABLE_SIZE = 1024;
        var MAX_TABLE_SIZE = Module['wasmMaxTableSize'];
        if (
          typeof WebAssembly === 'object' &&
          typeof WebAssembly.Table === 'function'
        ) {
          if (MAX_TABLE_SIZE !== undefined) {
            env['table'] = new WebAssembly.Table({
              initial: TABLE_SIZE,
              maximum: MAX_TABLE_SIZE,
              element: 'anyfunc',
            });
          } else {
            env['table'] = new WebAssembly.Table({
              initial: TABLE_SIZE,
              element: 'anyfunc',
            });
          }
        } else {
          env['table'] = new Array(TABLE_SIZE);
        }
        Module['wasmTable'] = env['table'];
      }
      if (!env['memoryBase']) {
        env['memoryBase'] = Module['STATIC_BASE'];
      }
      if (!env['tableBase']) {
        env['tableBase'] = 0;
      }
      var exports;
      exports = doNativeWasm(global, env, providedBuffer);
      assert(exports, 'no binaryen method succeeded.');
      return exports;
    };
  }
  integrateWasmJS();
  var ASM_CONSTS = [
    function () {
      throw 'SimulateInfiniteLoop';
    },
    function ($0, $1) {
      var level = $0;
      var msg = Pointer_stringify($1);
      if (Module['ndSyslog']) {
        Module['ndSyslog'](level, msg);
      }
    },
    function () {
      return 'NODE' !== Module['ENVIRONMENT'];
    },
    function () {
      return 'WORKER' !== Module['ENVIRONMENT'];
    },
    function ($0, $1) {
      var origin = Pointer_stringify($0, $1);
      var _origin = self.location.origin;
      var ret = origin === _origin ? 1 : 0;
      return ret;
    },
    function () {
      if (ENVIRONMENT_IS_WORKER && self.performance) {
        self.performance.mark('page-layout-start');
      }
    },
    function ($0) {
      if (
        ENVIRONMENT_IS_WORKER &&
        self.performance &&
        Module &&
        Module['console'] &&
        Module['console']['debug']
      ) {
        self.performance.mark('page-layout-end');
        self.performance.measure(
          'page-layout',
          'page-layout-start',
          'page-layout-end',
        );
        const duration_ms =
          performance.getEntriesByName('page-layout')[0].duration;
        Module['console']['debug'](
          'layout page ' + $0 + ' [' + duration_ms + ' ms]',
        );
        self.performance.clearMarks('page-layout-start');
        self.performance.clearMarks('page-layout-end');
        self.performance.clearMeasures('page-layout');
      }
    },
    function ($0, $1) {
      var resource = Module['ND_LICENSE_URL'];
      if (resource) {
        stringToUTF8(resource, $0, $1);
      }
    },
    function ($0, $1, $2) {
      var resourceName = Pointer_stringify($0);
      const resource = Module['readResource'](resourceName);
      if (resource && $2 == resource.length) {
        HEAP8.set(resource, $1);
        return resource.length;
      } else {
        return 0;
      }
    },
    function () {
      return Math.floor(Math.random() * 256);
    },
  ];

  function _emscripten_asm_const_i(code) {
    return ASM_CONSTS[code]();
  }

  function _emscripten_asm_const_ii(code, a0) {
    return ASM_CONSTS[code](a0);
  }

  function _emscripten_asm_const_iii(code, a0, a1) {
    return ASM_CONSTS[code](a0, a1);
  }

  function _emscripten_asm_const_iiii(code, a0, a1, a2) {
    return ASM_CONSTS[code](a0, a1, a2);
  }
  STATIC_BASE = GLOBAL_BASE;
  STATICTOP = STATIC_BASE + 47518720;
  __ATINIT__.push(
    {
      func: function () {
        __GLOBAL__I_000101();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkPDFGraphicState_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_iostream_cpp();
      },
    },
    {
      func: function () {
        ___emscripten_environ_constructor();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkDiscardableMemoryPool_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkResourceCache_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkPicture_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkMiniRecorder_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkPathRef_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkEventTracer_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_pfontpool_skia_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkPDFShader_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkImageDecoder_libjpeg_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkImageFilter_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkFontMgr_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkTypeface_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkXfermode_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkGlyphCache_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkData_cpp();
      },
    },
    {
      func: function () {
        __GLOBAL__sub_I_SkOpts_cpp();
      },
    },
  );
  var STATIC_BUMP = 47518720;
  Module['STATIC_BASE'] = STATIC_BASE;
  Module['STATIC_BUMP'] = STATIC_BUMP;
  STATICTOP += 16;
  var ENV = {};

  function ___buildEnvironment(environ) {
    var MAX_ENV_VALUES = 64;
    var TOTAL_ENV_SIZE = 1024;
    var poolPtr;
    var envPtr;
    if (!___buildEnvironment.called) {
      ___buildEnvironment.called = true;
      ENV['USER'] = ENV['LOGNAME'] = 'web_user';
      ENV['PATH'] = '/';
      ENV['PWD'] = '/';
      ENV['HOME'] = '/home/web_user';
      ENV['LANG'] = 'C.UTF-8';
      ENV['_'] = Module['thisProgram'];
      poolPtr = getMemory(TOTAL_ENV_SIZE);
      envPtr = getMemory(MAX_ENV_VALUES * 4);
      HEAP32[envPtr >> 2] = poolPtr;
      HEAP32[environ >> 2] = envPtr;
    } else {
      envPtr = HEAP32[environ >> 2];
      poolPtr = HEAP32[envPtr >> 2];
    }
    var strings = [];
    var totalSize = 0;
    for (var key in ENV) {
      if (typeof ENV[key] === 'string') {
        var line = key + '=' + ENV[key];
        strings.push(line);
        totalSize += line.length;
      }
    }
    if (totalSize > TOTAL_ENV_SIZE) {
      throw new Error('Environment size exceeded TOTAL_ENV_SIZE!');
    }
    var ptrSize = 4;
    for (var i = 0; i < strings.length; i++) {
      var line = strings[i];
      writeAsciiToMemory(line, poolPtr);
      HEAP32[(envPtr + i * ptrSize) >> 2] = poolPtr;
      poolPtr += line.length + 1;
    }
    HEAP32[(envPtr + strings.length * ptrSize) >> 2] = 0;
  }

  function ___cxa_pure_virtual() {
    ABORT = true;
    throw 'Pure virtual function called!';
  }

  function ___cxa_uncaught_exception() {
    return !!__ZSt18uncaught_exceptionv.uncaught_exception;
  }

  function ___lock() {}
  var ERRNO_CODES = {
    EPERM: 1,
    ENOENT: 2,
    ESRCH: 3,
    EINTR: 4,
    EIO: 5,
    ENXIO: 6,
    E2BIG: 7,
    ENOEXEC: 8,
    EBADF: 9,
    ECHILD: 10,
    EAGAIN: 11,
    EWOULDBLOCK: 11,
    ENOMEM: 12,
    EACCES: 13,
    EFAULT: 14,
    ENOTBLK: 15,
    EBUSY: 16,
    EEXIST: 17,
    EXDEV: 18,
    ENODEV: 19,
    ENOTDIR: 20,
    EISDIR: 21,
    EINVAL: 22,
    ENFILE: 23,
    EMFILE: 24,
    ENOTTY: 25,
    ETXTBSY: 26,
    EFBIG: 27,
    ENOSPC: 28,
    ESPIPE: 29,
    EROFS: 30,
    EMLINK: 31,
    EPIPE: 32,
    EDOM: 33,
    ERANGE: 34,
    ENOMSG: 42,
    EIDRM: 43,
    ECHRNG: 44,
    EL2NSYNC: 45,
    EL3HLT: 46,
    EL3RST: 47,
    ELNRNG: 48,
    EUNATCH: 49,
    ENOCSI: 50,
    EL2HLT: 51,
    EDEADLK: 35,
    ENOLCK: 37,
    EBADE: 52,
    EBADR: 53,
    EXFULL: 54,
    ENOANO: 55,
    EBADRQC: 56,
    EBADSLT: 57,
    EDEADLOCK: 35,
    EBFONT: 59,
    ENOSTR: 60,
    ENODATA: 61,
    ETIME: 62,
    ENOSR: 63,
    ENONET: 64,
    ENOPKG: 65,
    EREMOTE: 66,
    ENOLINK: 67,
    EADV: 68,
    ESRMNT: 69,
    ECOMM: 70,
    EPROTO: 71,
    EMULTIHOP: 72,
    EDOTDOT: 73,
    EBADMSG: 74,
    ENOTUNIQ: 76,
    EBADFD: 77,
    EREMCHG: 78,
    ELIBACC: 79,
    ELIBBAD: 80,
    ELIBSCN: 81,
    ELIBMAX: 82,
    ELIBEXEC: 83,
    ENOSYS: 38,
    ENOTEMPTY: 39,
    ENAMETOOLONG: 36,
    ELOOP: 40,
    EOPNOTSUPP: 95,
    EPFNOSUPPORT: 96,
    ECONNRESET: 104,
    ENOBUFS: 105,
    EAFNOSUPPORT: 97,
    EPROTOTYPE: 91,
    ENOTSOCK: 88,
    ENOPROTOOPT: 92,
    ESHUTDOWN: 108,
    ECONNREFUSED: 111,
    EADDRINUSE: 98,
    ECONNABORTED: 103,
    ENETUNREACH: 101,
    ENETDOWN: 100,
    ETIMEDOUT: 110,
    EHOSTDOWN: 112,
    EHOSTUNREACH: 113,
    EINPROGRESS: 115,
    EALREADY: 114,
    EDESTADDRREQ: 89,
    EMSGSIZE: 90,
    EPROTONOSUPPORT: 93,
    ESOCKTNOSUPPORT: 94,
    EADDRNOTAVAIL: 99,
    ENETRESET: 102,
    EISCONN: 106,
    ENOTCONN: 107,
    ETOOMANYREFS: 109,
    EUSERS: 87,
    EDQUOT: 122,
    ESTALE: 116,
    ENOTSUP: 95,
    ENOMEDIUM: 123,
    EILSEQ: 84,
    EOVERFLOW: 75,
    ECANCELED: 125,
    ENOTRECOVERABLE: 131,
    EOWNERDEAD: 130,
    ESTRPIPE: 86,
  };

  function ___setErrNo(value) {
    if (Module['___errno_location'])
      HEAP32[Module['___errno_location']() >> 2] = value;
    return value;
  }

  function ___map_file(pathname, size) {
    ___setErrNo(ERRNO_CODES.EPERM);
    return -1;
  }
  var SOCKFS = undefined;

  function __inet_pton4_raw(str) {
    var b = str.split('.');
    for (var i = 0; i < 4; i++) {
      var tmp = Number(b[i]);
      if (isNaN(tmp)) return null;
      b[i] = tmp;
    }
    return (b[0] | (b[1] << 8) | (b[2] << 16) | (b[3] << 24)) >>> 0;
  }

  function __inet_pton6_raw(str) {
    var words;
    var w, offset, z;
    var valid6regx =
      /^((?=.*::)(?!.*::.+::)(::)?([\dA-F]{1,4}:(:|\b)|){5}|([\dA-F]{1,4}:){6})((([\dA-F]{1,4}((?!\3)::|:\b|$))|(?!\2\3)){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})$/i;
    var parts = [];
    if (!valid6regx.test(str)) {
      return null;
    }
    if (str === '::') {
      return [0, 0, 0, 0, 0, 0, 0, 0];
    }
    if (str.indexOf('::') === 0) {
      str = str.replace('::', 'Z:');
    } else {
      str = str.replace('::', ':Z:');
    }
    if (str.indexOf('.') > 0) {
      str = str.replace(new RegExp('[.]', 'g'), ':');
      words = str.split(':');
      words[words.length - 4] =
        parseInt(words[words.length - 4]) +
        parseInt(words[words.length - 3]) * 256;
      words[words.length - 3] =
        parseInt(words[words.length - 2]) +
        parseInt(words[words.length - 1]) * 256;
      words = words.slice(0, words.length - 2);
    } else {
      words = str.split(':');
    }
    offset = 0;
    z = 0;
    for (w = 0; w < words.length; w++) {
      if (typeof words[w] === 'string') {
        if (words[w] === 'Z') {
          for (z = 0; z < 8 - words.length + 1; z++) {
            parts[w + z] = 0;
          }
          offset = z - 1;
        } else {
          parts[w + offset] = _htons(parseInt(words[w], 16));
        }
      } else {
        parts[w + offset] = words[w];
      }
    }
    return [
      (parts[1] << 16) | parts[0],
      (parts[3] << 16) | parts[2],
      (parts[5] << 16) | parts[4],
      (parts[7] << 16) | parts[6],
    ];
  }
  var DNS = {
    address_map: {
      id: 1,
      addrs: {},
      names: {},
    },
    lookup_name: function (name) {
      var res = __inet_pton4_raw(name);
      if (res !== null) {
        return name;
      }
      res = __inet_pton6_raw(name);
      if (res !== null) {
        return name;
      }
      var addr;
      if (DNS.address_map.addrs[name]) {
        addr = DNS.address_map.addrs[name];
      } else {
        var id = DNS.address_map.id++;
        assert(id < 65535, 'exceeded max address mappings of 65535');
        addr = '172.29.' + (id & 255) + '.' + (id & 65280);
        DNS.address_map.names[addr] = name;
        DNS.address_map.addrs[name] = addr;
      }
      return addr;
    },
    lookup_addr: function (addr) {
      if (DNS.address_map.names[addr]) {
        return DNS.address_map.names[addr];
      }
      return null;
    },
  };

  function __inet_ntop4_raw(addr) {
    return (
      (addr & 255) +
      '.' +
      ((addr >> 8) & 255) +
      '.' +
      ((addr >> 16) & 255) +
      '.' +
      ((addr >> 24) & 255)
    );
  }

  function __inet_ntop6_raw(ints) {
    var str = '';
    var word = 0;
    var longest = 0;
    var lastzero = 0;
    var zstart = 0;
    var len = 0;
    var i = 0;
    var parts = [
      ints[0] & 65535,
      ints[0] >> 16,
      ints[1] & 65535,
      ints[1] >> 16,
      ints[2] & 65535,
      ints[2] >> 16,
      ints[3] & 65535,
      ints[3] >> 16,
    ];
    var hasipv4 = true;
    var v4part = '';
    for (i = 0; i < 5; i++) {
      if (parts[i] !== 0) {
        hasipv4 = false;
        break;
      }
    }
    if (hasipv4) {
      v4part = __inet_ntop4_raw(parts[6] | (parts[7] << 16));
      if (parts[5] === -1) {
        str = '::ffff:';
        str += v4part;
        return str;
      }
      if (parts[5] === 0) {
        str = '::';
        if (v4part === '0.0.0.0') v4part = '';
        if (v4part === '0.0.0.1') v4part = '1';
        str += v4part;
        return str;
      }
    }
    for (word = 0; word < 8; word++) {
      if (parts[word] === 0) {
        if (word - lastzero > 1) {
          len = 0;
        }
        lastzero = word;
        len++;
      }
      if (len > longest) {
        longest = len;
        zstart = word - longest + 1;
      }
    }
    for (word = 0; word < 8; word++) {
      if (longest > 1) {
        if (parts[word] === 0 && word >= zstart && word < zstart + longest) {
          if (word === zstart) {
            str += ':';
            if (zstart === 0) str += ':';
          }
          continue;
        }
      }
      str += Number(_ntohs(parts[word] & 65535)).toString(16);
      str += word < 7 ? ':' : '';
    }
    return str;
  }

  function __read_sockaddr(sa, salen) {
    var family = HEAP16[sa >> 1];
    var port = _ntohs(HEAP16[(sa + 2) >> 1]);
    var addr;
    switch (family) {
      case 2:
        if (salen !== 16) {
          return {
            errno: ERRNO_CODES.EINVAL,
          };
        }
        addr = HEAP32[(sa + 4) >> 2];
        addr = __inet_ntop4_raw(addr);
        break;
      case 10:
        if (salen !== 28) {
          return {
            errno: ERRNO_CODES.EINVAL,
          };
        }
        addr = [
          HEAP32[(sa + 8) >> 2],
          HEAP32[(sa + 12) >> 2],
          HEAP32[(sa + 16) >> 2],
          HEAP32[(sa + 20) >> 2],
        ];
        addr = __inet_ntop6_raw(addr);
        break;
      default:
        return {
          errno: ERRNO_CODES.EAFNOSUPPORT,
        };
    }
    return {
      family: family,
      addr: addr,
      port: port,
    };
  }

  function __write_sockaddr(sa, family, addr, port) {
    switch (family) {
      case 2:
        addr = __inet_pton4_raw(addr);
        HEAP16[sa >> 1] = family;
        HEAP32[(sa + 4) >> 2] = addr;
        HEAP16[(sa + 2) >> 1] = _htons(port);
        break;
      case 10:
        addr = __inet_pton6_raw(addr);
        HEAP32[sa >> 2] = family;
        HEAP32[(sa + 8) >> 2] = addr[0];
        HEAP32[(sa + 12) >> 2] = addr[1];
        HEAP32[(sa + 16) >> 2] = addr[2];
        HEAP32[(sa + 20) >> 2] = addr[3];
        HEAP16[(sa + 2) >> 1] = _htons(port);
        HEAP32[(sa + 4) >> 2] = 0;
        HEAP32[(sa + 24) >> 2] = 0;
        break;
      default:
        return {
          errno: ERRNO_CODES.EAFNOSUPPORT,
        };
    }
    return {};
  }
  var SYSCALLS = {
    varargs: 0,
    get: function (varargs) {
      SYSCALLS.varargs += 4;
      var ret = HEAP32[(SYSCALLS.varargs - 4) >> 2];
      return ret;
    },
    getStr: function () {
      var ret = Pointer_stringify(SYSCALLS.get());
      return ret;
    },
    get64: function () {
      var low = SYSCALLS.get(),
        high = SYSCALLS.get();
      if (low >= 0) assert(high === 0);
      else assert(high === -1);
      return low;
    },
    getZero: function () {
      assert(SYSCALLS.get() === 0);
    },
  };

  function ___syscall102(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var call = SYSCALLS.get(),
        socketvararg = SYSCALLS.get();
      SYSCALLS.varargs = socketvararg;
      switch (call) {
        case 1: {
          var domain = SYSCALLS.get(),
            type = SYSCALLS.get(),
            protocol = SYSCALLS.get();
          var sock = SOCKFS.createSocket(domain, type, protocol);
          assert(sock.stream.fd < 64);
          return sock.stream.fd;
        }
        case 2: {
          var sock = SYSCALLS.getSocketFromFD(),
            info = SYSCALLS.getSocketAddress();
          sock.sock_ops.bind(sock, info.addr, info.port);
          return 0;
        }
        case 3: {
          var sock = SYSCALLS.getSocketFromFD(),
            info = SYSCALLS.getSocketAddress();
          sock.sock_ops.connect(sock, info.addr, info.port);
          return 0;
        }
        case 4: {
          var sock = SYSCALLS.getSocketFromFD(),
            backlog = SYSCALLS.get();
          sock.sock_ops.listen(sock, backlog);
          return 0;
        }
        case 5: {
          var sock = SYSCALLS.getSocketFromFD(),
            addr = SYSCALLS.get(),
            addrlen = SYSCALLS.get();
          var newsock = sock.sock_ops.accept(sock);
          if (addr) {
            var res = __write_sockaddr(
              addr,
              newsock.family,
              DNS.lookup_name(newsock.daddr),
              newsock.dport,
            );
            assert(!res.errno);
          }
          return newsock.stream.fd;
        }
        case 6: {
          var sock = SYSCALLS.getSocketFromFD(),
            addr = SYSCALLS.get(),
            addrlen = SYSCALLS.get();
          var res = __write_sockaddr(
            addr,
            sock.family,
            DNS.lookup_name(sock.saddr || '0.0.0.0'),
            sock.sport,
          );
          assert(!res.errno);
          return 0;
        }
        case 7: {
          var sock = SYSCALLS.getSocketFromFD(),
            addr = SYSCALLS.get(),
            addrlen = SYSCALLS.get();
          if (!sock.daddr) {
            return -ERRNO_CODES.ENOTCONN;
          }
          var res = __write_sockaddr(
            addr,
            sock.family,
            DNS.lookup_name(sock.daddr),
            sock.dport,
          );
          assert(!res.errno);
          return 0;
        }
        case 11: {
          var sock = SYSCALLS.getSocketFromFD(),
            message = SYSCALLS.get(),
            length = SYSCALLS.get(),
            flags = SYSCALLS.get(),
            dest = SYSCALLS.getSocketAddress(true);
          if (!dest) {
            return FS.write(sock.stream, HEAP8, message, length);
          } else {
            return sock.sock_ops.sendmsg(
              sock,
              HEAP8,
              message,
              length,
              dest.addr,
              dest.port,
            );
          }
        }
        case 12: {
          var sock = SYSCALLS.getSocketFromFD(),
            buf = SYSCALLS.get(),
            len = SYSCALLS.get(),
            flags = SYSCALLS.get(),
            addr = SYSCALLS.get(),
            addrlen = SYSCALLS.get();
          var msg = sock.sock_ops.recvmsg(sock, len);
          if (!msg) return 0;
          if (addr) {
            var res = __write_sockaddr(
              addr,
              sock.family,
              DNS.lookup_name(msg.addr),
              msg.port,
            );
            assert(!res.errno);
          }
          HEAPU8.set(msg.buffer, buf);
          return msg.buffer.byteLength;
        }
        case 14: {
          return -ERRNO_CODES.ENOPROTOOPT;
        }
        case 15: {
          var sock = SYSCALLS.getSocketFromFD(),
            level = SYSCALLS.get(),
            optname = SYSCALLS.get(),
            optval = SYSCALLS.get(),
            optlen = SYSCALLS.get();
          if (level === 1) {
            if (optname === 4) {
              HEAP32[optval >> 2] = sock.error;
              HEAP32[optlen >> 2] = 4;
              sock.error = null;
              return 0;
            }
          }
          return -ERRNO_CODES.ENOPROTOOPT;
        }
        case 16: {
          var sock = SYSCALLS.getSocketFromFD(),
            message = SYSCALLS.get(),
            flags = SYSCALLS.get();
          var iov = HEAP32[(message + 8) >> 2];
          var num = HEAP32[(message + 12) >> 2];
          var addr, port;
          var name = HEAP32[message >> 2];
          var namelen = HEAP32[(message + 4) >> 2];
          if (name) {
            var info = __read_sockaddr(name, namelen);
            if (info.errno) return -info.errno;
            port = info.port;
            addr = DNS.lookup_addr(info.addr) || info.addr;
          }
          var total = 0;
          for (var i = 0; i < num; i++) {
            total += HEAP32[(iov + (8 * i + 4)) >> 2];
          }
          var view = new Uint8Array(total);
          var offset = 0;
          for (var i = 0; i < num; i++) {
            var iovbase = HEAP32[(iov + (8 * i + 0)) >> 2];
            var iovlen = HEAP32[(iov + (8 * i + 4)) >> 2];
            for (var j = 0; j < iovlen; j++) {
              view[offset++] = HEAP8[(iovbase + j) >> 0];
            }
          }
          return sock.sock_ops.sendmsg(sock, view, 0, total, addr, port);
        }
        case 17: {
          var sock = SYSCALLS.getSocketFromFD(),
            message = SYSCALLS.get(),
            flags = SYSCALLS.get();
          var iov = HEAP32[(message + 8) >> 2];
          var num = HEAP32[(message + 12) >> 2];
          var total = 0;
          for (var i = 0; i < num; i++) {
            total += HEAP32[(iov + (8 * i + 4)) >> 2];
          }
          var msg = sock.sock_ops.recvmsg(sock, total);
          if (!msg) return 0;
          var name = HEAP32[message >> 2];
          if (name) {
            var res = __write_sockaddr(
              name,
              sock.family,
              DNS.lookup_name(msg.addr),
              msg.port,
            );
            assert(!res.errno);
          }
          var bytesRead = 0;
          var bytesRemaining = msg.buffer.byteLength;
          for (var i = 0; bytesRemaining > 0 && i < num; i++) {
            var iovbase = HEAP32[(iov + (8 * i + 0)) >> 2];
            var iovlen = HEAP32[(iov + (8 * i + 4)) >> 2];
            if (!iovlen) {
              continue;
            }
            var length = Math.min(iovlen, bytesRemaining);
            var buf = msg.buffer.subarray(bytesRead, bytesRead + length);
            HEAPU8.set(buf, iovbase + bytesRead);
            bytesRead += length;
            bytesRemaining -= length;
          }
          return bytesRead;
        }
        default:
          abort('unsupported socketcall syscall ' + call);
      }
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall118(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD();
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall125(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall140(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD(),
        offset_high = SYSCALLS.get(),
        offset_low = SYSCALLS.get(),
        result = SYSCALLS.get(),
        whence = SYSCALLS.get();
      var offset = offset_low;
      FS.llseek(stream, offset, whence);
      HEAP32[result >> 2] = stream.position;
      if (stream.getdents && offset === 0 && whence === 0)
        stream.getdents = null;
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall145(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD(),
        iov = SYSCALLS.get(),
        iovcnt = SYSCALLS.get();
      return SYSCALLS.doReadv(stream, iov, iovcnt);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall146(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.get(),
        iov = SYSCALLS.get(),
        iovcnt = SYSCALLS.get();
      var ret = 0;
      if (!___syscall146.buffers) {
        ___syscall146.buffers = [null, [], []];
        ___syscall146.printChar = function (stream, curr) {
          var buffer = ___syscall146.buffers[stream];
          assert(buffer);
          if (curr === 0 || curr === 10) {
            (stream === 1 ? out : err)(UTF8ArrayToString(buffer, 0));
            buffer.length = 0;
          } else {
            buffer.push(curr);
          }
        };
      }
      for (var i = 0; i < iovcnt; i++) {
        var ptr = HEAP32[(iov + i * 8) >> 2];
        var len = HEAP32[(iov + (i * 8 + 4)) >> 2];
        for (var j = 0; j < len; j++) {
          ___syscall146.printChar(stream, HEAPU8[ptr + j]);
        }
        ret += len;
      }
      return ret;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall183(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var buf = SYSCALLS.get(),
        size = SYSCALLS.get();
      if (size === 0) return -ERRNO_CODES.EINVAL;
      var cwd = FS.cwd();
      var cwdLengthInBytes = lengthBytesUTF8(cwd);
      if (size < cwdLengthInBytes + 1) return -ERRNO_CODES.ERANGE;
      stringToUTF8(cwd, buf, size);
      return buf;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall192(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var addr = SYSCALLS.get(),
        len = SYSCALLS.get(),
        prot = SYSCALLS.get(),
        flags = SYSCALLS.get(),
        fd = SYSCALLS.get(),
        off = SYSCALLS.get();
      off <<= 12;
      var ptr;
      var allocated = false;
      if (fd === -1) {
        ptr = _memalign(PAGE_SIZE, len);
        if (!ptr) return -ERRNO_CODES.ENOMEM;
        _memset(ptr, 0, len);
        allocated = true;
      } else {
        var info = FS.getStream(fd);
        if (!info) return -ERRNO_CODES.EBADF;
        var res = FS.mmap(info, HEAPU8, addr, len, off, prot, flags);
        ptr = res.ptr;
        allocated = res.allocated;
      }
      SYSCALLS.mappings[ptr] = {
        malloc: ptr,
        len: len,
        allocated: allocated,
        fd: fd,
        flags: flags,
      };
      return ptr;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall194(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var fd = SYSCALLS.get(),
        zero = SYSCALLS.getZero(),
        length = SYSCALLS.get64();
      FS.ftruncate(fd, length);
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall195(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var path = SYSCALLS.getStr(),
        buf = SYSCALLS.get();
      return SYSCALLS.doStat(FS.stat, path, buf);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall197(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD(),
        buf = SYSCALLS.get();
      return SYSCALLS.doStat(FS.stat, stream.path, buf);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }
  var PROCINFO = {
    ppid: 1,
    pid: 42,
    sid: 42,
    pgid: 42,
  };

  function ___syscall20(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      return PROCINFO.pid;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall221(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall3(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD(),
        buf = SYSCALLS.get(),
        count = SYSCALLS.get();
      return FS.read(stream, HEAP8, buf, count);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall33(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var path = SYSCALLS.getStr(),
        amode = SYSCALLS.get();
      return SYSCALLS.doAccess(path, amode);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall4(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD(),
        buf = SYSCALLS.get(),
        count = SYSCALLS.get();
      return FS.write(stream, HEAP8, buf, count);
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall41(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var old = SYSCALLS.getStreamFromFD();
      return FS.open(old.path, old.flags, 0).fd;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall5(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var pathname = SYSCALLS.getStr(),
        flags = SYSCALLS.get(),
        mode = SYSCALLS.get();
      var stream = FS.open(pathname, flags, mode);
      return stream.fd;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall54(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall6(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var stream = SYSCALLS.getStreamFromFD();
      FS.close(stream);
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall77(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var who = SYSCALLS.get(),
        usage = SYSCALLS.get();
      _memset(usage, 0, 136);
      HEAP32[usage >> 2] = 1;
      HEAP32[(usage + 4) >> 2] = 2;
      HEAP32[(usage + 8) >> 2] = 3;
      HEAP32[(usage + 12) >> 2] = 4;
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___syscall91(which, varargs) {
    SYSCALLS.varargs = varargs;
    try {
      var addr = SYSCALLS.get(),
        len = SYSCALLS.get();
      var info = SYSCALLS.mappings[addr];
      if (!info) return 0;
      if (len === info.len) {
        var stream = FS.getStream(info.fd);
        SYSCALLS.doMsync(addr, stream, len, info.flags);
        FS.munmap(stream);
        SYSCALLS.mappings[addr] = null;
        if (info.allocated) {
          _free(info.malloc);
        }
      }
      return 0;
    } catch (e) {
      if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) abort(e);
      return -e.errno;
    }
  }

  function ___unlock() {}

  function _abort() {
    Module['abort']();
  }

  function _atexit(func, arg) {
    __ATEXIT__.unshift({
      func: func,
      arg: arg,
    });
  }

  function _emscripten_get_now() {
    abort();
  }

  function _emscripten_get_now_is_monotonic() {
    return (
      ENVIRONMENT_IS_NODE ||
      typeof dateNow !== 'undefined' ||
      ((ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) &&
        self['performance'] &&
        self['performance']['now'])
    );
  }

  function _clock_gettime(clk_id, tp) {
    var now;
    if (clk_id === 0) {
      now = Date.now();
    } else if (clk_id === 1 && _emscripten_get_now_is_monotonic()) {
      now = _emscripten_get_now();
    } else {
      ___setErrNo(ERRNO_CODES.EINVAL);
      return -1;
    }
    HEAP32[tp >> 2] = (now / 1e3) | 0;
    HEAP32[(tp + 4) >> 2] = ((now % 1e3) * 1e3 * 1e3) | 0;
    return 0;
  }

  function _difftime(time1, time0) {
    return time1 - time0;
  }

  function __exit(status) {
    exit(status);
  }

  function _exit(status) {
    __exit(status);
  }

  function _getenv(name) {
    if (name === 0) return 0;
    name = Pointer_stringify(name);
    if (!ENV.hasOwnProperty(name)) return 0;
    if (_getenv.ret) _free(_getenv.ret);
    _getenv.ret = allocateUTF8(ENV[name]);
    return _getenv.ret;
  }

  function _gettimeofday(ptr) {
    var now = Date.now();
    HEAP32[ptr >> 2] = (now / 1e3) | 0;
    HEAP32[(ptr + 4) >> 2] = ((now % 1e3) * 1e3) | 0;
    return 0;
  }
  var ___tm_current = STATICTOP;
  STATICTOP += 48;
  var ___tm_timezone = allocate(intArrayFromString('GMT'), 'i8', ALLOC_STATIC);

  function _gmtime_r(time, tmPtr) {
    var date = new Date(HEAP32[time >> 2] * 1e3);
    HEAP32[tmPtr >> 2] = date.getUTCSeconds();
    HEAP32[(tmPtr + 4) >> 2] = date.getUTCMinutes();
    HEAP32[(tmPtr + 8) >> 2] = date.getUTCHours();
    HEAP32[(tmPtr + 12) >> 2] = date.getUTCDate();
    HEAP32[(tmPtr + 16) >> 2] = date.getUTCMonth();
    HEAP32[(tmPtr + 20) >> 2] = date.getUTCFullYear() - 1900;
    HEAP32[(tmPtr + 24) >> 2] = date.getUTCDay();
    HEAP32[(tmPtr + 36) >> 2] = 0;
    HEAP32[(tmPtr + 32) >> 2] = 0;
    var start = Date.UTC(date.getUTCFullYear(), 0, 1, 0, 0, 0, 0);
    var yday = ((date.getTime() - start) / (1e3 * 60 * 60 * 24)) | 0;
    HEAP32[(tmPtr + 28) >> 2] = yday;
    HEAP32[(tmPtr + 40) >> 2] = ___tm_timezone;
    return tmPtr;
  }

  function _gmtime(time) {
    return _gmtime_r(time, ___tm_current);
  }

  function _llvm_log2_f32(x) {
    return Math.log(x) / Math.LN2;
  }

  function _llvm_stackrestore(p) {
    var self = _llvm_stacksave;
    var ret = self.LLVM_SAVEDSTACKS[p];
    self.LLVM_SAVEDSTACKS.splice(p, 1);
    stackRestore(ret);
  }

  function _llvm_stacksave() {
    var self = _llvm_stacksave;
    if (!self.LLVM_SAVEDSTACKS) {
      self.LLVM_SAVEDSTACKS = [];
    }
    self.LLVM_SAVEDSTACKS.push(stackSave());
    return self.LLVM_SAVEDSTACKS.length - 1;
  }

  function _llvm_trap() {
    abort('trap!');
  }
  var _llvm_trunc_f32 = Math_trunc;

  function _tzset() {
    if (_tzset.called) return;
    _tzset.called = true;
    HEAP32[__get_timezone() >> 2] = new Date().getTimezoneOffset() * 60;
    var winter = new Date(2e3, 0, 1);
    var summer = new Date(2e3, 6, 1);
    HEAP32[__get_daylight() >> 2] = Number(
      winter.getTimezoneOffset() != summer.getTimezoneOffset(),
    );

    function extractZone(date) {
      var match = date.toTimeString().match(/\(([A-Za-z ]+)\)$/);
      return match ? match[1] : 'GMT';
    }
    var winterName = extractZone(winter);
    var summerName = extractZone(summer);
    var winterNamePtr = allocate(
      intArrayFromString(winterName),
      'i8',
      ALLOC_NORMAL,
    );
    var summerNamePtr = allocate(
      intArrayFromString(summerName),
      'i8',
      ALLOC_NORMAL,
    );
    if (summer.getTimezoneOffset() < winter.getTimezoneOffset()) {
      HEAP32[__get_tzname() >> 2] = winterNamePtr;
      HEAP32[(__get_tzname() + 4) >> 2] = summerNamePtr;
    } else {
      HEAP32[__get_tzname() >> 2] = summerNamePtr;
      HEAP32[(__get_tzname() + 4) >> 2] = winterNamePtr;
    }
  }

  function _localtime_r(time, tmPtr) {
    _tzset();
    var date = new Date(HEAP32[time >> 2] * 1e3);
    HEAP32[tmPtr >> 2] = date.getSeconds();
    HEAP32[(tmPtr + 4) >> 2] = date.getMinutes();
    HEAP32[(tmPtr + 8) >> 2] = date.getHours();
    HEAP32[(tmPtr + 12) >> 2] = date.getDate();
    HEAP32[(tmPtr + 16) >> 2] = date.getMonth();
    HEAP32[(tmPtr + 20) >> 2] = date.getFullYear() - 1900;
    HEAP32[(tmPtr + 24) >> 2] = date.getDay();
    var start = new Date(date.getFullYear(), 0, 1);
    var yday = ((date.getTime() - start.getTime()) / (1e3 * 60 * 60 * 24)) | 0;
    HEAP32[(tmPtr + 28) >> 2] = yday;
    HEAP32[(tmPtr + 36) >> 2] = -(date.getTimezoneOffset() * 60);
    var summerOffset = new Date(2e3, 6, 1).getTimezoneOffset();
    var winterOffset = start.getTimezoneOffset();
    var dst =
      (summerOffset != winterOffset &&
        date.getTimezoneOffset() == Math.min(winterOffset, summerOffset)) | 0;
    HEAP32[(tmPtr + 32) >> 2] = dst;
    var zonePtr = HEAP32[(__get_tzname() + (dst ? 4 : 0)) >> 2];
    HEAP32[(tmPtr + 40) >> 2] = zonePtr;
    return tmPtr;
  }

  function _longjmp(env, value) {
    Module['setThrew'](env, value || 1);
    throw 'longjmp';
  }

  function _emscripten_memcpy_big(dest, src, num) {
    HEAPU8.set(HEAPU8.subarray(src, src + num), dest);
    return dest;
  }

  function _usleep(useconds) {
    var msec = useconds / 1e3;
    if (
      (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) &&
      self['performance'] &&
      self['performance']['now']
    ) {
      var start = self['performance']['now']();
      while (self['performance']['now']() - start < msec) {}
    } else {
      var start = Date.now();
      while (Date.now() - start < msec) {}
    }
    return 0;
  }

  function _nanosleep(rqtp, rmtp) {
    var seconds = HEAP32[rqtp >> 2];
    var nanoseconds = HEAP32[(rqtp + 4) >> 2];
    if (rmtp !== 0) {
      HEAP32[rmtp >> 2] = 0;
      HEAP32[(rmtp + 4) >> 2] = 0;
    }
    return _usleep(seconds * 1e6 + nanoseconds / 1e3);
  }

  function _pthread_cond_wait() {
    return 0;
  }
  var PTHREAD_SPECIFIC = {};

  function _pthread_getspecific(key) {
    return PTHREAD_SPECIFIC[key] || 0;
  }
  var PTHREAD_SPECIFIC_NEXT_KEY = 1;

  function _pthread_key_create(key, destructor) {
    if (key == 0) {
      return ERRNO_CODES.EINVAL;
    }
    HEAP32[key >> 2] = PTHREAD_SPECIFIC_NEXT_KEY;
    PTHREAD_SPECIFIC[PTHREAD_SPECIFIC_NEXT_KEY] = 0;
    PTHREAD_SPECIFIC_NEXT_KEY++;
    return 0;
  }

  function _pthread_key_delete(key) {
    if (key in PTHREAD_SPECIFIC) {
      delete PTHREAD_SPECIFIC[key];
      return 0;
    }
    return ERRNO_CODES.EINVAL;
  }

  function _pthread_mutex_destroy() {}

  function _pthread_mutex_init() {}

  function _pthread_once(ptr, func) {
    if (!_pthread_once.seen) _pthread_once.seen = {};
    if (ptr in _pthread_once.seen) return;
    Module['dynCall_v'](func);
    _pthread_once.seen[ptr] = 1;
  }

  function _pthread_setcancelstate() {
    return 0;
  }

  function _pthread_setspecific(key, value) {
    if (!(key in PTHREAD_SPECIFIC)) {
      return ERRNO_CODES.EINVAL;
    }
    PTHREAD_SPECIFIC[key] = value;
    return 0;
  }

  function _sem_destroy() {}

  function _sem_init() {}

  function _sem_post() {}

  function _sem_wait() {}

  function _sigaction(signum, act, oldact) {
    return 0;
  }
  var __sigalrm_handler = 0;

  function _signal(sig, func) {
    if (sig == 14) {
      __sigalrm_handler = func;
    } else {
    }
    return 0;
  }

  function __isLeapYear(year) {
    return year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0);
  }

  function __arraySum(array, index) {
    var sum = 0;
    for (var i = 0; i <= index; sum += array[i++]);
    return sum;
  }
  var __MONTH_DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  var __MONTH_DAYS_REGULAR = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  function __addDays(date, days) {
    var newDate = new Date(date.getTime());
    while (days > 0) {
      var leap = __isLeapYear(newDate.getFullYear());
      var currentMonth = newDate.getMonth();
      var daysInCurrentMonth = (
        leap ? __MONTH_DAYS_LEAP : __MONTH_DAYS_REGULAR
      )[currentMonth];
      if (days > daysInCurrentMonth - newDate.getDate()) {
        days -= daysInCurrentMonth - newDate.getDate() + 1;
        newDate.setDate(1);
        if (currentMonth < 11) {
          newDate.setMonth(currentMonth + 1);
        } else {
          newDate.setMonth(0);
          newDate.setFullYear(newDate.getFullYear() + 1);
        }
      } else {
        newDate.setDate(newDate.getDate() + days);
        return newDate;
      }
    }
    return newDate;
  }

  function _strftime(s, maxsize, format, tm) {
    var tm_zone = HEAP32[(tm + 40) >> 2];
    var date = {
      tm_sec: HEAP32[tm >> 2],
      tm_min: HEAP32[(tm + 4) >> 2],
      tm_hour: HEAP32[(tm + 8) >> 2],
      tm_mday: HEAP32[(tm + 12) >> 2],
      tm_mon: HEAP32[(tm + 16) >> 2],
      tm_year: HEAP32[(tm + 20) >> 2],
      tm_wday: HEAP32[(tm + 24) >> 2],
      tm_yday: HEAP32[(tm + 28) >> 2],
      tm_isdst: HEAP32[(tm + 32) >> 2],
      tm_gmtoff: HEAP32[(tm + 36) >> 2],
      tm_zone: tm_zone ? Pointer_stringify(tm_zone) : '',
    };
    var pattern = Pointer_stringify(format);
    var EXPANSION_RULES_1 = {
      '%c': '%a %b %d %H:%M:%S %Y',
      '%D': '%m/%d/%y',
      '%F': '%Y-%m-%d',
      '%h': '%b',
      '%r': '%I:%M:%S %p',
      '%R': '%H:%M',
      '%T': '%H:%M:%S',
      '%x': '%m/%d/%y',
      '%X': '%H:%M:%S',
    };
    for (var rule in EXPANSION_RULES_1) {
      pattern = pattern.replace(new RegExp(rule, 'g'), EXPANSION_RULES_1[rule]);
    }
    var WEEKDAYS = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    var MONTHS = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    function leadingSomething(value, digits, character) {
      var str = typeof value === 'number' ? value.toString() : value || '';
      while (str.length < digits) {
        str = character[0] + str;
      }
      return str;
    }

    function leadingNulls(value, digits) {
      return leadingSomething(value, digits, '0');
    }

    function compareByDay(date1, date2) {
      function sgn(value) {
        return value < 0 ? -1 : value > 0 ? 1 : 0;
      }
      var compare;
      if ((compare = sgn(date1.getFullYear() - date2.getFullYear())) === 0) {
        if ((compare = sgn(date1.getMonth() - date2.getMonth())) === 0) {
          compare = sgn(date1.getDate() - date2.getDate());
        }
      }
      return compare;
    }

    function getFirstWeekStartDate(janFourth) {
      switch (janFourth.getDay()) {
        case 0:
          return new Date(janFourth.getFullYear() - 1, 11, 29);
        case 1:
          return janFourth;
        case 2:
          return new Date(janFourth.getFullYear(), 0, 3);
        case 3:
          return new Date(janFourth.getFullYear(), 0, 2);
        case 4:
          return new Date(janFourth.getFullYear(), 0, 1);
        case 5:
          return new Date(janFourth.getFullYear() - 1, 11, 31);
        case 6:
          return new Date(janFourth.getFullYear() - 1, 11, 30);
      }
    }

    function getWeekBasedYear(date) {
      var thisDate = __addDays(
        new Date(date.tm_year + 1900, 0, 1),
        date.tm_yday,
      );
      var janFourthThisYear = new Date(thisDate.getFullYear(), 0, 4);
      var janFourthNextYear = new Date(thisDate.getFullYear() + 1, 0, 4);
      var firstWeekStartThisYear = getFirstWeekStartDate(janFourthThisYear);
      var firstWeekStartNextYear = getFirstWeekStartDate(janFourthNextYear);
      if (compareByDay(firstWeekStartThisYear, thisDate) <= 0) {
        if (compareByDay(firstWeekStartNextYear, thisDate) <= 0) {
          return thisDate.getFullYear() + 1;
        } else {
          return thisDate.getFullYear();
        }
      } else {
        return thisDate.getFullYear() - 1;
      }
    }
    var EXPANSION_RULES_2 = {
      '%a': function (date) {
        return WEEKDAYS[date.tm_wday].substring(0, 3);
      },
      '%A': function (date) {
        return WEEKDAYS[date.tm_wday];
      },
      '%b': function (date) {
        return MONTHS[date.tm_mon].substring(0, 3);
      },
      '%B': function (date) {
        return MONTHS[date.tm_mon];
      },
      '%C': function (date) {
        var year = date.tm_year + 1900;
        return leadingNulls((year / 100) | 0, 2);
      },
      '%d': function (date) {
        return leadingNulls(date.tm_mday, 2);
      },
      '%e': function (date) {
        return leadingSomething(date.tm_mday, 2, ' ');
      },
      '%g': function (date) {
        return getWeekBasedYear(date).toString().substring(2);
      },
      '%G': function (date) {
        return getWeekBasedYear(date);
      },
      '%H': function (date) {
        return leadingNulls(date.tm_hour, 2);
      },
      '%I': function (date) {
        var twelveHour = date.tm_hour;
        if (twelveHour == 0) twelveHour = 12;
        else if (twelveHour > 12) twelveHour -= 12;
        return leadingNulls(twelveHour, 2);
      },
      '%j': function (date) {
        return leadingNulls(
          date.tm_mday +
            __arraySum(
              __isLeapYear(date.tm_year + 1900)
                ? __MONTH_DAYS_LEAP
                : __MONTH_DAYS_REGULAR,
              date.tm_mon - 1,
            ),
          3,
        );
      },
      '%m': function (date) {
        return leadingNulls(date.tm_mon + 1, 2);
      },
      '%M': function (date) {
        return leadingNulls(date.tm_min, 2);
      },
      '%n': function () {
        return '\n';
      },
      '%p': function (date) {
        if (date.tm_hour >= 0 && date.tm_hour < 12) {
          return 'AM';
        } else {
          return 'PM';
        }
      },
      '%S': function (date) {
        return leadingNulls(date.tm_sec, 2);
      },
      '%t': function () {
        return '\t';
      },
      '%u': function (date) {
        var day = new Date(
          date.tm_year + 1900,
          date.tm_mon + 1,
          date.tm_mday,
          0,
          0,
          0,
          0,
        );
        return day.getDay() || 7;
      },
      '%U': function (date) {
        var janFirst = new Date(date.tm_year + 1900, 0, 1);
        var firstSunday =
          janFirst.getDay() === 0
            ? janFirst
            : __addDays(janFirst, 7 - janFirst.getDay());
        var endDate = new Date(date.tm_year + 1900, date.tm_mon, date.tm_mday);
        if (compareByDay(firstSunday, endDate) < 0) {
          var februaryFirstUntilEndMonth =
            __arraySum(
              __isLeapYear(endDate.getFullYear())
                ? __MONTH_DAYS_LEAP
                : __MONTH_DAYS_REGULAR,
              endDate.getMonth() - 1,
            ) - 31;
          var firstSundayUntilEndJanuary = 31 - firstSunday.getDate();
          var days =
            firstSundayUntilEndJanuary +
            februaryFirstUntilEndMonth +
            endDate.getDate();
          return leadingNulls(Math.ceil(days / 7), 2);
        }
        return compareByDay(firstSunday, janFirst) === 0 ? '01' : '00';
      },
      '%V': function (date) {
        var janFourthThisYear = new Date(date.tm_year + 1900, 0, 4);
        var janFourthNextYear = new Date(date.tm_year + 1901, 0, 4);
        var firstWeekStartThisYear = getFirstWeekStartDate(janFourthThisYear);
        var firstWeekStartNextYear = getFirstWeekStartDate(janFourthNextYear);
        var endDate = __addDays(
          new Date(date.tm_year + 1900, 0, 1),
          date.tm_yday,
        );
        if (compareByDay(endDate, firstWeekStartThisYear) < 0) {
          return '53';
        }
        if (compareByDay(firstWeekStartNextYear, endDate) <= 0) {
          return '01';
        }
        var daysDifference;
        if (firstWeekStartThisYear.getFullYear() < date.tm_year + 1900) {
          daysDifference = date.tm_yday + 32 - firstWeekStartThisYear.getDate();
        } else {
          daysDifference = date.tm_yday + 1 - firstWeekStartThisYear.getDate();
        }
        return leadingNulls(Math.ceil(daysDifference / 7), 2);
      },
      '%w': function (date) {
        var day = new Date(
          date.tm_year + 1900,
          date.tm_mon + 1,
          date.tm_mday,
          0,
          0,
          0,
          0,
        );
        return day.getDay();
      },
      '%W': function (date) {
        var janFirst = new Date(date.tm_year, 0, 1);
        var firstMonday =
          janFirst.getDay() === 1
            ? janFirst
            : __addDays(
                janFirst,
                janFirst.getDay() === 0 ? 1 : 7 - janFirst.getDay() + 1,
              );
        var endDate = new Date(date.tm_year + 1900, date.tm_mon, date.tm_mday);
        if (compareByDay(firstMonday, endDate) < 0) {
          var februaryFirstUntilEndMonth =
            __arraySum(
              __isLeapYear(endDate.getFullYear())
                ? __MONTH_DAYS_LEAP
                : __MONTH_DAYS_REGULAR,
              endDate.getMonth() - 1,
            ) - 31;
          var firstMondayUntilEndJanuary = 31 - firstMonday.getDate();
          var days =
            firstMondayUntilEndJanuary +
            februaryFirstUntilEndMonth +
            endDate.getDate();
          return leadingNulls(Math.ceil(days / 7), 2);
        }
        return compareByDay(firstMonday, janFirst) === 0 ? '01' : '00';
      },
      '%y': function (date) {
        return (date.tm_year + 1900).toString().substring(2);
      },
      '%Y': function (date) {
        return date.tm_year + 1900;
      },
      '%z': function (date) {
        var off = date.tm_gmtoff;
        var ahead = off >= 0;
        off = Math.abs(off) / 60;
        off = (off / 60) * 100 + (off % 60);
        return (ahead ? '+' : '-') + String('0000' + off).slice(-4);
      },
      '%Z': function (date) {
        return date.tm_zone;
      },
      '%%': function () {
        return '%';
      },
    };
    for (var rule in EXPANSION_RULES_2) {
      if (pattern.indexOf(rule) >= 0) {
        pattern = pattern.replace(
          new RegExp(rule, 'g'),
          EXPANSION_RULES_2[rule](date),
        );
      }
    }
    var bytes = intArrayFromString(pattern, false);
    if (bytes.length > maxsize) {
      return 0;
    }
    writeArrayToMemory(bytes, s);
    return bytes.length - 1;
  }

  function _strftime_l(s, maxsize, format, tm) {
    return _strftime(s, maxsize, format, tm);
  }

  function _sysconf(name) {
    switch (name) {
      case 30:
        return PAGE_SIZE;
      case 85:
        var maxHeapSize = 2 * 1024 * 1024 * 1024 - 65536;
        return maxHeapSize / PAGE_SIZE;
      case 132:
      case 133:
      case 12:
      case 137:
      case 138:
      case 15:
      case 235:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 149:
      case 13:
      case 10:
      case 236:
      case 153:
      case 9:
      case 21:
      case 22:
      case 159:
      case 154:
      case 14:
      case 77:
      case 78:
      case 139:
      case 80:
      case 81:
      case 82:
      case 68:
      case 67:
      case 164:
      case 11:
      case 29:
      case 47:
      case 48:
      case 95:
      case 52:
      case 51:
      case 46:
        return 200809;
      case 79:
        return 0;
      case 27:
      case 246:
      case 127:
      case 128:
      case 23:
      case 24:
      case 160:
      case 161:
      case 181:
      case 182:
      case 242:
      case 183:
      case 184:
      case 243:
      case 244:
      case 245:
      case 165:
      case 178:
      case 179:
      case 49:
      case 50:
      case 168:
      case 169:
      case 175:
      case 170:
      case 171:
      case 172:
      case 97:
      case 76:
      case 32:
      case 173:
      case 35:
        return -1;
      case 176:
      case 177:
      case 7:
      case 155:
      case 8:
      case 157:
      case 125:
      case 126:
      case 92:
      case 93:
      case 129:
      case 130:
      case 131:
      case 94:
      case 91:
        return 1;
      case 74:
      case 60:
      case 69:
      case 70:
      case 4:
        return 1024;
      case 31:
      case 42:
      case 72:
        return 32;
      case 87:
      case 26:
      case 33:
        return 2147483647;
      case 34:
      case 1:
        return 47839;
      case 38:
      case 36:
        return 99;
      case 43:
      case 37:
        return 2048;
      case 0:
        return 2097152;
      case 3:
        return 65536;
      case 28:
        return 32768;
      case 44:
        return 32767;
      case 75:
        return 16384;
      case 39:
        return 1e3;
      case 89:
        return 700;
      case 71:
        return 256;
      case 40:
        return 255;
      case 2:
        return 100;
      case 180:
        return 64;
      case 25:
        return 20;
      case 5:
        return 16;
      case 6:
        return 6;
      case 73:
        return 4;
      case 84: {
        if (typeof navigator === 'object')
          return navigator['hardwareConcurrency'] || 1;
        return 1;
      }
    }
    ___setErrNo(ERRNO_CODES.EINVAL);
    return -1;
  }

  function _time(ptr) {
    var ret = (Date.now() / 1e3) | 0;
    if (ptr) {
      HEAP32[ptr >> 2] = ret;
    }
    return ret;
  }
  if (ENVIRONMENT_IS_NODE) {
    _emscripten_get_now = function _emscripten_get_now_actual() {
      var t = process['hrtime']();
      return t[0] * 1e3 + t[1] / 1e6;
    };
  } else if (typeof dateNow !== 'undefined') {
    _emscripten_get_now = dateNow;
  } else if (
    typeof self === 'object' &&
    self['performance'] &&
    typeof self['performance']['now'] === 'function'
  ) {
    _emscripten_get_now = function () {
      return self['performance']['now']();
    };
  } else if (
    typeof performance === 'object' &&
    typeof performance['now'] === 'function'
  ) {
    _emscripten_get_now = function () {
      return performance['now']();
    };
  } else {
    _emscripten_get_now = Date.now;
  }
  DYNAMICTOP_PTR = staticAlloc(4);
  STACK_BASE = STACKTOP = alignMemory(STATICTOP);
  STACK_MAX = STACK_BASE + TOTAL_STACK;
  DYNAMIC_BASE = alignMemory(STACK_MAX);
  HEAP32[DYNAMICTOP_PTR >> 2] = DYNAMIC_BASE;
  staticSealed = true;

  function intArrayFromString(stringy, dontAddNull, length) {
    var len = length > 0 ? length : lengthBytesUTF8(stringy) + 1;
    var u8array = new Array(len);
    var numBytesWritten = stringToUTF8Array(
      stringy,
      u8array,
      0,
      u8array.length,
    );
    if (dontAddNull) u8array.length = numBytesWritten;
    return u8array;
  }
  Module['wasmTableSize'] = 9521;
  Module['wasmMaxTableSize'] = 9521;

  function invoke_ii(index, a1) {
    var sp = stackSave();
    try {
      return Module['dynCall_ii'](index, a1);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_iii(index, a1, a2) {
    var sp = stackSave();
    try {
      return Module['dynCall_iii'](index, a1, a2);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_iiii(index, a1, a2, a3) {
    var sp = stackSave();
    try {
      return Module['dynCall_iiii'](index, a1, a2, a3);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_iiiii(index, a1, a2, a3, a4) {
    var sp = stackSave();
    try {
      return Module['dynCall_iiiii'](index, a1, a2, a3, a4);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_iiiiii(index, a1, a2, a3, a4, a5) {
    var sp = stackSave();
    try {
      return Module['dynCall_iiiiii'](index, a1, a2, a3, a4, a5);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_iiiiiiiiii(index, a1, a2, a3, a4, a5, a6, a7, a8, a9) {
    var sp = stackSave();
    try {
      return Module['dynCall_iiiiiiiiii'](
        index,
        a1,
        a2,
        a3,
        a4,
        a5,
        a6,
        a7,
        a8,
        a9,
      );
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_v(index) {
    var sp = stackSave();
    try {
      Module['dynCall_v'](index);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_vi(index, a1) {
    var sp = stackSave();
    try {
      Module['dynCall_vi'](index, a1);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_vii(index, a1, a2) {
    var sp = stackSave();
    try {
      Module['dynCall_vii'](index, a1, a2);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_viii(index, a1, a2, a3) {
    var sp = stackSave();
    try {
      Module['dynCall_viii'](index, a1, a2, a3);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_viiii(index, a1, a2, a3, a4) {
    var sp = stackSave();
    try {
      Module['dynCall_viiii'](index, a1, a2, a3, a4);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_viiiii(index, a1, a2, a3, a4, a5) {
    var sp = stackSave();
    try {
      Module['dynCall_viiiii'](index, a1, a2, a3, a4, a5);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }

  function invoke_viiiiii(index, a1, a2, a3, a4, a5, a6) {
    var sp = stackSave();
    try {
      Module['dynCall_viiiiii'](index, a1, a2, a3, a4, a5, a6);
    } catch (e) {
      stackRestore(sp);
      if (typeof e !== 'number' && e !== 'longjmp') throw e;
      Module['setThrew'](1, 0);
    }
  }
  Module.asmGlobalArg = {};
  Module.asmLibraryArg = {
    abort: abort,
    enlargeMemory: enlargeMemory,
    getTotalMemory: getTotalMemory,
    abortOnCannotGrowMemory: abortOnCannotGrowMemory,
    invoke_ii: invoke_ii,
    invoke_iii: invoke_iii,
    invoke_iiii: invoke_iiii,
    invoke_iiiii: invoke_iiiii,
    invoke_iiiiii: invoke_iiiiii,
    invoke_iiiiiiiiii: invoke_iiiiiiiiii,
    invoke_v: invoke_v,
    invoke_vi: invoke_vi,
    invoke_vii: invoke_vii,
    invoke_viii: invoke_viii,
    invoke_viiii: invoke_viiii,
    invoke_viiiii: invoke_viiiii,
    invoke_viiiiii: invoke_viiiiii,
    ___buildEnvironment: ___buildEnvironment,
    ___cxa_pure_virtual: ___cxa_pure_virtual,
    ___cxa_uncaught_exception: ___cxa_uncaught_exception,
    ___lock: ___lock,
    ___map_file: ___map_file,
    ___setErrNo: ___setErrNo,
    ___syscall102: ___syscall102,
    ___syscall118: ___syscall118,
    ___syscall125: ___syscall125,
    ___syscall140: ___syscall140,
    ___syscall145: ___syscall145,
    ___syscall146: ___syscall146,
    ___syscall183: ___syscall183,
    ___syscall192: ___syscall192,
    ___syscall194: ___syscall194,
    ___syscall195: ___syscall195,
    ___syscall197: ___syscall197,
    ___syscall20: ___syscall20,
    ___syscall221: ___syscall221,
    ___syscall3: ___syscall3,
    ___syscall33: ___syscall33,
    ___syscall4: ___syscall4,
    ___syscall41: ___syscall41,
    ___syscall5: ___syscall5,
    ___syscall54: ___syscall54,
    ___syscall6: ___syscall6,
    ___syscall77: ___syscall77,
    ___syscall91: ___syscall91,
    ___unlock: ___unlock,
    _abort: _abort,
    _atexit: _atexit,
    _clock_gettime: _clock_gettime,
    _difftime: _difftime,
    _emscripten_asm_const_i: _emscripten_asm_const_i,
    _emscripten_asm_const_ii: _emscripten_asm_const_ii,
    _emscripten_asm_const_iii: _emscripten_asm_const_iii,
    _emscripten_asm_const_iiii: _emscripten_asm_const_iiii,
    _emscripten_memcpy_big: _emscripten_memcpy_big,
    _exit: _exit,
    _getenv: _getenv,
    _gettimeofday: _gettimeofday,
    _gmtime: _gmtime,
    _gmtime_r: _gmtime_r,
    _llvm_log2_f32: _llvm_log2_f32,
    _llvm_stackrestore: _llvm_stackrestore,
    _llvm_stacksave: _llvm_stacksave,
    _llvm_trap: _llvm_trap,
    _llvm_trunc_f32: _llvm_trunc_f32,
    _localtime_r: _localtime_r,
    _longjmp: _longjmp,
    _nanosleep: _nanosleep,
    _pthread_cond_wait: _pthread_cond_wait,
    _pthread_getspecific: _pthread_getspecific,
    _pthread_key_create: _pthread_key_create,
    _pthread_key_delete: _pthread_key_delete,
    _pthread_mutex_destroy: _pthread_mutex_destroy,
    _pthread_mutex_init: _pthread_mutex_init,
    _pthread_once: _pthread_once,
    _pthread_setcancelstate: _pthread_setcancelstate,
    _pthread_setspecific: _pthread_setspecific,
    _sem_destroy: _sem_destroy,
    _sem_init: _sem_init,
    _sem_post: _sem_post,
    _sem_wait: _sem_wait,
    _sigaction: _sigaction,
    _signal: _signal,
    _strftime: _strftime,
    _strftime_l: _strftime_l,
    _sysconf: _sysconf,
    _time: _time,
    _tzset: _tzset,
    DYNAMICTOP_PTR: DYNAMICTOP_PTR,
    STACKTOP: STACKTOP,
  };
  var asm = Module['asm'](Module.asmGlobalArg, Module.asmLibraryArg, buffer);
  Module['asm'] = asm;
  var __GLOBAL__I_000101 = (Module['__GLOBAL__I_000101'] = function () {
    return Module['asm']['__GLOBAL__I_000101'].apply(null, arguments);
  });
  var __GLOBAL__sub_I_SkData_cpp = (Module['__GLOBAL__sub_I_SkData_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_SkData_cpp'].apply(null, arguments);
    });
  var __GLOBAL__sub_I_SkDiscardableMemoryPool_cpp = (Module[
    '__GLOBAL__sub_I_SkDiscardableMemoryPool_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkDiscardableMemoryPool_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkEventTracer_cpp = (Module[
    '__GLOBAL__sub_I_SkEventTracer_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkEventTracer_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkFontMgr_cpp = (Module['__GLOBAL__sub_I_SkFontMgr_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_SkFontMgr_cpp'].apply(
        null,
        arguments,
      );
    });
  var __GLOBAL__sub_I_SkGlyphCache_cpp = (Module[
    '__GLOBAL__sub_I_SkGlyphCache_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkGlyphCache_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkImageDecoder_libjpeg_cpp = (Module[
    '__GLOBAL__sub_I_SkImageDecoder_libjpeg_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkImageDecoder_libjpeg_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkImageFilter_cpp = (Module[
    '__GLOBAL__sub_I_SkImageFilter_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkImageFilter_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkMiniRecorder_cpp = (Module[
    '__GLOBAL__sub_I_SkMiniRecorder_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkMiniRecorder_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkOpts_cpp = (Module['__GLOBAL__sub_I_SkOpts_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_SkOpts_cpp'].apply(null, arguments);
    });
  var __GLOBAL__sub_I_SkPDFGraphicState_cpp = (Module[
    '__GLOBAL__sub_I_SkPDFGraphicState_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkPDFGraphicState_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkPDFShader_cpp = (Module[
    '__GLOBAL__sub_I_SkPDFShader_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkPDFShader_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkPathRef_cpp = (Module['__GLOBAL__sub_I_SkPathRef_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_SkPathRef_cpp'].apply(
        null,
        arguments,
      );
    });
  var __GLOBAL__sub_I_SkPicture_cpp = (Module['__GLOBAL__sub_I_SkPicture_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_SkPicture_cpp'].apply(
        null,
        arguments,
      );
    });
  var __GLOBAL__sub_I_SkResourceCache_cpp = (Module[
    '__GLOBAL__sub_I_SkResourceCache_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkResourceCache_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkTypeface_cpp = (Module[
    '__GLOBAL__sub_I_SkTypeface_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkTypeface_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_SkXfermode_cpp = (Module[
    '__GLOBAL__sub_I_SkXfermode_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_SkXfermode_cpp'].apply(
      null,
      arguments,
    );
  });
  var __GLOBAL__sub_I_iostream_cpp = (Module['__GLOBAL__sub_I_iostream_cpp'] =
    function () {
      return Module['asm']['__GLOBAL__sub_I_iostream_cpp'].apply(
        null,
        arguments,
      );
    });
  var __GLOBAL__sub_I_pfontpool_skia_cpp = (Module[
    '__GLOBAL__sub_I_pfontpool_skia_cpp'
  ] = function () {
    return Module['asm']['__GLOBAL__sub_I_pfontpool_skia_cpp'].apply(
      null,
      arguments,
    );
  });
  var __ZSt18uncaught_exceptionv = (Module['__ZSt18uncaught_exceptionv'] =
    function () {
      return Module['asm']['__ZSt18uncaught_exceptionv'].apply(null, arguments);
    });
  var ___emscripten_environ_constructor = (Module[
    '___emscripten_environ_constructor'
  ] = function () {
    return Module['asm']['___emscripten_environ_constructor'].apply(
      null,
      arguments,
    );
  });
  var __get_daylight = (Module['__get_daylight'] = function () {
    return Module['asm']['__get_daylight'].apply(null, arguments);
  });
  var __get_timezone = (Module['__get_timezone'] = function () {
    return Module['asm']['__get_timezone'].apply(null, arguments);
  });
  var __get_tzname = (Module['__get_tzname'] = function () {
    return Module['asm']['__get_tzname'].apply(null, arguments);
  });
  var _emscripten_replace_memory = (Module['_emscripten_replace_memory'] =
    function () {
      return Module['asm']['_emscripten_replace_memory'].apply(null, arguments);
    });
  var _free = (Module['_free'] = function () {
    return Module['asm']['_free'].apply(null, arguments);
  });
  var _htons = (Module['_htons'] = function () {
    return Module['asm']['_htons'].apply(null, arguments);
  });
  var _main = (Module['_main'] = function () {
    return Module['asm']['_main'].apply(null, arguments);
  });
  var _malloc = (Module['_malloc'] = function () {
    return Module['asm']['_malloc'].apply(null, arguments);
  });
  var _memalign = (Module['_memalign'] = function () {
    return Module['asm']['_memalign'].apply(null, arguments);
  });
  var _memset = (Module['_memset'] = function () {
    return Module['asm']['_memset'].apply(null, arguments);
  });
  var _noox_worker_fetch_sync_message = (Module[
    '_noox_worker_fetch_sync_message'
  ] = function () {
    return Module['asm']['_noox_worker_fetch_sync_message'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_get_changes = (Module['_noox_worker_get_changes'] =
    function () {
      return Module['asm']['_noox_worker_get_changes'].apply(null, arguments);
    });
  var _noox_worker_get_layout_info = (Module['_noox_worker_get_layout_info'] =
    function () {
      return Module['asm']['_noox_worker_get_layout_info'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_get_page_as_json = (Module['_noox_worker_get_page_as_json'] =
    function () {
      return Module['asm']['_noox_worker_get_page_as_json'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_get_pages_info = (Module['_noox_worker_get_pages_info'] =
    function () {
      return Module['asm']['_noox_worker_get_pages_info'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_get_status = (Module['_noox_worker_get_status'] =
    function () {
      return Module['asm']['_noox_worker_get_status'].apply(null, arguments);
    });
  var _noox_worker_main_commit_buffer = (Module[
    '_noox_worker_main_commit_buffer'
  ] = function () {
    return Module['asm']['_noox_worker_main_commit_buffer'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_commit_writer_length = (Module[
    '_noox_worker_main_commit_writer_length'
  ] = function () {
    return Module['asm']['_noox_worker_main_commit_writer_length'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_ensure_sync_length = (Module[
    '_noox_worker_main_ensure_sync_length'
  ] = function () {
    return Module['asm']['_noox_worker_main_ensure_sync_length'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_ensure_writer_length = (Module[
    '_noox_worker_main_ensure_writer_length'
  ] = function () {
    return Module['asm']['_noox_worker_main_ensure_writer_length'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_export_as_docx = (Module[
    '_noox_worker_main_export_as_docx'
  ] = function () {
    return Module['asm']['_noox_worker_main_export_as_docx'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_export_as_pdf = (Module[
    '_noox_worker_main_export_as_pdf'
  ] = function () {
    return Module['asm']['_noox_worker_main_export_as_pdf'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_export_as_raw_json = (Module[
    '_noox_worker_main_export_as_raw_json'
  ] = function () {
    return Module['asm']['_noox_worker_main_export_as_raw_json'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_export_as_raw_text = (Module[
    '_noox_worker_main_export_as_raw_text'
  ] = function () {
    return Module['asm']['_noox_worker_main_export_as_raw_text'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_export_font = (Module['_noox_worker_main_export_font'] =
    function () {
      return Module['asm']['_noox_worker_main_export_font'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_export_image = (Module[
    '_noox_worker_main_export_image'
  ] = function () {
    return Module['asm']['_noox_worker_main_export_image'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_get_buffer = (Module['_noox_worker_main_get_buffer'] =
    function () {
      return Module['asm']['_noox_worker_main_get_buffer'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_get_buffer_length = (Module[
    '_noox_worker_main_get_buffer_length'
  ] = function () {
    return Module['asm']['_noox_worker_main_get_buffer_length'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_get_comment = (Module['_noox_worker_main_get_comment'] =
    function () {
      return Module['asm']['_noox_worker_main_get_comment'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_get_export_token_ts = (Module[
    '_noox_worker_main_get_export_token_ts'
  ] = function () {
    return Module['asm']['_noox_worker_main_get_export_token_ts'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_get_session = (Module['_noox_worker_main_get_session'] =
    function () {
      return Module['asm']['_noox_worker_main_get_session'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_get_writer = (Module['_noox_worker_main_get_writer'] =
    function () {
      return Module['asm']['_noox_worker_main_get_writer'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_get_writer_length = (Module[
    '_noox_worker_main_get_writer_length'
  ] = function () {
    return Module['asm']['_noox_worker_main_get_writer_length'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_grow_buffer = (Module['_noox_worker_main_grow_buffer'] =
    function () {
      return Module['asm']['_noox_worker_main_grow_buffer'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_grow_raw = (Module['_noox_worker_main_grow_raw'] =
    function () {
      return Module['asm']['_noox_worker_main_grow_raw'].apply(null, arguments);
    });
  var _noox_worker_main_import_file = (Module['_noox_worker_main_import_file'] =
    function () {
      return Module['asm']['_noox_worker_main_import_file'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_import_ranges = (Module[
    '_noox_worker_main_import_ranges'
  ] = function () {
    return Module['asm']['_noox_worker_main_import_ranges'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_mimetype_buffer = (Module[
    '_noox_worker_main_mimetype_buffer'
  ] = function () {
    return Module['asm']['_noox_worker_main_mimetype_buffer'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_needs_export_token = (Module[
    '_noox_worker_main_needs_export_token'
  ] = function () {
    return Module['asm']['_noox_worker_main_needs_export_token'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_perform_operation = (Module[
    '_noox_worker_main_perform_operation'
  ] = function () {
    return Module['asm']['_noox_worker_main_perform_operation'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_raw_import = (Module['_noox_worker_main_raw_import'] =
    function () {
      return Module['asm']['_noox_worker_main_raw_import'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_reset_buffer = (Module[
    '_noox_worker_main_reset_buffer'
  ] = function () {
    return Module['asm']['_noox_worker_main_reset_buffer'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_reset_sync = (Module['_noox_worker_main_reset_sync'] =
    function () {
      return Module['asm']['_noox_worker_main_reset_sync'].apply(
        null,
        arguments,
      );
    });
  var _noox_worker_main_reset_writer = (Module[
    '_noox_worker_main_reset_writer'
  ] = function () {
    return Module['asm']['_noox_worker_main_reset_writer'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_resolve_style_id = (Module[
    '_noox_worker_main_resolve_style_id'
  ] = function () {
    return Module['asm']['_noox_worker_main_resolve_style_id'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_main_set_export_token = (Module[
    '_noox_worker_main_set_export_token'
  ] = function () {
    return Module['asm']['_noox_worker_main_set_export_token'].apply(
      null,
      arguments,
    );
  });
  var _noox_worker_perform_sync = (Module['_noox_worker_perform_sync'] =
    function () {
      return Module['asm']['_noox_worker_perform_sync'].apply(null, arguments);
    });
  var _noox_worker_update_layout = (Module['_noox_worker_update_layout'] =
    function () {
      return Module['asm']['_noox_worker_update_layout'].apply(null, arguments);
    });
  var _noox_worker_update_sid = (Module['_noox_worker_update_sid'] =
    function () {
      return Module['asm']['_noox_worker_update_sid'].apply(null, arguments);
    });
  var _ntohs = (Module['_ntohs'] = function () {
    return Module['asm']['_ntohs'].apply(null, arguments);
  });
  var setThrew = (Module['setThrew'] = function () {
    return Module['asm']['setThrew'].apply(null, arguments);
  });
  var stackAlloc = (Module['stackAlloc'] = function () {
    return Module['asm']['stackAlloc'].apply(null, arguments);
  });
  var stackRestore = (Module['stackRestore'] = function () {
    return Module['asm']['stackRestore'].apply(null, arguments);
  });
  var stackSave = (Module['stackSave'] = function () {
    return Module['asm']['stackSave'].apply(null, arguments);
  });
  var dynCall_ii = (Module['dynCall_ii'] = function () {
    return Module['asm']['dynCall_ii'].apply(null, arguments);
  });
  var dynCall_iii = (Module['dynCall_iii'] = function () {
    return Module['asm']['dynCall_iii'].apply(null, arguments);
  });
  var dynCall_iiii = (Module['dynCall_iiii'] = function () {
    return Module['asm']['dynCall_iiii'].apply(null, arguments);
  });
  var dynCall_iiiii = (Module['dynCall_iiiii'] = function () {
    return Module['asm']['dynCall_iiiii'].apply(null, arguments);
  });
  var dynCall_iiiiii = (Module['dynCall_iiiiii'] = function () {
    return Module['asm']['dynCall_iiiiii'].apply(null, arguments);
  });
  var dynCall_iiiiiiiiii = (Module['dynCall_iiiiiiiiii'] = function () {
    return Module['asm']['dynCall_iiiiiiiiii'].apply(null, arguments);
  });
  var dynCall_v = (Module['dynCall_v'] = function () {
    return Module['asm']['dynCall_v'].apply(null, arguments);
  });
  var dynCall_vi = (Module['dynCall_vi'] = function () {
    return Module['asm']['dynCall_vi'].apply(null, arguments);
  });
  var dynCall_vii = (Module['dynCall_vii'] = function () {
    return Module['asm']['dynCall_vii'].apply(null, arguments);
  });
  var dynCall_viii = (Module['dynCall_viii'] = function () {
    return Module['asm']['dynCall_viii'].apply(null, arguments);
  });
  var dynCall_viiii = (Module['dynCall_viiii'] = function () {
    return Module['asm']['dynCall_viiii'].apply(null, arguments);
  });
  var dynCall_viiiii = (Module['dynCall_viiiii'] = function () {
    return Module['asm']['dynCall_viiiii'].apply(null, arguments);
  });
  var dynCall_viiiiii = (Module['dynCall_viiiiii'] = function () {
    return Module['asm']['dynCall_viiiiii'].apply(null, arguments);
  });
  Module['asm'] = asm;
  Module['UTF8ToString'] = UTF8ToString;
  Module['stringToUTF8'] = stringToUTF8;
  Module['UTF16ToString'] = UTF16ToString;
  Module['stringToUTF16'] = stringToUTF16;
  Module['then'] = function (func) {
    if (Module['calledRun']) {
      func(Module);
    } else {
      var old = Module['onRuntimeInitialized'];
      Module['onRuntimeInitialized'] = function () {
        if (old) old();
        func(Module);
      };
    }
    return Module;
  };

  function ExitStatus(status) {
    this.name = 'ExitStatus';
    this.message = 'Program terminated with exit(' + status + ')';
    this.status = status;
  }
  ExitStatus.prototype = new Error();
  ExitStatus.prototype.constructor = ExitStatus;
  var initialStackTop;
  var calledMain = false;
  dependenciesFulfilled = function runCaller() {
    if (!Module['calledRun']) run();
    if (!Module['calledRun']) dependenciesFulfilled = runCaller;
  };
  Module['callMain'] = function callMain(args) {
    args = args || [];
    ensureInitRuntime();
    var argc = args.length + 1;
    var argv = stackAlloc((argc + 1) * 4);
    HEAP32[argv >> 2] = allocateUTF8OnStack(Module['thisProgram']);
    for (var i = 1; i < argc; i++) {
      HEAP32[(argv >> 2) + i] = allocateUTF8OnStack(args[i - 1]);
    }
    HEAP32[(argv >> 2) + argc] = 0;
    try {
      var ret = Module['_main'](argc, argv, 0);
      exit(ret, true);
    } catch (e) {
      if (e instanceof ExitStatus) {
        return;
      } else if (e == 'SimulateInfiniteLoop') {
        Module['noExitRuntime'] = true;
        return;
      } else {
        var toLog = e;
        if (e && typeof e === 'object' && e.stack) {
          toLog = [e, e.stack];
        }
        err('exception thrown: ' + toLog);
        Module['quit'](1, e);
      }
    } finally {
      calledMain = true;
    }
  };

  function run(args) {
    args = args || Module['arguments'];
    if (runDependencies > 0) {
      return;
    }
    preRun();
    if (runDependencies > 0) return;
    if (Module['calledRun']) return;

    function doRun() {
      if (Module['calledRun']) return;
      Module['calledRun'] = true;
      if (ABORT) return;
      ensureInitRuntime();
      preMain();
      if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();
      if (Module['_main'] && shouldRunNow) Module['callMain'](args);
      postRun();
    }
    if (Module['setStatus']) {
      Module['setStatus']('Running...');
      setTimeout(function () {
        setTimeout(function () {
          Module['setStatus']('');
        }, 1);
        doRun();
      }, 1);
    } else {
      doRun();
    }
  }
  Module['run'] = run;

  function exit(status, implicit) {
    if (implicit && Module['noExitRuntime'] && status === 0) {
      return;
    }
    if (Module['noExitRuntime']) {
    } else {
      ABORT = true;
      EXITSTATUS = status;
      STACKTOP = initialStackTop;
      exitRuntime();
      if (Module['onExit']) Module['onExit'](status);
    }
    Module['quit'](status, new ExitStatus(status));
  }

  function abort(what) {
    if (Module['onAbort']) {
      Module['onAbort'](what);
    }
    if (what !== undefined) {
      out(what);
      err(what);
      what = JSON.stringify(what);
    } else {
      what = '';
    }
    ABORT = true;
    EXITSTATUS = 1;
    throw 'abort(' + what + '). Build with -s ASSERTIONS=1 for more info.';
  }
  Module['abort'] = abort;
  if (Module['preInit']) {
    if (typeof Module['preInit'] == 'function')
      Module['preInit'] = [Module['preInit']];
    while (Module['preInit'].length > 0) {
      Module['preInit'].pop()();
    }
  }
  var shouldRunNow = true;
  if (Module['noInitialRun']) {
    shouldRunNow = false;
  }
  Module['noExitRuntime'] = true;
  run();
  if (nd_init['WASM_ENV']) {
    Module.preRun.push(function () {
      Object.keys(nd_init['WASM_ENV']).forEach(function (key) {
        if (undefined === nd_init['WASM_ENV'][key]) {
        } else if ('string' === typeof nd_init['WASM_ENV'][key]) {
          ENV[key] = nd_init['WASM_ENV'][key];
        } else {
          ENV[key] = JSON.stringify(nd_init['WASM_ENV'][key]);
        }
      });
    });
  }
  Module['isTerm'] = function () {
    return ABORT;
  };

  return nd_init;
};
if (typeof exports === 'object' && typeof module === 'object')
  module.exports = nd_init;
else if (typeof define === 'function' && define['amd'])
  define([], function () {
    return nd_init;
  });
else if (typeof exports === 'object') exports['nd_init'] = nd_init;
