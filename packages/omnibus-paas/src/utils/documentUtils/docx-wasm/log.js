const LEVEL_STR = ['F', 'E', 'J', 'W', 'N', 'I', 'D', '?'];

class Log {
  constructor() {
    this.entries = new Array(20); // max entries
    this.count = 0;
  }

  push(level, msg) {
    const M = this.entries.length;
    const i = this.count++ % M;
    this.entries[i] = {
      level: level,
      msg: msg,
    };
  }

  toArray() {
    const M = this.entries.length;
    var helper = new Array(Math.min(this.count, M));
    const d = this.count - helper.length;
    for (var i = 0; i < helper.length; i++) {
      helper[i] = this.entries[(d + i) % M];
    }
    return helper;
  }

  toString() {
    return this.toArray()
      .map(function (entry) {
        return (
          LEVEL_STR[Math.min(entry.level, LEVEL_STR.length - 1)] +
          '|' +
          entry.msg
        );
      })
      .join('\n');
  }
}

module.exports = Log;

/*
var log = new Log();
log.push(0, "0");
log.push(1, "1");
log.push(2, "2");
log.push(3, "3");
log.push(4, "4");
log.push(5, "5");
//log.push(6, "6");
console.log(JSON.stringify(log.toString()));
*/
