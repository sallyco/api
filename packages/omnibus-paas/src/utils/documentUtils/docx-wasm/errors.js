class DocxWasmError extends Error {
  constructor(message, module) {
    if ('string' === typeof module) {
      message = message + '\nDetails:\n' + module;
    } else if (module && module['ndLog']) {
      // add log when available
      const log = module['ndLog'];
      message = message + '\nLog:\n' + log.toString();
    }

    // Calling parent constructor of base Error class.
    super(message);

    // Saving class name in the property of our custom error as a shortcut.
    this.name = this.constructor.name;

    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);
  }
}

class TokenValidationError extends DocxWasmError {}
class InitFailedError extends DocxWasmError {}
class NetworkError extends DocxWasmError {}
class OperationFailedError extends DocxWasmError {}
class EngineExceptionError extends DocxWasmError {}
class EngineTerminatedError extends DocxWasmError {}
class EngineSyncError extends DocxWasmError {}

module.exports = {
  TokenValidationError,
  InitFailedError,
  NetworkError,
  OperationFailedError,
  EngineExceptionError,
  EngineTerminatedError,
  EngineSyncError,
};
