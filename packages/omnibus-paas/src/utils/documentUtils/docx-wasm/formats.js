const Format = Object.freeze({
  DOCX: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  PDF: 'application/pdf',
});

module.exports = Format;
