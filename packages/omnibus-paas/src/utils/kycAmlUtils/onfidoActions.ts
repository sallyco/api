/* eslint-disable @typescript-eslint/no-explicit-any */
import {Applicant, Onfido} from '@onfido/api';
import {Profile} from '../../models/profile.model';

const onfido = new Onfido({
  apiToken:
    process.env.ONFIDO_TOKEN ??
    'api_sandbox.F5FNEEMeIrX.uLDCsO5rOoxWIhYUPB7FJfZWGXPlcZa0',
});

export async function createOnfidoApplicant(invProfile: Profile) {
  try {
    const applicant: Applicant = await onfido.applicant.create({
      firstName: invProfile.firstName,
      lastName: invProfile.lastName,
      email: invProfile.email,
      dob: invProfile.dateOfBirth,
      idNumbers: [
        {
          type: invProfile.taxDetails?.taxIdentification?.type ?? '',
          value: invProfile.taxDetails?.taxIdentification?.value ?? '',
        },
      ],
      address: {
        street: invProfile.address?.address1,
        subStreet: invProfile.address?.address2,
        town: invProfile.address?.city,
        // TODO Fix This hard coding
        state: 'UT', //invProfile.address?.state, // US states must use the USPS abbreviation
        postcode: invProfile.address?.postalCode ?? '',
        // TODO Fix This hard coding
        country: 'USA', //invProfile.address?.country ?? '',
      },
    });
    return {success: true, data: applicant};
  } catch (err) {
    return {success: false, error: err};
  }
}

export async function updateApplicant(applicantId: any, invProfile: Profile) {
  try {
    const applicant = await onfido.applicant.update(applicantId, {
      firstName: invProfile.firstName,
      lastName: invProfile.lastName,
      email: invProfile.email,
      dob: invProfile.dateOfBirth,
      idNumbers: [
        {
          type: invProfile.taxDetails?.taxIdentification?.type ?? '',
          value: invProfile.taxDetails?.taxIdentification?.value ?? '',
        },
      ],
      address: {
        street: invProfile.address?.address1,
        subStreet: invProfile.address?.address2,
        town: invProfile.address?.city,
        state: invProfile.address?.state, // US states must use the USPS abbreviation
        postcode: invProfile.address?.postalCode ?? '',
        country: invProfile.address?.country ?? '',
      },
    });
    return {success: true, data: applicant};
  } catch (err) {
    return {success: false, error: err};
  }
}

export async function checkApplicant(applicantId: string) {
  try {
    const check = await onfido.check.create({
      applicantId: applicantId,
      reportNames: ['identity_enhanced', 'watchlist_enhanced'],
    });

    // temporary delay and then retrived check
    const delay = new Promise(resolve => {
      setTimeout(resolve, 4000);
    });
    await delay;
    const retrievedCheck = await onfido.check.find(check.id);

    if (retrievedCheck.result === 'consider') {
      await notifyConsiderApplicant(applicantId);
    }

    return {success: true, data: retrievedCheck};
  } catch (err) {
    return {success: false, error: err};
  }
}

export async function notifyConsiderApplicant(applicantId: string) {
  // const transporter = nodemailer.createTransport({
  //   host: 'maildev',
  //   port: 1025,
  //   ignoreTLS: true,
  // });
  //
  // const applicant = await onfido.applicant.find(applicantId);
  // const message = await templates.constructEmails(
  //   'onfidoConsider',
  //   `${applicant.firstName} ${applicant.lastName}`,
  //   'returnUrl',
  //   '',
  // );
  // await transporter.sendMail({
  //   from: 'glassboard@glassboardtech.com',
  //   to: applicant.email,
  //   subject: '',
  //   html: message,
  // });

  return;
}
