import {BindingKey, CoreBindings} from '@loopback/core';
import {AuthorizationComponent} from './authorization-component';
import {AuthorizationMetadata} from './types';

/**
 * Binding keys used by authorization component.
 */
export namespace AuthorizationBindings {
  export const METADATA = BindingKey.create<AuthorizationMetadata>(
    'authorization.operationMetadata',
  );

  export const COMPONENT = BindingKey.create<AuthorizationComponent>(
    `${CoreBindings.COMPONENTS}.AuthorizationComponent`,
  );

  export const RESOURCE_ID = BindingKey.create<string>('source.type');
}

/**
 * Binding tags used by authorization component
 */
export namespace AuthorizationTags {
  /**
   * A tag for authorizers
   */
  export const AUTHORIZER = 'authorizer';
}
