import {
  Binding,
  Component,
  ContextTags,
  createBindingFromClass,
  injectable,
} from '@loopback/core';
import {
  AuthorizationInterceptor,
  AuthorizationProvider,
  getCasbinEnforcer,
} from './services';
import {AuthorizationBindings, AuthorizationTags} from './keys';

@injectable({tags: {[ContextTags.KEY]: AuthorizationBindings.COMPONENT.key}})
export class AuthorizationComponent implements Component {
  bindings: Binding[] = [
    createBindingFromClass(AuthorizationInterceptor),
    Binding.bind('authorizationProviders.authorizer-provider')
      .toProvider(AuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER),
    Binding.bind('casbin.enforcer.factory').to(getCasbinEnforcer),
  ];
}
