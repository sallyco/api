import {Provider} from '@loopback/core';
import {
  Authorizer,
  AuthorizationContext,
  AuthorizationRequest,
  AuthorizationMetadata,
  AuthorizationDecision,
} from '../types';
import {securityId, UserProfile} from '@loopback/security';
import {inject} from '@loopback/core';

//import {AuthorizationBindings} from '../';
import _ from 'lodash';
import * as casbin from 'casbin';

const DEFAULT_SCOPE = 'execute';

export class AuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @inject('casbin.enforcer.factory')
    private enforcerFactory: () => Promise<casbin.Enforcer>,
  ) {}

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    let currentUser: UserProfile;
    let tenantId: string;
    if (authorizationCtx.principals.length > 0) {
      const user = _.pick(authorizationCtx.principals[0], [
        'id',
        'tenantId',
        'roles',
      ]);
      currentUser = {[securityId]: user.id, roles: user.roles};
      tenantId = user.tenantId;
    } else {
      return AuthorizationDecision.DENY;
    }

    const resourceId = authorizationCtx.invocationContext?.args[0];
    const subject = this.getUserName(authorizationCtx.principals[0].id);
    const object = resourceId ?? metadata.resource ?? authorizationCtx.resource;

    const request: AuthorizationRequest = {
      subject,
      object,
      action: metadata.scopes?.[0] ?? DEFAULT_SCOPE,
    };

    const enforcer = await this.enforcerFactory();
    /* eslint-disable-next-line @typescript-eslint/no-unused-vars */
    const allowedByAttributes = await enforcer.enforce(
      request.subject,
      request.object,
      request.action,
    );

    const allowedRoles = metadata.allowedRoles ?? [];
    const allowedTenants = metadata.allowedTenants ?? [];

    const matchedRoles = allowedRoles?.filter(e =>
      currentUser.roles?.account?.roles?.includes(e),
    );

    if (allowedTenants.length === 0 && allowedRoles.length === 0) {
      return AuthorizationDecision.ALLOW;
    }

    if (allowedTenants.includes(tenantId) || matchedRoles.length > 0) {
      return AuthorizationDecision.ALLOW;
    }
    return AuthorizationDecision.DENY;
  }
  getUserName(id: number): string {
    return `u${id}`;
  }
}
