import {inject} from '@loopback/context';
import {HttpErrors} from '@loopback/rest';
import {promisify} from 'util';
import {TokenService} from '@loopback/authentication';
import {UserProfile, securityId} from '@loopback/security';
import {TokenServiceBindings} from '../../keys';
import {MultiTenancyBindings, Tenant} from '../../multi-tenancy';
import KcAdminClient from 'keycloak-admin';
import axios, {AxiosResponse} from 'axios';

import {WhereBuilder} from '@loopback/repository';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export class JWTService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
    @inject(MultiTenancyBindings.CURRENT_TENANT)
    private tenant: Tenant,
  ) {}

  async clientAuth() {
    const kcAdminClient = new KcAdminClient({
      baseUrl: process.env.KC_BASE_URL,
      realmName: 'master',
    });

    await kcAdminClient.auth({
      username: process.env.KC_REALM_USER
        ? process.env.KC_REALM_USER
        : 'development',
      password: process.env.KC_REALM_PASS
        ? process.env.KC_REALM_PASS
        : 'development',
      grantType: 'password',
      clientId: 'admin-cli',
    });

    return kcAdminClient;
  }

  async verifyToken(token: string): Promise<UserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : 'token' is null`,
      );
    }

    let userProfile: UserProfile;

    interface RealmMeta {
      realm: string;
      public_key: string;
      'token-service': string;
      'account-service': string;
      'tokens-not-before': number;
    }
    // Pull public key for this realm. Doesn't exist in the KeycloakAdmin module, so we have to pull it ourselves
    const realmMetadata: AxiosResponse<RealmMeta> = await axios.get(
      process.env.KC_BASE_URL + '/realms/' + this.tenant.id,
    );

    try {
      // decode user profile from token
      const decodedToken = await verifyAsync(
        token,
        '-----BEGIN PUBLIC KEY-----\n' +
          realmMetadata.data.public_key +
          '\n-----END PUBLIC KEY-----',
        {
          algorithms: ['RS256'],
        },
      );

      //get realm from issuer
      const t_id = decodedToken.iss.split('/').pop();

      //build base where filter
      let whereBuilder = new WhereBuilder();
      if (t_id !== 'master') {
        if (
          decodedToken.resource_access?.account?.roles.includes('admin') ||
          decodedToken.account?.roles.includes('admin')
        ) {
          whereBuilder = whereBuilder.eq('tenantId', t_id);
        } else {
          whereBuilder = whereBuilder.or(
            {coOwnerIds: decodedToken.sub},
            {ownerId: decodedToken.sub},
          );
        }
      }
      const where = whereBuilder.build();

      // don't copy over  token field 'iat' and 'exp', nor 'email' to user profile
      userProfile = Object.assign(
        {[securityId]: '', name: ''},
        {
          [securityId]: decodedToken.id,
          name: decodedToken.name,
          id: decodedToken.sub,
          email: decodedToken.email,
          roles: decodedToken.resource_access ?? decodedToken.account,
          tenantId: t_id,
        },
        {where: where},
      );
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
    return userProfile;
  }

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token : userProfile is null',
      );
    }
    const userInfoForToken = {
      id: userProfile[securityId],
      name: userProfile.name,
      roles: userProfile.roles,
    };
    // Generate a JSON Web Token
    let token: string;
    try {
      token = await signAsync(userInfoForToken, this.jwtSecret, {
        expiresIn: Number(this.jwtExpiresIn),
      });
    } catch (error) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${error}`);
    }

    return token;
  }
}
