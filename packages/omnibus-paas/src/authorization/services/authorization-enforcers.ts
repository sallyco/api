import * as casbin from 'casbin';
import path from 'path';

export async function getCasbinEnforcer(): Promise<casbin.Enforcer> {
  const conf = path.resolve(__dirname, './../fixtures/abac_model.conf');
  return casbin.newEnforcer(conf);
}
