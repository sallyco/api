import {ThemeRepository} from '../repositories';

const up = async app => {
  const themeRepo = await app.getRepository(ThemeRepository);
  const found = await themeRepo.findOne({where: {tenantId: 'gbt'}});
  if (!found) {
    themeRepo.create({
      tenantId: 'gbt',
      name: 'gbt.theme.css',
      key: 'gbt.theme.css',
    });
  }
};
export default up;
