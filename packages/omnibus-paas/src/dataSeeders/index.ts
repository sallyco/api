import * as emailTemplateSeeder from './EmailTemplateSeeder';
import * as tenantSeeder from './TenantSeeder';
import * as tenantThemeSeeder from './TenantThemeSeeder';
const main = async app =>
  emailTemplateSeeder
    .default(app)
    .then(() =>
      process.env.NODE_ENV === 'development'
        ? tenantThemeSeeder.default(app)
        : null,
    )
    .then(() =>
      process.env.NODE_ENV === 'development' ? tenantSeeder.default(app) : null,
    )
    .then(() => {
      console.log('Database Seeded Successfully');
    })
    .catch(err => {
      console.error(`Database Seeding ERROR: ${err}`);
    });
export default main;
