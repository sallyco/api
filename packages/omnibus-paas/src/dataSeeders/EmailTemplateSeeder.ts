import {EmailTemplateRepository} from '../repositories';

const up = async app => {
  const emailTemplateRepo = await app.getRepository(EmailTemplateRepository);
  const templates = [
    {
      tenantId: 'global',
      key: 'inviteInvestor',
      data: {
        salutations: 'Hello,',
        contentIntroduction: 'You have been invited to',
        buttonText: 'Create an Account',
      },
    },
    {
      tenantId: 'global',
      key: 'emailFooter',
      data: {
        footerText: '',
      },
    },
    {
      tenantId: 'global',
      key: 'inviteInvestorExisting',
      data: {
        salutations: 'Hello',
        contentIntroduction: 'You have been invited to',
        buttonText: 'Log In',
      },
    },
    {
      tenantId: 'global',
      key: 'newAccount',
      data: {
        salutations: 'Hello',
        contentIntroduction: 'An account has been created for',
        contentInstructions: 'Follow the link to verify this email.',
        buttonText: 'Verify My Email',
      },
    },
    {
      tenantId: 'global',
      key: 'nudgeInvestor',
      data: {
        salutations: 'Hello',
        contentIntroduction: 'This is a reminder that you have been invited to',
        contentInstructions: 'Follow the link to verify this email.',
        buttonText: 'Log In',
      },
    },
    {
      tenantId: 'global',
      key: 'inviteOrganizer',
      data: {
        salutations: 'Hello',
        contentIntroduction: 'has invited you to be the organizer for',
        contentInstructions: 'Review the company information below',
        buttonPass: 'Pass',
        buttonAccept: 'Organize Deal',
      },
    },
    {
      tenantId: 'global',
      key: 'resetPassword',
      data: {
        salutations: 'Password Reset Requested for',
        contentIntroduction: 'Finish resetting your password.',
        contentInstructions:
          'The stronger your password, the better. We recommend using a unique mix of both uppercase and lowercase letters, numbers, and symbols.',
        buttonText: 'Reset Password',
      },
    },
    {
      tenantId: 'global',
      key: 'inviteAccountAdminUser',
      data: {
        salutations: 'Hello,',
        contentIntroduction:
          'You have been invited as an administator for an account connected to ',
        buttonText: 'Create an Account',
      },
    },
    {
      tenantId: 'global',
      key: 'removeInvite',
      data: {
        salutations: 'Hello',
      },
    },
    {
      tenantId: 'global',
      key: 'inviteSigner',
      data: {
        salutations: 'Hello,',
        contentIntroduction: 'You have been invited to',
        buttonText: 'View Details',
      },
    },
  ];

  templates.map(el =>
    emailTemplateRepo
      .findOne({
        where: {and: [{tenantId: el.tenantId}, {key: el.key}]},
      })
      .then(isFound => {
        if (!isFound) {
          return emailTemplateRepo.create(el);
        }
      })
      .catch(err => err),
  );
};
export default up;
