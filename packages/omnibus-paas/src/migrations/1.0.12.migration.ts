/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {BlueSkyFeeRepository} from '../repositories';

@migrationScript()
export class AddBlueSkyBase implements MigrationScript {
  version = '1.0.12';
  scriptName = AddBlueSkyBase.name;
  description = 'Adds Bluesky base fees';

  constructor(
    @repository(BlueSkyFeeRepository)
    private blueSkyFeeRepository: BlueSkyFeeRepository,
  ) {}

  async up(): Promise<void> {
    const fixedStates = [
      {state: 'Alabama', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Alaska', exemption: 'b', type: 'Fixed', fee: 600},
      {state: 'Arizona', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'California', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Colorado', exemption: 'b', type: 'Fixed', fee: 50},
      {state: 'Connecticut', exemption: 'b', type: 'Fixed', fee: 150},
      {state: 'District of Columbia', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Georgia', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Hawaii', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Idaho', exemption: 'b', type: 'Fixed', fee: 50},
      {state: 'Iowa', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Illinois', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Kansas', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Kentucky', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Louisiana', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Maine', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Maryland', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Michigan', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Mississippi', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Missouri', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Nebraska', exemption: 'b', type: 'Fixed', fee: 200},
      {state: 'Nevada', exemption: 'b', type: 'Fixed', fee: 500},
      {state: 'New Hampshire', exemption: 'b', type: 'Fixed', fee: 500},
      {state: 'New Jersey', exemption: 'b', type: 'Fixed', fee: 500},
      {state: 'New Mexico', exemption: 'b', type: 'Fixed', fee: 350},
      {state: 'North Carolina', exemption: 'b', type: 'Fixed', fee: 350},
      {state: 'North Dakota', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Ohio', exemption: 'b', type: 'Fixed', fee: 100},
      {state: 'Oklahoma', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Oregon', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Pennsylvania', exemption: 'b', type: 'Fixed', fee: 525},
      {state: 'Rhode Island', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'South Carolina', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'Tennessee', exemption: 'b', type: 'Fixed', fee: 500},
      {state: 'Vermont', exemption: 'b', type: 'Fixed', fee: 600},
      {state: 'Virginia', exemption: 'b', type: 'Fixed', fee: 250},
      {state: 'Washington', exemption: 'b', type: 'Fixed', fee: 300},
      {state: 'West Virginia', exemption: 'b', type: 'Fixed', fee: 125},
      {state: 'Wisconsin', exemption: 'b', type: 'Fixed', fee: 200},
      {state: 'Wyoming', exemption: 'b', type: 'Fixed', fee: 200},
    ];

    const newBlueSkyFees = fixedStates.map(entry => {
      return this.blueSkyFeeRepository.create(entry);
    });

    await Promise.all(newBlueSkyFees);
  }

  async down(): Promise<void> {
    //
  }
}
