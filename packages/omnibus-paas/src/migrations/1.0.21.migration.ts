/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EmailTemplateRepository} from '../repositories';

@migrationScript()
export class AddWireSentEmailTemplate implements MigrationScript {
  version = '1.0.21';
  scriptName = AddWireSentEmailTemplate.name;
  description = 'Add new email template for sent Wire transfers';

  constructor(
    @repository(EmailTemplateRepository)
    private emailTemplateRepository: EmailTemplateRepository,
  ) {}

  async up(): Promise<void> {
    const emailTemplate = await this.emailTemplateRepository.find({
      where: {key: 'wireSentNotification'},
    });

    if (!emailTemplate || emailTemplate.length === 0) {
      await this.emailTemplateRepository.create({
        tenantId: 'global',
        key: 'wireSentNotification',
        data: {
          salutations: 'Hello',
        },
      });
    }
  }

  async down(): Promise<void> {
    //
  }
}
