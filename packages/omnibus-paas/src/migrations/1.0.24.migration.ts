/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EmailTemplateRepository} from '../repositories';

@migrationScript()
export class ChangeInviteOrganizerEmailTemplateKey implements MigrationScript {
  version = '1.0.24';
  scriptName = ChangeInviteOrganizerEmailTemplateKey.name;
  description = 'Change key of Invite Organizer email template';

  constructor(
    @repository(EmailTemplateRepository)
    private readonly emailTemplateRepository: EmailTemplateRepository,
  ) {}

  async up(): Promise<void> {
    const emailTemplates = await this.emailTemplateRepository.find({
      where: {key: 'organizerInvite'},
    });

    if (emailTemplates) {
      const changes = emailTemplates.map(e =>
        this.emailTemplateRepository.updateById(e.id, {
          key: 'inviteOrganizer',
        }),
      );
      await Promise.all(changes);
    }
  }

  async down(): Promise<void> {
    //
  }
}
