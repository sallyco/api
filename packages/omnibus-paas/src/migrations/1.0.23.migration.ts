/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {TenantRepository} from '../repositories';

@migrationScript()
export class AddFlagForCarryRecipientView implements MigrationScript {
  version = '1.0.23';
  scriptName = AddFlagForCarryRecipientView.name;
  description =
    'Add new field in tenenats to show/hide carry recipients from investor view, default to hide';

  constructor(
    @repository(TenantRepository)
    private readonly tenantRepository: TenantRepository,
  ) {}

  async up(): Promise<void> {
    const tenants = await this.tenantRepository.find({});

    const tenantsToUpdate = tenants
      .filter(
        tenant =>
          tenant.settings?.showCarryRecipientsForInvestors === null ||
          tenant.settings?.showCarryRecipientsForInvestors === undefined,
      )
      .map(tenant => {
        return this.tenantRepository.updateById(tenant.id, {
          settings: {
            ...tenant?.settings,
            showCarryRecipientsForInvestors: false,
          },
        });
      });

    await Promise.all(tenantsToUpdate);
  }

  async down(): Promise<void> {
    //
  }
}
