/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {TenantRepository} from '../repositories';

@migrationScript()
export class MoveDealLimitToDealLimitsConfig implements MigrationScript {
  version = '1.0.7';
  scriptName = MoveDealLimitToDealLimitsConfig.name;
  description =
    'Moves the max deal limit configuration to the dealLimits object that houses other limits for deals';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
  ) {}

  async up(): Promise<void> {
    const tenants = await this.tenantRepository.find();

    const updatedTenants = tenants.map(tenant => {
      return this.tenantRepository.updateById(tenant.id, {
        dealLimits: {
          maxDealCount: tenant.dealLimit,
        },
      });
    });

    await Promise.all(updatedTenants);
  }

  async down(): Promise<void> {
    //
  }
}
