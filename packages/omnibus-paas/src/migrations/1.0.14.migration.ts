import {service} from '@loopback/core';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {KeycloakService} from '../services';

@migrationScript()
export class Migration_1_0_14 implements MigrationScript {
  version = '1.0.14';
  scriptName = this.constructor.name;
  description = 'Adds additional Signer role to existing tenants';

  constructor(
    @service(KeycloakService) private keycloakService: KeycloakService,
  ) {}

  async up(): Promise<void> {
    try {
      const kcClient = await this.keycloakService.getClient();
      let realms = await kcClient.realms.find();
      realms = realms.filter(realm => realm.id !== 'master');
      for (const realm of realms) {
        const client = (
          await kcClient.clients.find({
            realm: String(realm.id),
            clientId: 'account',
          })
        )[0];
        const roles = await kcClient.clients.listRoles({
          id: String(client.id),
          realm: String(realm.id),
        });
        if (!roles.find(role => role.name === 'signer')) {
          console.log(`Adding Signer role to ${realm.id}`);
          await kcClient.clients.createRole({
            id: String(client.id),
            name: 'signer',
            realm: String(realm.id),
          });
        } else {
          console.log(`Found Signer role in ${realm.id}`);
        }
      }
    } catch (e) {
      // Ignoring because KC might not be available
    }
  }

  async down(): Promise<void> {
    //
  }
}
