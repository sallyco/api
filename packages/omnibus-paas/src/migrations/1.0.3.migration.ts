/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EntitiesRepository} from '../repositories';

@migrationScript()
export class AddBankContactInfo implements MigrationScript {
  version = '1.0.3';
  scriptName = AddBankContactInfo.name;
  description = 'add bank contact info for The Provident Bank';

  constructor(
    @repository(EntitiesRepository)
    private entityRepository: EntitiesRepository,
  ) {}

  async up(): Promise<void> {
    const entities = await this.entityRepository.find({
      where: {'bankAccount.bankName': 'The Provident Bank'},
    });

    const updateEntities = entities.map(entity => {
      return this.entityRepository.updateById(entity.id, {
        ...entity,
        bankAccount: {
          ...entity.bankAccount,
          bankContact: {
            name: 'Sara Spaulding',
            phone: '978-225-1402',
            email: 'sspaulding@bankprov.com',
          },
        },
      });
    });

    await Promise.all(updateEntities);
  }

  async down(): Promise<void> {
    //
  }
}
