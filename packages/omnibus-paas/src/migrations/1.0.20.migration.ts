/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EmailTemplateRepository, TenantRepository} from '../repositories';

@migrationScript()
export class AddContactOrganizerEmailTemplate implements MigrationScript {
  version = '1.0.20';
  scriptName = AddContactOrganizerEmailTemplate.name;
  description = 'Add new email template for Contact Organizer';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
    @repository(EmailTemplateRepository)
    private emailTemplateRepository: EmailTemplateRepository,
  ) {}

  async up(): Promise<void> {
    const emailTemplate = await this.emailTemplateRepository.find({
      where: {key: 'contactOrganizer'},
    });

    if (!emailTemplate || emailTemplate.length === 0) {
      await this.emailTemplateRepository.create({
        tenantId: 'global',
        key: 'contactOrganizer',
        data: {
          salutations: 'Hello',
          contentIntroduction: 'You have got a message from',
          buttonText: 'Go to Deal',
        },
      });
    }
  }

  async down(): Promise<void> {
    //
  }
}
