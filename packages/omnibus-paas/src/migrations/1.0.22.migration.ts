/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {CompanyRepository, TenantRepository} from '../repositories';

@migrationScript()
export class ImplementTenantAndCompanyModelChanges implements MigrationScript {
  version = '1.0.22';
  scriptName = ImplementTenantAndCompanyModelChanges.name;
  description =
    'Convert services field in Tenant to an array, convert individual/business fields in Company to an array';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
    @repository(CompanyRepository)
    private companyRepository: CompanyRepository,
  ) {}

  async up(): Promise<void> {
    // Update Tenant model

    // // Update Services
    const tenantsListForService = await this.tenantRepository.find({
      where: {services: {exists: true}},
    });
    const tenantsToUpdateService = tenantsListForService.map(tenant => {
      if (tenant.services) {
        return this.tenantRepository.updateById(tenant.id, {
          tenantServices: [tenant.services],
        });
      }
    });
    const removeServiceOnTenant = this.tenantRepository.updateAll(<any>{
      $unset: {services: 1},
    });
    // // Update Frequency
    const tenantsListForFrequency = await this.tenantRepository.find({
      where: {paymentMethod: {exists: true}},
    });
    const tenantsToUpdateFrequency = tenantsListForFrequency.map(tenant => {
      if (
        tenant.paymentMethod?.frequency &&
        tenant.paymentMethod?.frequency === 'Anually'
      ) {
        return this.tenantRepository.updateById(tenant.id, {
          paymentMethod: {
            ...tenant.paymentMethod,
            frequency: 'Annually',
          },
        });
      }
    });

    // Update Company model

    // // Update Individuals
    const companiesListForIndividual = await this.companyRepository.find({
      where: {individual: {exists: true}},
    });
    const companiesToUpdateIndividual = companiesListForIndividual.map(
      company => {
        if (company.individual) {
          return this.companyRepository.updateById(company.id, {
            individuals: [company.individual],
            individual: null,
          });
        }
      },
    );
    const removeIndividualOnCompany = this.tenantRepository.updateAll(<any>{
      $unset: {individual: {}},
    });
    // // Update Businesses
    const companiesListForBusiness = await this.companyRepository.find({
      where: {business: {exists: true}},
    });
    const companiesToUpdateBusiness = companiesListForBusiness.map(company => {
      if (company.business) {
        return this.companyRepository.updateById(company.id, {
          businesses: [company.business],
          business: null,
        });
      }
    });
    const removeBusinessOnCompany = this.tenantRepository.updateAll(<any>{
      $unset: {business: {}},
    });

    await Promise.all([
      ...tenantsToUpdateService,
      ...tenantsToUpdateFrequency,
      ...companiesToUpdateIndividual,
      ...companiesToUpdateBusiness,
      removeServiceOnTenant,
      removeIndividualOnCompany,
      removeBusinessOnCompany,
    ]);
  }

  async down(): Promise<void> {
    //
  }
}
