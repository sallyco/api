/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EntitiesRepository} from '../repositories';

@migrationScript()
export class ConvertPurchaseAgreementInEntityToArray
  implements MigrationScript
{
  version = '1.0.16';
  scriptName = ConvertPurchaseAgreementInEntityToArray.name;
  description = 'Convert Purchase Agreement field in Entities to an array';

  constructor(
    @repository(EntitiesRepository)
    private entitiesRepository: EntitiesRepository,
  ) {}

  async up(): Promise<void> {
    const entities = await this.entitiesRepository.find();

    const updatedEntities = entities.map(entity => {
      const purchaseAgreement =
        entity?.entityDocuments?.purchaseAgreement ?? null;
      if (purchaseAgreement && !Array.isArray(purchaseAgreement)) {
        return this.entitiesRepository.updateById(entity.id, {
          entityDocuments: {
            ...entity.entityDocuments,
            purchaseAgreement: [entity.entityDocuments.purchaseAgreement],
          },
        });
      }
    });

    await Promise.all(updatedEntities);
  }

  async down(): Promise<void> {
    //
  }
}
