/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {
  AssetRepository,
  DealRepository,
  ProfileRepository,
} from '../repositories';

@migrationScript()
export class PropertyImportUpdate implements MigrationScript {
  version = '1.0.2';
  scriptName = PropertyImportUpdate.name;
  description = 'Update Properties from Schema Update for Imports';

  constructor(
    @repository(DealRepository)
    private dealRepository: DealRepository,
    @repository(AssetRepository)
    private assetRepository: AssetRepository,
    @repository(ProfileRepository)
    private profileRepository: ProfileRepository,
  ) {}

  async up(): Promise<void> {
    // DEALS migrations
    const marketingSloganDeals = await this.dealRepository.find({
      where: {marketingSlogan: {exists: true}},
    });
    const marketingLogoDeals = await this.dealRepository.find({
      where: {marketingLogo: {exists: true}},
    });

    const updatemarketingSloganOnDeal = marketingSloganDeals.map(deal => {
      // eslint-disable-next-line no-void
      void this.dealRepository.updateById(deal.id, {
        marketing: {
          tagline: deal.marketingSlogan,
        },
      });
    });
    const updatemarketingLogoOnDeal = marketingLogoDeals.map(deal => {
      // eslint-disable-next-line no-void
      void this.dealRepository.updateById(deal.id, {
        marketing: {
          logo: deal.marketingLogo,
        },
      });
    });

    const raiseAmountDeals = await this.dealRepository.find({
      where: {raiseAmount: {exists: true}},
    });

    const updateTargetRaiseAmountDeals = raiseAmountDeals.map(deal => {
      // eslint-disable-next-line no-void
      void this.dealRepository.updateById(deal.id, {
        targetRaiseAmount: deal.raiseAmount,
      });
    });

    const removeRaiseAmountOnDeal = this.dealRepository.updateAll(<any>{
      $unset: {raiseAmount: true},
    });
    const removeMarketingSloganOnDeal = this.dealRepository.updateAll(<any>{
      $unset: {marketingSlogan: true},
    });
    const removeMarketingLogoOnDeal = this.dealRepository.updateAll(<any>{
      $unset: {marketingLogo: true},
    });

    await Promise.all([
      updatemarketingSloganOnDeal,
      removeMarketingSloganOnDeal,
      updatemarketingLogoOnDeal,
      removeMarketingLogoOnDeal,
      updateTargetRaiseAmountDeals,
      removeRaiseAmountOnDeal,
    ]);

    // Assets Migrations
    const assetTypeAssets = await this.assetRepository.find({
      where: {assetType: {exists: true}},
    });
    const pithcAssets = await this.assetRepository.find({
      where: {pithcDoc: {exists: true}},
    });

    const updateAssetType = pithcAssets.map(asset => {
      // eslint-disable-next-line no-void
      void this.assetRepository.updateById(asset.id, {
        type: asset.assetType,
      });
    });
    const updateAssetPitchDoc = assetTypeAssets.map(asset => {
      // eslint-disable-next-line no-void
      void this.assetRepository.updateById(asset.id, {
        pitchDoc: asset.pithcDoc,
      });
    });

    const removeAssetTypeOnAsset = this.assetRepository.updateAll(<any>{
      $unset: {assetType: true},
    });
    const removePithcDocOnAsset = this.assetRepository.updateAll(<any>{
      $unset: {pithcDoc: true},
    });

    await Promise.all([
      updateAssetType,
      removeAssetTypeOnAsset,
      updateAssetPitchDoc,
      removePithcDocOnAsset,
    ]);

    // PROFILE migrations
    const legalNameProfiles = await this.profileRepository.find({
      where: {legalName: {exists: true}},
    });
    const passportProfiles = await this.profileRepository.find({
      where: {passport: {exists: true}},
    });
    const isUSPersonProfiles = await this.profileRepository.find({
      where: {isUSPerson: {exists: true}},
    });

    const updateProfileName = legalNameProfiles.map(profile => {
      // eslint-disable-next-line no-void
      void this.profileRepository.updateById(profile.id, {
        name: profile.legalName,
      });
    });
    const updateProfilePassportId = passportProfiles.map(profile => {
      // eslint-disable-next-line no-void
      void this.profileRepository.updateById(profile.id, {
        passportId: profile.passport,
      });
    });
    const updateProfileIsUSBased = isUSPersonProfiles.map(profile => {
      // eslint-disable-next-line no-void
      void this.profileRepository.updateById(profile.id, {
        isUSBased: profile.isUSPerson,
      });
    });

    const removeLegalNameOnProfile = this.profileRepository.updateAll(<any>{
      $unset: {legalName: true},
    });
    const removePassportOnProfile = this.profileRepository.updateAll(<any>{
      $unset: {passport: true},
    });
    const removeIsUSPersonOnProfile = this.profileRepository.updateAll(<any>{
      $unset: {isUSPerson: true},
    });

    await Promise.all([
      updateProfileName,
      removeLegalNameOnProfile,
      updateProfilePassportId,
      removePassportOnProfile,
      updateProfileIsUSBased,
      removeIsUSPersonOnProfile,
    ]);
  }

  async down(): Promise<void> {
    //
  }
}
