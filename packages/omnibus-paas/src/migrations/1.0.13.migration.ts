/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {BlueSkyFeeRepository} from '../repositories';

@migrationScript()
export class AddBlueSkyBaseToo implements MigrationScript {
  version = '1.0.13';
  scriptName = AddBlueSkyBaseToo.name;
  description = 'More Adds Bluesky base fees';

  constructor(
    @repository(BlueSkyFeeRepository)
    private blueSkyFeeRepository: BlueSkyFeeRepository,
  ) {}

  async up(): Promise<void> {
    const noState = [
      {state: 'Florida', exemption: 'b', type: 'No File'},
      {state: 'Indiana', exemption: 'b', type: 'No Fee'},
      {state: 'South Dakota', exemption: 'b', type: 'No Fee'},
      //don't try to seed tiered
      {state: 'New York', exemption: 'b', type: 'No Fee'},
      //'New York': [
      //  {amount: 300, breakpoint: 500000},
      //  {amount: 1200, breakpoint: 0},
      //],
      {state: 'Massachusetts', exemption: 'b', type: 'No Fee'},
      //Massachusetts: [
      //  {amount: 250, breakpoint: 2000000},
      //  {amount: 500, breakpoint: 7500000},
      //  {amount: 750, breakpoint: 0},
      //],
      {state: 'Utah', exemption: 'b', type: 'No Fee'},
      //Utah: [
      //  {amount: 0, breakpoint: 500000},
      //  {amount: 100, breakpoint: 0},
      //],
    ];
    const newNS = noState.map(entry => {
      return this.blueSkyFeeRepository.create(entry);
    });
    await Promise.all(newNS);

    const variableState = [
      {
        state: 'Arkansas',
        exemption: 'b',
        type: 'Variable',
        fee: 0,
        variablePercent: 0.001,
        variableBase: 'State Offering',
        variableMin: 100,
        variableMax: 500,
      },
      {
        state: 'Delaware',
        exemption: 'b',
        type: 'Variable',
        fee: 0,
        variablePercent: 0.005,
        variableBase: 'State Offering',
        variableMin: 200,
        variableMax: 1000,
      },
      {
        state: 'Minnesota',
        exemption: 'b',
        type: 'Fixed & Variable',
        fee: 100,
        variablePercent: 0.001,
        variableBase: 'State Offering',
        variableMin: 0,
        variableMax: 200,
      },
      {
        state: 'Montana',
        exemption: 'b',
        type: 'Fixed & Variable',
        fee: 200,
        variablePercent: 0.001,
        variableBase: 'State Offering',
        variableMin: 0,
        variableMax: 800,
      },
      {
        state: 'Texas',
        exemption: 'b',
        type: 'Variable',
        fee: 0,
        variablePercent: 0.001,
        variableBase: 'EDGAR Offering',
        variableMin: 0,
        variableMax: 500,
      },
    ];
    const newVS = variableState.map(entry => {
      return this.blueSkyFeeRepository.create(entry);
    });
    await Promise.all(newVS);
  }

  async down(): Promise<void> {
    //
  }
}
