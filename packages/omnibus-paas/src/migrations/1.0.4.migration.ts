/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {SubscriptionRepository} from '../repositories';

@migrationScript()
export class AddMultipleTransactions implements MigrationScript {
  version = '1.0.4';
  scriptName = AddMultipleTransactions.name;
  description = 'Adds Multiple transaction support to subscriptions';

  constructor(
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
  ) {}

  async up(): Promise<void> {
    const subscriptions = await this.subscriptionRepository.find({
      where: {
        transactionId: {
          exists: true,
          neq: null,
        },
      },
    });

    const updatedSubscriptions = subscriptions.map(subscription => {
      return this.subscriptionRepository.updateById(subscription.id, {
        ...subscription,
        transactionIds: [subscription.transactionId],
      });
    });

    await Promise.all(updatedSubscriptions);
  }

  async down(): Promise<void> {
    //
  }
}
