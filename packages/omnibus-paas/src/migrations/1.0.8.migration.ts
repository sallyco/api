import {repository, WhereBuilder} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {EntitiesRepository} from '../repositories';

@migrationScript()
export class MigrationAddBankSwiftCode implements MigrationScript {
  version = '1.0.8';
  scriptName = MigrationAddBankSwiftCode.name;
  description = 'Add BankProv Swift Code to Banking Information';

  constructor(
    @repository(EntitiesRepository)
    private entityRepository: EntitiesRepository,
  ) {}

  async up(): Promise<void> {
    const entitiesWhere = new WhereBuilder({
      'bankAccount.bankName': 'The Provident Bank',
    });
    const entities = await this.entityRepository.find(entitiesWhere);

    const updateEntities = entities.map(entity => {
      return this.entityRepository.updateById(entity.id, {
        ...entity,
        bankAccount: {
          ...entity.bankAccount,
          swiftCode: 'PRDIUS33',
        },
      });
    });

    await Promise.all(updateEntities);
  }

  async down(): Promise<void> {
    //
  }
}
