/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {CloseRepository, SubscriptionRepository} from '../repositories';

@migrationScript()
export class CalculateOwn implements MigrationScript {
  version = '1.0.19';
  scriptName = CalculateOwn.name;
  description = 'Calculate the ownership percentage on old deals';

  constructor(
    @repository(CloseRepository)
    private closeRepository: CloseRepository,
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
  ) {}

  async up(): Promise<void> {
    const closes = await this.closeRepository.find();

    for (let i = 0; i < closes.length; i++) {
      if (!closes[i].statement?.totalProceeds) continue;
      const total = closes[i].statement.totalProceeds;
      const updateSubs = closes[i].subscriptions.map(id => {
        return this.subscriptionRepository.findById(id).then(sub => {
          return this.subscriptionRepository.updateById(sub.id, {
            ...sub,
            ownershipPercentageAtClose: sub.amount / total,
          });
        });
      });

      await Promise.all(updateSubs);
    }
  }

  async down(): Promise<void> {
    //
  }
}
