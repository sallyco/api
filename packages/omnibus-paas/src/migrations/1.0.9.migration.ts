/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {TenantRepository} from '../repositories';

@migrationScript()
export class Add99InvestorPerDealLimit implements MigrationScript {
  version = '1.0.9';
  scriptName = Add99InvestorPerDealLimit.name;
  description =
    'By default put a config of maximum 99 investors per deal on each tenant';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
  ) {}

  async up(): Promise<void> {
    const tenants = await this.tenantRepository.find();

    const updatedTenants = tenants.map(tenant => {
      return this.tenantRepository.updateById(tenant.id, {
        dealLimits: {
          ...tenant?.dealLimits,
          perDealMaxInvestorCount: 99,
        },
      });
    });

    await Promise.all(updatedTenants);
  }

  async down(): Promise<void> {
    //
  }
}
