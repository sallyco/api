/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {TenantRepository, WhiteLabelRepository} from '../repositories';

@migrationScript()
export class SeedWhiteLabels implements MigrationScript {
  version = '1.0.18';
  scriptName = SeedWhiteLabels.name;
  description = 'Copy current theme information to the white label schema';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
    @repository(WhiteLabelRepository)
    private whiteLabelRepository: WhiteLabelRepository,
  ) {}

  async up(): Promise<void> {
    const tenants = await this.tenantRepository.find();

    const addWhiteLabel = tenants.map(tenant => {
      return this.whiteLabelRepository
        .create({
          images: {
            logo: tenant?.assets?.images?.logo,
            logoInverted: tenant?.assets?.images?.logoInverted,
          },
          theme: {
            palette: {
              type: 'light',
              primary: {
                main: tenant?.assets?.colors?.primaryColor,
              },
              secondary: {
                main: tenant?.assets?.colors?.secondaryColor,
              },
            },
          },
          tenantId: tenant.id,
        })
        .then(wl => {
          return this.tenantRepository.updateById(tenant.id, {
            ...tenant,
            whiteLabelId: wl.id,
          });
        });
    });

    await Promise.all(addWhiteLabel);
  }

  async down(): Promise<void> {
    //
  }
}
