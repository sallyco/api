/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {
  AssetRepository,
  CloseRepository,
  DealRepository,
  EntitiesRepository,
  FileRepository,
  SubscriptionRepository,
  TenantRepository,
} from '../repositories';

@migrationScript()
export class AddTenantIdToFiles implements MigrationScript {
  version = '1.0.17';
  scriptName = AddTenantIdToFiles.name;
  description =
    'Adding tenantId field to File documents, for viewing in admin UI';

  constructor(
    @repository(DealRepository)
    private dealRepository: DealRepository,
    @repository(FileRepository)
    private fileRepository: FileRepository,
    @repository(EntitiesRepository)
    private entitiesRepository: EntitiesRepository,
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
    @repository(CloseRepository)
    private closeRepository: CloseRepository,
    @repository(AssetRepository)
    private assetRepository: AssetRepository,
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
  ) {}

  async up(): Promise<void> {
    const filesWithoutTenant = await this.fileRepository.find({
      where: {
        tenantId: {
          exists: false,
        },
      },
    });

    const allEntities = await this.entitiesRepository.find();

    const fileUpdations: Promise<any>[] = [];
    for (const file of filesWithoutTenant) {
      // Check if file is in deals
      const dealWithFile = await this.dealRepository.findOne({
        where: {
          or: [
            {
              organizerFiles: {
                regexp: '.*' + file.id + '.*',
              },
            },
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
          ],
        },
      });
      if (dealWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: dealWithFile.tenantId,
          }),
        );
        continue;
      }

      // Check if file is in subscriptions
      const subscriptionWithFile = await this.subscriptionRepository.findOne({
        where: {
          or: [
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
            {closeDoc: file.id},
          ],
        },
      });
      if (subscriptionWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: subscriptionWithFile.tenantId,
          }),
        );
        continue;
      }

      // Check if file is in closes
      const closeWithFile = await this.closeRepository.findOne({
        where: {
          or: [
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
          ],
        },
      });
      if (closeWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: closeWithFile.tenantId,
          }),
        );
        continue;
      }

      // Check if file is in assets
      const assetWithFile = await this.assetRepository.findOne({
        where: {
          or: [
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
          ],
        },
      });
      if (assetWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: assetWithFile.tenantId,
          }),
        );
        continue;
      }

      // Check if file is in tenants
      const tenantWithFile = await this.tenantRepository.findOne({
        where: {
          or: [
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
          ],
        },
      });
      if (tenantWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: tenantWithFile.id,
          }),
        );
        continue;
      }

      // Check if file is in entities
      const entityWithFile = await this.entitiesRepository.findOne({
        where: {
          or: [
            {
              files: {
                regexp: '.*' + file.id + '.*',
              },
            },
          ],
        },
      });
      if (entityWithFile) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: entityWithFile.tenantId,
          }),
        );
        continue;
      }

      // Check if file is in entity documents
      const entityDocumentWithFile = allEntities.filter(
        entity =>
          entity.entityDocuments &&
          (entity.entityDocuments.operatingAgreement === file.id ||
            entity.entityDocuments.privatePlacementMemorandum === file.id ||
            entity.entityDocuments.subscriptionAgreement === file.id ||
            entity.entityDocuments.einLetter === file.id ||
            entity.entityDocuments.purchaseAgreement === file.id),
      );
      if (entityDocumentWithFile && entityDocumentWithFile.length > 0) {
        fileUpdations.push(
          this.fileRepository.updateById(file.id, {
            tenantId: entityDocumentWithFile[0].tenantId,
          }),
        );
        continue;
      }
    }

    await Promise.all(fileUpdations);
  }

  async down(): Promise<void> {
    //
  }
}
