/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {DealRepository} from '../repositories';

@migrationScript()
export class DealAssetArray implements MigrationScript {
  version = '1.0.1';
  scriptName = DealAssetArray.name;
  description = 'make deal assetId and array';

  constructor(
    @repository(DealRepository)
    private dealRepository: DealRepository,
  ) {}

  async up(): Promise<void> {
    const deals = await this.dealRepository.find({
      where: {assetId: {exists: true}},
    });

    const updateAssetsOnDeal = deals.map(deal => {
      // eslint-disable-next-line no-void
      void this.dealRepository.updateById(deal.id, {
        assetIds: [deal.assetId],
      });
    });

    const removeAssetOnDeal = this.dealRepository.updateAll(<any>{
      $unset: {assetId: 0},
    });

    await Promise.all([updateAssetsOnDeal, removeAssetOnDeal]);
  }

  async down(): Promise<void> {
    //
  }
}
