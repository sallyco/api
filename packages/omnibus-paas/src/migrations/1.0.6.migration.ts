/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {DealRepository} from '../repositories';

@migrationScript()
export class AddRequireQualifiedPurchaser implements MigrationScript {
  version = '1.0.6';
  scriptName = AddRequireQualifiedPurchaser.name;
  description =
    'Adds a boolean field to deals called requireQualifiedPurchaser';

  constructor(
    @repository(DealRepository)
    private dealRepository: DealRepository,
  ) {}

  async up(): Promise<void> {
    const deals = await this.dealRepository.find();

    const updatedDeals = deals.map(deal => {
      return this.dealRepository.updateById(deal.id, {
        requireQualifiedPurchaser: false,
      });
    });

    await Promise.all(updatedDeals);
  }

  async down(): Promise<void> {
    //
  }
}
