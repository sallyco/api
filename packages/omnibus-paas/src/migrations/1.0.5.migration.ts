/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {SubscriptionRepository} from '../repositories';

@migrationScript()
export class AddSubscriptionAcquisition implements MigrationScript {
  version = '1.0.5';
  scriptName = AddSubscriptionAcquisition.name;
  description =
    'Sets the acquisition method for previous subscriptions to INVITATION';

  constructor(
    @repository(SubscriptionRepository)
    private subscriptionRepository: SubscriptionRepository,
  ) {}

  async up(): Promise<void> {
    const subscriptions = await this.subscriptionRepository.find();

    const updatedSubscriptions = subscriptions.map(subscription => {
      return this.subscriptionRepository.updateById(subscription.id, {
        ...subscription,
        acquisitionMethod: 'INVITATION',
      });
    });

    await Promise.all(updatedSubscriptions);
  }

  async down(): Promise<void> {
    //
  }
}
