/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {
  TenantRepository,
  ProfileRepository,
  EntitiesRepository,
} from '../repositories';

@migrationScript()
export class UpdateFieldNamesWithId implements MigrationScript {
  version = '1.0.10';
  scriptName = UpdateFieldNamesWithId.name;
  description = 'Updates manager & masterEntity to include ID suffix';

  constructor(
    @repository(TenantRepository)
    private tenantRepository: TenantRepository,
    @repository(ProfileRepository)
    private profileRepository: ProfileRepository,
    @repository(EntitiesRepository)
    private entitiesRepository: EntitiesRepository,
  ) {}

  async up(): Promise<void> {
    //tenant
    const tenants = await this.tenantRepository.find({
      where: {
        or: [
          {
            manager: {
              exists: true,
            },
          },
          {
            masterEntity: {
              exists: true,
            },
          },
        ],
      },
    });

    const renameTenantFields = tenants.map(tenant => {
      return this.tenantRepository.updateById(tenant.id, {
        $rename: {manager: 'managerId', masterEntity: 'masterEntityId'},
      } as any);
    });

    await Promise.all([renameTenantFields]);

    //profile
    const profiles = await this.profileRepository.find({
      where: {
        or: [
          {
            manager: {
              exists: true,
            },
          },
          {
            masterEntity: {
              exists: true,
            },
          },
        ],
      },
    });

    const renameProfileFields = profiles.map(profile => {
      return this.profileRepository.updateById(profile.id, {
        $rename: {manager: 'managerId', masterEntity: 'masterEntityId'},
      } as any);
    });

    await Promise.all([renameProfileFields]);

    //entity
    const entities = await this.entitiesRepository.find({
      where: {
        or: [
          {
            manager: {
              exists: true,
            },
          },
          {
            masterEntity: {
              exists: true,
            },
          },
        ],
      },
    });

    const renameEntityFields = entities.map(entity => {
      return this.entitiesRepository.updateById(entity.id, {
        $rename: {manager: 'managerId', masterEntity: 'masterEntityId'},
      } as any);
    });

    await Promise.all([renameEntityFields]);
  }

  async down(): Promise<void> {
    //
  }
}
