/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {repository} from '@loopback/repository';
import {MigrationScript, migrationScript} from 'loopback4-migration';
import {ProfileRepository} from '../repositories';
import {service} from '@loopback/core';
import {KycAmlOnfidoService} from '../services';

@migrationScript()
export class Migration_1_0_11 implements MigrationScript {
  version = '1.0.11';
  scriptName = this.constructor.name;
  description = 'Updates KycAml Records to use a subset of reports';

  constructor(
    @repository(ProfileRepository)
    private profileRepository: ProfileRepository,
    @service(KycAmlOnfidoService)
    private kycAml: KycAmlOnfidoService,
  ) {}

  async up(): Promise<void> {
    /**
     * Fix Older KYC Checks to use new calculation
     */
    const older_profiles = await this.profileRepository.find({
      where: {
        'kycAml.result': 'CONSIDER',
      },
    });
    for (const profile of older_profiles) {
      profile.kycAml.result = await this.kycAml.calculateCheckResult(
        profile.kycAml?.providerMeta,
      );
      await this.profileRepository.update(profile);
    }

    /**
     * Fix Address not valid exceptions
     */
    const exception_profiles = await this.profileRepository.find({
      where: {
        and: [
          {
            'kycAml.status': 'EXCEPTION',
          },
          {
            'kycAml.reason': 'Address is Invalid',
          },
        ],
      },
    });
    for (const profile of exception_profiles) {
      profile.kycAml = {};
      await this.profileRepository.update(profile);
    }
  }

  async down(): Promise<void> {}
}
