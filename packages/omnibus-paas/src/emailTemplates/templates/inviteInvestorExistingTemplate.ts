import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface InviteInvestorExistingEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}
export function InviteInvestorExistingTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'inviteInvestorExisting.mjml'),
    'utf8',
  );
}
