/* eslint-disable @typescript-eslint/no-explicit-any */
import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';

export interface ResetPasswordEmailData extends EmailData {
  recipient: {
    email: string;
  };
  resetUrl: string;
}

export const ResetPasswordEmailTemplate = function () {
  return fs.readFileSync(path.resolve(__dirname, 'resetPassword.mjml'), 'utf8');
};
