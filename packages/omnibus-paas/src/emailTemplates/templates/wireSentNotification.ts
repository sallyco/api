/* eslint-disable @typescript-eslint/no-explicit-any */
import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';

export interface WireSentNotificationEmailData extends EmailData {
  recipient: {
    email: string;
  };
  amount: number;
  entity: {
    name: string;
  };
}

export const WireSentNotificationTemplate = function () {
  return fs.readFileSync(
    path.resolve(__dirname, 'wireSentNofications.mjml'),
    'utf8',
  );
};
