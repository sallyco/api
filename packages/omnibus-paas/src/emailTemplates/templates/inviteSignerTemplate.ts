import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';
export interface InviteSignerEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export function InviteSignerTemplate() {
  return fs.readFileSync(path.resolve(__dirname, 'inviteSigner.mjml'), 'utf8');
}
