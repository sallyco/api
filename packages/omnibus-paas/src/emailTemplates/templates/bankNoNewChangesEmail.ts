import {EmailData} from '../../services';

export interface BankNoNewChangesEmailTemplate extends EmailData {
  organizationName: string;
}

export function BankNoNewChangesEmailTemplate() {
  return `
    <p>There have been no organizational or ownership changes to {{organizationName}} in the last 7 days.</p>
  `;
}
