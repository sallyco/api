import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';
export interface ProfileNeedsUpdateEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export function ProfileNeedsUpdateTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'profileNeedsUpdate.mjml'),
    'utf8',
  );
}
