import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface RemoveInviteEmailData extends EmailData {
  recipient: {
    firstName?: string;
  };
  message: string;
}
export function RemoveInviteTemplate() {
  return fs.readFileSync(path.resolve(__dirname, 'removeInvite.mjml'), 'utf8');
}
