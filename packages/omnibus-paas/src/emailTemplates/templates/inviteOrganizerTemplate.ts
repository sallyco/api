import {UserProfile} from '@loopback/security';
import * as fs from 'fs';
import * as path from 'path';
import {Company} from '../../models';
import {Tenant} from '../../multi-tenancy';
import {EmailData} from '../../services';
export interface InviteOrganizerEmailData extends EmailData {
  sender: UserProfile;
  recipient: {
    firstName?: string;
    lastName?: string;
    email: string;
  };
  tenant: Tenant;
  company: Company;
  founder: {
    name: string;
    email: string;
  };
  passLink: string;
  acceptLink: string;
}

export function InviteOrganizerTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'organizerInvite.mjml'),
    'utf8',
  );
}
