import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface ContactOrganizerEmailData extends EmailData {
  recipient: {
    email: string;
    firstName: string;
  };
  deal: {
    name: string;
  };
  sender: {
    name: string;
    email: string;
    phone: string;
  };
  messageContent: string;
  returnUrl: string;
}
export function ContactOrganizerEmailTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'contactOrganizer.mjml'),
    'utf8',
  );
}
