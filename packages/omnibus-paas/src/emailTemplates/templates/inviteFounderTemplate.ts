import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';
export interface InviteFounderEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export function InviteFounderTemplate() {
  return fs.readFileSync(path.resolve(__dirname, 'inviteFounder.mjml'), 'utf8');
}
