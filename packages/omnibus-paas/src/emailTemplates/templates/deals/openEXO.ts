import {EmailData} from '../../../services';
import fs from 'fs';
import path from 'path';
export interface OpenEXOEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export function OpenEXOTemplate() {
  return fs.readFileSync(path.resolve(__dirname, 'openEXO.mjml'), 'utf8');
}
