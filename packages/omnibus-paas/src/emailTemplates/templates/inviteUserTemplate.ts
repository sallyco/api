import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface InviteUserTemplate extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}
export function InviteUserTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'inviteUserTemplate.mjml'),
    'utf8',
  );
}
