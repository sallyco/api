import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface InviteAccountAdminUserTemplate extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}
export function InviteAccountAdminUserTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'inviteAccountAdminUserTemplate.mjml'),
    'utf8',
  );
}
