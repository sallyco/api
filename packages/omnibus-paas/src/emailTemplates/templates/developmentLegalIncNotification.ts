import {EmailData} from '../../services';
export interface DevelopmentLegalIncNotificationData extends EmailData {
  orderId: string;
}

export function DevelopmentLegalncNotificationTemplate() {
  return `
    <p>Please process the following order ID on UAT: # {{orderId}}</p>
  `;
}
