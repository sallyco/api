import {EmailData} from '../../services';

export interface OnfidoConsiderEmailData extends EmailData {
  name: string;
  returnUrl: string;
}
export function OnfidoConsiderTemplate() {
  return `<html>
  <head>
  <style>
        .outer-container {
            height: 100%;
            width: 100%;
            text-align: center;
            background: white;
        }

        .inner-container {
            margin: 0 auto;
            padding-top: 4rem;
            padding-bottom: 4rem;
            max-width: 30rem;
            color: black;
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
        }

        .img {
            width: 10rem
            border: none !important;
            border-radius: 2px;
            background-color: grey;
        }

        .top-margin {
            margin-top: 1rem;
            font-size: 12px;
        }

        .link {
        	color: white;
        }

        @media screen and (max-width: 400px) {
            .inner-container{
            width: 14rem;
        }
}
  </style>
  </head>

  <body>
  <div
   class="outer-container"
  >
    <div
      class="inner-container"
    >
      <img src="https://marketing-images-gbt.s3.us-east-2.amazonaws.com/icon.png"/>
      <p> Hello {{name}},
      <p>There was an issue verifying your identity based on the information you provided</p>
      <p>
        <a class="link" href="{{returnUrl}}">Click here</a>
        and follow the instructions to upload a photo ID or any other required steps.
      </p>

      <div class="top-margin">
        This email was sent from Glassboard, the leading fund management
        software.
      </div>

      <div class="top-margin">
      <a class="link" href="http://glassboardtech.com/">Glassboard Tech</a>
      </div>

    </div>
  </div>

  </body>
  </html>
    `;
}
