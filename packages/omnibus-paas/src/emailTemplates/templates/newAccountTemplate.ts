import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';

export interface NewAccountEmailData extends EmailData {
  recipient: {
    firstName: string;
    email: string;
  };
  verifyUrl: string;
}
export function NewAccountTemplate() {
  return fs.readFileSync(path.resolve(__dirname, 'newAccount.mjml'), 'utf8');
}
