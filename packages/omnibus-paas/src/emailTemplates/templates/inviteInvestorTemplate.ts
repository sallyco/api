import {EmailData} from '../../services';
import fs from 'fs';
import path from 'path';
export interface InviteInvestorEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export function InviteInvestorTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'inviteInvestor.mjml'),
    'utf8',
  );
}
