import fs from 'fs';
import path from 'path';
import {EmailData} from '../../services';

export interface NudgeInvestorExistingEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
  subscription: {
    status: string;
  };
}
export function NudgeInvestorExistingTemplate() {
  return fs.readFileSync(
    path.resolve(__dirname, 'nudgeInvestorExisting.mjml'),
    'utf8',
  );
}
