import {
  AuthenticationComponent,
  registerAuthenticationStrategy,
} from '@loopback/authentication';
import {AuthorizationComponent} from './authorization';
import {JWTService} from './authorization/services';
import {BcryptHasher} from './authorization/services/hash.password.bcryptjs';
import {BootMixin} from '@loopback/boot';
import {MigrationComponent, MigrationBindings} from 'loopback4-migration';
import {
  ApplicationConfig,
  BindingKey,
  createBindingFromClass,
} from '@loopback/core';
import {MetricsComponent} from '@loopback/extension-metrics';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication, RestBindings} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import multer from 'multer';
import path from 'path';
import {JWTAuthenticationStrategy} from './authentication-strategies/jwt.strategy';
import {
  FILE_UPLOAD_SERVICE,
  MinIOServiceBindings,
  PackageBindings,
  PackageFactory,
  PasswordHasherBindings,
  STORAGE_DIRECTORY,
  TokenServiceBindings,
  TokenServiceConstants,
} from './keys';
import {LogComponent} from './logger';
import {
  NotificationsComponent,
  NotificationBindings,
  NodemailerBindings,
  NodemailerProvider,
} from 'loopback4-notifications';
import {MultiTenancyBindings} from './multi-tenancy';
import {MultiTenancyComponent} from './multi-tenancy/component';
import {MySequence} from './sequence';
import {MinIOService} from './services';
import {CronComponent} from '@loopback/cron';
import {EntitiesBankingCron} from './crons/entities-banking.cron';
import {MongodbDataSource} from './datasources';
import APISpec from './APISpec';

export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}

export const PackageKey = BindingKey.create<PackageInfo>('application.package');

export class OmnibusPaasApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(
    options: ApplicationConfig = {
      rest: {
        cors: {
          allowedHeaders: ['X-Total-Count'],
        },
        expressSettings: {
          'x-powered-by': false,
        },
      },
      primaryDatabase: MongodbDataSource.dataSourceName,
    },
  ) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);
    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    //Configure Authentication
    this.component(AuthenticationComponent);
    this.component(AuthorizationComponent);

    this.bind(RestBindings.REQUEST_BODY_PARSER_OPTIONS).to({limit: '512mb'});

    //    const authBinding = this.component(AuthorizationComponent);
    //    const authOptions: AuthorizationOptions = {
    //      precedence: AuthorizationDecision.DENY,
    //      defaultDecision: AuthorizationDecision.DENY,
    //    };
    //    this.configure(authBinding.key).to(authOptions);
    //    this.bind('authorizationProviders.authorizer-provider')
    //      .toProvider(AuthorizationProvider)
    //      .tag(AuthorizationTags.AUTHORIZER);

    this.component(MetricsComponent);
    registerAuthenticationStrategy(this, JWTAuthenticationStrategy);

    //Configure Tenancy
    this.component(MultiTenancyComponent);
    this.configure(MultiTenancyBindings.ACTION).to({
      strategyNames: ['jwt', 'header', 'query'],
    });

    //Configure Logger
    this.component(LogComponent);

    // Configure migration component
    this.component(MigrationComponent);

    this.bind(MigrationBindings.CONFIG).to({
      appVersion: '1.0.24', //database version
      dataSourceName: options.primaryDatabase,
      modelName: 'Migration',
    });

    // Configure Notifications
    this.component(NotificationsComponent);
    this.bind(NodemailerBindings.Config).to({
      host: process.env.SMTP_HOST,
      port: parseInt(process.env.SMTP_PORT ?? '25'),
      ignoreTLS: JSON.parse(process.env.SMTP_NO_SSL ?? 'false'),
      auth: {
        user: process.env.SMTP_AUTH_USER,
        pass: process.env.SMTP_AUTH_PASS,
      },
      tls: {
        rejectUnauthorized: true,
      },
    });
    this.bind(NotificationBindings.EmailProvider).toProvider(
      NodemailerProvider,
    );

    this.configure(RestBindings.ERROR_WRITER_OPTIONS).to({
      debug: true,
    });

    this.configureFileUpload(options.fileStorageDirectory);
    this.component(CronComponent);
    this.add(createBindingFromClass(EntitiesBankingCron));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.setUpBindings();

    this.projectRoot = __dirname;

    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        dirs: ['controllers'],
        extensions: ['.controller.ts'],
        nested: true,
      },
      datasources: {
        dirs: ['datasources'],
        extensions: '.datasource.ts',
        nested: true,
      },
      interceptors: {
        dirs: ['interceptors'],
        extensions: '.interceptor.ts',
        nested: true,
      },
      observers: {
        dirs: ['observers'],
        extensions: '.observer.ts',
        nested: true,
      },
      models: {
        dirs: ['models'],
        extensions: '.model.ts',
        nested: true,
      },
      repositories: {
        dirs: ['repositories'],
        extensions: '.repository.ts',
        nested: true,
      },
      services: {
        dirs: ['services'],
        extensions: '.service.ts',
        nested: true,
      },
      migrations: {
        dirs: ['migrations'],
        extensions: ['.migration.ts'],
        nested: true,
      },
    };

    this.api(APISpec);
  }

  setUpBindings(): void {
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceConstants.TOKEN_SECRET_VALUE,
    );

    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
    );

    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(MinIOServiceBindings.MINIO_SERVICE).toClass(MinIOService);
    this.bind(PackageBindings.ONFIDIO_PACKAGE).toDynamicValue(
      PackageFactory.ONFIDO_INSTANCE,
    );
  }

  protected configureFileUpload(destination?: string) {
    destination = destination ?? path.join('/tmp');
    this.bind(STORAGE_DIRECTORY).to(destination);
    const multerOptions: multer.Options = {
      storage: multer.diskStorage({
        destination,
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    };
    this.configure(FILE_UPLOAD_SERVICE).to(multerOptions);
  }
}
