module.exports = {
  extends: '@loopback/eslint-config',
  rules: {
    '@typescript-eslint/naming-convention': 0,
    '@typescript-eslint/prefer-for-of': 0,
    '@typescript-eslint/camelcase': 0,
    "@typescript-eslint/no-invalid-this": 0
  },
};
