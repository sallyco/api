module.exports = {
    roots: ["<rootDir>/src"],
    transform: {
        '^.+\\.(t|j)sx?$': ['@swc-node/jest'],
    },
    setupFiles: [
      'dotenv/config'
    ],
    setupFilesAfterEnv: [
        // "@testing-library/react/cleanup-after-each",
        "@testing-library/jest-dom/extend-expect",
        "./src/__mocks__/msw/server.ts"
    ],
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    // Module file extensions for importing
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/src/tools/jestFileMock.js",
        "\\.(css|less)$": "identity-obj-proxy"
    }
};
