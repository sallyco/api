// @ts-nocheck
import React from "react";
import NavBar from "../../src/components/NavBar";
import RouterWrapper from "../decorators/RouterWrapper";
interface Props {}

export default {
  title: "Components / NavBar",
  component: NavBar,
  decorators: [RouterWrapper],
};

const Template: Story<Props> = (args) => <NavBar {...args} />;

export const Default = Template.bind({});
Default.args = {};
