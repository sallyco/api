// @ts-nocheck
import React from "react";
import RouterWrapper from "../../decorators/RouterWrapper";
import { Notification } from "../../../src/components/notifications/Notification";

import { notificationMockData } from "../../helpers/swrConnector";

interface Props {}

export default {
  title: "Components / Notification",
  component: Notification,
  decorators: [RouterWrapper],
};

const Template: Story<Props> = (args) => <Notification {...args} />;

export const Default = Template.bind({});
Default.args = {
  notification: notificationMockData.data[0],
};
