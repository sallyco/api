// @ts-nocheck
import React from "react";
import { Grid } from "semantic-ui-react";
import RouterWrapper from "../../decorators/RouterWrapper";
import { NotificationList } from "../../../src/components/notifications/NotificationList";
import { notificationMockData } from "../../helpers/swrConnector";

export default {
  title: "Components / NotificationList",
  component: NotificationList,
  decorators: [
    (Story) => RouterWrapper(Story),
    (Story) => (
      <>
        <Grid
          style={{ backgroundColor: "black" }}
          verticalAlign="middle"
          columns="equal"
          padded
        >
          <Grid.Row>
            <Grid.Column textAlign="right">
              <Story />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </>
    ),
  ],
};

const Template: Story<Props> = (args) => <NotificationList {...args} />;

export const Default = Template.bind({});
Default.args = {
  notificationList: notificationMockData,
};
