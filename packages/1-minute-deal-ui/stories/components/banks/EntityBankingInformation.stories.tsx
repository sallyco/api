import React, { Props } from "react";
import moment from "moment";
import EntityBankingInformation, {
  EntityBankingInformationProps,
} from "../../../src/components/banks/EntityBankingInformation";
import { Story } from "@storybook/react";
import { DummyEntity } from "../../helpers/dataObjects";

const Template: Story = (args) => <EntityBankingInformation {...args} />;
export const Filled = Template.bind({});
Filled.args = { entity: DummyEntity };

export const MissingEntity = Template.bind({});
MissingEntity.args = { entity: null };

export const MissingBankAccount = Template.bind({});
const MissingBankAccountDummyEntity = { ...DummyEntity };
delete MissingBankAccountDummyEntity.bankAccount;
MissingBankAccount.args = { entity: MissingBankAccountDummyEntity };

export default {
  title: "Components / Banks / Entity Banking Information",
  component: EntityBankingInformation,
  argTypes: {
    entity: {
      description: "Entity Object",
      control: {
        type: "object",
      },
    },
  },
};
