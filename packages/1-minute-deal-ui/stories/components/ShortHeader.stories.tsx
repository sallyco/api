// @ts-nocheck
import React from "react";
import ShortHeader from "../../src/components/ShortHeader";
import { HashRouter as Router } from "react-router-dom";

interface Props {}

export default {
  title: "Components / ShortHeader",
  component: ShortHeader,
  decorators: [
    (Story, context) => (
      <Router>
        <Story />
      </Router>
    ),
  ],
};

const Template: Story<Props> = (args) => <ShortHeader {...args} />;

export const PrimaryLabelWithTooltip = Template.bind({});
PrimaryLabelWithTooltip.args = {};
