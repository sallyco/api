import * as React from "react";
import { Story } from "@storybook/react";
import SignerCard, {
  SignerCardProps,
  defaultValues,
  validationSchema,
} from "../../../src/components/profiles/SignerCard";
import { YupWrapper } from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Components / Profiles / SignerCard",
  component: SignerCard,
  decorators: [
    (Story) => YupWrapper({ Story, validation, initialValues: defaultValues }),
  ],
};

const Template: Story<SignerCardProps> = (args) => <SignerCard {...args} />;

export const Default = Template.bind({});
Default.args = {};
