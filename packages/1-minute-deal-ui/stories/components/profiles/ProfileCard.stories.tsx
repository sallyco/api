// @ts-nocheck
import React from "react";
import { ProfileEditCard } from "../../../src/components/profiles/ProfileEditCard";
import ReduxWrapper from "../../decorators/ReduxWrapper";

const store = {
  profiles: {
    profilesById: {
      profile_1: {
        id: "profile_1",
        firstName: "SOME CRAZY FIRST NAME!!!",
        lastName: "And a Last Name",
        email: "test@email.com",
        countryOfFormation: "United States of America",
        stateOfFormation: "Minnesota",
      },
    },
  },
};

export default {
  title: "Components / Profiles / ProfileEditCard",
  component: ProfileEditCard,
  decorators: [(Story) => ReduxWrapper(Story, store)],
};

const Template: Story<Props> = (args) => <ProfileEditCard {...args} />;

export const DefaultProfileCard = Template.bind({});
DefaultProfileCard.args = {
  profile: store.profiles.profilesById.profile_1,
};

export const NewProfileCard = Template.bind({});
NewProfileCard.args = {
  profileId: null,
};
