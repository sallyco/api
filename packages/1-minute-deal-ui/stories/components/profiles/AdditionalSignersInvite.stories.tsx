import * as React from "react";
import { Story } from "@storybook/react";
import { YupWrapper } from "../../decorators/YupWrapper";
import * as Yup from "yup";
import AdditionalSignersInvite, {
  AdditionalSignersInviteProps,
  validationSchema,
} from "../../../src/components/profiles/AdditionalSignersInvite";

export default {
  title: "Components / Profiles / AdditionalSignersInvite",
  component: AdditionalSignersInvite,
  decorators: [
    (Story) =>
      YupWrapper({
        Story,
        validation: validationSchema(),
        initialValues: {
          signers: [],
        },
      }),
  ],
};

const Template: Story<AdditionalSignersInviteProps> = (args) => (
  <AdditionalSignersInvite {...args} />
);

export const Default = Template.bind({});
Default.args = {
  minSigners: 2,
  maxSigners: 4,
};
