// @ts-nocheck
import { Story } from "@storybook/react";
import React from "react";
import { ProfileSelector } from "../../../src/components/profiles/ProfileSelector";
import ReduxWrapper from "../../decorators/ReduxWrapper";

const store = {
  profiles: {
    profilesById: {
      profile_1: {
        id: "profile_1",
        firstName: "SOME CRAZY FIRST NAME!!!",
        lastName: "And a Last Name",
        email: "test@email.com",
        profileType: "INVESTOR",
        countryOfFormation: "United States of America",
        stateOfFormation: "Minnesota",
      },
      profile_2: {
        id: "profile_2",
        firstName: "2 FIRST NAME!!!",
        lastName: "2 a Last Name",
        email: "test@2.com",
        profileType: "INVESTOR",
        countryOfFormation: "United States of America",
        stateOfFormation: "Minnesota",
      },
      profile_3: {
        id: "profile_3",
        firstName: "3 CRAZY FIRST NAME!!!",
        lastName: "3 a Last Name",
        email: "3@email.com",
        profileType: "ORGANIZER",
        countryOfFormation: "United States of America",
        stateOfFormation: "Minnesota",
      },
      profile_4: {
        id: "profile_4",
        firstName: "4 FIRST NAME!!!",
        lastName: "4 a Last Name",
        email: "test@4.com",
        profileType: "ORGANIZER",
        countryOfFormation: "United States of America",
        stateOfFormation: "Minnesota",
      },
    },
  },
};

export default {
  title: "Components / Profiles / ProfileSelector",
  component: ProfileSelector,
  decorators: [(Story: Story) => ReduxWrapper(Story, store)],
};

const Template: Story<Props> = (args) => <ProfileSelector {...args} />;

export const DefaultProfileSelector = Template.bind();
DefaultProfileSelector.args = null;
