// @ts-nocheck
import React from "react";
import Header from "../../src/components/Header";
import { HashRouter as Router } from "react-router-dom";

interface Props {}

export default {
  title: "Components / Header",
  component: Header,
  decorators: [
    (Story, context) => (
      <Router>
        <Story />
      </Router>
    ),
  ],
};

const Template: Story<Props> = (args) => <Header {...args} />;

export const PrimaryLabelWithTooltip = Template.bind({});
PrimaryLabelWithTooltip.args = {};
