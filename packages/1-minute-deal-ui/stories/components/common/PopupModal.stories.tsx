// @ts-nocheck
import React from "react";
import { PopupModal, Props } from "../../../src/components/common/PopupModal";

export default {
  title: "Components / PopupModal",
  component: PopupModal,
};

const Template: Story<Props> = (args) => <PopupModal {...args} />;

export const PrimaryPopupModal = Template.bind({});
PrimaryPopupModal.args = {
  open: true,
  size: "small",
  heading: "heading_text",
  content: <p>CONTENT_TEXT</p>,
  closecaption: "",
  okcaption: "",
  onClickClose: () => {},
  onClickOk: () => {},
};

export const ClosedPopupModal = Template.bind({});
ClosedPopupModal.args = {
  ...PrimaryPopupModal.args,
  open: false,
};
