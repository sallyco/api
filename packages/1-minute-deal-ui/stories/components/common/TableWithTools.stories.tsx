// @ts-nocheck
import React from "react";
import {
  TableWithTools,
  TableProps,
} from "../../../src/components/common/TableWithTools";
import moment from "moment";

export default {
  title: "Components / Table With Tools",
  component: TableWithTools,
};

const Template: Story<Props> = (args: TableProps) => (
  <TableWithTools {...args} />
);

export const Default = Template.bind({});
Default.args = {
  data: [],
  columnDefinitions: [],
  // selectedColumn?: any;
  // direction?: any;
  // exportDataMap?: any;
  entityName: "dummy",
};

export const ThreeCols = Template.bind({});
ThreeCols.args = {
  ...Default.args,
  data: [
    {
      id: "lkjlkjlkjlkjlk",
      name: "WORDS",
      date: new Date(),
      amount: 12345,
    },
    {
      id: "asdfasdfasdf",
      name: "BLAH",
      date: new Date(),
      amount: 34343434,
    },
    {
      id: "2werwerwerwer",
      name: "CRAP",
      date: new Date(),
      amount: 56565656,
    },
  ],
  columnDefinitions: [
    {
      key: "name",
      headerTitle: "NAME",
      rowDisplayValue: (rowData) => rowData["name"],
    },
    {
      key: "date",
      headerTitle: "DATE",
      rowDisplayValue: (rowData) =>
        moment.utc(rowData["date"]).format("MMMM DD, YYYY"),
    },
    {
      key: "amount",
      headerTitle: "AMOUNT",
      rowDisplayValue: (rowData) => rowData["amount"].toString(),
    },
  ],
  exportDataMap: (data) => ({
    name: data.name.toLowerCase(),
    date: moment.utc(data.date).format("MMMM DD, YYYY"),
    amount: data.amount.toString(),
  }),
};
