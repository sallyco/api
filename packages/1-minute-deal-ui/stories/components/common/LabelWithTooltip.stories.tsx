// @ts-nocheck
import React from "react";
import {
  SemanticCOLORS,
  SemanticICONS,
} from "semantic-ui-react/dist/commonjs/generic";
import { LabelwithTooltip } from "../../../src/components/common/LabelwithTooltip";

interface Props {
  labelText?: string;
  popupText?: string;
  iconName?: SemanticICONS;
  iconColor?: SemanticCOLORS;
}

export default {
  title: "Components / LabelwithTooltip",
  component: LabelwithTooltip,
};

const Template: Story<Props> = (args) => <LabelwithTooltip {...args} />;

export const PrimaryLabelWithTooltip = Template.bind({});
PrimaryLabelWithTooltip.args = {
  labelText: "labelText",
  popupText: "POPUP_TEXT",
  iconName: "question circle",
  iconColor: "red",
};
