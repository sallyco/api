export const SemanticFLOATS = ["left", "right"];
export const SemanticTEXTALIGNMENTS = ["left", "center", "right", "justified"];
export const SemanticVERTICALALIGNMENTS = ["top", "middle", "bottom"];
export const SemanticCOLORS = [
  "red",
  "orange",
  "yellow",
  "olive",
  "green",
  "teal",
  "blue",
  "violet",
  "purple",
  "pink",
  "brown",
  "grey",
  "black",
];
export const SemanticSIZES = [
  "mini",
  "tiny",
  "small",
  "medium",
  "large",
  "big",
  "huge",
  "massive",
];

const SemanticDIRECTIONALTRANSITIONS = [
  "browse",
  "browse right",
  "drop",
  "fade",
  "fade up",
  "fade down",
  "fade left",
  "fade right",
  "fly up",
  "fly down",
  "fly left",
  "fly right",
  "horizontal flip",
  "vertical flip",
  "scale",
  "slide up",
  "slide down",
  "slide left",
  "slide right",
  "swing up",
  "swing down",
  "swing left",
  "swing right",
  "zoom",
];
const SemanticSTATICTRANSITIONS = [
  "jiggle",
  "flash",
  "shake",
  "pulse",
  "tada",
  "bounce",
  "glow",
];

export const SemanticTRANSITIONS = [
  ...SemanticDIRECTIONALTRANSITIONS,
  ...SemanticSTATICTRANSITIONS,
];

// ======================================================
// Widths
// ======================================================

const SemanticWIDTHSNUMBER = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
];
const SemanticWIDTHSSTRING = [
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fifteen",
  "sixteen",
];

export const SemanticWIDTHS = [
  ...SemanticWIDTHSNUMBER,
  ...SemanticWIDTHSSTRING,
];

// ======================================================
// Icon Names
// ======================================================

export const SemanticICONS = [
  "500px",
  "accessible",
  "accusoft",
  "add circle",
  "add square",
  "add to calendar",
  "add to cart",
  "add user",
  "add",
  "address book outline",
  "address book outline",
  "address book outline",
  "address book",
  "address book",
  "address book",
  "address card outline",
  "address card outline",
  "address card outline",
  "address card",
  "address card",
  "address card",
  "adjust",
  "adjust",
  "adn",
  "adversal",
  "affiliatetheme",
  "alarm mute",
  "alarm",
  "ald",
  "algolia",
  "align center",
  "align justify",
  "align left",
  "align right",
  "als",
  "amazon pay",
  "amazon",
  "ambulance",
  "ambulance",
  "ambulance",
  "ambulance",
  "ambulance",
  "american sign language interpreting",
  "american sign language interpreting",
  "amilia",
  "anchor",
  "anchor",
  "android",
  "angellist",
  "angle double down",
  "angle double left",
  "angle double right",
  "angle double up",
  "angle down",
  "angle left",
  "angle right",
  "angle up",
  "angrycreative",
  "angular",
  "announcement",
  "app store ios",
  "app store",
  "apper",
  "apple pay",
  "apple",
  "archive",
  "archive",
  "archive",
  "archive",
  "archive",
  "area chart",
  "area graph",
  "arrow alternate circle down outline",
  "arrow alternate circle down",
  "arrow alternate circle left outline",
  "arrow alternate circle left",
  "arrow alternate circle right outline",
  "arrow alternate circle right",
  "arrow alternate circle up outline",
  "arrow alternate circle up",
  "arrow circle down",
  "arrow circle left",
  "arrow circle right",
  "arrow circle up",
  "arrow down cart",
  "arrow down",
  "arrow left",
  "arrow right",
  "arrow up",
  "arrows alternate horizontal",
  "arrows alternate vertical",
  "arrows alternate",
  "asexual",
  "asl interpreting",
  "asl",
  "assistive listening devices",
  "assistive listening systems",
  "assistive listening systems",
  "asterisk",
  "asymmetrik",
  "at",
  "attach",
  "attention",
  "audible",
  "audio description",
  "audio description",
  "autoprefixer",
  "avianex",
  "aviato",
  "aws",
  "backward",
  "balance scale",
  "balance scale",
  "balance scale",
  "balance",
  "ban",
  "ban",
  "band aid",
  "bandcamp",
  "bar",
  "barcode",
  "barcode",
  "bars",
  "baseball ball",
  "basketball ball",
  "bath",
  "bath",
  "bath",
  "bathtub",
  "battery empty",
  "battery four",
  "battery full",
  "battery half",
  "battery high",
  "battery low",
  "battery one",
  "battery quarter",
  "battery three quarters",
  "battery three",
  "battery two",
  "battery zero",
  "bed",
  "bed",
  "bed",
  "beer",
  "beer",
  "beer",
  "behance square",
  "behance",
  "bell outline",
  "bell outline",
  "bell outline",
  "bell outline",
  "bell outline",
  "bell outline",
  "bell outline",
  "bell slash outline",
  "bell slash outline",
  "bell slash outline",
  "bell slash outline",
  "bell slash outline",
  "bell slash",
  "bell slash",
  "bell slash",
  "bell slash",
  "bell slash",
  "bell",
  "bell",
  "bell",
  "bell",
  "bell",
  "bell",
  "bell",
  "bicycle",
  "bicycle",
  "bicycle",
  "bimobject",
  "binoculars",
  "binoculars",
  "birthday cake",
  "birthday cake",
  "birthday cake",
  "birthday",
  "bitbucket square",
  "bitbucket",
  "bitcoin",
  "bity",
  "black tie",
  "blackberry",
  "blind",
  "blind",
  "blind",
  "block layout",
  "blogger b",
  "blogger",
  "bluetooth alternative",
  "bluetooth b",
  "bluetooth",
  "bold",
  "bolt",
  "bomb",
  "bomb",
  "book",
  "book",
  "book",
  "book",
  "bookmark outline",
  "bookmark outline",
  "bookmark outline",
  "bookmark outline",
  "bookmark outline",
  "bookmark",
  "bookmark",
  "bookmark",
  "bookmark",
  "bookmark",
  "bowling ball",
  "box",
  "boxes",
  "braille",
  "briefcase",
  "briefcase",
  "briefcase",
  "broken chain",
  "browser",
  "btc",
  "bug",
  "bug",
  "bug",
  "building outline",
  "building outline",
  "building outline",
  "building",
  "building",
  "building",
  "bullhorn",
  "bullhorn",
  "bullhorn",
  "bullhorn",
  "bullhorn",
  "bullseye",
  "bullseye",
  "bullseye",
  "buromobelexperte",
  "bus",
  "bus",
  "buysellads",
  "calculator",
  "calculator",
  "calculator",
  "calendar alternate outline",
  "calendar alternate outline",
  "calendar alternate outline",
  "calendar alternate outline",
  "calendar alternate outline",
  "calendar alternate",
  "calendar alternate",
  "calendar alternate",
  "calendar alternate",
  "calendar alternate",
  "calendar check outline",
  "calendar check outline",
  "calendar check outline",
  "calendar check",
  "calendar check",
  "calendar check",
  "calendar minus outline",
  "calendar minus outline",
  "calendar minus outline",
  "calendar minus",
  "calendar minus",
  "calendar minus",
  "calendar outline",
  "calendar outline",
  "calendar outline",
  "calendar outline",
  "calendar outline",
  "calendar outline",
  "calendar plus outline",
  "calendar plus outline",
  "calendar plus outline",
  "calendar plus",
  "calendar plus",
  "calendar plus",
  "calendar times outline",
  "calendar times outline",
  "calendar times outline",
  "calendar times",
  "calendar times",
  "calendar times",
  "calendar",
  "calendar",
  "calendar",
  "calendar",
  "calendar",
  "calendar",
  "call square",
  "call",
  "camera retro",
  "camera retro",
  "camera retro",
  "camera",
  "camera",
  "camera",
  "cancel",
  "car",
  "car",
  "car",
  "caret down",
  "caret left",
  "caret right",
  "caret square down outline",
  "caret square down",
  "caret square left outline",
  "caret square left",
  "caret square right outline",
  "caret square right",
  "caret square up outline",
  "caret square up",
  "caret up",
  "cart arrow down",
  "cart arrow down",
  "cart arrow down",
  "cart plus",
  "cart plus",
  "cart",
  "cc amazon pay",
  "cc amex",
  "cc apple pay",
  "cc diners club",
  "cc discover",
  "cc jcb",
  "cc mastercard",
  "cc paypal",
  "cc stripe",
  "cc visa",
  "cc",
  "centercode",
  "certificate",
  "certificate",
  "certificate",
  "certificate",
  "certificate",
  "chain",
  "chart area",
  "chart bar outline",
  "chart bar",
  "chart line",
  "chart line",
  "chart pie",
  "chat",
  "check circle outline",
  "check circle",
  "check square outline",
  "check square",
  "check",
  "checked calendar",
  "checkmark box",
  "checkmark",
  "chess bishop",
  "chess board",
  "chess king",
  "chess knight",
  "chess pawn",
  "chess queen",
  "chess rock",
  "chess rook",
  "chess",
  "chevron circle down",
  "chevron circle left",
  "chevron circle right",
  "chevron circle up",
  "chevron down",
  "chevron left",
  "chevron right",
  "chevron up",
  "child",
  "chrome",
  "circle notch",
  "circle notched",
  "circle outline",
  "circle outline",
  "circle outline",
  "circle thin",
  "circle",
  "circle",
  "circle",
  "clipboard check",
  "clipboard list",
  "clipboard outline",
  "clipboard outline",
  "clipboard outline",
  "clipboard outline",
  "clipboard",
  "clipboard",
  "clipboard",
  "clipboard",
  "clock outline",
  "clock",
  "clone outline",
  "clone outline",
  "clone outline",
  "clone outline",
  "clone outline",
  "clone",
  "clone",
  "clone",
  "clone",
  "clone",
  "close",
  "closed captioning outline",
  "closed captioning outline",
  "closed captioning",
  "closed captioning",
  "cloud download",
  "cloud download",
  "cloud download",
  "cloud upload",
  "cloud upload",
  "cloud upload",
  "cloud",
  "cloud",
  "cloud",
  "cloudscale",
  "cloudsmith",
  "cloudversify",
  "cny",
  "cocktail",
  "code branch",
  "code",
  "codepen",
  "codiepie",
  "coffee",
  "coffee",
  "coffee",
  "coffee",
  "coffee",
  "cog",
  "cog",
  "cog",
  "cogs",
  "cogs",
  "columns",
  "columns",
  "comment alternate outline",
  "comment alternate",
  "comment outline",
  "comment outline",
  "comment",
  "comment",
  "commenting",
  "comments outline",
  "comments",
  "compass outline",
  "compass outline",
  "compass outline",
  "compass",
  "compass",
  "compass",
  "compose",
  "compress",
  "compress",
  "computer",
  "configure",
  "connectdevelop",
  "contao",
  "content",
  "conversation",
  "copy outline",
  "copy outline",
  "copy outline",
  "copy outline",
  "copy outline",
  "copy outline",
  "copy",
  "copy",
  "copy",
  "copy",
  "copy",
  "copy",
  "copyright outline",
  "copyright",
  "cpanel",
  "creative commons",
  "credit card alternative",
  "credit card outline",
  "credit card",
  "crop",
  "crosshairs",
  "crosshairs",
  "crosshairs",
  "css3 alternate",
  "css3",
  "cube",
  "cubes",
  "currency",
  "cut",
  "cut",
  "cut",
  "cut",
  "cut",
  "cut",
  "cuttlefish",
  "d and d",
  "dashboard",
  "dashcube",
  "database",
  "deaf",
  "deafness",
  "delete calendar",
  "delete",
  "delicious",
  "deploydog",
  "deskpro",
  "desktop",
  "detective",
  "deviantart",
  "diamond",
  "digg",
  "digital ocean",
  "discord",
  "discourse",
  "discussions",
  "disk",
  "dna",
  "dochub",
  "docker",
  "doctor",
  "dollar sign",
  "dollar sign",
  "dollar",
  "dolly flatbed",
  "dolly",
  "dont",
  "dot circle outline",
  "dot circle",
  "download",
  "download",
  "download",
  "draft2digital",
  "dribbble square",
  "dribbble",
  "drivers license",
  "dropbox",
  "dropdown",
  "drupal",
  "dyalog",
  "earlybirds",
  "edge",
  "edit outline",
  "edit outline",
  "edit outline",
  "edit outline",
  "edit outline",
  "edit",
  "edit",
  "edit",
  "edit",
  "edit",
  "eject",
  "elementor",
  "ellipsis horizontal",
  "ellipsis vertical",
  "ember",
  "emergency",
  "empire",
  "envelope open outline",
  "envelope open outline",
  "envelope open outline",
  "envelope open outline",
  "envelope open outline",
  "envelope open",
  "envelope open",
  "envelope open",
  "envelope open",
  "envelope open",
  "envelope outline",
  "envelope outline",
  "envelope outline",
  "envelope outline",
  "envelope outline",
  "envelope square",
  "envelope square",
  "envelope",
  "envelope",
  "envelope",
  "envelope",
  "envelope",
  "envira gallery",
  "envira",
  "erase",
  "eraser",
  "eraser",
  "eraser",
  "eraser",
  "eraser",
  "eraser",
  "erlang",
  "ethereum",
  "etsy",
  "eur",
  "euro sign",
  "euro",
  "exchange",
  "exchange",
  "exclamation circle",
  "exclamation circle",
  "exclamation triangle",
  "exclamation triangle",
  "exclamation",
  "exclamation",
  "expand arrows alternate",
  "expand arrows alternate",
  "expand",
  "expand",
  "expeditedssl",
  "external alternate",
  "external alternate",
  "external share",
  "external square alternate",
  "external square alternate",
  "external square",
  "external",
  "eye dropper",
  "eye dropper",
  "eye dropper",
  "eye slash outline",
  "eye slash outline",
  "eye slash outline",
  "eye slash outline",
  "eye slash outline",
  "eye slash",
  "eye slash",
  "eye slash",
  "eye slash",
  "eye slash",
  "eye",
  "eye",
  "eye",
  "eye",
  "eye",
  "eye",
  "eyedropper",
  "facebook f",
  "facebook messenger",
  "facebook official",
  "facebook square",
  "facebook",
  "factory",
  "fast backward",
  "fast forward",
  "favorite",
  "fax",
  "fax",
  "fax",
  "feed",
  "female homosexual",
  "female",
  "fighter jet",
  "fighter jet",
  "fighter jet",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate outline",
  "file alternate",
  "file alternate",
  "file alternate",
  "file alternate",
  "file alternate",
  "file alternate",
  "file alternate",
  "file alternate",
  "file archive outline",
  "file archive",
  "file audio outline",
  "file audio outline",
  "file audio",
  "file audio",
  "file code outline",
  "file code outline",
  "file code",
  "file code",
  "file excel outline",
  "file excel",
  "file image outline",
  "file image outline",
  "file image",
  "file image",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file outline",
  "file pdf outline",
  "file pdf",
  "file powerpoint outline",
  "file powerpoint",
  "file text outline",
  "file text",
  "file video outline",
  "file video outline",
  "file video",
  "file video",
  "file word outline",
  "file word",
  "file",
  "file",
  "file",
  "file",
  "file",
  "file",
  "file",
  "file",
  "file",
  "film",
  "film",
  "film",
  "filter",
  "filter",
  "find",
  "fire extinguisher",
  "fire extinguisher",
  "fire extinguisher",
  "fire",
  "fire",
  "firefox",
  "first aid",
  "first aid",
  "first order",
  "firstdraft",
  "flag checkered",
  "flag checkered",
  "flag checkered",
  "flag outline",
  "flag outline",
  "flag outline",
  "flag",
  "flag",
  "flag",
  "flask",
  "flask",
  "flickr",
  "flipboard",
  "fly",
  "folder open outline",
  "folder open outline",
  "folder open outline",
  "folder open outline",
  "folder open outline",
  "folder open outline",
  "folder open",
  "folder open",
  "folder open",
  "folder open",
  "folder open",
  "folder open",
  "folder outline",
  "folder outline",
  "folder outline",
  "folder outline",
  "folder outline",
  "folder outline",
  "folder outline",
  "folder",
  "folder",
  "folder",
  "folder",
  "folder",
  "folder",
  "folder",
  "font awesome alternate",
  "font awesome flag",
  "font awesome",
  "font",
  "fonticons fi",
  "fonticons",
  "food",
  "football ball",
  "fork",
  "fort awesome alternate",
  "fort awesome",
  "forumbee",
  "forward",
  "foursquare",
  "free code camp",
  "freebsd",
  "frown outline",
  "frown outline",
  "frown",
  "frown",
  "futbol outline",
  "futbol outline",
  "futbol",
  "futbol",
  "game",
  "gamepad",
  "gamepad",
  "gavel",
  "gavel",
  "gay",
  "gbp",
  "gem outline",
  "gem outline",
  "gem",
  "gem",
  "genderless",
  "get pocket",
  "gg circle",
  "gg",
  "gift",
  "gift",
  "gift",
  "git square",
  "git",
  "github alternate",
  "github square",
  "github",
  "gitkraken",
  "gitlab",
  "gitter",
  "glass martini",
  "glass martini",
  "glide g",
  "glide",
  "globe",
  "globe",
  "globe",
  "gofore",
  "golf ball",
  "goodreads g",
  "goodreads",
  "google drive",
  "google play",
  "google plus circle",
  "google plus g",
  "google plus official",
  "google plus square",
  "google plus",
  "google wallet",
  "google",
  "grab",
  "graduation cap",
  "graduation cap",
  "graduation",
  "gratipay",
  "grav",
  "grid layout",
  "gripfire",
  "group",
  "grunt",
  "gulp",
  "h square",
  "h square",
  "h",
  "hacker news square",
  "hacker news",
  "hand lizard outline",
  "hand lizard",
  "hand paper outline",
  "hand paper",
  "hand peace outline",
  "hand peace",
  "hand point down outline",
  "hand point down outline",
  "hand point down",
  "hand point down",
  "hand point left outline",
  "hand point left outline",
  "hand point left",
  "hand point left",
  "hand point right outline",
  "hand point right outline",
  "hand point right",
  "hand point right",
  "hand point up outline",
  "hand point up outline",
  "hand point up",
  "hand point up",
  "hand pointer outline",
  "hand pointer outline",
  "hand pointer",
  "hand pointer",
  "hand rock outline",
  "hand rock",
  "hand scissors outline",
  "hand scissors",
  "hand spock outline",
  "hand spock",
  "hand victory",
  "handicap",
  "handshake outline",
  "handshake outline",
  "handshake",
  "handshake",
  "hard of hearing",
  "hashtag",
  "hdd outline",
  "hdd outline",
  "hdd",
  "hdd",
  "header",
  "heading",
  "headphones",
  "headphones",
  "headphones",
  "heart outline",
  "heart outline",
  "heart outline",
  "heart outline",
  "heart outline",
  "heart outline",
  "heart outline",
  "heart",
  "heart",
  "heart",
  "heart",
  "heart",
  "heart",
  "heart",
  "heartbeat",
  "heartbeat",
  "heartbeat",
  "help circle",
  "help",
  "heterosexual",
  "hide",
  "hips",
  "hire a helper",
  "history",
  "history",
  "hockey puck",
  "home",
  "home",
  "home",
  "hooli",
  "hospital outline",
  "hospital outline",
  "hospital outline",
  "hospital outline",
  "hospital symbol",
  "hospital",
  "hospital",
  "hospital",
  "hospital",
  "hotel",
  "hotjar",
  "hourglass end",
  "hourglass four",
  "hourglass full",
  "hourglass half",
  "hourglass one",
  "hourglass outline",
  "hourglass outline",
  "hourglass start",
  "hourglass three",
  "hourglass two",
  "hourglass zero",
  "hourglass",
  "hourglass",
  "houzz",
  "html5",
  "hubspot",
  "i cursor",
  "i cursor",
  "id badge outline",
  "id badge outline",
  "id badge",
  "id badge",
  "id card outline",
  "id card outline",
  "id card",
  "id card",
  "idea",
  "ils",
  "image outline",
  "image outline",
  "image outline",
  "image",
  "image",
  "image",
  "images outline",
  "images outline",
  "images outline",
  "images",
  "images",
  "images",
  "imdb",
  "in cart",
  "inbox",
  "indent",
  "industry",
  "industry",
  "industry",
  "info circle",
  "info circle",
  "info circle",
  "info",
  "info",
  "info",
  "inr",
  "instagram",
  "intergender",
  "internet explorer",
  "intersex",
  "ioxhost",
  "italic",
  "itunes note",
  "itunes",
  "jenkins",
  "joget",
  "joomla",
  "jpy",
  "js square",
  "js",
  "jsfiddle",
  "key",
  "key",
  "key",
  "keyboard outline",
  "keyboard outline",
  "keyboard outline",
  "keyboard outline",
  "keyboard",
  "keyboard",
  "keyboard",
  "keyboard",
  "keycdn",
  "kickstarter k",
  "kickstarter",
  "korvue",
  "krw",
  "lab",
  "language",
  "language",
  "laptop",
  "laptop",
  "laravel",
  "lastfm square",
  "lastfm",
  "law",
  "leaf",
  "leaf",
  "leanpub",
  "legal",
  "lemon outline",
  "lemon outline",
  "lemon",
  "lemon",
  "lesbian",
  "less",
  "level down alternate",
  "level down",
  "level up alternate",
  "level up",
  "life ring outline",
  "life ring outline",
  "life ring outline",
  "life ring",
  "life ring",
  "life ring",
  "lightbulb outline",
  "lightbulb outline",
  "lightbulb",
  "lightbulb",
  "lightning",
  "like",
  "line graph",
  "linechat",
  "linkedin alternate",
  "linkedin square",
  "linkedin",
  "linkify",
  "linkify",
  "linode",
  "linux",
  "lira sign",
  "lira",
  "list alternate outline",
  "list alternate",
  "list layout",
  "list ol",
  "list ul",
  "list",
  "location arrow",
  "location arrow",
  "lock open",
  "lock open",
  "lock",
  "lock",
  "log out",
  "long arrow alternate down",
  "long arrow alternate left",
  "long arrow alternate right",
  "long arrow alternate up",
  "low vision",
  "low vision",
  "lyft",
  "magento",
  "magic",
  "magic",
  "magnet",
  "magnet",
  "magnify",
  "mail forward",
  "mail outline",
  "mail square",
  "mail",
  "male homosexual",
  "male",
  "male",
  "man",
  "map marker alternate",
  "map marker alternate",
  "map marker",
  "map marker",
  "map marker",
  "map outline",
  "map outline",
  "map pin",
  "map pin",
  "map signs",
  "map signs",
  "map",
  "map",
  "marker",
  "mars alternate",
  "mars double",
  "mars horizontal",
  "mars stroke horizontal",
  "mars stroke vertical",
  "mars stroke",
  "mars vertical",
  "mars",
  "maxcdn",
  "meanpath",
  "medapps",
  "medium m",
  "medium",
  "medkit",
  "medkit",
  "medkit",
  "medrt",
  "meetup",
  "meh outline",
  "meh outline",
  "meh",
  "meh",
  "mercury",
  "microchip",
  "microchip",
  "microchip",
  "microphone slash",
  "microphone slash",
  "microphone slash",
  "microphone",
  "microphone",
  "microphone",
  "microphone",
  "microsoft edge",
  "microsoft",
  "military",
  "minus circle",
  "minus circle",
  "minus square outline",
  "minus square outline",
  "minus square",
  "minus square",
  "minus",
  "minus",
  "mix",
  "mixcloud",
  "mizuni",
  "mobile alternate",
  "mobile alternate",
  "mobile alternate",
  "mobile",
  "mobile",
  "mobile",
  "modx",
  "monero",
  "money bill alternate outline",
  "money bill alternate outline",
  "money bill alternate outline",
  "money bill alternate",
  "money bill alternate",
  "money bill alternate",
  "money",
  "moon outline",
  "moon",
  "motorcycle",
  "motorcycle",
  "motorcycle",
  "mouse pointer",
  "move",
  "ms edge",
  "music",
  "music",
  "mute",
  "napster",
  "neuter",
  "new pied piper",
  "newspaper outline",
  "newspaper outline",
  "newspaper outline",
  "newspaper",
  "newspaper",
  "newspaper",
  "nintendo switch",
  "node js",
  "node",
  "non binary transgender",
  "npm",
  "ns8",
  "numbered list",
  "nutritionix",
  "object group outline",
  "object group",
  "object ungroup outline",
  "object ungroup",
  "odnoklassniki square",
  "odnoklassniki",
  "opencart",
  "openid",
  "opera",
  "optin monster",
  "options",
  "ordered list",
  "osi",
  "other gender horizontal",
  "other gender vertical",
  "other gender",
  "outdent",
  "page4",
  "pagelines",
  "paint brush",
  "paint brush",
  "palfed",
  "pallet",
  "paper plane outline",
  "paper plane outline",
  "paper plane outline",
  "paper plane outline",
  "paper plane outline",
  "paper plane",
  "paper plane",
  "paper plane",
  "paper plane",
  "paper plane",
  "paperclip",
  "paperclip",
  "paperclip",
  "paperclip",
  "paragraph",
  "paragraph",
  "paste",
  "paste",
  "paste",
  "paste",
  "paste",
  "paste",
  "patreon",
  "pause circle outline",
  "pause circle",
  "pause",
  "paw",
  "paw",
  "payment",
  "paypal card",
  "paypal",
  "pen square",
  "pen square",
  "pencil alternate",
  "pencil alternate",
  "pencil alternate",
  "pencil alternate",
  "pencil alternate",
  "pencil alternate",
  "pencil square",
  "pencil",
  "percent",
  "periscope",
  "phabricator",
  "phoenix framework",
  "phone square",
  "phone square",
  "phone square",
  "phone volume",
  "phone volume",
  "phone volume",
  "phone volume",
  "phone volume",
  "phone",
  "phone",
  "phone",
  "phone",
  "photo",
  "php",
  "picture",
  "pie chart",
  "pie graph",
  "pied piper alternate",
  "pied piper hat",
  "pied piper pp",
  "pied piper",
  "pills",
  "pin",
  "pinterest p",
  "pinterest square",
  "pinterest",
  "plane",
  "plane",
  "plane",
  "play circle outline",
  "play circle",
  "play",
  "play",
  "play",
  "playstation",
  "plug",
  "plug",
  "plug",
  "plus cart",
  "plus circle",
  "plus circle",
  "plus square outline",
  "plus square outline",
  "plus square outline",
  "plus square outline",
  "plus square",
  "plus square",
  "plus square",
  "plus square",
  "plus",
  "plus",
  "plus",
  "plus",
  "podcast",
  "point",
  "pointing down",
  "pointing left",
  "pointing right",
  "pointing up",
  "pound sign",
  "pound",
  "power cord",
  "power off",
  "power off",
  "power",
  "print",
  "print",
  "print",
  "print",
  "privacy",
  "product hunt",
  "protect",
  "pushed",
  "puzzle piece",
  "puzzle",
  "python",
  "qq",
  "qrcode",
  "qrcode",
  "question circle outline",
  "question circle outline",
  "question circle outline",
  "question circle",
  "question circle",
  "question circle",
  "question",
  "question",
  "quidditch",
  "quinscape",
  "quora",
  "quote left",
  "quote left",
  "quote left",
  "quote right",
  "quote right",
  "quote right",
  "r circle",
  "radio",
  "rain",
  "random",
  "random",
  "ravelry",
  "react",
  "rebel",
  "record",
  "recycle",
  "recycle",
  "reddit alien",
  "reddit square",
  "reddit",
  "redo alternate",
  "redo alternate",
  "redo alternate",
  "redo alternate",
  "redo",
  "redo",
  "redo",
  "redo",
  "redriver",
  "refresh",
  "registered outline",
  "registered",
  "remove bookmark",
  "remove circle",
  "remove from calendar",
  "remove user",
  "remove",
  "rendact",
  "renren",
  "repeat",
  "reply all",
  "reply all",
  "reply all",
  "reply",
  "reply",
  "reply",
  "replyd",
  "resize horizontal",
  "resize vertical",
  "resolving",
  "retweet",
  "rmb",
  "road",
  "road",
  "rocket",
  "rocket",
  "rocket",
  "rocketchat",
  "rockrms",
  "rouble",
  "rss square",
  "rss square",
  "rss square",
  "rss",
  "rss",
  "rss",
  "rub",
  "ruble sign",
  "ruble",
  "rupee sign",
  "rupee",
  "s15",
  "safari",
  "sass",
  "save outline",
  "save outline",
  "save outline",
  "save outline",
  "save outline",
  "save outline",
  "save",
  "save",
  "save",
  "save",
  "save",
  "save",
  "schlix",
  "scribd",
  "search minus",
  "search minus",
  "search plus",
  "search plus",
  "search",
  "search",
  "search",
  "searchengin",
  "selected radio",
  "sellcast",
  "sellsy",
  "send",
  "server",
  "servicestack",
  "setting",
  "settings",
  "share alternate square",
  "share alternate",
  "share square outline",
  "share square outline",
  "share square",
  "share square",
  "share",
  "share",
  "share",
  "shekel sign",
  "shekel",
  "sheqel",
  "shield alternate",
  "shield alternate",
  "shield alternate",
  "shield alternate",
  "shield",
  "ship",
  "ship",
  "shipping fast",
  "shipping",
  "shirtsinbulk",
  "shop",
  "shopping bag",
  "shopping bag",
  "shopping bag",
  "shopping basket",
  "shopping basket",
  "shopping basket",
  "shopping cart",
  "shopping cart",
  "shopping cart",
  "shopping cart",
  "shopping cart",
  "shower",
  "shower",
  "shuffle",
  "shutdown",
  "sidebar",
  "sign in alternate",
  "sign in",
  "sign language",
  "sign out alternate",
  "sign out",
  "sign-in alternate",
  "sign-in",
  "sign-in",
  "sign-out alternate",
  "sign-out",
  "sign-out",
  "signal",
  "signing",
  "signup",
  "simplybuilt",
  "sistrix",
  "sitemap",
  "sitemap",
  "sitemap",
  "skyatlas",
  "skype",
  "slack hash",
  "slack",
  "sliders horizontal",
  "sliders horizontal",
  "sliders",
  "slideshare",
  "smile outline",
  "smile outline",
  "smile",
  "smile",
  "snapchat ghost",
  "snapchat square",
  "snapchat",
  "snowflake outline",
  "snowflake outline",
  "snowflake",
  "snowflake",
  "soccer",
  "sort alphabet ascending",
  "sort alphabet descending",
  "sort alphabet down",
  "sort alphabet down",
  "sort alphabet up",
  "sort alphabet up",
  "sort amount down",
  "sort amount down",
  "sort amount up",
  "sort amount up",
  "sort ascending",
  "sort content ascending",
  "sort content descending",
  "sort descending",
  "sort down",
  "sort down",
  "sort numeric ascending",
  "sort numeric descending",
  "sort numeric down",
  "sort numeric down",
  "sort numeric up",
  "sort numeric up",
  "sort up",
  "sort up",
  "sort",
  "sort",
  "sound",
  "soundcloud",
  "space shuttle",
  "space shuttle",
  "speakap",
  "spinner",
  "spoon",
  "spotify",
  "spy",
  "square full",
  "square outline",
  "square",
  "stack exchange",
  "stack overflow",
  "star half empty",
  "star half full",
  "star half outline",
  "star half",
  "star outline",
  "star outline",
  "star outline",
  "star outline",
  "star",
  "star",
  "star",
  "star",
  "staylinked",
  "steam square",
  "steam symbol",
  "steam",
  "step backward",
  "step forward",
  "stethoscope",
  "stethoscope",
  "sticker mule",
  "sticky note outline",
  "sticky note outline",
  "sticky note outline",
  "sticky note outline",
  "sticky note",
  "sticky note",
  "sticky note",
  "sticky note",
  "stop circle outline",
  "stop circle",
  "stop",
  "stopwatch",
  "stopwatch",
  "strava",
  "street view",
  "street view",
  "strikethrough",
  "stripe card",
  "stripe s",
  "stripe",
  "student",
  "studiovinari",
  "stumbleupon circle",
  "stumbleupon",
  "subscript",
  "subway",
  "subway",
  "subway",
  "suitcase",
  "suitcase",
  "suitcase",
  "sun outline",
  "sun outline",
  "sun",
  "sun",
  "superpowers",
  "superscript",
  "supple",
  "sync alternate",
  "sync alternate",
  "sync alternate",
  "sync alternate",
  "sync",
  "sync",
  "sync",
  "sync",
  "sync",
  "syringe",
  "table tennis",
  "table",
  "table",
  "tablet alternate",
  "tablet alternate",
  "tablet",
  "tablet",
  "tachometer alternate",
  "tag",
  "tag",
  "tag",
  "tag",
  "tags",
  "tags",
  "tags",
  "tags",
  "talk",
  "target",
  "tasks",
  "tasks",
  "taxi",
  "taxi",
  "taxi",
  "teleconst",
  "telegram plane",
  "telegram",
  "television",
  "tencent weibo",
  "terminal",
  "text cursor",
  "text height",
  "text height",
  "text telephone",
  "text width",
  "text width",
  "th large",
  "th list",
  "th",
  "theme",
  "themeisle",
  "thermometer empty",
  "thermometer full",
  "thermometer half",
  "thermometer quarter",
  "thermometer three quarters",
  "thermometer",
  "thermometer",
  "thumb tack",
  "thumbs down outline",
  "thumbs down outline",
  "thumbs down outline",
  "thumbs down outline",
  "thumbs down",
  "thumbs down",
  "thumbs down",
  "thumbs down",
  "thumbs up outline",
  "thumbs up outline",
  "thumbs up outline",
  "thumbs up outline",
  "thumbs up",
  "thumbs up",
  "thumbs up",
  "thumbs up",
  "thumbtack",
  "thumbtack",
  "thumbtack",
  "thumbtack",
  "ticket alternate",
  "ticket alternate",
  "ticket",
  "time",
  "times circle outline",
  "times circle",
  "times rectangle",
  "times",
  "tint",
  "tint",
  "tint",
  "tm",
  "toggle down",
  "toggle left",
  "toggle off",
  "toggle off",
  "toggle on",
  "toggle on",
  "toggle right",
  "toggle up",
  "trademark",
  "train",
  "train",
  "train",
  "transgender alternate",
  "transgender",
  "translate",
  "trash alternate outline",
  "trash alternate outline",
  "trash alternate outline",
  "trash alternate",
  "trash alternate",
  "trash alternate",
  "trash",
  "trash",
  "trash",
  "travel",
  "treatment",
  "tree",
  "tree",
  "trello",
  "triangle down",
  "triangle left",
  "triangle right",
  "triangle up",
  "tripadvisor",
  "trophy",
  "trophy",
  "trophy",
  "trophy",
  "truck",
  "truck",
  "truck",
  "truck",
  "try",
  "tty",
  "tty",
  "tty",
  "tumblr square",
  "tumblr",
  "tv",
  "tv",
  "twitch",
  "twitter square",
  "twitter",
  "typo3",
  "uber",
  "uikit",
  "umbrella",
  "umbrella",
  "underline",
  "undo alternate",
  "undo alternate",
  "undo alternate",
  "undo alternate",
  "undo",
  "undo",
  "undo",
  "undo",
  "unhide",
  "uniregistry",
  "universal access",
  "university",
  "university",
  "unlink",
  "unlinkify",
  "unlock alternate",
  "unlock alternate",
  "unlock",
  "unlock",
  "unmute",
  "unordered list",
  "untappd",
  "upload",
  "upload",
  "upload",
  "usb",
  "usd",
  "user cancel",
  "user circle outline",
  "user circle outline",
  "user circle",
  "user circle",
  "user close",
  "user delete",
  "user doctor",
  "user md",
  "user md",
  "user md",
  "user outline",
  "user outline",
  "user plus",
  "user secret",
  "user secret",
  "user times",
  "user x",
  "user",
  "user",
  "users",
  "ussunnah",
  "utensil spoon",
  "utensil spoon",
  "utensils",
  "utensils",
  "vaadin",
  "vcard",
  "venus double",
  "venus mars",
  "venus",
  "viacoin",
  "viadeo square",
  "viadeo",
  "viber",
  "video camera",
  "video play",
  "video",
  "vimeo square",
  "vimeo v",
  "vimeo",
  "vine",
  "vk",
  "vnv",
  "volleyball ball",
  "volume control phone",
  "volume down",
  "volume off",
  "volume up",
  "vuejs",
  "wait",
  "warehouse",
  "warning circle",
  "warning sign",
  "warning",
  "wechat",
  "weibo",
  "weight",
  "weixin",
  "whatsapp square",
  "whatsapp",
  "wheelchair",
  "wheelchair",
  "wheelchair",
  "wheelchair",
  "wheelchair",
  "wheelchair",
  "whmcs",
  "wi-fi",
  "wifi",
  "wifi",
  "wifi",
  "wikipedia w",
  "window close outline",
  "window close",
  "window maximize outline",
  "window maximize",
  "window minimize outline",
  "window minimize",
  "window restore outline",
  "window restore",
  "windows",
  "winner",
  "wizard",
  "woman",
  "won sign",
  "won",
  "wordpress beginner",
  "wordpress forms",
  "wordpress simple",
  "wordpress",
  "world",
  "wpbeginner",
  "wpexplorer",
  "wpforms",
  "wrench",
  "wrench",
  "write square",
  "write",
  "x",
  "xbox",
  "xing square",
  "xing",
  "y combinator",
  "yahoo",
  "yandex international",
  "yandex",
  "yelp",
  "yen sign",
  "yen",
  "yoast",
  "youtube play",
  "youtube square",
  "youtube",
  "zip",
  "zoom in",
  "zoom out",
  "zoom-in",
  "zoom-out",
  "zoom",
];
