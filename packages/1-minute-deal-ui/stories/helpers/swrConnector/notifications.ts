import {
  NotificationList,
  Notification,
} from "../../../src/api/notificationsApi";
import useSWR from "swr";
import { AxiosResponse } from "axios";

export const notificationMockData = {
  totalCount: 2,
  perPage: 10,
  page: 0,
  data: [
    {
      id: "5fb5fb95f19386089641ccbe",
      tenantId: "gbt",
      userId: "7667164e-0627-44b2-9c72-1d31095d59e6",
      event: {
        type: "deal",
        id: "5fa1c99353f9c495ae3427f9",
      },
      category: "DEAL_STATUS",
      message: "THIS IS A TEST MESSAGE??? DOES IT WORK?",
      acknowledged: false,
      createdAt: "2020-11-19T04:59:01.353Z",
      updatedAt: "2020-11-22T04:17:09.000Z",
    },
    {
      id: "5fb5fb97f19386089641ccbf",
      tenantId: "gbt",
      userId: "7667164e-0627-44b2-9c72-1d31095d59e6",
      event: {
        type: "deal",
        id: "5fa1c99353f9c495ae3427f9",
      },
      category: "INVESTMENTS",
      message: "THIS IS A TEST MESSAGE??? DOES IT WORK?",
      acknowledged: false,
      createdAt: "2020-11-19T04:59:03.413Z",
      updatedAt: "2020-11-22T05:54:21.000Z",
    },
  ],
};

const axiosResponse: AxiosResponse = {
  config: {},
  data: notificationMockData,
  headers: {},
  status: 200,
  statusText: "OK",
};

export const notificationMockFetcher = (url) =>
  Promise.resolve(axiosResponse.data);

export const useNotifications = (): NotificationList => {
  const defaultData: NotificationList = {
    totalCount: 0,
    perPage: 0,
    page: 0,
    data: [],
  };

  const { data, error } = useSWR<NotificationList>(
    `/notifications`,
    notificationMockFetcher,
    {
      initialData: defaultData,
      suspense: true,
      revalidateOnMount: true,
      refreshInterval: 60000,
      dedupingInterval: 10000,
    }
  );

  return data;
};
