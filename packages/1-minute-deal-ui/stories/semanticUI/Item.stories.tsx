// @ts-nocheck
import React from "react";
import { Item, ItemProps } from "semantic-ui-react";
import { SemanticCOLORS, SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Item",
  component: Item,
  decorators: [],
};

const Template: Story<Props> = (args: ItemProps) => <Item {...args} />;

export const Default = Template.bind({});
Default.args = {
  as: "div",
  className: "",
  //   content: "CONTENT",
  description: "DESCRIPTION",
  extra: "EXTRA",
  header: "HEADER",
  meta: "META",
};

const ComplexTemplate: Story<Props> = (args: any) => (
  <Item>
    <Item.Content>
      <Item.Header {...args?.header}></Item.Header>
      <Item.Meta {...args?.meta}></Item.Meta>
      <Item.Description {...args?.description}></Item.Description>
      <Item.Extra {...args?.extra}></Item.Extra>
    </Item.Content>
  </Item>
);
export const Complex = ComplexTemplate.bind({});
Complex.args = {
  header: {
    as: "h2",
    content: "HEADER CONTENT",
  },
  meta: {
    as: "b",
    content: "META CONTENT",
  },
  description: {
    as: "div",
    content: "DESCRIPTION CONTENT",
  },
  extra: {
    as: "i",
    content: "EXTRA CONTENT",
  },
};
