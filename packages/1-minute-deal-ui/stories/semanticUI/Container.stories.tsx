// @ts-nocheck
import React from "react";
import { Container, ContainerProps } from "semantic-ui-react";
import { SemanticCOLORS, SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Container",
  component: Container,
  decorators: [],
  argTypes: {
    textAlign: {
      control: {
        type: "select",
        options: ["left", "center", "right", "justified"],
      },
    },
  },
};

const Template: Story<Props> = (args: ContainerProps) => (
  <Container {...args} />
);

export const Default = Template.bind({});
Default.args = {
  as: "div",
  // "children"
  className: "",
  content:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices convallis ornare. Nullam sollicitudin arcu nibh. Vivamus fringilla leo nec nisl posuere pellentesque. Ut sem ex, scelerisque nec finibus quis, commodo sit amet leo. Mauris porttitor justo nec metus maximus euismod. Nullam tincidunt nulla lorem, a luctus felis facilisis nec. Maecenas tincidunt porttitor rhoncus. Sed mattis dolor quis enim ultricies venenatis. In pulvinar urna sed leo blandit, ut volutpat elit dapibus.",
  fluid: false,
  text: false,
  textAlign: "justified",
};
