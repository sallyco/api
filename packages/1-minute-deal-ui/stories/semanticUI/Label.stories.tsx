// @ts-nocheck
import React from "react";
import { Label, LabelProps } from "semantic-ui-react";
import { SemanticCOLORS, SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Label",
  component: Label,
  decorators: [],
  argTypes: {
    attached: {
      control: {
        type: "select",
        options: [
          "top",
          "bottom",
          "top left",
          "top right",
          "bottom left",
          "bottom right",
        ],
      },
    },
    color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    pointing: {
      control: {
        type: "select",
        options: ["above", "below", "left", "right"],
      },
    },
    size: {
      control: {
        type: "select",
        options: SemanticSIZES,
      },
    },
  },
};

const Template: Story<Props> = (args: LabelProps) => <Label {...args} />;

export const Default = Template.bind({});
Default.args = {
  active: false,
  as: "label",
  attached: "top left",
  basic: false,
  // "children"
  circular: false,
  className: "",
  color: "black",
  content: "",
  corner: false,
  detail: "",
  empty: false,
  floating: false,
  horizontal: false,
  icon: false,
  image: false,
  onClick: () => null,
  onRemove: () => null,
  pointing: false,
  prompt: false,
  //   removeIcon:Icon,
  ribbon: false,
  size: "medium",
  tag: false,
};
