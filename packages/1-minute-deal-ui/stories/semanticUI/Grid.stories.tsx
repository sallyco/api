// @ts-nocheck
import React from "react";
import { Grid, GridProps } from "semantic-ui-react";
import { SemanticCOLORS } from "../helpers/SemanticOptions";
import _ from "lodash";
interface Props {}

const subsetArgs = (args: object, prefix: string): object => {
  let ret = {};
  for (const [key, value] of Object.entries(args)) {
    if (key.toString().startsWith(prefix)) {
      ret[key.substring(prefix.length)] = value;
    }
  }
  return ret;
};

export default {
  title: "SemanticUI / Grid",
  component: Grid,
  decorators: [],
  argTypes: {
    BASE_columns: {
      control: {
        type: "select",
        options: [
          "1",
          "2",
          "3",
          "4",
          "5",
          "6",
          "7",
          "8",
          "9",
          "10",
          "11",
          "12",
          "13",
          "14",
          "15",
          "16",
        ],
      },
    },
    BASE_padded: {
      control: {
        type: "select",
        options: ["horizontally", "vertically"],
      },
    },
    BASE_textAlign: {
      control: {
        type: "select",
        options: ["left", "center", "right", "justified"],
      },
    },
    BASE_verticalAlign: {
      control: {
        type: "select",
        options: ["bottom", "middle", "top"],
      },
    },
    ROW_color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    ROW_textAlign: {
      control: {
        type: "select",
        options: ["left", "center", "right", "justified"],
      },
    },
    ROW_verticalAlign: {
      control: {
        type: "select",
        options: ["bottom", "middle", "top"],
      },
    },
    COLUMN_color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    COLUMN_floated: {
      control: {
        type: "select",
        options: ["left", "right"],
      },
    },
    COLUMN_textAlign: {
      control: {
        type: "select",
        options: ["left", "center", "right", "justified"],
      },
    },
    COLUMN_verticalAlign: {
      control: {
        type: "select",
        options: ["bottom", "middle", "top"],
      },
    },
    COLUMN_width: {
      control: {
        type: "select",
        options: [
          "1",
          "2",
          "3",
          "4",
          "5",
          "6",
          "7",
          "8",
          "9",
          "10",
          "11",
          "12",
          "13",
          "14",
          "15",
          "16",
        ],
      },
    },
  },
};

const Template: Story<Props> = (args: GridProps) => (
  <Grid {...subsetArgs(args, "BASE_")}>
    {_.times(3, (i) => (
      <Grid.Row key={`row${i}`} {...subsetArgs(args, "ROW_")}>
        {_.times(parseInt(subsetArgs(args, "BASE_").columns), (j) => (
          <Grid.Column key={`row${i}col${j}`} {...subsetArgs(args, "COLUMN_")}>
            {i}-{j}
          </Grid.Column>
        ))}
      </Grid.Row>
    ))}
  </Grid>
);

export const Default = Template.bind({});
Default.args = {
  BASE_as: "div",
  BASE_celled: false,
  BASE_centered: false,
  //   children: NODE,
  BASE_className: "",
  BASE_columns: "16",
  BASE_container: true,
  BASE_divided: true,
  BASE_doubling: false,
  BASE_inverted: false,
  BASE_padded: "horizontally",
  BASE_relaxed: false,
  BASE_stackable: false,
  BASE_stretched: false,
  BASE_textAlign: "left",
  BASE_verticalAlign: "middle",
  //   ROW_as: "",
  ROW_centered: false,
  //   ROW_children: NODE,
  ROW_className: "",
  ROW_color: "grey",
  ROW_columns: "16",
  ROW_divided: false,
  //   ROW_only: CUSTOM,
  //   ROW_reversed: CUSTOM,
  ROW_stretched: false,
  ROW_textAlign: "left",
  ROW_verticalAlign: "middle",
  //   COLUMN_as: "",
  //   COLUMN_children: "",
  COLUMN_className: "",
  COLUMN_color: "grey",
  //   COLUMN_computer: "16",
  COLUMN_floated: "left",
  //   COLUMN_largeScreen: "16",
  //   COLUMN_mobile: "16",
  //   COLUMN_only: CUSTOM,
  COLUMN_stretched: true,
  //   COLUMN_tablet: "16",
  COLUMN_textAlign: "left",
  COLUMN_verticalAlign: "middle",
  //   COLUMN_widescreen: "16",
  COLUMN_width: "1",
};
