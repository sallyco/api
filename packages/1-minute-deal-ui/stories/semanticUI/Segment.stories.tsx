// @ts-nocheck
import React from "react";
import { Segment, SegmentProps } from "semantic-ui-react";
import { SemanticCOLORS, SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Segment",
  component: Segment,
  decorators: [],
  argTypes: {
    attached: {
      control: {
        type: "select",
        options: ["top", "bottom"],
      },
    },
    color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    floated: {
      control: {
        type: "select",
        options: ["left", "right"],
      },
    },
    size: {
      control: {
        type: "select",
        options: SemanticSIZES,
      },
    },
    textAlign: {
      control: {
        type: "select",
        options: ["left", "center", "right"],
      },
    },
  },
};

const Template: Story<Props> = (args: SegmentProps) => <Segment {...args} />;

export const Default = Template.bind({});
Default.args = {
  as: "div",
  attached: "top",
  basic: false,
  // "children"
  circular: false,
  className: "",
  clearing: false,
  color: "black",
  compact: false,
  content:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices convallis ornare. Nullam sollicitudin arcu nibh. Vivamus fringilla leo nec nisl posuere pellentesque. Ut sem ex, scelerisque nec finibus quis, commodo sit amet leo. Mauris porttitor justo nec metus maximus euismod. Nullam tincidunt nulla lorem, a luctus felis facilisis nec. Maecenas tincidunt porttitor rhoncus. Sed mattis dolor quis enim ultricies venenatis. In pulvinar urna sed leo blandit, ut volutpat elit dapibus.",
  disabled: false,
  floated: "left",
  inverted: false,
  loading: false,
  padded: false,
  piled: false,
  placeholder: false,
  raised: false,
  secondary: false,
  size: "small",
  stacked: false,
  tertiary: false,
  textAlign: "left",
  vertical: false,
};
