// @ts-nocheck
import React from "react";
import { Button, ButtonProps } from "semantic-ui-react";
import { SemanticCOLORS, SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Button",
  component: Button,
  decorators: [],
  argTypes: {
    animated: {
      control: {
        type: "select",
        options: ["fade", "vertical"],
      },
    },
    attached: {
      control: {
        type: "select",
        options: ["left", "right", "top", "bottom"],
      },
    },
    color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    floated: {
      control: {
        type: "select",
        options: ["left", "right"],
      },
    },
    labelPosition: {
      control: {
        type: "select",
        options: ["left", "right"],
      },
    },
    size: {
      control: {
        type: "select",
        options: SemanticSIZES,
      },
    },
  },
};

const Template: Story<Props> = (args: ButtonProps) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
  active: false,
  animated: "fade",
  as: "button",
  attached: "left",
  basic: false,
  // "children"
  circular: false,
  className: "",
  color: "black",
  compact: false,
  content: "BUTTON",
  disabled: false,
  floated: "left",
  fluid: false,
  icon: false,
  inverted: false,
  label: "label",
  labelPosition: "left",
  loading: false,
  negative: false,
  onClick: () => null,
  positive: false,
  primary: false,
  role: "",
  secondary: false,
  size: "medium",
  tabIndex: "",
  toggle: false,
};
