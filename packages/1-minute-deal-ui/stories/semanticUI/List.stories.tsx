// @ts-nocheck
import React from "react";
import { List, ListProps } from "semantic-ui-react";
import { SemanticSIZES } from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / List",
  component: List,
  decorators: [],
  argTypes: {
    floated: {
      control: {
        type: "select",
        options: ["left", "right"],
      },
    },
    size: {
      control: {
        type: "select",
        options: SemanticSIZES,
      },
    },
    verticalAlign: {
      control: {
        type: "select",
        options: ["bottom", "middle", "top"],
      },
    },
  },
};

const Template: Story<Props> = (args: ListProps) => <List {...args} />;

export const Default = Template.bind({});
Default.args = {
  animated: false,
  //   as: "ul",
  bulleted: false,
  celled: false,
  // "children"
  className: "",
  //   content: "LIST CONTENT",
  divided: false,
  floated: "left",
  horizontal: false,
  inverted: false,
  items: ["one", "two", "three"],
  link: false,
  onItemClick: () => null,
  ordered: false,
  relaxed: false,
  selection: false,
  size: "medium",
  verticalAlign: "top",
};
