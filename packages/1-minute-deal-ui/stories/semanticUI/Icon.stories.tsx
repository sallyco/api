// @ts-nocheck
import React from "react";
import { Icon, IconProps } from "semantic-ui-react";
import {
  SemanticCOLORS,
  SemanticICONS,
  SemanticSIZES,
} from "../helpers/SemanticOptions";

interface Props {}

export default {
  title: "SemanticUI / Icon",
  component: Icon,
  decorators: [],
  argTypes: {
    color: {
      control: {
        type: "select",
        options: SemanticCOLORS,
      },
    },
    flipped: {
      control: {
        type: "select",
        options: ["horizontally", "vertically"],
      },
    },
    name: {
      control: {
        type: "select",
        options: SemanticICONS,
      },
    },
    rotated: {
      control: {
        type: "select",
        options: ["clockwise", "counterclockwise"],
      },
    },
    size: {
      control: {
        type: "select",
        options: SemanticSIZES,
      },
    },
    corner: {
      control: {
        type: "select",
        options: ["top left", "top right", "bottom left", "bottom right"],
      },
    },
  },
};

const Template: Story<Props> = (args: IconProps) => (
  <Icon name="circle" {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "aria-hidden": "",
  "aria-label": "",
  as: "i",
  bordered: false,
  circular: false,
  className: "",
  color: "black",
  corner: false,
  disabled: false,
  fitted: false,
  flipped: "horizontally",
  inverted: false,
  link: false,
  loading: false,
  name: "circle",
  rotated: "clockwise",
  size: "small",
};

const GroupTemplate: Story<Props> = (args: IconProps) => (
  <Icon.Group size={args?.size}>
    <Icon name={args?.name} {...args} />
    <Icon corner={"top right"} color={args?.cornercolor} name={args?.name} />
  </Icon.Group>
);

export const GroupDefault = GroupTemplate.bind({});
GroupDefault.args = {
  ...Default.args,
  cornercolor: "red",
};
