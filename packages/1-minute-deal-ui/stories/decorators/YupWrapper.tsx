// @ts-nocheck
import { Formik } from "formik";
import { Form } from "semantic-ui-react";
import React from "react";
import { Story as StoryType } from "@storybook/react";

interface Props {
  Story: StoryType;
  validation: any;
  initialValues: any;
  [x: string]: any;
}

export const YupWrapper: React.FunctionComponent<Props> = ({
  Story,
  validation,
  initialValues,
  ...props
}) => {
  return (
    <Formik
      initialValues={initialValues ?? {}}
      onSubmit={() => null}
      validationSchema={validation ?? Story.validation}
      {...props}
    >
      <Form onSubmit={() => null}>
        <Story />
      </Form>
    </Formik>
  );
};
export default YupWrapper;
