// @ts-nocheck
import { Formik } from "formik";
import { Form } from "semantic-ui-react";
import { Provider } from "react-redux";
import React from "react";
import configureStore from "redux-mock-store";
import { Story as StoryType } from "@storybook/react";
const mockStore = configureStore([]);

const store = mockStore({});

const FormikWrapper: React.FC<{ Story: StoryType; [x: string]: any }> = (
  Story,
  ...props
) => (
  <Provider store={store}>
    <Formik initialValues={{}} onSubmit={() => null} {...props}>
      <Form>
        <Story />
      </Form>
    </Formik>
  </Provider>
);
export default FormikWrapper;
