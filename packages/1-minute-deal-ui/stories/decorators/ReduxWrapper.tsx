// @ts-nocheck
import { Provider } from "react-redux";
import React from "react";
import configureStore from "redux-mock-store";
import { Story as StoryType } from "@storybook/react";
const mockStore = configureStore([]);

const myMockedStore = mockStore({});

const ReduxWrapper: React.FC<{
  Story: StoryType;
  store: any;
  [x: string]: any;
}> = (Story, store, ...props) => (
  <Provider
    store={Object.keys(store).length ? mockStore(store) : myMockedStore}
  >
    <Story />
  </Provider>
);
export default ReduxWrapper;
