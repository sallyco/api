// @ts-nocheck
import React from "react";
import { Story as StoryType } from "@storybook/react";
import { HashRouter as Router } from "react-router-dom";

const ReduxWrapper: React.FC<{
  Story: StoryType;
  [x: string]: any;
}> = (Story, ...props) => (
  <Router>
    <Story />
  </Router>
);
export default ReduxWrapper;
