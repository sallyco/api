// @ts-nocheck
import React from "react";
import { Story as StoryType } from "@storybook/react";
import { TenantContext } from "../../src/contexts";

interface Props {
  Story: StoryType;
  tenant: any;
  [x: string]: any;
}

export const TenantContextWrapper: React.FunctionComponent<Props> = ({
  Story,
  tenant,
  ...props
}) => {
  return (
    <TenantContext.Provider value={tenant}>
      <Story />
    </TenantContext.Provider>
  );
};
