import React from "react";
import SubformBOwner, {
  defaultValues,
  validationSchema,
} from "../../../src/forms/subforms/SubformBOwner";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Subform / Beneficial Owner",
  component: SubformBOwner,
  decorators: [
    (Story) => YupWrapper({ Story, validation, initialValues: defaultValues }),
  ],
};

const Template = (args) => <SubformBOwner {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: {
    country: "United States of America",
    ...defaultValues,
  },
};
