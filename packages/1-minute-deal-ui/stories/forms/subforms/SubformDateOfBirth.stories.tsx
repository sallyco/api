import React from "react";
import SubformDateOfBirth, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformDateOfBirth";
import * as Yup from "yup";
import YupWrapper from "../../decorators/YupWrapper";

export default {
  title: "Subform / Date of Birth",
  component: SubformDateOfBirth,
  decorators: [
    (Story) =>
      YupWrapper({
        Story: Story,
        initialValues: defaultValues,
        validation: Yup.object().shape(validationSchema({ isRequired: true })),
      }),
  ],
};

const Template = (args) => <SubformDateOfBirth {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  id: "test",
  name: "test",
  placeholder: "Date",
  label: "Date",
  value: "",
};
