import React from "react";
import * as Yup from "yup";
import SubformAddress, {
  defaultValues,
  validationSchema,
} from "../../../src/forms/subforms/SubformAddress";
import YupWrapper from "../../decorators/YupWrapper";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Subform / Address",
  component: SubformAddress,
  decorators: [
    (Story) => YupWrapper({ Story, validation, initialValues: defaultValues }),
  ],
};

const Template = (args) => <SubformAddress {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: defaultValues,
};
