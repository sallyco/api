import React from "react";
import SubformSigner, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformSigner";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Subform / Signer",
  component: SubformSigner,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformSigner {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: {
    ...defaultValues,
    entityCountry: "United States of America",
    primarySignatoryisUSBased: "Yes",
  },
};
