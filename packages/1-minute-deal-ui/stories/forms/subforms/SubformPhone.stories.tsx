import React from "react";
import SubformPhone, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformPhone";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / Phone",
  component: SubformPhone,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformPhone {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {};
