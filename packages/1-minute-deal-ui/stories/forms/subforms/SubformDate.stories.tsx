import React from "react";
import SubformDate, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformDate";
import * as Yup from "yup";
import YupWrapper from "../../decorators/YupWrapper";

export default {
  title: "Subform / Date",
  component: SubformDate,
  decorators: [
    (Story) =>
      YupWrapper({
        Story: Story,
        initialValues: defaultValues,
        validation: Yup.object().shape(validationSchema({ isRequired: true })),
      }),
  ],
};

const Template = (args) => <SubformDate {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  id: "test",
  name: "test",
  placeholder: "Date",
  label: "Date",
  value: "",
};
