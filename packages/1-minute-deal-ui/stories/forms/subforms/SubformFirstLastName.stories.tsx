import React from "react";
import SubformFirstLastName, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformFirstLastName";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / First Last Name",
  component: SubformFirstLastName,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformFirstLastName {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  ...defaultValues,
};
