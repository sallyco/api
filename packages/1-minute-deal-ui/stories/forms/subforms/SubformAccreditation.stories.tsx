import React from "react";
import * as Yup from "yup";
import SubformAccreditation, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformAccreditation";
import YupWrapper from "../../decorators/YupWrapper";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Subform / Accreditation",
  component: SubformAccreditation,
  decorators: [
    (Story) => YupWrapper({ Story, validation, initialValues: defaultValues }),
  ],
};

const Template = (args) => <SubformAccreditation {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: defaultValues,
};
