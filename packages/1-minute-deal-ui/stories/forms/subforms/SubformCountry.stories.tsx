import React from "react";
import SubformCountry, {
  SubformCountryProps,
} from "../../../src/forms/subforms/SubformCountry";
import FormikWrapper from "../../decorators/FormikWrapper";

export default {
  title: "Subform / Country",
  component: SubformCountry,
  decorators: [FormikWrapper],
};

const Template = (args) => <SubformCountry {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  label: "Country",
  name: "country",
  id: "country",
  placeholder: "Select a Country",
  disabled: false,
} as SubformCountryProps;
