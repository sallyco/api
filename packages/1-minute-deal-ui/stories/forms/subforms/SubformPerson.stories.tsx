import React from "react";
import SubformPerson, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformPerson";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / Person",
  component: SubformPerson,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformPerson {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: defaultValues,
};
