import React from "react";
import FormikWrapper from "../../decorators/FormikWrapper";
import SubformCurrency from "../../../src/forms/subforms/SubformCurrency";
import { SemanticICONS } from "../../helpers/SemanticOptions";

export default {
  title: "Subform / Currency",
  component: SubformCurrency,
  decorators: [FormikWrapper],
  argTypes: {
    icon: {
      control: {
        type: "select",
        options: SemanticICONS,
      },
    },
  },
};

const Template = (args) => <SubformCurrency {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  id: "currency",
  name: "currency",
  placeholder: "$1,000",
  label: "Currency",
  icon: "dollar",
};
