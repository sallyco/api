import React from "react";
import SubformEmail, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformEmail";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / Email",
  component: SubformEmail,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformEmail {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  id: "email",
  name: "email",
  placeholder: "email@example.com",
  "data-testid": "email",
  label: "Email",
  value: "",
};
