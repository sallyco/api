import React from "react";
import SubformEntityTaxId, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformEntityTaxId";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / Entity Tax ID",
  component: SubformEntityTaxId,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformEntityTaxId {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  ...defaultValues,
};
