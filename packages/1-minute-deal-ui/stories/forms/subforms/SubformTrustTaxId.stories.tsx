import React from "react";
import { Story } from "@storybook/react";
import SubformTrustTaxId, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformTrustTaxId";
import * as Yup from "yup";
import YupWrapper from "../../decorators/YupWrapper";

const validation = Yup.object().shape({
  ...validationSchema({ isRequired: true }),
});

export default {
  title: "Subform / TrustTaxID",
  component: SubformTrustTaxId,
  decorators: [
    (Story) => YupWrapper({ Story, validation, initialValues: defaultValues }),
  ],
};

interface Props {}

const Template: Story<Props> = (args) => (
  <SubformTrustTaxId values={defaultValues} {...args} />
);

export const Default = Template.bind({});
// @ts-ignore
Default.args = {
  values: {
    ...defaultValues,
  },
};
