import React from "react";
import SubformPassword, {
  validationSchema,
  defaultValues,
} from "../../../src/forms/subforms/SubformPassword";
import YupWrapper from "../../decorators/YupWrapper";
import * as Yup from "yup";

const validation = Yup.object().shape({
  ...validationSchema(),
});

export default {
  title: "Subform / Password",
  component: SubformPassword,
  decorators: [
    (Story) => {
      return YupWrapper({ Story, validation, initialValues: defaultValues });
    },
  ],
};

const Template = (args) => <SubformPassword {...args} />;

export const Default = Template.bind({});
// @ts-ignore
Default.args = {};
