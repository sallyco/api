import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <link
            href="https://fonts.googleapis.com/css2?family=Yellowtail&display=swap"
            rel="stylesheet"
          />
          <link
            href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
            rel="stylesheet"
          />
          <link rel="icon" href="data:," />
        </Head>
        <body style={{ display: "block" }}>
          <style>
            {`
            body{
              padding: 0;
              margin: 0;
              font-size: 14px;
            }
            #app{
              min-height: 100vh;
            }
            #loader-wrapper{
              min-height: 100vh;
              background: #EDF2F7;
              display: flex;
              flex-direction: column;
              align-items: center;
              justify-content: center;
            }
            @keyframes pulsar {
              0%{
                transform:rotate(0deg);
              }
              50%{
                transform:rotate(180deg);
              }
              100%{
                transform:rotate(365deg);
              }
            }
            #loader-wrapper .pulsar{
              display: block;
              box-sizing: border-box;
              width: 50px;
              height: 50px;
              border-radius: 100px;
              border: 8px solid #718096;
              border-top-color: #2D3748;
              animation: pulsar 0.7s linear infinite;
            }
            #loader-wrapper .loading-label{
              font-family: sans-serif;
              color: #2D3748;
              margin: 1em;
            }
        `}
          </style>
          <div id="app">
            <div id="loader-wrapper">
              <div className="pulsar" />
              <div className="loading-label">Loading Assets...</div>
            </div>
          </div>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
