import React, { useEffect, useState } from "react";
import { HashRouter as Router } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { PersistGate } from "redux-persist/integration/react";
import * as Sentry from "@sentry/react";
import ThemeSelector from "../src/themes/ThemeSelector";
import ErrorPageFallback from "../src/components/ErrorPageFallback";
import NoSSR from "react-no-ssr";
import ParallelMarkets from "../src/components/accreditation/ParallelMarkets";
import { EnhancedStore } from "@reduxjs/toolkit";
import { Persistor } from "redux-persist/es/types";
import { History } from "history";
import dynamic from "next/dynamic";

const App = dynamic(() => import("../src/App"), {
  ssr: false,
});

// Main render
export default function Index() {
  const [store, setStore] = useState<EnhancedStore | undefined>();
  const [persistor, setPersistor] = useState<Persistor | undefined>();
  const [history, setHistory] = useState<History | undefined>();
  let dynamicallyImportPackage = async () => {
    const storePackage = await import("../src/store");
    // you can now use the package in here
    setStore(storePackage.default);
    setPersistor(storePackage.persistor);
    setHistory(storePackage.history);
  };
  useEffect(() => {
    dynamicallyImportPackage();
  }, []);
  return (
    <NoSSR>
      <style jsx global>{`
        @font-face {
          font-family: "Icons";
          src: url("/fonts/icons.woff2");
        }
      `}</style>
      <div suppressHydrationWarning>
        {typeof window === "undefined" ? null : (
          <>
            {store && window && persistor && history && (
              <ReduxProvider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                  <ConnectedRouter history={history}>
                    <Router>
                      <Sentry.ErrorBoundary fallback={ErrorPageFallback}>
                        <ThemeSelector>
                          <ParallelMarkets>
                            <App />
                          </ParallelMarkets>
                        </ThemeSelector>
                      </Sentry.ErrorBoundary>
                    </Router>
                  </ConnectedRouter>
                </PersistGate>
              </ReduxProvider>
            )}
          </>
        )}
      </div>
    </NoSSR>
  );
}

// if (process.env.NODE_ENV === "development") {
//   // (module as any).hot?.accept("./App", render);
// } else {
//   // Service Worker setup and installation
//   if ("serviceWorker" in navigator) {
//     window.addEventListener("load", () => {
//       let refreshing;
//       navigator.serviceWorker.addEventListener("controllerchange", function () {
//         if (refreshing) return;
//         refreshing = true;
//         window.location.reload();
//       });
//       //      navigator.serviceWorker
//       //        .register('/sw.js')
//       //        .then(registration => {
//       //
//       //          console.log('SW registered: ', registration);
//       ////          registration.onupdatefound = () => {
//       ////            console.log('onupdatefoune: ', registration);
//       //            registration.installing?.addEventListener('statechange', stateEvent => {
//       //              let target:ServiceWorker = stateEvent.target;
//       //              console.log('SW stateEvent: ', stateEvent);
//       //              if(stateEvent.target:ServiceWorker.state === 'installed' && registration.active){
//       //              //  window.location.reload();
//       //              //  store.dispatch(setApplicationUpdateStatus(true));
//       //              }
//       //            });
//       ////          }
//       //          setInterval(()=>{
//       //            registration.update();
//       //          }, 30000);
//       //
//       //        });
//     });
//   }
// }
