import { AppProps } from "next/app";
import React, { useEffect } from "react";
import "../src/themes/global.css";
import "../src/components/common/CopyrightFooter.css";
import "../src/development/BuildInfo.css";
import NoSSR from "react-no-ssr";

function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    if (
      typeof window !== "undefined" &&
      window.document.getElementById("app")
    ) {
      window.document.getElementById("app").remove();
    }
  });
  return (
    <NoSSR>
      <div suppressHydrationWarning>
        {typeof window === "undefined" ? null : <Component {...pageProps} />}
      </div>
    </NoSSR>
  );
}
export default App;
