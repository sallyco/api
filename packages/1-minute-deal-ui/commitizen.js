const custom = require('@digitalroute/cz-conventional-changelog-for-jira/configurable');

module.exports = custom({
    jiraPrefix: "GBT",
    jiraOptional: true,
    maxHeaderWidth: 150,
    scopes: [
      "organizer",
      "admin",
      "investor",
      "documents",
      "login",
      "invites",
      "forms",
      "profiles",
      "other"
    ]
});
