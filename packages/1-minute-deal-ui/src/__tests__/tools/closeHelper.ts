import { isSeriesEntityOfGlassboardMasterII } from "../../tools/closeHelpers";
import { Entity } from "../../api/entitiesApi";
import { describe, expect, it } from "@jest/globals";

describe("Close Helper Tests", () => {
  it("Should use the entity name to determine if entity is a series of Glassboard Master II", async () => {
    const GlassboardSeriesEntity: Entity = {
      arbitration: {
        city: "",
        country: "",
        state: "",
      },
      assetComposition: "",
      bankAccount: {
        accountName: "",
        accountNumber: "",
        bankAddress: "",
        bankName: "",
        beneficiaryAddress: "",
        beneficiaryPhone: "",
        routingNumber: "",
        swiftCode: "",
      },
      countryOfFormation: "",
      createdAt: "",
      dealId: "",
      ein: "",
      entityDocuments: {},
      entityType: "",
      expenseReserve: {},
      files: [],
      id: "",
      managementFee: {},
      managerId: "",
      masterEntityId: "",
      minInvestmentAmount: 0,
      name: "Series 001, a series of Glassboard Master II LLC",
      ownerId: "",
      regDExemption: "",
      registeredAgent: "",
      regulationType: "",
      stateOfFormation: "",
      transactions: [],
      updatedAt: "",
    };
    const OtherMasterEntity: Entity = {
      arbitration: {
        city: "",
        country: "",
        state: "",
      },
      assetComposition: "",
      bankAccount: {
        accountName: "",
        accountNumber: "",
        bankAddress: "",
        bankName: "",
        beneficiaryAddress: "",
        beneficiaryPhone: "",
        routingNumber: "",
        swiftCode: "",
      },
      countryOfFormation: "",
      createdAt: "",
      dealId: "",
      ein: "",
      entityDocuments: {},
      entityType: "",
      expenseReserve: {},
      files: [],
      id: "",
      managementFee: {},
      managerId: "",
      masterEntityId: "",
      minInvestmentAmount: 0,
      name: "Series 001, a series of FanClubSports Master LLC",
      ownerId: "",
      regDExemption: "",
      registeredAgent: "",
      regulationType: "",
      stateOfFormation: "",
      transactions: [],
      updatedAt: "",
    };
    expect(isSeriesEntityOfGlassboardMasterII(GlassboardSeriesEntity)).toBe(
      true
    );
    expect(isSeriesEntityOfGlassboardMasterII(OtherMasterEntity)).toBe(false);
  });
});
