import { describe, expect, it } from "@jest/globals";
import {
  currencyFormat,
  DocumentHelpers,
  startsWithSearch,
} from "../../tools/helpers";
import { Profile } from "../../api/profilesApi";
import mockAxios from "jest-mock-axios";
import { rest } from "msw";
import { API } from "../../api/swrApi";
import { server } from "../../__mocks__/msw/server";

afterEach(async () => {
  await mockAxios.reset();
});

describe("startsWithSearch Tests", () => {
  it("should only show items that start with input", () => {
    const searchItems = [
      {
        text: "blueberry",
        value: "blueberry",
      },
      {
        text: "apple",
        value: "apple",
      },
      {
        text: "asparagus",
        value: "Asparagus",
      },
      {
        text: "Ale",
        value: "apple",
      },
      {
        text: "Banana",
        value: "Banana",
      },
      {
        text: "orange",
        value: "orange",
      },
    ];
    const searchResults = startsWithSearch(searchItems, "a");
    expect(searchResults).toStrictEqual([
      {
        text: "apple",
        value: "apple",
      },
      {
        text: "asparagus",
        value: "Asparagus",
      },
      {
        text: "Ale",
        value: "apple",
      },
    ]);
  });
});

describe("currencyFormat Helper Tests", () => {
  it("should allow a string format with dollar sign", () => {
    expect(currencyFormat("$1000")).toBe("$1,000.00");
  });

  it("should allow a string format without dollar sign", () => {
    expect(currencyFormat("1000")).toBe("$1,000.00");
  });

  it("should allow a string format with float", () => {
    expect(currencyFormat("$1000.10")).toBe("$1,000.10");
  });

  it("should allow a number format", () => {
    expect(currencyFormat(1000)).toBe("$1,000.00");
  });

  it("should allow a float format", () => {
    expect(currencyFormat(1000.1)).toBe("$1,000.10");
  });
});

describe("DocumentHelper Tests", () => {
  it("Should return true if Profile is INDIVIDUAL with SSN", async () => {
    server.use(
      rest.post(
        API.FULL_PATH(API.DECISIONS("should-use-w9")),
        (req, res, context) => {
          return res(context.json({ shouldUseW9: { value: true } }));
        }
      )
    );
    const profile: Profile = {
      createdAt: "",
      deals: [],
      id: "",
      image: null,
      updatedAt: "",
      isUSBased: true,
      profileType: "INVESTOR",
      taxDetails: {
        registrationType: "INDIVIDUAL",
        taxIdentification: {
          type: "ssn",
          value: "123123123",
        },
      },
    };
    const call = DocumentHelpers.shouldUseW9(profile);
    await expect(call).resolves.toBe(true);
  });
  it("Should return false if Profile is INDIVIDUAL without SSN", async () => {
    server.use(
      rest.post(
        API.FULL_PATH(API.DECISIONS("should-use-w9")),
        (req, res, context) => {
          return res(context.json({ shouldUseW9: { value: false } }));
        }
      )
    );
    const profile: Profile = {
      createdAt: "",
      deals: [],
      id: "",
      image: null,
      updatedAt: "",
      isUSBased: true,
      profileType: "INVESTOR",
      taxDetails: {
        registrationType: "INDIVIDUAL",
        taxIdentification: {
          type: "itin",
          value: "123123123",
        },
      },
    };
    const call = DocumentHelpers.shouldUseW9(profile);
    await expect(call).resolves.toBe(false);
  });
  it("Should return true if Profile is ENTITY is Domestic with SSN", async () => {
    server.use(
      rest.post(
        API.FULL_PATH(API.DECISIONS("should-use-w9")),
        (req, res, context) => {
          return res(context.json({ shouldUseW9: { value: true } }));
        }
      )
    );
    const profile: Profile = {
      createdAt: "",
      deals: [],
      id: "",
      image: null,
      updatedAt: "",
      isUSBased: true,
      profileType: "INVESTOR",
      typeOfEntity: "LIMITED_LIABILITY_COMPANY",
      taxDetails: {
        registrationType: "ENTITY",
        taxIdentification: {
          type: "ein",
          value: "123123123",
        },
      },
    };
    const call = DocumentHelpers.shouldUseW9(profile);
    await expect(call).resolves.toBe(true);
  });

  it("Should return true if Profile is ENTITY with type of FOREIGN_ENTITY", async () => {
    server.use(
      rest.post(
        API.FULL_PATH(API.DECISIONS("should-use-w9")),
        (req, res, context) => {
          return res(context.json({ shouldUseW9: { value: true } }));
        }
      )
    );
    const profile: Profile = {
      createdAt: "",
      deals: [],
      id: "",
      image: null,
      updatedAt: "",
      isUSBased: true,
      profileType: "INVESTOR",
      typeOfEntity: "FOREIGN_ENTITY",
      taxDetails: {
        registrationType: "ENTITY",
        taxIdentification: {
          type: "ein",
          value: "123123123",
        },
      },
    };
    const call = DocumentHelpers.shouldUseW9(profile);
    await expect(call).resolves.toBe(true);
  });
});
