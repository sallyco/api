import { Close } from "../../api/closesApi";
import {
  SubscriptionStatusText,
  SUBSCRIPTION_DISPLAYED_STATUSES,
  DB_STATUSES,
  SubscriptionStatusInput,
  canDeleteSubscription,
  DISPLAY_ORDER,
  canRefundSubscription,
} from "../../tools/subscriptionStatuses";
import "@testing-library/jest-dom/extend-expect";
import { Subscription } from "../../api/subscriptionsApi";

const {
  INVITED,
  NEEDS_SIGNING,
  NEEDS_FUNDING,
  FUNDING_SENT,
  FUNDS_IN_TRANSIT_MANUAL,
  COMPLETED,
  CLOSED,
  CLOSE_PENDING,
  CUSTOM,
} = SUBSCRIPTION_DISPLAYED_STATUSES;

const testSubscription: Subscription = {
  amount: 0,
  createdAt: "",
  dealId: "",
  email: "",
  files: [],
  id: "",
  isDocsSigned: false,
  isKycAmlPassed: false,
  name: "",
  ownerId: "",
  profileId: "",
  updatedAt: "",
  status: "",
};

const testClose: Close = {
  id: "",
  dealId: "",
  entityId: "",
  files: [],
  // fundManagerSigned?: boolean
  createdAt: "",
  updatedAt: "",
};

describe("Subscription Statuses", () => {
  it("INVITED returns INVITED", () => {
    const subscription = { ...testSubscription, status: DB_STATUSES.INVITED };
    expect(SubscriptionStatusText(subscription)).toBe(INVITED);
  });

  it("COMPLETED status but no Close object returns COMPLETED", () => {
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.COMPLETED,
    };
    expect(SubscriptionStatusText(subscription)).toBe(COMPLETED);
  });

  it("COMPLETED status but Close not signed returns CLOSE_PENDING", () => {
    const close = { ...testClose, fundManagerSigned: false };
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.COMPLETED,
      close: close,
    };
    expect(SubscriptionStatusText(subscription)).toBe(CLOSE_PENDING);
  });

  it("COMPLETED and Close was signed by Fund Manager returns CLOSED", () => {
    const close = { ...testClose, fundManagerSigned: true };
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.COMPLETED,
      close: close,
    };
    expect(SubscriptionStatusText(subscription)).toBe(CLOSED);
  });

  it("PENDING returns NEEDS_SIGNING", () => {
    const subscription = { ...testSubscription, status: DB_STATUSES.PENDING };
    expect(SubscriptionStatusText(subscription)).toBe(NEEDS_SIGNING);
  });

  it("COMMITTED but no transactionId returns NEEDS_FUNDING", () => {
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.COMMITTED,
    };
    expect(SubscriptionStatusText(subscription)).toBe(NEEDS_FUNDING);
    expect(canDeleteSubscription(subscription)).toBeTruthy();
  });

  it("COMMITTED WITH transactionId returns FUNDING_SENT", () => {
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.COMMITTED,
      transactionIds: ["test1234"],
    };
    expect(SubscriptionStatusText(subscription)).toBe(FUNDING_SENT);
    expect(canDeleteSubscription(subscription)).toBeFalsy();
  });

  it("Derive displayed status from complex ID string", () => {
    const subscription = {
      ...testSubscription,
      status: DB_STATUSES.FUNDS_IN_TRANSIT_MANUAL,
    };
    expect(SubscriptionStatusText(subscription)).toBe(FUNDS_IN_TRANSIT_MANUAL);
  });

  it("Invited is the first displayed status", () => {
    expect(DISPLAY_ORDER[0]).toBe(INVITED);
    expect(DISPLAY_ORDER.pop()).toBe(CUSTOM);
  });

  it("canRefundSubscription should return true if subscription is COMPLETED with a transactionId", () => {
    const subscription = {
      ...testSubscription,
      status: "COMPLETED",
      transactionId: "12345",
    };
    expect(canRefundSubscription(subscription)).toBeTruthy();
  });

  it("canRefundSubscription should return false if subscription status is COMPLETED without a transactionId", () => {
    const subscription = {
      ...testSubscription,
      reverseTransactionId: "234",
      status: "COMPLETED",
    };
    expect(canRefundSubscription(subscription)).toBeFalsy();
  });

  it("canRefundSubscription should return false if subscription status is not COMPLETED", () => {
    let subscription = {
      ...testSubscription,
      status: "PENDING",
    };
    expect(canRefundSubscription(subscription)).toBeFalsy();
    subscription = {
      ...testSubscription,
      status: "CLOSED",
    };
    expect(canRefundSubscription(subscription)).toBeFalsy();
    subscription = {
      ...testSubscription,
      status: "REFUNDED",
    };
    expect(canRefundSubscription(subscription)).toBeFalsy();
  });

  it("canRefundSubscription should return false if subscription has a reverseTransactionId", () => {
    const subscription = {
      ...testSubscription,
      status: "COMPLETED",
      transactionId: "test12312",
      reverseTransactionId: "test123455",
    };
    expect(canRefundSubscription(subscription)).toBeFalsy();
  });
});
