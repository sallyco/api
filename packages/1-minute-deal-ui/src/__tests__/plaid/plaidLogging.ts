import * as Sentry from "@sentry/react";
import {
  buildPlaidSentryContext,
  logPlaidLinkError,
  NO_LINK_ERROR,
} from "../../plaid/plaidLogging";

jest.mock("@sentry/react");

const testError = {
  error_code: "123",
  error_type: "123",
  error_message: "123",
  display_message: "123",
};
const testExtra = {
  specialData: "yup",
};
const convertedError = {
  tags: {
    "plaid.error_code": "123",
    "plaid.error_type": "123",
  },
  extra: {
    plaidErrorMessage: "123",
    plaidDisplayMessage: "123",
    plaidMetaData: { specialData: "yup" },
  },
};

test("if null error was given to onExit, we create a tag", () => {
  const context = buildPlaidSentryContext(null, testExtra);
  expect(context).toEqual({
    tags: {
      "plaid.error_code": NO_LINK_ERROR,
      "plaid.error_type": NO_LINK_ERROR,
    },
    extra: {
      plaidErrorMessage: "",
      plaidDisplayMessage: "",
      plaidMetaData: { specialData: "yup" },
    },
  });
});

test("can build a context object for sentry", () => {
  const context = buildPlaidSentryContext(testError, testExtra);
  expect(context).toEqual(convertedError);
});

test("plaid error is converted to sentry context", () => {
  logPlaidLinkError({ ...testError }, testExtra);

  expect(Sentry.captureMessage).toHaveBeenCalled();
  expect(Sentry.captureMessage).toHaveBeenCalledWith(
    "Plaid connection error",
    convertedError
  );
});
