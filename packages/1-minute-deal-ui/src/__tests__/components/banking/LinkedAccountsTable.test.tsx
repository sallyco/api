import React from "react";
import { Store } from "redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  act,
  cleanup,
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { waitFor } from "@testing-library/dom";
import userEvent from "@testing-library/user-event";
import { cache } from "swr";

import LinkedAccountsTable from "../../../components/banking/LinkedAccountsTable";
import { RootState } from "../../../rootReducer";
import {
  getProfileSuccess,
  updateProfileStart,
  updateProfileSuccess,
} from "../../../slices/profilesSlice";
import { server } from "../../../__mocks__/msw/server";
import { rest } from "msw";
import { API } from "../../../api/swrApi";
import { TestComponentWrapper } from "../../../__mocks__/helpers";
import { createMemoryHistory } from "history";

const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;
let falseyProfile: any;
let truthyProfile: any;
let plaidLinkToken: string;
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

beforeEach(async () => {
  plaidLinkToken = "link-sandbox-7da28343-d9e6-4c58-8af3-16aa7c1ebbe4";
  falseyProfile = {
    id: "60f52163f7df8400010bc15d",
    ownerId: "8336a397-7f2d-4843-ab79-8ef6caba4428",
    tenantId: "gbt",
    profileType: "INVESTOR",
    stateOfFormation: null,
    countryOfFormation: null,
    displayName: "Reuben Mohr",
    name: "Reuben Mohr",
    jointAccountName: null,
    typeOfEntity: null,
    jointType: null,
    firstName: "Reuben",
    lastName: "Mohr",
    address: {
      address1: "32589 Ruecker Trail",
      city: "Hollisberg",
      state: "California",
      postalCode: "81273",
      country: "United States of America",
    },
    deliveryAddress: null,
    phone: "(000) 000-1234",
    dateOfBirth: null,
    dateOfFormation: null,
    email: "reuben.mohr@fantastic.test",
    image: null,
    taxDetails: {
      registrationType: "INDIVIDUAL",
      taxIdentification: {
        type: "INDIVIDUAL",
        value: "",
      },
    },
    passportId: null,
    title: null,
    isUSBased: null,
    isRIA: null,
    isERA: null,
    ERAStatus: null,
    primaryContact: null,
    isSingleMemberLLC: null,
    primarySignatory: null,
    additionalSignatories: null,
    beneficialOwner: null,
    signers: null,
    investorStatus: null,
    managerId: null,
    masterEntityId: null,
    bankingUser: {
      accounts: [
        {
          counterpartyId: "cp_11gjhh763074mk",
          accountName: "Citibank Online ending in 1111",
          plaidAccountId: "6128c4e1f38ef556ceeb0e72",
          plaidSubAccountId: "Jgq76KPaApSZkZ7ybldAh97b83NJb3sbVB34B",
          isDefaultForDistributions: false,
        },
      ],
    },
    purchaserStatus: [1],
    createdAt: null,
    updatedAt: "2021-08-27T10:59:33.208Z",
  };
  truthyProfile = {
    id: "60f52163f7df8400010bc15e",
    ownerId: "8336a397-7f2d-4843-ab79-8ef6caba4428",
    tenantId: "gbt",
    profileType: "INVESTOR",
    stateOfFormation: null,
    countryOfFormation: null,
    displayName: "Test Name",
    name: "Test Name",
    jointAccountName: null,
    typeOfEntity: null,
    jointType: null,
    firstName: "Reuben",
    lastName: "Mohr",
    address: {
      address1: "32589 Ruecker Trail",
      city: "Hollisberg",
      state: "California",
      postalCode: "81273",
      country: "United States of America",
    },
    deliveryAddress: null,
    phone: "(000) 000-1234",
    dateOfBirth: null,
    dateOfFormation: null,
    email: "reuben.mohr@fantastic.test",
    image: null,
    taxDetails: {
      registrationType: "INDIVIDUAL",
      taxIdentification: {
        type: "INDIVIDUAL",
        value: "",
      },
    },
    passportId: null,
    title: null,
    isUSBased: null,
    isRIA: null,
    isERA: null,
    ERAStatus: null,
    primaryContact: null,
    isSingleMemberLLC: null,
    primarySignatory: null,
    additionalSignatories: null,
    beneficialOwner: null,
    signers: null,
    investorStatus: null,
    managerId: null,
    masterEntityId: null,
    bankingUser: {
      accounts: [
        {
          counterpartyId: "cp_11gjhh763074mk",
          accountName: "Citibank Online ending in 1111",
          plaidAccountId: "6128c4e1f38ef556ceeb0e72",
          plaidSubAccountId: "Jgq76KPaApSZkZ7ybldAh97b83NJb3sbVB34B",
          isDefaultForDistributions: true,
        },
      ],
    },
    purchaserStatus: [1],
    createdAt: null,
    updatedAt: "2021-08-27T10:59:33.208Z",
  };
  store = mockStore({
    profiles: {
      profilesById: {
        [falseyProfile.id]: falseyProfile,
      },
    },
  });
  jest.spyOn(console, "error").mockImplementation(() => {});
});

function renderComponent() {
  return render(
    <TestComponentWrapper store={store} history={createMemoryHistory()}>
      <LinkedAccountsTable plaidLinkToken={plaidLinkToken} />
    </TestComponentWrapper>
  );
}

describe("Link Accounts Table Tests", () => {
  // it('Happy render', () => {
  //   expect(() => renderComponent()).not.toThrow();
  // });

  // Disabled due to inconsistent runs
  // it('Shows status color as gray if profile lists with non-default bank account', async () => {
  //   server.use(
  //     rest.get(API.FULL_PATH(API.INVESTOR_PROFILES_OF_USER), (req, res, ctx) => {
  //       return res(
  //         ctx.json([
  //           falseyProfile,
  //         ]),
  //       );
  //     }),
  //     rest.get(API.FULL_PATH(API.PROFILES), (req, res, ctx) => {
  //       return res(
  //         ctx.json({
  //           data: [
  //             falseyProfile,
  //           ],
  //         }),
  //       );
  //     }),
  //   );
  //   await act(async ()=>{
  //     const component = renderComponent();
  //     await waitFor(() =>
  //       expect(component.getByTestId('status-color')).toBeInTheDocument());
  //     await waitFor(() =>
  //       expect(component.getByTestId('status-color')).toHaveStyle(
  //         'backgroundColor: gray',
  //       ),
  //     );
  //   })
  // });

  it("Shows status color as green if profile lists with default bank account", async () => {
    server.use(
      rest.get(
        API.FULL_PATH(API.INVESTOR_PROFILES_OF_USER),
        (req, res, ctx) => {
          return res(ctx.json([truthyProfile]));
        }
      ),
      rest.get(API.FULL_PATH(API.PROFILES), (req, res, ctx) => {
        return res(
          ctx.json({
            data: [truthyProfile],
          })
        );
      })
    );
    await act(async () => {
      const component = renderComponent();
      await waitFor(() =>
        expect(component.getByTestId("status-color")).toBeVisible()
      );
      await waitFor(() =>
        expect(component.getByTestId("status-color")).toHaveStyle(
          "backgroundColor: green"
        )
      );
      component.unmount();
    });
  });

  it("It should set the account as default for profile", async () => {
    server.use(
      rest.get(
        API.FULL_PATH(API.INVESTOR_PROFILES_OF_USER),
        (req, res, ctx) => {
          return res(ctx.json([falseyProfile]));
        }
      ),
      rest.get(API.FULL_PATH(API.PROFILES), (req, res, ctx) => {
        return res(
          ctx.json({
            data: [falseyProfile],
          })
        );
      })
    );
    await act(async () => {
      const component = renderComponent();
      await waitFor(() =>
        expect(component.getByTestId("status-button")).toBeVisible()
      );

      const updatedProfile = {
        ...falseyProfile,
        bankingUser: {
          accounts: [
            {
              counterpartyId: "cp_11gjhh763074mk",
              accountName: "Citibank Online ending in 1111",
              plaidAccountId: "6128c4e1f38ef556ceeb0e72",
              plaidSubAccountId: "Jgq76KPaApSZkZ7ybldAh97b83NJb3sbVB34B",
              isDefaultForDistributions: true,
            },
          ],
        },
      };
      userEvent.click(component.getByTestId("status-button"));
      await waitFor(() =>
        expect(component.getByText("Set as default")).toBeVisible()
      );
      await waitFor(() => expect(component.getByText("Confirm")).toBeVisible());
      component.unmount();
    });
  });
});
