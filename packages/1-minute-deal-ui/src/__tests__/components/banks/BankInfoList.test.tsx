import React from "react";
import { cleanup, render, screen } from "@testing-library/react";

import BankInfoList from "../../../components/banks/BankInfoList";

// A mock bank account object:
// Bank account interface?...
// see src/api/entitiesApi.ts Entity.bankAccount structure
const bankAccountMock = {
  bankName: "My Bank Name",
  // bankAddress: "123 Fake Street and more",
  accountNumber: "123fakeAccount",
  routingNumber: "123fakeRouting",
  swiftCode: "swiftCode",
  beneficiaryAddress: "beneficiary fake street",
  beneficiaryPhone: "(111) 111 1111",
  accountName: "My Account Name",
};

const testReference = {
  entityName: "test",
  investorProfileName: "investor",
};

test("Bank Info renders the List successfully", () => {
  const testAccount = {
    ...bankAccountMock,
  };
  render(<BankInfoList bankAccount={testAccount} reference={testReference} />);

  expect(
    screen.getByText(`Bank Name: ${testAccount.bankName}`)
  ).toBeInTheDocument();
  expect(screen.queryByText("Bank Address")).not.toBeInTheDocument();
});

test("Bank Info shows address if  provided", () => {
  const testAccount = {
    ...bankAccountMock,
    bankAddress: "123 Fake Street and more",
  };
  render(<BankInfoList bankAccount={testAccount} reference={testReference} />);

  expect(
    screen.getByText(`Bank Address: ${testAccount.bankAddress}`)
  ).toBeInTheDocument();
});
