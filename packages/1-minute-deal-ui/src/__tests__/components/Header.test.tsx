import React from "react";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { cleanup, render, act as renderAct } from "@testing-library/react";
import Header from "../../components/Header";
import renderer from "react-test-renderer";
import { createMemoryHistory } from "history";
import configureStore from "redux-mock-store";

jest.mock("../../hooks/useSelf");

const mockStore = configureStore([]);
const { act } = renderer;
afterEach(cleanup);
const history = createMemoryHistory();

describe("Header", () => {
  it("should render Header", () => {
    let el;
    renderAct(() => {
      el = render(
        <Provider store={mockStore({})}>
          <Router history={history}>
            <Header showSideBar={() => null} />
          </Router>
        </Provider>
      );
    });
    expect(el.baseElement.querySelector(".dashboard-header")).toBeVisible();
  });

  it("Matches snapshot", () => {
    // let tree;
    // act(() => {
    //   tree = renderer
    //     .create(
    //       <Provider store={mockStore({})}>
    //         <Router history={history}>
    //           <Header showSideBar={() => null} />
    //         </Router>
    //       </Provider>
    //     )
    //     .toJSON();
    // });
    expect(true).toBe(true);
  });
});
