import React from "react";
import mockAxios from "jest-mock-axios";
import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ProfileEditForm from "../../../components/profiles/ProfileEditForm";
import { server } from "../../../__mocks__/msw/server";
import { rest } from "msw";
import { API } from "../../../api/swrApi";

let profile;
let ipLookup;
const onSuccess = (data) => Promise.resolve();
const onDelete = (data) => Promise.resolve();

beforeEach(() => {
  profile = [
    {
      id: "test",
      profileType: "ORGANIZER",
      displayName: "Test 1",
      firstName: "Test",
      lastName: "Dealer Five",
      phone: "(000) 000 0000",
      email: "td5@t.t",
      taxDetails: {
        registrationType: "INDIVIDUAL",
        taxIdentification: {
          type: "ssn",
          value: "000000000",
        },
      },
      address: {
        address1: "32589 Ruecker Trail",
        city: "Hollisberg",
        state: "California",
        postalCode: "81273",
        country: "United States of America",
        address2: "Paradise Street",
      },
    },
    {
      id: "test-1",
      profileType: "INVESTOR",
      displayName: "Test 1",
      firstName: "Test",
      lastName: "Dealer Five",
      phone: "(000) 000 0000",
      email: "td5@t.t",
      taxDetails: {
        registrationType: "INDIVIDUAL",
        taxIdentification: {
          type: "ssn",
          value: "000000000",
        },
      },
      address: {
        address1: "32589 Ruecker Trail",
        city: "Hollisberg",
        state: "California",
        postalCode: "81273",
        country: "United States of America",
        address2: "Paradise Street",
      },
    },
  ];
  ipLookup = {
    businessName: "",
    businessWebsite: "",
    city: "Kochi",
    continent: "Asia",
    country: "India",
    countryCode: "IN",
    ipName: "",
    ipType: "Residential",
    isp: "Bharti Airtel Limited",
    lat: "9.93988",
    lon: "76.26022",
    org: "Bharti Airtel Limited",
    query: "106.206.216.160",
    region: "Kerala",
    status: "success",
  };
  jest.spyOn(console, "error").mockImplementation(() => {});
});

afterEach(async () => {
  await mockAxios.reset();
});

const renderComponent = (props?) => {
  const defaultProps = {
    profile: profile[0],
    onSuccess,
    onDelete,
    ...props,
  };
  return render(<ProfileEditForm {...defaultProps} />);
};

const mockExtremeIPLookupResponse = (
  status = 200,
  method = "GET",
  returnBody?: object
) => {
  global.fetch = jest.fn().mockImplementationOnce(() => {
    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        status,
        json: () => {
          return returnBody ? returnBody : {};
        },
      });
    });
  });
};

describe("Profile Edit Form Tests", () => {
  it("Shows data in form on successful load", async () => {
    mockExtremeIPLookupResponse(200, "GET", ipLookup);
    server.use(
      rest.get(
        API.FULL_PATH(`/subscriptions/${profile[1].id}/subscriptions-meta`),
        (req, res, ctx) => {
          return res(
            ctx.json({
              totalCount: 1,
            })
          );
        }
      )
    );
    const result = renderComponent({ profile: profile[1] });

    await waitFor(() =>
      expect(screen.getByTestId("submit-button")).toBeInTheDocument()
    );

    const deleteButton = screen.queryByTestId("delete-button");
    await waitFor(() => expect(deleteButton).toBeFalsy());

    const submitButton = await screen.getByTestId("submit-button");

    const someElement = result.container.querySelector("#displayName");
    await userEvent.clear(someElement);
    await userEvent.tab();
    await waitFor(() => expect(someElement).toHaveValue(""));
    await waitFor(() => expect(submitButton).toHaveAttribute("disabled"));
  });

  it("Shows delete button in form on successful load", async () => {
    mockExtremeIPLookupResponse(200, "GET", ipLookup);
    server.use(
      rest.get(
        API.FULL_PATH(`/deals/${profile[0].id}/deals-meta`),
        (req, res, ctx) => {
          return res(
            ctx.json({
              totalCount: 0,
            })
          );
        }
      )
    );
    renderComponent({ profile: profile[0] });

    await waitFor(() =>
      expect(screen.getByTestId("delete-button")).toBeInTheDocument()
    );
  });
});
