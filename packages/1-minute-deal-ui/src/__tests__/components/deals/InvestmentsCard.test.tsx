import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, act, waitFor } from "@testing-library/react";
import InvestmentsCard from "../../../components/deals/InvestmentsCard";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { Store } from "redux";
import { RootState } from "../../../rootReducer";
import { Deal } from "../../../api/dealsApi";
import { Subscription } from "../../../api/subscriptionsApi";
import { createMemoryHistory } from "history";
import { TestComponentWrapper } from "../../../__mocks__/helpers";
const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;
let deal: Deal;
let subscription: Subscription;

beforeEach(async () => {
  deal = {
    createdAt: new Date().toISOString(),
    description: "Test",
    id: "test",
    ownerId: "test",
    isPublic: false,
    name: "test",
    portfolioCompanyEntity: "Test",
    portfolioCompanyName: "Test",
    portfolioCompanyState: "Test",
    profileId: "testprofile123",
    status: "COMPLETED",
    targetRaiseAmount: 10000,
    updatedAt: new Date().toISOString(),
  };
  subscription = {
    files: [],
    isDocsSigned: false,
    isKycAmlPassed: false,
    amount: 1000,
    createdAt: new Date().toISOString(),
    dealId: deal.id,
    email: "test@test.com",
    id: "test123",
    name: "Test Tester",
    ownerId: "test",
    profileId: "test",
    status: "PENDING",
    updatedAt: new Date().toISOString(),
  };
  store = mockStore({
    deals: {
      dealsById: {
        [deal.id]: deal,
      },
    },
    closes: {
      closesById: {},
    },
    subscriptions: {
      subscriptionsById: {
        [subscription.id]: subscription,
      },
      subscriptionsByDealId: {
        [deal.id]: [subscription],
      },
    },
    profiles: {
      profilesById: {},
    },
    users: {
      self: {},
    },
  });
});

describe("Investments Card Tests", () => {
  it("Shows row if subscription in store for deal", async () => {
    await act(async () => {
      const component = render(
        <TestComponentWrapper store={store} history={createMemoryHistory()}>
          <InvestmentsCard dealId={deal.id} />
        </TestComponentWrapper>
      );
      await waitFor(() =>
        expect(component.queryByText("test@test.com")).toBeInTheDocument()
      );
    });
  });
});
