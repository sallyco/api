import React from "react";
import { Provider as ReduxProvider } from "react-redux";
import { Store } from "redux";
import configureMockStore from "redux-mock-store";
import { HashRouter } from "react-router-dom";
import thunk from "redux-thunk";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import SideLetterForm from "../../../components/deals/SideLetterForm";
import { RootState } from "../../../rootReducer";

let deal;
let entity;
let profile;
const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;

beforeEach(() => {
  deal = {
    id: "61151f85eb3d8a9162599b3d",
    entityId: "61151f86eb3d8a9162599b3e",
    name: "Test Deal 5.1",
    marketing: {},
    targetRaiseAmount: 50000,
    previouslyRaisedAmount: 5000,
    isPublic: false,
    estimatedCloseDate: new Date("2021-09-01T00:00:00.000Z"),
    organizerCarryPercentage: 4,
    additionalCarryRecipients: [
      {
        carryPercentage: 10,
        individual: {
          id: "test",
          name: "Test Carry Recipient One",
          type: "Individual",
          isUSBased: false,
          address: {
            address1: "Address",
            address2: "Address 2",
            city: "City",
            state: "Assam",
            postalCode: "00000",
            country: "India",
          },
          phone: "000000000000",
          email: "test@gbt-test.com",
          stateOfFormation: "Assam",
          countryOfFormation: "India",
        },
      },
    ],
    status: "OPEN",
    requireQualifiedPurchaser: false,
    createdAt: "2021-08-12T13:17:57.913Z",
    updatedAt: "2021-09-01T12:42:48.580Z",
    profileId: "60f521b79fecba0c2404de0d",
  };
  entity = {
    additionalProperties: null,
    arbitration: null,
    assetComposition: null,
    countryOfFormation: "US",
    createdAt: "2021-08-12T13:17:58.184Z",
    dealId: "61151f85eb3d8a9162599b3d",
    ein: "12-3119578",
    entityDocuments: {
      operatingAgreement: "612f757e1ad0dfa304832793",
      privatePlacementMemorandum: "612f5d52d1487b86fc207335",
      subscriptionAgreement: "612f5d57d1487b86fc207336",
    },
    operatingAgreement: "612f757e1ad0dfa304832793",
    privatePlacementMemorandum: "612f5d52d1487b86fc207335",
    subscriptionAgreement: "612f5d57d1487b86fc207336",
    entityType: "LIMITED_LIABILITY_COMPANY",
    expenseReserve: null,
    files: null,
    id: "61151f86eb3d8a9162599b3e",
    legalIncOrder: { status: 1, orderId: 1080746 },
    orderId: 1080746,
    status: 1,
    managementFee: { type: "percent", amount: 3, frequency: "Annual" },
    amount: 3,
    frequency: "Annual",
    type: "percent",
    managerId: null,
    masterEntityId: null,
    minInvestmentAmount: 1,
    name: "Series 002, a series of Glassboard Master II LLC",
    ownerId: "8336a397-7f2d-4843-ab79-8ef6caba4428",
    regDExemption: null,
    registeredAgent: null,
    regulationType: "REGULATION_D",
    stateOfFormation: null,
    tenantId: "gbt",
    updatedAt: "2021-09-01T12:43:43.250Z",
  };
  profile = {
    id: "60f521b79fecba0c2404de0d",
    displayName: "Test profile",
  };
  store = mockStore({
    deals: {
      dealsById: {
        [deal.id]: deal,
      },
    },
    entities: {
      entitiesById: {
        [deal.entityId]: entity,
      },
    },
    profiles: {
      profilesById: {
        [profile.id]: profile,
      },
    },
  });
  jest.spyOn(console, "error").mockImplementation(() => {});
});

const renderComponent = (props?) => {
  const defaultProps = {
    deal,
    onSubmit: () => {},
  };
  return render(
    <ReduxProvider store={store}>
      <HashRouter>
        <SideLetterForm {...defaultProps} {...props} />
      </HashRouter>
    </ReduxProvider>
  );
};

describe("Side Letter Form Tests", () => {
  it("Happy render", () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it("Should display data on successful load", async () => {
    const result = renderComponent();

    const organizerCarryPercentage = result.container.querySelector(
      "#organizerCarryPercentage"
    );
    await waitFor(() => expect(organizerCarryPercentage).toHaveValue("4%"));

    await waitFor(() =>
      expect(screen.getByTestId("submit-button")).toBeInTheDocument()
    );

    const submit = await screen.getByTestId("submit-button");
    await waitFor(() => expect(submit).toHaveAttribute("disabled"));

    const enableCarryPercentageEdit = result.container.querySelector(
      "#enableCarryPercentageEdit"
    );
    await userEvent.click(enableCarryPercentageEdit);
    await waitFor(() => expect(submit).not.toHaveAttribute("disabled"));
  });
});
