import React from "react";
import { Provider as ReduxProvider } from "react-redux";
import { Store } from "redux";
import configureMockStore from "redux-mock-store";
import { HashRouter } from "react-router-dom";
import thunk from "redux-thunk";
import mockAxios from "jest-mock-axios";
import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import CarryRecipientsCard from "../../../components/deals/CarryRecipientsCard";
import { RootState } from "../../../rootReducer";
import { updateDealStart, updateDealSuccess } from "../../../slices/dealsSlice";
import {
  generateEntityDocumentStart,
  generateEntityDocumentSuccess,
} from "../../../slices/entitySlice";
import { server } from "../../../__mocks__/msw/server";
import { rest } from "msw";
import { API } from "../../../api/swrApi";

let deal;
let entity;
let profile;
const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;

beforeEach(() => {
  deal = {
    id: "61151f85eb3d8a9162599b3d",
    entityId: "61151f86eb3d8a9162599b3e",
    name: "Test Deal 5.1",
    marketing: {},
    targetRaiseAmount: 50000,
    previouslyRaisedAmount: 5000,
    isPublic: false,
    estimatedCloseDate: new Date("2021-09-01T00:00:00.000Z"),
    organizerCarryPercentage: 4,
    additionalCarryRecipients: [
      {
        carryPercentage: 10,
        individual: {
          id: "test",
          name: "Test Carry Recipient One",
          type: "Individual",
          isUSBased: false,
          address: {
            address1: "Address",
            address2: "Address 2",
            city: "City",
            state: "Assam",
            postalCode: "00000",
            country: "India",
          },
          phone: "000000000000",
          email: "test@gbt-test.com",
          stateOfFormation: "Assam",
          countryOfFormation: "India",
        },
      },
    ],
    status: "OPEN",
    requireQualifiedPurchaser: false,
    createdAt: "2021-08-12T13:17:57.913Z",
    updatedAt: "2021-09-01T12:42:48.580Z",
    profileId: "60f521b79fecba0c2404de0d",
  };
  entity = {
    additionalProperties: null,
    arbitration: null,
    assetComposition: null,
    countryOfFormation: "US",
    createdAt: "2021-08-12T13:17:58.184Z",
    dealId: "61151f85eb3d8a9162599b3d",
    ein: "12-3119578",
    entityDocuments: {
      operatingAgreement: "612f757e1ad0dfa304832793",
      privatePlacementMemorandum: "612f5d52d1487b86fc207335",
      subscriptionAgreement: "612f5d57d1487b86fc207336",
    },
    operatingAgreement: "612f757e1ad0dfa304832793",
    privatePlacementMemorandum: "612f5d52d1487b86fc207335",
    subscriptionAgreement: "612f5d57d1487b86fc207336",
    entityType: "LIMITED_LIABILITY_COMPANY",
    expenseReserve: null,
    files: null,
    id: "61151f86eb3d8a9162599b3e",
    legalIncOrder: { status: 1, orderId: 1080746 },
    orderId: 1080746,
    status: 1,
    managementFee: { type: "percent", amount: 3, frequency: "Annual" },
    amount: 3,
    frequency: "Annual",
    type: "percent",
    managerId: null,
    masterEntityId: null,
    minInvestmentAmount: 1,
    name: "Series 002, a series of Glassboard Master II LLC",
    ownerId: "8336a397-7f2d-4843-ab79-8ef6caba4428",
    regDExemption: null,
    registeredAgent: null,
    regulationType: "REGULATION_D",
    stateOfFormation: null,
    tenantId: "gbt",
    updatedAt: "2021-09-01T12:43:43.250Z",
  };
  profile = {
    id: "60f521b79fecba0c2404de0d",
    displayName: "Test profile",
  };
  store = mockStore({
    deals: {
      dealsById: {
        [deal.id]: deal,
      },
    },
    entities: {
      entitiesById: {
        [deal.entityId]: entity,
      },
    },
    profiles: {
      profilesById: {
        [profile.id]: profile,
      },
    },
  });
  jest.spyOn(console, "error").mockImplementation(() => {});
});

afterEach(async () => {
  await mockAxios.reset();
});

const renderComponent = (props?) => {
  const defaultProps = {
    dealId: deal.id,
    editable: true,
  };
  return render(
    <ReduxProvider store={store}>
      <HashRouter>
        <CarryRecipientsCard {...defaultProps} {...props} />
      </HashRouter>
    </ReduxProvider>
  );
};

describe("Carry Recipients Card Tests", () => {
  it("Happy render", () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it("Shows carry percentage in table on successful load", async () => {
    renderComponent();

    await waitFor(() =>
      expect(screen.getByTestId("organizer-carry")).toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.getByTestId("organizer-carry").innerHTML).toBe(
        deal.organizerCarryPercentage.toString() + "%"
      )
    );

    await waitFor(() =>
      expect(screen.getByTestId("additional-carry-0")).toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.getByTestId("additional-carry-0").innerHTML).toBe(
        deal.additionalCarryRecipients[0].carryPercentage.toString() + "%"
      )
    );
  });

  it("Should call entity update API when additional carry is deleted", async () => {
    renderComponent();

    const updatedDeal = {
      ...deal,
      additionalCarryRecipients: [],
    };

    await waitFor(() =>
      expect(screen.getByTestId("delete-carry-0-button")).toBeInTheDocument()
    );
    const deleteAdditionalCarry = await screen.getByTestId(
      "delete-carry-0-button"
    );
    server.use(
      rest.get(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
        const { dealId } = req.params;
        return res(
          ctx.json({
            ...deal,
          })
        );
      })
    );

    await act(async () => {
      await userEvent.click(deleteAdditionalCarry);
      store.dispatch(updateDealStart);
      store.dispatch(updateDealSuccess(updatedDeal));
      store.dispatch(generateEntityDocumentStart);
      store.dispatch(generateEntityDocumentSuccess({ id: "testDocumentId" }));
    });
  });
});
