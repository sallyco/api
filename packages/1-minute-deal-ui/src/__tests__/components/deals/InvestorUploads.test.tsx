import React from "react";
import { Provider as ReduxProvider } from "react-redux";
import { Store } from "redux";
import configureMockStore from "redux-mock-store";
import { HashRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import thunk from "redux-thunk";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import mockAxios from "jest-mock-axios";

import InvestorUploads from "../../../components/deals/InvestorUploads";
import { RootState } from "../../../rootReducer";

let subscription;
let file;
const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;

beforeEach(() => {
  subscription = {
    id: "61480d037d279a09241742c8",
    tenantId: "gbt",
    dealId: "61151f85eb3d8a9162599b3d",
    name: "Test Investor",
    email: "t@t.t",
    phone: null,
    type: null,
    status: "COMMITTED",
    amount: 1,
    reconciledAmount: null,
    ownershipPercentageAtClose: null,
    ownershipPercentageAtDistribution: null,
    transactionId: null,
    transactionIds: null,
    bankTransactionIds: null,
    reverseTransactionId: null,
    distributionTransactionIds: null,
    isDocsSigned: true,
    isAmountMatched: true,
    isKycAmlPassed: false,
    packageId: null,
    signers: null,
    bankAccount: null,
    accountTransaction: null,
    documents: null,
    files: [],
    closeDoc: null,
    createdAt: "2021-09-20T09:54:35.984+05:30",
    updatedAt: "2021-09-20T09:55:16.679+05:30",
    signature:
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXwAAADICAYAAADry1odAAAKZ0lEQVR4nO3dvW4j1cPAYQu6BSkjhBAFki1RREhI9lZbbKS4o3RqGqeDzhRIlA4VHeYOvCWdKeiNxAXMJcwlnEs4b8E7/o8dx3HijxnPeR5pah9v8VvnzPnoRACS0Kl7AACch+ADJELwARIh+ACJEHyARAg+QCIEHyARgg+QCMEHSITgAyRC8AESIfgAiRB8gEQIPkAiBB8gEYIPkAjBB0iE4AMkQvABEiH4AIkQfIBECD5AIgQfIBGCD5AIwQdIhOADJELwARIh+ACJEHyARAg+QCIEHyARgg+QCMEHSITgAyRC8AESIfgAiRB8gEQIPkAiBB8gEYIPkAjBB0iE4AMkQvABEiH4AIkQfIBECD5AIgQfIBGCD5AIwd8hhBD/+eefGEKoeygABxP8Hfr9fux0OnEwGIg+cPEEf4erq6vY6XREH2gFwd8hz/PY7XbXol8URd3DAngVwX9Gnudrv/R7vV7M87zuYQG8mODvYTP6WZaJPnBxBH9P26K/WCzqHhbA3gT/BTaj3+l04nw+r3tYAHsR/BfaFv2Hh4e6hwXwLMF/hW3Rv7+/r3tYADsJ/istFou14Is+0HSCf4D5fP4o+jc3N3UPC2ArwT/QeDz2Sx+4CIJ/BNXduOUzm83qHhbAGsE/gqIotkbfkk2gSQT/SLat3MmyzNk7QGMI/hFtW7kzHA7rHhZAjFHwj242m5nPBxpJ8E/g9vbW1A7QOIJ/AiGERy9xTe0AdRP8E8nz3NQO0CiCf0LbXuL++++/dQ8LSJTgn9jm8QtfffWVu3GBWgj+GSwWi/jxxx+vXZMo+sC5Cf6ZbE7vDAYD0QfOSvDPaHN6R/SBcxL8M9uMfq/Xs0YfOAvBr8Fm9LMsi3me1z0soOUEvybbor9cLuseFtBigl+jxWLx6IRNRyoDpyL4Ndt2rLLoA6cg+A0QQoj9fl/0gZMS/AYZjUbm9IGTEfyGqV6K7lhl4JgEv4Emk4ljGICjE/yGqkZ/NBrVPRygBQS/warTO9PptO7hABdO8Bvuu+++W0V/sVjUPRzgggl+w4UQVnfkOncHOITgX4CiKFabswaDQd3DAS6U4F+I6nn6k8mk7uEAF0jwL8h0OnUhOvBqgn9hyiMYHKkMvJTgXxjz+cBrCf4Fms1mpnaAFxP8C1Uu1XTeDrAvwb9QIYTV1M7d3V3dwwEugOBfsOrUjl24wHME/8JVd+E6VRPYRfAvXJ7nDlgD9iL4LVBuyPICF9hF8Fui2+06Ox/YSfBbonrWjgvQgW0Ev0XKW7KyLKt7KEADCX6LhBBWUzvj8bju4QANI/gts1wurc0HthL8Fio3ZA0GA6t2gBXBb6lyQ5ZjF4CS4LdUURRO1ATWCH6LlVM7LksBYhT81huNRi5LAWKMgt961WOUnbUDaRP8BMzn89V8/nK5rHs4QE0EPxHl1I5jlCFdgp8IN2QBgp+Q6gFrduFCegQ/MdUD1kztQFoEP0HlAWvD4bDuoQBnJPgJql6LaBcupEPwE1XdheuANUiD4CesPGDNLlxIg+AnzC5cSIvgJ666VNMBa9Bugs9qqWav16t7KMAJCT4xxhj7/X7sdDpxMpnUPRTgRASfGON/SzXfvHkTP/vsM7twoaUEn5XqXbh24UL7CD5rylM17+/v6x4KcGSCz5rqUs35fF73cIAjEnweKS9M6fV6duFCiwg+W7kLF9pH8NnKLtyXK4oiPjw8xOFwuNrM9u2338abm5v44cMHG9uoneDzJLtw91MURby/v48fffTR6t/rqafX68UPHz5YBUUtBJ+dxuOxqZ0dZrNZzLLs2dA/FX44J8FnpxCCXbhPKP8zrD5ff/11nM/ncblcxvl8Ht+9e7cz/O/evYvL5bLur0IiBJ9nVS9MEaf/bMZ+PB7vnKZZLpdxMpms3otsPnd3d1ZEcXKCz16m0+lqKiLl+ecQQry5uVmF+urq6kVHUYQQ4nQ6jZ9++umj6GdZFv/4448Tjn73uH766Seb7lpO8NlbObVzd3dX91BqURRFHAwGq0B3u91Xv8wOIcQffvhhdb9w9fn888/Pep7RcrmMvV5vbQw23bWT4LO36tROakHI83wt9tfX10eZggkhrPY8bHuxe39/H//666/Dv8ATyvOTNh8H6LWT4PMi1aWaqcw553m+thJnNBodfVprPp/HTz755MmXu1mWHX1Vz88//7z1s+y7aC/B58XKX6QpXJiyGfvxeHyyzwohxO+///7Z5ZzH2BNRHp9Rffr9vpfyLSf4vFgIYTX33OalmkVRnC32m587mUy2zu+XzyEvdzf/E+t0OvHt27dH/AY0leDzKsvlstXzvSGEtTn7c8V+U57n8e3btzunefadWsvzPN7e3j5aGtrtdpNeeZUSwefVyhd+bTxVs1yRVGfsq/I8XxvT5jMYDOKXX34Zv/jiizgYDOJwOFx7dk0TmcZJh+BzkNvb29jpdOJwOKx7KEdT3VTVhNiXQghbd/ce8sxms7q/Fmck+BykeqpmG+bzf/zxx1UMb29v6x7OVnmex/fv3x8U+m+++aaVU3HsJvgcrFyqmWXZRUdkMpmsxb7p89rlrt3yr6znnm63GyeTiZNPEyb4HEUZyyzLGh/Kbaqxv76+vsjvEON/K3yKooh5nsflcrl6IEbB54jKl4qXNp9f3W16dXXVuhfQUBJ8jibP84u7JWtzA5LpDtpM8DmqakCbPp+/GfumjxcOJfgcXXn0QpZljZ0eqW4cszyRVAg+R1e9JWswGDTuBeg5z8eBJhF8TqI6n9+kCzVCCGtnvzd1rT2cguBzMk2cz6+ePd/v9xv31weckuBzUtX1+XXP55fXNFp+SaoEn5OrzufXZfMlbVP+4oBzEnxOriiKWs/bCSGsvaS9lD0CcGyCz1lUr0Y896/r6rHCXtKSMsHnbMo59CzLzrajtXpGjos+SJ3gc1blyY7nWJ9ffUnb6XTin3/+edLPg6YTfM7qXOfnb76kHY1GJ/ssuBSCz9lVT6c8xdG9m5urOp2OJZgQBZ+anHJ9fnXe3qoc+B/BpxYhhNjtdo9+fn6e52uxv7q68qIW/p/gU5vqUs1j/QrfvO7Pr3v4H8GnVtXpl0OXalbfDfh1D48JPrUrN0YdMp+/eeSxX/fwmOBTu+pRyq85byeEEAeDwVrs+/3+CUYKl03waYTqfP5LLyQZj8drsXc3LWwn+DRGdWfsvpuyfv/990exd10hbCf4NMr79+9X8/nPHbIWQohv3rxZi73rCuFpgk+jVO/D7fV6O1fZbE7lXF9fW5UDOwg+jVPdPPXUfbjVOX/XFcJ+BJ9Gqt49uzm1UxTF2hJM1xXCfgSfRqqeqpllWfztt9/i33//HR8eHh7F3ooc2I/g01jz+fzRCpzN5xSnbUJbCT6N9lT0u92uX/bwQoJP4xVFEX/99df4yy+/xOl0evY7caEtBB8gEYIPkAjBB0iE4AMkQvABEiH4AIkQfIBECD5AIgQfIBGCD5AIwQdIhOADJELwARIh+ACJEHyARAg+QCIEHyARgg+QCMEHSITgAyRC8AESIfgAifg/+CbzpqNewL0AAAAASUVORK5CYII=",
    deletedAt: null,
    isDeleted: false,
    acquisitionMethod: "INVITATION",
    ownerId: "38551bae-bc41-4f20-bcf3-88aa8a755739",
    profileId: "6135de97d2a0ec5697daee86",
    additionalProperties: null,
    additionalFiles: ["614871763a62137bd9fd26a6"],
  };
  file = {
    id: "614871763a62137bd9fd26a6",
    ownerId: "38551bae-bc41-4f20-bcf3-88aa8a755739",
    tenantId: "gbt",
    name: "sample-2.pdf",
    type: "application/pdf",
    key: "6802dbc0-f65e-4939-97d2-0044d6d931a8",
    lastModified: "1632137590460",
    createdAt: "2021-09-20T17:03:10.460+05:30",
    updatedAt: "2021-09-20T17:03:10.460+05:30",
    isDeleted: false,
    generating: false,
    isPublic: false,
  };
  store = mockStore({
    subscriptions: {
      subscriptionsById: {
        [subscription.id]: subscription,
      },
    },
    files: {
      filesById: {
        [file.id]: file,
      },
    },
  });
  jest.spyOn(console, "error").mockImplementation(() => {});
});

afterEach(async () => {
  await mockAxios.reset();
});

const renderComponent = (props?) => {
  const defaultProps = {
    subscriptionId: subscription.id,
    canUpload: true,
  };
  return render(
    <ReduxProvider store={store}>
      <HashRouter>
        <ToastContainer />
        <InvestorUploads {...defaultProps} {...props} />
      </HashRouter>
    </ReduxProvider>
  );
};

describe("Investor Uploads Card Tests", () => {
  it("Happy render", () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it("Shows the file in card on successful load", async () => {
    renderComponent();

    await waitFor(() =>
      expect(screen.getByTestId("file-upload")).toBeInTheDocument()
    );

    await waitFor(() =>
      expect(screen.getByText("sample-2.pdf")).toBeInTheDocument()
    );
  });

  it("Successfully uploads a file", async () => {
    renderComponent();

    await waitFor(() =>
      expect(screen.getByTestId("file-upload")).toBeInTheDocument()
    );

    const str = JSON.stringify({ name: "sample-text" });
    const blob = new Blob([str]);
    const file = new File([blob], "sample.json", {
      type: "application/JSON",
    });
    File.prototype.text = jest.fn().mockResolvedValueOnce(str);
    const input = await screen.getByTestId("file-upload");
    userEvent.upload(input, file);

    const formData = new FormData();
    formData.append("files", file);

    const uploadResult = [
      {
        createdAt: "2021-09-21T05:16:55.763Z",
        generating: false,
        id: "61496ac77641a3143b5e73d8",
        isPublic: false,
        key: "0f205d75-896d-444f-af0c-16771a9828bf",
        lastModified: "1632201415763",
        name: "sample.json",
        tenantId: "gbt",
        type: "application/JSON",
        updatedAt: "2021-09-21T05:16:55.763Z",
      },
    ];

    await waitFor(() =>
      expect(
        screen.getByText("File uploaded to subscription")
      ).toBeInTheDocument()
    );
  });
});
