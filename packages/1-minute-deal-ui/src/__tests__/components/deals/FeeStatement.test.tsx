import React from "react";
import { act, render, screen, waitFor } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import FeesStatement from "../../../components/deals/FeesStatement";
import { createMemoryHistory } from "history";
import { TestComponentWrapper } from "../../../__mocks__/helpers";
import { RootState } from "../../../rootReducer";
import thunk from "redux-thunk";

const middlewares = [thunk];
let feesStatement;
let tenant;
const dealId = "614d7169f918fb18abb740a4";
const mockStore = configureMockStore<RootState>(middlewares);

beforeEach(() => {
  feesStatement = {
    title: "Glassboard Technology - Test Seed",
    subTitle: "SPV Closing Statement",
    investors: [
      {
        name: "Test Investor Seven",
        state: "Goa",
        amount: "$1.00",
      },
      {
        name: "Test Investor Six",
        state: "Assam",
        amount: "$1.00",
      },
    ],
    sections: [
      {
        title: "Closing Statement Summary",
        entries: [
          {
            description: "Bank Balance",
            proceeds: "0",
          },
          {
            description: "Proceeds Collected From the Investors",
            proceeds: "$2.00",
            isInvestorRow: true,
          },
          {
            description: "Total Amount Witheld From Investor Proceeds",
            expenses: "$9,709.04",
          },
          {
            description: "Amount of Proceeds to be sent to Portfolio Company",
            proceeds: "$-9,707.04",
            bold: true,
          },
        ],
      },
      {
        title: "SPV Expenses",
        subTitle: "Entity Management & Maintenance Fees",
        entries: [
          {
            description: "EIN Obtainment",
            expenses: "$49.00",
          },
        ],
      },
      {
        title: "Administration Fees",
        entries: [
          {
            description: "SPV Administration Fixed Cost per SPV",
            expenses: "$9,500.00",
          },
          {
            description: "Management Fees",
            expenses: "$0.04",
          },
          {
            description: "Expense Reserve",
            expenses: "$0.00",
          },
        ],
      },
      {
        title: "Tax and Accounting Fees",
        entries: [
          {
            description: "Tax and K1 Processing",
            expenses: "included",
          },
        ],
      },
      {
        title: "Regulatory Filings Fees",
        entries: [
          {
            description: "NASAA EFD System Fee",
            expenses: "$160.00",
          },
        ],
      },
      {
        title: "TOTALS",
        entries: [
          {
            description: "",
            expenses: "$9,709.04",
          },
        ],
      },
    ],
    totalProceeds: 2,
    totalFees: 9709.04,
    proceedsToSend: -9707.04,
    spvFee: 9500,
    createdAt: "Fri Sep 24 2021 14:37:41 GMT+0530 (India Standard Time)",
  };
  tenant = {
    id: "gbt",
    inverted: true,
    managerId: null,
    masterEntityId: null,
    maxAllowedDaysToClose: 0,
    name: "Glassboard Technology",
    url: "https://glassboardtech.com",
    assets: {
      colors: {
        primaryColor: "#1D3557",
        secondaryColor: "#457B9D",
      },
      images: {
        logo: null,
        logoInverted: null,
      },
    },
    settings: {
      emails: {
        fromAddress: "",
      },
    },
    bankingLimits: {},
    realm: {
      id: "gbt",
      realm: "gbt",
      verifyEmail: true,
    },
    billingContact: "Test Billing Contact",
  };
  jest.spyOn(console, "error").mockImplementation(() => {});
});

const renderComponent = (props?) => {
  const defaultProps = {
    dealId,
  };
  return render(
    <TestComponentWrapper store={mockStore({})} history={createMemoryHistory()}>
      <FeesStatement {...defaultProps} {...props} />
    </TestComponentWrapper>
  );
};

describe("Fee Statement Tests", () => {
  it("Happy render", () => {
    expect(() => renderComponent()).not.toThrow();
  });

  it("Shows billing contact on successful load", async () => {
    await act(async () => {
      renderComponent();
    });
    await waitFor(() =>
      expect(screen.getByTestId("billing-contact")).toBeInTheDocument()
    );
  });
});
