import React from "react";
import "@testing-library/jest-dom";
import { cleanup, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";
import renderer from "react-test-renderer";
import NavBar from "../../components/NavBar";

afterEach(cleanup);
const history = createMemoryHistory();
function renderNavBarWithRouting() {
  return render(
    <Router history={history}>
      <NavBar />
    </Router>
  );
}

describe("NavBar rendering and routing test", () => {
  it("should render the navbar", () => {
    const el = renderNavBarWithRouting();
    expect(el.baseElement.querySelector(".main-navigation")).toBeVisible();
  });

  it("Matches snapshot", () => {
    const tree = renderer
      .create(
        <Router history={history}>
          <NavBar />
        </Router>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
