import React from "react";
import { render, fireEvent, waitFor, act } from "@testing-library/react";

import LoginForm from "../../../forms/LoginForm";
const renderLoginForm = (props?) => {
  const defaultProps = {
    onSubmit() {
      return;
    },
  };
  return render(<LoginForm {...defaultProps} {...props} />);
};

describe("<Login />", () => {
  test("should display a blank login form", async () => {
    await act(async () => {
      const { findByTestId } = renderLoginForm();
      const loginForm = await findByTestId("login-form");
      expect(loginForm).toHaveFormValues({
        email: "",
        password: "",
      });
    });
  });
  test("should allow submitting the form", async () => {
    const onSubmit = jest.fn();

    await act(async () => {
      const { findByTestId } = renderLoginForm({
        onSubmit,
      });

      const email = await findByTestId("email");
      const password = await findByTestId("password");
      const submit = await findByTestId("submit-button");
      fireEvent.change(email.querySelector("input") ?? email, {
        target: { value: "test@test.com" },
      });
      fireEvent.change(password.querySelector("input") ?? password, {
        target: { value: "password" },
      });
      fireEvent.click(submit);
    });

    await waitFor(() =>
      expect(onSubmit).toHaveBeenCalledWith(
        expect.objectContaining({
          email: "test@test.com",
          password: "password",
        }),
        expect.any(Object)
      )
    );
  });

  describe("email input tests", () => {
    it("should throw an error when an invalid email is used and display error message", async () => {
      const { container, findByTestId } = renderLoginForm();
      await act(async () => {
        const email = await findByTestId("email");
        fireEvent.change(email.querySelector("input") ?? email, {
          target: { value: "test@test.com" },
        });
        fireEvent.blur(email.querySelector("input") ?? email);
      });
      expect(container.querySelector("div.error")).toBeNull();
      await act(async () => {
        const email = await findByTestId("email");
        fireEvent.change(email.querySelector("input") ?? email, {
          target: { value: "test@" },
        });
        fireEvent.blur(email.querySelector("input") ?? email);
      });

      // await waitFor(() =>
      //   expect(
      //     container.querySelector("div#email-error-message")
      //   ).toBeInTheDocument()
      // );
    });
  });
});
