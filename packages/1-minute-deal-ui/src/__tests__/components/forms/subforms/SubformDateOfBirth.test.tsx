import React from "react";
import {
  render,
  fireEvent,
  waitFor,
  cleanup,
  screen,
} from "@testing-library/react";
import renderer from "react-test-renderer";

import SubformDateOfBirthStory from "../../../../../stories/forms/subforms/SubformDateOfBirth.stories";
const renderSubform = (props?) => {
  const defaultProps = {
    onSubmit() {
      return;
    },
  };
  const SubformDateOfBirth = SubformDateOfBirthStory.component;

  return render(SubformDateOfBirthStory.decorators[0](SubformDateOfBirth));
};

afterEach(cleanup);

describe("Forms / Subforms / Email", () => {
  it("should display an empty input with placeholder text", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    expect(subform.parentElement).toHaveFormValues({
      month: "",
      day: "",
      year: "",
      dateOfBirth: "--",
    });
  });

  it("MONTH should error when empty after blur", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const monthInput = subform.getElementsByTagName("input")[1];
    fireEvent.blur(monthInput);

    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent(
      /Month Is A Required Field/i
    );
  });

  it("DAY should error when empty after blur", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const dayInput = subform.getElementsByTagName("input")[2];
    fireEvent.blur(dayInput);

    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent(
      /Day Is A Required Field/i
    );
  });

  it("YEAR should error when empty after blur", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const yearInput = subform.getElementsByTagName("input")[3];
    fireEvent.blur(yearInput);

    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent(
      /Year Is A Required Field/i
    );
  });

  it("MONTH should error when invalid", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const monthInput = subform.getElementsByTagName("input")[1];
    fireEvent.change(monthInput, {
      target: {
        value: "1",
      },
    });
    fireEvent.blur(monthInput);
    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent("Invalid Month");
  });

  it("DAY should error when empty after blur", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const dayInput = subform.getElementsByTagName("input")[2];
    fireEvent.change(dayInput, {
      target: {
        value: "1",
      },
    });
    fireEvent.blur(dayInput);
    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent("Invalid Day");
  });

  it("YEAR should error when empty after blur", async () => {
    const { findByTestId } = renderSubform();
    const subform = await findByTestId("subform-date-of-birth");
    const yearInput = subform.getElementsByTagName("input")[3];
    fireEvent.change(yearInput, {
      target: {
        value: "1",
      },
    });
    fireEvent.blur(yearInput);
    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent("Invalid Year");
  });

  // it("should not error in correct format", async () => {
  //   const { findByTestId } = renderSubform();
  //   const subform = await findByTestId("subform-date-of-birth");
  //   const monthInput = subform.querySelector("#month");
  //   const dayInput = subform.querySelector("#day");
  //   const yearInput = subform.querySelector("#year");
  //   fireEvent.change(monthInput, {
  //     target: {
  //       value: "10",
  //     },
  //   });
  //   fireEvent.change(dayInput, {
  //     target: {
  //       value: "10",
  //     },
  //   });
  //   fireEvent.change(yearInput, {
  //     target: {
  //       value: "2000",
  //     },
  //   });
  //   expect(screen.queryAllByRole("alert")).toBeEmpty();
  // });

  it("Matches Snapshot", () => {
    const SubformEmail = SubformDateOfBirthStory.component;

    const tree = renderer
      .create(SubformDateOfBirthStory.decorators[0](SubformEmail))
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
