import React from "react";
import { render } from "@testing-library/react";
import { Formik } from "formik";
import { Form, Button } from "semantic-ui-react";
import * as Yup from "yup";

import SubformPurchaserStatus, {
  getPurchaserStatusMeta,
} from "./../../../../forms/subforms/SubformPurchaserStatus";

function TestComponent({ testSubmit, isQP = true }) {
  const { validationSchema, oneOfTest, defaultValues } =
    getPurchaserStatusMeta(isQP);

  // Configure your form using the
  // - returned validationSchema,
  // - oneOfTest function,
  // - and defaultValues
  const validation = Yup.object()
    .shape({
      ...validationSchema,
    })
    .test("run the oneOf test", null, oneOfTest);

  return (
    <Formik
      initialValues={defaultValues}
      validationSchema={validation}
      onSubmit={testSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit}>
          <SubformPurchaserStatus />
          <Button type="submit" id="testButton" disabled={!props.isValid}>
            Submit
          </Button>
        </Form>
      )}
    </Formik>
  );
}

const expectedDefaults = {
  purchaserStatus0: false,
  purchaserStatus1: false,
  purchaserStatus2: false,
  purchaserStatus3: false,
  purchaserStatus4: false,
  purchaserStatus5: false,
  purchaserStatus6: false,
  purchaserStatus7: false,
  purchaserStatus8: false, // This value is not allowed for Qualified Purchasers
  purchaserStatus9: false,
  purchaserStatus10: false,
};

describe("SubformPurchaserStatus Tests", () => {
  test("A Non-QP form allows purchaserStatus8", () => {
    const isQualifiedPurchaserDeal = false;
    const { validationSchema, oneOfTest, defaultValues } =
      getPurchaserStatusMeta(isQualifiedPurchaserDeal);

    expect(defaultValues).toEqual(expectedDefaults);

    let choseObject = {
      ...defaultValues,
      purchaserStatus7: true,
      purchaserStatus8: true,
    };

    expect(oneOfTest(choseObject)).toBe(true);
  });

  test("A QP form DOES NOT allow purchaserStatus8", () => {
    // Most of the magic happens here
    const isQualifiedPurchaserDeal = true;
    const { validationSchema, oneOfTest, defaultValues } =
      getPurchaserStatusMeta(isQualifiedPurchaserDeal);

    expect(defaultValues).toEqual(expectedDefaults);

    const invalidObject = {
      ...defaultValues,
      purchaserStatus7: true,
      purchaserStatus8: true, // This is not allowed
    };
    expect(oneOfTest(invalidObject)).not.toBe(true);

    const validObject = {
      ...defaultValues,
      purchaserStatus6: true,
      purchaserStatus7: true,
    };
    expect(oneOfTest(validObject)).toBe(true);
  });

  test("Render Test", async () => {
    const handleSubmit = jest.fn();
    const rendered = render(
      <TestComponent testSubmit={handleSubmit} isQP={true} />
    );
    const validOption = rendered.getByLabelText(/\(vii\)/);
    const rejectedOption = rendered.getByLabelText(/\(viii\)/);
    const lastOption = rendered.getByLabelText(
      /The Excepted Investment Company was formed after April/i
    );
    expect(validOption).toBeInTheDocument();
    expect(rejectedOption).toBeInTheDocument();
    expect(lastOption).toBeInTheDocument();
  });
});
