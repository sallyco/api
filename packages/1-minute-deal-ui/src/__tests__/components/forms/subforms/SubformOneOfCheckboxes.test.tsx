import React from "react";
import { render } from "@testing-library/react";
import { Formik } from "formik";
import { Form, Button } from "semantic-ui-react";
import * as Yup from "yup";

import SubformOneOfCheckboxes, {
  getMeta,
  OneOfCheckbox,
} from "./../../../../forms/subforms/SubformOneOfCheckboxes";

const checkboxes: OneOfCheckbox[] = [
  {
    fieldName: "field1",
    text: "text1",
  },
  {
    fieldName: "field2",
    text: "text2",
    options: {
      optional: true,
    },
  },
  {
    fieldName: "field3",
    text: "text3",
    options: {
      rejectIfSelected: true,
    },
  },
];

const expectedDefaults = {
  field1: false,
  field2: false,
  field3: false,
};

function TestComponent({ testSubmit }) {
  const { validationSchema, oneOfTest, defaultValues } = getMeta(
    checkboxes,
    true
  );

  // Configure your form using the
  // - returned validationSchema,
  // - oneOfTest function,
  // - and defaultValues
  const validation = Yup.object()
    .shape({
      ...validationSchema,
    })
    .test("run the oneOf test", null, oneOfTest);

  return (
    <Formik
      initialValues={defaultValues}
      validationSchema={validation}
      onSubmit={testSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit}>
          <SubformOneOfCheckboxes
            header={"My Header"}
            section1Checkboxes={checkboxes}
          />
          <Button type="submit" id="testButton" disabled={!props.isValid}>
            Submit
          </Button>
        </Form>
      )}
    </Formik>
  );
}

describe("SubformPurchaserStatus Tests", () => {
  test("When rejection is turned off, all non-optional fields validate the form", () => {
    const turnOnRejection = false;
    const { oneOfTest, defaultValues } = getMeta(checkboxes, turnOnRejection);
    expect(defaultValues).toEqual(expectedDefaults);

    let choseObject = {
      ...defaultValues,
      field3: true, // normally rejects form if selected
    };

    expect(oneOfTest(choseObject)).toBe(true);
  });

  test("When rejection is turned on, rejected fields fail form validation", () => {
    const turnOnRejection = true;
    const { oneOfTest, defaultValues } = getMeta(checkboxes, turnOnRejection);
    expect(defaultValues).toEqual(expectedDefaults);

    let choseObject = {
      ...defaultValues,
      field3: true, // normally rejects form if selected
    };

    expect(oneOfTest(choseObject)).not.toBe(true);
  });

  test("Optional fields don't make form pass validation", () => {
    const turnOnRejection = true;
    const { oneOfTest, defaultValues } = getMeta(checkboxes, turnOnRejection);
    expect(defaultValues).toEqual(expectedDefaults);

    let choseObject = {
      ...defaultValues,
      field2: true, // optional, shouldn't pass validation
    };

    expect(oneOfTest(choseObject)).not.toBe(true);
  });

  test("Render Test", async () => {
    const handleSubmit = jest.fn();
    const rendered = render(<TestComponent testSubmit={handleSubmit} />);
    const validOption = rendered.getByLabelText(/text1/i);
    const rejectedOption = rendered.getByLabelText(/text2/i);
    const lastOption = rendered.getByLabelText(/text3/i);
    expect(validOption).toBeInTheDocument();
    expect(rejectedOption).toBeInTheDocument();
    expect(lastOption).toBeInTheDocument();
  });
});
