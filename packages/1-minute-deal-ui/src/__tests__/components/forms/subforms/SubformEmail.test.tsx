import React from "react";
import {
  render,
  fireEvent,
  waitFor,
  cleanup,
  screen,
  getByPlaceholderText,
} from "@testing-library/react";

import SubformEmailStory from "../../../../../stories/forms/subforms/SubformEmail.stories";
const renderSubform = (props?) => {
  const defaultProps = {
    onSubmit() {
      return;
    },
  };
  const SubformEmail = SubformEmailStory.component;

  return render(SubformEmailStory.decorators[0](SubformEmail));
};

afterEach(cleanup);

describe("Forms / Subforms / Email", () => {
  it("should display an empty input with placeholder text", async () => {
    const { findByTestId } = renderSubform();
    const emailWrapper = await findByTestId("email");
    const Form = emailWrapper.parentElement.parentElement;
    expect(Form).toHaveFormValues({
      email: "",
    });
    emailWrapper.getElementsByTagName("input");
    expect(
      emailWrapper.getElementsByTagName("input")[0].getAttribute("placeholder")
    ).toBeTruthy();
  });

  it("should error when empty after blur", async () => {
    const { findByTestId, getByPlaceholderText } = renderSubform();
    const emailWrapper = await findByTestId("email");
    const emailInput = emailWrapper.getElementsByTagName("input")[0];
    fireEvent.blur(emailInput);

    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent("Email Is Required");
  });

  it("should error when not in correct format", async () => {
    const { findByTestId, getByPlaceholderText } = renderSubform();
    const emailWrapper = await findByTestId("email");
    const emailInput = emailWrapper.getElementsByTagName("input")[0];
    fireEvent.blur(emailInput);
    fireEvent.change(emailInput, {
      target: {
        value: "a",
      },
    });

    await waitFor(() => screen.getByRole("alert"));
    expect(screen.getByRole("alert")).toHaveTextContent(
      "Email Is Not In The Correct Format"
    );
  });

  it("should not error in correct format", async () => {
    const { findByTestId } = renderSubform();
    const emailWrapper = await findByTestId("email");
    const emailInput = emailWrapper.getElementsByTagName("input")[0];
    fireEvent.change(emailInput, {
      target: {
        value: "test@something.com",
      },
    });
    expect(screen.queryByRole("alert")).toBeNull();
  });
});
