import { renderHook, act } from "@testing-library/react-hooks";
import { useProfileSubmit } from "../../../forms/profiles/profileHooks";
import React from "react";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { RootState } from "../../../rootReducer";
import { Store } from "redux";
import { cleanup } from "@testing-library/react";
import { cache } from "swr";
import { server } from "../../../__mocks__/msw/server";
import { rest } from "msw";
import { API } from "../../../api/swrApi";
import { makeHookWrapper } from "../../../__mocks__/helpers";
import { createMemoryHistory } from "history";

const middlewares = [thunk];
const mockStore = configureMockStore<RootState>(middlewares);
let store: Store<RootState>;

afterEach(async () => {
  cache.clear();
  await cleanup();
});

afterAll(() => {});

beforeEach(() => {
  jest.useFakeTimers();
  store = mockStore({
    subscriptions: [],
    users: {
      self: {
        id: "test",
      },
    },
  });
});

const subscriptionId = "616dc3bdd13ed2822a0a7b84";

const handlers = [
  rest.get(
    API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
    (req, res, ctx) => {
      const { subscriptionId } = req.params;
      return res(
        ctx.json({
          id: subscriptionId,
          dealId: "test",
          ownerId: "test",
        })
      );
    }
  ),
  rest.put(
    API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
    (req, res, ctx) => {
      const { subscriptionId } = req.params;
      return res(
        ctx.json({
          id: subscriptionId,
          dealId: "test",
        })
      );
    }
  ),
  rest.get(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
    const { dealId } = req.params;
    return res(
      ctx.json({
        id: dealId,
        entityId: "test",
      })
    );
  }),
  rest.get(API.FULL_PATH(API.ENTITY_BY_ID(":entityId")), (req, res, ctx) => {
    const { entityId } = req.params;
    return res(
      ctx.json({
        id: entityId,
        entityId: "test",
      })
    );
  }),
  rest.post(API.FULL_PATH(API.PROFILES), (req, res, context) => {
    return res(
      context.json({
        id: "test",
      })
    );
  }),
  rest.post(
    API.FULL_PATH(API.PROFILE_CREATE_KYCAML_APPLICANT("test")),
    (req, res, context) => {
      return res(
        context.json({
          id: "test",
        })
      );
    }
  ),
];

const TestProfileData = {
  ownerId: "test",
  registrationType: "INDIVIDUAL",
};

describe("Profile Hook Tests", () => {
  it("Should redirect Primary Signatories (without Accreditation Check) to /subscriptions/:id/set-amount", async () => {
    const history = createMemoryHistory();
    history.push(
      `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
    );
    server.use(...handlers);
    const { result, waitForValueToChange } = renderHook(
      () =>
        useProfileSubmit({
          type: "subscriber",
          redirectTo: "/dashboard",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await waitForValueToChange(() => result.current.entity);
      await result.current.onSubmit(TestProfileData);
    });
    expect(history.location.pathname).toBe(
      `/subscriptions/${subscriptionId}/set-amount`
    );
  });

  it("Should redirect REGULATION_D_S to subscription directly", async () => {
    const history = createMemoryHistory();
    server.use(...handlers);
    history.push(
      `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
    );
    const { result, waitForValueToChange } = renderHook(
      () =>
        useProfileSubmit({
          type: "subscriber",
          redirectTo: "/dashboard",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await waitForValueToChange(() => result.current.entity);
      await result.current.onSubmit({
        ...TestProfileData,
        regType: "REGULATION_D_S",
      });
    });
    expect(history.location.pathname).toBe(`/subscriptions/${subscriptionId}`);
  });

  it("Should redirect regeneration to set-bank-only", async () => {
    const history = createMemoryHistory();
    server.use(...handlers);
    history.push(
      `/profiles/create/subscriber?subscriptionId=${subscriptionId}&regenerate=true`
    );
    const { result, waitForValueToChange } = renderHook(
      () =>
        useProfileSubmit({
          type: "subscriber",
          redirectTo: "/dashboard",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await waitForValueToChange(() => result.current.entity);
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(
      `/subscriptions/${subscriptionId}/set-bank-only`
    );
  });

  it("Should redirect non-primary signatories to sign-documents with generate", async () => {
    const history = createMemoryHistory();
    server.use(
      rest.get(
        API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
        (req, res, ctx) => {
          const { subscriptionId } = req.params;
          return res(
            ctx.json({
              id: subscriptionId,
              dealId: "test",
              ownerId: "test2",
            })
          );
        }
      ),
      rest.put(
        API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
        (req, res, ctx) => {
          const { subscriptionId } = req.params;
          return res(
            ctx.json({
              id: subscriptionId,
              dealId: "test",
            })
          );
        }
      ),
      rest.get(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
        const { dealId } = req.params;
        return res(
          ctx.json({
            id: dealId,
            entityId: "test",
          })
        );
      }),
      rest.get(
        API.FULL_PATH(API.ENTITY_BY_ID(":entityId")),
        (req, res, ctx) => {
          const { entityId } = req.params;
          return res(
            ctx.json({
              id: entityId,
              entityId: "test",
            })
          );
        }
      ),
      rest.post(API.FULL_PATH(API.PROFILES), (req, res, context) => {
        return res(
          context.json({
            id: "test",
          })
        );
      }),
      rest.post(
        API.FULL_PATH(API.PROFILE_CREATE_KYCAML_APPLICANT("test")),
        (req, res, context) => {
          return res(
            context.json({
              id: "test",
            })
          );
        }
      )
    );
    history.push(
      `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
    );
    const { result, waitForValueToChange } = renderHook(
      () =>
        useProfileSubmit({
          type: "subscriber",
          redirectTo: "/dashboard",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await waitForValueToChange(() => result.current.entity);
      await result.current.onSubmit({
        ...TestProfileData,
        ownerId: "signer",
      });
    });
    expect(history.location.pathname).toBe(
      `/subscriptions/${subscriptionId}/sign-documents`
    );
    expect(history.location.search).toBe(`?generate=true`);
  });

  it("Should redirect Primary Signatories (with Failing Accreditation Check) to /profiles/create/subscriber?subscriptionId=:subscriptionId&selected=:profileId", async () => {
    const history = createMemoryHistory();
    server.use(
      rest.get(
        API.FULL_PATH(API.ENTITY_BY_ID(":entityId")),
        (req, res, ctx) => {
          const { entityId } = req.params;
          return res(
            ctx.json({
              id: entityId,
              regulationType: "REGULATION_D",
              regDExemption: "c",
            })
          );
        }
      )
    );
    history.push(
      `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
    );
    const { result, waitForValueToChange } = renderHook(
      () =>
        useProfileSubmit({
          type: "subscriber",
          redirectTo: "/dashboard",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await waitForValueToChange(() => result.current.entity);
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(`/profiles/create/subscriber`);
    expect(history.location.search).toBe(
      `?subscriptionId=${subscriptionId}&selected=test`
    );
  });

  it("Should redirect Founder Profiles without companyId to /assets/companies", async () => {
    const history = createMemoryHistory();
    history.push(`/profiles/create/founder`);
    const { result } = renderHook(
      () =>
        useProfileSubmit({
          type: "founder",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(`/assets/companies`);
  });

  it("Should redirect Founder Profiles with companyId to /assets/companies/:companyId", async () => {
    const history = createMemoryHistory();
    history.push(`/profiles/create/founder?companyId=${subscriptionId}`);
    const { result } = renderHook(
      () =>
        useProfileSubmit({
          type: "founder",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(
      `/assets/companies/${subscriptionId}`
    );
  });

  it("Should use redirectTo for other profile types (if present)", async () => {
    const history = createMemoryHistory();
    history.push(`/profiles/create/organizer`);
    const { result } = renderHook(
      () =>
        useProfileSubmit({
          type: "organizer",
          redirectTo: "/example/issue",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(`/example/issue`);
  });

  it("Should redirect to /deals/create?profileId without redirectTo defined", async () => {
    const history = createMemoryHistory();
    history.push(`/profiles/create/organizer`);
    const { result } = renderHook(
      () =>
        useProfileSubmit({
          type: "organizer",
        }),
      {
        wrapper: makeHookWrapper(history, store),
      }
    );

    await act(async () => {
      await result.current.onSubmit({
        ...TestProfileData,
      });
    });
    expect(history.location.pathname).toBe(`/deals/create`);
    expect(history.location.search).toBe(`?profileId=test`);
  });
});
