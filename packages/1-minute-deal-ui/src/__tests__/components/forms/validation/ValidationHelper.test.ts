import { getDigitsFromString } from "../../../../forms/validation/ValidationHelper";

test("String turns to dollars", () => {
  expect(getDigitsFromString("123,456")).toBe(123456);
});

test("bad string returns 0", () => {
  // All non-digit characters are removed,
  // so it results in a 0
  expect(getDigitsFromString("nope")).toBe(0);
});

test("123abc456 returns 123456", () => {
  expect(getDigitsFromString("123abc456")).toBe(123456);
});

test("123.456 returns 123456", () => {
  expect(getDigitsFromString("123.456")).toBe(123456);
});

// Runtime checks...
test("a number param returns a number", () => {
  // @ts-ignore
  expect(getDigitsFromString(123)).toBe(123);
  // @ts-ignore
  expect(getDigitsFromString({})).toBe(0);
});
