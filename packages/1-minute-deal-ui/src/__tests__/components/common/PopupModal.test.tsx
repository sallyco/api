import React from "react";
import "@testing-library/jest-dom/extend-expect";
import {
  PrimaryPopupModal,
  ClosedPopupModal,
} from "../../../../stories/components/common/PopupModal.stories";
import { cleanup, render } from "@testing-library/react";
import renderer from "react-test-renderer";

afterEach(() => {
  cleanup();
});

describe("Popup Modal", () => {
  it("Renders an open PopupModal", () => {
    const el = render(<PrimaryPopupModal {...PrimaryPopupModal.args} />);
    expect(el.baseElement.querySelector(".header")).toBeVisible();
  });

  it("Can be closed", () => {
    const el = render(<ClosedPopupModal {...ClosedPopupModal.args} />);
    expect(el.baseElement.querySelector(".header")).toBeNull();
  });

  it("Matches Snapshot", () => {
    const tree = renderer
      .create(<ClosedPopupModal {...ClosedPopupModal.args} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
