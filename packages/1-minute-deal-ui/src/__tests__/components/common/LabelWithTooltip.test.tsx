import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { PrimaryLabelWithTooltip } from "../../../../stories/components/common/LabelWithTooltip.stories";
import { cleanup, render } from "@testing-library/react";
import renderer, { act } from "react-test-renderer";

afterEach(async () => {
  await cleanup();
});

describe("Label with Tooltip", () => {
  it("Shows the label", () => {
    const el = render(
      <PrimaryLabelWithTooltip {...PrimaryLabelWithTooltip.args} />
    );
    expect(el.baseElement.querySelector("label")).toBeVisible();
  });

  it("Does not show the Popup", () => {
    const el = render(
      <PrimaryLabelWithTooltip {...PrimaryLabelWithTooltip.args} />
    );
    expect(el.baseElement.querySelector("div.popup")).toBeNull();
  });

  it("Matches Snapshot", () => {
    let testRendererInstance;
    act(async () => {
      testRendererInstance = renderer.create(
        <PrimaryLabelWithTooltip {...PrimaryLabelWithTooltip.args} />
      );
    }).then((el) => {
      expect(testRendererInstance.toJSON()).toMatchSnapshot();
    });
  });
});
