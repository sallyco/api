import React from "react";
import "@testing-library/jest-dom";
import {
  cleanup,
  render,
  fireEvent,
  screen,
  act,
} from "@testing-library/react";

import { createMemoryHistory } from "history";
import { Router, Switch, Route } from "react-router-dom";
import { Login } from "../../pages/Login";
import { ForgotCredentials } from "../../pages/ForgotCredentials";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import mockAxios from "jest-mock-axios";
const mockStore = configureStore([]);

afterEach(async () => {
  await cleanup();
  await mockAxios.reset();
});

const history = createMemoryHistory();
function renderLoginWithRouting() {
  const store = mockStore({
    users: {
      isLoading: false,
    },
  });
  return render(
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/forgot-credentials" component={ForgotCredentials} />
        </Switch>
      </Router>
    </Provider>
  );
}

describe("rendering and routing test", () => {
  it("should render Log In", () => {
    act(() => {
      const { debug } = renderLoginWithRouting();
      history.push("/");
    });
    expect(screen.getByTestId("login-form")).toBeInTheDocument();
  });

  it("should route to forgot-credentials when Forgot email or Password link is clicked", () => {
    const { getByTestId } = renderLoginWithRouting();
    history.push("/");
    fireEvent.click(getByTestId("forgot-link"));
    expect(getByTestId("forgot-credentials-form")).toBeInTheDocument();
  });
});

// describe("login form tests", () => {
//     describe("email input tests", () => {
//         it("should throw an error when an invalid email is used and display error message", async () => {
//             const { container, queryByText, debug } = renderLoginWithRouting();
//
//             const email = container.querySelector('input[name="email"]') ?? container;
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "badEmail" } });
//                 fireEvent.blur(email);
//             });
//             // debug();
//             expect(email.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText("Please enter a valid email address")).toBeTruthy();
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "badEmail@.co" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText("Please enter a valid email address")).toBeTruthy();
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "badEmail@yahoo" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText("Please enter a valid email address")).toBeTruthy();
//         });
//
//         it("should not throw an error or display error message when valid email is used", async () => {
//             const { container, queryByText } = renderLoginWithRouting();
//             const email = container.querySelector('input[name="email"]') ?? container;
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "test@test.co" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//             expect(queryByText("Please enter a valid email address")).toBeNull();
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "12test@test.co" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//             expect(queryByText("Please enter a valid email address")).toBeNull();
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "test@test.com" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//             expect(queryByText("Please enter a valid email address")).toBeNull();
//         });
//     });
//
//     describe("password input tests", () => {
//         it("should throw an error when an invalid password is used and display error message", async () => {
//             const { container, queryByText } = renderLoginWithRouting();
//
//             const password = container.querySelector('input[name="password"]') ?? container;
//             expect(password.getAttribute("aria-invalid")).toBe("false");
//             await waitFor(() => {
//                 fireEvent.change(password, { target: { value: "badpassword" } });
//                 fireEvent.blur(password);
//             });
//             expect(password.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText(/Please enter a password/i)).toBeTruthy();
//
//             await waitFor(() => {
//                 fireEvent.change(password, { target: { value: "badPassword" } });
//                 fireEvent.blur(password);
//             });
//             expect(password.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText(/Please enter a password/i)).toBeTruthy();
//
//             await waitFor(() => {
//                 fireEvent.change(password, { target: { value: "badP@ssword" } });
//                 fireEvent.blur(password);
//             });
//             expect(password.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText(/Please enter a password/i)).toBeTruthy();
//         });
//
//         it("should not throw an error or display error message when valid password is used", async () => {
//             const { container, queryByText } = renderLoginWithRouting();
//             const email = container.querySelector('input[name="password"]') ?? container;
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//
//             await waitFor(() => {
//                 fireEvent.change(email, { target: { value: "Assure!234" } });
//                 fireEvent.blur(email);
//             });
//             expect(email.getAttribute("aria-invalid")).toBe("false");
//             expect(queryByText(/Please enter a password/i)).toBeNull();
//         });
//     });
//
//     describe("form submit tests", () => {
//         it("should throw errors and display messages when no data is entered", async done => {
//             const { container, queryByText, getByText } = renderLoginWithRouting();
//             await waitFor(() => fireEvent.click(getByText("LOG IN")));
//             const email = container.querySelector('input[name="email') ?? container;
//             const password = container.querySelector("input[name=password]") ?? container;
//             expect(email.getAttribute("aria-invalid")).toBe("true");
//             expect(password.getAttribute("aria-invalid")).toBe("true");
//             expect(queryByText("Please enter an email address")).toBeTruthy();
//             expect(queryByText(/Please enter a password/i)).toBeTruthy();
//             done();
//         });
//     });
// });
