import React from "react";
import "@testing-library/jest-dom";
import { cleanup, render, screen, act } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router, Switch, Route } from "react-router-dom";
import { Login } from "../../pages/Login";
import { VerifyEmail } from "../../pages/VerifyEmailPage";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import mockAxios from "jest-mock-axios";
const mockStore = configureStore([]);

afterEach(async () => {
  await cleanup();
  await mockAxios.reset();
});

const history = createMemoryHistory();
function renderVerifyEmailWithRouting() {
  const store = mockStore({
    users: {
      isLoading: false,
    },
  });
  return render(
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/verify-email/:email" component={VerifyEmail} />
        </Switch>
      </Router>
    </Provider>
  );
}

describe("Verify Email test", () => {
  it("should render the wrapper", async () => {
    act(() => {
      renderVerifyEmailWithRouting();

      history.push("/verify-email/test@email.com");
    });
    expect(screen.getByTestId("verify-email")).toBeInTheDocument();
  });

  it("should render the loader while waiting for response", async () => {
    act(() => {
      renderVerifyEmailWithRouting();
      history.push("/verify-email/test@email.com");
    });
    expect(screen.getByTestId("verify-email-loader")).toBeInTheDocument();
  });
});
