import React, { useState } from "react";
import { Switch } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Sidebar } from "semantic-ui-react";
import { AdminBanner } from "../components/common/AdminBanner";
import Header from "../components/Header";
import AdminNavBar from "../components/common/AdminNavBar";
import AdminNavSideBar from "../components/common/AdminNavSideBar";
import { ProtectedRoute } from "./helpers";
import { AdminDashboard } from "../pages/admin/AdminDashboard";
import { AdminUsers } from "../pages/admin/AdminUsers";
import { AdminEmails } from "../pages/admin/AdminEmails";
import { AdminConfig } from "../pages/admin/AdminConfig";
import { AdminSignings } from "../pages/admin/AdminSignings";

export const AdminRoutes = (path: string) => {
  const [sideBarVisible, setSideBarVisible] = useState(false);

  return (
    <>
      <Helmet bodyAttributes={{ class: "grey-background" }} />
      <Sidebar.Pushable>
        <AdminNavSideBar
          isVisible={sideBarVisible}
          setVisible={setSideBarVisible}
        />
        <Sidebar.Pusher dimmed={sideBarVisible}>
          <Header showSideBar={setSideBarVisible} />
          <AdminBanner />
          <AdminNavBar />
          <Switch>
            <ProtectedRoute
              path={`${path}/`}
              exact
              comp={AdminDashboard}
              roles={["admin"]}
            />
            <ProtectedRoute
              path={`${path}/users`}
              exact
              comp={AdminUsers}
              roles={["admin"]}
            />
            <ProtectedRoute
              path={`${path}/emails`}
              exact
              comp={AdminEmails}
              roles={["admin"]}
            />
            <ProtectedRoute
              path={`${path}/signings`}
              exact
              comp={AdminSignings}
              roles={["admin"]}
            />
            <ProtectedRoute
              path={`${path}/configurations`}
              exact
              comp={AdminConfig}
              roles={["admin"]}
            />
          </Switch>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </>
  );
};
