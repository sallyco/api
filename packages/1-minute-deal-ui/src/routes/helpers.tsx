import React from "react";
import { Route, Redirect } from "react-router-dom";
import Cookies from "js-cookie";
import Log from "../tools/Log";
import PropTypes from "prop-types";
import { FeatureFlagsContext } from "../components/featureflags/FeatureFlags";
import useSWR from "swr";
import { getFeatureFlags } from "../api/featureFlags";
import { Unverified } from "../pages/UnverifiedPage";
import { AuthContext } from "../contexts";

const getSession = () => {
  const jwt = Cookies.get("__session");
  let session;
  try {
    if (jwt) {
      const base64Url = jwt.split(".")[1];
      const base64 = base64Url.replace("-", "+").replace("_", "/");
      session = JSON.parse(window.atob(base64));
    }
  } catch (error) {
    Log.debug("getSession", error);
  }
  return session;
};

export const UnprotectedRoute = ({ comp: Component, ...rest }) => {
  const { data: featureFlags } = useSWR("/feature-flags", getFeatureFlags, {
    focusThrottleInterval: 60000,
  });
  return (
    <Route
      {...rest}
      render={(props: any) =>
        getSession() && getSession().exp > Date.now() / 1000 ? (
          <Redirect
            to={{ pathname: "/dashboard", state: { from: props.location } }}
          />
        ) : (
          <FeatureFlagsContext.Provider value={featureFlags}>
            <Component {...props} />
          </FeatureFlagsContext.Provider>
        )
      }
    />
  );
};
UnprotectedRoute.propTypes = {
  comp: PropTypes.func,
  location: PropTypes.object,
};

export const ProtectedRoute = ({ comp: Component, roles, ...rest }) => {
  const session = getSession();
  const { data: featureFlags } = useSWR("/feature-flags", getFeatureFlags, {
    focusThrottleInterval: 60000,
  });
  Log.debug("session", session);
  if (session && session.exp > Date.now() / 1000 && !session.email_verified) {
    return <Unverified email={session.email} />;
  }
  if (session && session.exp > Date.now() / 1000) {
    if (
      roles.includes("*") ||
      roles.some((r) => session.resource_access.account.roles.includes(r))
    ) {
      Log.debug("roles", "here");
      return (
        <AuthContext.Provider value={session}>
          <FeatureFlagsContext.Provider value={featureFlags}>
            <Route {...rest} render={(props) => <Component {...props} />} />
          </FeatureFlagsContext.Provider>
        </AuthContext.Provider>
      );
    } else {
      let home = "/dashboard/subscriptions";
      if (session.resource_access.account.roles.includes("founder"))
        home = "/dashboard/assets/companies";
      else if (session.resource_access.account.roles.includes("organizer"))
        home = "/dashboard/deals";
      else if (
        session.resource_access.account.roles.includes("signer") &&
        !session.resource_access.account.roles.includes("investor")
      )
        home = "/dashboard/signing";

      return (
        <Route
          {...rest}
          render={(props) => (
            <Redirect
              to={{ pathname: home, state: { from: props.location } }}
            />
          )}
        />
      );
    }
  } else {
    return (
      <Route
        {...rest}
        render={(props) => {
          document.cookie = `__postlogin-url=${encodeURIComponent(
            props.location.pathname
          )};`;
          return (
            <Redirect to={{ pathname: `/`, state: { from: props.location } }} />
          );
        }}
      />
    );
  }
};
ProtectedRoute.propTypes = {
  component: PropTypes.func,
  roles: PropTypes.array,
  location: PropTypes.object,
};
