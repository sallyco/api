import { configureStore, getDefaultMiddleware, Action } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";
import { createHashHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import * as Sentry from "@sentry/react";
import createSentryMiddleware from "redux-sentry-middleware";
import actionCounter from "./tools/actionCounter";

import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

import rootReducer, { RootState } from "./rootReducer";

export const history = createHashHistory({
  hashType: "slash",
  getUserConfirmation: (message, callback) => callback(window.confirm(message)),
});

declare var module: any;

Sentry.init({
  dsn: process.env.SENTRY_DSN ?? process.env.NEXT_PUBLIC_SENTRY_DSN,
  environment: process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT,
  release:
    "omd@" + process.env.BUILD_NAME ?? process.env.NEXT_PUBLIC_BUILD_NAME,
});

const persistConfig = {
  key: "root",
  version: 1,
  storage,
  whitelist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const actionCounterMiddleware = (storeAPI) => (next) => (action) => {
  actionCounter.increaseCount();
  return next(action);
};

const middleware = [
  routerMiddleware(history),
  createSentryMiddleware(Sentry),
  actionCounterMiddleware,
  ...getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
];

if (process.env.NODE_ENV === `development`) {
  const { createLogger } = require(`redux-logger`);

  middleware.push(createLogger({}));
}

const store = configureStore({
  reducer: persistedReducer,
  middleware,
  devTools: process.env.NODE_ENV === "development",
});

if (process.env.NODE_ENV === "development") {
  if (module.hot) {
    module.hot.accept("./rootReducer", () => {
      const newRootReducer = require("./rootReducer").default;
      store.replaceReducer(newRootReducer);
    });
  }
}

export type AppDispatch = typeof store.dispatch;

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;

export const persistor = persistStore(store);

export default store;
