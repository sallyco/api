import { combineReducers } from "redux";
import { createBrowserHistory } from "history";
import { connectRouter } from "connected-react-router";

// Data reducers
import applicationUpdateStatusReducer from "./slices/applicationUpdateSlice";
import accountsReducer from "./slices/accountsSlice";
import assetsReducer from "./slices/assetsSlice";
import closesReducer from "./slices/closesSlice";
import companiesReducer from "./slices/companiesSlice";
import dealsReducer from "./slices/dealsSlice";
import entitiesReducer from "./slices/entitySlice";
import emailTemplatesReducer from "./slices/emailTemplatesSlice";
import filesReducer from "./slices/filesSlice";
import invitesReducer from "./slices/invitesSlice";
import profilesReducer from "./slices/profilesSlice";
import subscriptionsReducer from "./slices/subscriptionsSlice";
import usersReducer from "./slices/usersSlice";
import tenantReducer from "./slices/tenantSlice";
import notificationReducer from "./slices/notificationsSlice";

// Service Data reducers
import locationServiceReducer from "./slices/locationServiceSlice";

// UI reducers
import dealsListDisplayReducer from "./slices/dealsListDisplaySlice";
import dealsListOperationReducer from "./slices/dealsListOperationSlice";
import subscriptionsListOperationReducer from "./slices/subscriptionsListOperationSlice";

const history = createBrowserHistory();

const appReducer = combineReducers({
  applicationUpdateStatus: applicationUpdateStatusReducer,
  accounts: accountsReducer,
  assets: assetsReducer,
  closes: closesReducer,
  companies: companiesReducer,
  deals: dealsReducer,
  entities: entitiesReducer,
  emailTemplates: emailTemplatesReducer,
  files: filesReducer,
  invites: invitesReducer,
  notifications: notificationReducer,
  router: connectRouter(history),
  profiles: profilesReducer,
  subscriptions: subscriptionsReducer,
  users: usersReducer,
  tenant: tenantReducer,

  locationService: locationServiceReducer,

  dealsListDisplay: dealsListDisplayReducer,
  dealsListOpeartions: dealsListOperationReducer,
  subscriptionsListOpeartions: subscriptionsListOperationReducer,
});

// Action
export const UpdateReset = () => {
  return {
    type: "Reset",
  };
};

const rootReducer = (state, action) => {
  if (action.type === "Reset") {
    state = { tenant: state.tenant };
  }
  return appReducer(state, action);
};

export type RootState = ReturnType<typeof appReducer>;

export default rootReducer;
