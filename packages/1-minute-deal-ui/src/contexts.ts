import React, { useContext } from "react";
import { Tenant } from "./api/tenantApi";

export const AuthContext = React.createContext({
  sub: "",
  given_name: "",
  family_name: "",
  email: "",
  preferred_username: "",
  attributes: {
    tenantId: [],
    phone: [],
  },
  resource_access: {
    account: { roles: [] },
  },
});

export const useAccountRole = (role) => {
  const userProfile = useContext(AuthContext);
  return userProfile.resource_access.account.roles.includes(role);
};

export const TenantContext = React.createContext<Tenant>({
  id: "",
  inverted: false,
  name: "",
  url: "",
  assets: {
    colors: {
      primaryColor: null,
      secondaryColor: null,
    },
    images: {
      logo: null,
      logoInverted: null,
    },
  },
  settings: {
    emails: {
      fromAddress: "",
    },
  },
  bankingLimits: {},
  realm: {
    verifyEmail: true,
  },
  billingContact: "",
});

export const ThemeContext = React.createContext({});
