import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { apiCallPing } from "../api/pingApi";
import { Label } from "semantic-ui-react";

const Section = ({
  label,
  value,
  errorState = false,
  errorValue = "error",
  preFormatted = false,
}) => {
  return (
    <Label className={`section ${errorState ? "error" : ""}`}>
      <span className={"label"}>{label}</span>
      {!preFormatted && (
        <span className={"value"}>{errorState ? errorValue : value}</span>
      )}
      {preFormatted && (
        <pre className={"value"}>{errorState ? errorValue : value}</pre>
      )}
    </Label>
  );
};
Section.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  errorState: PropTypes.any,
  errorValue: PropTypes.string,
  preFormatted: PropTypes.bool,
};

const BuildInfo = () => {
  const [environment] = useState(
    process.env.ENVIRONMENT ??
      process.env.NEXT_PUBLIC_ENVIRONMENT ??
      "development"
  );
  const [buildName] = useState(
    process.env.BUILD_NAME ??
      process.env.NEXT_PUBLIC_BUILD_NAME ??
      "development"
  );
  const [buildVersion] = useState(
    process.env.BUILD_VERSION ??
      process.env.NEXT_PUBLIC_BUILD_VERSION ??
      "x.x.x"
  );
  const [buildNote] = useState(
    process.env.BUILD_NOTE ?? process.env.NEXT_PUBLIC_BUILD_NOTE ?? ""
  );
  const [buildDate] = useState(
    process.env.BUILD_DATE ??
      process.env.NEXT_PUBLIC_BUILD_DATE ??
      new Date().toISOString()
  );
  const [apiData, setApiData] = useState({
    camundaVersion: "",
    buildName: "",
    buildVersion: "",
    buildDate: "",
    buildNote: "",
  });
  const [expanded, setExpanded] = useState(false);
  const [hidden, setHidden] = useState(false);

  useEffect(() => {
    (async () => await getPingData())();
    const interval = setInterval(() => {
      (async () => await getPingData())();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  async function getPingData() {
    try {
      const response = await apiCallPing();
      setApiData(response?.data);
    } catch (e) {
      // pass
    }
  }

  function formatRelativeTime(time) {
    return (
      moment.utc(time).local().format("lll") +
      " (" +
      moment.utc(time).local().fromNow() +
      ")"
    );
  }

  function undefinedOrBlank(value) {
    return value === undefined || value === "";
  }

  return (
    <>
      {(process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) !==
        "production" &&
        (process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) !==
          "sandbox" && (
          <div
            className={`buildInfoBar ${expanded ? "expanded" : ""} ${
              hidden ? "hidden" : ""
            }`}
          >
            <div className={"actions"}>
              <div className={"close action"} onClick={() => setHidden(true)}>
                <i className="close icon" />
              </div>
              <div
                className={"expand action"}
                onClick={() => setExpanded(!expanded)}
              >
                <i
                  className={`angle double ${expanded ? "down" : "up"} icon`}
                />
              </div>
            </div>
            <div className={"sections"}>
              <div className={"group"}>
                <Section label={"Environment"} value={environment} />
                <Section label={"Build Name"} value={buildName} />
                <Section label={"Build Version"} value={buildVersion} />
                <Section
                  label={"Build Date"}
                  value={formatRelativeTime(buildDate)}
                />
              </div>
              <div className={"group"}>
                <Section
                  label={"Camunda Version"}
                  value={apiData.camundaVersion}
                  errorState={undefinedOrBlank(apiData.camundaVersion)}
                  errorValue={"Camunda Not Found"}
                />
                <Section
                  label={"API Build Name"}
                  value={apiData.buildName}
                  errorState={undefinedOrBlank(apiData.buildName)}
                  errorValue={"API Not Found"}
                />
                <Section
                  label={"API Build Version"}
                  value={apiData.buildVersion}
                  errorState={undefinedOrBlank(apiData.buildVersion)}
                  errorValue={"API Not Found"}
                />
                <Section
                  label={"API Build Date"}
                  value={formatRelativeTime(apiData.buildDate)}
                  errorState={undefinedOrBlank(apiData.buildDate)}
                  errorValue={"API Not Found"}
                />
              </div>
              <div className={"group separate"}>
                <Section
                  label={"Build Note"}
                  value={buildNote}
                  preFormatted={true}
                />
                <Section
                  label={"API Build Note"}
                  value={apiData.buildNote}
                  preFormatted={true}
                  errorState={false}
                  errorValue={"No Build Notes"}
                />
              </div>
            </div>
          </div>
        )}
    </>
  );
};

BuildInfo.propTypes = {};

export default BuildInfo;
