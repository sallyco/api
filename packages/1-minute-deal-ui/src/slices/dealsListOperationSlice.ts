import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface DealsListOperation {
  opType: "search" | "filter" | "sort" | "";
}

interface DealsListOperationPayload {
  opType: "search" | "filter" | "sort" | "";
}

type DealsListOperationState = {
  page: number;
  opValue: string;
  opFilterValue?: { field?: string; value?: string }[];
} & DealsListOperation;

let initialState: DealsListOperationState = {
  page: 1,
  opValue: "",
  opFilterValue: [],
  opType: "",
};

const dealsListOperationSlice = createSlice({
  name: "dealsListOperation",
  initialState,
  reducers: {
    setDealsListOpPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setDealsListOpType(
      state,
      action: PayloadAction<DealsListOperationPayload>
    ) {
      const { opType } = action.payload;
      (state.opType = opType), (state.opValue = "");
    },
    setDealsListOpValue(
      state,
      action: PayloadAction<{ field?: string; value?: string }[] | string>
    ) {
      if (typeof action.payload === "string") {
        state.opValue = action.payload;
      } else {
        state.opFilterValue = action.payload;
      }
    },
    setDealsListOpReset(state) {
      (state.opType = ""),
        (state.opValue = ""),
        (state.page = 1),
        (state.opFilterValue = []);
    },
  },
});

export const {
  setDealsListOpPage,
  setDealsListOpType,
  setDealsListOpValue,
  setDealsListOpReset,
} = dealsListOperationSlice.actions;

export default dealsListOperationSlice.reducer;
