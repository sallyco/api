import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  User,
  getUsers,
  getSelf,
  getUserById,
  createUser,
  updateUser,
} from "../api/usersApi";

import { AppThunk } from "../store";

interface UsersState {
  totalCount: number;
  page: number;
  perPage: number;
  usersById: Record<string, User>;
  isInitialized: boolean;
  self: User | null;
  currentPageUsers: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: UsersState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageUsers: [],
  usersById: {},
  self: null,
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  isInitialized: false,
  error: null,
};

function startLoading(state: UsersState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: UsersState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: UsersState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const users = createSlice({
  name: "users",
  initialState,
  reducers: {
    getUserStart: startLoading,
    getUsersListStart: startLoading,
    createUserStart: startSubmitting,
    updateUserStart: startSubmitting,
    getUserSuccess: (state, { payload }: PayloadAction<User>) => {
      const { id } = payload;

      state.usersById[id] = payload;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;
    },
    getSelfSuccess: (state, { payload }: PayloadAction<User>) => {
      state.self = payload;
      state.isLoading = false;
      state.error = null;
    },
    getUsersListSuccess: (state, { payload }: PayloadAction<User[]>) => {
      const { ...props } = payload;
      //state.totalCount = props.totalCount;
      //state.page = props.page;
      //state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      payload.map((user) => {
        state.usersById[user.id] = user;
      });
      state.currentPageUsers = payload.map((user) => user.id);
    },
    getUsersListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;
    },
    updateUserSuccess: (state, { payload }: PayloadAction<any>) => {
      state.self = payload;
      state.isLoading = false;
      state.isSubmitting = false;
      state.error = null;
    },
    createUserSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.usersById[id] = payload;
      state.isSubmitting = false;
      state.isInitialized = true;
      state.error = null;
    },
    getUserFailure: loadingFailed,
    getUsersListFailure: loadingFailed,
    createUserFailure: loadingFailed,
    updateUserFailure: loadingFailed,
  },
});

export const {
  getUserStart,
  getUsersListStart,
  getUserSuccess,

  getSelfSuccess,
  getUsersListSuccess,
  getUsersListNoDataSuccess,
  getUserFailure,
  getUsersListFailure,

  createUserStart,
  createUserSuccess,
  createUserFailure,

  updateUserStart,
  updateUserSuccess,
  updateUserFailure,
} = users.actions;

export default users.reducer;

export const fetchUsersList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getUsersListStart());
      const usersList = await getUsers(page, perPage);
      if (usersList && usersList.length) {
        dispatch(getUsersListSuccess(usersList));
      } else {
        dispatch(getUsersListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getUsersListFailure(err.toString()));
    }
  };

export const fetchUserById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getUserStart());
      const user = await getUserById(id);
      dispatch(getUserSuccess(user));
    } catch (err) {
      dispatch(getUserFailure(err.toString()));
    }
  };

export const fetchSelf = (): AppThunk => async (dispatch) => {
  try {
    dispatch(getUserStart());
    const user = await getSelf();
    dispatch(getSelfSuccess(user));
    return user;
  } catch (err) {
    dispatch(getUserFailure(err.toString()));
    return null;
  }
};

export const insertUser =
  (user: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createUserStart());
      const response = await createUser(user);
      dispatch(createUserSuccess(response));
      return response;
    } catch (err) {
      dispatch(createUserFailure(err.toString()));
      return null;
    }
  };

export const updateUserById =
  (user: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateUserStart());
      const response = await updateUser(user);
      dispatch(updateUserSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateUserFailure(err.toString()));
      return null;
    }
  };
