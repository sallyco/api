import React from "react";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { AppThunk } from "../store";

import { toast } from "react-toastify";
import UpdateNotice from "../components/applicationStatus/UpdateNotice";

interface ApplicationUpdateStatueState {
  updateAvailable: boolean;
}

const initialState = {
  updateAvailable: false,
};

const applicationUpdateStatus = createSlice({
  name: "applicationUpdateStatus",
  initialState,
  reducers: {
    setApplicationUpdateStatus: (
      state,
      { payload }: PayloadAction<boolean>
    ) => {
      if (payload && !state.updateAvailable) {
        // TODO this is not the right place for this reducers must be side effect free
        // Move this into either index.tsx or app.tsx and trigger it viea useEffect
        // this file should be a .ts not a .tsx
        toast(<UpdateNotice />, {
          position: "top-center",
          autoClose: false,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      state.updateAvailable = payload;
    },
  },
});

export const { setApplicationUpdateStatus } = applicationUpdateStatus.actions;

export default applicationUpdateStatus.reducer;
