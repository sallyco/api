import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Deal,
  DealsList,
  getDeals,
  getDealById,
  createDeal,
  updateDeal,
  searchByDeal,
} from "../api/dealsApi";

import { AppThunk } from "../store";
import Log from "../tools/Log";
import _ from "lodash";
import { updateEntityFundFiles } from "./filesSlice";

interface SearchState {
  field: string;
  value: string;
}

interface DealsState {
  totalCount: number;
  page: number;
  perPage: number;
  dealsById: Record<string, Deal>;
  isInitialized: boolean;
  currentPageDeals: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: any;
  } | null;
  currentDealContext: string;
}

const initialState: DealsState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageDeals: [],
  dealsById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  isInitialized: false,
  error: null,
  currentDealContext: null,
};

function startLoading(state: DealsState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: DealsState) {
  state.isSubmitting = true;
  state.error = null;
}

function stopSubmitting(state: DealsState) {
  state.isSubmitting = false;
  state.error = null;
}

function loadingFailed(state: DealsState, action: PayloadAction<any>) {
  state.isLoading = false;
  state.isSubmitting = false;
  const data = action.payload?.data?.error ?? "An error occurred";
  state.error = { timestamp: new Date().getTime(), data: data };
}

const deals = createSlice({
  name: "deals",
  initialState,
  reducers: {
    getDealStart: startLoading,
    getDealsListStart: startLoading,
    createDealStart: startSubmitting,
    updateDealStart: startSubmitting,
    searchDealStart: startSubmitting,
    getDealSuccess: (state, { payload }: PayloadAction<Deal>) => {
      const { id } = payload;

      state.dealsById[id] = payload;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;
    },
    getDealsListSuccess: (state, { payload }: PayloadAction<DealsList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      props.data.forEach((deal) => {
        state.dealsById[deal.id] = deal;
      });
      state.currentPageDeals = props.data.map((deal) => deal.id);
    },
    createDealSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.dealsById[id] = payload;
      state.isSubmitting = false;
      state.isInitialized = true;
      state.error = null;
    },
    updateDealSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.dealsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getDealsListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;
    },
    searchDealSuccess: (state, { payload }: PayloadAction<DealsList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      props.data.forEach((deal) => {
        state.dealsById[deal.id] = deal;
      });
      state.currentPageDeals = props.data.map((deal) => deal.id);
    },
    setCurrentDealContext: (state, { payload }: PayloadAction<string>) => {
      state.currentDealContext = payload;
    },

    getDealFailure: loadingFailed,
    getDealsListFailure: loadingFailed,
    createDealFailure: loadingFailed,
    updateDealFailure: loadingFailed,
    searchDealFailure: loadingFailed,
  },
});

export const {
  getDealStart,
  getDealsListStart,
  getDealSuccess,
  getDealsListSuccess,
  getDealsListNoDataSuccess,
  getDealFailure,
  getDealsListFailure,

  createDealStart,
  createDealSuccess,
  createDealFailure,

  updateDealStart,
  updateDealSuccess,
  updateDealFailure,
  searchDealStart,
  searchDealSuccess,
  searchDealFailure,

  setCurrentDealContext,
} = deals.actions;

export default deals.reducer;

export const fetchDealsList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getDealsListStart());
      const dealsList = await getDeals(page, perPage);
      if (dealsList && dealsList.data && dealsList.data.length) {
        dispatch(getDealsListSuccess(dealsList));
      } else {
        dispatch(getDealsListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getDealsListFailure(_.pick(err.response, ["data"])));
    }
  };

export const fetchDealById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getDealStart());
      const deal = await getDealById(id);
      dispatch(getDealSuccess(deal));
      return deal;
    } catch (err) {
      dispatch(getDealFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const insertDeal =
  (deal: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createDealStart());
      const response = await createDeal(deal);
      dispatch(createDealSuccess(response));
      return response;
    } catch (err) {
      dispatch(createDealFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const updateDealById =
  (deal: any): AppThunk =>
  async (dispatch) => {
    try {
      // Check if current deal name changed
      const previousDeal = await getDealById(deal?.id);
      const nameChanged = deal.name && deal.name !== previousDeal.name;

      dispatch(updateDealStart());
      const response = await updateDeal(deal);
      dispatch(updateDealSuccess(response));

      // Handle renamed deals
      if (nameChanged) {
        dispatch(updateEntityFundFiles(response.entityId, deal.name));
      }

      return response;
    } catch (err) {
      dispatch(updateDealFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const searchDeal =
  (SearchState: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(searchDealStart());
      const response = await searchByDeal(SearchState);
      dispatch(searchDealSuccess(response));
      return response;
    } catch (err) {
      dispatch(searchDealFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };
