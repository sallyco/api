import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  File,
  FilesList,
  getFiles,
  getFileById,
  createFile,
  uploadFile,
  updateFile as updateFileAPI,
  uploadDocCompany,
  DeleteDocCompany,
  updateDocCompany,
  deleteFile,
} from "../api/filesApi";
import { getEntityById } from "../api/entitiesApi";

import { AppThunk } from "../store";

interface FilesState {
  totalCount: number;
  page: number;
  perPage: number;
  filesById: Record<string, File>;
  currentPageFiles: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: FilesState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageFiles: [],
  filesById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: FilesState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: FilesState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: FilesState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const files = createSlice({
  name: "files",
  initialState,
  reducers: {
    getFileStart: startLoading,
    getFilesListStart: startLoading,
    updateEntityFundFilesStart: startLoading,
    createFileStart: startSubmitting,
    uploadFileStart: startSubmitting,
    updateFilesStart: startSubmitting,
    getFileSuccess: (state, { payload }: PayloadAction<File>) => {
      const { id } = payload;

      state.filesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    uploadFileSuccess: (state, { payload }: PayloadAction<any>) => {
      //const { id } = payload;

      //state.filesById[id] = payload;
      state.isSubmitting = false;
      state.isLoading = false;
      state.error = null;
    },
    updateEntityFundFilesSuccess: (state) => {
      state.isSubmitting = false;
      state.isLoading = false;
      state.error = null;
    },
    updateFilesSuccess: (state, { payload }: PayloadAction<File[]>) => {
      payload.forEach((file) => {
        state.filesById[file.id] = file;
      });
    },
    getFilesListSuccess: (state, { payload }: PayloadAction<FilesList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((file) => {
        state.filesById[file.id] = file;
      });
      state.currentPageFiles = props.data.map((file) => file.id);
    },
    getFilesListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    createFileSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.filesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getFileFailure: loadingFailed,
    getFilesListFailure: loadingFailed,
    createFileFailure: loadingFailed,
    uploadFileFailure: loadingFailed,
    updateFilesFailure: loadingFailed,
    updateEntityFundFilesFailure: loadingFailed,
    deleteFilesStart: startSubmitting,
    deleteFilesSuccess: (state, { payload }: PayloadAction<string[]>) => {
      state.isSubmitting = false;
      state.isLoading = false;
      state.error = null;
      payload.forEach((fileId) => {
        delete state.filesById[fileId];
      });
    },
    deleteFilesFailure: loadingFailed,
  },
});

export const {
  getFileStart,
  getFilesListStart,
  getFileSuccess,
  getFilesListSuccess,
  getFilesListNoDataSuccess,
  getFileFailure,
  getFilesListFailure,

  createFileStart,
  createFileSuccess,
  createFileFailure,

  uploadFileStart,
  uploadFileSuccess,
  uploadFileFailure,

  updateEntityFundFilesStart,
  updateEntityFundFilesSuccess,
  updateEntityFundFilesFailure,

  updateFilesStart,
  updateFilesSuccess,
  updateFilesFailure,

  deleteFilesStart,
  deleteFilesSuccess,
  deleteFilesFailure,
} = files.actions;

export default files.reducer;

export const fetchFilesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getFilesListStart());
      const filesList = await getFiles(page, perPage);
      if (filesList && filesList.data && filesList.data.length) {
        dispatch(getFilesListSuccess(filesList));
      } else {
        dispatch(getFilesListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getFilesListFailure(err.toString()));
    }
  };

export const fetchFileById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getFileStart());
      const file = await getFileById(id);
      dispatch(getFileSuccess(file));
    } catch (err) {
      dispatch(getFileFailure(err.toString()));
    }
  };

export const insertFile =
  (file: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createFileStart());
      const response = await createFile(file);
      dispatch(createFileSuccess(response));
      return response;
    } catch (err) {
      dispatch(createFileFailure(err.toString()));
      return null;
    }
  };

export const uploadFileToServer =
  (file: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(uploadFileStart());
      const response = await uploadFile(file);
      dispatch(uploadFileSuccess(response));
      return response;
    } catch (err) {
      dispatch(uploadFileFailure(err.toString()));
      return null;
    }
  };

export const uploadCompanyFileToServer =
  (file: any, companyId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(uploadFileStart());
      const response = await uploadDocCompany(file, companyId);
      dispatch(uploadFileSuccess(response));
      return response;
    } catch (err) {
      dispatch(uploadFileFailure(err.toString()));
      return null;
    }
  };

export const DeleteCompanyFileToServer =
  (fileId: string, companyId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(uploadFileStart());
      const response = await DeleteDocCompany(fileId, companyId);
      dispatch(uploadFileSuccess(response));
      return response;
    } catch (err) {
      dispatch(uploadFileFailure(err.toString()));
      return null;
    }
  };

export const updateCompanyFileToServer =
  (file: any, fileId: string, companyId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(uploadFileStart());
      const response = await updateDocCompany(file, fileId, companyId);
      dispatch(uploadFileSuccess(response));
      return response;
    } catch (err) {
      dispatch(uploadFileFailure(err.toString()));
      return null;
    }
  };

export const updateEntityFundFiles =
  (entityId: string, dealName) => async (dispatch) => {
    try {
      dispatch(updateEntityFundFilesStart());

      const { entityDocuments } = await getEntityById(entityId);

      if (
        !entityDocuments.operatingAgreement ||
        !entityDocuments.subscriptionAgreement ||
        !entityDocuments.subscriptionAgreement
      ) {
        throw new Error("Some files don't exist so renaming was aborted");
      }

      const filePromises = [
        getFileById(entityDocuments.operatingAgreement),
        getFileById(entityDocuments.privatePlacementMemorandum),
        getFileById(entityDocuments.subscriptionAgreement),
      ];

      const files = await Promise.all(filePromises);
      files[0].name = `${dealName} - Operating Agreement.pdf`;
      files[1].name = `${dealName} - Private Placement Memorandum.pdf`;
      files[2].name = `${dealName} - Subscription Agreement.pdf`;

      dispatch(updateFiles(files));
      dispatch(updateEntityFundFilesSuccess());
      return true;
    } catch (err) {
      dispatch(updateEntityFundFilesFailure(err.toString()));
      return null;
    }
  };

export const updateFiles =
  (files: File[]): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateFilesStart());

      const promises = files.map((file) => {
        return updateFileAPI(file);
      });
      const newFiles = await Promise.all(promises);
      dispatch(updateFilesSuccess(newFiles));

      return newFiles;
    } catch (err) {
      dispatch(updateFilesFailure(err.toString()));
      return null;
    }
  };

export const deleteFiles =
  (fileIds: string[]): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteFilesStart());

      const promises = fileIds.map((id) => {
        return deleteFile(id);
      });
      await Promise.all(promises);
      dispatch(deleteFilesSuccess(fileIds));

      return fileIds;
    } catch (err) {
      dispatch(deleteFilesFailure(err.toString()));
      return null;
    }
  };
