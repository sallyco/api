import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Company,
  CompaniesList,
  getCompanies,
  getCompanyById,
  createCompany,
  updateCompany,
} from "../api/companyApi";

import { AppThunk } from "../store";

interface CompanyState {
  totalCount: number;
  page: number;
  perPage: number;
  companiesById: Record<string, Company>;
  // companiesByDealId: Record<string, Company>;
  currentPageCompanies: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: CompanyState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageCompanies: [],
  companiesById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: CompanyState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: CompanyState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: CompanyState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const companies = createSlice({
  name: "companies",
  initialState,
  reducers: {
    getCompanyStart: startLoading,
    getCompaniesListStart: startLoading,
    createCompanyStart: startSubmitting,
    updateCompanyStart: startSubmitting,
    getCompanySuccess: (state, { payload }: PayloadAction<Company>) => {
      const { id } = payload;

      state.companiesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getCompaniesListSuccess: (
      state,
      { payload }: PayloadAction<CompaniesList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((company) => {
        state.companiesById[company.id] = company;
      });
      state.currentPageCompanies = props.data.map((company) => company.id);
    },
    createCompanySuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.companiesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateCompanySuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.companiesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },

    getCompaniesListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    getCompanyFailure: loadingFailed,
    getCompaniesListFailure: loadingFailed,
    createCompanyFailure: loadingFailed,
    updateCompanyFailure: loadingFailed,
  },
});

export const {
  getCompanyStart,
  getCompaniesListStart,
  getCompanySuccess,
  getCompaniesListSuccess,
  getCompaniesListNoDataSuccess,
  getCompanyFailure,
  getCompaniesListFailure,

  createCompanyStart,
  createCompanySuccess,
  createCompanyFailure,

  updateCompanyStart,
  updateCompanySuccess,
  updateCompanyFailure,
} = companies.actions;

export default companies.reducer;

export const fetchCompaniesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getCompaniesListStart());
      const companiesList = await getCompanies(page, perPage);
      if (companiesList && companiesList.data && companiesList.data.length) {
        dispatch(getCompaniesListSuccess(companiesList));
      } else {
        dispatch(getCompaniesListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getCompaniesListFailure(err.toString()));
    }
  };

export const fetchCompanyById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getCompanyStart());
      const company = await getCompanyById(id);
      dispatch(getCompanySuccess(company));
    } catch (err) {
      dispatch(getCompanyFailure(err.toString()));
    }
  };

export const insertCompany =
  (company: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createCompanyStart());
      const response = await createCompany(company);
      dispatch(createCompanySuccess(response));
      return response;
    } catch (err) {
      dispatch(createCompanyFailure(err.toString()));
      return null;
    }
  };

export const updateCompanyById =
  (company: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateCompanyStart());
      const response = await updateCompany(company);
      dispatch(updateCompanySuccess(response));
      return response;
    } catch (err) {
      dispatch(updateCompanyFailure(err.toString()));
      return null;
    }
  };
