import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Account,
  AccountsList,
  getAccountById,
  getAccounts,
  getPlaidAccounts,
  deleteSubAccountById,
  deleteAccountById,
} from "../api/accountsApi";

import { AppThunk } from "../store";

interface AccountsState {
  totalCount: number;
  page: number;
  perPage: number;
  accountsById: Record<string, Account>;
  accounts: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: AccountsState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  accounts: [],
  accountsById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: AccountsState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: AccountsState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: AccountsState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const accounts = createSlice({
  name: "accounts",
  initialState,
  reducers: {
    getAccountStart: startLoading,
    getAccountsListStart: startLoading,
    createAccountStart: startSubmitting,
    deleteAccountStart: startSubmitting,
    deleteSubAccountStart: startSubmitting,
    getAccountSuccess: (state, { payload }: PayloadAction<Account>) => {
      const { id } = payload;

      state.accountsById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getAccountsListSuccess: (
      state,
      { payload }: PayloadAction<AccountsList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((account) => {
        state.accountsById[account.id] = account;
      });
      state.accounts = props.data.map((asset) => asset.id);
    },
    createAccountSuccess: (state, { payload }: PayloadAction<Account>) => {
      const { id } = payload;

      state.accountsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    deleteAccountSuccess: (
      state,
      { payload }: PayloadAction<Partial<Account>>
    ) => {
      const { id } = payload;
      delete state.accountsById[id];
      state.isSubmitting = false;
      state.error = null;
      state.accounts = state.accounts.filter((account) => account !== id);
    },
    deleteSubAccountSuccess: (
      state,
      { payload }: PayloadAction<Partial<Account>>
    ) => {
      state.isSubmitting = false;
      state.error = null;
    },

    getAccountsListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    getAccountFailure: loadingFailed,
    getAccountListFailure: loadingFailed,
    createAccountFailure: loadingFailed,
    deleteAccountFailure: loadingFailed,
    deleteSubAccountFailure: loadingFailed,
  },
});

export const {
  getAccountStart,
  getAccountsListStart,
  getAccountSuccess,
  getAccountsListSuccess,
  getAccountsListNoDataSuccess,
  getAccountFailure,
  getAccountListFailure,

  createAccountStart,
  createAccountSuccess,
  createAccountFailure,

  deleteAccountStart,
  deleteAccountSuccess,
  deleteAccountFailure,

  deleteSubAccountStart,
  deleteSubAccountSuccess,
  deleteSubAccountFailure,
} = accounts.actions;

export default accounts.reducer;

export const fetchAccountsList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAccountsListStart());
      const accountsList = await getAccounts(page, perPage);
      if (accountsList && accountsList.totalCount > 0) {
        dispatch(getAccountsListSuccess(accountsList));
      } else {
        dispatch(getAccountsListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getAccountListFailure(err.toString()));
    }
  };

export const fetchPlaidAccountsList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAccountsListStart());
      const accountsList = await getPlaidAccounts(page, perPage);
      if (accountsList && accountsList.totalCount > 0) {
        dispatch(getAccountsListSuccess(accountsList));
      } else {
        dispatch(getAccountsListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getAccountListFailure(err.toString()));
    }
  };

export const fetchAccountById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAccountStart());
      const account = await getAccountById(id);
      dispatch(getAccountSuccess(account));
    } catch (err) {
      dispatch(getAccountFailure(err.toString()));
    }
  };

export const removeAccountById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteAccountStart());
      const response = await deleteAccountById(id);
      dispatch(deleteAccountSuccess({ id: id }));
      return response;
    } catch (err) {
      dispatch(deleteAccountFailure(err.toString()));
      return null;
    }
  };

export const removeSubAccountById =
  (id: string, subAccountId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteSubAccountStart());
      const response = await deleteSubAccountById(id, subAccountId);
      dispatch(deleteSubAccountSuccess({ id: id }));
      return response;
    } catch (err) {
      dispatch(deleteSubAccountFailure(err.toString()));
      return null;
    }
  };
