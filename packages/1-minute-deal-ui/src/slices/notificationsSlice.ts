import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Notification,
  NotificationList,
  createNotification,
  getNotificationById,
  getNotifications,
  updateNotification,
} from "../api/notificationsApi";

import { AppThunk } from "../store";

interface NotificationsState {
  totalCount: number;
  page: number;
  perPage: number;
  notificationsById: Record<string, Notification>;
  currentPageNotifications: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: NotificationsState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageNotifications: [],
  notificationsById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: NotificationsState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: NotificationsState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(
  state: NotificationsState,
  action: PayloadAction<string>
) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const notifications = createSlice({
  name: "notifications",
  initialState,
  reducers: {
    getNotificationStart: startLoading,
    getNotificationListStart: startLoading,
    createNotificationStart: startSubmitting,
    updateNotificationStart: startSubmitting,
    getNotificationSuccess: (
      state,
      { payload }: PayloadAction<Notification>
    ) => {
      const { id } = payload;

      state.notificationsById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getNotificationListSuccess: (
      state,
      { payload }: PayloadAction<NotificationList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((notification) => {
        state.notificationsById[notification.id] = notification;
      });
      state.currentPageNotifications = props.data.map(
        (notification) => notification.id
      );
    },
    createNotificationSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.notificationsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateNotificationSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.notificationsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getNotificationListNoDataSuccess: (
      state,
      { payload }: PayloadAction<string | null>
    ) => {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    getNotificationFailure: loadingFailed,
    getNotificationListFailure: loadingFailed,
    createNotificationFailure: loadingFailed,
    updateNotificationFailure: loadingFailed,
  },
});

export const {
  getNotificationStart,
  getNotificationListStart,
  getNotificationSuccess,
  getNotificationListSuccess,
  getNotificationListNoDataSuccess,
  getNotificationFailure,
  getNotificationListFailure,

  createNotificationStart,
  createNotificationSuccess,
  createNotificationFailure,

  updateNotificationStart,
  updateNotificationSuccess,
  updateNotificationFailure,
} = notifications.actions;

export default notifications.reducer;

export const fetchNotificationList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getNotificationListStart());
      const notificationList = await getNotifications(page, perPage);
      if (
        notificationList &&
        notificationList.data &&
        notificationList.data.length
      ) {
        dispatch(getNotificationListSuccess(notificationList));
      } else {
        dispatch(getNotificationListNoDataSuccess(null));
      }
    } catch (err) {
      dispatch(getNotificationListFailure(err.toString()));
    }
  };

export const fetchNotificationById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getNotificationStart());
      const notification = await getNotificationById(id);
      dispatch(getNotificationSuccess(notification));
    } catch (err) {
      dispatch(getNotificationFailure(err.toString()));
    }
  };

export const insertNotification =
  (notification: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createNotificationStart());
      const response = await createNotification(notification);
      dispatch(createNotificationSuccess(response));
      return response;
    } catch (err) {
      dispatch(createNotificationFailure(err.toString()));
      return null;
    }
  };

export const updateNotificationById =
  (notification: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateNotificationStart());
      const response = await updateNotification(notification);
      dispatch(updateNotificationSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateNotificationFailure(err.toString()));
      return null;
    }
  };
