import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  EmailTemplate,
  getEmailTemplate,
  updateEmailTemplate,
} from "../api/emailTemplatesApi";

import { AppThunk } from "../store";
import _ from "lodash";

interface EmailTemplatesState {
  emailTemplateKey: string;
  emailTemplate: EmailTemplate;
  error: {
    timestamp: number;
    data: any;
  } | null;
  state:
    | "IDLE"
    | "LOADING"
    | "LOADING_FAILED"
    | "RETREIVED"
    | "SUBMITTING"
    | "UPDATED"
    | "UPDATE_FAILED";
}

const initialState: EmailTemplatesState = {
  emailTemplateKey: null,
  emailTemplate: null,
  error: null,
  state: "IDLE",
};

// https://dev.to/davidkpiano/no-disabling-a-button-is-not-app-logic-598i

function startLoading(state: EmailTemplatesState) {
  state.error = null;
  state.state = "LOADING";
}

function loadingFailed(state: EmailTemplatesState, action: PayloadAction<any>) {
  state.state = "LOADING_FAILED";
  const data = action.payload?.data?.error ?? "An error occurred";
  state.error = { timestamp: new Date().getTime(), data: data };
}

function loadingSucceeded(
  state: EmailTemplatesState,
  { payload }: PayloadAction<any>
) {
  state.state = "RETREIVED";
  state.emailTemplate = payload;
}

function startSubmitting(state: EmailTemplatesState) {
  state.state = "SUBMITTING";
  state.error = null;
}

function submittingFailed(
  state: EmailTemplatesState,
  action: PayloadAction<any>
) {
  state.state = "UPDATE_FAILED";
  const data = action.payload?.data?.error ?? "An error occurred";
  state.error = { timestamp: new Date().getTime(), data: data };
}

function submittingSucceeded(
  state: EmailTemplatesState,
  { payload }: PayloadAction<any>
) {
  state.state = "UPDATED";
  state.emailTemplate = payload;
}

const emailTemplates = createSlice({
  name: "emailTemplates",
  initialState,
  reducers: {
    getEmailTemplateStart: (state) => {
      if (
        !["SUBMITTING", "LOADING"].includes(state.state) &&
        state.emailTemplateKey
      )
        startLoading(state);
    },
    getEmailTemplateSuccess: loadingSucceeded,
    getEmailTemplateFailure: loadingFailed,

    updateEmailTemplateStart: (state) => {
      if (!["SUBMITTING", "LOADING"].includes(state.state))
        startSubmitting(state);
    },
    updateEmailTemplateSuccess: submittingSucceeded,
    updateEmailTemplateFailure: submittingFailed,

    setEmailTemplateKey: (state, { payload }: PayloadAction<any>) => {
      state.emailTemplateKey = payload;
    },
  },
});

export const {
  getEmailTemplateStart,
  getEmailTemplateSuccess,
  getEmailTemplateFailure,
  updateEmailTemplateStart,
  updateEmailTemplateSuccess,
  updateEmailTemplateFailure,
  setEmailTemplateKey,
} = emailTemplates.actions;

export default emailTemplates.reducer;

export const fetchEmailTemplateByKey =
  (emailTemplateKey: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getEmailTemplateStart());
      const emailTemplate = await getEmailTemplate(emailTemplateKey);
      if (emailTemplate) {
        dispatch(getEmailTemplateSuccess(emailTemplate));
      } else {
        dispatch(getEmailTemplateFailure("Email Template Not Found"));
      }
    } catch (err) {
      dispatch(getEmailTemplateFailure(_.pick(err.response, ["data"])));
    }
  };

export const updateEmailTemplateById =
  (emailTemplate: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateEmailTemplateStart());
      const response = await updateEmailTemplate(emailTemplate);
      dispatch(updateEmailTemplateSuccess(response));
    } catch (err) {
      dispatch(updateEmailTemplateFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };
