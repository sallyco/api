import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Asset,
  AssetsList,
  getAssets,
  getAssetById,
  getAssetByDealId,
  createAsset,
  updateAsset,
} from "../api/assetsApi";

import { AppThunk } from "../store";

interface AssetState {
  totalCount: number;
  page: number;
  perPage: number;
  assetsById: Record<string, Asset>;
  // assetsByDealId: Record<string, Asset>;
  currentPageAssets: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: AssetState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageAssets: [],
  assetsById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: AssetState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: AssetState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: AssetState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const assets = createSlice({
  name: "assets",
  initialState,
  reducers: {
    getAssetStart: startLoading,
    getAssetsListStart: startLoading,
    createAssetStart: startSubmitting,
    updateAssetStart: startSubmitting,
    getAssetSuccess: (state, { payload }: PayloadAction<Asset>) => {
      const { id, dealId } = payload;

      state.assetsById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getAssetsListSuccess: (state, { payload }: PayloadAction<AssetsList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((asset) => {
        state.assetsById[asset.id] = asset;
      });
      state.currentPageAssets = props.data.map((asset) => asset.id);
    },
    createAssetSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id, dealId } = payload;

      state.assetsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateAssetSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id, dealId } = payload;

      state.assetsById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },

    getAssetsListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    getAssetFailure: loadingFailed,
    getAssetsListFailure: loadingFailed,
    createAssetFailure: loadingFailed,
    updateAssetFailure: loadingFailed,
  },
});

export const {
  getAssetStart,
  getAssetsListStart,
  getAssetSuccess,
  getAssetsListSuccess,
  getAssetsListNoDataSuccess,
  getAssetFailure,
  getAssetsListFailure,

  createAssetStart,
  createAssetSuccess,
  createAssetFailure,

  updateAssetStart,
  updateAssetSuccess,
  updateAssetFailure,
} = assets.actions;

export default assets.reducer;

export const fetchAssetsList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAssetsListStart());
      const assetsList = await getAssets(page, perPage);
      if (assetsList && assetsList.data && assetsList.data.length) {
        dispatch(getAssetsListSuccess(assetsList));
      } else {
        dispatch(getAssetsListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getAssetsListFailure(err.toString()));
    }
  };

export const fetchAssetById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAssetStart());
      const asset = await getAssetById(id);
      dispatch(getAssetSuccess(asset));
    } catch (err) {
      dispatch(getAssetFailure(err.toString()));
    }
  };

export const fetchAssetByDealId =
  (dealId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getAssetStart());
      const asset = await getAssetByDealId(dealId);
      if (asset) {
        dispatch(getAssetSuccess(asset));
      } else {
        dispatch(getAssetsListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getAssetFailure(err.toString()));
    }
  };

export const insertAsset =
  (asset: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createAssetStart());
      const response = await createAsset(asset);
      dispatch(createAssetSuccess(response));
      return response;
    } catch (err) {
      dispatch(createAssetFailure(err.toString()));
      return null;
    }
  };

export const updateAssetById =
  (asset: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateAssetStart());
      const response = await updateAsset(asset);
      dispatch(updateAssetSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateAssetFailure(err.toString()));
      return null;
    }
  };
