import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { getAutosuggest, Suggestion } from "../api/locationServiceApi";

import { AppThunk } from "../store";

interface LocationServiceState {
  suggestions: Suggestion[];
  isEmpty: boolean;
  isLoading: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: LocationServiceState = {
  suggestions: [],
  isEmpty: true,
  isLoading: false,
  error: null,
};

function startLoading(state: LocationServiceState) {
  state.isLoading = true;
  state.error = null;
}

function loadingFailed(
  state: LocationServiceState,
  action: PayloadAction<string>
) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const locationService = createSlice({
  name: "locationService",
  initialState,
  reducers: {
    getSuggestionsStart: startLoading,
    getSuggestionsSuccess: (
      state,
      { payload }: PayloadAction<Suggestion[]>
    ) => {
      //TODO error checking?
      //
      state.suggestions = payload;
      state.isEmpty = !payload;
      state.isLoading = false;
      state.error = null;
    },
    getSuggestionsFailure: loadingFailed,
  },
});

export const {
  getSuggestionsStart,
  getSuggestionsSuccess,
  getSuggestionsFailure,
} = locationService.actions;

export default locationService.reducer;

export const fetchSuggestions =
  (query: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getSuggestionsStart());
      const suggestions = await getAutosuggest(query);
      dispatch(getSuggestionsSuccess(suggestions));
    } catch (err) {
      dispatch(getSuggestionsFailure(err.toString()));
    }
  };
