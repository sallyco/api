import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { Tenant, getTenantById, updateTenant } from "../api/tenantApi";
import { AppThunk } from "../store";

interface TenantState {
  tenant: Tenant;
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: TenantState = {
  tenant: {
    id: "",
    inverted: false,
    name: "",
    url: "",
    assets: {
      images: {
        logo: "",
        logoInverted: "",
        favicon: "",
      },
      colors: {
        primaryColor: null,
        secondaryColor: null,
      },
    },
    settings: {
      emails: {
        fromAddress: "",
      },
    },
    bankingLimits: {},
  },
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: TenantState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: TenantState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: TenantState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

function submittingFailed(state: TenantState, action: PayloadAction<string>) {
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const tenant = createSlice({
  name: "tenant",
  initialState: initialState,
  reducers: {
    getTenantStart: startLoading,
    getTenantSuccess: (state, { payload }: PayloadAction<Tenant>) => {
      state.tenant = payload;
      state.isLoading = false;
      state.error = null;
    },
    getTenantFailure: loadingFailed,

    updateTenantStart: startSubmitting,
    updateTenantSuccess: (state, { payload }: PayloadAction<Tenant>) => {
      state.tenant = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateTenantFailure: submittingFailed,
  },
});

export const {
  getTenantStart,
  getTenantSuccess,
  getTenantFailure,
  updateTenantStart,
  updateTenantSuccess,
  updateTenantFailure,
} = tenant.actions;

export const fetchTenantById =
  (tenantId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getTenantStart());
      const tenant = await getTenantById(tenantId);
      if (!tenant?.assets) {
        tenant.assets = tenant?.whiteLabel;
      }
      axios.defaults.headers.common["x-tenant-id"] = `${tenant.id}`;
      dispatch(getTenantSuccess(tenant));
    } catch (err) {
      dispatch(getTenantFailure(err.toString()));
    }
  };

export const updateTenantById =
  (tenant: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateTenantStart());
      const response = await updateTenant(tenant);
      dispatch(updateTenantSuccess(response));
    } catch (err) {
      dispatch(updateTenantFailure(err.toString()));
    }
  };

export default tenant.reducer;
