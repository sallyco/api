import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Close,
  ClosesList,
  getCloses,
  getClosesByDeal,
  getCloseById,
  createClose,
  updateClose,
  generateClosingDocument,
  fundManagerSign,
} from "../api/closesApi";

import { AppThunk } from "../store";

interface ClosesState {
  totalCount: number;
  page: number;
  perPage: number;
  closesById: Record<string, Close>;
  currentPageCloses: string[];
  currentClosingDocuments: any[];
  currentPageDealId: string | null;
  selectedCloseId: string;
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: ClosesState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageDealId: null,
  currentPageCloses: [],
  closesById: {},
  selectedCloseId: "",
  currentClosingDocuments: [],
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: ClosesState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: ClosesState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: ClosesState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const closes = createSlice({
  name: "closes",
  initialState,
  reducers: {
    getCloseStart: startLoading,
    getClosesListStart: startLoading,
    createCloseStart: startSubmitting,
    updateCloseStart: startSubmitting,
    generateCloseDocumentStart: startLoading,
    getCloseSuccess: (state, { payload }: PayloadAction<Close>) => {
      const { id } = payload;

      state.closesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getClosesListSuccess: (state, { payload }: PayloadAction<ClosesList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((close) => {
        state.closesById[close.id] = close;
      });
      state.currentPageCloses = props.data.map((close) => close.id);
      state.currentPageDealId = props.dealId ?? null;
    },
    getClosesListNoDataSuccess: (
      state,
      { payload }: PayloadAction<string | null>
    ) => {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;

      state.currentPageDealId = payload ?? null;
    },
    createCloseSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.closesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateCloseSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.closesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    generateCloseDocumentSuccess: (state, { payload }: PayloadAction<any>) => {
      state.currentClosingDocuments.push(payload);
      state.isLoading = false;
      state.error = null;
    },
    getCloseFailure: loadingFailed,
    getClosesListFailure: loadingFailed,
    createCloseFailure: loadingFailed,
    updateCloseFailure: loadingFailed,
    generateCloseDocumentFailure: loadingFailed,
    setSelectedClose: (state, { payload }: PayloadAction<any>) => {
      state.selectedCloseId = payload;
    },
  },
});

export const {
  getCloseStart,
  getClosesListStart,
  getCloseSuccess,
  getClosesListSuccess,
  getClosesListNoDataSuccess,
  getCloseFailure,
  getClosesListFailure,

  createCloseStart,
  createCloseSuccess,
  createCloseFailure,

  updateCloseStart,
  updateCloseSuccess,
  updateCloseFailure,

  generateCloseDocumentStart,
  generateCloseDocumentSuccess,
  generateCloseDocumentFailure,

  setSelectedClose,
} = closes.actions;

export default closes.reducer;

export const fetchClosesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getClosesListStart());
      const closesList = await getCloses(page, perPage);
      if (closesList && closesList.data && closesList.data.length) {
        dispatch(getClosesListSuccess(closesList));
      } else {
        dispatch(getClosesListNoDataSuccess(null));
      }
    } catch (err) {
      dispatch(getClosesListFailure(err.toString()));
    }
  };

export const fetchClosesListByDeal =
  (dealId: string, page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getClosesListStart());
      const closesList = await getClosesByDeal(dealId, page, perPage);
      if (closesList && closesList.data && closesList.data.length) {
        dispatch(getClosesListSuccess({ ...closesList, dealId }));
      } else {
        dispatch(getClosesListNoDataSuccess(dealId));
      }
    } catch (err) {
      dispatch(getClosesListFailure(err.toString()));
    }
  };

export const fetchCloseById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getCloseStart());
      const close = await getCloseById(id);
      dispatch(getCloseSuccess(close));
    } catch (err) {
      dispatch(getCloseFailure(err.toString()));
    }
  };

export const insertClose =
  (
    dealId: string,
    organizerSignature: string,
    statement?: object,
    subscriptions?: string[]
  ): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createCloseStart());
      const response = await createClose(
        dealId,
        organizerSignature,
        statement,
        subscriptions
      );
      dispatch(createCloseSuccess(response));
      return response;
    } catch (err) {
      dispatch(createCloseFailure(err.toString()));
      throw new Error(err.toString());
    }
  };

export const updateCloseById =
  (close: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateCloseStart());
      const response = await updateClose(close);
      dispatch(updateCloseSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateCloseFailure(err.toString()));
      return null;
    }
  };

export const generateCloseDocument =
  (closeId: string, documentType: string, payload: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(generateCloseDocumentStart());
      const response = await generateClosingDocument(
        closeId,
        documentType,
        payload
      );
      dispatch(generateCloseDocumentSuccess(response));
      return response;
    } catch (err) {
      dispatch(generateCloseDocumentFailure(err.toString()));
      return null;
    }
  };

export const fundManagerSignClose =
  (closeId: string, managerSignature: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateCloseStart());
      const response = await fundManagerSign(closeId, managerSignature);
      dispatch(updateCloseSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateCloseFailure(err.toString()));
      return null;
    }
  };
