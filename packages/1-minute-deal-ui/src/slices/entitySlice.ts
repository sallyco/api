import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Entity,
  EntitiesList,
  getEntities,
  getEntityById,
  createEntity,
  updateEntity,
  generateDocument,
  requestEntityEIN,
  createEntityBankAccount,
  getEntityBankAccountTransactions,
  patchEntity,
} from "../api/entitiesApi";

import { AppThunk } from "../store";
import _ from "lodash";
import { getSubscriptionById } from "../api/subscriptionsApi";

interface EntitiesState {
  totalCount: number;
  page: number;
  perPage: number;
  entitiesById: Record<string, Entity>;
  currentPageEntities: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: any;
  } | null;
}

const initialState: EntitiesState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageEntities: [],
  entitiesById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: EntitiesState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: EntitiesState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: EntitiesState, action: PayloadAction<any>) {
  state.isLoading = false;
  state.isSubmitting = false;
  const data = action.payload?.data?.error ?? "An error occurred";
  state.error = { timestamp: new Date().getTime(), data: data };
}

const entities = createSlice({
  name: "entities",
  initialState,
  reducers: {
    getEntityStart: startLoading,
    getEntitiesListStart: startLoading,
    createEntityStart: startSubmitting,
    updateEntityStart: startSubmitting,
    generateEntityDocumentStart: startSubmitting,
    generateEntityEINStart: startSubmitting,
    generateEntityBankAccountStart: startSubmitting,
    getEntitySuccess: (state, { payload }: PayloadAction<Entity>) => {
      const { id } = payload;

      state.entitiesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getEntitiesListSuccess: (
      state,
      { payload }: PayloadAction<EntitiesList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((entity) => {
        state.entitiesById[entity.id] = entity;
      });
      state.currentPageEntities = props.data.map((entity) => entity.id);
    },
    getEntitiesListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    createEntitySuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.entitiesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateEntitySuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.entitiesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    generateEntityDocumentSuccess: (state, { payload }: PayloadAction<any>) => {
      state.isSubmitting = false;
      state.error = null;
    },
    generateEntityEINSuccess: (state, { payload }: PayloadAction<any>) => {
      state.isSubmitting = false;
      state.error = null;
    },
    generateEntityBankAccountSuccess: (
      state,
      { payload }: PayloadAction<Entity>
    ) => {
      state.entitiesById[payload.id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getEntityFailure: loadingFailed,
    getEntitiesListFailure: loadingFailed,
    createEntityFailure: loadingFailed,
    updateEntityFailure: loadingFailed,
    generateEntityDocumentFailure: loadingFailed,
    generateEntityEINFailure: loadingFailed,
    generateEntityBankAccountFailure: loadingFailed,
    getEntityBankAccountTransactionsStart: startSubmitting,
    getEntityBankAccountTransactionsSuccess: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      const { id, transactions } = payload;
      state.entitiesById[id].transactions = transactions;
      state.isSubmitting = false;
      state.error = null;
    },
    getEntityBankAccountTransactionsFailure: loadingFailed,
  },
});

export const {
  getEntityStart,
  getEntitiesListStart,
  getEntitySuccess,
  getEntitiesListSuccess,
  getEntitiesListNoDataSuccess,
  getEntityFailure,
  getEntitiesListFailure,
  getEntityBankAccountTransactionsStart,
  getEntityBankAccountTransactionsSuccess,
  getEntityBankAccountTransactionsFailure,

  createEntityStart,
  createEntitySuccess,
  createEntityFailure,

  updateEntityStart,
  updateEntitySuccess,
  updateEntityFailure,
  generateEntityDocumentStart,
  generateEntityDocumentSuccess,
  generateEntityDocumentFailure,
  generateEntityEINStart,
  generateEntityEINSuccess,
  generateEntityEINFailure,
  generateEntityBankAccountStart,
  generateEntityBankAccountSuccess,
  generateEntityBankAccountFailure,
} = entities.actions;

export default entities.reducer;

export const fetchEntitiesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getEntitiesListStart());
      const entitiesList = await getEntities(page, perPage);
      if (entitiesList && entitiesList.data && entitiesList.data.length) {
        dispatch(getEntitiesListSuccess(entitiesList));
      } else {
        dispatch(getEntitiesListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getEntitiesListFailure(_.pick(err.response, ["data"])));
    }
  };

export const fetchEntityById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getEntityStart());
      const entity = await getEntityById(id);
      dispatch(getEntitySuccess(entity));
    } catch (err) {
      dispatch(getEntityFailure(_.pick(err.response, ["data"])));
    }
  };

export const insertEntity =
  (entity: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createEntityStart());
      const response = await createEntity(entity);
      dispatch(createEntitySuccess(response));
      return response;
    } catch (err) {
      dispatch(createEntityFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const updateEntityById =
  (entity: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateEntityStart());
      const response = await updateEntity(entity);
      dispatch(updateEntitySuccess(response));
      return response;
    } catch (err) {
      dispatch(updateEntityFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const patchEntityById =
  (entityId: string, entityData: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateEntityStart());
      const response = await patchEntity(entityId, entityData);
      dispatch(updateEntitySuccess(response));
      return response;
    } catch (err) {
      dispatch(updateEntityFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const generateEntityDocument =
  (entityId: string, documentType: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(generateEntityDocumentStart());
      const response = await generateDocument(entityId, documentType);
      dispatch(generateEntityDocumentSuccess(response));
      return response;
    } catch (err) {
      dispatch(generateEntityDocumentFailure(_.pick(err.response, ["data"])));
      return null;
    }
  };

export const generateEntityEIN =
  (entity: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(generateEntityEINStart());
      const response = await requestEntityEIN(entity);
      dispatch(generateEntityEINSuccess(response));
      return response;
    } catch (err) {
      dispatch(generateEntityEINFailure(err.toString()));
      return null;
    }
  };

export const generateEntityBankAccount =
  (entity: Entity): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(generateEntityBankAccountStart());
      await createEntityBankAccount(entity);
      const updatedEntity = await getEntityById(entity.id);
      dispatch(generateEntityBankAccountSuccess(updatedEntity));
      return updatedEntity;
    } catch (err) {
      dispatch(generateEntityBankAccountFailure(err.toString()));
      return null;
    }
  };

export const fetchEntityBankAccountTransactions =
  (entity: Entity): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getEntityBankAccountTransactionsStart());
      const transactions = await getEntityBankAccountTransactions(entity);
      transactions.map(async (single_transaction) => {
        if (single_transaction.subscriptionId) {
          single_transaction.subscription = await getSubscriptionById(
            single_transaction.subscriptionId
          );
        }
      });
      dispatch(
        getEntityBankAccountTransactionsSuccess({ id: entity.id, transactions })
      );
    } catch (err) {
      dispatch(getEntityBankAccountTransactionsFailure(err.toString()));
      return null;
    }
  };
