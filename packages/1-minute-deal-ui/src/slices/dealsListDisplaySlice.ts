import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface DealsListDisplay {
  displayType: "cards" | "table";
}

interface DealsListDisplayPayload {
  displayType: "cards" | "table";
}

type DealsListDisplayState = {
  page: number;
} & DealsListDisplay;

let initialState: DealsListDisplayState = {
  page: 1,
  displayType: "cards",
};

const dealsListDisplaySlice = createSlice({
  name: "dealsListDisplay",
  initialState,
  reducers: {
    setDealsListPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setDealsListDisplayType(
      state,
      action: PayloadAction<DealsListDisplayPayload>
    ) {
      const { displayType } = action.payload;
      state.displayType = displayType;
    },
  },
});

export const { setDealsListPage, setDealsListDisplayType } =
  dealsListDisplaySlice.actions;

export default dealsListDisplaySlice.reducer;
