import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface SubscriptionsListOperation {
  opType: "search" | "filter" | "sort" | "";
}

interface SubscriptionsListOperationPayload {
  opType: "search" | "filter" | "sort" | "";
}

type SubscriptionsListOperationState = {
  page: number;
  opValue: string;
  opFilterValue?: { field?: string; value?: string }[];
} & SubscriptionsListOperation;

let initialState: SubscriptionsListOperationState = {
  page: 1,
  opValue: "",
  opFilterValue: [],
  opType: "",
};

const subscriptionsListOperationSlice = createSlice({
  name: "subscriptionsListOperation",
  initialState,
  reducers: {
    setSubscriptionsListOpPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setSubscriptionsListOpType(
      state,
      action: PayloadAction<SubscriptionsListOperationPayload>
    ) {
      const { opType } = action.payload;
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      (state.opType = opType), (state.opValue = "");
    },
    setSubscriptionsListOpValue(
      state,
      action: PayloadAction<{ field?: string; value?: string }[] | string>
    ) {
      if (typeof action.payload === "string") {
        state.opValue = action.payload;
      } else {
        state.opFilterValue = action.payload;
      }
    },
    setSubscriptionsListOpReset(state) {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      (state.opType = ""),
        (state.opValue = ""),
        (state.page = 1),
        (state.opFilterValue = []);
    },
  },
});

export const {
  setSubscriptionsListOpPage,
  setSubscriptionsListOpType,
  setSubscriptionsListOpValue,
  setSubscriptionsListOpReset,
} = subscriptionsListOperationSlice.actions;

export default subscriptionsListOperationSlice.reducer;
