import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Profile,
  getProfiles,
  getProfileById,
  createProfile,
  updateProfile,
  addBankingUser,
  addBankingAccount,
  deleteBankingAccount,
  deleteProfileByProfileId,
} from "../api/profilesApi";

import { AppThunk } from "../store";
import { EntityList } from "../api/interfaces";

interface ProfilesState {
  totalCount: number;
  page: number;
  perPage: number;
  profilesById: Record<string, Profile>;
  currentPageProfiles: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
  profileManagementDisplayType: string;
  profileManagementSelectedId: string;
}

const initialState: ProfilesState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageProfiles: [],
  profilesById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
  profileManagementDisplayType: "edit",
  profileManagementSelectedId: null,
};

function startLoading(state: ProfilesState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: ProfilesState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: ProfilesState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const profiles = createSlice({
  name: "profiles",
  initialState,
  reducers: {
    getProfileStart: startLoading,
    getProfilesListStart: startLoading,
    createProfileStart: startSubmitting,
    updateProfileStart: startSubmitting,
    deleteProfileStart: startSubmitting,
    addBankingUserStart: startSubmitting,
    addBankingAccountStart: startSubmitting,
    removeBankingAccountStart: startSubmitting,
    getProfileSuccess: (state, { payload }: PayloadAction<Profile>) => {
      const { id } = payload;

      state.profilesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getProfilesListSuccess: (
      state,
      { payload }: PayloadAction<EntityList<Profile>>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((profile) => {
        state.profilesById[profile.id] = profile;
      });
      state.currentPageProfiles = props.data.map((profile) => profile.id);
    },
    getProfilesListNoDataSuccess(state) {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;
    },
    createProfileSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.profilesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateProfileSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.profilesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    deleteProfileSuccess: (state, { payload }: PayloadAction<any>) => {
      let profilesById = state.profilesById;
      delete profilesById[payload.id];

      state.profilesById = profilesById;
      state.isSubmitting = false;
      state.error = null;
    },
    addBankingUserSuccess: (state, { payload }: PayloadAction<any>) => {
      state.isSubmitting = false;
      state.error = null;
    },
    addBankingAccountSuccess: (state, { payload }: PayloadAction<any>) => {
      state.isSubmitting = false;
      state.error = null;
    },
    removeBankingAccountSuccess: (state, { payload }: PayloadAction<any>) => {
      state.isSubmitting = false;
      state.error = null;
    },
    getProfileFailure: loadingFailed,
    getProfilesListFailure: loadingFailed,
    createProfileFailure: loadingFailed,
    updateProfileFailure: loadingFailed,
    deleteProfileFailure: loadingFailed,
    addBankingUserFailure: loadingFailed,
    addBankingAccountFailure: loadingFailed,
    removeBankingAccountFailure: loadingFailed,
    setProfileManagementDisplayType: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.profileManagementDisplayType = payload;
    },
    setProfileManagementSelectedId: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.profileManagementSelectedId = payload;
    },
  },
});

export const {
  getProfileStart,
  getProfilesListStart,
  getProfileSuccess,
  getProfilesListSuccess,
  getProfilesListNoDataSuccess,
  getProfileFailure,
  getProfilesListFailure,

  createProfileStart,
  createProfileSuccess,
  createProfileFailure,

  updateProfileStart,
  updateProfileSuccess,
  updateProfileFailure,

  addBankingUserStart,
  addBankingUserSuccess,
  addBankingUserFailure,
  addBankingAccountSuccess,
  addBankingAccountFailure,

  setProfileManagementDisplayType,
  setProfileManagementSelectedId,

  removeBankingAccountStart,
  removeBankingAccountSuccess,
  removeBankingAccountFailure,

  deleteProfileStart,
  deleteProfileSuccess,
  deleteProfileFailure,
} = profiles.actions;

export default profiles.reducer;

export const fetchProfilesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getProfilesListStart());
      const profilesList = await getProfiles(page, perPage);
      if (profilesList && profilesList.data && profilesList.data.length) {
        dispatch(getProfilesListSuccess(profilesList));
      } else {
        dispatch(getProfilesListNoDataSuccess());
      }
    } catch (err) {
      dispatch(getProfilesListFailure(err.toString()));
    }
  };

export const fetchProfileById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getProfileStart());
      const profile = await getProfileById(id);
      dispatch(getProfileSuccess(profile));
    } catch (err) {
      dispatch(getProfileFailure(err.toString()));
    }
  };

export const insertProfile =
  (profile: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createProfileStart());
      const response = await createProfile(profile);
      dispatch(createProfileSuccess(response));
      return response;
    } catch (err) {
      dispatch(createProfileFailure(err.toString()));
      return null;
    }
  };

export const updateProfileById =
  (profile: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateProfileStart());
      const response = await updateProfile(profile);
      dispatch(updateProfileSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateProfileFailure(err.toString()));
      return null;
    }
  };

export const updateBankingUser =
  (id: string, data: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(addBankingUserStart());
      const response = await addBankingUser(id, data);
      dispatch(addBankingUserSuccess(response));
      return response;
    } catch (err) {
      dispatch(addBankingUserFailure(err.toString()));
      return null;
    }
  };

export const insertBankingAccount =
  (id: string, accountId: string, subAccountId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(addBankingUserStart());
      const response = await addBankingAccount(id, accountId, subAccountId);
      dispatch(addBankingAccountSuccess(response));
      return response;
    } catch (err) {
      if (err?.response?.status === 409) {
        dispatch(
          addBankingAccountFailure(
            err?.response?.data?.error?.message ??
              "Error while linking bank account to profile"
          )
        );
        return null;
      } else {
        dispatch(
          addBankingAccountFailure(
            "This Plaid account is no longer available. " +
              'Please click "+ Add a Bank Account" to re-link the account'
          )
        );
        throw new Error("Plaid Auth Failed");
      }
    }
  };

export const removeBankingAccount =
  (id: string, accountId: string, subAccountId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(removeBankingAccountStart());
      const response = await deleteBankingAccount(id, accountId, subAccountId);
      dispatch(removeBankingAccountSuccess(response));
      return response;
    } catch (err) {
      dispatch(removeBankingAccountFailure(err.toString()));
      return null;
    }
  };

export const deleteProfileById =
  (profile: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteProfileStart());
      const response = await deleteProfileByProfileId(profile.id);
      dispatch(deleteProfileSuccess(profile));
      return response;
    } catch (err) {
      dispatch(deleteProfileFailure(err.toString()));
      return null;
    }
  };
