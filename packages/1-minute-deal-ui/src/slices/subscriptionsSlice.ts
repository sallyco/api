import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Subscription,
  SubscriptionsList,
  getSubscriptions,
  getSubscriptionsByDeal,
  getSubscriptionById,
  createSubscription,
  updateSubscription,
  generateSigningDocument,
  initSubscriptionACHTransfer,
  deleteSubscription,
  initSubscriptionACHReverseTransfer,
  patchSubscription,
  searchBySubscription,
  resetSubscription,
} from "../api/subscriptionsApi";

import { AppThunk } from "../store";
import { ACHAccountDetails } from "../components/subscriptions/RefundSubscriptionForm";
import { API } from "../api/swrApi";
import { mutate } from "swr";

interface SubscriptionsState {
  totalCount: number;
  page: number;
  perPage: number;
  subscriptionsById: Record<string, Subscription>;
  subscriptionsByDealId: Record<string, Record<string, Subscription>>;
  currentPageSubscriptions: string[];
  currentSigningDocuments: any[];
  isInitialized: boolean;
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: SubscriptionsState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  subscriptionsById: {},
  subscriptionsByDealId: {},
  currentSigningDocuments: [],
  currentPageSubscriptions: [],
  isInitialized: false,
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: SubscriptionsState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: SubscriptionsState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(
  state: SubscriptionsState,
  action: PayloadAction<string>
) {
  state.isLoading = false;
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const subscriptions = createSlice({
  name: "subscriptions",
  initialState,
  reducers: {
    getSubscriptionStart: startLoading,
    getSubscriptionsListStart: startLoading,
    createSubscriptionStart: startSubmitting,
    updateSubscriptionStart: startSubmitting,
    deleteSubscriptionStart: startSubmitting,
    generateSubscriptionDocumentStart: startLoading,
    initSubscriptionACHTransferStart: startLoading,
    initSubscriptionACHReverseTransferStart: startLoading,
    searchSubscriptionStart: startSubmitting,
    getSubscriptionSuccess: (
      state,
      { payload }: PayloadAction<Subscription>
    ) => {
      const { id } = payload;

      state.subscriptionsById[id] = payload;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;
    },
    getSubscriptionsListSuccess: (
      state,
      { payload }: PayloadAction<SubscriptionsList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      state.currentPageSubscriptions = props.data.map(
        (subscription) => subscription.id
      );

      props.data.forEach((subscription) => {
        state.subscriptionsById[subscription.id] = subscription;
        if (!(subscription.dealId in state.subscriptionsByDealId))
          state.subscriptionsByDealId[subscription.dealId] = {};
        state.subscriptionsByDealId[subscription.dealId][subscription.id] =
          subscription;
      });
    },
    createSubscriptionSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id, dealId } = payload;

      state.subscriptionsById[id] = payload;
      if (!(dealId in state.subscriptionsByDealId))
        state.subscriptionsByDealId[dealId] = {};
      state.subscriptionsByDealId[dealId][id] = payload;
      state.isSubmitting = false;
      state.isInitialized = true;
      state.error = null;
    },
    updateSubscriptionSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id, dealId } = payload;

      state.subscriptionsById[id] = payload;
      if (!(dealId in state.subscriptionsByDealId))
        state.subscriptionsByDealId[dealId] = {};
      state.subscriptionsByDealId[dealId][id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    deleteSubscriptionSuccess: (state, { payload }: PayloadAction<any>) => {
      let subscriptionsById = state.subscriptionsById;
      delete subscriptionsById[payload];
      state.subscriptionsById = subscriptionsById;

      let subsByDealId = state.subscriptionsByDealId;
      for (let deal in subsByDealId) {
        delete subsByDealId[deal][payload];
      }

      state.currentPageSubscriptions = state.currentPageSubscriptions.filter(
        (subId) => subId !== payload
      );

      state.isSubmitting = false;
      state.error = null;
    },
    getSubscriptionsListNoDataSuccess: (
      state,
      { payload }: PayloadAction<string | null>
    ) => {
      state.isEmpty = true;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      if (payload) state.subscriptionsByDealId[payload] = {};
    },
    generateSubscriptionDocumentSuccess: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.currentSigningDocuments.push(payload);
      state.isLoading = false;
      state.error = null;
    },
    clearSigningDocuments: (state) => {
      state.currentSigningDocuments = [];
    },
    initSubscriptionACHTransferSuccess: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.isLoading = false;
      state.error = null;
    },
    initSubscriptionACHReverseTransferSuccess: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.isLoading = false;
      state.error = null;
    },
    searchSubscriptionSuccess: (
      state,
      { payload }: PayloadAction<SubscriptionsList>
    ) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.isInitialized = true;
      state.error = null;

      props.data.forEach((subscription) => {
        state.subscriptionsById[subscription.id] = subscription;
        if (!(subscription.dealId in state.subscriptionsByDealId))
          state.subscriptionsByDealId[subscription.dealId] = {};
        state.subscriptionsByDealId[subscription.dealId][subscription.id] =
          subscription;
      });
      state.currentPageSubscriptions = props.data.map(
        (subscription) => subscription.id
      );
    },
    getSubscriptionFailure: loadingFailed,
    getSubscriptionsListFailure: loadingFailed,
    createSubscriptionFailure: loadingFailed,
    updateSubscriptionFailure: loadingFailed,
    deleteSubscriptionFailure: loadingFailed,
    generateSubscriptionDocumentFailure: loadingFailed,
    initSubscriptionACHTransferFailure: loadingFailed,
    initSubscriptionACHReverseTransferFailure: loadingFailed,
    searchSubscriptionFailure: loadingFailed,
  },
});

export const {
  getSubscriptionStart,
  getSubscriptionsListStart,
  getSubscriptionSuccess,
  getSubscriptionsListSuccess,
  getSubscriptionsListNoDataSuccess,
  getSubscriptionFailure,
  getSubscriptionsListFailure,

  createSubscriptionStart,
  createSubscriptionSuccess,
  createSubscriptionFailure,

  updateSubscriptionStart,
  updateSubscriptionSuccess,
  updateSubscriptionFailure,

  deleteSubscriptionStart,
  deleteSubscriptionSuccess,
  deleteSubscriptionFailure,

  generateSubscriptionDocumentStart,
  generateSubscriptionDocumentSuccess,
  clearSigningDocuments,
  generateSubscriptionDocumentFailure,

  initSubscriptionACHTransferStart,
  initSubscriptionACHTransferSuccess,
  initSubscriptionACHTransferFailure,

  initSubscriptionACHReverseTransferStart,
  initSubscriptionACHReverseTransferSuccess,
  initSubscriptionACHReverseTransferFailure,

  searchSubscriptionStart,
  searchSubscriptionSuccess,
  searchSubscriptionFailure,
} = subscriptions.actions;

export default subscriptions.reducer;

export const fetchSubscriptionsList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getSubscriptionsListStart());
      const subscriptionsList = await getSubscriptions(page, perPage);
      // if (
      //   subscriptionsList &&
      //   subscriptionsList.data &&
      //   subscriptionsList.data.length
      // ) {
      dispatch(getSubscriptionsListSuccess(subscriptionsList));
      // } else {
      //   dispatch(getSubscriptionsListNoDataSuccess(null));
      // }
    } catch (err) {
      dispatch(getSubscriptionsListFailure(err.toString()));
    }
  };

export const fetchSubscriptionsListByDeal =
  (dealId: string, page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getSubscriptionsListStart());
      const subscriptionsList = await getSubscriptionsByDeal(
        dealId,
        page,
        perPage,
        true
      );
      if (
        subscriptionsList &&
        subscriptionsList.data &&
        subscriptionsList.data.length
      ) {
        dispatch(getSubscriptionsListSuccess({ ...subscriptionsList, dealId }));
      } else {
        dispatch(getSubscriptionsListNoDataSuccess(dealId));
      }
    } catch (err) {
      dispatch(getSubscriptionsListFailure(err.toString()));
    }
  };
export const fetchSubscriptionById =
  (id: string, filter?: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getSubscriptionStart());
      const subscription = await getSubscriptionById(id, filter);
      dispatch(getSubscriptionSuccess(subscription));
    } catch (err) {
      dispatch(getSubscriptionFailure(err.toString()));
    }
  };

export const insertSubscription =
  (subscription: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createSubscriptionStart());
      const response = await createSubscription(subscription);
      dispatch(createSubscriptionSuccess(response));
      return response;
    } catch (err) {
      dispatch(createSubscriptionFailure(err.toString()));
      return null;
    }
  };

export const updateSubscriptionById =
  (subscription: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateSubscriptionStart());
      const response = await updateSubscription(subscription);
      await mutate(API.SUBSCRIPTION_BY_ID(subscription.id));
      dispatch(updateSubscriptionSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateSubscriptionFailure(err.toString()));
      return null;
    }
  };

export const resetSubscriptionById =
  (subscriptionId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateSubscriptionStart());
      const response = await resetSubscription(subscriptionId);
      await mutate(API.SUBSCRIPTION_BY_ID(subscriptionId));
      dispatch(updateSubscriptionSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateSubscriptionFailure(err.toString()));
      return null;
    }
  };

export const patchSubscriptionById =
  (subscriptionId: string, subscription: Partial<Subscription>): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateSubscriptionStart());
      const response = await patchSubscription(subscriptionId, subscription);
      dispatch(updateSubscriptionSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateSubscriptionFailure(err.toString()));
      return null;
    }
  };

export const generateSubscriptionDocument =
  (
    subscriptionId: string,
    documentType: string,
    signature?: string
  ): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(generateSubscriptionDocumentStart());
      if (signature) {
        const response = await generateSigningDocument(
          subscriptionId,
          documentType,
          { signature: signature }
        );
        dispatch(generateSubscriptionDocumentSuccess(response));
        return response;
      } else {
        const response = await generateSigningDocument(
          subscriptionId,
          documentType,
          undefined
        );
        dispatch(generateSubscriptionDocumentSuccess(response));
        return response;
      }
    } catch (err) {
      dispatch(generateSubscriptionDocumentFailure(err.toString()));
      return null;
    }
  };

export const subscriptionACHTransfer =
  (subscription: any): AppThunk =>
  async (dispatch) => {
    try {
      await dispatch(initSubscriptionACHTransferStart());
      const response = await initSubscriptionACHTransfer(subscription.id);
      if (response) {
        dispatch(initSubscriptionACHTransferSuccess(response));
        return response;
      }
      throw new Error("Failed to initiate transfer: Unknown Reason");
    } catch (err) {
      await dispatch(initSubscriptionACHTransferFailure(err));
      return null;
    }
  };

export const subscriptionACHReverseTransfer =
  (subscriptionId: string, accountDetails: ACHAccountDetails): AppThunk =>
  async (dispatch) => {
    try {
      await dispatch(initSubscriptionACHTransferStart());
      const response = await initSubscriptionACHReverseTransfer(
        subscriptionId,
        accountDetails
      );
      dispatch(initSubscriptionACHReverseTransferSuccess(response));
      return response;
    } catch (err) {
      await dispatch(initSubscriptionACHTransferFailure());
      throw err;
    }
  };

export const deleteSubscriptionById =
  (subscriptionId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteSubscriptionStart());
      const response = await deleteSubscription(subscriptionId);
      dispatch(deleteSubscriptionSuccess(subscriptionId));
      return response;
    } catch (err) {
      dispatch(deleteSubscriptionFailure(err.toString()));
      return null;
    }
  };

export const searchSubscription =
  (SearchState: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(searchSubscriptionStart());
      const response = await searchBySubscription(SearchState);
      dispatch(searchSubscriptionSuccess(response));
      return response;
    } catch (err) {
      dispatch(searchSubscriptionFailure(err.toString()));
      return null;
    }
  };
