import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Invite,
  InvitesList,
  getInvites,
  getInviteById,
  getInvitesByDeal,
  createInvite,
  updateInvite,
  acceptInvite,
  deleteInvite,
} from "../api/invitesApi";

import { AppThunk } from "../store";

interface InvitesState {
  totalCount: number;
  page: number;
  perPage: number;
  invitesById: Record<string, Invite>;
  currentPageInvites: string[];
  currentPageDealId: string | null;
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: InvitesState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageInvites: [],
  currentPageDealId: null,
  invitesById: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: InvitesState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: InvitesState) {
  state.isSubmitting = true;
  state.error = null;
}

function submittingFailed(state: InvitesState, action: PayloadAction<string>) {
  state.isSubmitting = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

function loadingFailed(state: InvitesState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const invites = createSlice({
  name: "invites",
  initialState,
  reducers: {
    getInviteStart: startLoading,
    getInvitesListStart: startLoading,
    createInviteStart: startSubmitting,
    updateInviteStart: startSubmitting,
    getInviteSuccess: (state, { payload }: PayloadAction<Invite>) => {
      const { id } = payload;

      state.invitesById[id] = payload;
      state.isLoading = false;
      state.error = null;
    },
    getInvitesListSuccess: (state, { payload }: PayloadAction<InvitesList>) => {
      const { ...props } = payload;
      state.totalCount = props.totalCount;
      state.page = props.page;
      state.perPage = props.perPage;
      state.isEmpty = false;
      state.isLoading = false;
      state.error = null;

      props.data.forEach((invite) => {
        state.invitesById[invite.id] = invite;
      });
      state.currentPageInvites = props.data.map((invite) => invite.id);
      state.currentPageDealId = props.dealId ?? null;
    },
    createInviteSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.invitesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateInviteSuccess: (state, { payload }: PayloadAction<any>) => {
      const { id } = payload;

      state.invitesById[id] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getInvitesListNoDataSuccess: (
      state,
      { payload }: PayloadAction<string | null>
    ) => {
      state.isEmpty = true;
      state.isLoading = false;
      state.error = null;

      state.currentPageDealId = payload ?? null;
    },
    getInviteFailure: loadingFailed,
    getInvitesListFailure: loadingFailed,
    createInviteFailure: loadingFailed,
    updateInviteFailure: loadingFailed,

    deleteInviteStart: startSubmitting,
    deleteInviteSuccess: (state, { payload }: PayloadAction<any>) => {
      state.currentPageInvites = state.currentPageInvites.filter(
        (inviteId) => inviteId !== payload
      );
      let invitesById = state.invitesById;
      delete invitesById[payload];
      state.invitesById = invitesById;
      state.isSubmitting = false;
      state.error = null;
    },
    deleteInviteFailure: submittingFailed,
  },
});

export const {
  getInviteStart,
  getInvitesListStart,
  getInviteSuccess,
  getInvitesListSuccess,
  getInvitesListNoDataSuccess,
  getInviteFailure,
  getInvitesListFailure,

  createInviteStart,
  createInviteSuccess,
  createInviteFailure,

  updateInviteStart,
  updateInviteSuccess,
  updateInviteFailure,

  deleteInviteStart,
  deleteInviteSuccess,
  deleteInviteFailure,
} = invites.actions;

export default invites.reducer;

export const fetchInvitesList =
  (page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getInvitesListStart());
      const invitesList = await getInvites(page, perPage);
      if (invitesList && invitesList.data && invitesList.data.length) {
        dispatch(getInvitesListSuccess(invitesList));
      } else {
        dispatch(getInvitesListNoDataSuccess(null));
      }
    } catch (err) {
      dispatch(getInvitesListFailure(err.toString()));
    }
  };

export const fetchInvitesListByDeal =
  (dealId: string, page?: number, perPage?: number): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getInvitesListStart());
      const invitesList = await getInvitesByDeal(dealId, page, perPage);
      if (invitesList && invitesList.data && invitesList.data.length) {
        dispatch(getInvitesListSuccess({ ...invitesList, dealId }));
      } else {
        dispatch(getInvitesListNoDataSuccess(dealId));
      }
    } catch (err) {
      dispatch(getInvitesListFailure(err.toString()));
    }
  };

export const fetchInviteById =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getInviteStart());
      const invite = await getInviteById(id);
      dispatch(getInviteSuccess(invite));
    } catch (err) {
      dispatch(getInviteFailure(err.toString()));
    }
  };

export const insertInvite =
  (invite: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createInviteStart());
      const response = await createInvite(invite);
      dispatch(createInviteSuccess(response));
      return response;
    } catch (err) {
      dispatch(createInviteFailure(err.toString()));
      return null;
    }
  };

export const updateInviteById =
  (invite: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateInviteStart());
      const response = await updateInvite(invite);
      dispatch(updateInviteSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateInviteFailure(err.toString()));
      return null;
    }
  };

export const acceptInviteById =
  (inviteId: string, userId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateInviteStart());
      const response = await acceptInvite(inviteId, userId);
      dispatch(updateInviteSuccess(response));
      return response;
    } catch (err) {
      return null;
    }
  };

export const deleteInviteById =
  (inviteId: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(deleteInviteStart());
      await deleteInvite(inviteId);
      dispatch(deleteInviteSuccess(inviteId));
      return;
    } catch (err) {
      dispatch(deleteInviteFailure(err.toString()));
      return null;
    }
  };
