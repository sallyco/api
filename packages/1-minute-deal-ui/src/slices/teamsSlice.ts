import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  Team,
  getTeamByOwnerId,
  createTeam,
  updateTeam,
} from "../api/teamsApi";

import { AppThunk } from "../store";

interface TeamState {
  totalCount: number;
  page: number;
  perPage: number;
  teamsByOwnerId: Record<string, Team>;
  // teamsByDealId: Record<string, Team>;
  currentPageTeams: string[];
  isEmpty: boolean;
  isLoading: boolean;
  isSubmitting: boolean;
  error: {
    timestamp: number;
    data: string;
  } | null;
}

const initialState: TeamState = {
  page: 1,
  totalCount: 0,
  perPage: 10,
  currentPageTeams: [],
  teamsByOwnerId: {},
  isEmpty: true,
  isLoading: false,
  isSubmitting: false,
  error: null,
};

function startLoading(state: TeamState) {
  state.isLoading = true;
  state.error = null;
}

function startSubmitting(state: TeamState) {
  state.isSubmitting = true;
  state.error = null;
}

function loadingFailed(state: TeamState, action: PayloadAction<string>) {
  state.isLoading = false;
  state.error = { timestamp: new Date().getTime(), data: action.payload };
}

const teams = createSlice({
  name: "teams",
  initialState,
  reducers: {
    getTeamStart: startLoading,
    createTeamStart: startSubmitting,
    updateTeamStart: startSubmitting,
    getTeamSuccess: (state, { payload }: PayloadAction<Team>) => {
      const { ownerId } = payload;

      state.teamsByOwnerId[ownerId] = payload;
      state.isLoading = false;
      state.error = null;
    },
    createTeamSuccess: (state, { payload }: PayloadAction<any>) => {
      const { ownerId } = payload;

      state.teamsByOwnerId[ownerId] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    updateTeamSuccess: (state, { payload }: PayloadAction<any>) => {
      const { ownerId } = payload;

      state.teamsByOwnerId[ownerId] = payload;
      state.isSubmitting = false;
      state.error = null;
    },
    getTeamFailure: loadingFailed,
    createTeamFailure: loadingFailed,
    updateTeamFailure: loadingFailed,
  },
});

export const {
  getTeamStart,
  getTeamSuccess,
  getTeamFailure,

  createTeamStart,
  createTeamSuccess,
  createTeamFailure,

  updateTeamStart,
  updateTeamSuccess,
  updateTeamFailure,
} = teams.actions;

export default teams.reducer;

export const fetchTeamByOwnerId =
  (id: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(getTeamStart());
      const team = await getTeamByOwnerId(id);
      dispatch(getTeamSuccess(team));
    } catch (err) {
      dispatch(getTeamFailure(err.toString()));
    }
  };

export const insertTeam =
  (team: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(createTeamStart());
      const response = await createTeam(team);
      dispatch(createTeamSuccess(response));
      return response;
    } catch (err) {
      dispatch(createTeamFailure(err.toString()));
      return null;
    }
  };

export const updateTeamById =
  (team: any): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(updateTeamStart());
      const response = await updateTeam(team);
      dispatch(updateTeamSuccess(response));
      return response;
    } catch (err) {
      dispatch(updateTeamFailure(err.toString()));
      return null;
    }
  };
