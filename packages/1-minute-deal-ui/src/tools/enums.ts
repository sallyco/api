export const EntityTypes = [
  {
    key: "1",
    value: "LIMITED_LIABILITY_COMPANY",
    text: "Limited Liability Company",
  },
  { key: "2", value: "LIMITED_PARTNERSHIP", text: "Limited Partnership" },
  { key: "3", value: "C_CORPORATION", text: "C Corporation" },
  { key: "4", value: "S_CORPORATION", text: "S Corporation" },
  { key: "5", value: "GENERAL_PARTNERSHIP", text: "General Partnership" },
  { key: "6", value: "501_C_NONPROFIT", text: "501(c) Non-Profit" },
];

/**
 * Named Temporary, because we need to consolidate EntityTypes in all areas.
 * It needs to be determined if all areas can have all 6 UI options, and save
 * all 6 options in the DB
 */
export const TemporaryDealEntityTypes = EntityTypes.concat({
  key: "6",
  value: "FOREIGN_ENTITY",
  text: "Foreign Entity",
});

export const IdentificationTypes = [
  { key: "1", value: "identity_card", text: "Identity Card (Passport)" },
  { key: "2", value: "driving_licence", text: "Driving License" },
  { key: "3", value: "social_insurance", text: "National Insurance" },
];

export const ForeignEntityTypes = [
  {
    key: "1",
    value: "Corporation",
    text: "Corporation",
  },
  { key: "2", value: "Disregarded entity", text: "Disregarded entity" },
  { key: "3", value: "Partnership", text: "Partnership" },
  { key: "4", value: "Simple trust", text: "Simple trust" },
  { key: "5", value: "Grantor trust", text: "Grantor trust" },
  { key: "6", value: "Complex trust", text: "Complex trust" },
  { key: "7", value: "Estate", text: "Estate" },
  { key: "8", value: "Government", text: "Government" },
  { key: "9", value: "Central Bank of Issue", text: "Central Bank of Issue" },
  {
    key: "10",
    value: "Tax-exempt organization",
    text: "Tax-exempt organization",
  },
  { key: "11", value: "Private foundation", text: "Private foundation" },
  {
    key: "12",
    value: "International organization",
    text: "International organization",
  },
];

export const UsTrustTaxIdTypes = [
  {
    key: "1",
    value: "ssn",
    text: "Social Security Number",
  },
  {
    key: "2",
    value: "ein",
    text: "Employer Identification Number",
  },
];

export const ForeignPersonTaxIdTypes = [
  {
    key: "1",
    value: "itin",
    text: "Individual Taxpayer Identification Number",
  },
  {
    key: "2",
    value: "ftin",
    text: "Foreign Tax Identification",
  },
  {
    key: "3",
    value: "ssn",
    text: "Social Security Number",
  },
];

export const ForeignEntityTaxIdTypes = [
  {
    key: "1",
    value: "ein",
    text: "Employer Identification Number",
  },
  {
    key: "2",
    value: "giin",
    text: "Global Intermediary Identification Number",
  },
  {
    key: "3",
    value: "ftin",
    text: "Foreign Tax Identification",
  },
];

export const ForeignTrustTaxIdTypes = [
  {
    key: "1",
    value: "ein",
    text: "Employer Identification Number",
  },
  {
    key: "2",
    value: "itin",
    text: "Individual Taxpayer Identification Number",
  },
  {
    key: "3",
    value: "ftin",
    text: "Foreign Tax Identification",
  },
];

export const JointTypes = [
  {
    key: "1",
    value: "JOINT_TENANTS_WITH_RIGHTS_OF_SURVIVORSHIP",
    text: "Joint Tentants With Rights of Survivorship",
  },
  {
    key: "2",
    value: "TENANTS_IN_COMMON",
    text: "Tenants In Common",
  },
  {
    key: "3",
    value: "COMMUNITY_PROPERTY",
    text: "Community Property",
  },
  {
    key: "4",
    value: "TENANTS_BY_ENTIRETY",
    text: "Tenants by Entirety",
  },
];

export const CompanyMarketSector = [
  {
    key: "1",
    value: "COMMUNICATION_SERVICES",
    text: "Communication services",
  },
  {
    key: "2",
    value: "CONSUMER_DISCRETIONARY",
    text: "Consumer discretionary",
  },
  { key: "3", value: "CONSUMER_STAPLES", text: "Consumer staples" },
  { key: "4", value: "ENERGY", text: "Energy" },
  { key: "5", value: "FINANCIALS", text: "Financials" },
  { key: "6", value: "HEALTHCARE", text: "Healthcare" },
  { key: "7", value: "INDUSTRIALS", text: "Industrials" },
  {
    key: "8",
    value: "INFORMATION_TECHNOLOGY",
    text: "Information Technology",
  },
  { key: "9", value: "MATERIALS", text: "Materials" },
  { key: "10", value: "REAL_ESTATE", text: "Real Estate" },
  { key: "11", value: "UTILITIES", text: "Utilities" },
];

export const SecurityTypes = [
  { key: "1", value: "COMMON_STOCK", text: "Common Stock" },
  { key: "2", value: "PREFERRED_STOCK", text: "Preferred Stock" },
  {
    key: "3",
    value: "CONVERTIBLE_PROMISSORY_NOTE",
    text: "Convertible Promissory Note",
  },
  { key: "4", value: "SAFE", text: "SAFE" },
  { key: "5", value: "SAFT", text: "SAFT" },
  { key: "6", value: "SAFE-T", text: "SAFE-T" },
  { key: "7", value: "LLC_MEMBERSHIP_UNITS", text: "LLC Membership Units" },
  { key: "8", value: "LP_MEMBERSHIP_UNITS", text: "LP Membership Units" },
  { key: "9", value: "CRYPTOCURENCY", text: "Cryptocurency" },
  { key: "10", value: "OTHER", text: "Other" },
];

export const RoundTypes = [
  { key: "1", value: "None", text: "None" },
  { key: "2", value: "Secondary", text: "Secondary" },
  { key: "3", value: "Pre-Seed", text: "Pre-Seed" },
  { key: "4", value: "Seed", text: "Seed" },
  { key: "5", value: "Series A", text: "Series A" },
  { key: "6", value: "Series B", text: "Series B" },
  { key: "7", value: "Series C", text: "Series C" },
];

export const Countries = [
  {
    key: "1",
    value: "United States of America",
    text: "United States of America",
  },
];

export const States = [
  {
    key: "1",
    value: "Alabama",
    text: "Alabama",
  },

  {
    key: "2",
    value: "Alaska",
    text: "Alaska",
  },

  {
    key: "3",
    value: "American Samoa",
    text: "American Samoa",
  },

  {
    key: "4",
    value: "Arizona",
    text: "Arizona",
  },

  {
    key: "5",
    value: "Arkansas",
    text: "Arkansas",
  },

  {
    key: "6",
    value: "California",
    text: "California",
  },

  {
    key: "7",
    value: "Colorado",
    text: "Colorado",
  },

  {
    key: "8",
    value: "Connecticut",
    text: "Connecticut",
  },

  {
    key: "9",
    value: "Delaware",
    text: "Delaware",
  },

  {
    key: "10",
    value: "District of Columbia",
    text: "District of Columbia",
  },

  {
    key: "11",
    value: "Federated States of Micronesia",
    text: "Federated States of Micronesia",
  },

  {
    key: "12",
    value: "Florida",
    text: "Florida",
  },

  {
    key: "13",
    value: "Georgia",
    text: "Georgia",
  },

  {
    key: "14",
    value: "Guam",
    text: "Guam",
  },

  {
    key: "15",
    value: "Hawaii",
    text: "Hawaii",
  },

  {
    key: "16",
    value: "Idaho",
    text: "Idaho",
  },

  {
    key: "17",
    value: "Illinois",
    text: "Illinois",
  },

  {
    key: "18",
    value: "Indiana",
    text: "Indiana",
  },

  {
    key: "19",
    value: "Iowa",
    text: "Iowa",
  },

  {
    key: "20",
    value: "Kansas",
    text: "Kansas",
  },

  {
    key: "21",
    value: "Kentucky",
    text: "Kentucky",
  },

  {
    key: "22",
    value: "Louisiana",
    text: "Louisiana",
  },

  {
    key: "23",
    value: "Maine",
    text: "Maine",
  },

  {
    key: "24",
    value: "Marshall Islands",
    text: "Marshall Islands",
  },

  {
    key: "25",
    value: "Maryland",
    text: "Maryland",
  },

  {
    key: "26",
    value: "Massachusetts",
    text: "Massachusetts",
  },

  {
    key: "27",
    value: "Michigan",
    text: "Michigan",
  },

  {
    key: "28",
    value: "Minnesota",
    text: "Minnesota",
  },

  {
    key: "29",
    value: "Mississippi",
    text: "Mississippi",
  },

  {
    key: "30",
    value: "Missouri",
    text: "Missouri",
  },

  {
    key: "31",
    value: "Montana",
    text: "Montana",
  },

  {
    key: "32",
    value: "Nebraska",
    text: "Nebraska",
  },

  {
    key: "33",
    value: "Nevada",
    text: "Nevada",
  },

  {
    key: "34",
    value: "New Hampshire",
    text: "New Hampshire",
  },

  {
    key: "35",
    value: "New Jersey",
    text: "New Jersey",
  },

  {
    key: "36",
    value: "New Mexico",
    text: "New Mexico",
  },

  {
    key: "37",
    value: "New York",
    text: "New York",
  },

  {
    key: "38",
    value: "North Carolina",
    text: "North Carolina",
  },

  {
    key: "39",
    value: "North Dakota",
    text: "North Dakota",
  },

  {
    key: "40",
    value: "Northern Mariana Islands",
    text: "Northern Mariana Islands",
  },

  {
    key: "41",
    value: "Ohio",
    text: "Ohio",
  },

  {
    key: "42",
    value: "Oklahoma",
    text: "Oklahoma",
  },

  {
    key: "43",
    value: "Oregon",
    text: "Oregon",
  },

  {
    key: "44",
    value: "Palau",
    text: "Palau",
  },

  {
    key: "45",
    value: "Pennsylvania",
    text: "Pennsylvania",
  },

  {
    key: "46",
    value: "Puerto Rico",
    text: "Puerto Rico",
  },

  {
    key: "47",
    value: "Rhode Island",
    text: "Rhode Island",
  },

  {
    key: "48",
    value: "South Carolina",
    text: "South Carolina",
  },

  {
    key: "49",
    value: "South Dakota",
    text: "South Dakota",
  },

  {
    key: "50",
    value: "Tennessee",
    text: "Tennessee",
  },

  {
    key: "51",
    value: "Texas",
    text: "Texas",
  },

  {
    key: "52",
    value: "Utah",
    text: "Utah",
  },

  {
    key: "53",
    value: "Vermont",
    text: "Vermont",
  },

  {
    key: "54",
    value: "Virgin Island",
    text: "Virgin Island",
  },

  {
    key: "55",
    value: "Virginia",
    text: "Virginia",
  },

  {
    key: "56",
    value: "Washington",
    text: "Washington",
  },

  {
    key: "57",
    value: "West Virginia",
    text: "West Virginia",
  },

  {
    key: "58",
    value: "Wisconsin",
    text: "Wisconsin",
  },

  {
    key: "59",
    value: "Wyoming",
    text: "Wyoming",
  },
];

export const Revenue = [
  { key: "1", value: "Pre-revenue", text: "Pre-revenue" },
  { key: "2", value: "$1-$50K Annually", text: "$1-$50K Annually" },
  { key: "3", value: "$50K-$200K Annually", text: "$50K-$200K Annually" },
  { key: "4", value: "$200K-$500K Annually", text: "$200K-$500K Annually" },
  { key: "5", value: "$500K-$1M Annually", text: "$500K-$1M Annually" },
  { key: "6", value: "$1M-$5M Annually", text: "$1M-$5M Annually" },
  { key: "7", value: "> $5M Annually", text: "> $5M Annually" },
];

export const NumberOfemplyees = [
  { key: "1", value: "0-1", text: "0-1" },
  { key: "2", value: "2-10", text: "2-10" },
  { key: "3", value: "11-50", text: "11-50" },
  { key: "4", value: "51-100", text: "51-100" },
  { key: "5", value: "101-200", text: "101-200" },
  { key: "6", value: "> 201", text: "> 201" },
];
