import { Profile } from "../../api/profilesApi";
import moment from "moment";
import { Entity } from "../../api/entitiesApi";
import { Accreditation } from "../../api/accreditationsApi";

export const is506cAccredited = (profile: Profile, allowPending = false) => {
  if (
    !profile ||
    !profile.accreditations ||
    profile.accreditations.length < 0
  ) {
    return false;
  }
  const validAccreditations = profile.accreditations
    .filter((acc) => {
      return (
        (allowPending
          ? ["PENDING", "CURRENT", "THIRD_PARTY_PENDING"]
          : ["CURRENT"]
        ).includes(acc.status) && acc.type === "506c"
      );
    })
    .sort((a, b) => {
      return moment.utc(a.createdAt).isBefore(moment.utc(b.createdAt)) ? 1 : 0;
    });
  return validAccreditations.length >= 1;
};

export const latestAccreditation = (accreditations: Accreditation[]) => {
  return accreditations
    .sort((a, b) => {
      return moment.utc(a.createdAt).isBefore(moment.utc(b.createdAt)) ? 1 : 0;
    })
    .pop();
};

export const is506cEntity = (entity: Entity) => {
  return !(
    !entity ||
    entity.regulationType !== "REGULATION_D" ||
    entity.regDExemption !== "c"
  );
};
