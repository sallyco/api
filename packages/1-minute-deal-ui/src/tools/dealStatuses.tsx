import { Deal } from "../api/dealsApi";

export enum DEAL_DISPLAYED_STATUSES {
  PENDING = "PENDING",
  DRAFT = "DRAFT",
  ONBOARDING = "ONBOARDING",
  OPEN = "OPEN",
  IN_CLOSING = "IN CLOSING",
  CLOSED = "CLOSED",
}

export function DealStatusText(deal: Deal) {
  if (
    deal?.status !== DEAL_DISPLAYED_STATUSES.CLOSED &&
    deal?.closes &&
    deal?.closes.length > 0
  ) {
    return DEAL_DISPLAYED_STATUSES.IN_CLOSING;
  }
  return deal?.status;
}
