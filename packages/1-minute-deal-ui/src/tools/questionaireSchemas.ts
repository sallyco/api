import { JSONSchema7 } from "json-schema";
export const applicationQuestionaire: Array<JSONSchema7> = [
  {
    title: "Application Questionaire - Part 1",
    type: "object",
    properties: {
      question1: {
        type: "string",
        title: "What is your company name?",
      },
      question1_05: {
        type: "string",
        title: "What is your company's tagline?",
      },
      question1_1: {
        type: "string",
        title: "What is your website address?",
      },
      question1_2: {
        type: "string",
        title: "Where are you located?",
      },
      question1_5: {
        type: "number",
        title: "What year was your company incorporated?",
      },
      question1_7: {
        type: "string",
        title:
          "Please describe to us what your company does in 140 characters or less!	",
      },
      question2: {
        type: "string",
        title:
          "Please provide a 1 minute video demo of your product. Please include the URL so we can view it with any usercode / passwords that may be required.",
      },
      question4: {
        type: "string",
        title: "How is your current round structured?",
        enum: [
          "Not currently raising",
          "Pre-Seed",
          "Seed",
          "Series A",
          "Series B",
          "Series C",
          "We are open to any structure",
        ],
        default: "Pre-Seed",
      },
      question4_5: {
        type: "string",
        title: "What instrument are you raising on?",
        enum: [
          "Not currently raising",
          "Convertible Note",
          "SAFE",
          "Common",
          "Series Seed Preferred",
          "Series A Preferred",
          "Series B Preferred",
          "Series C Preferred",
        ],
        default: "Convertible Note",
      },
      question4_9: {
        type: "number",
        title:
          "How much funding are you raising in your current round? (If this does not apply to your company, please enter 0)",
      },
      question5_1: {
        type: "number",
        title:
          "How much funding of your current round have you raised to date? (If this does not apply to your company, please enter 0)",
      },
      question5_5: {
        type: "number",
        title:
          "If convertible note or SAFE, what is the valuation cap and discount? (If this does not apply to your company, please enter 0)",
      },
      question6: {
        type: "number",
        title:
          "If common or preferred, what is the pre-money valuation of your current round?	(If this does not apply to your company, please enter 0)",
      },
      question7: {
        type: "string",
        title: "List major investors in the current round.",
      },
      question7_3: {
        type: "array",
        title: "Please identify all prior closed rounds.",
        items: {
          type: "string",
          enum: [
            "None",
            "Convertible Note",
            "SAFE",
            "Common",
            "Series Seed Preferred",
            "Series A Preferred",
            "Series B Preferred",
            "Series C Preferred",
          ],
        },
        uniqueItems: true,
      },
      question7_4: {
        type: "string",
        title: "What is your estimated closing date?",
      },
      question7_5: {
        type: "string",
        title: "Please describe your use of funds in 1 sentence.",
      },
      question7_7: {
        type: "string",
        title: "Please select your anticipated timeframe to exit.",
        enum: [
          "1 to 3 years",
          "3 to 5 years",
          "5 to 7 years",
          "more than 7 years.",
        ],
        default: "1 to 3 years",
      },
      question7_9: {
        type: "string",
        title:
          "What is your management status? (Complete for each Founder and CEO: 1) Full-time or Part-time; 2) CEO, Founder or CEO & Founder; 3) Enter All that Apply: Business Experience, Industry Experience, Sales Experience, Entrepreneurial Experience; 4) No Prior Successful Exit or Prior Successful Exit [List name of seller, purchaser and exit multiple])",
      },
      question8_1: {
        type: "string",
        title: "What is your board status?",
        enum: [
          "We have an active fully constituted board of directors or advisors with individuals who are recognized in our industry",
          "We have 1-2 experienced board members",
          "No board and/or no recognized industry leaders and/or not active.",
        ],
      },
      question8_3: {
        type: "number",
        title: "Number of full-time employees.",
      },
      question8_4: {
        type: "string",
        title: "Product description in 1 sentence.",
      },
      question9: {
        type: "string",
        title: "What is your primary claimed competitive advantage(s)?",
      },
      question10: {
        type: "array",
        title: "What is your IP status?",
        items: {
          type: "string",
          enum: [
            "Patent",
            "Patent Pending",
            "Exclusive License",
            "Non-exclusive License",
            "Trade Secret",
            "Copyright",
          ],
        },
        uniqueItems: true,
      },
      question10_1: {
        type: "string",
        title:
          "If you have a patent, patent pending and/or copyright, please enter the filing number(s)",
      },
      question11: {
        type: "array",
        title: "What is your development status?",
        items: {
          type: "string",
          enum: [
            "Feasability studies complete",
            "ex vivo animal studies complete",
            "in vivo animal studies complete",
            "pre-clinical trials complete",
            "phase 1 trials complete",
            "phase 2 trials complete",
            "no clinical trials required to go to market",
          ],
        },
        uniqueItems: true,
      },
      question12: {
        type: "string",
        title: "What is your regulatory status?",
        enum: [
          "No regulatory requirements",
          "510(k) submission required but not complete (no predicate)",
          "510(k) submission required but not complete (with predicate)",
          "510(k) submission required and complete",
          "Pre-market approval application required but not complete",
          "Pre-market approval application required and complete.",
        ],
      },
      question13: {
        type: "string",
        title: "What is your manufacturing status?",
        enum: [
          "Manufacturer not yet identified",
          "Manufacturer identifed",
          "Signed agreement with manfacturer",
          "Signed agreement with manfacturer that is producing prototype or a pilot run",
          "Signed agreement with manfacturer that is producing commercial product runs.",
        ],
        default: "Manufacturer not yet identified",
      },
      question14: {
        type: "string",
        title: "What is your traction to date?",
        enum: ["None", "KOL support", "commercial launch"],
        default: "None",
      },
      question15: {
        type: "string",
        title: "What is the status of your exit strategy?",
        enum: [
          "None",
          "Initial positive conversations with strategic(s)",
          "Significant strategic interest with signed agreements in place.",
        ],
        default: "None",
      },
    },
    required: [
      "question1",
      "question1_05",
      "question1_1",
      "question1_2",
      "question1_5",
      "question1_7",
      "question2",
      "question4",
      "question4_5",
      "question4_9",
      "question5_1",
      "question5_5",
      "question6",
      "question7",
      "question7_3",
      "question7_5",
      "question7_7",
      "question7_9",
      "question8_1",
      "question8_3",
      "question8_4",
      "question9",
      "question10",
      "question11",
      "question12",
      "question13",
      "question14",
      "question15",
    ],
    additionalProperties: false,
  },
  {
    title: "Application Questionaire - Part 2",
    type: "object",
    properties: {
      question2_1: {
        type: "string",
        title:
          "Describe the problem your company is solving.  Include market numbers and statistics where possible.",
      },
      question2_2: {
        type: "string",
        title:
          "Please describe to us what your company does in further detail up to 500 words.",
      },
      question2_3: {
        type: "string",
        title:
          "Please describe your competitors.  Include name of company and website link, name of product, description of competitive advantages and market share where possible.",
      },
      question2_4: {
        type: "string",
        title: "Please describe your main competitive advantages.",
      },
      question2_5: {
        type: "string",
        title:
          "Please describe your Intellectual Property.  Include filing numbers and details of any licensing agreement (including, exclusivity, field of use, territory, royalties, equity interests, etc.)",
      },
      question2_6: {
        type: "string",
        title:
          "Please describe your total addressable market, your target market, market shares of competitors and your anticipated market share.  Include market numbers and statistics where possible.",
      },
      question2_7: {
        type: "string",
        title:
          "Please describe your go-to-market strategy.  Include pro forma projections, sales forecasts, and pricing information where possible.",
      },
      question2_8: {
        type: "string",
        title:
          "Please describe your go-to-market strategy.  Include pro forma projections, sales forecasts, and pricing information where possible.",
      },
      question2_9: {
        type: "string",
        title:
          "Please list existing reimbursement codes, if any, and any new ones required.",
      },
      question2_10: {
        type: "string",
        title:
          "Please describe any feedback from your market.  Include names of strategics where possible.",
      },
      question2_11: {
        type: "string",
        title:
          "Please describe comparable exits including company names, devices, exit values and exit multiples where possible.",
      },
      question2_12: {
        type: "string",
        title:
          "Please describe your traction to date.  Includes KOL support, survey feedback, clinical trial data, strategic conversations, sales, etc.",
      },
      question2_13: {
        type: "string",
        title:
          "Please describe your development plan (milestones), timeline for completion, and use of funds to accomplish that plan and timeline.",
      },
      question2_14: {
        type: "string",
        title:
          "Please describe your regulatory strategy including any predicate devices, clinical trials, materials clearance, and timeline.",
      },
      question2_15: {
        type: "string",
        title:
          "Please list your management team, board, partners, manufacturers and vendors including links to biographies and websites.",
      },
    },
    required: [
      "question2_1",
      "question2_2",
      "question2_3",
      "question2_4",
      "question2_5",
      "question2_6",
      "question2_7",
      "question2_8",
      "question2_9",
      "question2_10",
      "question2_11",
      "question2_12",
      "question2_13",
      "question2_14",
      "question2_15",
    ],
    additionalProperties: false,
  },
  {
    title:
      "IF YOU'RE INTERESTED IN EXPLORING ADDITIONAL SUPPORT, PLEASE RESPOND BELOW",
    type: "object",
    properties: {
      question2_1: {
        type: "string",
        title:
          "Please share with us any functional areas (legal, product, organization, investment, marketing, operations, sales, etc.) that you would like further mentorship on.",
      },
    },
    additionalProperties: false,
  },
  {
    title: "Score Card Questionaire",
    type: "object",
    properties: {
      question1: {
        type: "string",
        title:
          "How many startups have you founded BEFORE your current Startup?",
        enum: ["0", "1", "2", "3", "> 3"],
      },
      question2: {
        type: "string",
        title: "How many profitable exits have you had?",
        enum: ["0", "1", "2", "3", "> 3"],
      },
      question3: {
        type: "string",
        title:
          "How many domain/industries would you classify yourself as an expert?",
        enum: ["0", "1", "2", "3", ">3"],
      },
      question4: {
        type: "string",
        title:
          "How many years of management experience do you have? (ie. leading a team)",
        enum: ["0", "1-3", "4-7", "8-15", "> 15"],
      },
      question4_2: {
        type: "string",
        title:
          "Do you have both a Business Co-Founder along with a Technical Co-Founder?",
        enum: ["Yes", "No"],
      },
      question5: {
        type: "string",
        title:
          "What is your Service Obtainable Market (SOM) market size? Please upload evidence if available.",
        enum: ["I dont know", "<$250M", "$251M - $1B", "$1B - $3B", "> $3B"],
      },
      question6: {
        type: "string",
        title:
          "What is your primary method of getting your current product to market",
        enum: [
          "Enterprise Sales: > 6 months",
          "Direct Sales: 3 - 6 months",
          "Direct Sales: 0 - 2 months or E-marketing/Social with moderate costs to acquire",
          "Extensive Partner/Channel network in place and working",
          "E-Marketing / Viral approach, no touch, no cost to acquire",
        ],
      },
      question7: {
        type: "string",
        title: "What is the maturity level of your product?",
        enum: [
          "Only a concept/design at this stage",
          "Inital prototype thats working",
          "Minimal Viable Product (MVP) with > 50 engaged Users",
          "Multiple Products of which several are being monetized",
          "Multiple solutions that have achieved Product-Market Fit (PMF) and scaling",
        ],
      },
      question8: {
        type: "string",
        title:
          "How impactful is the value-proposition of your solution/product?",
        enum: [
          "Unknown at this time",
          "1 - 15% cost savings or increased revenue  to your company",
          "16% - 30% cost savings or increased revenue to your company",
          "31-45% cost savings or increased revenue  to your company",
          "> 45% cost savings or increased revenue  to your company",
        ],
      },
      question9: {
        type: "string",
        title: "What is your company’s market differentiator?",
        enum: [
          "Cost Leadership",
          "Better Function and Features / Ease of Use",
          "Large barriers to Entry",
          "Better Distribution Strategy",
          "Higher Value Proposition",
        ],
      },
      question10_a: {
        type: "string",
        title:
          "How many competitors do you have? Please upload your Competitive Matrix or other Supporting documents.",
        enum: ["Unknown at this time", ">15", "11 - 15", "6 - 10", "< 5"],
      },
      question11_a: {
        type: "string",
        title: "How well funded are your Competitors?",
        enum: [
          "Unknown at this time",
          "Self-funded by Founders",
          "Seed Round < $1M",
          "A-Series round $1 - $5M",
          "A-Series or later round > $5M",
        ],
      },
      question12: {
        type: "string",
        title: "How many customers does your largest competitor have?",
        enum: ["Unknown at this time", "1 - 10", "11 - 25", "26 -100", ">100"],
      },
      question13: {
        type: "string",
        title:
          "How many months does it take to recover your customer acquisition costs (CAC)?",
        enum: [
          "Unknown at this time",
          "> 24 months",
          "12 - 24 months",
          "6-12 months",
          "< 6 months",
        ],
      },
      question14: {
        type: "string",
        title:
          "How complete is your business model canvas? Please upload your latest Canvas and any experiments",
        enum: [
          "We have not started",
          "Business model canvas completed - no experiments completed",
          "Canvas completed along with at least 10 validated experiments",
          "Canvas completed with 25 experiments validated with evidence",
          "Canvas completed with more than 50+ experiments validated with evidence",
        ],
      },
      question14_2: {
        type: "string",
        title:
          "How many current distribution channels do you have in place and working at this time?",
        enum: ["0", "1 - 5", "6 - 10", "11 - 15", "> 15"],
      },
      question15: {
        type: "string",
        title: "What is your Monthly Recurring Revenue (MRR)?",
        enum: ["0.00  $", "< $1K", "$1K - $5K", "$5K - $25K", "> $25K"],
      },
      question16: {
        type: "string",
        title: "How many paying customers do you have?",
        enum: ["0", "1 - 10", "11 - 25", "26 - 100", "> 100"],
      },
      question17: {
        type: "string",
        title:
          "How many months of runway do you have? (ie. how long until you run out of money?)",
        enum: [
          "0 - we are out of money now!",
          "1 - 3 months",
          "4 - 12 months",
          "13 - 18 months",
          "> 18 months",
        ],
      },
      question18: {
        type: "string",
        title: "How many Monthly Active Users (MAU) do you have?",
        enum: ["Unknown at this time", "< 25", "25 - 100", "101-250", "> 250"],
      },
      question19: {
        type: "string",
        title:
          "What is your month-over-month (MoM) growth rate of your MRR in the past 6 months?",
        enum: ["0", "1 - 10%", "11 - 20%", "21 - 25%", "> 25%"],
      },
      question20: {
        type: "string",
        title: "How much money have you raised in the last 12 months?",
        enum: ["0", "< $100K", "$101 - $250K", "$251K - $750K", "> $750K"],
      },
      question21: {
        type: "string",
        title:
          "How much investment do you have soft circled for your next round?",
        enum: [
          "$0 or I am not raising a round in the next 12 months",
          "< $100K",
          "$101K - $250K",
          "$251K - $500K",
          "> $500K",
        ],
      },
      question22: {
        type: "string",
        title:
          "What is your attrition rate over the trailing twelve months (TTM)?",
        enum: [
          "We do not have Customers at this time.",
          "> 12%",
          "8% - 12%",
          "3% - 7%",
          "< 3%",
        ],
      },
    },
    required: [
      "question1",
      "question2",
      "question3",
      "question4",
      "question4_2",
      "question5",
      "question6",
      "question7",
      "question8",
      "question9",
      "question10_a",
      "question11_a",
      "question12",
      "question13",
      "question14",
      "question14_2",
      "question15",
      "question16",
      "question17",
      "question18",
      "question19",
      "question20",
      "question21",
      "question22",
    ],
    additionalProperties: false,
  },
];
