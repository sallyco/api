// inMemoryJwt.js
const actionCounter = () => {
  let counts = 0;
  let checkId = null;

  const getCount = () => {
    return counts;
  };

  const increaseCount = () => {
    counts += 1;
  };

  const resetCount = () => {
    counts = 0;
  };

  const setCheckId = (id) => {
    if (checkId) {
      window.clearInterval(checkId);
    }
    checkId = id;
  };

  return {
    getCount,
    increaseCount,
    resetCount,
    setCheckId,
  };
};

export default actionCounter();
