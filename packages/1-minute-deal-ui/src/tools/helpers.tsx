import axios from "axios";
import { Profile } from "../api/profilesApi";
import * as parseCookie from "../api/parseCookie";

export const startsWithSearch = (options, query) => {
  return options.filter((opt) =>
    opt.text.toUpperCase().startsWith(query.toUpperCase())
  );
};

export const currencyFormat = (number) => {
  if (typeof number === "string") {
    number = parseFloat(number.replace("$", ""));
  }
  return "$" + number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

export class DocumentHelpers {
  static async shouldUseW9(profile: Profile) {
    const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
    const url = baseUrl + "/decisions/should-use-w9/evaluate";
    const token = parseCookie.getToken("__session");
    const payload = {
      variables: {
        registrationType: {
          value: profile.taxDetails.registrationType,
          type: "String",
        },
        taxIdentificationType: {
          value: profile.taxDetails.taxIdentification?.type,
          type: "String",
        },
        typeOfEntity: { value: profile?.typeOfEntity, type: "String" },
      },
    };
    try {
      const res = await axios.post(url, payload, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        timeout: 2,
      });
      return res.data.shouldUseW9.value;
    } catch {
      const isIndividual = profile.taxDetails.registrationType === "INDIVIDUAL";

      const isForeign = (() => {
        if (isIndividual) {
          return profile.taxDetails.taxIdentification?.type !== "ssn";
        }
        return (
          profile?.taxDetails?.taxIdentification?.type === "ein" &&
          profile?.typeOfEntity === "FOREIGN_ENTITY"
        );
      })();

      if (isIndividual && isForeign) {
        return false;
      }
      return true;
    }
  }
}

export function toTitleCase(str) {
  if (!str) return "";
  return str
    .split("_")
    .join(" ")
    .replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}
