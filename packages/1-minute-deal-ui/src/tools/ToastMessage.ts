import { toast } from "react-toastify";

export const ToastError = (err) => {
  if (err) {
    toast.error(`${err.data.error.statusCode} - ${err.data.error.message}`);
  }
  return;
};
