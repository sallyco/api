import * as parseCookie from "../api/parseCookie";
import axios from "axios";
import { toast } from "react-toastify";
import { useState } from "react";
import { Icon, Popup } from "semantic-ui-react";
const API_ROOT = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
export const DownloadFile = async (fileId: string, fileName?: string) => {
  const token = parseCookie.getToken("__session");
  try {
    if (!fileName) {
      const { data: fileData } = await axios({
        url: `${API_ROOT}/files/${fileId}`,
        method: "GET",
        responseType: "json",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      fileName = fileData.name;
    }
    const response = await axios({
      url:
        process.env.API_URL ??
        process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return;
    }
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    if (fileName) {
      link.setAttribute("download", fileName);
    }
    document.body.appendChild(link);
    link.click();
  } catch {
    toast.error("Download is unavailable at this time for this document");
  }
};

export const DownloadGeneratorFile = async (
  gen_url: string,
  fileName: string
) => {
  const token = parseCookie.getToken("__session");
  try {
    const response = await axios({
      url: process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL + gen_url,
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return;
    }
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    if (fileName) {
      link.setAttribute("download", fileName);
    }
    document.body.appendChild(link);
    link.click();
  } catch {
    toast.error("Download is unavailable at this time for this document");
  }
};

export const DownloadMassFiles = async (files) => {
  return Promise.all(files.map((file) => DownloadFile(file)));
};

/**
 * @param handlePDF: function to run if the file is a PDF
 */
export function handlePDFOrDownloadFile(
  file: { id: string; name: string; type: string },
  handlePDF: Function
) {
  if (file?.type === "application/pdf") {
    handlePDF();
  } else {
    DownloadFile(file?.id, file?.name);
  }
}
