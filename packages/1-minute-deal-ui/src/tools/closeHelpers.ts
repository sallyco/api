import { Entity } from "../api/entitiesApi";

export const isSeriesEntityOfGlassboardMasterII = (entity: Entity) => {
  return entity.name.indexOf("Glassboard Master II") > -1;
};
