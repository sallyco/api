import React from "react";

import { Icon } from "semantic-ui-react";
import { File } from "../api/filesApi";

export const fileIcons = (file: File) => {
  let type = file.type;
  const nameTyping = file.name.split(".").pop();
  if (!type || type === "application/octet-stream") {
    type = nameTyping;
  }
  switch (type) {
    case "pdf":
    case "application/pdf":
      return (
        <Icon
          style={{ color: "red" }}
          name="file pdf"
          size="large"
          className={"documents-tab-icon documents-tab-document-icon-height"}
        />
      );
    case "docx":
    case "pages":
    case "application/docx":
      return (
        <Icon
          style={{ color: "blue" }}
          name="file word"
          size="large"
          className={"documents-tab-icon documents-tab-document-icon-height"}
        />
      );
    case "pptx":
    case "application/pptx":
      return (
        <Icon
          style={{ color: "orange" }}
          name="file powerpoint"
          size="large"
          className={"documents-tab-icon documents-tab-document-icon-height"}
        />
      );
    default:
      return (
        <Icon
          name="file"
          size="large"
          className={"documents-tab-icon documents-tab-document-icon-height"}
        />
      );
  }
};
