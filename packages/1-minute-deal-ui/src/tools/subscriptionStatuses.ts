import { Subscription } from "../api/subscriptionsApi";
import { Close } from "../api/closesApi";

/**
 * IMPORTANT, SUBSCRIPTION_DISPLAYED_STATUSES are ordered as they should display
 * If new statuses are introduced, or shifted, ORDER will be affected
 */
export const SUBSCRIPTION_DISPLAYED_STATUSES = {
  INVITED: "INVITED",
  PENDING: "PENDING",
  NEEDS_SIGNING: "NEEDS SIGNING",
  COMMITTED: "COMMITTED",
  NEEDS_FUNDING: "NEEDS FUNDING",
  FUNDING_SENT: "FUNDING SENT",
  FUNDS_IN_TRANSIT_MANUAL: "FUNDS IN TRANSIT - MANUAL",
  COMPLETED: "COMPLETED",
  COMPLETED_FUNDS_RECEIVED: "COMPLETED - FUNDS RECEIVED",
  CLOSE_PENDING: "CLOSE PENDING",
  CLOSED: "CLOSED",
  CANCELLED: "CANCELLED",
  TRANSFERRED: "TRANSFERRED",
  REFUNDED: "REFUNDED",
  CUSTOM: "CUSTOM",
};

export const DISPLAY_ORDER = Object.values(SUBSCRIPTION_DISPLAYED_STATUSES);
export function getStatusOrder(displayedStatus) {
  return DISPLAY_ORDER.indexOf(displayedStatus);
}

export const DB_STATUSES = {
  INVITED: "INVITED",
  PENDING: "PENDING",
  COMMITTED: "COMMITTED",
  COMPLETED: "COMPLETED",
  FUNDS_IN_TRANSIT_MANUAL: "FUNDS_IN_TRANSIT-MANUAL",
  COMPLETED_FUNDS_RECEIVED: "COMPLETED_FUNDS_RECEIVED",
};

export const RECEIVED_STATUSES = [
  SUBSCRIPTION_DISPLAYED_STATUSES.COMPLETED,
  SUBSCRIPTION_DISPLAYED_STATUSES.CLOSED,
  SUBSCRIPTION_DISPLAYED_STATUSES.TRANSFERRED,
  SUBSCRIPTION_DISPLAYED_STATUSES.COMPLETED_FUNDS_RECEIVED,
];
export function isReceivedStatus(subscription: Subscription): boolean {
  return RECEIVED_STATUSES.includes(SubscriptionStatusText(subscription));
}

const {
  INVITED,
  PENDING,
  NEEDS_SIGNING,
  COMMITTED,
  NEEDS_FUNDING,
  FUNDING_SENT,
  FUNDS_IN_TRANSIT_MANUAL,
  COMPLETED,
  COMPLETED_FUNDS_RECEIVED,
  CLOSE_PENDING,
  CLOSED,
  CANCELLED,
  TRANSFERRED,
  CUSTOM,
  REFUNDED,
} = SUBSCRIPTION_DISPLAYED_STATUSES;

const subscriptionStatusColors: {
  [key: string]:
    | "teal"
    | "red"
    | "orange"
    | "yellow"
    | "olive"
    | "green"
    | "blue"
    | "violet"
    | "purple"
    | "pink"
    | "brown"
    | "grey"
    | "black"
    | undefined;
} = {
  [INVITED]: "red",
  [PENDING]: "orange",
  [NEEDS_SIGNING]: "orange",
  [COMMITTED]: "orange",
  [NEEDS_FUNDING]: "orange",
  [FUNDING_SENT]: "olive",
  [FUNDS_IN_TRANSIT_MANUAL]: "olive",
  [COMPLETED]: "green",
  [COMPLETED_FUNDS_RECEIVED]: "green",
  [CLOSE_PENDING]: "olive",
  [CLOSED]: "grey",
  [CANCELLED]: "grey",
  [TRANSFERRED]: "grey",
  [CUSTOM]: "yellow",
};

export const amountChangeableStatuses = [
  COMMITTED,
  NEEDS_FUNDING,
  NEEDS_SIGNING,
  COMPLETED,
  FUNDS_IN_TRANSIT_MANUAL,
];

export const deleteableStatuses = [
  INVITED,
  PENDING,
  NEEDS_SIGNING,
  COMMITTED,
  NEEDS_FUNDING,
  REFUNDED,
];

export const markableCompleteStatuses = [
  COMMITTED,
  NEEDS_FUNDING,
  FUNDING_SENT,
  FUNDS_IN_TRANSIT_MANUAL,
];

export const reinvitableStatuses = [CANCELLED, REFUNDED];

export const markableCommittedStatuses = [
  FUNDS_IN_TRANSIT_MANUAL,
  COMPLETED,
  FUNDING_SENT,
];

export interface SubscriptionStatusInput {
  status: string;
  accountTransaction?: { id?: string };
  close?: Close;
}

export const SubscriptionStatusColors = (status: string) =>
  subscriptionStatusColors[status];

export function SubscriptionStatusText(subscription: Subscription): string {
  if ([DB_STATUSES.INVITED].includes(subscription.status)) {
    return subscription.status;
  } else if (subscription.status === DB_STATUSES.COMPLETED) {
    if (subscription?.transactions) {
      const sentTransactions = subscription.transactions.filter(
        (transaction) => transaction.direction === "DEBIT"
      );
      if (
        sentTransactions.filter(
          (transaction) => transaction.bankAccountTransactionId
        ).length === sentTransactions.length &&
        sentTransactions.length > 0
      ) {
        if (!subscription?.close) {
          return COMPLETED_FUNDS_RECEIVED;
        }

        return !subscription.close?.fundManagerSigned ? CLOSE_PENDING : CLOSED;
      }
    }
    if (!subscription?.close) {
      return COMPLETED;
    }

    return !subscription.close?.fundManagerSigned ? CLOSE_PENDING : CLOSED;
  } else if (subscription.status === DB_STATUSES.PENDING) {
    return NEEDS_SIGNING;
  } else if (subscription.status === DB_STATUSES.COMMITTED) {
    if (!subscription?.transactionIds) {
      return NEEDS_FUNDING;
    } else {
      return FUNDING_SENT;
    }
  } else if (subscription.status === DB_STATUSES.COMPLETED_FUNDS_RECEIVED) {
    return COMPLETED_FUNDS_RECEIVED;
  } else {
    return subscription.status.replace(/[_]/gi, " ").replace(/[-]/gi, " - ");
  }
}

/**
 *
 * @param subscription
 * @returns true if the derived displayed status is allowed to be deleted or not
 */
export function canDeleteSubscription(subscription: Subscription): boolean {
  return deleteableStatuses.includes(SubscriptionStatusText(subscription));
}

/**
 *
 * @param subscription
 * @returns true if the derived displayed status is allowed to have the amount updated
 */
export function canChangeSubscriptionAmount(
  subscription: Subscription
): boolean {
  return amountChangeableStatuses.includes(
    SubscriptionStatusText(subscription)
  );
}

/**
 *
 * @param subscription
 * @returns true if the derived displayed status is allowed to be deleted or not
 */
export function canMarkCompleteSubscription(
  subscription: Subscription
): boolean {
  return markableCompleteStatuses.includes(
    SubscriptionStatusText(subscription)
  );
}

/**
 *
 * @param subscription
 * @returns true if can re-invite
 */
export function canReinviteSubscription(subscription: Subscription): boolean {
  return reinvitableStatuses.includes(SubscriptionStatusText(subscription));
}

/**
 *
 * @param subscription
 * @returns true if the derived displayed status is allowed to be marked COMMITTED
 */
export function canMarkCommittedSubscription(
  subscription: Subscription
): boolean {
  return markableCommittedStatuses.includes(
    SubscriptionStatusText(subscription)
  );
}

/**
 *
 * @param subscription
 * @returns true if the subscription is in a state to allow refunds
 */
export function canRefundSubscription(subscription: Subscription): boolean {
  return (
    subscription.status === "COMPLETED" &&
    !subscription.reverseTransactionId &&
    !subscription.close
  );
}

export function canNudgeSubscription(subscription: Subscription): boolean {
  return ![
    "COMPLETED",
    "CLOSED",
    "FUNDING SENT",
    "REFUNDED",
    "CANCELLED",
  ].includes(subscription.status);
}
