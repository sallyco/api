import data from "./mockData.json";

export function getDeals() {
    return data.deals;
}

export function getProfiles() {
    return data.profiles;
}

export function getInvites() {
    return data.invites;
}

export function getInviteById(inviteId) {
    return data.invites.find((invite) => invite.id === inviteId)
}
