import { DEAL_DISPLAYED_STATUSES } from "./dealStatuses";

const dealStatusColors: {
  [key: string]:
    | "teal"
    | "red"
    | "orange"
    | "yellow"
    | "olive"
    | "green"
    | "blue"
    | "violet"
    | "purple"
    | "pink"
    | "brown"
    | "grey"
    | "black"
    | undefined;
} = {
  [DEAL_DISPLAYED_STATUSES.PENDING]: "olive",
  [DEAL_DISPLAYED_STATUSES.DRAFT]: "olive",
  [DEAL_DISPLAYED_STATUSES.ONBOARDING]: "orange",
  [DEAL_DISPLAYED_STATUSES.OPEN]: "green",
  [DEAL_DISPLAYED_STATUSES.IN_CLOSING]: "blue",
  [DEAL_DISPLAYED_STATUSES.CLOSED]: "black",
};

export const DealStatusColors = (status: string | DEAL_DISPLAYED_STATUSES) =>
  dealStatusColors[status];

const subscriptionStatusColors: {
  [key: string]:
    | "teal"
    | "red"
    | "orange"
    | "yellow"
    | "olive"
    | "green"
    | "blue"
    | "violet"
    | "purple"
    | "pink"
    | "brown"
    | "grey"
    | "black"
    | undefined;
} = {
  INVITED: "green",
  PENDING: "olive",
  COMMITTED: "orange",
  "FUNDS_IN_TRANSIT-MANUAL": "teal",
  COMPLETED: "blue",
  CLOSED: "grey",
  CANCELLED: "grey",
  TRANSFERRED: "grey",
};

export const SubscriptionStatusColors = (status: string) =>
  subscriptionStatusColors[status];
