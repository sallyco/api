import * as Sentry from "@sentry/react";

interface PlaidLinkError {
  error_code: string;
  error_type: string;
  display_message: string;
  error_message: string;
}

export const NO_LINK_ERROR = "NO_ERROR";

/**
 *
 * @param error can be null, otherwise it will be PlaidLinkError
 * @param metaData extra data about the user's session in Plaid
 * @returns a context object ready for Sentry
 */
export const buildPlaidSentryContext = (
  error: PlaidLinkError,
  metaData: any
) => {
  /**
   * Create tags for the event
   * https://docs.sentry.io/platforms/javascript/guides/react/enriching-events/tags/
   */
  return {
    tags: {
      "plaid.error_code": error?.error_code ?? NO_LINK_ERROR,
      "plaid.error_type": error?.error_type ?? NO_LINK_ERROR,
    },
    extra: {
      plaidDisplayMessage: error?.display_message ?? "",
      plaidErrorMessage: error?.error_message ?? "",
      plaidMetaData: metaData,
    },
  };
};

export const logPlaidLinkError = (
  error: PlaidLinkError,
  metaData: any
): string => {
  const context = buildPlaidSentryContext(error, metaData);
  const sentryEventId = Sentry.captureMessage(
    "Plaid connection error",
    context
  );

  return sentryEventId;
};
