export const ERROR_TYPES = {
  MFA_NOT_SUPPORTED: "MFA_NOT_SUPPORTED",
  NO_LINK_ERROR: "NO_ERROR",
};

// Using arrays if the message needs multiple sections
export const messagesByType = {
  [ERROR_TYPES.MFA_NOT_SUPPORTED]: [
    `Plaid supports multi-factor authentication for the majority of their supported banks. However, the type of MFA used by some banks, such as Wells Fargo or Capital One, may have intermittent problems connecting through Plaid. In these cases we recommend modifying your MFA to text or email verification.`,
    `While not recommended, you may be able to successfully connect your account using Plaid by temporarily disabling MFA.  After a successful connection is made MFA can then be re-enabled.`,
  ],
};

export function getErrorMessagesByType(type): string[] {
  return messagesByType[type] ?? [""];
}
