import { StringSchema } from "yup";
import * as Yup from "yup";
import { getIn } from "formik";

export const isValidZip = (
  schema: StringSchema,
  validationErrorMessage = "ZIP code format is incorrect"
) => schema.matches(/^\d{5}(?:[-\s]\d{4})?$/, validationErrorMessage);

export const TaxIdValidationSchema = (
  validationErrorMessage = "Tax ID is not in the correct format"
) => {
  return Yup.string().matches(
    /^([0-9]{9}|[0-9]{3}-[0-9]{2}-[0-9]{4}|XXX-XX-XXXX)$/,
    validationErrorMessage
  );
};

export const regexSkipDigitAndDecimals = /[^0-9,.]+/g;

/**
 *
 * @param amountString string that has digits 0-9 in it
 * @returns number - only returning digits in the string
 * @warning Take care...  any non-digit characters will be removed
 * 123abc456 will return 123456
 */
export function getDigitsFromString(amountString: string): number {
  // Catch runtime errors
  if (typeof amountString === "number") {
    return amountString;
  }
  if (typeof amountString !== "string") return 0;

  return Number((amountString ? amountString : "0").replace(/\D+/g, ""));
}

export const partialFormikValidation = (props, properties: string[]) => {
  for (const property of properties) {
    if (getIn(props.errors, property)) {
      return false;
    }
  }
  return true;
};
