import * as Yup from "yup";

export function getTargetRaiseAmountYup({
  max,
  required = true,
}: {
  max?: number;
  required?: boolean;
}) {
  let targetRaiseYup = Yup.number().transform(function (value, originalvalue) {
    return Number(originalvalue.replace(/\D+/g, ""));
  });

  if (required) {
    targetRaiseYup = targetRaiseYup.required();
  }

  if (max > 0) {
    targetRaiseYup = targetRaiseYup.max(
      max,
      "This amount exceeds your per deal target raise limit"
    );
  }

  return targetRaiseYup;
}
