import React, { useContext, useState, useEffect } from "react";
import { useSelector } from "react-redux";
import * as Yup from "yup";
import { RootState } from "../rootReducer";

import {
  Segment,
  Form,
  Button,
  Header,
  Divider,
  Popup,
  Icon,
} from "semantic-ui-react";
import { Field, Formik } from "formik";
import { TenantContext } from "../contexts";
import { fetchCompanyById } from "../slices/companiesSlice";
import { MaskedFormField, FormField } from "./common/FormField";
import { Prompt } from "react-router-dom";
import { getCurrentSeriesName } from "../api/entitiesApi";
import {
  FeatureFlag,
  useFeatureFlag,
} from "../components/featureflags/FeatureFlags";
import SubformDate, {
  validationSchema as DateValidationSchema,
  defaultValues as DateDefaultValues,
} from "./subforms/SubformDate";
import moment from "moment";
import QuillField from "./common/QuillField";

import SubformManagementFee, {
  validationSchema as FeeValidationSchema,
  testValidFeeAmount,
} from "./subforms/SubformManagementFee";
import { getTargetRaiseAmountYup } from "./validation/dealValidationHelpers";
import SubformRegDExemption, {
  validationSchema as SubformRegDExemptionValidation,
  defaultValues as SubformRegDExemptionDefaults,
} from "./subforms/SubformRegDExemption";

interface Props {
  entityName: string;
  onSubmit: (any) => void;
  submitting: boolean;
  fieldSettings?: {
    maxTargetRaiseAmount?: number;
  };
}

const INVESTOR_LABEL_TEXT = "Investors Must Be A Qualified Purchaser";
const QP_FEATURE_FLAG = "qualified_purchaser";

export default function CreateDealForm({
  entityName,
  onSubmit,
  submitting = false,
  fieldSettings = {},
}: Props) {
  const [isPublic, setIsPublic] = useState(false);
  const [hasInternational, setHasInternational] = useState(false);
  const [seriesName] = useState(entityName);

  const validation = Yup.object()
    .shape({
      dealName: Yup.string().trim().required(),
      description: Yup.string().trim(),
      targetRaiseAmount: getTargetRaiseAmountYup({
        max: fieldSettings?.maxTargetRaiseAmount,
      }),
      previouslyRaisedAmount: Yup.number()
        .transform(function (value, originalvalue) {
          return Number(originalvalue.replace(/\D+/g, ""));
        })
        .max(Yup.ref("targetRaiseAmount"), "Must be less than raise amount"),
      minInvestmentAmount: Yup.number()
        .transform(function (value, originalvalue) {
          return Number(originalvalue.replace(/\D+/g, ""));
        })

        .max(Yup.ref("targetRaiseAmount"), "Must be less than raise amount")
        .required(),
      organizerCarryPercentage: Yup.number()
        .transform(function (value, originalvalue) {
          return Number(originalvalue.replace("%", ""));
        })
        .min(0)
        .max(50)
        .required(),
      ...DateValidationSchema({ isRequired: true }),
      ...FeeValidationSchema(),
      ...SubformRegDExemptionValidation(),
    })
    .test("estimatedCloseDate", "estimatedCloseDate", (obj) => {
      let ecd = new Date(`${obj.year}-${obj.month}-${obj.day}`);
      if (moment(ecd).isAfter(new Date())) {
        return true;
      } else {
        return new Yup.ValidationError(
          "Estimated Close Date must be after Today",
          null,
          "year"
        );
      }
    })
    .test(
      "Check if amount is above fee %",
      "Amount cannot be above 4.5% of the target",
      (obj) => {
        return testValidFeeAmount(
          obj.targetRaiseAmount,
          obj.managementFee.amount,
          "managementFee.amount"
        );
      }
    );

  const offeringType = [
    {
      key: "1",
      value: "REGULATION_D",
      text: "Regulation D Only",
    },
    {
      key: "2",
      value: "REGULATION_S",
      text: "Regulation S Only",
    },
    //{
    //  key: "3",
    //  value: "REGULATION_D_S",
    //  text: "Both Regulation D & S",
    //},
  ];

  const tenant = useContext(TenantContext);

  return (
    <>
      <Formik
        initialValues={{
          isPublic: false,
          hasInternational: false,
          ...DateDefaultValues,
          ...SubformRegDExemptionDefaults,
        }}
        enableReinitialize={true}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            {/*
            <Prompt
              message={
                "Your Deal has not been created yet. Leaving now will require you to fill out the form again."
              }
              when={!props.isSubmitting && props.dirty}
            />
*/}
            <FormField
              name="dealName"
              label="Name Your Deal"
              placeholder="Capital Co Seed"
              required
            />
            <label style={{ color: "white" }}>
              Fund Name:{" "}
              {seriesName === "" ? (
                <Icon loading name="circle notch" />
              ) : (
                seriesName
              )}
            </label>
            <Divider hidden />
            <QuillField
              name={"description"}
              placeholder={"Provide a description of this deal"}
              label={"Deal Description"}
            />
            <FeatureFlag name={"publish_to_exchange"}>
              <FormField
                name="isPublic"
                component={Form.Checkbox}
                checked={isPublic}
                onClick={() => setIsPublic(!isPublic)}
                label={
                  <>
                    <label>
                      Publish to Exchange{" "}
                      <Popup
                        float="right"
                        trigger={<Icon color="green" name="question circle" />}
                        content="Click this box if you would like your deal to be published to our large community of accredited investors as part of our GLX exchange"
                        position="right center"
                      />
                    </label>
                  </>
                }
              />
            </FeatureFlag>
            <Divider hidden />
            <Header as="h3" inverted textAlign="center">
              About The Deal
            </Header>
            <Divider inverted />
            <FeatureFlag name={QP_FEATURE_FLAG}>
              <FormField
                inline
                name={"requireQualifiedPurchaser"}
                id={"requireQualifiedPurchaser"}
                label={INVESTOR_LABEL_TEXT}
                component={Form.Checkbox}
              />
            </FeatureFlag>
            <MaskedFormField
              id="targetRaiseAmount"
              name="targetRaiseAmount"
              thousandSeparator={true}
              allowNegative={false}
              prefix={"$"}
              decimalScale={0}
              type="tel"
              label="Raise Amount"
              placeholder="$500,000"
              required
            />
            <Form.Group widths="equal">
              <MaskedFormField
                id="minInvestmentAmount"
                name="minInvestmentAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Minimum Investment Amount"
                placeholder="$5,000"
                required
              />
              <MaskedFormField
                id="previouslyRaisedAmount"
                name="previouslyRaisedAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Amount Already Raised (off platform)"
                placeholder="$300,000"
              />
            </Form.Group>
            <Form.Group widths="equal">
              <MaskedFormField
                id="organizerCarryPercentage"
                name="organizerCarryPercentage"
                suffix={"%"}
                allowNegative={false}
                decimalScale={2}
                type="tel"
                label="Carry Percentage For Organizer"
                placeholder="20%"
                required
              />
            </Form.Group>
            <Form.Group widths="equal">
              <SubformDate
                id={"estimatedCloseDate"}
                label={"Estimated Close Date"}
                name={"estimatedCloseDate"}
              />
            </Form.Group>
            <FeatureFlag name={"offering_details"}>
              <Header as="h3" inverted textAlign="center">
                Offering Details
              </Header>
              <Divider inverted />
              <FormField
                name="hasInternational"
                component={Form.Checkbox}
                checked={hasInternational}
                onClick={() => setHasInternational(!hasInternational)}
                label={
                  <>
                    <label>
                      Offer this deal to investors outside the United States of
                      America?{" "}
                      <Popup
                        float="right"
                        trigger={<Icon color="green" name="question circle" />}
                        content="Regulation S is similar to Regulation D in that it provides exemption from registering private securities with the SEC. The main difference is that Regulation S is intended for offerings aimed exclusively at international investors."
                        position="right center"
                      />
                    </label>
                  </>
                }
              />
            </FeatureFlag>
            {hasInternational && (
              <FormField
                id="regulationType"
                name="regulationType"
                component={Form.Select}
                label="Offering Type"
                options={offeringType}
                placeholder="Select an Option"
              />
            )}
            <Divider hidden />
            <Header as="h3" inverted textAlign="center">
              Management Fees (Optional)
            </Header>
            <Divider inverted />
            <SubformManagementFee />
            {tenant && tenant?.settings?.use506cAccreditation && (
              <>
                <Header as="h3" inverted textAlign="center">
                  SEC Regulation D Rule 506 Subtype
                </Header>
                <Divider inverted />
                <SubformRegDExemption />
              </>
            )}
            m
            <Divider hidden />
            <Form.Group>
              <Form.Field width={10}>
                <Segment basic inverted>
                  The deal information above is used to create a set of legal
                  documents for this deal.
                </Segment>
              </Form.Field>
              <Form.Button
                width={6}
                secondary
                type="submit"
                loading={submitting}
                fluid
                content="Save & Generate Docs"
              />
            </Form.Group>
          </Form>
        )}
      </Formik>
    </>
  );
}
