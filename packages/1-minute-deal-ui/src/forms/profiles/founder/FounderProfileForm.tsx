import React, { useEffect, useRef } from "react";
import * as Yup from "yup";
import { Form, Divider } from "semantic-ui-react";
import { Formik } from "formik";
import SubformFirstLastName, * as SubformFirstLastNameMeta from "../../subforms/SubformFirstLastName";
import SubformDateOfBirth, * as SubformDateOfBirthMeta from "../../subforms/SubformDateOfBirth";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import { FormField } from "../../common/FormField";
import SubformPhone, * as SubformPhoneMeta from "../../../forms/subforms/SubformPhone";
import { useLoggedInUser } from "../../../hooks/userHooks";
import WizardSteps from "../../../components/common/WizardSteps";
import { ProfileWizardStep } from "../../../components/profiles/ProfileWizardActions";
import { partialFormikValidation } from "../../validation/ValidationHelper";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  stepNumber: number;
  country?: any;
  useAccountDefaultValues?: boolean;
}

export default function FounderProfileForm({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
  country = "United States of America",
  useAccountDefaultValues = false,
}: Props) {
  const formikRef = useRef(null);
  const loggedInUser = useLoggedInUser();

  useEffect(() => {
    if (loggedInUser && useAccountDefaultValues) {
      if (formikRef?.current) {
        formikRef.current.setValues(
          {
            ...formikRef.current.values,
            firstName: loggedInUser?.firstName,
            lastName: loggedInUser?.lastName,
            email: loggedInUser?.email,
            phone: loggedInUser?.attributes?.phone[0] ?? "",
          },
          true
        );
        formikRef.current.setTouched(
          {
            ...formikRef.current.touched,
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
          },
          false
        );
      }
    }
  }, [loggedInUser, useAccountDefaultValues]);

  let yupObject = {
    ...SubformFirstLastNameMeta.validationSchema({ isRequired: true }),
    ...SubformDateOfBirthMeta.validationSchema({ isRequired: true }),
    ...SubformAddressMeta.validationSchema(),
    email: Yup.string().email().required(),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    displayName: Yup.string()
      .max(64, "Display Name is limited to 64 characters")
      .required(),
  };

  let validation = Yup.object().shape(yupObject);

  return (
    <>
      <Formik
        initialValues={{
          countryOfResidence: "United States",
          registrationType: "INDIVIDUAL",
          ...SubformFirstLastNameMeta.defaultValues,
          ...SubformDateOfBirthMeta.defaultValues,
          ...{ ...SubformAddressMeta.defaultValues, ...{ country: country } },
          email: "",
          ...SubformPhoneMeta.defaultValues,
          displayName: "",
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
        innerRef={formikRef}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <WizardSteps currentStep={stepNumber}>
              <ProfileWizardStep
                submitting={submitting}
                stepNumber={stepNumber}
                onHandleSection={onHandleSection}
                canSubmitForm={props.isValid}
                nextDisabled={
                  !partialFormikValidation(props, [
                    "firstName",
                    "lastName",
                    "day",
                    "month",
                    "year",
                    "displayName",
                    "dateOfBirth",
                    "email",
                    "address1",
                    "city",
                    "state",
                    "country",
                    "postalCode",
                  ])
                }
                header="Profile Basics"
              >
                <Form.Group widths="equal">
                  <FormField
                    id={`displayName`}
                    name={`displayName`}
                    label="Display Name"
                    placeholder="Display Name"
                    data-testid={"displayName"}
                    required
                    {...props}
                  />
                </Form.Group>

                <Form.Group widths="equal">
                  <SubformFirstLastName />
                </Form.Group>

                <Form.Group widths="equal">
                  <FormField
                    label={"Contact Email"}
                    id="email"
                    name="email"
                    placeholder="email@example.com"
                    required
                  />
                  <SubformPhone required={true} />
                </Form.Group>
                <Form.Group widths="equal">
                  <SubformDateOfBirth />
                </Form.Group>
                <Divider />
                <SubformAddress {...props} />
              </ProfileWizardStep>
              <ProfileWizardStep
                submitting={submitting}
                stepNumber={stepNumber}
                onHandleSection={onHandleSection}
                nextDisabled={false}
                header="Identity"
              >
                <></>
              </ProfileWizardStep>
            </WizardSteps>
          </Form>
        )}
      </Formik>
    </>
  );
}
