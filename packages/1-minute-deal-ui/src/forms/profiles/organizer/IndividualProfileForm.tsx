import React, { useState, useEffect, useRef } from "react";

import * as Yup from "yup";

import {
  Form,
  Header,
  Dropdown,
  Button,
  Divider,
  Transition,
  Progress,
} from "semantic-ui-react";
import { Formik } from "formik";
import { getCountries } from "country-state-picker";
import { startsWithSearch } from "../../../tools/helpers";

import SubformPerson, * as SubformPersonMeta from "../../subforms/SubformPerson";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function IndividualProfileForm({
  onSubmit,
  submitting = false,
}: Props) {
  const [country, setCountry]: any = useState("");

  const validation = Yup.object({
    ...SubformPersonMeta.validationSchema({ isRequired: true }),
  });

  useEffect(() => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        const countries = getCountries();
        const foundCountry = countries.find(
          (el) =>
            el?.code?.toLowerCase() === response?.countryCode?.toLowerCase()
        );
        if (foundCountry) setCountry(foundCountry.name);
      })
      .catch((err) => {});
  }, []);

  const completedStep = () => {
    return country ? 1 : 0;
  };

  const totalSteps = () => {
    return 2;
  };

  return (
    <>
      <Transition transitionOnMount animation="fade up">
        <div>
          <Header as="h3" inverted>
            My country of residence is...
          </Header>
          <Dropdown
            name="country"
            placeholder="Select a Country"
            clearable
            selection
            fluid
            search={startsWithSearch}
            onChange={(e, d) => {
              setCountry(d.value);
              if (d.value === "")
                window.scrollTo({ top: 0, behavior: "smooth" });
            }}
            options={getCountries().map((item) => {
              return {
                key: item.code,
                value: item.name,
                text: item.name,
              };
            })}
            value={country}
          />
          <Divider hidden />
        </div>
      </Transition>

      <Transition visible={country !== ""}>
        <div>
          <Formik
            initialValues={{
              ...{
                ...SubformPersonMeta.defaultValues,
                ...{ country: country },
              },
              registrationType: "INDIVIDUAL",
            }}
            enableReinitialize={true}
            validationSchema={validation}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form
                onSubmit={props.handleSubmit}
                data-testid={"individual-profile-form"}
              >
                <SubformPerson {...props} useAccountDefaultValues={true} />
                <Divider hidden />
                <Button
                  secondary
                  type="submit"
                  loading={submitting}
                  fluid
                  data-testid={"submit-button"}
                  content="Create Profile"
                />
              </Form>
            )}
          </Formik>
        </div>
      </Transition>
    </>
  );
}
