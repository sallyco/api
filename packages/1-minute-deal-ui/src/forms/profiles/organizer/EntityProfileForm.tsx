import React, { useEffect, useState } from "react";

import * as Yup from "yup";

import {
  Form,
  Button,
  Dropdown,
  Header,
  Divider,
  Transition,
  Progress,
} from "semantic-ui-react";
import { Formik, useFormikContext } from "formik";
import { FormField } from "../../common/FormField";

import { EntityTypes } from "../../../tools/enums";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../../../tools/helpers";
import SubformEmail, * as SubformEmailMeta from "../../subforms/SubformEmail";
import SubformPhone, * as SubformPhoneMeta from "../../subforms/SubformPhone";
import SubformFirstLastName, * as SubformFirstLastNameMeta from "../../subforms/SubformFirstLastName";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../rootReducer";
import { fetchSelf } from "../../../slices/usersSlice";
import SubformIndividualTaxId, * as SubformIndividualTaxIdMeta from "../../subforms/SubformIndividualTaxId";
import SubformDateOfBirth from "../../subforms/SubformDateOfBirth";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  useAccountDefaultValues?: boolean;
}

export default function EntityProfileForm({
  useAccountDefaultValues = false,
  onSubmit,
  submitting = false,
}: Props) {
  const [country, setCountry]: any = useState("");
  const [state, setState]: any = useState("");
  const [defaultValues, setDefaultValues] = useState<{
    firstName?: string;
    lastName?: string;
    phone?: string;
    email?: string;
  }>({});

  const dispatch = useDispatch();
  const myUser = useSelector((state: RootState) => state.users.self);

  useEffect(() => {
    if (!myUser) {
      dispatch(fetchSelf());
    }
  }, [dispatch, myUser]);
  useEffect(() => {
    if (myUser && useAccountDefaultValues) {
      setDefaultValues({
        firstName: myUser?.firstName,
        lastName: myUser?.lastName,
        email: myUser?.email,
        phone: myUser?.attributes?.phone[0] ?? "",
      });
    }
  }, [myUser]);

  const validation = Yup.object().shape({
    displayName: Yup.string()
      .max(64, "Display Name is limited to 64 characters")
      .required(),
    name: Yup.string().max(1024).required(),
    ...SubformFirstLastNameMeta.validationSchema({ isRequired: true }),
    ...SubformEmailMeta.validationSchema({ isRequired: true }),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    ...SubformAddressMeta.validationSchema(),
    title: Yup.string().min(2).max(64),
    entityType: Yup.string(),
  });

  const completedStep = () => {
    if (country) {
      if (country === "United States of America") {
        return state ? 1 : 0;
      } else {
        return 1;
      }
    } else {
      return 0;
    }
  };

  const totalSteps = () => {
    return 2;
  };

  return (
    <>
      <Transition animation="fade up">
        <div>
          <Header as="h3" inverted>
            My entity&#39;s county of formation is...
          </Header>
          <Dropdown
            name="country"
            placeholder="Select a Country"
            clearable
            selection
            fluid
            search={startsWithSearch}
            onChange={(e, d) => {
              setCountry(d.value);
              if (d.value === "") {
                setState("");
                window.scrollTo({ top: 0, behavior: "smooth" });
              }
            }}
            options={getCountries().map((item) => {
              return {
                key: item.code,
                value: item.name,
                text: item.name,
              };
            })}
          />
          <Divider hidden />
        </div>
      </Transition>

      <Transition visible={country === "United States of America"}>
        <div>
          <Header as="h3" inverted>
            My entity&#39;s state of formation is...
          </Header>
          <Dropdown
            name="state"
            clearable
            selection
            fluid
            search={startsWithSearch}
            onChange={(e, d) => {
              setState(d.value);
              if (d.value === "")
                window.scrollTo({ top: 0, behavior: "smooth" });
            }}
            options={(country
              ? getStates(
                  getCountries().find((item) => {
                    return item.name === country;
                  })?.code
                )
              : []
            ).map((item) => {
              return {
                key: item,
                value: item,
                text: item,
              };
            })}
            placeholder={
              country === "United States of America"
                ? "Select a State"
                : "State/Province/Territory"
            }
          />
          <Divider hidden />
        </div>
      </Transition>

      <Transition
        visible={
          state !== "" ||
          (country !== "" && country !== "United States of America")
        }
      >
        <div>
          {myUser && (
            <Formik
              initialValues={{
                ...SubformFirstLastNameMeta.defaultValues,
                ...{
                  ...SubformAddressMeta.defaultValues,
                  ...{ country: country, state: state },
                },
                ...SubformEmailMeta.defaultValues,
                ...SubformPhoneMeta.defaultValues,
                ...SubformIndividualTaxIdMeta.defaultValues,
                countryOfFormation: country,
                stateOfFormation: state,
                registrationType: "ENTITY",
                displayName: "",
                ...defaultValues,
              }}
              enableReinitialize={true}
              validationSchema={validation}
              onSubmit={onSubmit}
            >
              {(props) => (
                <Form onSubmit={props.handleSubmit} autoComplete="off">
                  <Form.Group widths="equal">
                    <FormField
                      id={`displayName`}
                      name={`displayName`}
                      label="Display Name"
                      placeholder="Display Name"
                      data-testid={"displayName"}
                      required
                    />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <FormField
                      name="name"
                      component={Form.Input}
                      label="Entity Name"
                      placeholder="My Company, LLC"
                      required
                    />
                    {country === "United States of America" && (
                      <FormField
                        name="entityType"
                        component={Form.Select}
                        label="Entity Type"
                        options={EntityTypes}
                        placeholder="Select a Type"
                        search
                      />
                    )}
                  </Form.Group>
                  <Form.Group widths="equal">
                    <SubformEmail />
                    <SubformPhone />
                  </Form.Group>
                  <SubformAddress {...props} />

                  <Header as="h3" inverted textAlign="center">
                    Authorized signer for this Entity
                  </Header>
                  <Divider inverted />
                  <Form.Group widths="equal">
                    <SubformFirstLastName />
                  </Form.Group>

                  <Form.Group widths="equal">
                    <FormField
                      id="title"
                      name="title"
                      label="Title"
                      placeholder="Partner"
                    />
                  </Form.Group>
                  <SubformDateOfBirth />

                  <SubformIndividualTaxId
                    values={{
                      country,
                    }}
                    {...props}
                  />

                  <Divider hidden />
                  <Button secondary type="submit" loading={submitting} fluid>
                    Create Entity Profile
                  </Button>
                </Form>
              )}
            </Formik>
          )}
        </div>
      </Transition>
    </>
  );
}
