import Log from "../../tools/Log";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { insertProfile } from "../../slices/profilesSlice";
import { createKycAmlCheck, Profile } from "../../api/profilesApi";
import { getProfileName } from "../../api/profileApiWrapper";
import { toast } from "react-toastify";
import { updateSubscriptionById } from "../../slices/subscriptionsSlice";
import { ToastError } from "../../tools/ToastMessage";
import { useLocation, useHistory } from "react-router-dom";
import { useCallback } from "react";
import { API, doChange, useRequest } from "../../api/swrApi";
import { mutate } from "swr";

function useProfileSubmit({
  type,
  redirectTo,
}: {
  type: string;
  redirectTo?: string;
}) {
  const dispatch = useDispatch();
  const { self } = useSelector((state: RootState) => state.users);
  const query = new URLSearchParams(useLocation().search);
  const subscriptionId = query.get("subscriptionId") ?? false;
  const companyId = query.get("companyId") ?? "";
  const isRegenerate = query.get("regenerate") ?? false;
  const { data: subscription } = useRequest(
    subscriptionId && API.SUBSCRIPTION_BY_ID(subscriptionId)
  );
  const { data: deal } = useRequest(
    subscription?.dealId && API.DEAL_BY_ID(subscription.dealId)
  );
  const { data: entity } = useRequest(
    deal?.entityId && API.ENTITY_BY_ID(deal.entityId)
  );
  let history = useHistory();

  const checkForAccreditation = () => {
    return (
      entity.regulationType === "REGULATION_D" && entity.regDExemption === "c"
    );
  };

  const isPrimarySignatory = useCallback(() => {
    return subscription && self && subscription.ownerId === self.id;
  }, [subscription, self]);

  const onSubmit = async function onSubmit(data) {
    Log.debug("data", data);
    if (!data?.phone) {
      data.phone = self?.attributes?.phone
        ? self?.attributes?.phone[0] ?? ""
        : "";
    }
    type = type.toUpperCase();
    if (type === "SUBSCRIBER") {
      type = "INVESTOR";
      data.registrationType = data.registrationType.toUpperCase();
    }
    if (type === "INDIVIDUAL") {
      type = "ORGANIZER";
      data.registrationType = "INDIVIDUAL";
    }
    if (type === "FOUNDER") {
      data.registrationType = "INDIVIDUAL";
    }

    const profileData: Partial<Profile> = {
      profileType: type,
      ...(data.displayName && { displayName: data.displayName }),
      ...(data.name && { name: data.name }),
      ...(data.typeOfEntity && { typeOfEntity: data.typeOfEntity }),
      ...(data.jointType && { jointType: data.jointType }),
      ...(data.firstName && { firstName: data.firstName }),
      ...(data.lastName && { lastName: data.lastName }),
      countryOfFormation: data.country ?? "",
      stateOfFormation: data.state ?? "",
      address: {
        address1: data.address1 ?? "",
        ...(data.address2 && { address2: data.address2 }),
        city: data.city ?? "",
        state: data.state ?? "",
        postalCode: data.postalCode ?? "",
        country: data.country ?? "",
      },
      ...(data.phone && { phone: data.phone }),
      ...(data.dateOfBirth && { dateOfBirth: data.dateOfBirth }),
      isUSBased:
        data.isUSBased === "Yes" && data.typeOfEntity !== "FOREIGN_ENTITY",
      ...(data.email && { email: data.email.toLowerCase() }),
      investorStatus: [
        ...(data.certify0 ? [0] : []),
        ...(data.certify1 ? [1] : []),
        ...(data.certify2 ? [2] : []),
        ...(data.certify3 ? [3] : []),
        ...(data.certify4 ? [4] : []),
        ...(data.certify5 ? [5] : []),
        ...(data.certify6 ? [6] : []),
        ...(data.certify7 ? [7] : []),
        ...(data.certify8 ? [8] : []),
        ...(data.certify9 ? [9] : []),
        ...(data.certify10 ? [10] : []),
      ],

      purchaserStatus: [
        ...(data.purchaserStatus0 ? [0] : []),
        ...(data.purchaserStatus1 ? [1] : []),
        ...(data.purchaserStatus2 ? [2] : []),
        ...(data.purchaserStatus3 ? [3] : []),
        ...(data.purchaserStatus4 ? [4] : []),
        ...(data.purchaserStatus5 ? [5] : []),
        ...(data.purchaserStatus6 ? [6] : []),
        ...(data.purchaserStatus7 ? [7] : []),
        ...(data.purchaserStatus8 ? [8] : []),
        ...(data.purchaserStatus9 ? [9] : []),
        ...(data.purchaserStatus10 ? [10] : []),
      ],

      taxDetails: {
        registrationType: data.registrationType.toUpperCase(),
        taxIdentification: {
          type: data.taxIdType,
          value: data.taxId,
        },
      },
      primarySignatory: undefined,
    };

    if (data.primarySignatoryfirstName) {
      profileData["primarySignatory"] = {
        title: data.primarySignatorytitle ?? "",
        name:
          (data.primarySignatoryfirstName ?? "") +
          " " +
          (data.primarySignatorylastName ?? ""),
        isUSBased: data.primarySignatoryisUSBased === "Yes",
        taxDetails: {
          type: "ssn",
          value: data.primarySignatorytaxId ?? "",
        },
        address: {
          address1: data.primarySignatoryAddress?.address1,
          address2: data.primarySignatoryAddress?.address2,
          city: data.primarySignatoryAddress?.city,
          state: data.primarySignatoryAddress?.state,
          postalCode: data.primarySignatoryAddress?.postalCode,
          country: data.primarySignatoryAddress?.country,
        },
      };
    }
    if (!profileData?.address) {
      profileData.address = profileData.primarySignatory.address;
    }

    if (data?.signers && data?.signers.length > 0) {
      profileData["additionalSignatories"] = data.signers.map((signer) => {
        return {
          email: signer.email.toLowerCase(),
          name: `${signer.firstName} ${signer.lastName}`,
        };
      });
    }

    try {
      // TODO figure out how to use proper type (Profile) not any
      // throw new Error("Testing Thrown");
      const profile: any = await dispatch(insertProfile(profileData));

      if (type === "INVESTOR") {
        if (subscription) {
          let signers;
          if (
            profile?.additionalSignatories &&
            (!subscription.signers || subscription.signers.length < 1)
          ) {
            signers = [
              {
                name: profile.primarySignatory.name,
                email: self.email.toLowerCase(),
                profileId: profile.id,
              },
            ];
            for (const signatory of profile.additionalSignatories) {
              signers.push({
                email: signatory.email.toLowerCase(),
                name: signatory.name,
                dateSigned: undefined,
              });
            }
          }
          if (!isPrimarySignatory() && subscription.signers) {
            signers = [...subscription.signers];
            const signerIndex = signers.findIndex(
              (signer) =>
                signer.email.toLowerCase() === self.email.toLowerCase()
            );
            signers[signerIndex] = {
              ...signers[signerIndex],
              profileId: profile.id,
            };
          }
          if (!checkForAccreditation()) {
            await dispatch(
              updateSubscriptionById({
                ...subscription,
                profileId: profile.id,
                signers: signers,
              })
            );
          }
        }

        try {
          await createKycAmlCheck(profile.id);
        } catch (e) {
          console.log("Failed to create KYC/AML Check");
        }
        Log.debug("profile", profile);
        if (profile.name) toast.success(`Profile "${profile.name}" Created`);
        else toast.success(`Profile "${getProfileName(profile)}"  Created`);

        // Short circuit default behavior redirect instead
        if (!subscriptionId && redirectTo) {
          history.push(redirectTo);
        }

        if (
          subscriptionId &&
          data.regType &&
          data.regType === "REGULATION_D_S"
        ) {
          history.push(`/subscriptions/${subscriptionId}`);
          return;
        }
        if (subscriptionId) {
          if (isRegenerate) {
            history.push(`/subscriptions/${subscriptionId}/set-bank-only`);
            return;
          } else if (!isPrimarySignatory()) {
            history.push(
              `/subscriptions/${subscriptionId}/sign-documents?generate=true`
            );
            return;
          } else {
            if (checkForAccreditation()) {
              history.replace(
                `/profiles/create/subscriber?subscriptionId=${subscriptionId}&selected=${profile.id}`
              );
              return;
            }
            history.push(`/subscriptions/${subscriptionId}/set-amount`);
            return;
          }
        } else {
          history.push(`/dashboard/subscriptions`);
        }
      }
      if (type === "FOUNDER") {
        if (companyId) {
          const data = await doChange(
            API.ASSET_BY_ID(companyId),
            {
              founderProfileId: profile.id,
            },
            "PATCH"
          );
          await mutate(
            API.ADD_FILTER(API.ASSET_BY_ID(companyId), {
              include: ["deal"],
              fields: {
                logo: false,
                banner: false,
              },
            }),
            data
          );
          history.push(`/assets/companies/${companyId}`);
        } else {
          history.push(`/assets/companies`);
        }
      } else {
        if (redirectTo) {
          history.push(redirectTo);
        } else {
          history.push(`/deals/create?profileId=${profile.id}`);
        }
      }
    } catch (e) {
      const err: any = e;
      if (err?.response) {
        ToastError(err.response);
      }
    }
  };

  return { onSubmit, subscription, deal, entity };
}

export { useProfileSubmit };
