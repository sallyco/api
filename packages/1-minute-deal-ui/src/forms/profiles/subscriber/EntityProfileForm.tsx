import React, { useState } from "react";
import {
  Form,
  Header,
  Icon,
  AccordionTitle,
  AccordionContent,
  Segment,
  Accordion,
  Divider,
} from "semantic-ui-react";
import * as Yup from "yup";
import { Formik } from "formik";
import { FormField } from "../../common/FormField";
import { EntityTypes } from "../../../tools/enums";

import SubformEntityTaxId, * as SubformEntityTaxIdMeta from "../../subforms/SubformEntityTaxId";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import SubformSigner, * as SubformSignerMeta from "../../subforms/SubformSigner";
import SubformPhone, * as SubformPhoneMeta from "../../../forms/subforms/SubformPhone";

import SubformPurchaserStatus, {
  getPurchaserStatusMeta,
} from "../../../forms/subforms/SubformPurchaserStatus";
import { useFeatureFlag } from "../../../components/featureflags/FeatureFlags";
import { useLoggedInUser } from "../../../hooks/userHooks";
import WizardSteps from "../../../components/common/WizardSteps";
import { ProfileWizardStep } from "../../../components/profiles/ProfileWizardActions";
import AdditionalSignersInvite, * as AdditionalSignersInviteMeta from "../../../components/profiles/AdditionalSignersInvite";
import { Box, Alert, AlertTitle } from "@mui/material";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  stepNumber: number;
  regType: string;
  country?: any;
  isQualifiedPurchaserForm?: boolean;
}

export default function CreateSubscriberEntityProfile({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
  regType = "REGULATION_D",
  country = "United States of America",
  isQualifiedPurchaserForm = false,
}: Props) {
  const purchaserFeature = useFeatureFlag("qualified_purchaser");
  const loggedInUser = useLoggedInUser();
  const totalSections = purchaserFeature ? 5 : 4;
  const purchaserMeta = getPurchaserStatusMeta(isQualifiedPurchaserForm);
  const qpMeta = getPurchaserStatusMeta(true);
  const [accordionState, setAccordionState] = useState<{ activeIndex: number }>(
    { activeIndex: -1 }
  );

  let yupObject = {
    displayName: Yup.string()
      .trim()
      .min(2)
      .max(64, "Display Name is limited to 64 characters")
      .required(),
    name: Yup.string().trim().min(2).max(1024).required(),
    typeOfEntity: Yup.string(),
    ...SubformEntityTaxIdMeta.validationSchema({ isRequired: false }),
    ...SubformAddressMeta.validationSchema(),
    ...SubformSignerMeta.validationSchema(),
    email: Yup.string().email().required(),
    //  ...SubformSigner2Meta.validationSchema(),
    //  ...SubformBOwnerMeta.validationSchema(),
    ...(!isQualifiedPurchaserForm
      ? {
          certify3: Yup.boolean(),
          certify4: Yup.boolean(),
          certify5: Yup.boolean(),
          certify6: Yup.boolean(),
          certify7: Yup.boolean(),
          certify8: Yup.boolean(),
          certify9: Yup.boolean(),
          certify10: Yup.boolean(),
        }
      : {}),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    ...AdditionalSignersInviteMeta.validationSchema(0, false),
  };

  if (purchaserFeature) {
    yupObject = {
      ...yupObject,
      ...purchaserMeta.validationSchema,
    };
  }

  let entityAccreditationTest = (obj) => {
    if (regType === "REGULATION_S") return true;

    if (
      obj.certify10 &&
      (country === "United States of America" || regType === "REGULATION_D")
    ) {
      return new Yup.ValidationError(
        "If none of the preceding statments are true, you cannot invest in this deal.  Please contact the deal organizer.",
        null,
        "certify7"
      );
    } else if (
      obj.certify10 &&
      country !== "United States of America" &&
      regType !== "REGULATION_D"
    ) {
      return true;
    }

    if (
      (obj.certify3 ||
        obj.certify4 ||
        obj.certify5 ||
        obj.certify6 ||
        obj.certify7 ||
        obj.certify8 ||
        obj.certify9) &&
      !obj.certify10
    ) {
      return true; // everything is fine
    }

    return new Yup.ValidationError(
      "Please select one of the applicable statement.",
      null,
      "certify3"
    );
  };

  let validation = Yup.object().shape(yupObject);

  if (purchaserFeature) {
    // This function tests QP, then Accred if needed.
    // The purpose being you can skip accred checkboxes
    // If you are a QP
    let entityQPOverridesAccredTest = (obj) => {
      // If a QP option is chosen
      const qpTestResult = qpMeta.oneOfTest(obj);
      if (qpTestResult === true) {
        return true;
      }

      // Validate the QP step regularly (allows none apply option)
      const qpSecondaryTest = purchaserMeta.oneOfTest(obj);
      if (qpSecondaryTest !== true) {
        return qpSecondaryTest;
      }

      return entityAccreditationTest(obj);
    };
    validation = validation.test(
      "PurchaserTest",
      null,
      entityQPOverridesAccredTest
    );
  } else {
    validation = validation.test(
      "CertificationTest",
      null,
      entityAccreditationTest
    );
  }

  const handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = accordionState;
    const newIndex = activeIndex === index ? -1 : index;

    setAccordionState({ activeIndex: newIndex });
  };

  const isInvalidSelectionInQP = (props) => {
    const qpTestInvalid =
      purchaserMeta.oneOfTest(props.values) !== true ||
      props.values.purchaserStatus8;
    if (isQualifiedPurchaserForm) {
      return qpTestInvalid;
    } else {
      return qpTestInvalid
        ? (!props.values.certify3 &&
            !props.values.certify4 &&
            !props.values.certify5 &&
            !props.values.certify6 &&
            !props.values.certify7 &&
            !props.values.certify8 &&
            !props.values.certify9) ||
            props.values.certify10
        : qpTestInvalid;
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          displayName: "",
          name: "",
          typeOfEntity: "",
          registrationType: "ENTITY",
          isUSBased: "Yes",
          isSingleMemberLLC: "Yes",
          entityCountry: country,
          primarySignatory: {
            country: country,
          },
          regType: regType,
          email: "",
          ...SubformEntityTaxIdMeta.defaultValues,
          ...{ ...SubformAddressMeta.defaultValues, ...{ country: country } },
          ...SubformSignerMeta.defaultValues,
          certify3: false,
          certify4: false,
          certify5: false,
          certify6: false,
          certify7: false,
          certify8: false,
          certify9: false,
          certify10: false,
          phone: loggedInUser?.attributes?.phone[0] ?? "",
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <WizardSteps currentStep={stepNumber}>
              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.name ||
                  !(
                    !props.errors.displayName &&
                    !props.errors.name &&
                    !props.errors.email &&
                    !props.errors.address1 &&
                    !props.errors.city &&
                    !props.errors.state &&
                    !props.errors.country &&
                    !props.errors.postalCode
                  )
                }
                header="Profile Basics"
              >
                <Form.Group widths="equal">
                  <FormField
                    id={`displayName`}
                    name={`displayName`}
                    label="Display Name"
                    placeholder="Display Name"
                    data-testid={"displayName"}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <FormField
                    id="name"
                    name="name"
                    label="Entity Name"
                    placeholder="Entity Name"
                    required
                  />
                  {props.values.entityCountry ===
                    "United States of America" && (
                    <FormField
                      id="typeOfEntity"
                      name="typeOfEntity"
                      component={Form.Select}
                      label="Entity Type"
                      options={EntityTypes}
                      placeholder="Select a Type"
                      search
                    />
                  )}
                </Form.Group>

                {props.values.entityCountry === "United States of America" &&
                  props.values.typeOfEntity === "LIMITED_LIABILITY_COMPANY" && (
                    <Form.Group widths={"equal"}>
                      <Form.Field>
                        <Header as="h3" inverted>
                          Is this a single member-LLC?
                        </Header>
                        <Form.Group>
                          <FormField
                            label="Yes"
                            component={Form.Radio}
                            name="isSingleMemberLLC"
                            value="Yes"
                          />
                          <FormField
                            label="No"
                            component={Form.Radio}
                            name="isSingleMemberLLC"
                            value="No"
                          />
                        </Form.Group>
                      </Form.Field>
                    </Form.Group>
                  )}
                <Form.Group widths={"equal"}>
                  <FormField
                    label={"Contact Email"}
                    id="email"
                    name="email"
                    placeholder="email@example.com"
                    required
                  />
                  <SubformPhone required={true} />
                </Form.Group>
                <SubformAddress {...props} />
              </ProfileWizardStep>

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !(props.values.hasTaxId === "No") &&
                  (!props.touched.taxId || Boolean(props.errors.taxId))
                }
                header="Tax Identification"
              >
                <SubformEntityTaxId />
              </ProfileWizardStep>
              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.primarySignatoryfirstName ||
                  !(
                    !props.errors.primarySignatoryfirstName &&
                    !props.errors.primarySignatorylastName &&
                    !props.errors.primarySignatorytitle &&
                    !props.errors.primarySignatoryAddress &&
                    !props.errors.primarySignatorytaxId
                  )
                }
                header="Authorized Signatory"
              >
                <SubformSigner {...props} />
                <Box mb={2}>
                  <Box pt={2} px={2}>
                    <Header as="h3" inverted>
                      Additional Signers
                    </Header>
                    <Alert severity={"info"}>
                      <AlertTitle>
                        Does your entity have any control owners?
                      </AlertTitle>
                      A control owner is someone who owns 25% or more of the
                      entity, and therefore all control owners must also sign.
                      Please include their information below.
                    </Alert>
                  </Box>
                  <Box p={2}>
                    <AdditionalSignersInvite minSigners={0} maxSigners={25} />
                  </Box>
                </Box>
              </ProfileWizardStep>
              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={isInvalidSelectionInQP(props)}
                canSubmitForm={props.isValid}
                header="Purchaser/Accreditation"
              >
                <Segment inverted>
                  <Accordion inverted>
                    <AccordionTitle
                      active={accordionState.activeIndex === 0}
                      index={0}
                      onClick={handleAccordionClick}
                    >
                      <Header as="h3" inverted>
                        <Icon name="dropdown" />
                        Qualified Purchaser
                        <Header as="h4" inverted>
                          Select how you qualify as a qualified purchaser from
                          the following qualification criteria
                        </Header>
                      </Header>
                    </AccordionTitle>
                    <AccordionContent active={accordionState.activeIndex === 0}>
                      <SubformPurchaserStatus />
                    </AccordionContent>

                    <Divider />
                    {!isQualifiedPurchaserForm && (
                      <>
                        <AccordionTitle
                          active={accordionState.activeIndex === 1}
                          index={1}
                          onClick={handleAccordionClick}
                        >
                          <Header as="h3" inverted>
                            <Icon name="dropdown" />
                            Investor Accreditation
                            <Header as="h4" inverted>
                              Select how you qualify as an accredited investor
                              from the following qualification criteria
                            </Header>
                          </Header>
                        </AccordionTitle>
                        <AccordionContent
                          active={accordionState.activeIndex === 1}
                        >
                          {regType === "REGULATION_S" ? (
                            <FormField
                              id="certifyX"
                              name="certifyX"
                              component={Form.Checkbox}
                              label="Entity is not accredited"
                            />
                          ) : (
                            <>
                              <Header as="h3" inverted>
                                Check all that apply...
                              </Header>
                              <FormField
                                id="certify3"
                                name="certify3"
                                component={Form.Checkbox}
                                label="Subscriber is an irrevocable trust with total assets in excess of $5,000,000 whose purchase is directed by a person with such knowledge and experience in financial and business matters that such person is capable of evaluating the merits and risks of the prospective investment."
                              />
                              <FormField
                                id="certify4"
                                name="certify4"
                                component={Form.Checkbox}
                                label="Subscriber is a bank, insurance company, investment company registered under the Company Act, a broker or dealer registered pursuant to Section 15 of the Securities Exchange Act of 1934 (the 'Exchange Act'), a state-registered or SEC-registered investment adviser, an exempt reporting adviser pursuant to Section203(l) or 203(m) of the United States Investment Advisers Act of 1940, as amended (the “Advisers Act”), a rural business investment company (RBIC) as defined in Section 384A of the Consolidated Farm and Rural Development Act, a business development company, a Small Business Investment Company licensed by the United States Small Business Administration, a plan with total assets in excess of $5,000,000 established and maintained by a state for the benefit of its employees, or a private business development company as defined in Section 202(a)(22) of the Advisers Act."
                              />
                              <FormField
                                id="certify5"
                                name="certify5"
                                component={Form.Checkbox}
                                label="Subscriber is an employee benefit plan and either all investment decisions are made by a bank, savings and loan association, insurance company, or registered investment advisor, or Subscriber has total assets in excess of $5,000,000 or, if such plan is a self-directed plan, investment decisions are made solely by persons who are accredited investors."
                              />
                              <FormField
                                id="certify6"
                                name="certify6"
                                component={Form.Checkbox}
                                label="Subscriber is an Indian tribe, governmental body, or an entity organized under the law of a foreign county, that owns investments, as defined in Rule 2a51-1(b) of the Investment Company Act, in excess of $5,000,000.00, and was not formed for the specific purpose of investing in the securities offered. "
                              />
                              <FormField
                                id="certify7"
                                name="certify7"
                                component={Form.Checkbox}
                                label="Subscriber is a family office, as defined in Rule 202(a)(11)(G)-1 of the Advisers Act (the “Family Office Rule”, or a “family client” of such family office as such term is defined in the Family Office Rule: (a) with assets under management in excess of $5,000,000.00, (b) not formed for the purpose of acquiring the Interests, and (c) the acquisition of the Interests is directed by a person who has such knowledge and experience in financial and business matters that he or she is capable of evaluating the merits and risks of acquiring the Interests. "
                              />
                              <FormField
                                id="certify8"
                                name="certify8"
                                component={Form.Checkbox}
                                label="Subscriber is a corporation, partnership, limited liability company or business trust, not formed for the purpose of acquiring the Interests, or an organization described in Section 501(c)(3) of the Code, in each case with total assets in excess of $5,000,000."
                              />
                              <FormField
                                id="certify9"
                                name="certify9"
                                component={Form.Checkbox}
                                label={
                                  <label
                                    dangerouslySetInnerHTML={{
                                      __html:
                                        "Subscriber is an entity in which all of the equity owners, or a grantor or revocable trust in which all of the grantors and trustees, qualify under one of the previous clauses or:<ul><li>If an individual, Subscriber has a net worth, either individually or upon a joint basis with Subscriber's spouse, of at least $1,000,000, or has had an individual income in excess of $200,000 for each of the two most recent years, or a joint income with Subscriber's spouse, or spousal equivalent, in excess of $300,000 in each of those years, and has a reasonable expectation of reaching the same income level in the current year. </li><li>If an individual, Subscriber holds, in good standing, one of the following certifications or designations administered by the Financial Industry Regulatory Authority, Inc. (“FINRA”): the Licensed General Securities Representative (Series 7), Licensed Investment Adviser Representative (Series 65), or Licensed Private Securities Offerings Representative (Series 82).</li><li>If an individual, Subscriber is a “knowledgeable employee”  within the meaning prescribed under Rule 3c-5(a)(4) of the Investment Company Act of 1940, as amended (the “Investment Company Act”) of a private fund exempt from registration pursuant to Rule 3(c)(1) or Rule 3(c)(7) of the Investment Company Act.</li></ul>",
                                    }}
                                  />
                                }
                              />
                              <FormField
                                id="certify10"
                                name="certify10"
                                component={Form.Checkbox}
                                label="Subscriber cannot make any of the above representations."
                              />
                            </>
                          )}
                        </AccordionContent>
                      </>
                    )}
                  </Accordion>
                </Segment>
              </ProfileWizardStep>
            </WizardSteps>
          </Form>
        )}
      </Formik>
    </>
  );
}
