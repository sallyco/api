import React, { useEffect, useRef, useState } from "react";
import * as Yup from "yup";
import {
  Form,
  Button,
  Divider,
  Header,
  Progress,
  Grid,
  Icon,
  AccordionTitle,
  AccordionContent,
  Segment,
  Accordion,
} from "semantic-ui-react";
import { Formik } from "formik";
import SubformFirstLastName, * as SubformFirstLastNameMeta from "../../subforms/SubformFirstLastName";
import SubformPersonTaxId, * as SubformPersonTaxIdMeta from "../../subforms/SubformPersonTaxId";
import SubformDateOfBirth, * as SubformDateOfBirthMeta from "../../subforms/SubformDateOfBirth";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import SubformAccreditation, * as SubformAccreditationMeta from "../../subforms/SubformAccreditation";
import { FormField } from "../../common/FormField";
import SubformPurchaserStatus, {
  getPurchaserStatusMeta,
} from "../../../forms/subforms/SubformPurchaserStatus";
import { useFeatureFlag } from "../../../components/featureflags/FeatureFlags";
import SubformPhone, * as SubformPhoneMeta from "../../../forms/subforms/SubformPhone";
import { useLoggedInUser } from "../../../hooks/userHooks";
import WizardSteps from "../../../components/common/WizardSteps";
import { ProfileWizardStep } from "../../../components/profiles/ProfileWizardActions";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  stepNumber: number;
  regType: string;
  country?: any;
  useAccountDefaultValues?: boolean;
  isQualifiedPurchaserForm?: boolean;
}

export default function IndividualProfileForm({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
  regType = "REGULATION_D",
  country = "United States of America",
  useAccountDefaultValues = false,
  isQualifiedPurchaserForm = false,
}: Props) {
  const purchaserFeature = useFeatureFlag("qualified_purchaser");
  const totalSections = purchaserFeature ? 4 : 3;
  const formikRef = useRef(null);
  const loggedInUser = useLoggedInUser();
  const [accordionState, setAccordionState] = useState<{ activeIndex: number }>(
    { activeIndex: -1 }
  );

  useEffect(() => {
    if (loggedInUser && useAccountDefaultValues) {
      if (formikRef?.current) {
        formikRef.current.setValues(
          {
            ...formikRef.current.values,
            firstName: loggedInUser?.firstName,
            lastName: loggedInUser?.lastName,
            email: loggedInUser?.email,
            phone: loggedInUser?.attributes?.phone[0] ?? "",
          },
          true
        );
        formikRef.current.setTouched(
          {
            ...formikRef.current.touched,
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
          },
          false
        );
      }
    }
  }, [loggedInUser]);

  const purchaserMeta = getPurchaserStatusMeta(isQualifiedPurchaserForm);
  const qpMeta = getPurchaserStatusMeta(true);

  let yupObject = {
    ...SubformFirstLastNameMeta.validationSchema({ isRequired: true }),
    ...SubformDateOfBirthMeta.validationSchema({ isRequired: true }),
    ...SubformAddressMeta.validationSchema(),
    ...SubformPersonTaxIdMeta.validationSchema({ isRequired: true }),
    ...SubformAccreditationMeta.validationSchema(),
    ...purchaserMeta.validationSchema,
    email: Yup.string().email().required(),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    displayName: Yup.string()
      .max(64, "Display Name is limited to 64 characters")
      .required(),
  };

  if (purchaserFeature) {
    yupObject = {
      ...yupObject,
      ...purchaserMeta.validationSchema,
    };
  }

  // This function checks accreditation ONLY
  let individualAccreditationTest = (obj) => {
    if (regType === "REGULATION_S") return true;

    if (
      obj.certify10 &&
      (country === "United States of America" || regType === "REGULATION_D")
    ) {
      return new Yup.ValidationError(
        "If none of the preceding statements are true, you cannot invest in this deal.  Please contact the deal organizer.",
        null,
        "certify10"
      );
    } else if (
      obj.certify10 &&
      country !== "United States of America" &&
      regType !== "REGULATION_D"
    ) {
      return true;
    }

    if ((obj.certify0 || obj.certify1 || obj.certify2) && !obj.certify10) {
      return true;
    }

    return new Yup.ValidationError(
      "Please select one of the applicable statement.",
      null,
      "certify0"
    );
  };

  let validation = Yup.object().shape(yupObject);

  if (purchaserFeature) {
    // This function tests QP, then Accred if needed.
    // The purpose being you can skip accred checkboxes
    // If you are a QP
    let individualQPOverridesAccredTest = (obj) => {
      // If a QP option is chosen
      const qpTestResult = qpMeta.oneOfTest(obj);
      if (qpTestResult === true) {
        return true;
      }

      // Validate the QP step regularly (allows none apply option)
      const qpSecondaryTest = purchaserMeta.oneOfTest(obj);
      if (qpSecondaryTest !== true) {
        return qpSecondaryTest;
      }

      return individualAccreditationTest(obj);
    };

    validation = validation.test(
      "PurchaserTest",
      null,
      individualQPOverridesAccredTest
    );
  } else {
    validation = validation.test(
      "CertificationTest",
      null,
      individualAccreditationTest
    );
  }
  //.test("dateOfBirthGate", "dateOfBirthGate", (obj) => {
  //  let ecd = new Date(`${obj.year}-${obj.month}-${obj.day}`);
  //  let today = new Date();
  //  if (moment(ecd).isBefore(today.setFullYear(today.getFullYear() - 18))) {
  //    return true;
  //  } else {
  //    return new Yup.ValidationError("You must be 18 or older", null, "year");
  //  }
  //});

  const handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = accordionState;
    const newIndex = activeIndex === index ? -1 : index;

    setAccordionState({ activeIndex: newIndex });
  };

  return (
    <>
      <Formik
        initialValues={{
          countryOfResidence: "United States",
          registrationType: "INDIVIDUAL",
          ...SubformAccreditationMeta.defaultValues,
          ...SubformFirstLastNameMeta.defaultValues,
          ...SubformPersonTaxIdMeta.defaultValues,
          ...SubformDateOfBirthMeta.defaultValues,
          ...{ ...SubformAddressMeta.defaultValues, ...{ country: country } },
          email: "",
          regType: regType,
          entityCountry: country,
          ...SubformPhoneMeta.defaultValues,
          displayName: "",
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
        innerRef={formikRef}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <WizardSteps currentStep={stepNumber}>
              <ProfileWizardStep
                submitting={submitting}
                stepNumber={stepNumber}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.firstName ||
                  !props.touched.lastName ||
                  !props.touched?.day ||
                  !props.touched?.month ||
                  !props.touched?.year ||
                  !(
                    !props.errors?.displayName &&
                    !props.errors?.firstName &&
                    !props.errors?.lastName &&
                    !props.errors?.dateOfBirth &&
                    !props.errors?.month &&
                    !props.errors?.day &&
                    !props.errors?.year &&
                    !props.errors?.email &&
                    !props.errors?.address1 &&
                    !props.errors?.city &&
                    !props.errors?.state &&
                    !props.errors?.country &&
                    !props.errors?.postalCode
                  )
                }
                header="Profile Basics"
              >
                <Form.Group widths="equal">
                  <FormField
                    id={`displayName`}
                    name={`displayName`}
                    label="Display Name"
                    placeholder="Display Name"
                    data-testid={"displayName"}
                    required
                    {...props}
                  />
                </Form.Group>

                <Form.Group widths="equal">
                  <SubformFirstLastName />
                </Form.Group>

                <Form.Group widths="equal">
                  <FormField
                    label={"Contact Email"}
                    id="email"
                    name="email"
                    placeholder="email@example.com"
                    required
                  />
                  <SubformPhone required={true} />
                </Form.Group>
                <Form.Group widths="equal">
                  <SubformDateOfBirth />
                </Form.Group>
                <Divider />
                <SubformAddress {...props} />
              </ProfileWizardStep>
              <ProfileWizardStep
                submitting={submitting}
                stepNumber={stepNumber}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !(
                    (props.values.isUSBased === "No" ||
                      props.values.isUSBased === "") &&
                    props.values.hasTaxId === "No" &&
                    props.values.hasId === "No"
                  ) &&
                  (!props.touched.taxId || Boolean(props.errors.taxId))
                }
                header="Identity"
              >
                <SubformPersonTaxId {...props} />
              </ProfileWizardStep>
              <ProfileWizardStep
                submitting={submitting}
                stepNumber={stepNumber}
                onHandleSection={onHandleSection}
                nextDisabled={
                  purchaserMeta.oneOfTest(props.values) !== true ||
                  !props.isValid
                }
                canSubmitForm={props.isValid}
                header="Purchaser/Accreditation"
              >
                <Segment inverted>
                  <Accordion inverted>
                    <AccordionTitle
                      active={accordionState.activeIndex === 0}
                      index={0}
                      onClick={handleAccordionClick}
                    >
                      <Header as="h3" inverted>
                        <Icon name="dropdown" />
                        Qualified Purchaser
                        <Header as="h4" inverted>
                          Select how you qualify as a qualified purchaser from
                          the following qualification criteria
                        </Header>
                      </Header>
                    </AccordionTitle>
                    <AccordionContent active={accordionState.activeIndex === 0}>
                      <SubformPurchaserStatus />
                    </AccordionContent>

                    <Divider />
                    {!isQualifiedPurchaserForm && (
                      <>
                        <AccordionTitle
                          active={accordionState.activeIndex === 1}
                          index={1}
                          onClick={handleAccordionClick}
                        >
                          <Header as="h3" inverted>
                            <Icon name="dropdown" />
                            Investor Accreditation
                            <Header as="h4" inverted>
                              Select how you qualify as an accredited investor
                              from the following qualification criteria
                            </Header>
                          </Header>
                        </AccordionTitle>
                        <AccordionContent
                          active={accordionState.activeIndex === 1}
                        >
                          {regType === "REGULATION_S" ? (
                            <FormField
                              id="certifyX"
                              name="certifyX"
                              component={Form.Checkbox}
                              label="Individual is not accredited"
                            />
                          ) : (
                            <SubformAccreditation />
                          )}
                        </AccordionContent>
                      </>
                    )}
                  </Accordion>
                </Segment>
              </ProfileWizardStep>
            </WizardSteps>
          </Form>
        )}
      </Formik>
    </>
  );
}
