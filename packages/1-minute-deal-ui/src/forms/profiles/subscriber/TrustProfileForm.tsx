import React, { useState } from "react";
import {
  Form,
  Header,
  Icon,
  AccordionTitle,
  AccordionContent,
  Segment,
  Accordion,
  Divider,
} from "semantic-ui-react";
import * as Yup from "yup";
import { Formik } from "formik";
import { FormField } from "../../common/FormField";

import SubformTrustTaxId, * as SubformTrustTaxIdMeta from "../../subforms/SubformTrustTaxId";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import SubformSigner, * as SubformSignerMeta from "../../subforms/SubformSigner";
import SubformPhone, * as SubformPhoneMeta from "../../../forms/subforms/SubformPhone";

import SubformPurchaserStatus, {
  getPurchaserStatusMeta,
} from "../../../forms/subforms/SubformPurchaserStatus";
import { useFeatureFlag } from "../../../components/featureflags/FeatureFlags";
import { useLoggedInUser } from "../../../hooks/userHooks";
import WizardSteps from "../../../components/common/WizardSteps";
import { ProfileWizardStep } from "../../../components/profiles/ProfileWizardActions";
import { Box } from "@mui/material";
import AdditionalSignersInvite, * as AdditionalSignersInviteMeta from "../../../components/profiles/AdditionalSignersInvite";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  stepNumber: number;
  regType: string;
  country?: any;
  isQualifiedPurchaserForm?: boolean;
}

export default function TrustProfileForm({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
  regType = "REGULATION_D",
  country = "United States of America",
  isQualifiedPurchaserForm = false,
}: Props) {
  const loggedInUser = useLoggedInUser();
  const purchaserFeature = useFeatureFlag("qualified_purchaser");
  const totalSections = purchaserFeature ? 5 : 4;
  const purchaserMeta = getPurchaserStatusMeta(isQualifiedPurchaserForm);
  const qpMeta = getPurchaserStatusMeta(true);
  const [accordionState, setAccordionState] = useState<{ activeIndex: number }>(
    { activeIndex: -1 }
  );

  let yupObject = {
    displayName: Yup.string()
      .trim()
      .min(2)
      .max(64, "Display Name is limited to 64 characters")
      .required(),
    name: Yup.string().trim().min(2).max(1024).required(),
    ...SubformTrustTaxIdMeta.validationSchema({ isRequired: false }),
    ...SubformAddressMeta.validationSchema(),
    ...SubformSignerMeta.validationSchema(),
    email: Yup.string().email().required(),
    //  ...SubformSigner2Meta.validationSchema(),
    //  ...SubformBOwnerMeta.validationSchema(),
    ...(!isQualifiedPurchaserForm
      ? {
        certify3: Yup.boolean(),
        certify9: Yup.boolean(),
        certify10: Yup.boolean(),
      }
      : {}),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    ...AdditionalSignersInviteMeta.validationSchema(0, false),
  };

  yupObject = {
    ...yupObject,
    primarySignatorytitle: yupObject.primarySignatorytitle.required(),
  };
  if (purchaserFeature) {
    yupObject = {
      ...yupObject,
      ...purchaserMeta.validationSchema,
    };
  }

  let trustAccreditationTest = (obj) => {
    if (regType === "REGULATION_S") return true;

    if (
      obj.certify10 &&
      (country === "United States of America" || regType === "REGULATION_D")
    ) {
      return new Yup.ValidationError(
        "If none of the preceding statments are true, you cannot invest in this deal.  Please contact the deal organizer.",
        null,
        "certify10"
      );
    } else if (
      obj.certify10 &&
      country !== "United States of America" &&
      regType !== "REGULATION_D"
    ) {
      return true;
    }

    if ((obj.certify3 || obj.certify9) && !obj.certify10) {
      return true; // everything is fine
    }

    return new Yup.ValidationError(
      "Please select one of the applicable statement.",
      null,
      "certify10"
    );
  };

  let validation = Yup.object().shape(yupObject);

  if (purchaserFeature) {
    // This function tests QP, then Accred if needed.
    // The purpose being you can skip accred checkboxes
    // If you are a QP
    let trustQPOverridesAccredTest = (obj) => {
      // If a QP option is chosen
      const qpTestResult = qpMeta.oneOfTest(obj);
      if (qpTestResult === true) {
        return true;
      }

      // Validate the QP step regularly (allows none apply option)
      const qpSecondaryTest = purchaserMeta.oneOfTest(obj);
      if (qpSecondaryTest !== true) {
        return qpSecondaryTest;
      }

      return trustAccreditationTest(obj);
    };

    validation = validation.test(
      "PurchaserTest",
      null,
      trustQPOverridesAccredTest
    );
  } else {
    validation = validation.test(
      "CertificationTest",
      null,
      trustAccreditationTest
    );
  }

  const handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = accordionState;
    const newIndex = activeIndex === index ? -1 : index;

    setAccordionState({ activeIndex: newIndex });
  };

  const isInvalidSelectionInQP = (props) => {
    const qpTestInvalid =
      purchaserMeta.oneOfTest(props.values) !== true ||
      props.values.purchaserStatus8;
    if (isQualifiedPurchaserForm) {
      return qpTestInvalid;
    } else {
      return qpTestInvalid
        ? (!props.values.certify3 && !props.values.certify9) ||
        props.values.certify10
        : qpTestInvalid;
    }
  };

  //disabled={
  //  !props.touched.name ||
  //  !(!props.errors.name && !props.errors.email)
  //}
  return (
    <>
      <Formik
        initialValues={{
          displayName: "",
          name: "",
          registrationType: "TRUST",
          isUSBased: "Yes",
          email: "",
          regType: regType,
          entityCountry: country,

          ...SubformTrustTaxIdMeta.defaultValues,
          ...{ ...SubformAddressMeta.defaultValues, ...{ country: country } },
          ...SubformSignerMeta.defaultValues,
          //  ...SubformSigner2Meta.defaultValues,
          //  ...SubformBOwnerMeta.defaultValues,
          certify3: false,
          certify9: false,
          certify10: false,
          phone: loggedInUser?.attributes?.phone[0] ?? "",
          primarySignatory: {
            country: country,
          },
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <WizardSteps currentStep={stepNumber}>
              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.name ||
                  !(
                    !props.errors.displayName &&
                    !props.errors.name &&
                    !props.errors.email &&
                    !props.errors.address1 &&
                    !props.errors.city &&
                    !props.errors.state &&
                    !props.errors.country &&
                    !props.errors.postalCode
                  )
                }
                header="Profile Basics"
              >
                <Form.Group widths="equal">
                  <FormField
                    id={`displayName`}
                    name={`displayName`}
                    label="Display Name"
                    placeholder="Display Name"
                    data-testid={"displayName"}
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <FormField
                    id="name"
                    name="name"
                    label="Trust Account Name"
                    placeholder="Trust Legal Name"
                    required
                  />
                </Form.Group>
                <Form.Group widths={"equal"}>
                  <FormField
                    label={"Contact Email"}
                    id="email"
                    name="email"
                    placeholder="email@example.com"
                    required
                  />
                  <SubformPhone required={true} />
                </Form.Group>
                <SubformAddress {...props} />
              </ProfileWizardStep>

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !(props.values.hasTaxId === "No") &&
                  (!props.touched.taxId || Boolean(props.errors.taxId))
                }
                header="Tax Identification"
              >
                <SubformTrustTaxId {...props} />
              </ProfileWizardStep>

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.primarySignatoryfirstName ||
                  !(
                    !props.errors.primarySignatoryfirstName &&
                    !props.errors.primarySignatorylastName &&
                    !props.errors.primarySignatorytitle &&
                    !props.errors.primarySignatoryAddress
                  )
                }
                header="Authorized Signatory"
              >
                <SubformSigner {...props} />
                <Box mb={2}>
                  <Box pt={2} px={2}>
                    <Header as="h3" inverted>
                      Additional Signers
                    </Header>
                  </Box>
                  <Box p={2}>
                    <AdditionalSignersInvite minSigners={0} maxSigners={25} />
                  </Box>
                </Box>
              </ProfileWizardStep>

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={isInvalidSelectionInQP(props)}
                canSubmitForm={props.isValid}
                header="Purchaser/Accreditation"
              >
                <Segment inverted>
                  <Accordion inverted>
                    <AccordionTitle
                      active={accordionState.activeIndex === 0}
                      index={0}
                      onClick={handleAccordionClick}
                    >
                      <Header as="h3" inverted>
                        <Icon name="dropdown" />
                        Qualified Purchaser
                        <Header as="h4" inverted>
                          Select how you qualify as a qualified purchaser from
                          the following qualification criteria
                        </Header>
                      </Header>
                    </AccordionTitle>
                    <AccordionContent active={accordionState.activeIndex === 0}>
                      <SubformPurchaserStatus />
                    </AccordionContent>

                    <Divider />
                    {!isQualifiedPurchaserForm && (
                      <>
                        <AccordionTitle
                          active={accordionState.activeIndex === 1}
                          index={1}
                          onClick={handleAccordionClick}
                        >
                          <Header as="h3" inverted>
                            <Icon name="dropdown" />
                            Investor Accreditation
                            <Header as="h4" inverted>
                              Select how you qualify as an accredited investor
                              from the following qualification criteria
                            </Header>
                          </Header>
                        </AccordionTitle>
                        <AccordionContent
                          active={accordionState.activeIndex === 1}
                        >
                          {regType === "REGULATION_S" ? (
                            <FormField
                              id="certifyX"
                              name="certifyX"
                              component={Form.Checkbox}
                              label="Trust is not accredited"
                            />
                          ) : (
                            <>
                              <FormField
                                id="certify3"
                                name="certify3"
                                component={Form.Checkbox}
                                label="Subscriber is an irrevocable trust with total assets in excess of $5,000,000 whose purchase is directed by a person with such knowledge and experience in financial and business matters that such person is capable of evaluating the merits and risks of the prospective investment."
                              />
                              <FormField
                                id="certify9"
                                name="certify9"
                                component={Form.Checkbox}
                                label="Subscriber is an entity in which all of the equity owners, or a grantor or revocable trust in which all of the grantors and trustees, qualify under any clause below."
                              />
                              <FormField
                                id="certify0"
                                name="certify0"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    If an individual, Subscriber has a net
                                    worth, either individually or upon a joint
                                    basis withSubscriber&apos;s spouse, of at
                                    least $1,000,000, or has had an individual
                                    income in excess of $200,000 for each of the
                                    two most recent years, or a joint income
                                    with Subscriber&apos;s spouse in excess of
                                    $300,000 in each of those years, and has a
                                    reasonable expectation of reaching the same
                                    income level in the current year.
                                  </>
                                }
                              />
                              <FormField
                                id="certify1"
                                name="certify1"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    If an individual, Subscriber holds, in good
                                    standing, one of the following
                                    certifications or designations administered
                                    by the Financial Industry Regulatory
                                    Authority, Inc. (&#x201C;FINRA&#x201D;): the
                                    Licensed General Securities Representative
                                    (Series 7), Licensed Investment Adviser
                                    Representative (Series 65), or Licensed
                                    Private Securities Offerings Representative
                                    (Series 82).
                                  </>
                                }
                              />
                              <FormField
                                id="certify2"
                                name="certify2"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    If an individual, Subscriber is a
                                    &#x201C;knowledgeable employee&#x201D;
                                    within the meaning prescribed under Rule
                                    3c-5(a)(4) of the Investment Company Act of
                                    1940, as amended (the &#x201C;Investment
                                    Company Act&#x201D;) of a private fund
                                    exempt from registration pursuant to Rule
                                    3(c)(1) or Rule 3(c)(7) of the Investment
                                    Company Act.
                                  </>
                                }
                              />
                              <FormField
                                id="certify4"
                                name="certify4"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    Subscriber is a bank, insurance company,
                                    investment company registered under the
                                    Company Act, a broker or dealer registered
                                    pursuant to Section 15 of the Securities
                                    Exchange Act of 1934 (the &#x22;Exchange
                                    Act&#x22;), a state-registered or
                                    SEC-registered investment adviser, an exempt
                                    reporting adviser pursuant to Section203(l)
                                    or 203(m) of the United States Investment
                                    Advisers Act of 1940, as amended (the
                                    &#x201C;Advisers Act&#x201D;), a rural
                                    business investment company (RBIC) as
                                    defined in Section 384A of the Consolidated
                                    Farm and Rural Development Act, a business
                                    development company, a Small Business
                                    Investment Company licensed by the United
                                    States Small Business Administration, a plan
                                    with total assets in excess of $5,000,000
                                    established and maintained by a state for
                                    the benefit of its employees, or a private
                                    business development company as defined in
                                    Section 202(a)(22) of the Advisers Act.
                                  </>
                                }
                              />
                              <FormField
                                id="certify5"
                                name="certify5"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    Subscriber is an employee benefit plan and
                                    either all investment decisions are made by
                                    a bank, savings and loan association,
                                    insurance company, or registered investment
                                    advisor, or Subscriber has total assets in
                                    excess of $5,000,000 or, if such plan is a
                                    self-directed plan, investment decisions are
                                    made solely by persons who are accredited
                                    investors.
                                  </>
                                }
                              />
                              <FormField
                                id="certify6"
                                name="certify6"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    Subscriber is an Indian tribe, governmental
                                    body, or an entity organized under the law
                                    of a foreign county, that owns investments,
                                    as defined in Rule 2a51-1(b) of the
                                    Investment Company Act, in excess of
                                    $5,000,000.00, and was not formed for the
                                    specific purpose of investing in the
                                    securities offered.
                                  </>
                                }
                              />
                              <FormField
                                id="certify7"
                                name="certify7"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    Subscriber is a family office, as defined in
                                    Rule 202(a)(11)(G)-1 of the Advisers Act
                                    (the &#x201C;Family Office Rule&#x201D;, or
                                    a &#x201C;family client&#x201D; of such
                                    family office as such term is defined in the
                                    Family Office Rule: (a) with assets under
                                    management in excess of $5,000,000.00, (b)
                                    not formed for the purpose of acquiring the
                                    Interests, and (c) the acquisition of the
                                    Interests is directed by a person who has
                                    such knowledge and experience in financial
                                    and business matters that he or she is
                                    capable of evaluating the merits and risks
                                    of acquiring the Interests. &#x9;
                                  </>
                                }
                              />
                              <FormField
                                id="certify8"
                                name="certify8"
                                component={Segment}
                                inverted
                                className={"no-padding"}
                                content={
                                  <>
                                    <Icon name="circle" size="tiny" />
                                    Subscriber is a corporation, partnership,
                                    limited liability company or business trust,
                                    not formed for the purpose of acquiring the
                                    Interests, or an organization described in
                                    Section 501(c)(3) of the Code, in each case
                                    with total assets in excess of $5,000,000.
                                  </>
                                }
                              />
                              <FormField
                                id="certify10"
                                name="certify10"
                                component={Form.Checkbox}
                                label="Subscriber cannot make any of the above representations."
                              />
                            </>
                          )}
                        </AccordionContent>
                      </>
                    )}
                  </Accordion>
                </Segment>
              </ProfileWizardStep>
            </WizardSteps>
          </Form>
        )}
      </Formik>
    </>
  );
}
