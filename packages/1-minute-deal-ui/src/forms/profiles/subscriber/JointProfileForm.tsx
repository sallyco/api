import React, { useState } from "react";
import {
  Form,
  Header,
  Icon,
  AccordionTitle,
  AccordionContent,
  Segment,
  Accordion,
  Divider,
} from "semantic-ui-react";
import * as Yup from "yup";
import { Formik } from "formik";
import { FormField } from "../../common/FormField";
import { JointTypes } from "../../../tools/enums";

import * as SubformTrustTaxIdMeta from "../../subforms/SubformTrustTaxId";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import SubformSigner, * as SubformSignerMeta from "../../subforms/SubformSigner";
import SubformPhone, * as SubformPhoneMeta from "../../../forms/subforms/SubformPhone";

import SubformPurchaserStatus, {
  getPurchaserStatusMeta,
} from "../../../forms/subforms/SubformPurchaserStatus";
import { useFeatureFlag } from "../../../components/featureflags/FeatureFlags";
import { useLoggedInUser } from "../../../hooks/userHooks";
import WizardSteps from "../../../components/common/WizardSteps";
import { ProfileWizardStep } from "../../../components/profiles/ProfileWizardActions";
import AdditionalSignersInvite, * as AdditionalSignersInviteMeta from "../../../components/profiles/AdditionalSignersInvite";
import { Box } from "@mui/material";
import SubformDateOfBirth from "../../subforms/SubformDateOfBirth";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  stepNumber: number;
  regType: string;
  country?: any;
  isQualifiedPurchaserForm?: boolean;
}

export default function JointProfileForm({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
  regType = "REGULATION_D",
  country = "United States of America",
  isQualifiedPurchaserForm = false,
}: Props) {
  const loggedInUser = useLoggedInUser();
  const purchaserFeature = useFeatureFlag("qualified_purchaser");
  const purchaserMeta = getPurchaserStatusMeta(isQualifiedPurchaserForm);
  const qpMeta = getPurchaserStatusMeta(true);
  const [accordionState, setAccordionState] = useState<{ activeIndex: number }>(
    { activeIndex: -1 }
  );

  let yupObject = {
    displayName: Yup.string()
      .trim()
      .min(2)
      .max(64, "Display Name is limited to 64 characters")
      .required("Display Name is Required"),
    name: Yup.string().trim().min(2).max(1024).required("Name is Required"),
    ...SubformSignerMeta.validationSchema(),
    email: Yup.string().email().required(),
    //  ...SubformSigner2Meta.validationSchema(),
    //  ...SubformBOwnerMeta.validationSchema(),
    ...(!isQualifiedPurchaserForm
      ? {
          certify2: Yup.boolean(),
          certify3: Yup.boolean(),
          certify4: Yup.boolean(),
          certify5: Yup.boolean(),
          certify6: Yup.boolean(),
          certify7: Yup.boolean(),
        }
      : {}),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    ...AdditionalSignersInviteMeta.validationSchema(0, false),
  };

  if (purchaserFeature) {
    yupObject = {
      ...yupObject,
      ...purchaserMeta.validationSchema,
    };
  }

  let jointAccreditationTest = (obj) => {
    if (obj.certify7) {
      return new Yup.ValidationError(
        "If none of the preceding statements are true, you cannot invest in this deal.  Please contact the deal organizer.",
        null,
        "certify7"
      );
    }

    if (
      (obj.certify2 ||
        obj.certify3 ||
        obj.certify4 ||
        obj.certify5 ||
        obj.certify6) &&
      !obj.certify7
    ) {
      return true; // everything is fine
    }

    return new Yup.ValidationError(
      "Please select one of the applicable statement.",
      null,
      "certify3"
    );
  };

  let validation = Yup.object().shape(yupObject);

  if (purchaserFeature) {
    // This function tests QP, then Accred if needed.
    // The purpose being you can skip accred checkboxes
    // If you are a QP
    const jointQPOverridesAccredTest = (obj) => {
      // If a QP option is chosen
      const qpTestResult = qpMeta.oneOfTest(obj);
      if (qpTestResult === true) {
        return true;
      }

      // Validate the QP step regularly (allows none apply option)
      const qpSecondaryTest = purchaserMeta.oneOfTest(obj);
      if (qpSecondaryTest !== true) {
        return qpSecondaryTest;
      }

      return jointAccreditationTest(obj);
    };

    validation = validation.test(
      "PurchaserTest",
      null,
      jointQPOverridesAccredTest
    );
  } else {
    validation = validation.test(
      "CertificationTest",
      null,
      jointAccreditationTest
    );
  }

  const handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = accordionState;
    const newIndex = activeIndex === index ? -1 : index;

    setAccordionState({ activeIndex: newIndex });
  };

  const isInvalidSelectionInQP = (props) => {
    const qpTestInvalid =
      purchaserMeta.oneOfTest(props.values) !== true ||
      props.values.purchaserStatus8;
    if (isQualifiedPurchaserForm) {
      return qpTestInvalid;
    } else {
      return qpTestInvalid
        ? (!props.values?.certify2 &&
            !props.values.certify3 &&
            !props.values.certify4 &&
            !props.values.certify5 &&
            !props.values.certify6) ||
            props.values.certify7
        : qpTestInvalid;
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          displayName: "",
          name: "",
          registrationType: "JOINT",
          isUSBased: "Yes",
          email: "",
          // ...SubformAddressMeta.defaultValues,
          ...SubformSignerMeta.defaultValues,
          certify2: false,
          certify3: false,
          certify4: false,
          certify5: false,
          certify6: false,
          certify7: false,
          phone: loggedInUser?.attributes?.phone[0] ?? "",
          entityCountry: country,
          signers: [],
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <WizardSteps currentStep={stepNumber}>
              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.name ||
                  !(
                    !props.errors.displayName &&
                    !props.errors.name &&
                    !props.errors.email
                  )
                }
                header="Account Basics"
              >
                <Form.Group widths="equal">
                  <FormField
                    id={`displayName`}
                    name={`displayName`}
                    label="Display Name"
                    placeholder="Display Name"
                    data-testid={"displayName"}
                    required
                  />
                </Form.Group>
                <div style={{ minHeight: 100 }}>
                  <Form.Group widths="equal">
                    <FormField
                      id="name"
                      name="name"
                      label="Joint Account Name"
                      placeholder="Joint Account Legal Name"
                      required
                    />
                    <FormField
                      id="jointType"
                      name="jointType"
                      component={Form.Select}
                      label="Joint Type"
                      options={JointTypes}
                      placeholder="Select a Type"
                    />
                  </Form.Group>
                </div>
                <Form.Group widths={"equal"}>
                  <FormField
                    label={"Contact Email"}
                    id="email"
                    name="email"
                    placeholder="email@example.com"
                    required
                  />
                </Form.Group>
                <Form.Group widths={"equal"}>
                  <SubformPhone required={true} />
                </Form.Group>
              </ProfileWizardStep>

              {/*<ProfileWizardStep*/}
              {/*  stepNumber={stepNumber}*/}
              {/*  onHandleSection={onHandleSection}*/}
              {/*  nextDisabled={*/}
              {/*    !props.touched.name ||*/}
              {/*    !![*/}
              {/*      "address1",*/}
              {/*      "address2",*/}
              {/*      "region",*/}
              {/*      "city",*/}
              {/*      "postalCode",*/}
              {/*      "country",*/}
              {/*    ].find((prop) => prop in props.errors)*/}
              {/*  }*/}
              {/*  header="Joint Primary Address"*/}
              {/*>*/}
              {/*  <Form.Group widths="equal">*/}
              {/*    <SubformDateOfBirth />*/}
              {/*  </Form.Group>*/}
              {/*  <SubformAddress {...props} />*/}
              {/*</ProfileWizardStep>*/}

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={
                  !props.touched.primarySignatoryfirstName ||
                  !(
                    !props.errors.primarySignatoryfirstName &&
                    !props.errors.primarySignatorylastName &&
                    !props.errors.primarySignatorytitle &&
                    !props.errors.primarySignatoryAddress &&
                    !props.errors.primarySignatorytaxId
                  )
                }
                header="Primary Signatory (Required)"
              >
                <SubformSigner {...props} />
                <Box mb={2}>
                  <Box pt={2} px={2}>
                    <Header as="h3" inverted>
                      Additional Signers
                    </Header>
                  </Box>
                  <Box p={2}>
                    <AdditionalSignersInvite minSigners={1} maxSigners={25} />
                  </Box>
                </Box>
              </ProfileWizardStep>

              <ProfileWizardStep
                stepNumber={stepNumber}
                submitting={submitting}
                onHandleSection={onHandleSection}
                nextDisabled={isInvalidSelectionInQP(props)}
                canSubmitForm={props.isValid}
                header="Purchaser/Accreditation"
              >
                <Segment inverted>
                  <Accordion inverted>
                    <AccordionTitle
                      active={accordionState.activeIndex === 0}
                      index={0}
                      onClick={handleAccordionClick}
                    >
                      <Header as="h3" inverted>
                        <Icon name="dropdown" />
                        Qualified Purchaser
                        <Header as="h4" inverted>
                          Select how you qualify as a qualified purchaser from
                          the following qualification criteria
                        </Header>
                      </Header>
                    </AccordionTitle>
                    <AccordionContent active={accordionState.activeIndex === 0}>
                      <SubformPurchaserStatus />
                    </AccordionContent>

                    <Divider />
                    {!isQualifiedPurchaserForm && (
                      <>
                        <AccordionTitle
                          active={accordionState.activeIndex === 1}
                          index={1}
                          onClick={handleAccordionClick}
                        >
                          <Header as="h3" inverted>
                            <Icon name="dropdown" />
                            Investor Accreditation
                            <Header as="h4" inverted>
                              Select how you qualify as an accredited investor
                              from the following qualification criteria
                            </Header>
                          </Header>
                        </AccordionTitle>
                        <AccordionContent
                          active={accordionState.activeIndex === 1}
                        >
                          <FormField
                            id="certify2"
                            name="certify2"
                            component={Form.Checkbox}
                            label="Investor is an irrevocable trust with total assets in excess of $5,000,000 whose purchase is directed by a person with such knowledge and experience in financial and business matters that such person is capable of evaluating the merits and risks of the prospective investment."
                          />
                          <FormField
                            id="certify3"
                            name="certify3"
                            component={Form.Checkbox}
                            label="Investor is a bank, insurance company, investment company registered under the Company Act, a broker or dealer registered pursuant to Section 15 of the Securities Exchange Act of 1934 (the “Exchange Act”), a business development company, a Small Business Investment Company licensed by the United States Small Business Administration, a plan with total assets in excess of $5,000,000 established and maintained by a state for the benefit of its employees, or a private business development company as defined in Section 202(a)(22) of the United States Investment Advisers Act of 1940, as amended."
                          />
                          <FormField
                            id="certify4"
                            name="certify4"
                            component={Form.Checkbox}
                            label="Investor is an employee benefit plan and either all investment decisions are made by a bank, savings and loan association, insurance company, or registered investment advisor, or investor has total assets in excess of $5,000,000 or, if such plan is a self-directed plan, investment decisions are made solely by persons who are accredited investors."
                          />
                          <FormField
                            id="certify5"
                            name="certify5"
                            component={Form.Checkbox}
                            label="Investor is a corporation, partnership, limited liability company or business trust, not formed for the purpose of acquiring the Interests, or an organization described in Section 501(c)(3) of the country, in each case with total assets in excess of $5,000,000."
                          />
                          <FormField
                            id="certify6"
                            name="certify6"
                            component={Form.Checkbox}
                            label="Subscriber is an entity in which all of the equity owners, or a grantor or revocable trust in which all of the grantors and trustees, qualify under clause (i), (ii), (iii), (iv) or (v) above or this clause (vi)."
                          />
                          <FormField
                            id="certify7"
                            name="certify7"
                            component={Form.Checkbox}
                            label="Investor cannot make any of the above representations."
                          />
                        </AccordionContent>
                      </>
                    )}
                  </Accordion>
                </Segment>
              </ProfileWizardStep>
            </WizardSteps>
          </Form>
        )}
      </Formik>
    </>
  );
}
