import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState } from "../../../rootReducer";
import { fetchSubscriptionById } from "../../../slices/subscriptionsSlice";
import {
  fetchProfileById,
  updateBankingUser,
} from "../../../slices/profilesSlice";
import { Formik, useFormikContext } from "formik";
import * as Yup from "yup";
import {
  Button,
  Image,
  Placeholder,
  Card,
  Loader,
  Icon,
  Form,
  Image as SImage,
  Message,
} from "semantic-ui-react";
import {
  FileSizes,
  SubformFileUpload,
  UploadPreview,
} from "../../subforms/SubformFileUpload";
import { toast } from "react-toastify";

export default function ProfileBankingUserForm() {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();

  const { isLoading: subscriptionsLoading } = useSelector(
    (state: RootState) => state.subscriptions
  );

  const { isLoading: profilesLoading, error: profileError } = useSelector(
    (state: RootState) => state.profiles
  );

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const profile = useSelector(
    (state: RootState) => state.profiles.profilesById[subscription.profileId]
  );

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !profile) {
      dispatch(fetchProfileById(subscription.profileId));
    }
  }, [subscription, profile]);

  const validationSchema = useCallback(() => {
    if (!profile) {
      return Yup.object().shape({});
    }
    return Yup.object().shape({});
    //   profile?.typeOfEntity
    //     ? {
    //         EIN_DOC: Yup.string().required(),
    //         BYLAWS_DOC: Yup.string().required(),
    //         AOI: Yup.string().required(),
    //         primarySignatoryGOV_ID: Yup.string().required(),
    //       }
    //     : {
    //         GOVT_ID: Yup.string().required(),
    //       }
    // );
  }, [profile]);

  return (
    <>
      {!subscriptionsLoading && !profilesLoading && profile && (
        <>
          {!profile.bankingUser && (
            <>
              <Formik
                initialValues={{
                  GOVT_ID: "",
                  EIN_DOC: "",
                  BYLAWS_DOC: "",
                  AOI: "",
                  primarySignatoryGOVT_ID: "",
                }}
                enableReinitialize={false}
                validationSchema={validationSchema}
                onSubmit={async (data) => {
                  if (!profile.typeOfEntity) {
                    await dispatch(
                      updateBankingUser(profile.id, {
                        GOVT_ID: data.GOVT_ID,
                      })
                    );
                  } else {
                    await dispatch(
                      updateBankingUser(profile.id, {
                        entityDocumentation: {
                          EIN_DOC: data.EIN_DOC,
                          BYLAWS_DOC: data.BYLAWS_DOC,
                          AOI: data.AOI,
                        },
                        primarySignatory: {
                          GOVT_ID: data.primarySignatoryGOVT_ID,
                        },
                      })
                    );
                  }
                  await dispatch(fetchProfileById(profile.id));
                }}
              >
                {(formikProps) => (
                  <>
                    {profile.typeOfEntity && (
                      <>
                        <BankingDocumentUpload
                          label={"Upload EIN Documentation"}
                          fieldName={"EIN_DOC"}
                        />
                        <BankingDocumentUpload
                          label={"Upload Company Bylaws"}
                          fieldName={"BYLAWS_DOC"}
                        />
                        <BankingDocumentUpload
                          label={"Upload Articles of Incorporation"}
                          fieldName={"AOI"}
                        />
                        <BankingDocumentUpload
                          label={"Upload Primary Signatory Government ID"}
                          fieldName={"primarySignatoryGOVT_ID"}
                        />
                      </>
                    )}
                    {!profile.typeOfEntity && (
                      <>
                        <BankingDocumentUpload
                          label={"Upload Government ID"}
                          fieldName={"GOVT_ID"}
                        />
                      </>
                    )}
                    <Button
                      type={"submit"}
                      onClick={(e) => formikProps.submitForm()}
                      secondary
                      loading={profilesLoading}
                      disabled={
                        profilesLoading ||
                        !formikProps.dirty ||
                        !formikProps.isValid
                      }
                      content="Create Bank Account"
                    />
                    {profileError && (
                      <Message error>{profileError.data}</Message>
                    )}
                  </>
                )}
              </Formik>
            </>
          )}
        </>
      )}
    </>
  );
}

const BankingDocumentUpload = function (props: {
  fieldName: string;
  label: string;
}) {
  const formikContext = useFormikContext();
  return (
    <SubformFileUpload
      dataUriOnly
      maxSize={FileSizes.MB * 50}
      onError={(errors) => {
        errors.forEach((error) => {
          toast.error(error);
        });
      }}
      onSuccess={(fileUris) => {
        formikContext.setFieldValue(props.fieldName, fileUris[0]);
      }}
    >
      {(uploadProps) => {
        return (
          <>
            <Form.Group widths="equal">
              <Form.Field width={4}>
                {uploadProps?.filePreviews[0]?.dataUri ? (
                  <UploadPreview />
                ) : (
                  <Icon.Group size="huge">
                    <Icon className={"primaryColor"} name="upload" />
                    <Icon
                      corner
                      className={"primaryColor"}
                      name="arrow alternate circle up outline"
                    />
                  </Icon.Group>
                )}
              </Form.Field>
              <Form.Button
                fluid
                width={4}
                secondary
                onClick={uploadProps.triggerFileUpload}
                content={props.label}
                type="button"
              />
              <Form.Button
                fluid
                width={4}
                negative
                secondary
                disabled={!formikContext.values[props.fieldName]}
                onClick={() => {
                  uploadProps.clearUpload();
                  formikContext.setFieldValue(props.fieldName, undefined);
                }}
                content="Remove File"
                type="button"
              />
            </Form.Group>
          </>
        );
      }}
    </SubformFileUpload>
  );
};
