import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import {
  Form,
  Button,
  Divider,
  Header,
  Progress,
  Icon,
  Grid,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";
import Log from "../../tools/Log";
import { updateCompanyFileToServer } from "../../slices/filesSlice";
import { toast } from "react-toastify";
import { ToastError } from "../../tools/ToastMessage";
import { RootState } from "../../rootReducer";

interface Props {
  companyId: string;
  filetype: number;
  fileId: string;
  onSubmit: (any) => void;
}

export const PitchAndDeckFiles: React.FunctionComponent<Props> = ({
  companyId,
  filetype,
  fileId,
  onSubmit,
}) => {
  const validation = Yup.object().shape({});

  const dispatch = useDispatch();
  const [isError, setisError] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const [isError1, setisError1] = useState(false);
  const [errorMessage1, seterrorMessage1] = useState("");
  const [isError2, setisError2] = useState(false);
  const [errorMessage2, seterrorMessage2] = useState("");
  const fileInputRef: any = useRef();
  const fileInputRef1: any = useRef();
  const fileInputRef2: any = useRef();

  const { isSubmitting } = useSelector((state: RootState) => state.files);

  const fileChange = (e) => {
    var pitchDoc = e.target.files[0];
    if (pitchDoc.size <= 8388608) {
      setisError(false);
      seterrorMessage("");
    } else {
      setisError(true);
      seterrorMessage("Your file is too big");
    }
  };
  const fileChange1 = (e) => {
    var size1 = 0;
    var documents = e.target.files;
    if (documents.length === 1) {
      let file = documents[0];
      if (file.size <= 8388608) {
        setisError1(false);
        seterrorMessage1("");
      } else {
        setisError1(true);
        seterrorMessage1("Your file is too big");
      }
    } else {
      for (var i = 0; i < documents.length; i++) {
        let file = documents[i];
        size1 += file.size;
        if (size1 <= 8388608) {
          setisError1(false);
          seterrorMessage1("");
        } else {
          setisError1(true);
          seterrorMessage1("Your file is too big");
        }
      }
    }
  };

  async function presubmit(data: any) {
    try {
      const formData = new FormData();

      if (filetype === 1) {
        var pitchDoc = fileInputRef.current.files[0];
        if (pitchDoc) {
          if (pitchDoc.size <= 8388608) {
            setisError(false);
            seterrorMessage("");
            formData.append("pitchDoc", pitchDoc, pitchDoc.name);
            const company: any = await dispatch(
              updateCompanyFileToServer(formData, fileId, companyId)
            );
            Log.debug("profile", company);
            toast.success(`File "${fileId}" Updated`);
            onSubmit(true);
          } else {
            setisError(true);
            seterrorMessage("Your file is too big");
          }
        }
      }
      if (filetype === 2) {
        var size = 0;
        var documents = fileInputRef1.current.files[0];
        if (documents) {
          if (documents.size <= 8388608) {
            setisError1(false);
            seterrorMessage1("");
            formData.append("documents", documents, documents.name);
            const company: any = await dispatch(
              updateCompanyFileToServer(formData, fileId, companyId)
            );
            Log.debug("profile", company);
            toast.success(`File "${fileId}" Updated`);
            onSubmit(true);
          } else {
            setisError1(true);
            seterrorMessage1("Your file is too big");
          }
        }
      }
    } catch (e: any) {
      if (e.response) {
        ToastError(e.response);
      }
    }
  }
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Formik
        initialValues={{ profileform: 4 }}
        validationSchema={validation}
        onSubmit={presubmit}
      >
        {(props: any) => (
          <Form onSubmit={props.handleSubmit}>
            <Grid>
              <Grid.Column textAlign="center">
                {filetype === 1 ? (
                  <div className={`field`}>
                    <Form.Button
                      fluid
                      onClick={() => fileInputRef.current.click()}
                      content="UPLOAD A PITCH DESK"
                      type="button"
                      icon="upload"
                    />
                    <input
                      ref={fileInputRef}
                      type="file"
                      name="headshot"
                      hidden
                      onChange={fileChange}
                      accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"
                    />
                    {isError && (
                      <div className="ui pointing above prompt label">
                        {errorMessage}
                      </div>
                    )}
                  </div>
                ) : (
                  <div className={`field`}>
                    <Form.Button
                      fluid
                      onClick={() => fileInputRef1.current.click()}
                      content="UPLOAD SUPPORTING FILE"
                      type="button"
                      icon="paperclip"
                    />
                    <input
                      ref={fileInputRef1}
                      type="file"
                      name="headshot"
                      hidden
                      onChange={fileChange1}
                      accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"
                    />
                    {isError1 && (
                      <div className="ui pointing above prompt label">
                        {errorMessage1}
                      </div>
                    )}
                    <Header textAlign="left">
                      <Header.Subheader>
                        Supporting files may include items for due diligence
                        such as product one-pagers, marketing material, and
                        financial projections.
                      </Header.Subheader>
                    </Header>
                  </div>
                )}
                <Divider hidden />
              </Grid.Column>
            </Grid>
            <Button secondary type="submit" fluid loading={isSubmitting}>
              SAVE CHANGES
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default PitchAndDeckFiles;
