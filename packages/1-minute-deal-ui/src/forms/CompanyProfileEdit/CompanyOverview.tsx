import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { Form, Button, Divider, Grid } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";
import SubformDate, {
  validationSchema as DateValidationSchema,
  defaultValues as DateDefaultValues,
} from "../subforms/SubformDate";
import moment from "moment";

const incorporationtype = [
  {
    key: "1",
    value: "Limited Liability Company",
    text: "Limited Liability Company",
  },
  { key: "2", value: "Limited Partnership", text: "Limited Partnership" },
  { key: "3", value: "C Corporation", text: "C Corporation" },
  { key: "4", value: "S Corporation", text: "S Corporation" },
  { key: "5", value: "General Partnership", text: "General Partnership" },
  { key: "6", value: "Foreign Entity", text: "Foreign Entity" },
];

interface Props {
  company: any;
  self: any;
  onSubmit: (any) => void;
  submitting: boolean;
}

export const CompanyOverview: React.FunctionComponent<Props> = ({
  company,
  self,
  onSubmit,
  submitting = false,
}) => {
  const dispatch = useDispatch();

  const validation = Yup.object().shape({
    founder: Yup.string()

      .trim()
      .min(3)
      .required(),
    minInvestment: Yup.string()

      .trim()
      .required(),
    incorporationType: Yup.string().required(),
    ...DateValidationSchema({ isRequired: true }),
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Formik
        initialValues={{
          founder: company?.contact?.name,
          minInvestment: company?.preValuation,
          incorporationType: company?.incorporationType,
          ...DateDefaultValues,
          dateFounded: moment.utc(company?.dateFounded).format("YYYY-MM-DD"),
          month: moment.utc(company?.dateFounded).format("MM"),
          day: moment.utc(company?.dateFounded).format("DD"),
          year: moment.utc(company?.dateFounded).format("YYYY"),
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <Grid>
              <Grid.Row>
                <Grid.Column width={16}>
                  <FormField
                    name="founder"
                    component={Form.Input}
                    label="Founder"
                    placeholder="Venture BC,LLC"
                    required
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  <SubformDate
                    id={"dateFounded"}
                    label={"DATE FOUNDED"}
                    name={"dateFounded"}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={8}>
                  <FormField
                    name="minInvestment"
                    id="minInvestment"
                    component={Form.Input}
                    label="Min.Investment*"
                    placeholder="$20"
                    required
                  />
                </Grid.Column>

                <Grid.Column width={8}>
                  <FormField
                    name="incorporationType"
                    id="incorporationType"
                    component={Form.Select}
                    label="INCORPORATION TYPE*"
                    placeholder="LLC"
                    options={incorporationtype}
                    required
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Divider hidden />
            <Button secondary type="submit" loading={submitting} fluid>
              SAVE CHANGES
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default CompanyOverview;
