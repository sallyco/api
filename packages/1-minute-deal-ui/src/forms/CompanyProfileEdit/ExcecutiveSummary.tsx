import React, { useEffect } from "react";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button, Divider, Header } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";
import { RootState } from "../../rootReducer";
import { fetchAssetById } from "../../slices/assetsSlice";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  companyId: string;
}

export const ExcecutiveSummary: React.FunctionComponent<Props> = ({
  onSubmit,
  submitting = false,
  companyId = "",
}) => {
  const dispatch = useDispatch();
  const validation = Yup.object().shape({
    managementOverView: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    customerProblem: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    targetMarketSize: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    customerPotential: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    businessModel: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    competitiveAdvantage: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    productsServices: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    marketingStrategy: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
    competitors: Yup.string()

      .trim()
      .min(8)
      .max(800)
      .required(),
  });

  const company = useSelector(
    (state: RootState) => state.assets.assetsById[companyId]
  );

  useEffect(() => {
    if (!company || company.id !== companyId) {
      dispatch(fetchAssetById(companyId));
    }
  }, [companyId]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Formik
        initialValues={
          {
            //managementOverView: company?.managementOverview,
            //customerProblem: company?.customerProblem,
            //targetMarketSize: company?.targetMarketSize,
            //customerPotential: company?.customerPotential,
            //businessModel: company?.businessModel,
            //competitiveAdvantage: company?.competitiveAdvantage,
            //productsServices: company?.productsServices,
            //marketingStrategy: company?.marketingStrategy,
            //competitors: company?.competitors,
          }
        }
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit}>
            <Divider hidden />
            <Header as="h3" inverted textAlign="center">
              Excecutive Summary
              <Header.Subheader>
                Tell us about your customer, market, business model, product and
                revenue
              </Header.Subheader>
            </Header>
            <Divider inverted />
            <FormField
              name="managementOverView"
              component={Form.TextArea}
              label="MANAGEMENT*"
              placeholder="PROVIDE AN OVERVIEW OF YOUR MANAGEMENT"
              required
            />
            <FormField
              name="customerProblem"
              component={Form.TextArea}
              label="CUSTOMER PROBLEM*"
              placeholder="Customer Problem"
              required
            />
            <FormField
              name="targetMarketSize"
              component={Form.TextArea}
              label="TARGET MARKET & MARKET SIZE*"
              placeholder="Target & Market Size"
              required
            />
            <FormField
              name="customerPotential"
              component={Form.TextArea}
              label="CUSTOMERS - CURRENT/POTENTIAL*"
              placeholder="Customers-Current/Potential"
              required
            />
            <FormField
              name="businessModel"
              component={Form.TextArea}
              label="BUSINESS MODEL*"
              placeholder="Business Model"
              required
            />
            <FormField
              name="competitiveAdvantage"
              component={Form.TextArea}
              label="COMPETITVE ADVANTAGE*"
              placeholder="Competitve Advantage"
              required
            />
            <FormField
              name="productsServices"
              component={Form.TextArea}
              label="PRODUCT/ SERVICE*"
              placeholder="product/ Service"
              required
            />
            <FormField
              name="marketingStrategy"
              component={Form.TextArea}
              label="SALES/ MARKETING STRATEGY*"
              placeholder="Sales/ Marketing Strategy"
              required
            />
            <FormField
              name="competitors"
              component={Form.TextArea}
              label="COMPETITORS*"
              placeholder="Competitors"
              required
            />
            <Button secondary type="submit" loading={submitting} fluid>
              SAVE AND CONTINUE
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default ExcecutiveSummary;
