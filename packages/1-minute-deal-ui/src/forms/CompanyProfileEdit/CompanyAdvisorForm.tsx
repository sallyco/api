import React from "react";
import * as Yup from "yup";
import { Form, Button, Container, Transition } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";

interface Props {
  name: string;
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function CompanyAdvisorForm({
  name,
  onSubmit,
  submitting = false,
}: Props) {
  const validation = Yup.object().shape({
    advisorNames: Yup.string()

      .max(1024)
      .trim()
      .required(),
  });

  return (
    <>
      <Transition animation="fade left">
        <Container className="mobile-container">
          <Formik
            initialValues={{ advisorNames: name }}
            validationSchema={validation}
            enableReinitialize={true}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit}>
                <FormField
                  name="advisorNames"
                  component={Form.Input}
                  placeholder="Annie Northrend,Jeremey Sell"
                  required
                />
                {/* label="LIST ADVISOR NAMES(COMMA SEPARATED)" */}
                <Button
                  secondary
                  fluid
                  type="submit"
                  loading={submitting}
                  content="SAVE & CONTINUE"
                />
              </Form>
            )}
          </Formik>
        </Container>
      </Transition>
    </>
  );
}
