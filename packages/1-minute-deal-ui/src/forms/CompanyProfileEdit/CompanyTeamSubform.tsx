import React, { useRef, useState } from "react";
import { Divider, Form, Header, Icon, Segment } from "semantic-ui-react";
import { FormField } from "../common/FormField";
import { Asset } from "../../api/assetsApi";
import { FormikProps, useFormikContext } from "formik";
import Log from "../../tools/Log";

interface CompanyTeamSubformProps {
  companyAsset?: Asset;
}
const CompanyTeamSubform: React.FC<CompanyTeamSubformProps> = ({
  companyAsset,
}) => {
  const formikContext = useFormikContext();
  const [teamMemberLength, setTeamMemberLength]: any = useState(
    companyAsset?.team.length ?? 1
  );

  const [isError, setisError] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const [uploadImage, setUploadImage]: any = useState([]);
  const [uploadImageName, setUploadImageName]: any = useState([]);

  const fileInputRef = useRef<HTMLInputElement>();

  const fileChange = (e) => {
    Log.debug("file ", e.target.files[0]);
    const file = e.target.files[0];
    let reader = new FileReader();
    if (file.size <= 2097152) {
      setisError(false);
      seterrorMessage("");
      reader.onload = function (event) {
        Log.debug("event ", event);
        let imageNameArray = uploadImageName;
        imageNameArray.splice(teamMemberLength, 0, file.name);
        setUploadImageName(imageNameArray);
        let uploadImageArray = uploadImage;
        uploadImageArray.splice(
          teamMemberLength,
          0,
          event?.target?.result ?? undefined
        );
        setUploadImage(uploadImageArray);
        const team = formikContext.values["team"];
        if (team) {
          uploadImageArray.forEach((img, index) => {
            team[index]["image"] = img;
          });
          formikContext.setFieldValue("team", team);
        }
      };
      reader.readAsDataURL(file);
    } else {
      setisError(true);
      seterrorMessage("Your file is too big");
      setUploadImage(undefined);
    }
  };

  const removeTeamMember = (index: number, props: FormikProps<any>) => {
    let imageNameArray = uploadImageName;
    imageNameArray.splice(index, 1);
    setUploadImageName(imageNameArray);
    let uploadImageArray = uploadImage;
    uploadImageArray.splice(index, 1);
    setUploadImage(uploadImageArray);
    setTeamMemberLength(teamMemberLength - 1);
    let teamMembersArray = props.values.team;
    teamMembersArray.splice(index, 1);
    props.setFieldValue("team", teamMembersArray);
  };

  return (
    <>
      {Array.from({ length: teamMemberLength }, (x, i) => i).map((index) => (
        <>
          <Segment key={"management-" + index}>
            <Form.Group>
              <Header as={"h4"}>Team Member</Header>
              {index >= 0 && (
                <>
                  <Divider />
                  <Form.Button
                    fluid
                    negative
                    onClick={() => removeTeamMember(index, formikContext)}
                    content="Remove Team Member"
                    type="button"
                    icon="remove user"
                  />
                </>
              )}
            </Form.Group>
            <Form.Group widths="equal">
              <FormField
                id={`team[${index}].name`}
                name={`team[${index}].name`}
                label="Name"
                placeholder="Christine Banfield"
                required
              />
              <FormField
                id={`team[${index}].role`}
                name={`team[${index}].role`}
                label="Role"
                placeholder="Founder and CEO"
                required
              />
            </Form.Group>
            <FormField
              id={`team[${index}].linkedInUrl`}
              name={`team[${index}].linkedInUrl`}
              label="LinkedIn Profile URL"
              placeholder="linkedin.com/profile"
            />
            <Divider hidden />

            <div className={`field`}>
              <Form.Button
                fluid
                secondary
                onClick={() => fileInputRef.current.click()}
                content="Upload A Photo"
                type="button"
                icon="user circle"
              />
              <input
                ref={fileInputRef}
                type="file"
                id={`team[${index}].image`}
                name={`team[${index}].image`}
                hidden
                onChange={fileChange}
                accept="image/*"
              />

              {uploadImage[index] && (
                <Header color="green" as="h6">
                  <Icon color="green" name="check circle" />
                  <Header.Content>
                    {uploadImageName[index]} uploaded
                  </Header.Content>
                </Header>
              )}

              {isError && (
                <div className="ui pointing above prompt label">
                  {errorMessage}
                </div>
              )}
            </div>
          </Segment>
        </>
      ))}

      <Form.Button
        fluid
        secondary
        onClick={() => setTeamMemberLength(teamMemberLength + 1)}
        content="Add Team Member"
        type="button"
        icon="add user"
      />
    </>
  );
};

export { CompanyTeamSubform };
