import React, { useCallback, useMemo, useState } from "react";
import {
  Button,
  Divider,
  Form,
  Header,
  Icon,
  Segment,
} from "semantic-ui-react";
import { Formik, FormikContext, useFormikContext } from "formik";
import SubformDate, {
  defaultValues as DateDefaultValues,
} from "../subforms/SubformDate";
import { FormField, MaskedFormField } from "../common/FormField";
import {
  CompanyMarketSector,
  EntityTypes,
  NumberOfemplyees,
  Revenue,
  SecurityTypes,
} from "../../tools/enums";
import QuillField from "../common/QuillField";
import SubformEmail from "../subforms/SubformEmail";
import SubformPhone from "../subforms/SubformPhone";
import { API, doChange, useRequest } from "../../api/swrApi";
import { CompanyTeamSubform } from "./CompanyTeamSubform";
import { Company } from "../../api/companyApi";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { fetchAssetById } from "../../slices/assetsSlice";

interface CompanyProfileEditProps {
  assetId?: string;
  onSave?: () => void;
}

const CompanyAssetEdit: React.FC<CompanyProfileEditProps> = ({
  assetId,
  onSave,
}) => {
  const { data: company, mutate: mutateAsset } = useRequest(
    API.ADD_FILTER(API.ASSET_BY_ID(assetId), {
      include: ["deal"],
      fields: {
        logo: false,
        banner: false,
      },
    })
  );

  const [saving, setSaving] = useState(false);

  const blankCompany: Company = {
    address: {},
    arbitrationCity: "",
    arbitrationState: "",
    createdAt: "",
    deletedAt: "",
    email: "",
    entityType: "",
    id: "",
    name: "",
    ownerId: "",
    phone: "",
    seriesNumber: 0,
    seriesPrefix: "",
    stateOfFormation: "",
    type: "",
    updatedAt: "",
    website: "",
    properties: [
      {
        key: "Location",
        value: "",
      },
      {
        key: "Market Sector",
        value: "",
      },
      {
        key: "No. Of Employees",
        value: "",
      },
      {
        key: "Incorporation",
        value: "",
      },
      {
        key: "Revenue Stage",
        value: "",
      },
      {
        key: "Date Founded",
        value: "",
      },
    ],
  };

  const propertyIndex = (
    values,
    fieldName,
    key,
    defaultIndex = 0,
    keyIndex = "key"
  ) => {
    return (
      values[fieldName].findIndex((field) => field[keyIndex] === key) ??
      defaultIndex
    );
  };

  const dispatch = useDispatch();

  return (
    <>
      {((assetId && company) || !assetId) && (
        <>
          <Header as="h3">
            Company Basics
            <Header.Subheader>
              Include your elevator pitch and some essentials about your company
            </Header.Subheader>
          </Header>
          <Divider />
          <Formik
            initialValues={company ?? blankCompany}
            enableReinitialize={false}
            onSubmit={async (data) => {
              delete data.day;
              delete data.month;
              delete data.year;
              delete data.deal;
              await mutateAsset(data, false);
              try {
                setSaving(true);
                await doChange(
                  API.ASSET_BY_ID(assetId),
                  {
                    ...data,
                  },
                  "PATCH"
                );
              } catch (e: any) {
                setSaving(false);
                toast.error(e.message);
              }
              await mutateAsset(data);
              await dispatch(fetchAssetById(assetId));
              setSaving(false);
              toast.success("Company Saved");
              onSave();
            }}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} autoComplete="off">
                {/*
            <Prompt
              message={
                "Your Company has not been created yet. Leaving now will erase the entered information."
              }
              when={!props.isSubmitting && props.dirty}
            />
*/}
                <Form.Group widths="equal">
                  <FormField
                    id="name"
                    name="name"
                    label="Company Name"
                    placeholder="Venture BC, LLC"
                    required
                  />
                  <FormField
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "Market Sector",
                      1
                    )}.value`}
                    component={Form.Select}
                    label="Company Market Sector"
                    placeholder="Financials"
                    options={CompanyMarketSector}
                  />
                </Form.Group>
                <QuillField
                  name={"description"}
                  label={"One-Line Pitch"}
                  placeholder={"A quick description of your company"}
                />
                <Form.Group widths="equal">
                  <FormField
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "Incorporation",
                      3
                    )}.value`}
                    component={Form.Select}
                    label="Incorporation Type"
                    placeholder="LLC"
                    options={EntityTypes}
                  />
                </Form.Group>
                <Form.Group>
                  <FormField
                    label={"Date Founded"}
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "Date Founded",
                      5
                    )}.value`}
                    component={Form.Input}
                    placeholder={"02-15-1998"}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <FormField
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "No. Of Employees",
                      2
                    )}.value`}
                    component={Form.Select}
                    label="Number Of Employees"
                    options={NumberOfemplyees}
                    placeholder="10-50"
                  />
                  <FormField
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "Revenue Stage",
                      4
                    )}.value`}
                    component={Form.Select}
                    label="Company Revenue Stage"
                    placeholder="$5M in TTM Revenue"
                    options={Revenue}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <FormField
                    name={`properties.${propertyIndex(
                      props.values,
                      "properties",
                      "Location",
                      0
                    )}.value`}
                    component={Form.Input}
                    label="Location"
                    placeholder="Salt Lake City, UT, USA"
                  />
                  <FormField
                    name="assetUrl"
                    id="assetUrl"
                    component={Form.Input}
                    label="Company Website"
                    placeholder="myventure.com"
                    required
                  />
                </Form.Group>

                <Form.Group widths="equal">
                  <MaskedFormField
                    name="funding.targetRaiseAmount"
                    thousandSeparator={true}
                    allowNegative={false}
                    prefix={"$"}
                    decimalScale={0}
                    type="tel"
                    label="Target Raise Amount"
                    placeholder="$500,000"
                    required
                  />
                  <FormField
                    name="funding.securityType"
                    component={Form.Select}
                    label="Security Type"
                    options={SecurityTypes}
                    placeholder="Select a Security Type"
                    required
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <FormField
                    name="funding.noOfShare"
                    component={Form.Input}
                    label="Number of Shares"
                    placeholder="Number of Shares"
                    type="number"
                  />
                  <MaskedFormField
                    name="funding.sharePrice"
                    thousandSeparator={true}
                    allowNegative={false}
                    prefix={"$"}
                    decimalScale={0}
                    type="tel"
                    label="Share Price"
                    placeholder="$500,000"
                  />
                </Form.Group>

                <Header as="h3" textAlign="left">
                  Contact Info
                </Header>
                <Divider />

                <Form.Group widths="equal">
                  <FormField
                    name="contact.name"
                    id="contact.name"
                    component={Form.Input}
                    label="Contact Name"
                    placeholder="John Doe"
                    required
                  />
                  <SubformEmail namePrefix={"contact."} />
                  <SubformPhone namePrefix={"contact."} />
                </Form.Group>

                <Header as="h3" textAlign="left">
                  Management
                  <Header.Subheader>
                    Add key team members, executives, directors and partners
                  </Header.Subheader>
                </Header>
                <Divider />

                <QuillField
                  name={`details.${propertyIndex(
                    props.values,
                    "details",
                    "Management Overview",
                    0,
                    "heading"
                  )}.body`}
                  label={"Overview of Management"}
                  placeholder={"Provide an Overview of Your Management Here"}
                />

                <Divider hidden />
                <Header as="h3" textAlign="left">
                  Advisors
                </Header>
                <Divider />
                <FormField
                  id="advisors"
                  name="advisors"
                  component={Form.Input}
                  label="List Advisor Names (Comma Separated)"
                  placeholder="Annie Northrend, Hashir Johnson"
                />
                <Divider />

                <CompanyTeamSubform companyAsset={company} />

                <Button
                  secondary
                  type="submit"
                  loading={saving}
                  fluid
                  content="Save"
                />
              </Form>
            )}
          </Formik>
        </>
      )}
    </>
  );
};

export { CompanyAssetEdit };
