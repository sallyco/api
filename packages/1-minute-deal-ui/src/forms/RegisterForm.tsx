import React, { useEffect, useState, useRef } from "react";
import {
  Form,
  Header,
  Dropdown,
  Divider,
  Transition,
  Progress,
} from "semantic-ui-react";

import { Formik } from "formik";
import { FormField } from "./common/FormField";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../tools/helpers";

import SubformFirstLastName, * as SubformFirstLastNameMeta from "./subforms/SubformFirstLastName";
import SubformEmail, * as SubformEmailMeta from "./subforms/SubformEmail";
import SubformPhone, * as SubformPhoneMeta from "./subforms/SubformPhone";
import SubformDateOfBirth, * as SubformDateOfBirthMeta from "./subforms/SubformDateOfBirth";
import SubformPassword, * as SubformPasswordMeta from "./subforms/SubformPassword";
import moment from "moment";
import { Invite } from "../api/invitesApi";

import * as Yup from "yup";
import { isValidZip } from "./validation/ValidationHelper";
import SubformPersonTaxId, * as SubformPersonTaxIdMeta from "./subforms/SubformPersonTaxId";
import SubformAddress from "./subforms/SubformAddress";

interface Props {
  title: string;
  invite?: Invite;
  onSubmit: (any) => void;
  submitting: boolean;
  setShowTerms: (any) => void;
  termsChecked: boolean;
  setTermsChecked: (any) => void;
}

export default function RegisterForm({
  title = "Register",
  invite,
  onSubmit,
  submitting,
  setShowTerms,
  termsChecked,
  setTermsChecked,
}: Props) {
  const formikRef: any = useRef();

  const [country, setCountry]: any = useState(null);
  const [state, setState]: any = useState(null);

  useEffect(() => {
    if (formikRef.current) {
      formikRef.current.setFieldValue("termsOfService", termsChecked);
    }
  }, [termsChecked]);

  useEffect(() => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        const countries = getCountries();
        const foundCountry = countries.find(
          (el) =>
            el?.code?.toLowerCase() === response?.countryCode?.toLowerCase()
        );
        if (foundCountry) {
          setCountry(foundCountry.name);
          const states = getStates(foundCountry?.code) || [];
          const foundState = states.find(
            (el) => el?.toLowerCase() === response?.region?.toLowerCase()
          );
          if (foundState) setState(foundState);
          formikRef.current.setFieldValue("state", foundState);
          formikRef.current.setFieldValue("entityCountry", foundCountry.name);
          formikRef.current.setFieldValue("country", foundCountry.name);
          if (foundCountry.name === "United States of America") {
            formikRef.current.setFieldValue("isUSBased", "Yes");
          } else {
            formikRef.current.setFieldValue("isUSBased", "No");
          }
        }
      })
      .catch((err) => {});
  }, []);
  const validation = Yup.object()
    .shape({
      ...SubformFirstLastNameMeta.validationSchema({ isRequired: true }),
      ...SubformEmailMeta.validationSchema({ isRequired: true }),
      ...SubformPhoneMeta.validationSchema({ isRequired: true }),
      ...SubformDateOfBirthMeta.validationSchema({ isRequired: true }),
      ...SubformPasswordMeta.validationSchema(),
      ...SubformPersonTaxIdMeta.validationSchema({ isRequired: true }),
      termsOfService: Yup.bool().oneOf(
        [true],
        "You must accept the terms and conditions"
      ),
      postalCode: Yup.string()
        .min(2)
        .max(1024)
        .when("country", (formCountry, schema) => {
          if (country === "United States of America") {
            return isValidZip(schema).required("ZIP Code is Required");
          }
          return schema;
        }),
    })
    .test("dateOfBirthGate", "dateOfBirthGate", (obj) => {
      let ecd = new Date();
      ecd.setFullYear(obj.year);
      ecd.setMonth(obj.month);
      ecd.setDate(obj.day);
      let today = new Date();
      if (moment(ecd).isBefore(today.setFullYear(today.getFullYear() - 18))) {
        return true;
      } else {
        return new Yup.ValidationError("You must be 18 or older", null, "year");
      }
    });

  const completedStep = () => {
    if (country) {
      if (country === "United States of America") {
        return state ? 1 : 0;
      } else {
        return 1;
      }
    } else {
      return 0;
    }
  };

  const totalSteps = () => {
    return 2;
  };

  return (
    <>
      <Header as="h1" inverted textAlign="center">
        {title}
      </Header>

      <Header as="h3" inverted>
        Completed step {completedStep()} of {totalSteps()}
      </Header>
      <Progress
        value={completedStep()}
        total={totalSteps()}
        size="small"
        success
      />
      <Transition transitionOnMount animation="fade up">
        <div>
          <Header as="h3" inverted>
            My country is...
          </Header>
          <Dropdown
            name="country"
            placeholder="Select a Country"
            clearable
            selection
            fluid
            search={startsWithSearch}
            onChange={(e, d) => {
              setCountry(d.value);
              formikRef.current.setFieldValue("entityCountry", d.value);
              formikRef.current.setFieldValue("country", d.value);
              if (country === "United States of America") {
                formikRef.current.setFieldValue("isUSBased", "Yes");
              } else {
                formikRef.current.setFieldValue("isUSBased", "Yes");
              }

              if (d.value === "")
                window.scrollTo({ top: 0, behavior: "smooth" });
            }}
            options={getCountries().map((item) => {
              return {
                key: item.code,
                value: item.name,
                text: item.name,
              };
            })}
            value={country}
          />
          <Divider hidden />
        </div>
      </Transition>

      <Transition visible={country === "United States of America"}>
        <div>
          <Header as="h3" inverted>
            My state is...
          </Header>
          <Dropdown
            name="state"
            clearable
            selection
            fluid
            search={startsWithSearch}
            onChange={(e, d) => {
              setState(d.value);
              formikRef.current.setFieldValue("state", d.value);
              if (d.value === "")
                window.scrollTo({ top: 0, behavior: "smooth" });
            }}
            options={(country
              ? getStates(
                  getCountries().find((item) => {
                    return item.name === country;
                  })?.code
                )
              : []
            ).map((item) => {
              return {
                key: item,
                value: item,
                text: item,
              };
            })}
            placeholder={
              country === "United States of America"
                ? "Select a State"
                : "State/Province/Territory"
            }
            value={state}
          />
          <Divider hidden />
        </div>
      </Transition>

      <Transition
        visible={
          state !== "" ||
          (country !== "" && country !== "United States of America")
        }
      >
        <div>
          <Formik
            innerRef={formikRef}
            initialValues={{
              termsOfService: false,
              email: invite ? invite.acceptableBy : "",
              ...SubformFirstLastNameMeta.defaultValues,
              ...SubformDateOfBirthMeta.defaultValues,
              ...SubformPhoneMeta.defaultValues,
              ...SubformPersonTaxIdMeta.defaultValues,
              postalCode: "",
            }}
            validationSchema={validation}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} autoComplete="off">
                <Form.Group widths="equal">
                  <SubformFirstLastName required={true} />
                </Form.Group>
                <Form.Group widths="equal">
                  <SubformEmail required={true} disabled={invite} />
                </Form.Group>

                <Form.Group widths="equal">
                  <SubformDateOfBirth required={true} />
                </Form.Group>
                <SubformPersonTaxId values={props.values} required={true} />
                <Form.Group widths="equal">
                  <SubformPhone />
                </Form.Group>
                <SubformAddress {...props} />

                <SubformPassword required={true} />

                <Divider hidden />

                <Form.Group widths="equal">
                  <FormField
                    id="termsOfService"
                    name="termsOfService"
                    width={10}
                    component={Form.Checkbox}
                    label="Review our Terms of Use & Privacy Policy"
                    checked={termsChecked}
                    onClick={() => {
                      if (termsChecked) setTermsChecked(false);
                      else setShowTerms(true);
                    }}
                    required={true}
                  />
                  <Form.Button
                    secondary
                    type="submit"
                    fluid
                    width={6}
                    loading={submitting}
                    disabled={!props.isValid}
                  >
                    Create Account
                  </Form.Button>
                </Form.Group>
              </Form>
            )}
          </Formik>
        </div>
      </Transition>
    </>
  );
}
