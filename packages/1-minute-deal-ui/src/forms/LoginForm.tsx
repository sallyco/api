import React, { useContext, useState } from "react";
import { TenantContext } from "../contexts";

import * as Yup from "yup";

import { Form, Button, Header, Message, Transition } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "./common/FormField";

import SubformEmail, * as SubformEmailMeta from "./subforms/SubformEmail";

interface Props {
  title: string;
  onSubmit: (any) => void;
  submitting: boolean;
  credentialError: boolean;
}

export default function LoginForm({
  title = "Login",
  onSubmit,
  submitting = false,
  credentialError = false,
}: Props) {
  const [showPassword, setShowPassword] = useState(false);

  const validation = Yup.object().shape({
    ...SubformEmailMeta.validationSchema({ isRequired: true }),
    password: Yup.string().trim().required(),
  });

  return (
    <>
      <Header as="h1" inverted textAlign="center">
        {title}
      </Header>
      <Formik
        initialValues={{}}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} data-testid={"login-form"}>
            <SubformEmail autoComplete="username email" />
            <FormField
              name="password"
              component={Form.Input}
              label="Password"
              type={showPassword ? "text" : "password"}
              autoComplete="current-password"
              placeholder="Password"
              data-testid={"password"}
              action={{
                icon: showPassword ? "eye slash" : "eye",
                onClick: () => setShowPassword(!showPassword),
                type: "button",
              }}
            />
            <Transition.Group animation={"fade"}>
              {credentialError && (
                <Message negative compact size="small">
                  The email or password you entered is incorrect
                </Message>
              )}
            </Transition.Group>
            <Button
              secondary
              fluid
              type="submit"
              data-testid={"submit-button"}
              loading={submitting}
              content="Log In"
            />
          </Form>
        )}
      </Formik>
    </>
  );
}
