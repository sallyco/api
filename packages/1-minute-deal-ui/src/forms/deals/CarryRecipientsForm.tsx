import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Button, Form } from "semantic-ui-react";
import { Formik } from "formik";

import { RootState } from "../../rootReducer";
import { MaskedFormField } from "../common/FormField";
import { Deal } from "../../api/dealsApi";
import { updateDealById } from "../../slices/dealsSlice";
import { regexSkipDigitAndDecimals } from "./../validation/ValidationHelper";
import {
  generateEntityDocument,
  patchEntityById,
} from "../../slices/entitySlice";

interface Props {
  deal: Deal;
  setShowEdit: (any) => void;
}

const CarryRecipientsForm = ({ deal, setShowEdit }: Props) => {
  const dispatch = useDispatch();

  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  async function onSubmit(data) {
    const newCarry = Number(
      data.organizerCarryPercentage.replace(regexSkipDigitAndDecimals, "")
    );

    let regenerateOA = false;
    if (deal.organizerCarryPercentage !== newCarry) {
      regenerateOA = true;
    }

    await dispatch(
      updateDealById({
        id: deal.id,
        organizerCarryPercentage:
          typeof data.organizerCarryPercentage === "number"
            ? data.organizerCarryPercentage
            : newCarry,
      })
    );

    if (regenerateOA) {
      const oaId = await updateOA();
      await dispatch(
        patchEntityById(entity.id, {
          entityDocuments: {
            ...entity.entityDocuments,
            operatingAgreement: oaId,
          },
        })
      );
    }

    setShowEdit(false);
  }

  async function updateOA() {
    const fileData = await dispatch(
      generateEntityDocument(deal.entityId, "oa")
    );
    return fileData["id"];
  }

  const validation = Yup.object().shape({
    organizerCarryPercentage: Yup.number()
      .transform((value, ogValue) =>
        typeof ogValue === "number"
          ? ogValue
          : Number(ogValue.replace(regexSkipDigitAndDecimals, ""))
      )
      .min(0)
      .max(20),
  });

  return (
    <Formik
      initialValues={{
        organizerCarryPercentage: deal?.organizerCarryPercentage,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <MaskedFormField
            id="organizerCarryPercentage"
            name="organizerCarryPercentage"
            thousandSeparator={true}
            allowNegative={false}
            decimalScale={2}
            suffix={"%"}
            type="tel"
            label="Organizer Carry Percentage"
            placeholder={"10.00%"}
            data-testid="carry-field"
          />
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
            data-testid="submit-button"
          />
        </Form>
      )}
    </Formik>
  );
};

export default CarryRecipientsForm;
