import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Button, Form } from "semantic-ui-react";
import { Formik } from "formik";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";

import { FormField, MaskedFormField } from "../common/FormField";
import { regexSkipDigitAndDecimals } from "../validation/ValidationHelper";
import { Deal } from "../../api/dealsApi";
import { RootState } from "../../rootReducer";
import { updateDealById } from "../../slices/dealsSlice";

interface Props {
  deal: Deal;
  setShowEditCarry: (any) => void;
  carry: any;
}

const IndividualCarryRecipientsForm = ({
  deal,
  setShowEditCarry,
  carry,
}: Props) => {
  const dispatch = useDispatch();
  let history = useHistory();

  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  const UpdateNotice = (dealId) => {
    const reload = () => {
      if (dealId) {
        history.push(
          `/deals/${deal?.id}/review-docs?generate=true&regenerate=true`
        );
      }
    };

    return (
      <>
        <div>
          <div>This change requires a regeneration of your documents.</div>
          <div>
            <Button
              onClick={reload}
              content="Click here to regenerate your documents."
              size="small"
              compact
            />
          </div>
        </div>
      </>
    );
  };

  const onSubmit = (data) => {
    let needDocumentRegeneration = false;
    const updatedCarryPercentage =
      typeof data.carryPercentage === "string"
        ? +data.carryPercentage.replace(regexSkipDigitAndDecimals, "")
        : +data.carryPercentage;
    if (updatedCarryPercentage !== carry?.carryPercentage) {
      needDocumentRegeneration = true;
    }
    const otherCarryRecipients = deal.additionalCarryRecipients.filter(
      (carryInQuestion) =>
        carryInQuestion.individual.name !== carry.individual.name
    );
    const carryRecipientToUpdate = {
      ...carry,
      carryPercentage: updatedCarryPercentage,
      individual: {
        ...carry.individual,
        name: data.individualName,
      },
    };

    dispatch(
      updateDealById({
        ...deal,
        additionalCarryRecipients: [
          ...otherCarryRecipients,
          carryRecipientToUpdate,
        ],
      })
    );
    setShowEditCarry(false);
    if (needDocumentRegeneration) {
      toast.warning(UpdateNotice(deal?.id), {
        closeButton: false,
        autoClose: false,
      });
    }
  };

  const validation = Yup.object().shape({
    carryPercentage: Yup.number()
      .transform((value, ogValue) => {
        if (typeof ogValue === "string") {
          return Number(ogValue.replace(regexSkipDigitAndDecimals, ""));
        }
        return ogValue;
      })
      .min(0)
      .max(20),
  });

  return (
    <Formik
      initialValues={{
        individualName: carry?.individual?.name,
        carryPercentage: carry?.carryPercentage,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <FormField
            id="individualName"
            name="individualName"
            label="Carry Recipient Name"
          />
          <MaskedFormField
            id="carryPercentage"
            name="carryPercentage"
            thousandSeparator={true}
            allowNegative={false}
            decimalScale={2}
            type="tel"
            label="Carry Percentage"
            placeholder={"0.00%"}
            suffix={"%"}
          />
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
          />
        </Form>
      )}
    </Formik>
  );
};

export default IndividualCarryRecipientsForm;
