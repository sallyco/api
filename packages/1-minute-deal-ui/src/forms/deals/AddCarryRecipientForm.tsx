import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Button, Form, Header } from "semantic-ui-react";
import { Formik } from "formik";

import { RootState } from "../../rootReducer";
import { FormField, MaskedFormField } from "../common/FormField";
import { Deal } from "../../api/dealsApi";
import { updateDealById } from "../../slices/dealsSlice";
import SubformState from "../subforms/SubformState";
import SubformCountry from "../subforms/SubformCountry";
import {
  isValidZip,
  regexSkipDigitAndDecimals,
} from "../validation/ValidationHelper";
import {
  generateEntityDocument,
  patchEntityById,
} from "../../slices/entitySlice";
interface Props {
  deal: any;
  setShowAddNew: (any) => void;
}

function removeEmpty(obj) {
  return Object.entries(obj)
    .filter(([_, v]) => v != null)
    .reduce(
      (acc, [k, v]) => ({ ...acc, [k]: v === Object(v) ? removeEmpty(v) : v }),
      {}
    );
}
const AddCarryRecipientForm = ({ deal, setShowAddNew }: Props) => {
  const dispatch = useDispatch();

  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  async function onSubmit(data) {
    let newCarryRecipient = {
      carryPercentage: Number(
        data.carryPercentage.replace(regexSkipDigitAndDecimals, "")
      ),
      individual: {
        name: data.name,
        type: data.type,
        title: data.title,
        isUSBased: data.countryOfFormation === "United States of America",
        address: {
          address1: data.address1,
          address2: data.address2,
          city: data.city,
          state: data.stateOfFormation,
          postalCode: data.postalCode,
          country: data.countryOfFormation,
        },
        phone: data.phone,
        email: data.email,
        stateOfFormation: data.stateOfFormation,
        countryOfFormation: data.countryOfFormation,
      },
    };

    newCarryRecipient = removeEmpty(newCarryRecipient);

    const { profile, ...rest } = deal;
    if (rest?.additionalCarryRecipients) {
      await dispatch(
        updateDealById({
          ...rest,
          additionalCarryRecipients: [
            ...rest.additionalCarryRecipients,
            newCarryRecipient,
          ],
        })
      );
    } else {
      await dispatch(
        updateDealById({
          ...rest,
          additionalCarryRecipients: [newCarryRecipient],
        })
      );
    }

    const oaId = await updateOA();
    await dispatch(
      patchEntityById(entity.id, {
        entityDocuments: {
          ...entity.entityDocuments,
          operatingAgreement: oaId,
        },
      })
    );

    setShowAddNew(false);
  }

  async function updateOA() {
    const fileData = await dispatch(
      generateEntityDocument(deal.entityId, "oa")
    );
    return fileData["id"];
  }

  const validation = Yup.object().shape({
    carryPercentage: Yup.number()
      .transform((value, ogValue) =>
        Number(ogValue.replace(regexSkipDigitAndDecimals, ""))
      )
      .min(0)
      .max(20),
    type: Yup.string().trim().required(),
    name: Yup.string().trim().nullable(),
    email: Yup.string().trim().nullable(),
    phone: Yup.string().trim().nullable(),
    title: Yup.string().trim().nullable(),
    address1: Yup.string().trim().nullable(),
    address2: Yup.string().trim().nullable(),
    city: Yup.string().trim().nullable(),
    countryOfFormation: Yup.string().trim().nullable(),
    stateOfFormation: Yup.string().trim().nullable(),
    postalCode: Yup.string()
      .min(2)
      .max(1024)
      .required()
      .when("country", (formCountry, schema) => {
        if (formCountry === "United States of America") {
          return isValidZip(schema).required("ZIP Code is Required");
        }
        return schema;
      }),
  });

  return (
    <Formik
      initialValues={{
        carryPercentage: 0,
        type: "Individual",
        name: null,
        email: null,
        phone: null,
        title: null,
        address1: null,
        address2: null,
        city: null,
        countryOfFormation: null,
        stateOfFormation: null,
        postalCode: null,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <MaskedFormField
            id="carryPercentage"
            name="carryPercentage"
            thousandSeparator={true}
            allowNegative={false}
            decimalScale={2}
            type="tel"
            label="Carry Percentage"
            placeholder={"0.00%"}
            data-testid="carry-field"
          />
          <FormField
            name="type"
            component={Form.Select}
            label="Recipient Type"
            required
            options={[
              {
                value: "Individual",
                text: "Individual",
              },
              {
                value: "LLC",
                text: "LLC",
              },
              {
                value: "LP",
                text: "LP",
              },
              {
                value: "CORP",
                text: "CORP",
              },
              {
                value: "Foreign CORP",
                text: "Foreign CORP",
              },
            ]}
          />
          {props.values.type === "Individual" ? (
            <>
              <FormField
                name="name"
                id="name"
                label="Name"
                placeholder="Name"
              />
              <FormField
                name="email"
                id="email"
                label="Email"
                placeholder="Email"
              />
              <FormField
                name="phone"
                id="phone"
                label="Phone"
                placeholder="Phone"
              />
            </>
          ) : (
            <>
              <FormField
                name="name"
                id="name"
                label="Carry Entity Name"
                placeholder="Carry Entity Name"
              />
              <FormField
                name="title"
                id="title"
                label="Carry Entity Title"
                placeholder="Carry Entity Title"
              />
            </>
          )}

          <Header>Address:</Header>
          <FormField
            name="address1"
            id="address1"
            label="Address 1"
            placeholder="Address 1"
          />
          <FormField
            name="address2"
            id="address2"
            label="Address 2"
            placeholder="Address 2"
          />
          <FormField name="city" id="city" label="City" placeholder="City" />
          <SubformCountry
            name={"countryOfFormation"}
            id={"countryOfFormation"}
            label={
              props.values.type === "Individual"
                ? "Country"
                : "Country Of Formation"
            }
            placeholder={"Select a Country"}
            data-testid="country-field"
          />
          <SubformState
            name={"stateOfFormation"}
            id={"stateOfFormation"}
            label={
              props.values.type === "Individual"
                ? "State"
                : "State Of Formation"
            }
            placeholder={"Select a State"}
            country={props?.values?.countryOfFormation}
            data-testid="state-field"
          />
          <FormField
            name="postalCode"
            id="postalCode"
            label="Postal Code"
            placeholder="Postal Code"
            required
          />
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
            data-testid="submit-button"
          />
        </Form>
      )}
    </Formik>
  );
};

export default AddCarryRecipientForm;
