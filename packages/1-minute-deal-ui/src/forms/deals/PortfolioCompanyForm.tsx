import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { Button, Form } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "../../rootReducer";
import { FormField } from "../common/FormField";
import { Deal } from "../../api/dealsApi";
import { updateDealById } from "../../slices/dealsSlice";
import { TemporaryDealEntityTypes } from "../../tools/enums";

interface Props {
  deal: Deal;
  setShowEdit: (any) => void;
}

const PortfolioCompanyForm = ({ deal, setShowEdit }: Props) => {
  const dispatch = useDispatch();
  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  const validation = Yup.object().shape({
    portfolioCompanyName: Yup.string().trim(),
    portfolioCompanyEntity: Yup.string().trim(),
    portfolioCompanyState: Yup.string().trim(),
  });

  function onSubmit(data) {
    dispatch(
      updateDealById({
        ...deal,
        portfolioCompanyName: data.portfolioCompanyName,
        portfolioCompanyEntity: data.portfolioCompanyEntity,
        portfolioCompanyState: data.portfolioCompanyState,
      })
    );
    setShowEdit(false);
  }

  return (
    <Formik
      initialValues={{
        portfolioCompanyName: deal?.portfolioCompanyName,
        portfolioCompanyEntity: deal?.portfolioCompanyEntity,
        portfolioCompanyState: deal?.portfolioCompanyState,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <FormField
            name="portfolioCompanyName"
            id="portfolioCompanyName"
            label="Name"
            placeholder="Name"
          />
          <FormField
            component={Form.Select}
            options={TemporaryDealEntityTypes}
            name="portfolioCompanyEntity"
            id="portfolioCompanyEntity"
            label="Entity"
            placeholder="Entity"
          />
          <FormField
            name="portfolioCompanyState"
            id="portfolioCompanyState"
            label="State"
            placeholder="State"
          />
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
          />
        </Form>
      )}
    </Formik>
  );
};

export default PortfolioCompanyForm;
