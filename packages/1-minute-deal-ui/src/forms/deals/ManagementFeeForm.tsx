import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button, Form } from "semantic-ui-react";
import { Formik } from "formik";
import * as Yup from "yup";

import { RootState } from "../../rootReducer";
import { Entity } from "../../api/entitiesApi";
import { patchEntityById } from "../../slices/entitySlice";
import SubformManagementFee, {
  validationSchema,
  fixAmountValue,
  MANAGEMENT_FEE_FREQUENCY,
  testValidFeeAmount,
} from "../subforms/SubformManagementFee";

interface Props {
  entity: Entity;
  setShowEdit: (any) => void;
  targetAmount: number;
}

const ManagementFeeForm = ({ entity, setShowEdit, targetAmount }: Props) => {
  const dispatch = useDispatch();
  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  function onSubmit(data) {
    if (data?.managementFee?.amount) {
      const amountValue = fixAmountValue(data.managementFee);
      dispatch(
        patchEntityById(entity.id, {
          managementFee: {
            type: data.managementFee?.type,
            amount: amountValue,
            frequency: MANAGEMENT_FEE_FREQUENCY,
            isRecurring: data.managementFee?.isRecurring,
          },
        })
      );
    } else {
      //   const { expenseReserve, ...newEntity } = entity;
      //   dispatch(updateEntityById(newEntity));
      dispatch(
        patchEntityById(entity.id, {
          managementFee: {
            type: "",
            amount: 0,
            frequency: "",
          },
        })
      );
    }
    setShowEdit(false);
  }

  const validation = Yup.object()
    .shape({
      ...validationSchema(targetAmount),
    })
    .test(
      "Check if amount is above fee %",
      "Amount cannot be above 4.5% of the target",
      (obj) => {
        return testValidFeeAmount(
          targetAmount,
          obj.managementFee.amount,
          "managementFee.amount"
        );
      }
    );

  return (
    <Formik
      initialValues={{
        managementFee: {
          ...entity?.managementFee,
        },
      }}
      enableReinitialize={true}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <SubformManagementFee managementFee={entity?.managementFee} />
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
          />
        </Form>
      )}
    </Formik>
  );
};

export default ManagementFeeForm;
