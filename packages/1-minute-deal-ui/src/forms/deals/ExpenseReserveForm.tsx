import React, { useContext, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Button, Form } from "semantic-ui-react";
import { Formik } from "formik";

import { RootState } from "../../rootReducer";
import { FormField, MaskedFormField } from "../common/FormField";
import { TenantContext } from "../../contexts";
import { Entity } from "../../api/entitiesApi";
import { Deal } from "../../api/dealsApi";
import { patchEntityById } from "../../slices/entitySlice";
import numeral from "numeral";

const DEFAULT_FEE_PERCENT = 25;
interface Props {
  entity: Entity;
  deal: Deal;
  setShowEdit: (any) => void;
}

const ExpenseReserveTypes = {
  Percent: "Percent",
  "Flat Fee": "Flat Fee",
};

const EditReserveForm = ({ entity, deal, setShowEdit }: Props) => {
  const dispatch = useDispatch();
  const [hasReserve, setHasReserve] = useState(
    entity?.expenseReserve?.amount > 0
  );
  const tenantContext = useContext(TenantContext);

  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  function onSubmit(data) {
    if (data.hasExpenseReserve) {
      dispatch(
        patchEntityById(entity.id, {
          expenseReserve: {
            amount:
              typeof data.amount === "number"
                ? data.amount
                : Number(data.amount.replace(/\D+/g, "")),
            type: data.type,
          },
        })
      );
    } else {
      //   const { expenseReserve, ...newEntity } = entity;
      //   dispatch(updateEntityById(newEntity));
      dispatch(
        patchEntityById(entity.id, {
          expenseReserve: {
            amount: 0,
            type: "Percent",
          },
        })
      );
    }
    setShowEdit(false);
  }

  const validation = Yup.object().shape({
    hasExpenseReserve: Yup.boolean(),
    type: Yup.string().trim(),
    amount: Yup.number()
      .when("hasExpenseReserve", {
        is: false, //just an e.g. you can return a function
        then: Yup.number().transform((value, ogValue) => {
          if (typeof ogValue === "number") return ogValue;
          if (ogValue) return Number(ogValue.replace(/[^0-9.]+/g, ""));
          return null;
        }),
        otherwise: Yup.number(),
      })
      .when("type", {
        is: ExpenseReserveTypes["Flat Fee"],
        then: Yup.number()
          .transform((value, ogValue) => {
            if (typeof ogValue === "number") return ogValue;
            if (ogValue) return Number(ogValue.replace(/[^0-9.]+/g, ""));
            return null;
          })
          .max(
            ((tenantContext?.dealLimits?.maxExpenseReserve ??
              DEFAULT_FEE_PERCENT) /
              100) *
              deal.targetRaiseAmount,
            `The total Expense Reserve amount cannot be greater than ${
              tenantContext?.dealLimits?.maxExpenseReserve ??
              DEFAULT_FEE_PERCENT
            }% of the Target Raise Amount (${numeral(
              ((tenantContext?.dealLimits?.maxExpenseReserve ??
                DEFAULT_FEE_PERCENT) /
                100) *
                deal.targetRaiseAmount
            ).format("$0,0.00")})`
          ),
      })
      .when("type", {
        is: ExpenseReserveTypes["Percent"],
        then: Yup.number()
          .transform((value, ogValue) => {
            if (typeof ogValue === "number") return ogValue;
            if (ogValue) return Number(ogValue.replace(/[^0-9.]+/g, ""));
            return null;
          })
          .max(
            tenantContext?.dealLimits?.maxExpenseReserve ?? DEFAULT_FEE_PERCENT,
            `The total Expense Reserve amount cannot be greater than ${
              tenantContext?.dealLimits?.maxExpenseReserve ??
              DEFAULT_FEE_PERCENT
            }% of the Target Raise Amount`
          ),
      }),
  });

  return (
    <Formik
      initialValues={{
        hasExpenseReserve: entity?.expenseReserve?.amount > 0,
        amount: entity?.expenseReserve?.amount,
        type: entity?.expenseReserve?.type ?? ExpenseReserveTypes.Percent,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <FormField
            name="hasExpenseReserve"
            label="The Fund have an Expense Reserve?"
            placeholder="Will the Fund have an Expense Reserve?"
            component={Form.Checkbox}
            onClick={() => setHasReserve(!hasReserve)}
          />
          {hasReserve && (
            <>
              <Form.Group>
                <FormField
                  label="Percent"
                  component={Form.Radio}
                  name="type"
                  value={ExpenseReserveTypes.Percent}
                />
                <FormField
                  label="Flat Fee"
                  component={Form.Radio}
                  name="type"
                  value={ExpenseReserveTypes["Flat Fee"]}
                />
              </Form.Group>
              {/*<MaskedFormField*/}
              {/*  id="amount"*/}
              {/*  name="amount"*/}
              {/*  thousandSeparator={true}*/}
              {/*  allowNegative={false}*/}
              {/*  decimalScale={0}*/}
              {/*  type="tel"*/}
              {/*  label="Amount"*/}
              {/*  placeholder={props.values.type === "Flat Fee" ? "0" : "0%"}*/}
              {/*  required*/}
              {/*/>*/}
              {props.values.type !== ExpenseReserveTypes["Flat Fee"] && (
                <MaskedFormField
                  name="amount"
                  thousandSeparator={false}
                  allowNegative={false}
                  suffix={"%"}
                  decimalScale={2}
                  type="tel"
                  label={`Amount`}
                  placeholder={"0%"}
                />
              )}
              {props.values.type === ExpenseReserveTypes["Flat Fee"] && (
                <MaskedFormField
                  name="amount"
                  thousandSeparator={true}
                  allowNegative={false}
                  prefix={"$"}
                  decimalScale={0}
                  type="tel"
                  label={`Amount`}
                  placeholder={"$1500"}
                />
              )}
            </>
          )}
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            disabled={!props.dirty || !props.isValid}
            loading={isSubmitting}
          />
        </Form>
      )}
    </Formik>
  );
};

export default EditReserveForm;
