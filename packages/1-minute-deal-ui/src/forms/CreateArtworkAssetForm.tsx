import React, { useState, useEffect } from "react";
import * as Yup from "yup";

import {
  Segment,
  Form,
  Button,
  Header,
  Divider,
  Popup,
  Icon,
} from "semantic-ui-react";
import GalleryUpload from "../components/GalleryUpload";
import { Field, Formik } from "formik";
import { MaskedFormField, FormField } from "./common/FormField";
import SubformDate, {
  validationSchema as DateValidationSchema,
  defaultValues as DateDefaultValues,
} from "./subforms/SubformDate";
import moment from "moment";
import ReactQuill from "react-quill";
import QuillField from "./common/QuillField";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function CreateArtworkAssetForm({
  onSubmit,
  submitting = false,
}: Props) {
  const [artworkGallery, setArtworkGallery] = useState([]);

  const validation = Yup.object().shape({
    name: Yup.string().required(),
    description: Yup.string().trim(),
    appraisedValue: Yup.number()
      .transform(function (value, originalvalue) {
        return Number(originalvalue.replace(/\D+/g, ""));
      })
      .required(),
    artist: Yup.object()
      .default(null)
      .nullable()
      .shape({
        name: Yup.string().typeError("Artist Name is required"),
        location: Yup.string().trim(),
      }),
  });

  return (
    <>
      <Formik
        initialValues={{
          type: "ARTWORK",
          images: artworkGallery,
        }}
        enableReinitialize={true}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <FormField
              id="name"
              name="name"
              label="Name of Artwork"
              placeholder="Candles of Uncertainty"
              required
            />
            <QuillField
              id="description"
              name="description"
              label="Artwork Description"
              placeholder="Provide a description of this piece"
            />
            <Form.Group widths="equal">
              <FormField
                id="size"
                name="size"
                label="Size"
                placeholder='10" x 10"'
              />
              <FormField
                id="category"
                name="category"
                label="Category"
                placeholder="oil, canvas, sculpture..."
              />
            </Form.Group>

            <Form.Group widths="equal">
              <MaskedFormField
                id="appraisedValue"
                name="appraisedValue"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Appraised Value"
                placeholder="$100,000"
                required
              />
              <FormField
                id="additionalProperties.dateComplete"
                name="additionalProperties.dateComplete"
                label="Date of Completion"
                placeholder=""
              />
            </Form.Group>

            <div className={"field bold"}>
              <label>Image Gallery</label>
            </div>
            <GalleryUpload
              fileArray={artworkGallery}
              setFileArray={setArtworkGallery}
            />

            <Header as="h3" inverted textAlign="center">
              About the Artist
            </Header>
            <Divider inverted />
            <Form.Group widths="equal">
              <FormField
                id="artist.Name"
                name="artist.Name"
                label="Artist Name"
                placeholder="Caesar Pierce"
              />
              <FormField
                id="artist.Location"
                name="artist.Location"
                label="Artist Location"
                placeholder="New York, NY"
              />
            </Form.Group>

            <Header as="h3" inverted textAlign="center">
              About the Offering
            </Header>
            <Divider inverted />

            <Form.Group widths="equal">
              <MaskedFormField
                id="additionalProperties.amount"
                name="additionalProperties.amount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Amount"
                placeholder="$1,000"
              />
              <MaskedFormField
                id="fractionalOwnershipAmount"
                name="fractionalOwnershipAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Fractional Ownership Amount"
                placeholder="$10,000"
              />
            </Form.Group>
            <Form.Group widths="equal">
              <MaskedFormField
                id="additionalProperties.entireAmount"
                name="additionalProperties.entireAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Entire Piece Amount"
                placeholder="$1,000,000"
              />
              <MaskedFormField
                id="additionalProperties.bidSoftCommitment"
                name="additionalProperties.bidSoftCommitment"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Bid/Soft Commitment"
                placeholder="$10,000"
              />
            </Form.Group>

            <Divider hidden />
            <Form.Group>
              <Form.Field width={10} />
              <Form.Button
                width={6}
                secondary
                type="submit"
                loading={submitting}
                fluid
                content="Add Artwork"
              />
            </Form.Group>
          </Form>
        )}
      </Formik>
    </>
  );
}
