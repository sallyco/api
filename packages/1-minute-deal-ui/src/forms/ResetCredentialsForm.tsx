import React from "react";
import { Form, Button, Segment, Header, Message } from "semantic-ui-react";

import { Formik } from "formik";

import SubformPassword, * as SubformPasswordMeta from "./subforms/SubformPassword";

import * as Yup from "yup";

interface Props {
  title?: string;
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function ResetCredentialsForm({
  title = "Reset Password",
  onSubmit,
  submitting = false,
}: Props) {
  const validation = Yup.object().shape({
    ...SubformPasswordMeta.validationSchema(),
  });

  return (
    <>
      <Header as="h1" inverted textAlign="center">
        Reset Password
      </Header>
      <Formik
        initialValues={{ termsOfService: false }}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit}>
            <Segment basic inverted>
              <p>
                Don&#39;t use a password from another site, and don&#39;t
                include any personal information such as your name or date of
                birth.
              </p>
              <Message info>
                Minimum twelve characters. At least one letter, one number and
                one special character @$!%*#?&
              </Message>
            </Segment>
            <SubformPassword />

            <Button
              secondary
              type="submit"
              loading={submitting}
              fluid
              content="Change Password"
            />
          </Form>
        )}
      </Formik>
    </>
  );
}
