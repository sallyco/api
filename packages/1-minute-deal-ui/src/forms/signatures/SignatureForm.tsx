import React, { useRef } from "react";
import { Button, Container } from "semantic-ui-react";
import SignatureCanvas from "react-signature-canvas";
import { FieldHookConfig, useField } from "formik";
import * as Yup from "yup";

export const validationSchema = ({ fieldName }: { fieldName: string }) => {
  return {
    [fieldName]: Yup.string().min(2).required(),
  };
};

const SignatureForm: React.FC<FieldHookConfig<string>> = (
  props: FieldHookConfig<string>
) => {
  const sigCanvas = useRef(SignatureCanvas) as React.MutableRefObject<any>;
  const [field, fieldMeta, fieldHelper] = useField(props);

  return (
    <>
      <input {...field} type={"hidden"} />
      <SignatureCanvas
        ref={sigCanvas}
        penColor="black"
        backgroundColor="white"
        canvasProps={{
          width: 380,
          height: 200,
          className: "sigCanvas",
        }}
        onEnd={() => {
          const value = sigCanvas.current
            .getTrimmedCanvas()
            .toDataURL("image/png");
          fieldHelper.setValue(value);
        }}
      />
      <Button
        color="orange"
        fluid
        onClick={() => {
          fieldHelper.setValue(undefined);
          sigCanvas.current.clear();
        }}
        attached="bottom"
        size="small"
      >
        Clear
      </Button>
    </>
  );
};

export default SignatureForm;
