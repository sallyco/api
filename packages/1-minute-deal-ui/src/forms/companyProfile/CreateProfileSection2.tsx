import React, { useEffect } from "react";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button, Divider, Header, Progress } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";
import QuillField from "../common/QuillField";
interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export const CreateProfileSection2: React.FunctionComponent<Props> = ({
  onSubmit,
  submitting = false,
}) => {
  const dispatch = useDispatch();
  const validation = Yup.object().shape({
    // customerProblem: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // targetMarketSize: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // customerPotential: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // businessModel: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // competitiveAdvantage: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // productsServices: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // marketingStrategy: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
    // competitors: Yup.string()
    //
    //   .trim()
    //   .min(8)
    //   .max(800)
    //   .required(),
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Header as="h5" color="green" textAlign="center">
        SECTION 2 to 4
      </Header>
      <Progress value={2} total={4} size="small" success />
      <Header as="h3" inverted>
        Excecutive Summary
        <Header.Subheader>
          Tell us about your customer, market, business model, product and
          revenue
        </Header.Subheader>
      </Header>
      <Divider inverted />
      <Formik
        initialValues={{
          profileform: 2,
          customerProblem: "",
          targetMarketSize: "",
          customerPotential: "",
          businessModel: "",
          competitiveAdvantage: "",
          productsServices: "",
          marketingStrategy: "",
          competitors: "",
        }}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit}>
            <QuillField
              name="customerProblem"
              label="Customer Problem"
              placeholder="Customer Problem"
              required
            />
            <QuillField
              name="targetMarketSize"
              label="Target Market & Market Size"
              placeholder="Target & Market Size"
              required
            />
            <QuillField
              name="customerPotential"
              label="Customers - Current / Potential"
              placeholder="Customers - Current / Potential"
              required
            />
            <QuillField
              name="businessModel"
              label="Business Model"
              placeholder="Business Model"
              required
            />
            <QuillField
              name="competitiveAdvantage"
              label="Competitve Advantage"
              placeholder="Competitve Advantage"
              required
            />
            <QuillField
              name="productsServices"
              label="Product / Service"
              placeholder="Product / Service"
              required
            />
            <QuillField
              name="marketingStrategy"
              label="Sales / Marketing Strategy"
              placeholder="Sales / Marketing Strategy"
              required
            />
            <QuillField
              name="competitors"
              label="Competitors"
              placeholder="Competitors"
              required
            />
            <Button secondary type="submit" loading={submitting} fluid>
              Save & Continue
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default CreateProfileSection2;
