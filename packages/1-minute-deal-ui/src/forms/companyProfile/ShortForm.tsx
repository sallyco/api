import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { Form, Button, Divider, Header } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField, MaskedFormField } from "../common/FormField";
import Log from "../../tools/Log";
import { defaultValues as DateDefaultValues } from "../subforms/SubformDate";
import SubformEmail, * as SubformEmailMeta from "../subforms/SubformEmail";
import SubformPhone, * as SubformPhoneMeta from "../subforms/SubformPhone";
import FormHint from "../common/FormHint";
import HelpIcon from "@mui/icons-material/Help";
import { Box } from "@mui/material";
import { SecurityTypes } from "../../tools/enums";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export const ShortForm: React.FunctionComponent<Props> = ({
  onSubmit,
  submitting = false,
}) => {
  const dispatch = useDispatch();
  const [isError, setisError] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const validation = Yup.object()
    .notRequired()
    .default(undefined)
    .shape({
      name: Yup.string().min(3).required(),
      contactName: Yup.string()
        .min(3)
        .required("Company Contact Name is Required"),
      assetUrl: Yup.string()
        .matches(
          /^((https?):\/\/)?(www.)?[a-zA-Z0-9]+(\.[a-zA-Z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
          "Valid URL required"
        )
        .required("Valid URL Required"),
      properties: Yup.object()
        .default(null)
        .nullable()
        .shape({
          location: Yup.string().trim().required("Location is Required"),
        }),
      ...SubformEmailMeta.validationSchema({
        isRequired: true,
        namePrefix: "company",
      }),
      ...SubformEmailMeta.validationSchema({
        isRequired: true,
        namePrefix: "founder",
      }),
      ...SubformPhoneMeta.validationSchema({
        isRequired: true,
        namePrefix: "company",
      }),
      targetRaiseAmount: Yup.string().trim().min(2),
      securityType: Yup.string().trim(),
    })
    .default(null)
    .nullable();

  const presubmit = (data: any) => {
    Log.debug("data", data);
    seterrorMessage("");
    setisError(false);
    onSubmit(data);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Header as="h3" inverted textAlign="center">
        Fill out the required sections to circulate your company
        <Divider hidden />
      </Header>

      <Header as="h3" inverted>
        Company Basics
      </Header>
      <Divider inverted />
      <Formik
        initialValues={{
          profileform: 1,
          properties: {
            // marketSector: "",
            // incorporationType: "",
            // companyRevenueStage: "",
            // noOfEmployees: "",
            location: "",
          },
          // description: "",
          assetUrl: "",
          // details: "",
          // team: [],
          ...DateDefaultValues,
          contactName: "",
          companyphone: "",
          companyemail: "",
          foundername: "",
          founderemail: "",
        }}
        enableReinitialize={true}
        validationSchema={validation}
        onSubmit={presubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            {/* <Form onSubmit={onSubmit} autoComplete="off"> */}
            <Form.Group widths="equal">
              <FormField
                id="name"
                name="name"
                label="Company Name"
                placeholder="Venture BC, LLC"
                autocomplete="organization"
                required
              />
            </Form.Group>

            <Form.Group widths="equal">
              <FormField
                name="assetUrl"
                id="assetUrl"
                component={Form.Input}
                label="Company Website"
                placeholder="myventure.com"
                autocomplete="url"
                required
              />
              <FormField
                name="properties.location"
                id="properties.location"
                component={Form.Input}
                label="Location"
                placeholder="Salt Lake City, UT, USA"
                autocomplete="street-address"
                required
              />
            </Form.Group>

            <Form.Group widths="equal">
              <SubformEmail
                required
                namePrefix={"company"}
                label={"Company Email"}
              />
              <SubformPhone
                required
                namePrefix={"company"}
                label={"Company Phone"}
              />
            </Form.Group>
            <FormField
              id="contactName"
              name="contactName"
              label="Company Contact Name"
              placeholder="John Doe"
              autocomplete={"name"}
              required
            />

            <Header as="h3" inverted>
              Founder Information
            </Header>
            <Divider inverted />
            <FormField
              name="founderName"
              label="Founder / CEO Name"
              placeholder="John Doe"
              autocomplete={"name"}
              required
            />
            <Form.Group widths="equal">
              <SubformEmail
                required
                namePrefix={"founder"}
                label={"Founder / CEO Email"}
              />
              <FormHint
                content={
                  <Box m={1}>
                    The Founder / CEO with this email will be able to manage
                    this Company&apos;s details.
                    <div>
                      If they are not already on the platform, they will be
                      invited.
                    </div>
                  </Box>
                }
              >
                <HelpIcon
                  style={{
                    color: "white",
                    backgroundColor: "#40000",
                  }}
                />
              </FormHint>
            </Form.Group>

            <Header as="h3" inverted textAlign="left">
              Funding
            </Header>
            <Divider inverted />

            <Form.Group widths="equal">
              <MaskedFormField
                id="targetRaiseAmount"
                name="targetRaiseAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Target Raise Amount"
                placeholder="$500,000"
              />
              <FormField
                name="securityType"
                id="securityType"
                component={Form.Select}
                label="Security Type"
                options={SecurityTypes}
                placeholder="Select a Security Type"
              />
            </Form.Group>
            <Form.Group widths="equal">
              <FormField
                name="noOfShare"
                id="noOfShare"
                component={Form.Input}
                label="Number of Shares"
                placeholder="Number of Shares"
                type="number"
              />
              <MaskedFormField
                name="sharePrice"
                id="sharePrice"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Share Price"
                placeholder="$500,000"
              />
            </Form.Group>
            <Divider hidden />
            <Button
              secondary
              type="submit"
              disabled={!props.dirty || !props.isValid}
              loading={submitting}
              fluid
              content="Save & Continue"
            />
          </Form>
        )}
      </Formik>
    </>
  );
};
export default ShortForm;
