import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import {
  Form,
  Button,
  Divider,
  Header,
  Progress,
  Icon,
  Segment,
  Card,
  Label,
  Input,
} from "semantic-ui-react";
import { Field, Formik, FormikProps } from "formik";
import { FormField } from "../common/FormField";
import {
  CompanyMarketSector,
  EntityTypes,
  Revenue,
  NumberOfemplyees,
} from "../../tools/enums";
import Log from "../../tools/Log";
import SubformDate, {
  validationSchema as DateValidationSchema,
  defaultValues as DateDefaultValues,
} from "../subforms/SubformDate";
import moment from "moment";
import SubformEmail, * as SubformEmailMeta from "../subforms/SubformEmail";
import SubformPhone, * as SubformPhoneMeta from "../subforms/SubformPhone";
import ReactQuill from "react-quill";
import QuillField from "../common/QuillField";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export const CreateProfileSection1: React.FunctionComponent<Props> = ({
  onSubmit,
  submitting = false,
}) => {
  const dispatch = useDispatch();
  const [isError, setisError] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const [uploadImage, setUploadImage]: any = useState([]);
  const [uploadImageName, setUploadImageName]: any = useState([]);
  const [teamMemberLength, setTeamMemberLength]: any = useState(1);
  const fileInputRef: any = useRef();
  const validation = Yup.object()
    .notRequired()
    .default(undefined)
    .shape({
      name: Yup.string()

        .min(3)
        .required(),
      properties: Yup.object().default(null).nullable().shape({
        // marketSector: Yup.string().typeError(
        //   "Company Market Sector is required"
        // ),
        // incorporationType: Yup.string().typeError(
        //   "Incorporation Type is required"
        // ),
        // companyRevenueStage: Yup.string().typeError(
        //   "Company Revenue Stage is required"
        // ),
        // noOfEmployees: Yup.string().typeError(
        //   "Number of Employees is required"
        // ),
        location: Yup.string().trim(),
      }),
      // description: Yup.string().trim().min(8).max(600).required(),
      assetUrl: Yup.string()
        .matches(
          /^((https?):\/\/)?(www.)?[a-zA-Z0-9]+(\.[a-zA-Z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
          "Valid URL required"
        )
        .required(),
      ...SubformEmailMeta.validationSchema({ isRequired: true }),
      ...SubformPhoneMeta.validationSchema({ isRequired: true }),
      ...DateValidationSchema({ isRequired: true }),
      details: Yup.string().trim(),
      // teamMember: Yup.object({
      //   name: Yup.string().trim().min(3),
      //   role: Yup.string(),
      //   //linkedInProfile: Yup.string().matches(
      //   //  /^((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
      //   //  "Please enter a valid URL"
      //   //),
      // }),
      advisors: Yup.string()

        .trim()
        .required(),
    })
    .test("dateFoundedGate", "dateFoundedGate", (obj) => {
      let ecd = new Date(`${obj.year}-${obj.month}-${obj.day}`);
      let today = new Date();
      if (moment(ecd).isSameOrBefore(today)) {
        return true;
      } else {
        return new Yup.ValidationError(
          "Date Founded cannot be in the future",
          null,
          "year"
        );
      }
    })
    .default(null)
    .nullable();

  const fileChange = (e) => {
    Log.debug("file ", e.target.files[0]);
    const file = e.target.files[0];
    let reader = new FileReader();
    if (file.size <= 2097152) {
      setisError(false);
      seterrorMessage("");
      reader.onload = function (event) {
        Log.debug("event ", event);
        let imageNameArray = uploadImageName;
        imageNameArray.splice(teamMemberLength, 0, file.name);
        setUploadImageName(imageNameArray);
        let uploadImageArray = uploadImage;
        uploadImageArray.splice(
          teamMemberLength,
          0,
          event?.target?.result ?? undefined
        );
        setUploadImage(uploadImageArray);
      };
      reader.readAsDataURL(file);
    } else {
      setisError(true);
      seterrorMessage("Your file is too big");
      setUploadImage(undefined);
    }
  };

  const presubmit = (data: any) => {
    Log.debug("data", data);
    if (uploadImage?.length) {
      seterrorMessage("");
      setisError(false);
      uploadImage.forEach((img, index) => {
        data["team"][index]["image"] = img;
      });
      onSubmit(data);
    } else {
      seterrorMessage("Photo is a required field");
      setisError(true);
    }
  };

  const removeTeamMember = (index: number, props: FormikProps<any>) => {
    let imageNameArray = uploadImageName;
    imageNameArray.splice(index, 1);
    setUploadImageName(imageNameArray);
    let uploadImageArray = uploadImage;
    uploadImageArray.splice(index, 1);
    setUploadImage(uploadImageArray);
    setTeamMemberLength(teamMemberLength - 1);
    let teamMembersArray = props.values.team;
    teamMembersArray.splice(index, 1);
    props.setFieldValue("team", teamMembersArray);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  interface FormValues {
    teamMember: {
      name: string;
      role: string;
      linkedinUrl: string;
      image: string;
    };
  }

  const initialValues: FormValues = {
    teamMember: {
      name: "",
      role: "",
      linkedinUrl: "",
      image: "",
    },
  };

  return (
    <>
      <Header as="h3" inverted textAlign="center">
        Fill out the required sections to circulate your company
        <Divider hidden />
        <Header.Subheader>
          Add a company overview, executive summary, financials, and a pitch
          deck, along with any supporting documents.
        </Header.Subheader>
      </Header>
      <Header as="h5" color="green" textAlign="center">
        SECTION 1 to 4
      </Header>
      <Progress value={1} total={4} size="small" success />

      <Header as="h3" inverted>
        Company Basics
        <Header.Subheader>
          Include your elevator pitch and some essentials about your company
        </Header.Subheader>
      </Header>
      <Divider inverted />
      <Formik
        initialValues={{
          profileform: 1,
          properties: {
            marketSector: "",
            incorporationType: "",
            companyRevenueStage: "",
            noOfEmployees: "",
            location: "",
          },
          description: "",
          assetUrl: "",
          details: "",
          team: [],
          ...DateDefaultValues,
        }}
        enableReinitialize={true}
        validationSchema={validation}
        onSubmit={presubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            {/*
            <Prompt
              message={
                "Your Company has not been created yet. Leaving now will erase the entered information."
              }
              when={!props.isSubmitting && props.dirty}
            />
*/}
            <Form.Group widths="equal">
              <FormField
                id="name"
                name="name"
                label="Company Name"
                placeholder="Venture BC, LLC"
                required
              />
              <FormField
                name="properties.marketSector"
                id="properties.marketSector"
                component={Form.Select}
                label="Company Market Sector"
                placeholder="Financials"
                options={CompanyMarketSector}
              />
            </Form.Group>
            <QuillField
              name={"description"}
              label={"One-Line Pitch"}
              placeholder={"A quick description of your company"}
            />
            <Form.Group widths="equal">
              <FormField
                name="properties.incorporationType"
                id="properties.incorporationType"
                component={Form.Select}
                label="Incorporation Type"
                placeholder="LLC"
                options={EntityTypes}
              />
            </Form.Group>
            <Form.Group>
              <SubformDate
                id={"dateFounded"}
                label={"Date Founded"}
                name={"dateFounded"}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <FormField
                name="properties.noOfEmployees"
                id="properties.noOfEmployees"
                component={Form.Select}
                label="Number Of Employees"
                options={NumberOfemplyees}
                placeholder="10-50"
              />
              <FormField
                name="properties.companyRevenueStage"
                id="properties.companyRevenueStage"
                component={Form.Select}
                label="Company Revenue Stage"
                placeholder="$5M in TTM Revenue"
                options={Revenue}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <FormField
                name="properties.location"
                id="properties.location"
                component={Form.Input}
                label="Location"
                placeholder="Salt Lake City, UT, USA"
              />
              <FormField
                name="assetUrl"
                id="assetUrl"
                component={Form.Input}
                label="Company Website"
                placeholder="myventure.com"
                required
              />
            </Form.Group>

            <Header as="h3" inverted textAlign="left">
              Contact Info
            </Header>
            <Divider inverted />

            <Form.Group widths="equal">
              <SubformEmail />
              <SubformPhone />
            </Form.Group>

            <Header as="h3" inverted textAlign="left">
              Management
              <Header.Subheader>
                Add key team members, excecutives, directores and partners
              </Header.Subheader>
            </Header>
            <Divider inverted />

            <QuillField
              id={"details"}
              name={"details"}
              label={"Provide an Overview of Your Management"}
              placeholder={"Provide an Overview of Your Management"}
            />

            {Array.from({ length: teamMemberLength }, (x, i) => i).map(
              (index) => (
                <Segment inverted key={"management-" + index}>
                  <Form.Group>
                    <Header inverted as={"h4"}>
                      Team Member
                    </Header>
                    {index > 0 && (
                      <>
                        <Divider inverted />
                        <Form.Button
                          fluid
                          negative
                          onClick={() => removeTeamMember(index, props)}
                          content="Remove Team Member"
                          type="button"
                          icon="remove user"
                        />
                      </>
                    )}
                  </Form.Group>
                  <Form.Group widths="equal">
                    <FormField
                      id={`team[${index}].name`}
                      name={`team[${index}].name`}
                      label="Name"
                      placeholder="Christine Banfield"
                      required
                    />
                    <FormField
                      id={`team[${index}].role`}
                      name={`team[${index}].role`}
                      label="Role"
                      placeholder="Founder and CEO"
                      required
                    />
                  </Form.Group>
                  <FormField
                    id={`team[${index}].linkedInUrl`}
                    name={`team[${index}].linkedInUrl`}
                    label="LinkedIn Profile URL"
                    placeholder="linkedin.com/profile"
                  />
                  <Divider hidden />

                  <div className={`field`}>
                    <Form.Button
                      fluid
                      secondary
                      onClick={() => fileInputRef.current.click()}
                      content="Upload A Photo *"
                      type="button"
                      icon="user circle"
                    />
                    <input
                      ref={fileInputRef}
                      type="file"
                      id={`team[${index}].image`}
                      name={`team[${index}].image`}
                      hidden
                      onChange={fileChange}
                      accept="image/*"
                    />

                    {uploadImage[index] && (
                      <Header color="green" as="h6">
                        <Icon color="green" name="check circle" />
                        <Header.Content>
                          {uploadImageName[index]} uploaded
                        </Header.Content>
                      </Header>
                    )}

                    {isError && (
                      <div className="ui pointing above prompt label">
                        {errorMessage}
                      </div>
                    )}
                  </div>
                </Segment>
              )
            )}

            <Form.Button
              fluid
              secondary
              onClick={() => setTeamMemberLength(teamMemberLength + 1)}
              content="Add Team Member"
              type="button"
              icon="add user"
            />

            <Divider hidden />
            <Header as="h3" inverted textAlign="left">
              Advisors
            </Header>
            <Divider inverted />
            <FormField
              id="advisors"
              name="advisors"
              component={Form.Input}
              label="List Advisor Names (Comma Separated)"
              placeholder="Annie Northrend, Hashir Johnson"
              required
            />
            <Divider hidden />
            <Button
              secondary
              type="submit"
              loading={submitting}
              fluid
              content="Save & Continue"
            />
          </Form>
        )}
      </Formik>
    </>
  );
};
export default CreateProfileSection1;
