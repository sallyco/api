import React, { useEffect } from "react";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { SecurityTypes, RoundTypes } from "../../tools/enums";
import { Form, Button, Divider, Header, Progress } from "semantic-ui-react";
import { Formik } from "formik";
import { MaskedFormField, FormField } from "../common/FormField";
import CurrencyInputField from "./CurrencyInputField";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export const CreateProfileSection3: React.FunctionComponent<Props> = ({
  onSubmit,
  submitting = false,
}) => {
  const dispatch = useDispatch();

  const validation = Yup.object().shape({
    round: Yup.string().trim().required(),
    targetRaiseAmount: Yup.string().trim().min(2).required(),
    securityType: Yup.string().trim().required(),
    preValuation: Yup.string().trim(),
    revenuePreYear: Yup.string().trim(),
    revenue2YearBack: Yup.string().trim(),
    revenue3YearBack: Yup.string().trim(),
    costsPreYear: Yup.string().trim(),
    costs2YearBack: Yup.string().trim(),
    costs3YearBack: Yup.string().trim(),
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Header as="h5" color="green" textAlign="center">
        SECTION 3 to 4
      </Header>
      <Progress value={3} total={4} size="small" success />
      <Header as="h3" inverted>
        Current Funding Round
        <Header.Subheader>
          Provide insight on the funding you are currently seeking.
        </Header.Subheader>
      </Header>
      <Divider inverted />
      <Formik
        initialValues={{ profileform: 3 }}
        enableReinitialize={true}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            <Form.Group widths="equal">
              <FormField
                name="round"
                id="round"
                component={Form.Select}
                label="Round"
                options={RoundTypes}
                placeholder="Provide Round"
                required
              />
              <MaskedFormField
                id="targetRaiseAmount"
                name="targetRaiseAmount"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Target Raise Amount"
                placeholder="$500,000"
                required
              />
            </Form.Group>
            <Form.Group widths="equal">
              <FormField
                name="securityType"
                id="securityType"
                component={Form.Select}
                label="Security Type"
                options={SecurityTypes}
                placeholder="Select a Security Type"
                required
              />
              <MaskedFormField
                id="preValuation"
                name="preValuation"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Pre Money Valuation"
                placeholder="Provide Pre Money Valuation"
                required
              />
            </Form.Group>
            <Header as="h3" inverted>
              Financial History
              <Header.Subheader>
                Provide number on revenue and costs for your past 3 year of
                business
              </Header.Subheader>
            </Header>

            <Divider inverted />
            <Form.Group widths="equal">
              <MaskedFormField
                id="revenuePreYear"
                name="revenuePreYear"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Revenue Previous Year"
                placeholder="Provide Revenue of Previous Year"
                required
              />
              <MaskedFormField
                id="revenue2YearBack"
                name="revenue2YearBack"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Revenue 2 Years Back"
                placeholder="Provide Revenue 2 Years Back"
                required
              />
              <MaskedFormField
                id="revenue3YearBack"
                name="revenue3YearBack"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Revenue 3 Years Back"
                placeholder="Provide Revenue 3 Years Back"
                required
              />
            </Form.Group>
            <Divider inverted />
            <Form.Group widths="equal">
              <MaskedFormField
                id="costsPreYear"
                name="costsPreYear"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Costs Previous Year"
                placeholder="Provide Costs of Previous Year"
                required
              />
              <MaskedFormField
                id="costs2YearBack"
                name="costs2YearBack"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Costs 2 Years Back"
                placeholder="Provide Costs 2 Years Back"
                required
              />
              <MaskedFormField
                id="costs3YearBack"
                name="costs3YearBack"
                thousandSeparator={true}
                allowNegative={false}
                prefix={"$"}
                decimalScale={0}
                type="tel"
                label="Costs 3 Years Back"
                placeholder="Provide Costs 3 Years Back"
                required
              />
            </Form.Group>
            <Divider hidden />
            <Button
              secondary
              type="submit"
              loading={submitting}
              fluid
              content="Save & Continue"
            />
          </Form>
        )}
      </Formik>
    </>
  );
};
export default CreateProfileSection3;
