import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { updateAssetById } from "../../slices/assetsSlice";
import {
  Form,
  Segment,
  Transition,
  List,
  Button,
  Divider,
  Header,
  Progress,
  Icon,
  Grid,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../common/FormField";
import Log from "../../tools/Log";
import { uploadCompanyFileToServer } from "../../slices/filesSlice";
import { toast } from "react-toastify";
import { ToastError } from "../../tools/ToastMessage";
import { RootState } from "../../rootReducer";
import { SubformFileUpload } from "../subforms/SubformFileUpload";
import DocumentItem from "../../components/files/DocumentItem";

interface Props {
  companyId: string;
  onSubmit: (any) => void;
}

export const CreateProfileSection4: React.FunctionComponent<Props> = ({
  companyId,
  onSubmit,
}) => {
  const validation = Yup.object().shape({
    //videoURL: Yup.string().matches(
    //  /^((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
    //  "Valid URL Required"
    //),
  });
  const dispatch = useDispatch();

  const [pitchDeck, setPitchDeck]: any = useState("");
  const [uploadedFiles, setUploadedFiles]: any = useState([]);

  const [isError, setisError] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const [isError1, setisError1] = useState(false);
  const [errorMessage1, seterrorMessage1] = useState("");
  const [isError2, setisError2] = useState(false);
  const [errorMessage2, seterrorMessage2] = useState("");
  const fileInputRef: any = useRef();
  const fileInputRef1: any = useRef();
  const fileInputRef2: any = useRef();

  const { isSubmitting } = useSelector((state: RootState) => state.files);

  const fileChange = (e) => {
    var pitchDoc = e.target.files[0];
    if (pitchDoc.size <= 8388608) {
      setisError(false);
      seterrorMessage("");
    } else {
      setisError(true);
      seterrorMessage("Your file is too big");
    }
  };
  const fileChange1 = (e) => {
    var size1 = 0;
    var documents = e.target.files;
    if (documents.length === 1) {
      let file = documents[0];
      if (file.size <= 8388608) {
        setisError1(false);
        seterrorMessage1("");
      } else {
        setisError1(true);
        seterrorMessage1("Your file is too big");
      }
    } else {
      for (var i = 0; i < documents.length; i++) {
        let file = documents[i];
        size1 += file.size;
        if (size1 <= 8388608) {
          setisError1(false);
          seterrorMessage1("");
        } else {
          setisError1(true);
          seterrorMessage1("Your file is too big");
        }
      }
    }
  };
  const fileChange2 = (e) => {
    var vid = e.target.files[0];
    if (vid.size <= 2147483648) {
      setisError2(false);
      seterrorMessage2("");
    } else {
      setisError2(true);
      seterrorMessage2("Your file is too big");
    }
  };

  async function presubmit(data: any) {
    try {
      const formData = new FormData();

      var pitchDoc = fileInputRef.current.files[0];
      if (pitchDoc) {
        seterrorMessage("");
        setisError(false);
        if (pitchDoc.size <= 8388608) {
          setisError(false);
          seterrorMessage("");
          formData.append("pitchDoc", pitchDoc, pitchDoc.name);
        } else {
          setisError(true);
          seterrorMessage("Your file is too big");
        }

        var size = 0;
        var documents = fileInputRef1.current.files;
        if (documents) {
          if (documents.length === 1) {
            let file = documents[0];
            if (file.size <= 8388608) {
              setisError1(false);
              seterrorMessage1("");
              formData.append("documents", file, file.name);
            } else {
              setisError1(true);
              seterrorMessage1("Your file is too big");
            }
          } else {
            for (var i = 0; i < documents.length; i++) {
              let file = documents[i];
              size += file.size;
              if (size <= 8388608) {
                setisError1(false);
                seterrorMessage1("");
                formData.append("documents", file, file.name);
              } else {
                setisError1(true);
                seterrorMessage1("Your file is too big");
              }
            }
          }
        }

        var video = fileInputRef2.current.files[0];
        if (video) {
          if (video.size <= 2147483648) {
            setisError2(false);
            seterrorMessage2("");
            formData.append("video", video, video.name);
          } else {
            setisError2(true);
            seterrorMessage2("Your file is too big");
          }
        } else {
          const url = data.videoURL ? data.videoURL : "";
          if (url) {
            formData.append(url, new Blob(), "videoURL");
          }
        }

        if (isError === false && isError1 === false && isError2 === false) {
          const company: any = await dispatch(
            uploadCompanyFileToServer(formData, companyId)
          ); //companyId
          Log.debug("profile", company);
          toast.success(`Company "${company.name}" Updated`);
          onSubmit(data);
        }
      } else {
        seterrorMessage("A pitch deck is required");
        setisError(true);
      }
    } catch (e: any) {
      if (e.response) {
        ToastError(e.response);
      }
    }
  }

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [dispatch]);

  return (
    <>
      <Header as="h5" color="green" textAlign="center">
        SECTION 4 OF 4
      </Header>
      <Progress value={4} total={4} size="small" success />
      <Header as="h3" inverted>
        Documents and Pitch Deck
        <Header.Subheader>
          Provide insight on the funding you are currently seeking.
        </Header.Subheader>
      </Header>
      <Divider inverted />
      <Formik
        initialValues={{ profileform: 4 }}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props: any) => (
          <Form onSubmit={props.handleSubmit}>
            <Grid>
              <Grid.Column textAlign="center">
                <div className={`field`}>
                  <SubformFileUpload
                    onSuccess={async (fileIds) => {
                      const upload = fileIds[0];
                      Log.debug("newfiles", upload);

                      await dispatch(
                        updateAssetById({
                          id: companyId,
                          pitchDoc: upload,
                        })
                      );

                      setPitchDeck(fileIds);

                      toast.success(`File uploaded to company`);
                    }}
                  >
                    {({ triggerFileUpload, uploading }) => (
                      <Button
                        inverted
                        fluid
                        type="button"
                        loading={uploading}
                        onClick={triggerFileUpload}
                      >
                        <Icon name="file alternate outline" /> Upload A Pitch
                        Deck
                      </Button>
                    )}
                  </SubformFileUpload>
                  {pitchDeck && (
                    <Segment inverted textAlign="center">
                      <Transition.Group
                        as={List}
                        duration={200}
                        divided
                        verticalAlign="middle"
                        inverted
                        relaxed
                      >
                        <DocumentItem key={pitchDeck} fileId={pitchDeck} />
                      </Transition.Group>
                    </Segment>
                  )}
                  <br />
                  <SubformFileUpload
                    onSuccess={async (fileIds) => {
                      const upload = fileIds;
                      Log.debug("newfiles", upload);

                      await dispatch(
                        updateAssetById({
                          id: companyId,
                          files: upload,
                        })
                      );

                      setUploadedFiles([...fileIds]);

                      toast.success(`File uploaded to deal`);
                    }}
                  >
                    {({ triggerFileUpload, uploading }) => (
                      <Button
                        inverted
                        fluid
                        type="button"
                        loading={uploading}
                        onClick={triggerFileUpload}
                      >
                        <Icon name="paperclip" /> Upload Supporting Files
                      </Button>
                    )}
                  </SubformFileUpload>

                  {uploadedFiles.length > 0 && (
                    <Segment inverted textAlign="center">
                      <Transition.Group
                        as={List}
                        duration={200}
                        divided
                        verticalAlign="middle"
                        inverted
                        relaxed
                      >
                        {uploadedFiles.map((fileId) => (
                          <DocumentItem key={fileId} fileId={fileId} />
                        ))}
                      </Transition.Group>
                    </Segment>
                  )}
                </div>
                <Divider hidden />
                <Header inverted textAlign="left">
                  <Header.Subheader>
                    Supporting files may include items for due diligence such as
                    product one-pagers, marketing material, and financial
                    projections.
                  </Header.Subheader>
                </Header>
                <Divider hidden />
                <div className={`field`}>
                  <Form.Button
                    fluid
                    inverted
                    onClick={() => fileInputRef2.current.click()}
                    content="Upload a 1-5min video"
                    type="button"
                    icon="youtube"
                  />
                  <input
                    ref={fileInputRef2}
                    type="file"
                    name="headshot"
                    hidden
                    onChange={fileChange2}
                    accept="video/*"
                  />
                  {isError2 && (
                    <div className="ui pointing above prompt label">
                      {errorMessage2}
                    </div>
                  )}
                </div>
                {/* <Button inverted fluid type="button">
                  <Icon name="youtube" />
                  UPLOAD A 1-5MIN VIDEO
                </Button> */}
                <Header inverted textAlign="center">
                  <Header.Subheader>-OR-</Header.Subheader>
                </Header>
                <Divider hidden />
              </Grid.Column>
            </Grid>
            <FormField
              name="videoURL"
              component={Form.Input}
              label="Provide a link to a video"
              placeholder="https://www.youtube.com/watch?..."
            />
            <Divider hidden />
            <Button secondary loading={isSubmitting} type="submit" fluid>
              Save Profile
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default CreateProfileSection4;
