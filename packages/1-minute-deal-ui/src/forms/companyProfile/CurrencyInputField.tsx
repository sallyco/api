import React, { useState } from "react";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";
import { getIn } from "formik";

const CurrencyInputField = (props) => {
  const {
    className,
    field: { name, value },
    form: { errors, handleBlur, setFieldValue, touched },
    label,
    onChange,
    disabled,
    placeholder,
  } = props;

  const [isFocused, setFocused] = useState(false);
  const isError = getIn(errors, name);
  const errorStyle = isError ? "error" : "";
  const filledStyle = isFocused || value ? "filled" : "";
  const disabledStyle = disabled ? "disabled" : "";

  const handleInputBlur = (e) => {
    setFocused(false);
    handleBlur(e);
  };

  return (
    <div className={`${className}  ${filledStyle} field`}>
      <label className="transition ml-10" htmlFor={name}>
        {label}
      </label>
      <NumberFormat
        {...props}
        placeholder={placeholder}
        name={name}
        value={value}
        thousandSeparator={true}
        prefix={"$"}
        onValueChange={(val) => setFieldValue(name, val.floatValue)}
      />
      {isError && (
        <div className="ui pointing above prompt label">
          {getIn(errors, name)}
        </div>
      )}
    </div>
  );
};

CurrencyInputField.propTypes = {
  className: PropTypes.string,
  form: PropTypes.any.isRequired,
  field: PropTypes.any.isRequired,
  onChange: PropTypes.func,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
};

CurrencyInputField.defaultProps = {
  className: "",
  label: "",
  onChange: null,
  disabled: false,
  placeholder: "Enter Currency",
};

export default CurrencyInputField;
