import React from "react";
import { Popover } from "@mui/material";

const FormHint: React.FC<{
  content: JSX.Element | string;
}> = ({ children, content }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handlePopoverOpen = (element) => {
    setAnchorEl(element);
  };
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };
  const hintNameOpen = Boolean(anchorEl);
  return (
    <>
      <div
        onMouseEnter={(e) => handlePopoverOpen(e.currentTarget)}
        onMouseLeave={handlePopoverClose}
      >
        {children}
      </div>
      <Popover
        open={hintNameOpen}
        anchorEl={anchorEl}
        style={{
          pointerEvents: "none",
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        {content}
      </Popover>
    </>
  );
};

export default FormHint;
