import React, { useState } from "react";

import { styled } from "@mui/material/styles";

import {
  Dialog,
  DialogContentText,
  Typography,
  Button,
  CircularProgress,
} from "@mui/material";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import { TimesIcon } from "react-line-awesome";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";

const PREFIX = "CommonDelete";

const classes = {
  root: `${PREFIX}-root`,
  deletebutton: `${PREFIX}-deletebutton`,
  closeButton: `${PREFIX}-closeButton`,
  inputText: `${PREFIX}-inputText`,
  deleteTitle: `${PREFIX}-deleteTitle`,
};

const Root = styled("div")(({ theme }) => ({
  [`& .${classes.root}`]: {
    margin: 0,
    padding: theme.spacing(2),
  },

  [`& .${classes.deletebutton}`]: {
    float: "right",
  },

  [`& .${classes.closeButton}`]: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

  [`& .${classes.inputText}`]: {
    border: 0,
  },

  [`& .${classes.deleteTitle}`]: {
    fontWeight: 1000,
  },
}));

export interface Props {
  objectType: string;
  name: string;
  processing: boolean;
  onClickOk?: () => void;
}

const CommonDelete = ({
  objectType,
  name,
  processing,
  onClickOk = () => {},
}: Props) => {
  const [modalOpen, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOkClose = () => {
    setOpen(false);
    onClickOk();
  };

  return (
    <Root>
      <div>
        {!processing && (
          <React.Fragment>
            <Dialog
              fullWidth={true}
              onClose={handleClose}
              aria-labelledby="customized-dialog-title"
              open={modalOpen}
            >
              <DialogTitle className={classes.root}>
                <Typography
                  className={classes.deleteTitle}
                >{`DELETE ${objectType}`}</Typography>
                {modalOpen ? (
                  <IconButton
                    aria-label="close"
                    className={classes.closeButton}
                    onClick={handleClose}
                    size="large"
                  >
                    <TimesIcon />
                  </IconButton>
                ) : null}
              </DialogTitle>
              <DialogContent dividers>
                <DialogContentText id="alert-dialog-description">
                  Are you sure want to delete profile: {name}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button onClick={handleOkClose} color="primary">
                  Yes ,Delete
                </Button>
              </DialogActions>
            </Dialog>
          </React.Fragment>
        )}
        {processing && (
          <Dialog open={true}>
            <DialogContent>
              <Typography align={"center"}>
                <DialogContentText>
                  Please wait while the deleting is being processed..
                </DialogContentText>
                <CircularProgress />
              </Typography>
            </DialogContent>
          </Dialog>
        )}
      </div>
      <React.Fragment>
        <Button
          size="small"
          onClick={handleClickOpen}
          variant="contained"
          color="secondary"
          className={classes.deletebutton}
          startIcon={<DeleteIcon />}
          data-testid="delete-button"
        >
          Delete
        </Button>
      </React.Fragment>
    </Root>
  );
};

export default CommonDelete;
