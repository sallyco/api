import {
  ErrorMessage,
  Field,
  FieldAttributes,
  FieldInputProps,
  FormikBag,
} from "formik";
import ReactQuill from "react-quill";
import React from "react";
import "react-quill/dist/quill.snow.css";

interface QuillFieldProps {
  name: string;
  label?: string;
  placeholder?: string;
}
type QuillFieldAttributes = QuillFieldProps & FieldAttributes<any>;

const QuillField: React.FC<QuillFieldAttributes> = ({
  ...props
}: QuillFieldAttributes) => {
  return (
    <div className={"field"}>
      <label>{props.label}</label>
      <Field {...props} showErrorsInline={true}>
        {({
          field,
          form,
        }: {
          field: FieldInputProps<string>;
          form: FormikBag<string, string>;
        }) => (
          <ReactQuill
            style={{ background: "white" }}
            placeholder={props.placeholder}
            value={field.value ?? ""}
            onChange={field.onChange(field.name)}
            onFocus={() => form.setFieldTouched(field.name)}
            modules={{
              toolbar: [
                [{ font: [] }, { size: ["small", false, "large", "huge"] }],
                ["bold", "italic", "underline", "strike"],
                [{ color: [] }, { background: [] }],
                [{ script: "sub" }, { script: "super" }],
                [{ header: 1 }, { header: 2 }, "blockquote", "code-block"],
                [
                  { list: "ordered" },
                  { list: "bullet" },
                  { indent: "-1" },
                  { indent: "+1" },
                ],
                [{ direction: "rtl" }, { align: [] }],
                [{ align: "" }, { align: "center" }, { align: "right" }],
                ["link", "image", "video", "formula"],
                ["clean"],
              ],
            }}
          />
        )}
      </Field>
      <ErrorMessage name={props.name}>
        {(msg) => (
          <div
            className={"ui basic red pointing prompt label transition visible"}
          >
            {msg}
          </div>
        )}
      </ErrorMessage>
    </div>
  );
};
export default QuillField;
