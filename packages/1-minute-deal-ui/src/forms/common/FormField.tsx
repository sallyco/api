import React from "react";
import { Field, getIn } from "formik";
import { Form } from "semantic-ui-react";
import NumberFormat from "react-number-format";
import { toast } from "react-toastify";

interface MaskedFormFieldProps {
  name: string;
  showErrorsInline?: boolean;
  value?: any;
  [x: string]: any;
  // Additional properties: https://github.com/s-yadav/react-number-format
}

const formatErrorPath = (message: string): string => {
  let words = message.split(" ");
  if (words.length < 1) {
    toast.error("Something went wrong");
    return "";
  }
  for (let i = 0, x = words.length; i < x; i++) {
    try {
      words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    } catch {
      words[i] = "";
    }
  }
  return words.join(" ");
};

export const MaskedFormField = ({ ...fieldProps }: MaskedFormFieldProps) => {
  const { showErrorsInline, ...rest } = fieldProps;

  return (
    <Field {...rest}>
      {({
        field: { onBlur, ...field },
        form: { submitCount, touched, errors, handleBlur, setFieldTouched },
        meta,
        ...props
      }) => {
        return React.createElement(NumberFormat, {
          ...rest,
          ...field,
          ...props,
          ...((submitCount >= 1 || getIn(touched, field.name)) &&
          getIn(errors, field.name)
            ? {
                error:
                  showErrorsInline === false
                    ? true
                    : {
                        content: getIn(errors, field.name),
                      },
              }
            : {}),
          customInput: Form.Input,
          onInput: () => setFieldTouched(field.name, true, true),
          onBlur: handleBlur,
          mask: "_",
        });
      }}
    </Field>
  );
};

interface FormFieldProps {
  name: string;
  component?: any;
  showErrorsInline?: boolean;
  value?: any;
  [x: string]: any;
}

export const FormField = ({
  component = Form.Input,
  customOnChange = () => {},
  ...fieldProps
}: FormFieldProps) => {
  const { showErrorsInline, ...rest } = fieldProps;

  return (
    <Field {...rest}>
      {({
        field: { value, onBlur, ...field },
        form: { setFieldValue, submitCount, touched, errors, handleBlur },
        meta,
        ...props
      }) => {
        return React.createElement(component, {
          ...rest,
          ...field,
          ...props,
          ...(component === Form.Radio || component === Form.Checkbox
            ? {
                checked:
                  component === Form.Radio ? fieldProps.value === value : value,
              }
            : {
                value: value || "",
              }),

          ...((submitCount >= 1 || getIn(touched, field.name)) &&
          getIn(errors, field.name)
            ? {
                error:
                  showErrorsInline === false
                    ? true
                    : {
                        content: formatErrorPath(getIn(errors, field.name)),
                      },
              }
            : {}),
          onChange: (e, { value: newValue, checked }) => {
            customOnChange(e, newValue);
            return setFieldValue(fieldProps.name, newValue || checked);
          },
          onBlur: handleBlur,
        });
      }}
    </Field>
  );
};
