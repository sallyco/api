import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchAccountById } from "../../slices/accountsSlice";
import { IPlaidAccount } from "../../api/accountsApi";
import { Button, Divider, Header, Icon, Modal } from "semantic-ui-react";
import { Field } from "formik";
import { TenantContext } from "../../contexts";
import { useParams } from "react-router-dom";
import { fetchSubscriptionById } from "../../slices/subscriptionsSlice";
import { removeBankingAccount } from "../../slices/profilesSlice";

interface PlaidAccountProps {
  accountId: string;
  selectedAccount: {
    primaryAccountId: string;
    subAccountId: string;
  };
  onSelectAccount: (selectedAccount: {
    primaryAccountId: string;
    subAccountId: string;
  }) => void;
  showRelinkButton?: boolean;
  onDelete?: ((acc: any, flag: boolean) => void) | null;
}

const PlaidAccountListItem: React.FC<PlaidAccountProps> = ({
  accountId,
  selectedAccount,
  onSelectAccount,
  showRelinkButton,
  onDelete = null,
}) => {
  const dispatch = useDispatch();
  const tenantContext = useContext(TenantContext);
  const [showDelete, setShowDelete] = useState(false);
  const [selectedBankingAccount, setSelectedBankingAccount] = useState(null);

  const account: Partial<IPlaidAccount> = useSelector(
    (state: RootState) => state.accounts.accountsById[accountId]
  );

  const isSelected = (id) => {
    return (
      account.id === selectedAccount.primaryAccountId &&
      id === selectedAccount.subAccountId
    );
  };

  useEffect(() => {
    if (!account || account.id !== accountId) {
      dispatch(fetchAccountById(accountId));
    }
  }, [dispatch, account, accountId]);

  const deleteHandler = () => {
    setShowDelete(false);
    let canDeleteAccount = false;
    if (
      account.plaidData.accounts.filter(
        (acc) => acc.id !== selectedBankingAccount.id
      ).length === 0
    ) {
      canDeleteAccount = true;
    }
    onDelete(selectedBankingAccount, canDeleteAccount);
  };

  return (
    <>
      {account &&
        account.plaidData &&
        account.plaidData.accounts.map((BankingAccount) => (
          <div
            key={BankingAccount.id}
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Button
              type={"button"}
              fluid
              style={{
                marginBottom: ".8em",
                fontWeight: isSelected(BankingAccount.id) ? "bold" : "normal",
                ...(isSelected(BankingAccount.id)
                  ? { borderColor: "white" }
                  : {}),
              }}
              key={BankingAccount.id}
              basic
              inverted
              icon
              labelPosition={"left"}
              onClick={() =>
                onSelectAccount({
                  primaryAccountId: account.id,
                  subAccountId: BankingAccount.id,
                })
              }
            >
              <Icon
                size={"large"}
                style={{
                  color: isSelected(BankingAccount.id)
                    ? tenantContext.assets.colors.secondaryColor
                    : "white",
                }}
                name={
                  showRelinkButton
                    ? "broken chain"
                    : isSelected(BankingAccount.id)
                    ? "check circle"
                    : "circle"
                }
              />
              {account.plaidData.institution.name} ending in{" "}
              {BankingAccount.mask}
            </Button>
            <Icon
              size={"large"}
              style={{
                color: "red",
                marginBottom: ".8em",
                cursor: "pointer",
              }}
              name={"trash"}
              onClick={() => {
                setSelectedBankingAccount(BankingAccount);
                setShowDelete(true);
              }}
            />
            {showDelete && (
              <Modal
                open={showDelete}
                onClose={() => setShowDelete(false)}
                closeIcon
              >
                <Modal.Content>
                  <Header as="h3" icon textAlign="center">
                    <Icon name="exclamation circle" color="orange" />
                    Are you sure you want to remove selected account?
                  </Header>
                  <Divider hidden />
                  <Button
                    secondary
                    fluid
                    onClick={deleteHandler}
                    content="Remove Account"
                  />
                </Modal.Content>
              </Modal>
            )}
          </div>
        ))}
    </>
  );
};
export default PlaidAccountListItem;
