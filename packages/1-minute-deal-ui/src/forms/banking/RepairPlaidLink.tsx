import React, { useCallback } from "react";
import { usePlaidLink } from "react-plaid-link";
import { useHistory } from "react-router-dom";
import { logPlaidLinkError } from "../../plaid/plaidLogging";
import { Subscription } from "../../api/subscriptionsApi";

interface RepairPlaidLinkProps {
  linkToken: string;
  selectedBankAccount: any;
  subscription: Subscription;
  onRepairSuccess: (account) => void | Promise<void>;
  action: "INVALID_PUBLIC_TOKEN" | "ITEM_LOGIN_REQUIRED";
}

const RepairPlaidLink: React.FC<RepairPlaidLinkProps> = (props) => {
  let history = useHistory();

  const onSuccess = useCallback(async (token, metadata) => {
    props.onRepairSuccess({ token, metadata });
  }, []);

  const onExit = useCallback((error, metaData) => {
    logPlaidLinkError(error, metaData);
    history.push(
      `/subscriptions/${
        props.subscription.id
      }/bank_account?error=true&errorType=${error?.error_code ?? ""}`
    );
  }, []);

  const config = {
    token: props.linkToken,
    onSuccess: onSuccess,
    onExit: onExit,
  };

  const { open, ready, error, exit } = usePlaidLink(config);

  if (props.linkToken) {
    open();
  }

  return <></>;
};

export default RepairPlaidLink;
