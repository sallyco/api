import React, { useContext, useEffect, useState } from "react";
import { Button, Divider, Header } from "semantic-ui-react";
import { usePlaidLink } from "react-plaid-link";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import {
  removeAccountById,
  fetchPlaidAccountsList,
  removeSubAccountById,
} from "../../slices/accountsSlice";
import PlaidAccountListItem from "./PlaidAccountListItem";
import { Field, FormikProps, useFormikContext } from "formik";
import { TenantContext } from "../../contexts";
import { fetchSubscriptionById } from "../../slices/subscriptionsSlice";
import {
  fetchProfileById,
  removeBankingAccount,
} from "../../slices/profilesSlice";
import { useParams } from "react-router-dom";

interface PlaidProps {
  linkToken: string;
  success: (token, metadata) => void | Promise<void>;
  exit: (error, metaData) => void;
  autoSkip?: boolean;
  relinkAccount?: string;
}
interface SelectedAccount {
  primaryAccountId: undefined | string;
  subAccountId: undefined | string;
}

const PlaidAccounts: React.FC<PlaidProps> = (props) => {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const {
    isLoading: accountsLoading,
    error: accountsError,
    accounts: accountsList,
  } = useSelector((state: RootState) => state.accounts);
  const [selectedAccountId, setSelectedAccountId] = useState<string>();
  const selectedAccount = useSelector(
    (state: RootState) => state.accounts.accountsById[selectedAccountId]
  );
  const formikContext = useFormikContext<SelectedAccount>();

  useEffect(() => {
    dispatch(fetchPlaidAccountsList());
  }, []);

  useEffect(() => {
    if (props.autoSkip && accountsList?.length && accountsList.length === 1) {
      setSelectedAccountId(accountsList[0]);
    }
  }, [accountsList]);

  useEffect(() => {
    if (
      props.autoSkip &&
      selectedAccount &&
      selectedAccount.plaidData.accounts?.length === 1
    ) {
      formikContext.setValues(
        {
          primaryAccountId: selectedAccount.id,
          subAccountId: selectedAccount.plaidData.accounts[0].id,
        },
        true
      );
      void formikContext.submitForm();
    }
  }, [selectedAccount]);

  const config = {
    token: props.linkToken,
    onSuccess: props.success,
    onExit: props.exit,
  };
  // TODO: take advantage of error and exit they provide?
  const { open, ready, error, exit } = usePlaidLink(config);

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [subscription, subscriptionId]);

  const onDelete = async (
    accountId,
    selectedBankingAccount,
    canDeleteAccount
  ) => {
    await dispatch(
      removeBankingAccount(
        subscription.profileId,
        accountId,
        selectedBankingAccount.id
      )
    );
    await dispatch(removeSubAccountById(accountId, selectedBankingAccount.id));
    if (canDeleteAccount) {
      await dispatch(removeAccountById(accountId));
    }
    await dispatch(fetchPlaidAccountsList());
    await dispatch(fetchProfileById(subscription.profileId));
  };

  return (
    <>
      {!accountsLoading && !accountsError && accountsList && (
        <>
          <Header inverted textAlign={"center"}>
            <Header.Subheader inverted={"true"}>
              {accountsList.length > 0
                ? "Select an account, add a new one or remove an existing account"
                : "You haven't set up any funding accounts"}
            </Header.Subheader>
          </Header>
          <div>
            <Header inverted textAlign={"center"}>
              {accountsList.length > 0 && "Your Plaid Accounts"}
            </Header>
            {accountsList.map((accountId) => (
              <PlaidAccountListItem
                key={accountId}
                accountId={accountId}
                selectedAccount={formikContext.values}
                onSelectAccount={async (data) => {
                  setSelectedAccountId(accountId);
                  formikContext.setValues(data, true);
                }}
                showRelinkButton={props.relinkAccount === accountId}
                onDelete={(selectedBankingAccount, canDeleteAccount) => {
                  onDelete(accountId, selectedBankingAccount, canDeleteAccount);
                }}
              />
            ))}
          </div>
          <Divider hidden />
          <Button
            inverted
            fluid
            onClick={() => {
              open();
            }}
          >
            + Add a Bank Account
          </Button>
        </>
      )}
    </>
  );
};
export default PlaidAccounts;
