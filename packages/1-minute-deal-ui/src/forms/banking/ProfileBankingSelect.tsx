import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState } from "../../rootReducer";
import {
  fetchSubscriptionById,
  updateSubscriptionById,
} from "../../slices/subscriptionsSlice";
import {
  fetchProfileById,
  insertBankingAccount,
} from "../../slices/profilesSlice";
import { getProfileName } from "../../api/profileApiWrapper";
import { Button, Card, Divider, Grid, Header, Icon } from "semantic-ui-react";
import { TenantContext } from "../../contexts";
import SubformPlaidAccounts from "../subforms/SubformPlaidAccounts";
import { Formik } from "formik";
import { ProfileBankAccount } from "../../api/profilesApi";

enum ViewStateOptions {
  SELECT_PROFILE_BANK,
  LINK_ACCOUNT_TO_PROFILE,
}

export default function ProfileBankingSelect({
  onSelect,
}: {
  onSelect: (data) => void;
}) {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const [autoSkip, setAutoSkip] = useState(true);
  const [relinkAccount, setRelinkAccount] = useState(null);
  const [viewState, setViewState] = useState<ViewStateOptions>(
    ViewStateOptions.SELECT_PROFILE_BANK
  );

  const {
    isLoading: subscriptionsLoading,
    isSubmitting: subscriptionsSubmitting,
  } = useSelector((state: RootState) => state.subscriptions);
  const { isLoading: profilesLoading, isSubmitting: profilesSubmitting } =
    useSelector((state: RootState) => state.profiles);
  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );
  const profile = useSelector(
    (state: RootState) => state.profiles.profilesById[subscription?.profileId]
  );
  const tenantContext = useContext(TenantContext);
  const [selectedBankAccount, setSelectedBankAccount] =
    useState<ProfileBankAccount | null>(null);

  const isSelected = (account) => {
    return selectedBankAccount?.counterpartyId === account.counterpartyId;
  };

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !profile) {
      dispatch(fetchProfileById(subscription.profileId));
    }
  }, [subscription, profile]);

  useEffect(() => {
    if (!profile?.bankingUser?.accounts?.length) {
      setViewState(ViewStateOptions.LINK_ACCOUNT_TO_PROFILE);
    }
    if (autoSkip && profile?.bankingUser?.accounts?.length === 1) {
      setSelectedBankAccount(profile?.bankingUser?.accounts[0]);
    }
  }, [profile]);

  return (
    <>
      {!subscriptionsLoading && !profilesLoading && profile && (
        <>
          {/* Holding until Onfido implementation {*/}
          {/*  !profile.bankingUser &&*/}
          {/*  <ProfileBankingUserForm/>*/}
          {/*}*/}
          <Grid container centered inverted>
            <Grid.Column width={8} textAlign={"center"}>
              <Grid.Row>
                <Header inverted textAlign={"center"}>
                  Profile Bank Accounts
                  <Divider hidden />
                  <Header>
                    <Header.Subheader>
                      {profile &&
                        (!profile?.bankingUser?.accounts ||
                          profile.bankingUser.accounts.length < 1) &&
                        "You don't have any bank accounts linked to this profile."}
                    </Header.Subheader>
                  </Header>
                </Header>
                {viewState === ViewStateOptions.LINK_ACCOUNT_TO_PROFILE && (
                  <>
                    <Divider hidden />
                    {viewState === ViewStateOptions.LINK_ACCOUNT_TO_PROFILE && (
                      <Formik
                        initialValues={{
                          primaryAccountId: undefined,
                          subAccountId: undefined,
                        }}
                        onSubmit={async ({
                          primaryAccountId,
                          subAccountId,
                        }) => {
                          // Convert Plaid account to Node
                          try {
                            const result = await dispatch(
                              insertBankingAccount(
                                profile.id,
                                primaryAccountId,
                                subAccountId
                              )
                            );
                            if (result) {
                              await dispatch(fetchProfileById(profile.id));
                              setViewState(
                                ViewStateOptions.SELECT_PROFILE_BANK
                              );
                            }
                          } catch (e) {
                            // Error with Plaid link
                            setAutoSkip(false);
                            setRelinkAccount(primaryAccountId);
                          }
                        }}
                      >
                        {(formikProps) => (
                          <>
                            <SubformPlaidAccounts
                              autoSkip={autoSkip}
                              relinkAccount={relinkAccount}
                            />
                            <Divider />
                            <div>
                              <Button
                                type={"submit"}
                                fluid
                                onClick={(e) => formikProps.submitForm()}
                                loading={profilesSubmitting}
                                secondary
                                disabled={
                                  !formikProps.dirty || !formikProps.isValid
                                }
                              >
                                {`Link to "${getProfileName(profile)}" Profile`}
                              </Button>
                              <Divider hidden />
                              <Button
                                type={"button"}
                                onClick={(e) => {
                                  setViewState(
                                    ViewStateOptions.SELECT_PROFILE_BANK
                                  );
                                }}
                                basic
                                fluid
                                secondary
                              >
                                Back to Profile Accounts
                              </Button>
                            </div>
                          </>
                        )}
                      </Formik>
                    )}
                  </>
                )}
                {viewState === ViewStateOptions.SELECT_PROFILE_BANK &&
                  profile?.bankingUser?.accounts &&
                  profile.bankingUser.accounts.map((account, index) => (
                    <div key={"profile-bank-" + index}>
                      <Button
                        type={"button"}
                        fluid
                        style={{
                          marginBottom: ".8em",
                          fontWeight: isSelected(account) ? "bold" : "normal",
                          ...(isSelected(account)
                            ? { borderColor: "white" }
                            : {}),
                        }}
                        basic
                        inverted
                        icon
                        labelPosition={"left"}
                        onClick={() => setSelectedBankAccount(account)}
                      >
                        <Icon
                          size={"large"}
                          style={{
                            color: isSelected(account)
                              ? tenantContext.assets.colors.secondaryColor
                              : "white",
                          }}
                          name={isSelected(account) ? "check circle" : "circle"}
                        />
                        {account.accountName}
                      </Button>
                    </div>
                  ))}
              </Grid.Row>
              {viewState === ViewStateOptions.SELECT_PROFILE_BANK && (
                <>
                  <Divider hidden />
                  <Button
                    size={"medium"}
                    onClick={() => {
                      setAutoSkip(false);
                      setViewState(ViewStateOptions.LINK_ACCOUNT_TO_PROFILE);
                    }}
                    inverted
                    fluid
                    primary
                  >
                    {`+ Manage bank accounts in this profile`}
                  </Button>
                  <Divider />
                  <Button
                    size={"medium"}
                    fluid
                    onClick={() => {
                      onSelect(selectedBankAccount);
                    }}
                    loading={subscriptionsSubmitting}
                    disabled={!selectedBankAccount}
                    secondary
                  >
                    Continue
                  </Button>
                </>
              )}
            </Grid.Column>
          </Grid>
        </>
      )}
    </>
  );
}
