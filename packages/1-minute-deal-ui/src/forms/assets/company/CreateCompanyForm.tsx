import React from "react";
import {
  Form,
  Icon,
  List,
  Popup,
  Button,
  Divider,
  Header,
  Progress,
  Segment,
  Grid,
} from "semantic-ui-react";
import * as Yup from "yup";
import { Formik } from "formik";
import { FormField } from "../../common/FormField";
import { EntityTypes } from "../../../tools/enums";

import SubformEntityTaxId, * as SubformEntityTaxIdMeta from "../../subforms/SubformEntityTaxId";
import SubformAddress, * as SubformAddressMeta from "../../subforms/SubformAddress";
import SubformSigner, * as SubformSignerMeta from "../../subforms/SubformSigner";
//import SubformSigner2, * as SubformSigner2Meta from "../../subforms/SubformSigner2";
//import SubformBOwner, * as SubformBOwnerMeta from "../../subforms/SubformBOwner";

import Log from "../../../tools/Log";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
  onHandleSection: (number) => void;
  stepNumber: number;
}

export default function CreateCompanyForm({
  onSubmit,
  submitting = false,
  onHandleSection,
  stepNumber,
}: Props) {
  const validation = Yup.object()
    .shape({
      name: Yup.string()

        .trim()
        .min(2)
        .max(1024)
        .required(),
      typeOfEntity: Yup.string().required(),
      ...SubformEntityTaxIdMeta.validationSchema({ isRequired: true }),
      ...SubformAddressMeta.validationSchema(),
      ...SubformSignerMeta.validationSchema(),
      //  ...SubformSigner2Meta.validationSchema(),
      //  ...SubformBOwnerMeta.validationSchema(),
      certify3: Yup.boolean(),
      certify4: Yup.boolean(),
      certify5: Yup.boolean(),
      certify6: Yup.boolean(),
      certify7: Yup.boolean(),
    })
    .test("CertificationTest", null, (obj) => {
      if (obj.certify7) {
        return new Yup.ValidationError(
          "If none of the preceding statments are true, you cannot invest in this deal.  Please contact the deal organizer.",
          null,
          "certify7"
        );
      }

      if (
        (obj.certify3 || obj.certify4 || obj.certify5 || obj.certify6) &&
        !obj.certify7
      ) {
        return true; // everything is fine
      }

      return new Yup.ValidationError(
        "Please select one of the applicable statement.",
        null,
        "certify3"
      );
    });

  return (
    <>
      <Formik
        initialValues={{
          name: "",
          typeOfEntity: "",
          registrationType: "ENTITY",
          isUSBased: "Yes",
          isSingleMemberLLC: "Yes",

          ...SubformEntityTaxIdMeta.defaultValues,
          ...SubformAddressMeta.defaultValues,
          ...SubformSignerMeta.defaultValues,
          //  ...SubformSigner2Meta.defaultValues,
          //  ...SubformBOwnerMeta.defaultValues,
          certify3: false,
          certify4: false,
          certify5: false,
          certify6: false,
          certify7: false,
        }}
        enableReinitialize={false}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} autoComplete="off">
            {stepNumber === 1 && (
              <>
                <Header as="h3" inverted textAlign="center">
                  Fill out the required sections to circulate your company
                  <Divider hidden />
                  <Header.Subheader>
                    Add a company overview, executive summary, financials, and a
                    pitch deck, along with any supporting documents.
                  </Header.Subheader>
                </Header>
                <Header as="h5" color="green" textAlign="center">
                  SECTION 1 to 4
                </Header>
                <Progress value={1} total={4} size="small" success />

                <Header as="h3" inverted>
                  Company Basics
                  <Header.Subheader>
                    Include your elevator pitch and some essentials about your
                    company
                  </Header.Subheader>
                </Header>
                <Divider inverted />
                <Segment
                  basic
                  inverted
                  textAlign="center"
                  className="ui no-padding"
                >
                  SECTION 1 of 4
                </Segment>
                <Progress value={1} total={4} size="small" success />
                <Header inverted>Account Basics</Header>
                <Divider />
                <Form.Group widths="equal">
                  <FormField
                    id="name"
                    name="name"
                    label="Entity Legal Name"
                    placeholder="Entity Legal Name"
                    required
                  />
                  <FormField
                    id="typeOfEntity"
                    name="typeOfEntity"
                    component={Form.Select}
                    label="Entity Type"
                    options={EntityTypes}
                    placeholder="Select a Type"
                    search
                    required
                  />
                </Form.Group>

                <label style={{ color: "white" }}>
                  Is this a single member-LLC?
                </label>
                <Form.Group>
                  <FormField
                    label="Yes"
                    component={Form.Radio}
                    name="isSingleMemberLLC"
                    value="Yes"
                  />
                  <FormField
                    label="No"
                    component={Form.Radio}
                    name="isSingleMemberLLC"
                    value="No"
                  />
                </Form.Group>
                <SubformEntityTaxId />

                <Divider hidden />
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={8}></Grid.Column>
                    <Grid.Column width={8}>
                      <Button
                        secondary
                        type="button"
                        fluid
                        size="mini"
                        disabled={
                          !props.touched.name ||
                          !(
                            (!props.errors.name && !props.errors.typeOfEntity) //&&
                            // !props.errors.entityTaxId
                          )
                        }
                        onClick={() => onHandleSection(2)}
                        content="Next Section"
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </>
            )}
            {stepNumber === 2 && (
              <>
                <Segment
                  basic
                  inverted
                  textAlign="center"
                  className="ui no-padding"
                >
                  SECTION 2 of 4
                </Segment>
                <Progress value={2} total={4} size="small" success />
                <Header as="h3" inverted>
                  Entity Address
                </Header>
                <Divider />
                <SubformAddress {...props} />
                <Form.Group widths="equal">
                  <Form.Button
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(1)}
                    content="Back"
                  />
                  <Form.Button
                    secondary
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(3)}
                    content="Next Section"
                  />
                </Form.Group>
              </>
            )}
            {stepNumber === 3 && (
              <>
                <Segment
                  basic
                  inverted
                  textAlign="center"
                  className="ui no-padding"
                >
                  SECTION 3 of 4{/*: Signatories*/}
                </Segment>
                <Progress value={3} total={4} size="small" success />
                <Header inverted>Primary Signatory (Required)</Header>
                <Divider />
                <SubformSigner {...props} />
                {/*
                {props.values.isSingleMemberLLC === "No" && (
                  <>
                    <Divider inverted />
                    <Header inverted>
                      Additional Authorized Signatory (Required)
                    </Header>
                    <SubformSigner2 {...props} />
                  </>
                )}
                */}
                <Form.Group widths="equal">
                  <Form.Button
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(2)}
                    content="Back"
                  />
                  <Form.Button
                    secondary
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(4)}
                    content="Next Section"
                  />
                </Form.Group>
              </>
            )}
            {/*
            {stepNumber === 4 && (
              <>
                <Segment
                  basic
                  inverted
                  textAlign="center"
                  className="ui no-padding"
                >
                  SECTION 4 of 5
                </Segment>
                <Progress value={4} total={5} size="small" success />
                <Header inverted textAlign="left">
                  Beneficial Owners (Required)
                </Header>
                <Header.Subheader style={{ color: "white" }}>
                  Please add individuals whose beneficial ownership of the
                  entity is greater than 20%.
                </Header.Subheader>
                <Divider hidden />
                <SubformBOwner {...props} />
                <Form.Group widths="equal">
                  <Form.Button
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(3)}
                    content="Back"
                  />
                  <Form.Button
                    secondary
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(5)}
                    content="Next Section"
                  />
                </Form.Group>
              </>
            )}
*/}
            {stepNumber === 4 && (
              <>
                <Segment
                  basic
                  inverted
                  textAlign="center"
                  className="ui no-padding"
                >
                  SECTION 4 of 4
                </Segment>
                <Progress value={4} total={4} size="small" success />
                <Header as="h3" inverted textAlign="center">
                  Investor Accreditation
                </Header>
                <Divider />
                <FormField
                  id="certify3"
                  name="certify3"
                  component={Form.Checkbox}
                  label="Investor is a bank, insurance company, investment company registered under the Company Act, a broker or dealer registered pursuant to Section 15 of the Securities Exchange Act of 1934 (the “Exchange Act”), a business development company, a Small Business Investment Company licensed by the United States Small Business Administration, a plan with total assets in excess of $5,000,000 established and maintained by a state for the benefit of its employees, or a private business development company as defined in Section 202(a)(22) of the United States Investment Advisers Act of 1940, as amended."
                />
                <FormField
                  id="certify4"
                  name="certify4"
                  component={Form.Checkbox}
                  label="Investor is an employee benefit plan and either all investment decisions are made by a bank, savings and loan association, insurance company, or registered investment advisor, or investor has total assets in excess of $5,000,000 or, if such plan is a self-directed plan, investment decisions are made solely by persons who are accredited investors."
                />
                <FormField
                  id="certify5"
                  name="certify5"
                  component={Form.Checkbox}
                  label="Investor is a corporation, partnership, limited liability company or business trust, not formed for the purpose of acquiring the Interests, or an organization described in Section 501(c)(3) of the country, in each case with total assets in excess of $5,000,000."
                />
                <FormField
                  id="certify6"
                  name="certify6"
                  component={Form.Checkbox}
                  label="Subscriber is an entity in which all of the equity owners, or a grantor or revocable trust in which all of the grantors and trustees, qualify under clause (i), (ii), (iii), (iv) or (v) above or this clause (vi)."
                />
                <FormField
                  id="certify7"
                  name="certify7"
                  component={Form.Checkbox}
                  label="Investor cannot make any of the above representations."
                />
                <Form.Group widths="equal">
                  <Form.Button
                    type="button"
                    fluid
                    size="mini"
                    onClick={() => onHandleSection(3)}
                    content="Back"
                  />
                  <Form.Button
                    secondary
                    type="submit"
                    loading={submitting}
                    fluid
                    size="mini"
                    disabled={!props.isValid}
                    content="Create Profile"
                  />
                </Form.Group>
              </>
            )}
          </Form>
        )}
      </Formik>
    </>
  );
}
