import React, { useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";

import { updateDealById } from "../slices/dealsSlice";

import * as Yup from "yup";
import Log from "../tools/Log";
import { RootState } from "../rootReducer";
import moment from "moment";

import { Icon, Image as SImage, Button, Form } from "semantic-ui-react";
import { Formik, Field } from "formik";
import { MaskedFormField, FormField } from "./common/FormField";
import SubformDate, {
  validationSchema as DateValidationSchema,
  defaultValues as DateDefaultValues,
} from "./subforms/SubformDate";

import { Deal } from "../api/dealsApi";
import { toast } from "react-toastify";
import ReactQuill from "react-quill";
import QuillField from "./common/QuillField";
import { Entity } from "../api/entitiesApi";
import { patchEntityById, updateEntityById } from "../slices/entitySlice";
import { getDigitsFromString } from "./validation/ValidationHelper";
import {
  FeatureFlag,
  useFeatureFlag,
} from "../components/featureflags/FeatureFlags";
import { Alert } from "@mui/material";
import { getTargetRaiseAmountYup } from "./validation/dealValidationHelpers";

interface Props {
  deal: Deal;
  entity: Entity;
  setShowEdit: (any) => void;
  fieldSettings?: {
    maxTargetRaiseAmount?: number;
  };
}

const INVESTOR_LABEL_TEXT = "Investors Must Be A Qualified Purchaser";
const QP_FEATURE_FLAG = "qualified_purchaser";

const EditDealForm = ({
  deal,
  entity,
  setShowEdit,
  fieldSettings = {},
}: Props) => {
  const dispatch = useDispatch();
  const [uploadImage, setUploadImage]: any = useState(undefined);
  const [removeImage, setRemoveImage] = useState(false);
  const qualifiedPurchaserFeature = useFeatureFlag(QP_FEATURE_FLAG);
  const fileInputRef: any = useRef();

  const { isSubmitting } = useSelector((state: RootState) => state.deals);

  const fileChange = (e) => {
    Log.debug("filename ", e.target.value);
    Log.debug("file ", e.target.files[0]);

    const file = e.target.files[0];
    if (!file) return;

    let reader = new FileReader();
    if (file.size <= 524288) {
      reader.onload = function (event) {
        Log.debug("event ", event);
        setUploadImage(event?.target?.result ?? undefined);
        Log.debug("uploadImage ", uploadImage);
      };
      reader.readAsDataURL(file);
      setRemoveImage(false);
    } else {
      toast.error("Files are limited to 512kb, please choose a smaller file");
    }
  };

  async function onSubmit(data) {
    let submitData = {
      id: deal.id,
      name: data.name,
      description: data.description,
      targetRaiseAmount: getDigitsFromString(data?.targetRaiseAmount),
      previouslyRaisedAmount: getDigitsFromString(data?.previouslyRaisedAmount),
      estimatedCloseDate: new Date(data.estimatedCloseDate),
      marketing: {
        tagline: data?.marketingTagline,
        logo: removeImage ? "" : uploadImage || deal?.marketing?.logo,
      },
      minInvestmentAmount: getDigitsFromString(data?.minInvestmentAmount),
      requireQualifiedPurchaser: false,
    };

    if (qualifiedPurchaserFeature) {
      submitData.requireQualifiedPurchaser = data.requireQualifiedPurchaser;
    }

    try {
      await dispatch(updateDealById(submitData));
      await dispatch(
        patchEntityById(entity.id, {
          minInvestmentAmount: getDigitsFromString(data?.minInvestmentAmount),
        })
      );
      toast.success("Deal updated");
      setShowEdit(false);
    } catch (e) {
      toast.error("Failed to update deal");
    }
    return;
  }

  const validation = Yup.object()
    .shape({
      name: Yup.string().trim().required(),
      description: Yup.string().trim(),
      marketingTagline: Yup.string().trim(),
      previouslyRaisedAmount: Yup.number()
        .transform(function (value, originalvalue) {
          return Number(originalvalue.replace(/\D+/g, ""));
        })
        .max(deal.targetRaiseAmount, "Must be less than raise amount"),
      minInvestmentAmount: Yup.number()
        .transform(function (value, originalvalue) {
          return Number(originalvalue.replace(/\D+/g, ""));
        })
        .max(deal.targetRaiseAmount, "Must be less than raise amount")
        .required(),
      ...DateValidationSchema({ isRequired: true }),
      targetRaiseAmount: getTargetRaiseAmountYup({
        max: fieldSettings?.maxTargetRaiseAmount,
      }),
    })
    .test("estimatedCloseDate", "estimatedCloseDate", (obj) => {
      let ecd = new Date(`${obj.year}-${obj.month}-${obj.day}`);
      if (moment(ecd).isAfter(new Date())) {
        return true;
      } else {
        return new Yup.ValidationError(
          "Estimated Close Date must be after Today",
          null,
          "year"
        );
      }
    });

  return (
    <Formik
      initialValues={{
        name: deal?.name,
        description: deal?.description,
        marketingTagline: deal?.marketing?.tagline,
        targetRaiseAmount:
          deal?.targetRaiseAmount > 0 ? deal?.targetRaiseAmount.toString() : "",
        previouslyRaisedAmount:
          deal?.previouslyRaisedAmount > 0
            ? deal?.previouslyRaisedAmount.toString()
            : "",
        minInvestmentAmount:
          entity?.minInvestmentAmount > 0
            ? entity?.minInvestmentAmount.toString()
            : "",
        // For Date Form
        ...DateDefaultValues,
        estimatedCloseDate: moment
          .utc(deal?.estimatedCloseDate)
          .format("YYYY-MM-DD"),
        month: moment.utc(deal?.estimatedCloseDate).format("MM"),
        day: moment.utc(deal?.estimatedCloseDate).format("DD"),
        year: moment.utc(deal?.estimatedCloseDate).format("YYYY"),
        requireQualifiedPurchaser: deal?.requireQualifiedPurchaser ?? false,
      }}
      enableReinitialize={false}
      validationSchema={validation}
      onSubmit={onSubmit}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} className="dark-labels">
          <FormField
            name="name"
            label="Name Your Fund / Deal"
            placeholder="Capital Co Seed"
            required
          />
          <FeatureFlag name={QP_FEATURE_FLAG}>
            <FormField
              inline
              name={"requireQualifiedPurchaser"}
              id={"requireQualifiedPurchaser"}
              label={INVESTOR_LABEL_TEXT}
              component={Form.Checkbox}
            />
          </FeatureFlag>
          <QuillField
            name={"description"}
            placeholder={"Provide a description of this deal"}
            label={"Deal Description"}
          />
          <label onClick={() => fileInputRef.current.click()}>
            Upload Deal Logo (filesize limit 512kb)
          </label>
          <Form.Group widths="equal">
            <Form.Field width={8}>
              {!removeImage && (uploadImage || deal?.marketing?.logo) ? (
                <SImage
                  src={uploadImage || deal?.marketing?.logo}
                  size="small"
                />
              ) : (
                <Icon.Group size="huge">
                  <Icon className={"primaryColor"} name="image outline" />
                  <Icon
                    corner
                    className={"primaryColor"}
                    name="arrow alternate circle up outline"
                  />
                </Icon.Group>
              )}
            </Form.Field>
            <Form.Button
              fluid
              width={4}
              secondary
              onClick={() => fileInputRef.current.click()}
              content="Upload Image"
              type="button"
            />
            <input
              ref={fileInputRef}
              type="file"
              hidden
              onChange={fileChange}
              accept="image/*"
            />
            <Form.Button
              fluid
              width={4}
              negative
              secondary
              disabled={!uploadImage && !deal?.marketing?.logo}
              onClick={() => setRemoveImage(true)}
              content="Remove Image"
              type="button"
            />
          </Form.Group>

          <FormField
            name="marketingTagline"
            label="Deal Tagline"
            placeholder="Invest in this deal now!"
          />
          <MaskedFormField
            id="targetRaiseAmount"
            name="targetRaiseAmount"
            thousandSeparator={true}
            allowNegative={false}
            prefix={"$"}
            decimalScale={0}
            type="tel"
            label="Raise Amount"
            placeholder="$500,000"
            required
          />
          <Form.Group widths="equal">
            <MaskedFormField
              id="previouslyRaisedAmount"
              name="previouslyRaisedAmount"
              thousandSeparator={true}
              allowNegative={false}
              prefix={"$"}
              decimalScale={0}
              type="tel"
              label="Amount Already Raised (off platform)"
              placeholder="$300,000"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <MaskedFormField
              id="minInvestmentAmount"
              name="minInvestmentAmount"
              thousandSeparator={true}
              allowNegative={false}
              prefix={"$"}
              decimalScale={0}
              type="tel"
              label="Minimum Investment Amount"
              placeholder="$5,000"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <SubformDate
              id={"estimatedCloseDate"}
              label={"Estimated Close Date"}
              name={"estimatedCloseDate"}
            />
          </Form.Group>
          {props.values.requireQualifiedPurchaser && (
            <Alert severity="info">{INVESTOR_LABEL_TEXT}</Alert>
          )}
          <Button
            secondary
            fluid
            content="Save Changes"
            type="submit"
            loading={isSubmitting}
          />
        </Form>
      )}
    </Formik>
  );
};

export default EditDealForm;
