import React from "react";
import PropTypes from "prop-types";

import * as Yup from "yup";

import { Form, Button, Header } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "./common/FormField";

interface Props {
  title: string;
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function ForgotCredentialsForm({
  title = "Forgot Password",
  onSubmit,
  submitting = false,
}: Props) {
  const validation = Yup.object().shape({
    email: Yup.string()

      .trim()
      .email()
      .required(),
  });

  return (
    <>
      <Header as="h1" inverted textAlign="center">
        {title}
      </Header>
      <Formik
        initialValues={{}}
        validationSchema={validation}
        onSubmit={onSubmit}
      >
        {(props) => (
          <Form
            onSubmit={props.handleSubmit}
            data-testid={"forgot-credentials-form"}
          >
            <FormField
              name="email"
              component={Form.Input}
              label="Email"
              placeholder="Email Address"
              required
            />
            <Button secondary type="submit" loading={submitting} fluid>
              Send Link
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
}
