import React, { useState } from "react";
import * as Yup from "yup";
import { FormField } from "../common/FormField";
import { Form } from "semantic-ui-react";

export const validationSchema = () => {
  return {
    password: Yup.string()

      .required()
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{12,}$/,
        "Minimum twelve characters, at least one letter, one number and one special character @$!%*#?&"
      ),
  };
};

export const defaultValues = {};

interface Props {
  label?: string;
}
export default function SubformPassword({ ...props }) {
  const [showPassword, setShowPassword] = useState(false);
  return (
    <FormField
      id="password"
      name="password"
      label={props?.label ?? "Password"}
      type={showPassword ? "text" : "password"}
      placeholder="Password"
      autoComplete="off"
      action={{
        icon: showPassword ? "eye slash" : "eye",
        onClick: () => setShowPassword(!showPassword),
        type: "button",
      }}
      {...props}
    />
  );
}
