import React, {
  Children,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import Log from "../../tools/Log";
import { uploadFileToServer } from "../../slices/filesSlice";
import { useDispatch } from "react-redux";
import filesize from "filesize";
import { Icon, Image, Loader, Message, Card } from "semantic-ui-react";
import { Document, Page } from "react-pdf";

export enum FileSizes {
  MB = 1048576,
  GB = 1073741824,
}

interface SubformFileUploadProps {
  onSuccess: (fileIds: string[]) => void | Promise<void>;
  onError: (errors: string[]) => void | Promise<void>;
  children: (props: {
    triggerFileUpload: () => void;
    uploading: boolean;
    errors: string[];
    error: boolean;
    clearUpload: () => void;
    filePreviews?: FilePreview[];
  }) => React.ReactNode;
  acceptMime: string;
  allowMultiple: boolean;
  maxSize?: number;
  convertToDataUri?: boolean;
  dataUriOnly?: boolean;
}

interface FilePreview {
  fileType: string;
  fileName: string;
  dataUri: string;
}

const FileUploadPreviewsContext = React.createContext<FilePreview[]>([]);
export const SubformFileUpload = function ({
  onSuccess,
  children,
  acceptMime = "*",
  allowMultiple = false,
  maxSize,
  onError,
  convertToDataUri,
  dataUriOnly,
}: Partial<React.PropsWithChildren<SubformFileUploadProps>>) {
  const dispatch = useDispatch();
  const [uploading, setUploading] = useState(false);
  const [errors, setErrors] = useState([]);
  const [error, setError] = useState(false);
  const fileInputRef = useRef(null);
  const [filePreviews, setFilePreviews] = useState<FilePreview[]>([]);

  async function fileChange(e) {
    setErrors([]);
    setError(false);
    setFilePreviews([]);
    setUploading(true);
    Log.debug("target ", e.target);
    Log.debug("filename ", e.target.value);
    Log.debug("file ", e.target.files);

    const data = new FormData();
    let errorsArray = [];
    let promises = [];
    for (const file of e.target.files) {
      // eslint-disable-next-line no-control-regex
      if (/[^\u0000-\u00ff]/.test(file.name)) {
        errorsArray.push(
          `File name should not contain Unicode characters. '${file.name}' `
        );
        setErrors(errorsArray);
      }
      if (maxSize && file.size > maxSize) {
        errorsArray.push(
          `File '${file.name}' is too large. Max Allowed size is ${filesize(
            maxSize
          )}`
        );
        setErrors(errorsArray);
      }
      data.append("files", file);
      if (convertToDataUri || dataUriOnly) {
        promises.push(
          new Promise((resolve, reject) => {
            let reader = new FileReader();
            reader.onload = (event) => {
              const tempFilePreviews = filePreviews;
              tempFilePreviews.push({
                dataUri: String(event?.target?.result),
                fileType: file.type,
                fileName: file.name,
              });
              setFilePreviews(tempFilePreviews);
              resolve(true);
            };
            reader.onerror = reject;
            reader.readAsDataURL(file);
          })
        );
      }
    }
    if (promises) {
      await Promise.all(promises).catch((e) => {
        Log.error("File Upload Error", e);
        errorsArray.push(e.message());
        setErrors(errorsArray);
      });
    }
    if (errorsArray.length > 0) {
      setError(true);
      setUploading(false);
      if (typeof onError === "function") {
        onError(errorsArray);
      }
      return;
    }
    Log.debug("data ", data);
    let fileIds = [];
    if (!dataUriOnly) {
      const files: any = await dispatch(uploadFileToServer(data));
      Log.debug("files ", files);
      fileIds = files.map((file) => file.id);
    }
    Log.debug("fileIds ", fileIds);
    setUploading(false);
    if (typeof onSuccess === "function") {
      dataUriOnly
        ? onSuccess(filePreviews.map((file) => file.dataUri))
        : onSuccess(fileIds);
    }
    e.target.value = "";
  }

  const triggerUpload = () => {
    fileInputRef.current.click();
  };
  return (
    <>
      <FileUploadPreviewsContext.Provider value={filePreviews}>
        {children({
          triggerFileUpload: triggerUpload,
          uploading: uploading,
          errors: errors,
          error: error,
          filePreviews: filePreviews,
          clearUpload: () => {
            setFilePreviews([]);
            fileInputRef.current.value = "";
          },
        })}
      </FileUploadPreviewsContext.Provider>
      <input
        ref={fileInputRef}
        type="file"
        hidden
        onChange={fileChange}
        accept={acceptMime}
        multiple={allowMultiple}
        data-testid="file-upload"
      />
    </>
  );
};
export const UploadPreview = ({
  placeholder,
  inverted = false,
  ...props
}: {
  placeholder?: React.ReactElement | React.FC | boolean;
  inverted?: boolean;
}) => {
  const previews = useContext(FileUploadPreviewsContext);
  const imageMIMEs = [
    "image/jpg",
    "image/png",
    "image/svg",
    "image/jpeg",
    "image/gif",
  ];
  const pdfMIMEs = ["application/pdf"];
  return (
    <>
      {previews &&
        previews.map((filePreview, index) => {
          return (
            <div key={`key-${index}-preview`}>
              {imageMIMEs.includes(filePreview.fileType) && (
                <Image size={"tiny"} src={filePreview.dataUri} inline />
              )}
              {pdfMIMEs.includes(filePreview.fileType) && (
                <Document
                  file={filePreview.dataUri}
                  loading={
                    <Loader active inverted inline content="Loading File..." />
                  }
                  error={<Message negative>Failed to load preview</Message>}
                >
                  <Page pageNumber={1} width={150} />
                </Document>
              )}
              {![...pdfMIMEs, ...imageMIMEs].includes(filePreview.fileType) && (
                <Icon name={"file"} size={"large"} inverted={inverted} />
              )}
              {filePreview.fileName}
            </div>
          );
        })}
      {previews.length < 1 &&
        placeholder &&
        (typeof placeholder === "boolean" ? (
          <Icon name={"file"} size={"large"} inverted={inverted} />
        ) : (
          placeholder
        ))}
    </>
  );
};
