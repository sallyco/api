import React from "react";
import * as Yup from "yup";
import { Form, Header } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import {
  IdentificationTypes,
  ForeignPersonTaxIdTypes,
} from "../../tools/enums";
import { Alert } from "@mui/material";

export const validationSchema = ({ isRequired }) => {
  const validation = {
    isUSBased: Yup.string(),
    taxId: Yup.string().when(["isUSBased", "hasTaxId", "taxIdType"], {
      is: (isUsBased, hasTaxId, taxIdType) =>
        isUsBased === "Yes" || (hasTaxId === "Yes" && taxIdType === "itin"),
      then: Yup.string().length(11),
      otherwise: Yup.string(),
    }),
  };
  if (isRequired) {
    validation.isUSBased = validation.isUSBased.required();
    // validation.taxId = validation.taxId.required();
  }
  return validation;
};

export const defaultValues = {
  isUSBased: "",
  taxId: "",
  hasTaxId: "",
  hasId: "",
  taxIdType: "ssn",
};

export default function SubformIndividualTaxId({ values, ...props }) {
  return (
    <>
      {values.country === "United States of America" && (
        <Form.Group widths="equal">
          <Form.Field>
            <Header as="h3" inverted>
              I am a US Citizen...
            </Header>
            <Form.Group>
              <FormField
                label="Yes"
                size="large"
                component={Form.Radio}
                name="isUSBased"
                value="Yes"
              />
              <FormField
                label="No"
                component={Form.Radio}
                name="isUSBased"
                value="No"
              />
            </Form.Group>
          </Form.Field>
        </Form.Group>
      )}

      {(values.country !== "United States of America" ||
        values.isUSBased === "No") && (
        <Form.Group widths="equal">
          <Form.Field>
            <Header as="h3" inverted>
              (I have one of these tax identification numbers..
              <Header.Subheader>
                &nbsp;Individual Taxpayer Identification Number (ITIN)
              </Header.Subheader>
              <Header.Subheader>
                &nbsp;Foreign Tax Identification
              </Header.Subheader>
              <Header.Subheader>
                &nbsp;Social Security Number (SSN)
              </Header.Subheader>
            </Header>
            <Form.Group>
              <FormField
                label="Yes"
                size="large"
                component={Form.Radio}
                name="hasTaxId"
                value="Yes"
              />
              <FormField
                label="No"
                component={Form.Radio}
                name="hasTaxId"
                value="No"
              />
            </Form.Group>
          </Form.Field>
        </Form.Group>
      )}

      {values.isUSBased !== "Yes" && values.hasTaxId === "No" && (
        <Form.Group widths="equal">
          {/*<Form.Field>*/}
          {/*  <Header as="h3" inverted>*/}
          {/*    I have one of these identification documents..*/}
          {/*    <Header.Subheader>*/}
          {/*      &nbsp;Identity Card (Passport)*/}
          {/*    </Header.Subheader>*/}
          {/*    <Header.Subheader>&nbsp;Driving License</Header.Subheader>*/}
          {/*    <Header.Subheader>&nbsp;National Insurance</Header.Subheader>*/}
          {/*  </Header>*/}
          {/*  <Form.Group>*/}
          {/*    <FormField*/}
          {/*      label="Yes"*/}
          {/*      size="large"*/}
          {/*      component={Form.Radio}*/}
          {/*      name="hasId"*/}
          {/*      value="Yes"*/}
          {/*    />*/}
          {/*    <FormField*/}
          {/*      label="No"*/}
          {/*      component={Form.Radio}*/}
          {/*      name="hasId"*/}
          {/*      value="No"*/}
          {/*    />*/}
          {/*  </Form.Group>*/}
          {/*</Form.Field>*/}
          <Alert severity={"warning"}>
            You must have a valid Tax Identification Number to Proceed
          </Alert>
        </Form.Group>
      )}

      {/* If the user has a tax ID, show the type selection */}
      {values.isUSBased !== "Yes" && values.hasTaxId === "Yes" && (
        <Form.Group widths="equal">
          <FormField
            id="taxIdType"
            name="taxIdType"
            component={Form.Select}
            label="Tax ID Type"
            options={ForeignPersonTaxIdTypes}
            placeholder="Select a Type"
          />
        </Form.Group>
      )}

      {values.hasTaxId === "No" && values.hasId === "Yes" && (
        <Form.Group widths="equal">
          <FormField
            id="identityIdType"
            name="identityIdType"
            component={Form.Select}
            label="Identification Type"
            options={IdentificationTypes}
            placeholder="Select a Type"
          />
        </Form.Group>
      )}

      {/* Should we Show the SSN field? */}
      {(values.isUSBased === "Yes" || values.hasTaxId === "Yes") &&
        values.taxIdType === "ssn" && (
          <Form.Group widths="equal">
            <MaskedFormField
              id="taxId"
              name="taxId"
              width={12}
              data-testid={"taxId"}
              autoComplete="off"
              label={
                <LabelwithTooltip
                  labelText="Social Security Number (SSN)"
                  popupText="Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                  iconName="question circle"
                  iconColor="green"
                />
              }
              type={"text"}
              format={"###-##-####"}
              placeholder={"123-54-6789"}
            />
          </Form.Group>
        )}

      {values.isUSBased !== "Yes" &&
        values.taxIdType !== "ssn" &&
        values.hasTaxId === "Yes" && (
          <Form.Group widths="equal">
            {values.taxIdType === "itin" ? (
              <MaskedFormField
                id="taxId"
                name="taxId"
                width={12}
                data-testid={"taxId"}
                autoComplete="off"
                label={
                  <LabelwithTooltip
                    labelText="Individual Taxpayer Identification Number (ITIN)"
                    popupText="An ITIN is issued by the IRS to persons (both residents and nonresidents of the US) who are not eligible for an SSN, but still need to file taxes and reports with the IRS."
                    iconName="question circle"
                    iconColor="green"
                  />
                }
                type={"text"}
                format={"9##-##-####"}
                placeholder={"987-65-4321"}
              />
            ) : (
              <FormField
                id="taxId"
                name="taxId"
                width={12}
                label="Foreign Tax ID"
              />
            )}
          </Form.Group>
        )}

      {values.isUSBased !== "Yes" &&
        values.hasTaxId !== "Yes" &&
        values.hasId === "Yes" && (
          <Form.Group widths="equal">
            <FormField
              id="taxId"
              name="taxId"
              width={12}
              label="Identification Document Value"
            />
          </Form.Group>
        )}
    </>
  );
}
