import React, { useState } from "react";
import * as Yup from "yup";
import { Divider, Form } from "semantic-ui-react";
import { FormField, MaskedFormField } from "../common/FormField";
import numeral from "numeral";

export const DEFAULT_FEE_PERCENT = 4.5;

const FeeTypes = {
  Percent: "percent",
  Flat: "flat",
};

export const MANAGEMENT_FEE_FREQUENCY = "Annual";

function getFeeText(isRecurring) {
  return isRecurring ? "paid annually" : "paid one time at closing";
}

function fixAmountValue(managementFee) {
  /**
   * Allow decimals in percent
   * But only whole numbers in $ amount
   */
  if (managementFee?.type === FeeTypes.Percent)
    return typeof managementFee.amount === "number"
      ? managementFee.amount
      : Number(managementFee.amount.replace(/[^0-9.]+/g, ""));
  if (managementFee?.type === FeeTypes.Flat) {
    return typeof managementFee.amount === "number"
      ? managementFee.amount
      : Number(managementFee.amount.replace(/[^0-9.]+/g, ""));
  }

  return 0;
}

const validationSchema = (
  targetRaise?: number,
  maxPercent: number = DEFAULT_FEE_PERCENT
) => {
  const yupShape = {
    type: Yup.string().trim(),
    amount: Yup.number()
      .when("type", {
        is: FeeTypes.Percent,
        then: Yup.number()
          .max(
            DEFAULT_FEE_PERCENT,
            !targetRaise
              ? `The total Management Fee amount cannot be greater than ${maxPercent}%`
              : `The total Management Fee amount cannot be greater than ${maxPercent}% of the Target Raise: ` +
                  numeral(targetRaise).format("$0,0.00")
          )
          .transform((value, ogValue) => {
            if (typeof ogValue === "number") return ogValue;
            if (ogValue) return Number(ogValue.replace(/[^0-9.]+/g, ""));
            return null;
          }),
      })
      .when("type", {
        is: FeeTypes.Flat,
        then: Yup.number().transform((value, ogValue) => {
          if (typeof ogValue === "number") return ogValue;
          if (ogValue) return Number(ogValue.replace(/[^0-9.]/g, ""));
          return null;
        }),
        //.max(9999, "The Flat Fee amount should not be above $9,999.00"),
      }),
    frequency: Yup.string().trim(),
    isRecurring: Yup.boolean(),
  };

  const managementFee = Yup.object().shape(yupShape);

  return {
    managementFee,
  };
};

export function testValidFeeAmount(
  targetRaise: number,
  feeAmount: number,
  yupFieldString: string,
  maxPercent: number = DEFAULT_FEE_PERCENT
) {
  const actualPercent = (feeAmount / targetRaise) * 100;

  if (actualPercent > maxPercent) {
    return new Yup.ValidationError(
      `The total Management Fee amount cannot be greater than ${maxPercent}% of the Target Raise: ` +
        numeral(targetRaise).format("$0,0.00"),
      null,
      yupFieldString
    );
  }

  return true;
}

const defaultValues = {
  type: "",
  amount: null,
  isRecurring: false,
};

interface Props {
  managementFee?: {
    type?: string;
    amount?: number;
    frequency?: string;
    isRecurring?: boolean;
  };
}

const SubformManagementFee = ({
  managementFee = {
    type: "",
    amount: 0,
    isRecurring: false,
  },
}: Props) => {
  const [feeType, setFeeType] = useState(managementFee?.type ?? "");
  const [isRecurring, setIsRecurring] = useState(
    managementFee?.isRecurring ?? false
  );

  return (
    <>
      <div className={"field bold"}>
        <label>Fee Type</label>
      </div>
      <Form.Group>
        <FormField
          label="Percent"
          component={Form.Radio}
          name="managementFee.type"
          id="managementFee.typePercent"
          value={FeeTypes.Percent}
          onClick={() => setFeeType(FeeTypes.Percent)}
        />
        <FormField
          label="Flat Fee"
          component={Form.Radio}
          name="managementFee.type"
          id="managementFee.typeFlat"
          value={FeeTypes.Flat}
          onClick={() => setFeeType(FeeTypes.Flat)}
        />
      </Form.Group>
      {feeType === FeeTypes.Percent && (
        <MaskedFormField
          id="managementFee.amountPercent"
          name="managementFee.amount"
          thousandSeparator={false}
          allowNegative={false}
          suffix={"%"}
          decimalScale={2}
          type="tel"
          label={`Fee Amount (% of total funds raised, ${getFeeText(
            isRecurring
          )})`}
          placeholder={"4.5%"}
        />
      )}
      {feeType === FeeTypes.Flat && (
        <MaskedFormField
          id="managementFee.amountFlat"
          name="managementFee.amount"
          thousandSeparator={true}
          allowNegative={false}
          prefix={"$"}
          decimalScale={0}
          type="tel"
          label={`Fee Amount ($ flat fee, ${getFeeText(isRecurring)})`}
          placeholder={"$1500"}
        />
      )}
      <Divider hidden />
      <FormField
        name="managementFee.isRecurring"
        id="managementFee.isRecurring"
        label="Is Recurring"
        component={Form.Checkbox}
        onClick={() => setIsRecurring(!isRecurring)}
      />
    </>
  );
};

export default SubformManagementFee;
export { validationSchema, defaultValues, FeeTypes, fixAmountValue };
