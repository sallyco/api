import React, { useEffect } from "react";
import * as Yup from "yup";
import { FormField, MaskedFormField } from "../common/FormField";
import { Grid, Item } from "semantic-ui-react";
import { useFormikContext, FormikValues } from "formik";
import _ from "lodash";

export const validationSchema = ({ isRequired }) => {
  const month = Yup.number().min(1).max(12).typeError("Invalid Month");
  const day = Yup.number()
    .min(1)
    .when(["month", "year"], (month, year, schema) => {
      if ([4, 6, 9, 11].includes(month)) {
        return schema.max(30);
      } else if ([1, 3, 5, 7, 8, 10, 12].includes(month)) {
        return schema.max(31);
      } else {
        if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)) {
          return schema.max(29);
        } else {
          return schema.max(28);
        }
      }
    })
    .typeError("Invalid Day");
  const year = Yup.number().min(1000).max(9999).typeError("Invalid Year");

  return isRequired
    ? {
        month: month.required(),
        day: day.required(),
        year: year.required(),
      }
    : { month: month, day: day, year: year };
};

export const defaultValues = {
  dateOfBirth: "",
  month: "",
  day: "",
  year: "",
};

interface FormikDOB {
  dateOfBirth: string;
  month: string;
  day: string;
  year: string;
  setFieldValue: object | Function;
  getFieldProps: object | Function;
}

export default function SubformDateOfBirth({ ...props }) {
  const formikContext = useFormikContext<FormikDOB>();
  let values: FormikValues = formikContext?.values ?? {};

  useEffect(() => {
    if (
      formikContext?.setFieldValue &&
      values?.dateOfBirth !== `${values?.year}-${values?.month}-${values?.day}`
    ) {
      formikContext.setFieldValue(
        "dateOfBirth",
        `${values?.year}-${values?.month}-${values?.day}`
      );
    }
  }, [
    formikContext.values.day,
    formikContext.values.month,
    formikContext.values.year,
  ]);

  const keyup = (e) => {
    if (
      e.key.search(/[0-9]/g) === 0 &&
      e.target.value.length &&
      !e.target.value.includes("_") &&
      !Object.keys(formikContext.errors).includes(e.target.id)
    ) {
      switch (e.target.id) {
        case "month":
          document.getElementById("day").focus();
          break;
        case "day":
          document.getElementById("year").focus();
          break;
        default:
          break;
      }
    }
  };

  const keydown = (e) => {
    if (e.key === "Tab" && e.target.value.includes("_")) {
      let partial = e.target.value.replaceAll("_", "");
      switch (e.target.id) {
        case "year":
          formikContext.setFieldValue(
            e.target.id,
            (e.target.value = "2000"
              .slice(0, 4 - partial.length)
              .concat(partial))
          );
          break;
        default:
          formikContext.setFieldValue(
            e.target.id,
            "00".slice(-partial.length).concat(partial)
          );
          break;
      }
    }
  };

  return (
    <div className={"field"} data-testid={"subform-date-of-birth"}>
      <Grid columns={"16"} stackable={false}>
        <Grid.Row style={{ paddingBottom: "0px" }}>
          <Grid.Column width={"15"}>
            <div className={"field bold"}>
              <label>{"Date of Birth"}</label>
            </div>
          </Grid.Column>
          <Grid.Column width={"1"}>
            <FormField
              id="dateOfBirth"
              name="dateOfBirth"
              type="hidden"
              css={"hidden"}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row style={{ paddingTop: "0px" }}>
          <Grid.Column width={"4"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="month"
                  name="month"
                  label="Month"
                  type="text"
                  placeholder="MM"
                  format={"##"}
                  onKeyUp={_.debounce((e) => keyup(e), 100)}
                  onKeyDown={_.debounce((e) => keydown(e), 100)}
                  {...props}
                />
              </Item.Description>
            </Item>
          </Grid.Column>
          <Grid.Column width={"4"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="day"
                  name="day"
                  label="Day"
                  type="text"
                  placeholder="DD"
                  format={"##"}
                  onKeyUp={_.debounce((e) => keyup(e), 100)}
                  onKeyDown={_.debounce((e) => keydown(e), 100)}
                  {...props}
                />
              </Item.Description>
            </Item>
          </Grid.Column>
          <Grid.Column width={"8"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="year"
                  name="year"
                  label="Year"
                  type="text"
                  placeholder="YYYY"
                  format={"####"}
                  onKeyDown={_.debounce((e) => keydown(e), 100)}
                  {...props}
                />
              </Item.Description>
            </Item>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
}
