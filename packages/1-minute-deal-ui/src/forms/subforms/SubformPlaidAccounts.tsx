import React, { useCallback, useEffect, useState } from "react";
import * as Yup from "yup";
import * as Sentry from "@sentry/react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { Loader } from "semantic-ui-react";
import { convertToAccount, getLinkToken } from "../../api/plaidApi";
import { logPlaidLinkError } from "../../plaid/plaidLogging";
import PlaidAccounts from "../banking/PlaidAccounts";
import { fetchPlaidAccountsList } from "../../slices/accountsSlice";

export const validationSchema = () => {
  return {
    primaryAccountId: Yup.string().required(),
    subAccountId: Yup.string().required(),
  };
};

const SubformPlaidAccounts: React.FC<{
  autoSkip?: boolean;
  relinkAccount?: string;
}> = ({ autoSkip, relinkAccount }) => {
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const history = useHistory();
  const [plaidLinkToken, setPlaidLinkToken] = useState<null | string>(null);
  const dispatch = useDispatch();
  const onSuccess = useCallback(async (token, metadata) => {
    await convertToAccount(metadata);
    await dispatch(fetchPlaidAccountsList());
  }, []);
  const onExit = useCallback((error, metaData) => {
    logPlaidLinkError(error, metaData);
    history.push(
      `/subscriptions/${subscriptionId}/bank_account?error=true&errorType=${
        error?.error_code ?? ""
      }`
    );
  }, []);
  useEffect(() => {
    const linkToken = async () => {
      const linkResponse = await getLinkToken();
      setPlaidLinkToken(linkResponse.data);
    };
    if (!plaidLinkToken) {
      linkToken();
    }
  }, [relinkAccount]);
  return (
    <>
      {!plaidLinkToken && (
        <Loader active inline="centered" size="large" inverted={true} />
      )}
      {plaidLinkToken && (
        <PlaidAccounts
          linkToken={plaidLinkToken}
          success={onSuccess}
          exit={onExit}
          autoSkip={autoSkip}
          relinkAccount={relinkAccount}
        />
      )}
    </>
  );
};

export default SubformPlaidAccounts;
