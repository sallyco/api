import React, { useState } from "react";
import * as Yup from "yup";
import { Form } from "semantic-ui-react";
import { FormField } from "../common/FormField";
import { NumberOfemplyees } from "../../tools/enums";
import { useFormikContext } from "formik";
import { Box, Typography } from "@mui/material";

type regDExemption = "b" | "c" | "d";
const validationSchema = () => {
  return {
    regDExemption: Yup.string().trim(),
  };
};

const defaultValues = {
  regDExemption: "b",
};

interface Props {
  regDExemption?: regDExemption;
}

const links: {
  [key: string]: string;
} = {
  b: "https://www.sec.gov/smallbusiness/exemptofferings/rule506b",
  c: "https://www.sec.gov/smallbusiness/exemptofferings/rule506c",
};

const SubformRegDExemption = ({ regDExemption = "b" }: Props) => {
  const props = useFormikContext<{
    regDExemption: regDExemption;
  }>();
  return (
    <>
      <Form.Group>
        <FormField
          name={`regDExemption`}
          component={Form.Select}
          label="Reg D Type"
          options={[
            {
              key: "b",
              value: "b",
              text: "506(b)",
            },
            {
              key: "c",
              value: "c",
              text: "506(c)",
            },
          ]}
          placeholder="Reg 506 (b)"
        />
      </Form.Group>
      <Box>
        <Typography
          style={{
            color: "white",
          }}
        >
          More Info:{" "}
          <a
            rel="noreferrer"
            target={"_blank"}
            href={links[props.values.regDExemption]}
          >
            {links[props.values.regDExemption]}
          </a>
        </Typography>
      </Box>
    </>
  );
};

export default SubformRegDExemption;
export { validationSchema, defaultValues };
