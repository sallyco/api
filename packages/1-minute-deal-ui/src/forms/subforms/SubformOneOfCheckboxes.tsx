import React from "react";
import * as Yup from "yup";
import { Form, Header } from "semantic-ui-react";
import { FormField } from "../common/FormField";

export interface OneOfCheckbox {
  fieldName: string;
  text: string;
  options?: {
    // When you choose to apply rejectedSelections
    // This will cause YUP to fail if it is selected
    rejectIfSelected?: boolean;
    rejectIfSelectedMessage?: string;
    optional?: boolean;
  };
}

export function getMeta(
  checkboxes: OneOfCheckbox[],
  activateRejections: boolean = false
) {
  const oneOfCheckboxes = checkboxes.filter(
    (checkbox) => !checkbox?.options?.optional
  );
  const badCheckboxes = activateRejections
    ? checkboxes.filter((checkbox) => checkbox?.options?.rejectIfSelected)
    : [];

  // Set all fields to be boolean false defaults
  const validationSchema = {};
  const defaultValues = {};
  checkboxes.forEach(({ fieldName }) => {
    validationSchema[fieldName] = Yup.boolean();
    defaultValues[fieldName] = false;
  });

  // Yup test function
  const oneOfTest = (obj) => {
    // Check exceptions first, are any excluded options checked?
    for (const checkbox of badCheckboxes) {
      if (obj?.[checkbox.fieldName]) {
        const message =
          checkbox?.options?.rejectIfSelectedMessage ||
          "If this statement is true, you may not be qualified to continue.";
        return new Yup.ValidationError(message, null, checkbox.fieldName);
      }
    }

    // check if any of the oneOf keys are checked
    // If one is checked, selections are valid
    for (const checkbox of oneOfCheckboxes) {
      if (obj?.[checkbox.fieldName]) {
        return true;
      }
    }

    return new Yup.ValidationError(
      "You must select at least one option",
      null,
      checkboxes[0].fieldName
    );
  };

  return { defaultValues, validationSchema, oneOfTest };
}

export default function SubformOneOfCheckboxes({
  header,
  section1Checkboxes = [],
  section2Header = "",
  section2Checkboxes = [],
  ...props
}: {
  header: string;
  section1Checkboxes: OneOfCheckbox[];
  section2Header?: string;
  section2Checkboxes?: OneOfCheckbox[];
}) {
  return (
    <>
      <Header as="h3" inverted>
        {header}
      </Header>
      {section1Checkboxes.map(({ fieldName, text }) => {
        return (
          <FormField
            component={Form.Checkbox}
            id={fieldName}
            name={fieldName}
            key={fieldName}
            label={text}
          />
        );
      })}

      {section2Header && (
        <Header as="h3" inverted>
          {section2Header}
        </Header>
      )}

      {section2Checkboxes.map(({ fieldName, text }) => {
        return (
          <FormField
            component={Form.Checkbox}
            id={fieldName}
            name={fieldName}
            key={fieldName}
            label={text}
          />
        );
      })}
    </>
  );
}
