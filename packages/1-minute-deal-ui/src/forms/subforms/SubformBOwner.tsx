import React, { useState } from "react";
import * as Yup from "yup";
import { Form, Divider, Segment } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";
import {
  isValidZip,
  TaxIdValidationSchema,
} from "../validation/ValidationHelper";

export const validationSchema = () => {
  return {
    beneficialOwnerfirstName: Yup.string().max(64),
    beneficialOwnerlastName: Yup.string().max(64),
    beneficialOwnertitle: Yup.string()

      .min(2)
      .max(64),

    beneficialOwneremail: Yup.string().email(
      "Email is not in the correct format"
    ),

    beneficialOwnerphone: Yup.string()

      .transform(function (value, originalvalue) {
        return originalvalue.replace(/\D+/g, "");
      })
      .length(10, "Phone is incomplete"),

    beneficialOwneraddress1: Yup.string()

      .min(2)
      .max(1024),
    beneficialOwneraddress2: Yup.string().max(1024),
    beneficialOwnercity: Yup.string()

      .min(2)
      .max(1024),
    beneficialOwnerstate: Yup.string()

      .min(2)
      .max(1024),
    beneficialOwnerpostalCode: Yup.string()

      .min(2)
      .max(1024)
      .when("beneficialOwnercountry", (country, schema) => {
        if (country === "United States of America") {
          return isValidZip(schema);
        }
        return schema;
      }),
    beneficialOwnercountry: Yup.string()

      .min(2)
      .max(1024),
    beneficialOwnerisUSBased: Yup.string(),
    beneficialOwnertaxId: TaxIdValidationSchema(),
  };
};

export const defaultValues = {
  beneficialOwnertitle: "",
  beneficialOwnerfirstName: "",
  beneficialOwnerlastName: "",
  beneficialOwnerphone: "",

  beneficialOwneraddress1: "",
  beneficialOwneraddress2: "",
  beneficialOwnercity: "",
  beneficialOwnerstate: "",
  beneficialOwnerpostalCode: "",
  beneficialOwnercountry: "United States of America",

  beneficialOwnerisUSBased: "Yes",
  beneficialOwnertaxId: "",
};

export default function SubformSigner1({ setFieldValue, values, ...props }) {
  const [showTaxId, setShowTaxId] = useState(false);

  return (
    <>
      <Form.Group widths="equal">
        <FormField
          id="beneficialOwnerfirstName"
          name="beneficialOwnerfirstName"
          label="First Name"
          placeholder="First Name"
        />
        <FormField
          id="beneficialOwnerlastName"
          name="beneficialOwnerlastName"
          label="Last Name"
          placeholder="Last Name"
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="beneficialOwnertitle"
          name="beneficialOwnertitle"
          label="Title"
          placeholder="Title"
        />
        <MaskedFormField
          id="beneficialOwnerphone"
          name="beneficialOwnerphone"
          label={"Phone"}
          format="(###) ###-####"
          placeholder="Phone Number"
        />
      </Form.Group>
      <FormField
        label={"Email"}
        id="beneficialOwneremail"
        name="beneficialOwneremail"
        placeholder="email@example.com"
      />

      <Divider />

      <FormField
        id="beneficialOwneraddress1"
        name="beneficialOwneraddress1"
        label="Address"
        placeholder="Address 1"
      />
      <FormField
        id="beneficialOwneraddress2"
        name="beneficialOwneraddress2"
        placeholder="Address 2"
      />
      <Form.Group widths="equal">
        <FormField
          id="beneficialOwnercity"
          name="beneficialOwnercity"
          placeholder="City"
        />
        <FormField
          name={"beneficialOwnerstate"}
          id={"beneficialOwnerstate"}
          component={Form.Select}
          options={(values.beneficialOwnercountry
            ? getStates(
                getCountries().find((item) => {
                  return item.name === values.country;
                })?.code
              )
            : []
          ).map((item) => {
            return {
              key: item,
              value: item,
              text: item,
            };
          })}
          placeholder={"Select a State"}
          search={startsWithSearch}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="beneficialOwnerpostalCode"
          name="beneficialOwnerpostalCode"
          placeholder="Postal Code"
        />
        <FormField
          name={"beneficialOwnercountry"}
          id={"beneficialOwnercountry"}
          component={Form.Select}
          options={getCountries().map((item) => {
            return {
              key: item.code,
              value: item.name,
              text: item.name,
            };
          })}
          placeholder={"Select a Country"}
          search={startsWithSearch}
        />
      </Form.Group>

      <Divider />

      <Form.Group widths="equal">
        <Form.Field width={4}>
          <Segment basic inverted className="ui no-padding">
            Are you a US person?
          </Segment>
          <Form.Group>
            <FormField
              label="Yes"
              component={Form.Radio}
              name="beneficialOwnerisUSBased"
              value="Yes"
            />
            <FormField
              label="No"
              component={Form.Radio}
              name="beneficialOwnerisUSBased"
              value="No"
            />
          </Form.Group>
        </Form.Field>

        <MaskedFormField
          id="beneficialOwnertaxId"
          name="beneficialOwnertaxId"
          width={12}
          data-testid={"taxId"}
          autoComplete="off"
          label={
            <LabelwithTooltip
              labelText={
                values.beneficialOwnerisUSBased === "Yes"
                  ? "Social Security Number (SSN)"
                  : "Individual Taxpayer Identification Number (ITIN)"
              }
              popupText={
                values.beneficialOwnerisUSBased === "Yes"
                  ? "Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                  : "An ITIN is issued by the IRS to persons (both residents and nonresidents of the US) who are not eligible for an SSN, but still need to file taxes and reports with the IRS."
              }
              iconName="question circle"
              iconColor="green"
            />
          }
          type={showTaxId ? "text" : "password"}
          format={
            values.beneficialOwnerisUSBased === "Yes"
              ? "###-##-####"
              : "9##-##-####"
          }
          placeholder={
            values.beneficialOwnerisUSBased === "Yes"
              ? "123-54-6789"
              : "987-56-4321"
          }
          action={{
            icon: showTaxId ? "eye slash" : "eye",
            onClick: () => setShowTaxId(!showTaxId),
            type: "button",
          }}
        />
      </Form.Group>
    </>
  );
}
