import React, { useEffect } from "react";
import { Form, Divider } from "semantic-ui-react";
import * as Yup from "yup";

import SubformFirstLastName, * as SubformFirstLastNameMeta from "./SubformFirstLastName";
import SubformEmail, * as SubformEmailMeta from "./SubformEmail";
import SubformPhone, * as SubformPhoneMeta from "./SubformPhone";
import SubformAddress, * as SubformAddressMeta from "./SubformAddress";
import SubformIndividualTaxId, * as SubformIndividualTaxIdMeta from "./SubformIndividualTaxId";
import { FormikProps, useFormikContext } from "formik";
import { useLoggedInUser } from "../../hooks/userHooks";
import { FormField } from "../common/FormField";
import SubformDateOfBirth, * as SubformDateOfBirthMeta from "./SubformDateOfBirth";
export const validationSchema = ({ isRequired }) => {
  return {
    ...(isRequired
      ? {
          displayName: Yup.string()
            .max(64, "Display Name is limited to 64 characters")
            .required(),
        }
      : { displayName: Yup.string().max(64) }),
    ...SubformFirstLastNameMeta.validationSchema({ isRequired }),
    ...SubformEmailMeta.validationSchema({ isRequired }),
    ...SubformIndividualTaxIdMeta.validationSchema({ isRequired }),
    ...SubformPhoneMeta.validationSchema({ isRequired }),
    ...SubformAddressMeta.validationSchema(),
    ...SubformDateOfBirthMeta.validationSchema({ isRequired }),
  };
};

export const defaultValues = {
  displayName: "",
  ...SubformFirstLastNameMeta.defaultValues,
  ...SubformEmailMeta.defaultValues,
  ...SubformPhoneMeta.defaultValues,
  ...SubformAddressMeta.defaultValues,
  ...SubformIndividualTaxIdMeta.defaultValues,
};

const SubformPerson: React.FC<
  { useAccountDefaultValues: boolean } & FormikProps<any>
> = ({ useAccountDefaultValues, setFieldValue, values, ...props }) => {
  const myUser = useLoggedInUser();
  const formikContext = useFormikContext();

  useEffect(() => {
    if (myUser && useAccountDefaultValues) {
      formikContext.setValues(
        {
          // @ts-ignore
          ...formikContext.values,
          displayName: "",
          firstName: myUser?.firstName,
          lastName: myUser?.lastName,
          email: myUser?.email,
          phone: myUser?.attributes?.phone[0] ?? "",
          dateOfBirth: "",
        },
        true
      );
      formikContext.setTouched(
        {
          displayName: true,
          firstName: true,
          lastName: true,
          email: true,
          phone: true,
        },
        false
      );
    }
  }, [myUser]);
  return (
    <>
      <Form.Group widths="equal">
        <FormField
          id={`displayName`}
          name={`displayName`}
          label="Display Name"
          placeholder="Display Name"
          data-testid={"displayName"}
          {...props}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <SubformFirstLastName />
      </Form.Group>
      <Form.Group widths="equal">
        <SubformEmail />
        <SubformPhone />
      </Form.Group>
      <Divider />
      <SubformDateOfBirth />
      <Divider />
      <SubformIndividualTaxId values={values} {...props} />
      <Divider />
      <SubformAddress
        setFieldValue={setFieldValue}
        values={values}
        {...props}
      />
    </>
  );
};

export default SubformPerson;
