import React, { useState } from "react";
import * as Yup from "yup";
import { Form, Divider, Header } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import {
  isValidZip,
  TaxIdValidationSchema,
} from "../validation/ValidationHelper";
import SubformDateOfBirth from "./SubformDateOfBirth";
import SubformAddress from "./SubformAddress";

export const validationSchema = () => {
  return {
    primarySignatoryfirstName: Yup.string().required().max(64),
    primarySignatorylastName: Yup.string().required().max(64),
    primarySignatorytitle: Yup.string().required().min(2).max(64),

    primarySignatoryemail: Yup.string().email(
      "Email is not in the correct format"
    ),

    primarySignatoryphone: Yup.string()

      .transform(function (value, originalvalue) {
        return originalvalue.replace(/\D+/g, "");
      })
      .length(10, "Phone is incomplete"),

    primarySignatoryaddress1: Yup.string()

      .min(2)
      .max(1024),
    primarySignatoryaddress2: Yup.string().max(1024),
    primarySignatorycity: Yup.string()

      .min(2)
      .max(1024),
    primarySignatorystate: Yup.string()

      .min(2)
      .max(1024),
    primarySignatorypostalCode: Yup.string()

      .min(2)
      .max(1024)
      .when("primarySignatorycountry", (country, schema) => {
        if (country === "United States of America") {
          return isValidZip(schema);
        }
        return schema;
      }),
    primarySignatorycountry: Yup.string()

      .min(2)
      .max(1024),
    primarySignatoryisUSBased: Yup.string(),
    primarySignatorytaxId: TaxIdValidationSchema().when(
      "primarySignatoryisUSBased",
      (primarySignatoryisUSBased, schema) => {
        if (primarySignatoryisUSBased && primarySignatoryisUSBased === "Yes") {
          return schema.required("SSN for Primary Signatory is Required");
        }
        return schema;
      }
    ),
    primarySignatoryAddress: Yup.object().shape({
      address1: Yup.string().required().min(2).max(1024),
      address2: Yup.string().max(1024),
      city: Yup.string().required().min(2).max(1024),
      state: Yup.string().required().min(2).max(1024),
      postalCode: Yup.string()
        .required()
        .min(2)
        .max(1024)
        .when("primarySignatorycountry", (country, schema) => {
          if (country === "United States of America") {
            return isValidZip(schema);
          }
          return schema;
        }),
      country: Yup.string().required().min(2).max(1024),
    }),
  };
};

export const defaultValues = {
  primarySignatorytitle: "",
  primarySignatoryfirstName: "",
  primarySignatorylastName: "",
  primarySignatoryphone: "",

  primarySignatoryaddress1: "",
  primarySignatoryaddress2: "",
  primarySignatorycity: "",
  primarySignatorystate: "",
  primarySignatorypostalCode: "",
  primarySignatorycountry: "United States of America",

  primarySignatoryAddress: {
    address1: "",
    address2: "",
    city: "",
    state: "",
    postalCode: "",
    country: "United States of America",
  },
  primarySignatoryisUSBased: "",
  primarySignatorytaxId: "",
};

export default function SubformSigner1({ setFieldValue, values, ...props }) {
  const [showTaxId, setShowTaxId] = useState(false);

  return (
    <>
      <Form.Group widths="equal">
        <FormField
          id="primarySignatoryfirstName"
          name="primarySignatoryfirstName"
          label="First Name"
          placeholder="First Name"
          data-testid={"first-name"}
          required
        />
        <FormField
          id="primarySignatorylastName"
          name="primarySignatorylastName"
          label="Last Name"
          placeholder="Last Name"
          required
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="primarySignatorytitle"
          name="primarySignatorytitle"
          label="Title"
          placeholder="Title"
          required
        />
      </Form.Group>
      <Form.Group>
        <SubformDateOfBirth {...props} />
      </Form.Group>
      <SubformAddress
        setFieldValue={setFieldValue}
        namespace={"primarySignatoryAddress"}
        {...props}
        values={values}
      />

      {values.entityCountry === "United States of America" && (
        <>
          <Divider />

          <Form.Group widths="equal">
            <Form.Field>
              <Header as="h3" {...props} inverted>
                Signatory is a US citizen...
              </Header>
              <Form.Group>
                <FormField
                  label="Yes"
                  component={Form.Radio}
                  name="primarySignatoryisUSBased"
                  value="Yes"
                />
                <FormField
                  label="No"
                  component={Form.Radio}
                  name="primarySignatoryisUSBased"
                  value="No"
                />
              </Form.Group>
            </Form.Field>
          </Form.Group>

          {values.primarySignatoryisUSBased === "Yes" && (
            <Form.Group widths="equal">
              <MaskedFormField
                id="primarySignatorytaxId"
                name="primarySignatorytaxId"
                data-testid={"taxId"}
                autoComplete="off"
                label={
                  <LabelwithTooltip
                    labelText="Social Security Number (SSN)"
                    popupText="Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                    iconName="question circle"
                    iconColor="green"
                  />
                }
                type={showTaxId ? "text" : "password"}
                format="###-##-####"
                placeholder="123-54-6789"
                action={{
                  icon: showTaxId ? "eye slash" : "eye",
                  onClick: () => setShowTaxId(!showTaxId),
                  type: "button",
                }}
              />
            </Form.Group>
          )}
        </>
      )}
    </>
  );
}
