import React from "react";
import * as Yup from "yup";
import { FormField, MaskedFormField } from "../common/FormField";
import { Form, Header } from "semantic-ui-react";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import { useFormikContext } from "formik";
import { ForeignEntityTaxIdTypes } from "../../tools/enums";

export const validationSchema = ({ isRequired }) => {
  const val = Yup.string().test(
    "dynamic_length",
    "Required number of characters is incorrect",
    (value, context) => {
      if (context?.parent?.entityCountry === "United States of America") {
        return value?.length === 10;
      } else if (context?.parent?.hasTaxId === "Yes") {
        if (context?.parent?.taxIdType === "giin") {
          return value?.length === 19;
        } else if (context?.parent?.taxIdType === "ein") {
          return value?.length === 10;
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  );
  return isRequired ? { taxId: val.required() } : { taxId: val };
};

export const defaultValues = {
  taxId: "",
  hasTaxId: "",
  taxIdType: "ein",
};

export default function SubformEntityTaxId() {
  const { values }: { values: any } = useFormikContext();
  return (
    <>
      {values.entityCountry === "United States of America" ? (
        <>
          <Header as="h3" inverted>
            Employer Identification Number (EIN)
          </Header>
          <MaskedFormField
            id="taxId"
            name="taxId"
            data-testid={"ein"}
            autoComplete="off"
            format="##-#######"
            placeholder={"98-7654321"}
          />
        </>
      ) : (
        <>
          <Form.Group widths="equal">
            <Form.Field>
              <Header as="h3" inverted>
                I have one of these tax identification numbers..
                <Header.Subheader>
                  &nbsp;Employer Identification Number (EIN)
                </Header.Subheader>
                <Header.Subheader>
                  &nbsp;Global Intermediary Identification Number (GIIN)
                </Header.Subheader>
                <Header.Subheader>
                  &nbsp;Foreign Tax Identification
                </Header.Subheader>
              </Header>
              <Form.Group>
                <FormField
                  label="Yes"
                  size="large"
                  component={Form.Radio}
                  name="hasTaxId"
                  value="Yes"
                />
                <FormField
                  label="No"
                  component={Form.Radio}
                  name="hasTaxId"
                  value="No"
                />
              </Form.Group>
            </Form.Field>
          </Form.Group>

          {values.hasTaxId === "Yes" && (
            <>
              <FormField
                id="taxIdType"
                name="taxIdType"
                component={Form.Select}
                label="Tax ID Type"
                options={ForeignEntityTaxIdTypes}
                placeholder="Select a Type"
              />
              {values.taxIdType === "giin" ? (
                <MaskedFormField
                  id="taxId"
                  name="taxId"
                  autoComplete="off"
                  label={
                    <LabelwithTooltip
                      labelText="Global Intermediary Identification Number (GIIN)"
                      popupText="A GIIN is a Global Intermediary Identification Number, consisting of 19 characters. "
                      iconName="question circle"
                      iconColor="green"
                    />
                  }
                  format="######.#####.##.###"
                  placeholder={"123456.12345.12.123"}
                />
              ) : (
                <>
                  {values.taxIdType === "ein" ? (
                    <MaskedFormField
                      id="taxId"
                      name="taxId"
                      data-testid={"ein"}
                      autoComplete="off"
                      label={
                        <LabelwithTooltip
                          labelText="Employer Identification Number (EIN)"
                          popupText="An EIN is a unique 9-digit number issued by the IRS to persons or entities doing business in the United States for purposes of identification. It is also known as Federal Employer Identification Number or Federal Tax Identification Number."
                          iconName="question circle"
                          iconColor="green"
                        />
                      }
                      format="##-#######"
                      placeholder={"98-7654321"}
                    />
                  ) : (
                    <FormField id="taxId" name="taxId" label="Foreign Tax ID" />
                  )}
                </>
              )}
            </>
          )}
        </>
      )}
    </>
  );
}
