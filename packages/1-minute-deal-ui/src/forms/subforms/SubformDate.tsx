import React from "react";
import moment from "moment";
import { FormField, MaskedFormField } from "../common/FormField";
import { Grid, Item, Label } from "semantic-ui-react";
import * as Yup from "yup";
import { useFormikContext, FormikValues } from "formik";

export const validationSchema = ({ isRequired }) => {
  const month = Yup.number().min(1).max(12).typeError("Invalid Month");
  const day = Yup.number()
    .min(1)
    .when(["month", "year"], (month, year, schema) => {
      if ([4, 6, 9, 11].includes(month)) {
        return schema.max(30);
      } else if ([1, 3, 5, 7, 8, 10, 12].includes(month)) {
        return schema.max(31);
      } else {
        if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)) {
          return schema.max(29);
        } else {
          return schema.max(28);
        }
      }
    })
    .typeError("Invalid Day");
  const year = Yup.number().min(1000).max(9999).typeError("Invalid Year");

  return isRequired
    ? {
        month: month.required(),
        day: day.required(),
        year: year.required(),
      }
    : { month: month, day: day, year: year };
};

export function testNotInThePast(obj) {
  const fullDate = moment(new Date(`${obj.year}-${obj.month}-${obj.day}`));
  const now = moment().startOf("day");

  if (fullDate.isSameOrAfter(now)) {
    return true;
  } else {
    return new Yup.ValidationError(
      "Date must be Today or in the Future",
      null,
      "year"
    );
  }
}

export const defaultValues = {
  date: "",
  month: "",
  day: "",
  year: "",
};

export interface SubformDateProps {
  id?: string;
  name?: string;
  placeholder?: string;
  label?: string;
  onChange?: (e: React.FormEvent<HTMLInputElement>) => void;
}
interface FormikDate {
  dateOfBirth: string;
  month: string;
  day: string;
  year: string;
  setFieldValue: object | Function;
  getFieldProps: object | Function;
}

const SubformDate = function ({
  id,
  name,
  placeholder,
  label,
  ...props
}: Partial<SubformDateProps>) {
  const formikContext = useFormikContext<FormikDate>();
  let values: FormikValues = formikContext?.values ?? {};
  id = id ?? "Date";
  name = name ?? "date";

  const setDate = () => {
    if (
      formikContext?.setFieldValue &&
      values[name] !== `${values?.year}-${values?.month}-${values?.day}`
    ) {
      formikContext.setFieldValue(
        name,
        `${values?.year}-${values?.month}-${values?.day}`
      );
    }
  };

  // Handle setting the error state of the field
  // call blur when it might be valid
  const keyup = (e) => {
    const keyIsNumeric = e.key.search(/[0-9]/g) === 0;
    const inputHasValue = e.currentTarget.value?.length > 0;
    const includesUnderscore = e.currentTarget.value.includes("_");

    if (keyIsNumeric && inputHasValue && !includesUnderscore) {
      if (["month", "day", "year"].includes(e.currentTarget.id)) {
        document.getElementById(e.currentTarget.id).blur();
      }
    }
  };

  const keydown = (e) => {
    formikContext.setFieldError("year", "");
    if (e.key === "Tab" && e.currentTarget.value.includes("_")) {
      let partial = e.currentTarget.value.replaceAll("_", "");
      switch (e.currentTarget.id) {
        case "year":
          formikContext.setFieldValue(
            e.currentTarget.id,
            (e.currentTarget.value = "2000"
              .slice(0, 4 - partial.length)
              .concat(partial))
          );
          break;
        default:
          formikContext.setFieldValue(
            e.currentTarget.id,
            "00".slice(-partial.length).concat(partial)
          );
          break;
      }
    }
  };

  return (
    <div className={"field"}>
      <Grid columns={"16"} stackable={false}>
        <Grid.Row style={{ paddingBottom: "0px" }}>
          <Grid.Column width={"15"}>
            <div className={"field bold"}>
              <label>{label}</label>
            </div>
          </Grid.Column>
          <Grid.Column width={"1"}>
            <FormField id={id} name={name} type="hidden" css={"hidden"} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row style={{ paddingTop: "0px" }}>
          <Grid.Column width={"4"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="month"
                  name="month"
                  label="Month"
                  type="text"
                  placeholder="MM"
                  format={"##"}
                  onChange={setDate()}
                  onKeyUp={(e) => keyup(e)}
                  onKeyDown={(e) => keydown(e)}
                  required
                />
              </Item.Description>
            </Item>
          </Grid.Column>
          <Grid.Column width={"4"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="day"
                  name="day"
                  label="Day"
                  type="text"
                  placeholder="DD"
                  format={"##"}
                  onChange={setDate()}
                  onKeyUp={(e) => keyup(e)}
                  onKeyDown={(e) => keydown(e)}
                  required
                />
              </Item.Description>
            </Item>
          </Grid.Column>
          <Grid.Column width={"8"}>
            <Item>
              <Item.Description>
                <MaskedFormField
                  id="year"
                  name="year"
                  label="Year"
                  type="text"
                  placeholder="YYYY"
                  format={"####"}
                  onChange={setDate()}
                  onKeyDown={(e) => keydown(e)}
                  onKeyUp={(e) => keyup(e)}
                  required
                />
              </Item.Description>
            </Item>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default SubformDate;
