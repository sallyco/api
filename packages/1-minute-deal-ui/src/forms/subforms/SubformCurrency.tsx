import React from "react";
import * as Yup from "yup";
import { MaskedFormField } from "../common/FormField";

export interface SubformCurrencyProps {
  id?: string;
  name?: string;
  placeholder?: string | number;
  label?: string;
  icon?: string;
  [x: string]: any;
}

const SubformCurrency = function ({
  id,
  name,
  placeholder,
  label,
  icon,
  ...props
}: Partial<SubformCurrencyProps>) {
  return (
    <MaskedFormField
      id={id ?? "amount"}
      name={name ?? "amount"}
      thousandSeparator={true}
      allowNegative={false}
      prefix={"$"}
      icon={icon}
      decimalScale={0}
      type="tel"
      label={label}
      placeholder={placeholder ?? "$5,000"}
      {...props}
    />
  );
};

export default SubformCurrency;
