import React from "react";
import * as Yup from "yup";
import { Form, Segment, Header, Divider } from "semantic-ui-react";
import { FormField } from "../common/FormField";

export const validationSchema = () => {
  return {
    certify0: Yup.boolean(),
    certify1: Yup.boolean(),
    certify2: Yup.boolean(),
    certify10: Yup.boolean(),
  };
};

export const defaultValues = {
  certify0: false,
  certify1: false,
  certify2: false,
  certify10: false,
};

export default function SubformAccreditation({ ...props }) {
  return (
    <>
      <Header as="h3" inverted>
        Check all that apply...
      </Header>
      <FormField
        id="certify0"
        name="certify0"
        component={Form.Checkbox}
        label="If an individual, Subscriber has a net worth, either individually or upon a joint basis with Subscriber's spouse, of at least $1,000,000, or has had an individual income in excess of $200,000 for each of the two most recent years, or a joint income with Subscriber's spouse, or spousal equivalent, in excess of $300,000 in each of those years, and has a reasonable expectation of reaching the same income level in the current year."
      />
      <FormField
        id="certify1"
        name="certify1"
        component={Form.Checkbox}
        label="If an individual, Subscriber holds, in good standing, one of the following certifications or designations administered by the Financial Industry Regulatory Authority, Inc. (“FINRA”): the Licensed General Securities Representative (Series 7), Licensed Investment Adviser Representative (Series 65), or Licensed Private Securities Offerings Representative (Series 82)."
      />
      <FormField
        id="certify2"
        name="certify2"
        component={Form.Checkbox}
        label="If an individual, Subscriber is a “knowledgeable employee”  within the meaning prescribed under Rule 3c-5(a)(4) of the Investment Company Act of 1940, as amended (the “Investment Company Act”) of a private fund exempt from registration pursuant to Rule 3(c)(1) or Rule 3(c)(7) of the Investment Company Act."
      />
      <FormField
        id="certify10"
        name="certify10"
        component={Form.Checkbox}
        label="None of the previous statments apply to me."
      />
    </>
  );
}
