import React, { useState } from "react";
import * as Yup from "yup";
import { Form, Segment, Header } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import { UsTrustTaxIdTypes, ForeignTrustTaxIdTypes } from "../../tools/enums";

export const validationSchema = ({ isRequired }) => {
  return {
    isUSBased: Yup.string(),
    taxId: Yup.string()
      .required()
      .test({
        name: "dynamic_length",
        message: "Required number of characters is incorrect",
        test: (value, context) => {
          if (context?.parent?.entityCountry === "United States of America") {
            if (context?.parent?.taxIdType === "ssn") {
              return value?.length === 11;
            } else {
              return value?.length === 10;
            }
          } else {
            if (context?.parent?.hasTaxId === "Yes") {
              if (context?.parent?.taxIdType === "ein") {
                return value?.length === 10;
              } else if (context?.parent?.taxIdType === "itin") {
                return value?.length === 11;
              } else {
                return true;
              }
            } else {
              return true;
            }
          }
        },
      }),
  };
};

export const defaultValues = {
  isUSBased: "Yes",
  taxId: "",
  hasTaxId: "",
  taxIdType: "ein",
};

export default function SubformTrustTaxId({ values, ...props }) {
  const [showTaxId, setShowTaxId] = useState(false);
  return (
    <>
      {values.entityCountry === "United States of America" && (
        <>
          <Form.Group widths="equal">
            <Form.Field>
              <FormField
                id="taxIdType"
                name="taxIdType"
                component={Form.Select}
                label="Tax ID Type"
                options={UsTrustTaxIdTypes}
                placeholder="Select a Type"
              />
            </Form.Field>
          </Form.Group>

          <MaskedFormField
            id="taxId"
            name="taxId"
            data-testid={"taxId"}
            autoComplete="off"
            label={
              <LabelwithTooltip
                labelText={
                  values.taxIdType === "ssn"
                    ? "Social Security Number (SSN)"
                    : "Employer Identification Number (EIN)"
                }
                popupText={
                  values.taxIdType === "ssn"
                    ? "Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                    : "An EIN is a unique 9-digit number issued by the IRS to persons or entities doing business in the United States for purposes of identification. It is also known as Federal Employer Identification Number or Federal Tax Identification Number."
                }
                iconName="question circle"
                iconColor="green"
              />
            }
            type={"text"}
            format={values.taxIdType === "ssn" ? "###-##-####" : "##-#######"}
            placeholder={
              values.taxIdType === "ssn" ? "123-54-6789" : "98-7564321"
            }
            // action={{
            //   icon: showTaxId ? "eye slash" : "eye",
            //   onClick: () => setShowTaxId(!showTaxId),
            //   type: "button",
            // }}
            required
          />
        </>
      )}

      {values.entityCountry !== "United States of America" && (
        <>
          <Form.Group widths="equal">
            <Form.Field>
              <Header as="h3" inverted>
                This trust has one of these tax identification numbers..
                <Header.Subheader>
                  &nbsp;Employer Identification Number (EIN)
                </Header.Subheader>
                <Header.Subheader>
                  &nbsp;Individual Taxpayer Identification Number (ITIN)
                </Header.Subheader>
                <Header.Subheader>
                  &nbsp;Foreign Tax Identification
                </Header.Subheader>
              </Header>
              <Form.Group>
                <FormField
                  label="Yes"
                  size="large"
                  component={Form.Radio}
                  name="hasTaxId"
                  value="Yes"
                />
                <FormField
                  label="No"
                  component={Form.Radio}
                  name="hasTaxId"
                  value="No"
                />
              </Form.Group>
            </Form.Field>
          </Form.Group>

          {values.hasTaxId === "Yes" && (
            <>
              <Form.Group widths="equal">
                <Form.Field>
                  <FormField
                    id="taxIdType"
                    name="taxIdType"
                    component={Form.Select}
                    label="Tax ID Type"
                    options={ForeignTrustTaxIdTypes}
                    placeholder="Select a Type"
                  />
                </Form.Field>
              </Form.Group>

              <Form.Group widths="equal">
                {(values.taxIdType === "ein" ||
                  values.taxIdType === "itin") && (
                  <MaskedFormField
                    id="taxId"
                    name="taxId"
                    data-testid={"taxId"}
                    autoComplete="off"
                    label={
                      <LabelwithTooltip
                        labelText={
                          values.taxIdType === "ein"
                            ? "Employer Identification Number (EIN)"
                            : "Individual Taxpayer Identification Number (ITIN)"
                        }
                        popupText={
                          values.taxIdType === "ein"
                            ? "An EIN is a unique 9-digit number issued by the IRS to persons or entities doing business in the United States for purposes of identification. It is also known as Federal Employer Identification Number or Federal Tax Identification Number."
                            : "An ITIN is issued by the IRS to persons (both residents and nonresidents of the US) who are not eligible for an SSN, but still need to file taxes and reports with the IRS."
                        }
                        iconName="question circle"
                        iconColor="green"
                      />
                    }
                    type="text"
                    format={
                      values.taxIdType === "ein" ? "##-#######" : "9##-##-####"
                    }
                    placeholder={
                      values.taxIdType === "ein" ? "12-3546789" : "987-56-4321"
                    }
                    required
                  />
                )}

                {values.taxIdType === "ftin" && (
                  <FormField
                    id="taxId"
                    name="taxId"
                    label="Foreign Tax ID"
                    required
                  />
                )}
              </Form.Group>
            </>
          )}
        </>
      )}
    </>
  );
}
