import React from "react";
import { Form, Header, Progress, Divider } from "semantic-ui-react";
import { FeatureFlag } from "../../components/featureflags/FeatureFlags";
import SubformOneOfCheckboxes, {
  OneOfCheckbox,
  getMeta,
} from "./SubformOneOfCheckboxes";

import ProfileWizardActions from "./../../components/profiles/ProfileWizardActions";

const section1Checkboxes: OneOfCheckbox[] = [
  {
    fieldName: "purchaserStatus0",
    text: "(i) Any natural person (including any person who holds a joint, community property, or other similar shared ownership interest in an issuer that is excepted under 3(c)(7) of the Investment Company Act with that person’s Qualified Purchaser spouse) who owns not less than $5,000,000 in investments, as defined by the Commission, and further defined below.",
  },
  {
    fieldName: "purchaserStatus1",
    text: "(ii) Any company that was not formed or recapitalized for the specific purpose of making an investment in the Fund that owns not less than $5,000,000 in investments and that is owned directly or indirectly by or for two or more natural persons who are related as siblings or spouse (including former spouses), or direct lineal descendants by birth or adoption, spouses of such persons, the estates of such persons, or foundations, charitable organizations, or trusts established by or for the benefit of such persons.",
  },
  {
    fieldName: "purchaserStatus2",
    text: "(iii) A trust that is not covered by clause (ii) and that was not formed or recapitalized for the specific purpose of making an investment in the Fund, as to which the trustee or other person authorized to make decisions with respect to the trust, and each settlor or other person who has contributed assets to the trust, is a person described in clause (i), (ii), or (iv).",
  },
  {
    fieldName: "purchaserStatus3",
    text: "(iv) A person acting for its own account or the accounts of other Qualified Purchasers, who in the aggregate owns and invests on a discretionary basis, not less than $25,000,000 in investments.",
  },
  {
    fieldName: "purchaserStatus4",
    text: "(v) A “qualified institutional buyer” as defined in paragraph (a) of Rule 144A of the Securities Act, acting for its own account, the account of another qualified institutional buyer, or the account of a Qualified Purchaser; provided that (i) a dealer described in paragraph (a)(1)(ii) of Rule 144A must own and invest on a discretionary basis at least $25,000,000 in securities of issuers that are not affiliated with the dealer and (ii) a plan referred to in paragraph (a)(1)(D) of Rule 144A, or a trust fund referred to in paragraph (a)(1)(F) of Rule 144A that holds the assets of such a plan, will not be deemed to be acting for its own account if investment decisions with respect to the plan are made by the beneficiaries of the plan, except with respect to investment decisions made solely by the fiduciary, trustee or sponsor of such plan.",
  },
  {
    fieldName: "purchaserStatus5",
    text: "(vi) Any natural person who is deemed to be a “knowledgeable employee” of the Fund as such term is defined in Rule 3c-5(a)(4) of the Investment Company Act.",
  },
  {
    fieldName: "purchaserStatus6",
    text: "(vii) Any person (“Transferee”) who acquires Interests from a person (“Transferor”) that is (or was) a Qualified Purchaser other than the Fund, provided that the Transferee is: (i) the estate of the Transferor; (ii) a person who acquires the Interests as a gift or bequest pursuant to an agreement relating to a legal separation or divorce; or (iii) a company established by the Transferor exclusively for the benefit of (or owned exclusively by) the Transferor and the persons specified in this paragraph.",
  },
  {
    fieldName: "purchaserStatus7",
    text: "(viii) Any entity in which each of the beneficial owners of its securities is a Qualified Purchaser",
  },
  {
    fieldName: "purchaserStatus8",
    text: "(ix) None of the above apply.",
    options: {
      // This only takes effect if isQualifiedPurchaserDeal is also set
      rejectIfSelected: true,
      rejectIfSelectedMessage:
        "If none of the above options apply you will not be able to invest in this deal.  Please contact your organizer.",
    },
  },
];
const section2Checkboxes: OneOfCheckbox[] = [
  {
    fieldName: "purchaserStatus9",
    text: "All beneficial owners of the Excepted Investment Company’s outstanding securities (other than short-term paper), determined in accordance with Section 3(c)(1)(A) under the Investment Company Act, that acquired such securities on or before April 30, 1996 (hereafter in this paragraph referred to as “pre-amendment beneficial owners”), and all pre-amendment beneficial owners of the outstanding securities (other than short-term paper) of any Excepted Investment Company that, directly or indirectly, owns any outstanding securities of such Excepted Investment Company, have consented to its treatment as a Qualified Purchaser.",
    options: {
      optional: true,
    },
  },
  {
    fieldName: "purchaserStatus10",
    text: "The Excepted Investment Company was formed after April 30, 1996.",
    options: {
      optional: true,
    },
  },
];

export function getPurchaserStatusMeta(
  isQualifiedPurchaserDeal: boolean = false
) {
  const allOptions = section1Checkboxes.concat(section2Checkboxes);

  // On a QP deal, one of the first 8 must be selected
  // None of the above is INVALID for a QP Deal
  // On a generic deal, any of the first 9 options are ok
  // Including None of the above apply
  const activateRejections = isQualifiedPurchaserDeal;
  return getMeta(allOptions, activateRejections);
}

export default function SubformPurchaserStatus() {
  const header =
    "Subscriber makes one or more of the following representations regarding Subscriber’s status as a “Qualified Purchaser” (as defined in Section 2(a)(51) of the Investment Company Act and the rules thereunder), and has checked and signed the applicable representation:";
  const section2Header =
    "An entity that would be defined as an investment company under the Investment Company Act but for the exception from that definition provided under Section 3(c)(1) or 3(c)(7) of the Investment Company Act (an “Excepted Investment Company”) MUST complete the additional certification below:";

  return (
    <SubformOneOfCheckboxes
      header={header}
      section1Checkboxes={section1Checkboxes}
      section2Header={section2Header}
      section2Checkboxes={section2Checkboxes}
    />
  );
}

interface PurchaserStatusStepProps {
  stepNumber: number;
  totalSections: number;
  submitting: boolean;
  onHandleSection: (number: number) => void;
  isValid: boolean;
}

export function PurchaserStatusStep({
  stepNumber,
  submitting,
  onHandleSection,
  isValid,
}: PurchaserStatusStepProps) {
  return (
    <>
      <Header as="h3" inverted content="Qualified Purchaser" />
      <Divider />
      <SubformPurchaserStatus />
      <ProfileWizardActions
        currentStep={stepNumber}
        goToStepHandler={onHandleSection}
        nextDisabled={!isValid || submitting}
        submitting={submitting}
        canSubmitForm={isValid}
      />
    </>
  );
}
