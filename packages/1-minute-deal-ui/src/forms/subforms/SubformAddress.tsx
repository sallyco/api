import React, { useRef, useState } from "react";
import { fetchSuggestions } from "../../slices/locationServiceSlice";

//import { RootState } from "../../rootReducer";

import * as Yup from "yup";
import { FormField } from "../common/FormField";
import { Divider, Form, Search } from "semantic-ui-react";
import SubformState from "./SubformState";
import SubformCountry from "./SubformCountry";
import { isValidZip } from "../validation/ValidationHelper";

export const validationSchema = () => {
  return {
    address1: Yup.string()
      .min(2)
      .max(1024)
      .required("Address Line 1 is Required"),
    address2: Yup.string().max(1024),
    city: Yup.string()

      .min(2)
      .max(1024)
      .required("City is Required"),
    state: Yup.string()

      .min(2)
      .max(1024)
      .required("State is Required"),
    postalCode: Yup.string()

      .min(2)
      .max(1024)
      .required("Postal Code is Required")
      .when("country", (country, schema) => {
        if (country === "United States of America") {
          return isValidZip(schema);
        }
        return schema;
      }),
    country: Yup.string().min(2).max(1024).required("Country is Required"),
  };
};
export const defaultValues = {
  address1: "",
  address2: "",
  city: "",
  state: "",
  postalCode: "",
  country: "United States of America",
  fixedCountry: false,
};

export default function SubformAddress({
  setFieldValue,
  values,
  fieldsRequired = true,
  namespace = "",
  ...props
}) {
  //const { isLoading, suggestions } = useSelector(
  //  (state: RootState) => state.locationService
  //);

  //var debounce = (callback, wait = 250) => {
  //  let timer;
  //  return (...args) => {
  //    clearTimeout(timer);
  //    timer = setTimeout(() => callback(...args), wait);
  //  };
  //};

  /**
      <Search
        input={{
          fluid: true,
          icon: "search",
          iconPosition: "left",
        }}
        loading={isLoading}
        placeholder="Start typing to search for address"
        fluid
        onSearchChange={debounce(
          (e, { value }) => value && dispatch(fetchSuggestions(value))
        )}
        onResultSelect={(e, { result }) => {
          const addr = result.data.address;
          setFieldValue("address1", `${addr.houseNumber} ${addr.street}`);
          setFieldValue("address2", addr.unit);
          setFieldValue("city", addr.city);
          setFieldValue("state", addr.state);
          setFieldValue("postalCode", addr.postalCode);
          setFieldValue("country", addr.country);
          setFieldValue("countryCode", result.data.countryCode);
        }}
        results={suggestions.map((elm) => ({
          title: elm.label,
          id: elm.locationId,
          data: elm,
        }))}
      />
      <Divider hidden />
**/

  // Namespacing logic
  let prefix = "";
  let country = values?.country ?? "";
  if (namespace) {
    prefix = namespace + ".";
    country = values?.[namespace].country;
  }

  return (
    <>
      <FormField
        id={`${prefix}address1`}
        name={`${prefix}address1`}
        label="Address"
        placeholder="Address 1"
        required={fieldsRequired}
      />
      <FormField
        id={`${prefix}address2`}
        name={`${prefix}address2`}
        placeholder="Address 2"
      />
      <Form.Group widths="equal">
        <FormField
          id={`${prefix}city`}
          name={`${prefix}city`}
          placeholder="City"
          required={fieldsRequired}
        />
        <SubformState
          name={`${prefix}state`}
          id={`${prefix}state`}
          country={country}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id={`${prefix}postalCode`}
          name={`${prefix}postalCode`}
          placeholder="Postal Code"
          required={fieldsRequired}
        />
        <SubformCountry
          name={`${prefix}country`}
          id={`${prefix}country`}
          disabled={values.fixedCountry}
        />
      </Form.Group>
    </>
  );
}
