import React from "react";
import * as Yup from "yup";
import { FormField } from "../common/FormField";

export const validationSchema = ({ isRequired, namePrefix = "" }) => {
  const fieldName = `${namePrefix}email`;
  const val = Yup.string().email("Email is not in the correct format");
  return isRequired
    ? { [fieldName]: val.required("Email is Required") }
    : { [fieldName]: val };
};

export const defaultValues = {};

interface Props {
  label?: string;
  namePrefix?: string;
  [x: string]: any;
}
const SubformEmail: React.FC<Props> = ({
  label = "Email",
  namePrefix = "",
  ...props
}: Props) => {
  return (
    <FormField
      label={label}
      id={`${namePrefix}email`}
      name={`${namePrefix}email`}
      placeholder="email@example.com"
      autoComplete={"email"}
      data-testid={"email"}
      {...props}
    />
  );
};
export default SubformEmail;
