import React from "react";
import * as Yup from "yup";
import { FormField } from "../common/FormField";
export const validationSchema = ({ isRequired }) => {
  const first = Yup.string().max(64);

  const last = Yup.string().max(64);

  return isRequired
    ? { firstName: first.required(), lastName: last.required() }
    : { firstName: first, lastName: last };
};

export const defaultValues = {
  firstName: "",
  lastName: "",
};
interface SubformFirstLastNameProps {
  namePrefix?: string;
  [prop: string]: any;
}

const SubformFirstLastName: React.FC<SubformFirstLastNameProps> = ({
  namePrefix = "",
  ...props
}) => {
  return (
    <>
      <FormField
        id={`${namePrefix}firstName`}
        name={`${namePrefix}firstName`}
        label="First Name"
        placeholder="First Name"
        data-testid={"firstName"}
        {...props}
      />
      <FormField
        id={`${namePrefix}lastName`}
        name={`${namePrefix}lastName`}
        label="Last Name"
        placeholder="Last Name"
        data-testid={"lastName"}
        {...props}
      />
    </>
  );
};
export default SubformFirstLastName;
