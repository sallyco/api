import React from "react";
import { FormField } from "../common/FormField";
import { Form } from "semantic-ui-react";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";

interface SubformCityStateProps {
  country?: string;
  label?: string;
  name?: string;
  id?: string;
  placeholder?: string;
  disabled?: boolean;
}

const getISOCompatibleCountryName = (country) => {
  if (country === "United States") {
    return "United States of America";
  }
  return country;
};

const SubformState = function ({
  country,
  label,
  id,
  name,
  placeholder,
  disabled,
  ...props
}: Partial<SubformCityStateProps>) {
  const selectedCountry = getCountries().find((item) => {
    return item.name === getISOCompatibleCountryName(country);
  });

  return (
    <>
      <FormField
        name={name ?? "state"}
        id={id ?? "state"}
        label={label}
        {...(selectedCountry
          ? {
              component: Form.Select,
              options: (country ? getStates(selectedCountry?.code) : []).map(
                (item) => {
                  return {
                    key: item,
                    value: item,
                    text: item,
                  };
                }
              ),
            }
          : {})}
        placeholder={
          country === "United States of America"
            ? "Select a State"
            : "State/Province/Territory"
        }
        disabled={disabled}
        search={startsWithSearch}
        {...props}
      />
    </>
  );
};
export default SubformState;
