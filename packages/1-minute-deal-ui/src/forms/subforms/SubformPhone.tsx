import React from "react";
import * as Yup from "yup";
import { MaskedFormField } from "../common/FormField";

export const validationSchema = ({ isRequired, namePrefix = "" }) => {
  const fieldName = `${namePrefix}phone`;
  const val = Yup.string()
    .transform(function (value, originalvalue) {
      return originalvalue.replace(/\D+/g, "");
    })
    .min(10, "Phone is incomplete");

  return isRequired
    ? { [fieldName]: val.required("Phone is Required") }
    : { [fieldName]: val };
};

export const defaultValues = {
  phone: "",
};

interface Props {
  label?: string;
  required?: boolean;
  namePrefix?: string;
}
export default function SubformPhone({
  label = "Phone",
  namePrefix = "",
  ...props
}: Props) {
  return (
    <MaskedFormField
      id={`${namePrefix}phone`}
      name={`${namePrefix}phone`}
      label={label}
      autocomplete={"tel"}
      format={(val) => {
        if (val.length === 10) {
          return (
            "(" +
            val.substring(0, 3) +
            ") " +
            val.substring(3, 6) +
            " " +
            val.substring(6)
          );
        } else if (val.length > 10) {
          return (
            "+" +
            val.substring(0, val.length - 10) +
            " " +
            val.substring(val.length - 10, val.length - 7) +
            " " +
            val.substring(val.length - 7, val.length - 4) +
            " " +
            val.substring(val.length - 4)
          );
        } else {
          return val;
        }
      }}
      placeholder="Phone Number"
      {...props}
    />
  );
}
