import React, { useEffect } from "react";
import { FormField } from "../common/FormField";
import { Form } from "semantic-ui-react";
import { getCountries } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";
import { toast } from "react-toastify";

export interface SubformCountryProps {
  label?: string;
  name?: string;
  id?: string;
  placeholder?: string;
  disabled?: boolean;
}

const SubformCountry = function ({
  label,
  id,
  name,
  placeholder,
  disabled,
  ...props
}: Partial<SubformCountryProps>) {
  useEffect(() => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        const countries = getCountries();
        const foundCountry = countries.find(
          (el) =>
            el?.code?.toLowerCase() === response?.countryCode?.toLowerCase()
        );
        props["value"] = foundCountry?.name || null;
      })
      .catch((err) => {
        toast(err);
      });
  }, []);

  return (
    <>
      <FormField
        name={name ?? "country"}
        id={id ?? "country"}
        component={Form.Select}
        label={label}
        options={getCountries().map((item) => {
          return {
            key: item.code,
            value: item.name,
            text: item.name,
          };
        })}
        placeholder={placeholder ?? "Select a Country"}
        disabled={disabled}
        search={startsWithSearch}
        {...props}
      />
    </>
  );
};
export default SubformCountry;
