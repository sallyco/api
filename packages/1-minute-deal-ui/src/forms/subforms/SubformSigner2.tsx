import React, { useState } from "react";
import * as Yup from "yup";
import { Form, Divider, Segment } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";
import {
  isValidZip,
  TaxIdValidationSchema,
} from "../validation/ValidationHelper";

export const validationSchema = () => {
  return {
    secondarySignatoryfirstName: Yup.string().max(64),
    secondarySignatorylastName: Yup.string().max(64),
    secondarySignatorytitle: Yup.string()

      .min(2)
      .max(64),

    secondarySignatoryemail: Yup.string().email(
      "Email is not in the correct format"
    ),

    secondarySignatoryphone: Yup.string()

      .transform(function (value, originalvalue) {
        return originalvalue.replace(/\D+/g, "");
      })
      .length(10, "Phone is incomplete"),

    secondarySignatoryaddress1: Yup.string()

      .min(2)
      .max(1024),
    secondarySignatoryaddress2: Yup.string().max(1024),
    secondarySignatorycity: Yup.string()

      .min(2)
      .max(1024),
    secondarySignatorystate: Yup.string()

      .min(2)
      .max(1024),
    secondarySignatorypostalCode: Yup.string()

      .min(2)
      .max(1024)
      .when("secondarySignatorycountry", (country, schema) => {
        if (country === "United States of America") {
          return isValidZip(schema);
        }
        return schema;
      }),
    secondarySignatorycountry: Yup.string().min(2).max(1024),
    secondarySignatoryisUSBased: Yup.string(),
    secondarySignatorytaxId: TaxIdValidationSchema(),
  };
};

export const defaultValues = {
  secondarySignatorytitle: "",
  secondarySignatoryfirstName: "",
  secondarySignatorylastName: "",
  secondarySignatoryphone: "",

  secondarySignatoryaddress1: "",
  secondarySignatoryaddress2: "",
  secondarySignatorycity: "",
  secondarySignatorystate: "",
  secondarySignatorypostalCode: "",
  secondarySignatorycountry: "United States of America",

  secondarySignatoryisUSBased: "Yes",
  secondarySignatorytaxId: "",
};

export default function SubformSigner1({ setFieldValue, values, ...props }) {
  const [showTaxId, setShowTaxId] = useState(false);

  return (
    <>
      <Form.Group widths="equal">
        <FormField
          id="secondarySignatoryfirstName"
          name="secondarySignatoryfirstName"
          label="First Name"
          placeholder="First Name"
        />
        <FormField
          id="secondarySignatorylastName"
          name="secondarySignatorylastName"
          label="Last Name"
          placeholder="Last Name"
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="secondarySignatorytitle"
          name="secondarySignatorytitle"
          label="Title"
          placeholder="Title"
        />
        <MaskedFormField
          id="secondarySignatoryphone"
          name="secondarySignatoryphone"
          label={"Phone"}
          format="(###) ###-####"
          placeholder="Phone Number"
        />
      </Form.Group>
      <FormField
        label={"Email"}
        id="secondarySignatoryemail"
        name="secondarySignatoryemail"
        placeholder="email@example.com"
      />

      <Divider />

      <FormField
        id="secondarySignatoryaddress1"
        name="secondarySignatoryaddress1"
        label="Address"
        placeholder="Address 1"
      />
      <FormField
        id="secondarySignatoryaddress2"
        name="secondarySignatoryaddress2"
        placeholder="Address 2"
      />
      <Form.Group widths="equal">
        <FormField
          id="secondarySignatorycity"
          name="secondarySignatorycity"
          placeholder="City"
        />
        <FormField
          name={"secondarySignatorystate"}
          id={"secondarySignatorystate"}
          component={Form.Select}
          options={(values.secondarySignatorycountry
            ? getStates(
                getCountries().find((item) => {
                  return item.name === values.country;
                })?.code
              )
            : []
          ).map((item) => {
            return {
              key: item,
              value: item,
              text: item,
            };
          })}
          placeholder={"Select a State"}
          search={startsWithSearch}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="secondarySignatorypostalCode"
          name="secondarySignatorypostalCode"
          placeholder="Postal Code"
        />
        <FormField
          name={"secondarySignatorycountry"}
          id={"secondarySignatorycountry"}
          component={Form.Select}
          options={getCountries().map((item) => {
            return {
              key: item.code,
              value: item.name,
              text: item.name,
            };
          })}
          placeholder={"Select a Country"}
          search={startsWithSearch}
        />
      </Form.Group>

      <Divider />

      <Form.Group widths="equal">
        <Form.Field width={4}>
          <Segment basic inverted className="ui no-padding">
            Are you a US person?
          </Segment>
          <Form.Group>
            <FormField
              label="Yes"
              component={Form.Radio}
              name="secondarySignatoryisUSBased"
              value="Yes"
            />
            <FormField
              label="No"
              component={Form.Radio}
              name="secondarySignatoryisUSBased"
              value="No"
            />
          </Form.Group>
        </Form.Field>

        <MaskedFormField
          id="secondarySignatorytaxId"
          name="secondarySignatorytaxId"
          width={12}
          data-testid={"taxId"}
          autoComplete="off"
          label={
            <LabelwithTooltip
              labelText={
                values.secondarySignatoryisUSBased === "Yes"
                  ? "Social Security Number (SSN)"
                  : "Individual Taxpayer Identification Number (ITIN)"
              }
              popupText={
                values.secondarySignatoryisUSBased === "Yes"
                  ? "Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                  : "An ITIN is issued by the IRS to persons (both residents and nonresidents of the US) who are not eligible for an SSN, but still need to file taxes and reports with the IRS."
              }
              iconName="question circle"
              iconColor="green"
            />
          }
          type={showTaxId ? "text" : "password"}
          format={
            values.secondarySignatoryisUSBased === "Yes"
              ? "###-##-####"
              : "9##-##-####"
          }
          placeholder={
            values.secondarySignatoryisUSBased === "Yes"
              ? "123-54-6789"
              : "987-56-4321"
          }
          action={{
            icon: showTaxId ? "eye slash" : "eye",
            onClick: () => setShowTaxId(!showTaxId),
            type: "button",
          }}
        />
      </Form.Group>
    </>
  );
}
