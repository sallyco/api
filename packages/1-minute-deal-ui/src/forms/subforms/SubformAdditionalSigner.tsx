import React, { useState } from "react";
import * as Yup from "yup";
import { Form, Divider, Header } from "semantic-ui-react";
import { MaskedFormField, FormField } from "../common/FormField";
import { LabelwithTooltip } from "../../components/common/LabelwithTooltip";
import { getCountries, getStates } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";
import { IdentificationTypes } from "../../tools/enums";
import {
  isValidZip,
  TaxIdValidationSchema,
} from "../validation/ValidationHelper";
import SubformAddress from "./SubformAddress";

export const validationSchema = () => {
  return {
    additionalSignatoryfirstName: Yup.string().max(64),
    additionalSignatorylastName: Yup.string().max(64),
    additionalSignatorytitle: Yup.string()

      .min(2)
      .max(64),

    additionalSignatoryemail: Yup.string().email(
      "Email is not in the correct format"
    ),

    additionalSignatoryphone: Yup.string()

      .transform(function (value, originalvalue) {
        return originalvalue.replace(/\D+/g, "");
      })
      .length(10, "Phone is incomplete"),

    additionalSignatoryaddress1: Yup.string()

      .min(2)
      .max(1024),
    additionalSignatoryaddress2: Yup.string().max(1024),
    additionalSignatorycity: Yup.string()

      .min(2)
      .max(1024),
    additionalSignatorystate: Yup.string()

      .min(2)
      .max(1024),
    additionalSignatorypostalCode: Yup.string()

      .min(2)
      .max(1024)
      .when("additionalSignatorycountry", (country, schema) => {
        if (country === "United States of America") {
          return isValidZip(schema);
        }
        return schema;
      }),
    additionalSignatorycountry: Yup.string()

      .min(2)
      .max(1024),
    additionalSignatoryisUSBased: Yup.string(),
    additionalSignatorytaxId: TaxIdValidationSchema(),
  };
};

export const defaultValues = {
  additionalSignatorytitle: "",
  additionalSignatoryfirstName: "",
  additionalSignatorylastName: "",
  additionalSignatoryphone: "",
  additionalSignatoryemail: "",

  additionalSignatoryaddress1: "",
  additionalSignatoryaddress2: "",
  additionalSignatorycity: "",
  additionalSignatorystate: "",
  additionalSignatorypostalCode: "",
  additionalSignatorycountry: "United States of America",

  additionalSignatoryAddress: {
    address1: "",
    address2: "",
    city: "",
    state: "",
    postalCode: "",
    country: "United States of America",
  },
  additionalSignatoryisUSBased: "",
  additionalSignatorytaxId: "",
};

export default function SubformSigner1({ setFieldValue, values, ...props }) {
  const [showTaxId, setShowTaxId] = useState(false);

  return (
    <>
      <Form.Group widths="equal">
        <FormField
          id="additionalSignatoryfirstName"
          name="additionalSignatoryfirstName"
          label="First Name"
          placeholder="First Name"
        />
        <FormField
          id="additionalSignatorylastName"
          name="additionalSignatorylastName"
          label="Last Name"
          placeholder="Last Name"
        />
      </Form.Group>
      <Form.Group widths="equal">
        <FormField
          id="additionalSignatorytitle"
          name="additionalSignatorytitle"
          label="Title"
          placeholder="Title"
        />
        <FormField
          id="additionalSignatoryemail"
          name="additionalSignatoryemail"
          label="Email"
          placeholder="Email"
        />
      </Form.Group>

      <SubformAddress
        namespace={"additionalSignatoryAddress"}
        setFieldValue={setFieldValue}
        values={values}
      />

      {values.entityCountry === "United States of America" && (
        <>
          <Divider />

          <Form.Group widths="equal">
            <Form.Field>
              <Header as="h3" inverted>
                Signatory is a US citizen...
              </Header>
              <Form.Group>
                <FormField
                  label="Yes"
                  component={Form.Radio}
                  name="additionalSignatoryisUSBased"
                  value="Yes"
                />
                <FormField
                  label="No"
                  component={Form.Radio}
                  name="additionalSignatoryisUSBased"
                  value="No"
                />
              </Form.Group>
            </Form.Field>
          </Form.Group>

          {values.additionalSignatoryisUSBased === "Yes" && (
            <Form.Group widths="equal">
              <MaskedFormField
                id="additionalSignatorytaxId"
                name="additionalSignatorytaxId"
                data-testid={"taxId"}
                autoComplete="off"
                label={
                  <LabelwithTooltip
                    labelText="Social Security Number (SSN)"
                    popupText="Social security numbers are required to receive an EIN during entity creation. We also require them for K-1 (Form 1065) tax documents."
                    iconName="question circle"
                    iconColor="green"
                  />
                }
                type={showTaxId ? "text" : "password"}
                format="###-##-####"
                placeholder="123-54-6789"
                action={{
                  icon: showTaxId ? "eye slash" : "eye",
                  onClick: () => setShowTaxId(!showTaxId),
                  type: "button",
                }}
              />
            </Form.Group>
          )}
        </>
      )}
    </>
  );
}
