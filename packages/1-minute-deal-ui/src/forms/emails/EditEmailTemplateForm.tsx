import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button, Divider, Form, Loader } from "semantic-ui-react";
import { Formik } from "formik";
import _ from "lodash";
import * as Yup from "yup";

import {
  fetchEmailTemplateByKey,
  updateEmailTemplateById,
} from "../../slices/emailTemplatesSlice";
import { RootState } from "../../rootReducer";
import { FormField } from "../common/FormField";

export const EditEmailTemplateForm = () => {
  const dispatch = useDispatch();
  const { emailTemplateKey, emailTemplate, state } = useSelector(
    (state: RootState) => state.emailTemplates
  );

  const [validationShape, setValidationShape] = useState({});

  useEffect(() => {
    if (!emailTemplate || emailTemplate.key !== emailTemplateKey) {
      dispatch(fetchEmailTemplateByKey(emailTemplateKey));
    }
  }, [emailTemplate, emailTemplateKey, dispatch]);

  useEffect(() => {
    if (emailTemplate) {
      setValidationShape({
        ..._.mapValues(emailTemplate.data, (value, key) => {
          return Yup.string().trim().required();
        }),
      });
    }
  }, [emailTemplate, emailTemplateKey, dispatch]);

  function onSubmit(data) {
    dispatch(
      updateEmailTemplateById({
        id: emailTemplate.id,
        data: {
          ...data,
        },
      })
    );
  }

  const prettyKey = (key) => {
    key = key.replace(/([a-z])([A-Z])/g, "$1 $2");
    key = key.replace(/([A-Z])([A-Z][a-z])/g, "$1 $2");
    key = key[0].toUpperCase() + key.substr(1);
    return key;
  };

  const validation = Yup.object().shape(validationShape);

  return (
    <>
      {["IDLE", "LOADING"].includes(state) ? (
        <Loader />
      ) : (
        <Formik
          initialValues={emailTemplate.data}
          enableReinitialize={false}
          validationSchema={validation}
          onSubmit={onSubmit}
        >
          {(props) => (
            <Form onSubmit={props.handleSubmit} className="dark-labels">
              <Divider hidden />
              {Object.entries(emailTemplate.data).map((entry) => (
                <Form.Group widths={"16"} key={entry[0]}>
                  <FormField
                    width={"16"}
                    name={`${entry[0]}`}
                    label={prettyKey(entry[0])}
                    placeholder={prettyKey(entry[0])}
                    value={entry[1]}
                    required
                  ></FormField>
                </Form.Group>
              ))}
              <Divider hidden />

              <Button
                disabled={!(props.dirty && props.isValid)}
                secondary
                fluid
                content="Save Changes"
                type="submit"
                loading={["IDLE", "LOADING", "SUBMITTING"].includes(state)}
              />
            </Form>
          )}
        </Formik>
      )}
    </>
  );
};
