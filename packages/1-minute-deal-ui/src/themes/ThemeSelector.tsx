import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { useDispatch, useSelector } from "react-redux";
import { TenantContext } from "../contexts";
import { Loader, Divider } from "semantic-ui-react";
import { IntercomProvider } from "react-use-intercom";
import { fetchTenantById } from "../slices/tenantSlice";
import { RootState } from "../rootReducer";
import axios from "axios";
import ThemeResolver from "./ThemeResolver";
import muiTheme from "./MaterialUITheme";
import { ThemeProvider, StyledEngineProvider } from "@mui/material";

const ThemeSelector = ({ children }: { children: any }) => {
  const dispatch = useDispatch();
  const tenant = useSelector((state: RootState) => state.tenant.tenant);
  const [tenantId, setTenantId] = useState<string | undefined>(undefined);

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const code = urlParams.get("code");
  const [checkSSO, setCheckSSO] = useState(false);

  useEffect(() => {
    async function getTenantId() {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const urlTheme = urlParams.get("theme");
      let tid: string | undefined = undefined;

      if (
        window.location.host.includes("glassboardtech") ||
        window.location.host.includes("localhost")
      ) {
        const parts = window.location.host.split(".");
        switch (parts[0]) {
          case "app":
          case "omd":
            tid = "gbt";
            break;
          default:
            if (window.location.host.split(".")[1] !== undefined) {
              tid = window.location.host.split(".")[0];
            } else if (urlTheme) {
              tid = urlTheme;
            }
            break;
        }
        //if (tid.indexOf("review") > -1) {
        //  tid = "gbt";
        //}
      } else {
        const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
        const url = `${baseUrl}/tenants/by-url/${window.location.host}`;
        const response = await fetch(url, { method: "GET" });
        const result = await response.json();
        if (!response.ok) {
          tid = undefined;
        }
        tid = result?.id ?? undefined;
      }
      setTenantId(tid);
      axios.defaults.headers.common["x-tenant-id"] = `${tid}`;
    }
    getTenantId();
  }, []);

  useEffect(() => {
    if (tenantId) {
      dispatch(fetchTenantById(tenantId));
    }
  }, [tenantId]);

  useEffect(() => {
    async function tryToken() {
      const protocol = window.location.protocol;

      const hn = window.location.hostname;
      let baseUrl = "api.glassboardtech.com";
      let redirectUrl = `${tenantId}.glassboardtech.com`;
      if (hn.includes("localhost")) {
        baseUrl = "localhost:8081";
        redirectUrl = `${tenantId}.localhost:3000`;
      } else if (hn.includes("dev")) {
        baseUrl = "api.dev.glassboardtech.com";
        redirectUrl = `${tenantId}.dev.glassboardtech.com`;
      } else if (hn.includes("qa")) {
        baseUrl = "api.qa.glassboardtech.com";
        redirectUrl = `${tenantId}.qa.glassboardtech.com`;
      } else if (hn.includes("staging")) {
        baseUrl = "api.sandbox.glassboardtech.com";
        redirectUrl = `${tenantId}.staging.glassboardtech.com`;
      } else if (hn.includes("sandbox")) {
        baseUrl = "api.sandbox.glassboardtech.com";
        redirectUrl = `${tenantId}.sandbox.glassboardtech.com`;
      }

      const fullurl = `${protocol}//${baseUrl}/auth/realms/${tenantId}/protocol/openid-connect/token`;
      const response = await fetch(fullurl, {
        body: new URLSearchParams({
          client_id: "account",
          grant_type: "authorization_code",
          redirect_uri: `${protocol}//${redirectUrl}`,
          code: code,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        mode: "cors",
        method: "POST",
      });
      const tokens = await response.json();
      if (!response.ok) {
        setCheckSSO(true);
        window.location.assign(
          window.location.href.replace(window.location.search, "")
        );
        return;
      }
      document.cookie = `__session=${tokens.access_token}`;
      document.cookie = `__sessionrefresh=${tokens.refresh_token}`;
      setCheckSSO(true);
      const redirectPath = Cookies.get("__postlogin-url");
      if (redirectPath) {
        document.cookie =
          "__postlogin-url= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
        window.location.assign(`/#${redirectPath}`);
      } else {
        window.location.assign(
          window.location.href.replace(window.location.search, "")
        );
      }
    }

    if (tenantId) {
      if (!code) {
        setCheckSSO(true);
      } else {
        tryToken();
      }
    }
  }, [code, tenantId]);

  const [muiThemeSettings, setMuiThemeSettings] = useState(
    muiTheme(tenant?.assets?.colors ?? {})
  );
  useEffect(() => {
    setMuiThemeSettings(muiTheme(tenant?.assets?.colors ?? {}));
  }, [tenant]);

  return (
    <>
      {!tenant || !checkSSO ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <TenantContext.Provider
          //TODO (Andrew): Yoooooooo! I heard you like hacks! Get a load of this guy! (2 of 2)
          value={{
            ...tenant,
            assets: {
              ...tenant.assets,
              images:
                window.location.hostname === "sages.variaventures.com"
                  ? {
                      logo: "https://omd-varia-sages.s3.us-east-2.amazonaws.com/shark-tank.png",
                      logoInverted:
                        "https://omd-varia-sages.s3.us-east-2.amazonaws.com/shark-tank.png",
                    }
                  : tenant.assets.images,
            },
          }}
          ////////
        >
          <IntercomProvider
            appId={tenant.id === "gbt" ? "jal85lrz" : ""}
            autoBoot={tenant.id === "gbt"}
          >
            <React.Fragment>
              <React.Suspense
                fallback={
                  <React.Fragment>
                    <div id="loader-wrapper">
                      <div className="pulsar" />
                      <div className="loading-label">
                        <Divider hidden />
                        <Loader active inline="centered" size="massive" />
                      </div>
                    </div>
                  </React.Fragment>
                }
              >
                <StyledEngineProvider injectFirst>
                  <ThemeProvider theme={muiThemeSettings}>
                    {tenant?.id && <ThemeResolver tenantId={tenantId} />}
                    {tenant && tenant.id && children}
                  </ThemeProvider>
                </StyledEngineProvider>
              </React.Suspense>
            </React.Fragment>
          </IntercomProvider>
        </TenantContext.Provider>
      )}
    </>
  );
};
export default ThemeSelector;
