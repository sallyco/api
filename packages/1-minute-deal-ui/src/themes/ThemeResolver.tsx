import { Helmet } from "react-helmet";
import React from "react";
import useSWR from "swr";
import { getTenantThemeById } from "../api/tenantApi";

const ThemeResolver = ({ tenantId }: { tenantId: string }) => {
  const { data: themeData } = useSWR(
    "theme",
    () => {
      return getTenantThemeById(tenantId);
    },
    { suspense: true }
  );

  return <Helmet style={[{ cssText: themeData }]} />;
};

export default ThemeResolver;
