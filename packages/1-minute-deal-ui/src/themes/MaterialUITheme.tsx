import { createTheme } from "@mui/material/styles";

const theme = (settings: any) => {
  return createTheme({
    typography: {
      fontFamily: ["Noto Sans", "Helvetica", "Arial", "sans-serif"].join(","),
      allVariants: {
        color: "rgba(0, 0, 0, 0.87)",
      },
    },
    components: {
      MuiTextField: {
        defaultProps: {
          variant: "filled",
        },
      },
      MuiTableCell: {
        styleOverrides: {
          root: {
            fontSize: "1rem",
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            fontSize: "1rem",
          },
        },
      },
    },
    palette: {
      primary: {
        main: settings?.primaryColor ?? "#3f50b5",
      },
      secondary: {
        main: settings?.secondaryColor ?? "#f44336",
      },
    },
  });
};

export default theme;
