import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Document as DocumentPreview, Page } from "react-pdf";
import { RootState } from "../rootReducer";
import axios from "axios";
import asset from "../assets/images/loadingDocsImage.png";
import { fetchFileById } from "../slices/filesSlice";
import { toast } from "react-toastify";
import { fileIcons } from "../tools/fileIcons";
import * as parseCookie from "../api/parseCookie";
import {
  Card,
  Placeholder,
  Image,
  Label,
  Loader,
  Modal,
  Button,
  Divider,
  Message,
} from "semantic-ui-react";
import moment from "moment";
import { DownloadFile } from "../tools/downloadUtils";
import PdfDocument from "../components/PdfDocument";
import useSWR from "swr";
import { getFileById } from "../api/filesApi";

interface Props {
  fileId: string;
  size?:
    | "medium"
    | "big"
    | "small"
    | "large"
    | "mini"
    | "tiny"
    | "huge"
    | "massive"
    | undefined;
}

export default function ImageFile({ fileId, size = "medium" }: Props) {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [fileData, setFileData] = useState("");
  const token = parseCookie.getToken("__session");

  const downloadDocument = async (fileId: string) => {
    const response = await axios({
      url:
        process.env.API_URL ??
        process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return "";
    }
    return window.URL.createObjectURL(new Blob([response.data]));
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    setIsLoading(true);
    if (!fileData && !isLoading) {
      downloadFile();
    }
    setIsLoading(false);
  }, [fileId, isLoading]);

  return (
    <>
      {fileData == "" || isLoading ? (
        <Placeholder style={{ height: 150, width: 150 }}>
          <Placeholder.Image />
        </Placeholder>
      ) : (
        <Image src={fileData} bordered />
      )}
    </>
  );
}
