import React, { useContext, useEffect, useState } from "react";

export interface FeatureFlagInstance {
  name: string;
  isEnabled: boolean;
}
export const FeatureFlagsContext = React.createContext<FeatureFlagInstance[]>(
  []
);

export const useFeatureFlag = (key: string) => {
  const featureContext = React.useContext(FeatureFlagsContext);
  const [flagState, setFlagState] = useState(false);
  useEffect(() => {
    if (featureContext) {
      const flag = featureContext.find((item) => {
        return item.name === key;
      });
      setFlagState(flag?.isEnabled ?? false);
    }
  }, [key, featureContext]);
  return flagState;
};

interface FeatureFlagProps {
  name: string;
  invertedState?: boolean;
}
export const FeatureFlag: React.FC<FeatureFlagProps> = ({
  name,
  children,
  invertedState,
}) => {
  const flagState = useFeatureFlag(name);
  return <>{(invertedState ? !flagState : flagState) && children}</>;
};
