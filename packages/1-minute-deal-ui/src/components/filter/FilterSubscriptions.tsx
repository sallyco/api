import React, { useState, useEffect } from "react";
import { Icon, Button, Container, Header, Dropdown } from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";
import {
  setSubscriptionsListOpValue,
  setSubscriptionsListOpReset,
} from "../../slices/subscriptionsListOperationSlice";
import { fetchProfilesList } from "../../slices/profilesSlice";
import { RootState } from "../../rootReducer";
import { getProfileName } from "../../api/profileApiWrapper";

const FilterSubscriptions = () => {
  const dispatch = useDispatch();
  const [searchCriteria, setSearchCriteria] = useState<
    { field?: string; value?: string }[]
  >([]);
  const [profileOptions, setProfileOptions] = useState([]);

  const { opType, opFilterValue } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  useEffect(() => {
    if (opType && opFilterValue && opType === "filter") {
      setSearchCriteria(opFilterValue);
    }
  }, []);

  const selectedId = useSelector(
    (state: RootState) => state.profiles.profileManagementSelectedId
  );
  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );

  useEffect(() => {
    if (
      !profiles ||
      (Object.keys(profiles).length === 0 && profiles.constructor === Object)
    ) {
      dispatch(fetchProfilesList());
    }
  }, []);

  useEffect(() => {
    const options: any[] = Object.values(profiles)
      .filter((profile) => profile.profileType === "INVESTOR")
      .map((profile) => ({
        key: profile.id,
        value: profile.id,
        text: getProfileName(profile),
        selected: profile.id === selectedId,
        active: profile.id === selectedId,
      }));
    setProfileOptions(options);
  }, [profiles]);

  const handleUpdate = (value, field = "status") => {
    const searchVal: any = { field, value };
    const newCriteria = searchCriteria
      .filter((criteria) => criteria.field !== field)
      .concat([searchVal]);
    setSearchCriteria(newCriteria);
    dispatch(setSubscriptionsListOpValue(newCriteria));
  };

  const onClose = () => {
    dispatch(setSubscriptionsListOpReset());
    setSearchCriteria([]);
  };

  return (
    <>
      <Container textAlign={"center"} className="search-container">
        <Header as="h5" className="search-header">
          FILTER SUBSCRIPTIONS
        </Header>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" && searchInput.value === "INVITED"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("INVITED")}
        >
          Invited
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" && searchInput.value === "PENDING"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("PENDING")}
        >
          Pending
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" &&
              searchInput.value === "COMMITTED"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("COMMITTED")}
        >
          Committed
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" &&
              searchInput.value === "COMPLETED"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("COMPLETED")}
        >
          Completed
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" &&
              searchInput.value === "FUNDS_IN_TRANSIT-MANUAL"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("FUNDS_IN_TRANSIT-MANUAL")}
        >
          Funds in Transit
        </Button>

        {profiles && (
          <div style={{ maxWidth: 100, display: "inline", minHeight: "auto" }}>
            <Dropdown
              placeholder={"Investor Profile"}
              selection
              scrolling
              options={profileOptions}
              onChange={(e, { value }) => {
                return (
                  typeof value === "string" && handleUpdate(value, "profileId")
                );
              }}
              {...(searchCriteria &&
              searchCriteria.filter(
                (searchInput) => searchInput.field === "profileId"
              ).length > 0
                ? {
                    value: searchCriteria.filter(
                      (searchInput) => searchInput.field === "profileId"
                    )[0].value,
                  }
                : {})}
            />
          </div>
        )}
        <Icon
          className="search-close-icon"
          name="close"
          size="large"
          onClick={onClose}
        ></Icon>
      </Container>
    </>
  );
};

export default FilterSubscriptions;
