import React, { useState, useEffect } from "react";
import { Icon, Button, Container, Header, Dropdown } from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";

import {
  setDealsListOpValue,
  setDealsListOpReset,
} from "../../slices/dealsListOperationSlice";
import { RootState } from "../../rootReducer";
import { getProfileName } from "../../api/profileApiWrapper";
import { fetchProfilesList } from "../../slices/profilesSlice";

export const FilterInput = () => {
  const dispatch = useDispatch();
  const [searchCriteria, setSearchCriteria] = useState<
    { field?: string; value?: string }[]
  >([]);
  const [profileOptions, setProfileOptions] = useState([]);

  const handleUpdate = (value, field = "status") => {
    const searchVal: any = { field, value };
    const newCriteria = searchCriteria
      .filter((criteria) => criteria.field !== field)
      .concat([searchVal]);
    setSearchCriteria(newCriteria);
    dispatch(setDealsListOpValue(newCriteria));
  };

  const onClose = () => {
    dispatch(setDealsListOpReset());
    setSearchCriteria([]);
  };

  const selectedId = useSelector(
    (state: RootState) => state.profiles.profileManagementSelectedId
  );
  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );

  useEffect(() => {
    if (
      !profiles ||
      (Object.keys(profiles).length === 0 && profiles.constructor === Object)
    ) {
      dispatch(fetchProfilesList());
    }
  }, []);

  useEffect(() => {
    const options: any[] = Object.values(profiles)
      .filter((profile) => profile.profileType === "ORGANIZER")
      .map((profile) => ({
        key: profile.id,
        value: profile.id,
        text: getProfileName(profile),
        selected: profile.id === selectedId,
        active: profile.id === selectedId,
      }));
    setProfileOptions(options);
  }, [profiles]);

  return (
    <>
      <Container textAlign={"center"} className="search-container">
        <Header as="h5" className="search-header">
          FILTER DEALS
        </Header>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" && searchInput.value === "PENDING"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("PENDING")}
        >
          Pending
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" &&
              searchInput.value === "ONBOARDING"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("ONBOARDING")}
        >
          Onboarding
        </Button>
        <Button
          className="filter-button"
          {...(searchCriteria &&
          searchCriteria.filter(
            (searchInput) =>
              searchInput.field === "status" && searchInput.value === "CLOSED"
          ).length > 0
            ? { primary: true }
            : { basic: true })}
          size="tiny"
          onClick={() => handleUpdate("CLOSED")}
        >
          Closed
        </Button>

        {profiles && (
          <div style={{ maxWidth: 100, display: "inline", minHeight: "auto" }}>
            <Dropdown
              placeholder={"Organizer"}
              selection
              scrolling
              options={profileOptions}
              onChange={(e, { value }) => {
                return (
                  typeof value === "string" && handleUpdate(value, "profileId")
                );
              }}
              {...(searchCriteria &&
              searchCriteria.filter(
                (searchInput) => searchInput.field === "profileId"
              ).length > 0
                ? {
                    defaultValue: searchCriteria.filter(
                      (searchInput) => searchInput.field === "profileId"
                    )[0].value,
                  }
                : {})}
            />
          </div>
        )}

        <Icon
          className="search-close-icon"
          name="close"
          size="large"
          onClick={onClose}
        ></Icon>
      </Container>
    </>
  );
};

export default FilterInput;
