import { API, useRequest } from "../api/swrApi";
import { useEffect } from "react";
import { hasRequiredActions } from "../pages/RequiredAccountActions";
import { useHistory } from "react-router-dom";
import { useFeatureFlag } from "./featureflags/FeatureFlags";

const RequiredAccountActionsGate = ({ children }) => {
  const featureFlag = useFeatureFlag("required_account_actions");
  const { data: self } = useRequest(API.SELF);
  const { data: profiles } = useRequest(
    self &&
      API.ADD_FILTER(API.PROFILES, {
        where: {
          ownerId: self?.id,
        },
      })
  );
  const history = useHistory();

  useEffect(() => {
    const profilesList = (profiles?.data ?? []).filter((profile) =>
      hasRequiredActions(profile)
    );
    if (profilesList.length > 0 && featureFlag) {
      history.push(
        `/required-account-actions?return=${encodeURI(
          history.location.pathname + history.location.search
        )}`
      );
    }
  }, [self, profiles, featureFlag]);

  return children;
};

export default RequiredAccountActionsGate;
