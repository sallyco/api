import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "../../rootReducer";
import { fetchFileById } from "../../slices/filesSlice";
import { fileIcons } from "../../tools/fileIcons";

import { List, Label, Dropdown, Loader } from "semantic-ui-react";
import moment from "moment";

interface Props {
  fileId: any; //string
  size?:
    | "medium"
    | "big"
    | "small"
    | "large"
    | "mini"
    | "tiny"
    | "huge"
    | "massive"
    | undefined;
}

const DocumentItem = ({ fileId, size = "medium" }: Props) => {
  const dispatch = useDispatch();

  const file = useSelector((state: RootState) => state.files.filesById[fileId]);

  useEffect(() => {
    if (fileId) {
      if (!file) {
        dispatch(fetchFileById(fileId));
      }
    }
  }, [file, fileId]);

  return (
    <List.Item>
      {!file ? (
        <List.Content>
          <Loader active inline="centered" />
        </List.Content>
      ) : (
        <>
          <List.Icon name="file" />
          <List.Content>{file.name}</List.Content>
        </>
      )}
    </List.Item>
  );
};

export default DocumentItem;
