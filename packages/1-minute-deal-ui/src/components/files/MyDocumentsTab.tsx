import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import {
  Container,
  Card,
  Segment,
  Header,
  Icon,
  Table,
  Button,
  Grid,
} from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";
import {
  SubformFileUpload,
  FileSizes,
} from "../../forms/subforms/SubformFileUpload";
import { toast } from "react-toastify";
import { fetchSelf, updateUserById } from "../../slices/usersSlice";

export const MyDocumentsTab = () => {
  const dispatch = useDispatch();
  const myUser = useSelector((state: RootState) => state.users.self);

  useEffect(() => {
    if (!myUser) {
      dispatch(fetchSelf());
    }
  }, [dispatch, myUser]);

  return (
    <Container textAlign="center">
      <Card.Group itemsPerRow="one" stackable>
        <Card>
          <Card.Content>
            <Card.Header>
              <Grid columns={"equal"}>
                <Grid.Column textAlign="left" width={"13"}>
                  My Documents
                </Grid.Column>
                <Grid.Column textAlign="right">
                  <SubformFileUpload
                    maxSize={FileSizes.MB * 50}
                    onSuccess={async (fileIds) => {
                      const upload = myUser.attributes.files
                        ? [...myUser.attributes.files, ...fileIds]
                        : fileIds;
                      await dispatch(
                        updateUserById({
                          ...myUser,
                          attributes: {
                            ...myUser.attributes,
                            files: upload,
                          },
                        })
                      );
                      toast.success(
                        `File${
                          fileIds.length > 1 ? "s" : ""
                        } saved successfully`
                      );
                    }}
                    onError={(errors) => {
                      errors.forEach((error) => {
                        toast.error(error);
                      });
                    }}
                  >
                    {(props) => {
                      return (
                        <Icon
                          className={"primaryColor"}
                          link
                          bordered
                          circular
                          loading={props.uploading}
                          name={props.uploading ? "spinner" : "upload"}
                          size="small"
                          onClick={props.triggerFileUpload}
                        />
                      );
                    }}
                  </SubformFileUpload>
                </Grid.Column>
              </Grid>
            </Card.Header>
            {myUser?.attributes?.files?.length > 0 ? (
              <Table basic="very" compact size="small" unstackable>
                <Table.Body>
                  {myUser.attributes.files.map((fileId) => (
                    <DocumentRow key={fileId} fileId={fileId} />
                  ))}
                </Table.Body>
              </Table>
            ) : (
              <Segment placeholder basic>
                <Header icon>
                  <Icon name="file alternate outline" color="grey" circular />
                  No Documents to Display
                </Header>
              </Segment>
            )}
          </Card.Content>
        </Card>
      </Card.Group>
    </Container>
  );
};
