import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import {
  Segment,
  Header,
  Icon,
  Card,
  Transition,
  Container,
  CardContent,
} from "semantic-ui-react";
import { RootState } from "../../rootReducer";
import { Link, useHistory } from "react-router-dom";

import { fetchDealsList } from "../../slices/dealsSlice";
import { fetchSubscriptionsList } from "../../slices/subscriptionsSlice";

import DealDocsCard from "./DealDocsCard";
import NoDeals from "../deals/NoDeals";
import { Subscription } from "../../api/subscriptionsApi";

export const ByDealTab = () => {
  const dispatch = useDispatch();

  const {
    currentPageDeals,
    isLoading: isLoadingDeals,
    error: dealsError,
    dealsById,
  } = useSelector((state: RootState) => state.deals);
  const {
    currentPageSubscriptions,
    isLoading: isLoadingSubscriptions,
    error: subscriptionsError,
    subscriptionsById,
  } = useSelector((state: RootState) => state.subscriptions);

  const deals = currentPageDeals.map((dealId) => dealId);
  const subscriptions = currentPageSubscriptions
    .filter((subscriptionId) => subscriptionsById[subscriptionId])
    .map((subscriptionId) => subscriptionsById[subscriptionId].dealId);

  const dealIds = deals.concat(
    subscriptions.filter((item) => deals.indexOf(item) < 0)
  );

  useEffect(() => {
    dispatch(fetchDealsList());
    dispatch(fetchSubscriptionsList());
  }, [dispatch]);

  return (
    <>
      {deals.length + subscriptions.length > 0 ? (
        <Container text>
          <Card.Group itemsPerRow="one" stackable>
            {dealIds.length > 0 &&
              dealIds.map((dealId, dealIndex) => (
                <DealDocsCard
                  key={dealId + "deal"}
                  dealId={dealId}
                  showNoDocsMessage={dealIndex === 0}
                />
              ))}
          </Card.Group>
        </Container>
      ) : (
        <>
          <Transition animation={"fade"}>
            <Container textAlign="center">
              <Card.Group itemsPerRow="one" stackable>
                <Card>
                  <Card.Content>
                    <Segment placeholder basic>
                      <Header icon>
                        <Icon
                          name="file alternate outline"
                          color="grey"
                          circular
                        />
                        No Documents to Display
                      </Header>
                    </Segment>
                  </Card.Content>
                </Card>
              </Card.Group>
            </Container>
          </Transition>
        </>
      )}
    </>
  );
};
