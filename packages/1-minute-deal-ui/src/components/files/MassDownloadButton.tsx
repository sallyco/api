import React, { useState } from "react";
import { Icon, Popup } from "semantic-ui-react";
import { DownloadMassFiles } from "../../tools/downloadUtils";

export const DownloadMassButton: React.FC<{ files: string[] }> = ({
  files,
}) => {
  const [downloading, setDownloading] = useState(false);

  return (
    <Popup
      trigger={
        <Icon
          name={downloading ? "spinner" : "cloud download"}
          loading={downloading}
          color={"black"}
          link
          bordered
          circular
          size="small"
          onClick={async () => {
            setDownloading(true);
            await DownloadMassFiles(files);
            setDownloading(false);
          }}
        />
      }
      basic
      inverted
      content="Download All Files"
    />
  );
};
