import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { Document as DocumentPreview, Page } from "react-pdf";
import axios from "axios";
import * as parseCookie from "../../api/parseCookie";
import { deleteFiles, fetchFileById } from "../../slices/filesSlice";
import { updateFile } from "../../api/filesApi";

import { fileIcons } from "../../tools/fileIcons";

import {
  Table,
  Checkbox,
  Header,
  Dropdown,
  Loader,
  Icon,
  Modal,
  Button,
  Message,
} from "semantic-ui-react";
import moment from "moment";
import {
  DownloadFile,
  handlePDFOrDownloadFile,
} from "../../tools/downloadUtils";
import { Box, Chip, LinearProgress, Typography } from "@mui/material";

interface Props {
  fileId: any; //string
  size?:
    | "medium"
    | "big"
    | "small"
    | "large"
    | "mini"
    | "tiny"
    | "huge"
    | "massive"
    | undefined;
  sharable?: boolean;
  shareCallback?: any; //function
  documentTitle?: string;
  onDelete?: (fileId: string) => void | Promise<void>;
  canDelete?: boolean;
}

const DocumentRow = ({
  fileId,
  size = "medium",
  sharable,
  shareCallback = () => {},
  documentTitle,
  onDelete,
  canDelete = false,
}: Partial<React.PropsWithChildren<Props>>) => {
  const dispatch = useDispatch();
  const [downloading, setDownloading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [numPages, setNumPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [fileData, setFileData] = useState("");
  const [deleting, setDeleting] = useState(false);
  const [failedDownload, setFailedDownload] = useState(false);
  const [currentPercent, setCurrentPercent] = useState(0);

  const file = useSelector((state: RootState) => state.files.filesById[fileId]);
  const { error: fileError } = useSelector((state: RootState) => state.files);

  useEffect(() => {
    if (fileId && !deleting) {
      if (!file || file.id !== fileId) {
        dispatch(fetchFileById(fileId));
      }
    }
  }, [file, fileId]);

  useEffect(() => {
    if (fileError) {
      setFailedDownload(true);
    }
  }, [fileError]);

  const token = parseCookie.getToken("__session");
  const downloadDocument = async (fileId: string) => {
    try {
      const response = await axios({
        url:
          process.env.API_URL ??
          process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
        method: "GET",
        responseType: "blob",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        onDownloadProgress: (progressEvent) => {
          const total = parseFloat(progressEvent.total);
          const current = progressEvent.loaded;
          let percentCompleted = Math.floor((current / total) * 100);
          setCurrentPercent(percentCompleted);
        },
      });
      if (response.status !== 200) {
        setFailedDownload(true);
        return "";
      }
      return window.URL.createObjectURL(new Blob([response.data]));
    } catch (e) {
      setFailedDownload(true);
      return "";
    }
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    if (file && !file.generating && modalOpen) {
      downloadFile();
    }
  }, [fileId, file, modalOpen]);

  const onPage = (type) => {
    var newPage = type ? currentPage + 1 : currentPage - 1;
    if (newPage > numPages) {
      newPage = numPages;
    } else if (newPage < 1) {
      newPage = 1;
    }
    setCurrentPage(newPage);
  };

  const options = [
    {
      key: "download",
      text: "Download",
      value: "download",
      onClick: async () => {
        setDownloading(true);
        await DownloadFile(file?.id, file?.name);
        setDownloading(false);
      },
    },
    {
      key: "delete",
      text: "Delete",
      value: "delete",
      disabled: !canDelete,
      onClick: async () => {
        if (!canDelete) {
          return;
        }
        await deleteFile(fileId);
      },
    },
    //    { key: 'hide', icon: 'hide', text: 'Hide Post', value: 'hide' },
  ];

  const previewFile = () => {
    setModalOpen(true);
  };

  const handleDownload = () => {
    handlePDFOrDownloadFile(file, previewFile);
  };

  const deleteFile = async (fileId) => {
    try {
      setDeleting(true);
      if (typeof onDelete === "function") {
        onDelete(fileId);
      }
      await dispatch(deleteFiles([fileId]));
      setDeleting(false);
    } catch (err) {
      setDeleting(false);
    }
  };

  return (
    <>
      {!file ? (
        <Table.Row>
          <Table.Cell>
            <Loader active inline="centered" />
          </Table.Cell>
          <Table.Cell>
            {failedDownload && (
              <>
                Failed to Download File <Chip label={`Ref: ${fileId}`} />
              </>
            )}
          </Table.Cell>
          <Table.Cell textAlign="right">
            <Dropdown
              className="icon"
              icon={
                <Icon
                  name={downloading ? "spinner" : "ellipsis vertical"}
                  loading={downloading}
                />
              }
              floating
              direction="left"
              options={options.filter((option) => option.key !== "download")}
              trigger={<React.Fragment />}
            />
          </Table.Cell>
        </Table.Row>
      ) : (
        <Table.Row>
          <Table.Cell
            onClick={handleDownload}
            style={{ cursor: "pointer" }}
            width={1}
          >
            {fileIcons(file)}
          </Table.Cell>
          <Table.Cell onClick={handleDownload} style={{ cursor: "pointer" }}>
            <Header as="h5">
              {file.name}
              <Header.Subheader>
                {documentTitle && <b>{documentTitle} - </b>}
                {moment(file.createdAt).format("ll")}
              </Header.Subheader>
            </Header>
          </Table.Cell>
          <Table.Cell textAlign="right">
            <Dropdown
              className="icon"
              icon={
                <Icon
                  name={downloading ? "spinner" : "ellipsis vertical"}
                  loading={downloading}
                />
              }
              floating
              direction="left"
              options={options}
              trigger={<React.Fragment />}
            />
          </Table.Cell>
          {sharable && (
            <Table.Cell width={1}>
              <Header as={"h4"} content={"Public"} />
              <Checkbox
                checked={file.isPublic}
                onChange={() => {
                  updateFile({ ...file, isPublic: !file.isPublic }).then(() => {
                    shareCallback();
                    dispatch(fetchFileById(file.id));
                  });
                }}
              />
            </Table.Cell>
          )}
        </Table.Row>
      )}
      {file && (
        <Modal
          onClose={() => setModalOpen(false)}
          onOpen={() => setModalOpen(true)}
          open={modalOpen}
          size="small"
        >
          <Modal.Header>{file?.name}</Modal.Header>
          <Modal.Content style={{ display: "flex" }}>
            {file && currentPercent !== 100 && (
              <Box width={"100%"}>
                <Box mx={2}>
                  <LinearProgress
                    variant="determinate"
                    color={"primary"}
                    value={currentPercent}
                  />
                </Box>
                <Box textAlign="center">
                  <Typography variant="overline" display="block">
                    Loading ({currentPercent}%)
                  </Typography>
                </Box>
              </Box>
            )}
            {file && currentPercent === 100 && (
              <div style={{ margin: "auto" }}>
                <DocumentPreview
                  file={fileData}
                  onLoadSuccess={({ numPages }) => setNumPages(numPages)}
                  loading={
                    <Loader active inverted inline content="Loading File..." />
                  }
                  error={
                    <Message negative>
                      Failed to load file. Please refresh this page
                    </Message>
                  }
                >
                  <Page pageNumber={currentPage} width={500} />
                </DocumentPreview>
              </div>
            )}
          </Modal.Content>
          <Modal.Actions>
            <Button disabled={currentPage <= 1} onClick={() => onPage(0)}>
              Previous
            </Button>
            <Button
              disabled={currentPage >= numPages}
              onClick={() => onPage(1)}
            >
              Next
            </Button>
            <Button onClick={() => setModalOpen(false)}>Close</Button>
            <Button
              content="Download"
              labelPosition="right"
              icon="download"
              onClick={() => DownloadFile(file?.id, file?.name)}
              positive
            />
          </Modal.Actions>
        </Modal>
      )}
    </>
  );
};

export default DocumentRow;
