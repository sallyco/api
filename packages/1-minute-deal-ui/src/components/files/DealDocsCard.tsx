import React, { useCallback, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { RootState } from "../../rootReducer";
import { fetchDealById, updateDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { fetchSubscriptionsList } from "../../slices/subscriptionsSlice";
import {
  fetchClosesListByDeal,
  fetchClosesList,
} from "../../slices/closesSlice";

import Log from "../../tools/Log";

import {
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Table,
  Button,
  Label,
  Container,
  Segment,
  Item,
  Transition,
  Loader,
  Card,
  Dropdown,
  Popup,
} from "semantic-ui-react";
import DocumentRow from "../../components/files/DocumentRow";

import { DealStatusColors as dsc } from "../../tools/statusColors";
import NumberFormat from "react-number-format";
import moment from "moment";
import { DownloadFile, DownloadMassFiles } from "../../tools/downloadUtils";
import { DownloadMassButton } from "./MassDownloadButton";

interface Props {
  dealId: string;
  showNoDocsMessage?: boolean;
}

const DealDocsCard = ({ dealId, showNoDocsMessage }: Props) => {
  const dispatch = useDispatch();
  const [requested, setRequested] = useState(false);
  const [requestedC, setRequestedC] = useState(false);
  const [validCloses, setValidCloses] = useState([]);
  const [downloadingFundDocuments, setDownloadingFundDocuments] =
    useState(false);

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const { isLoading: entitiesLoading, error: entitiesError } = useSelector(
    (state: RootState) => state.entities
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dealId]);

  useEffect(() => {
    if (deal?.entityId && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, entity]);

  //SUBSCRIPTIONS
  const {
    currentPageSubscriptions,
    subscriptionsByDealId,
    subscriptionsById,
    isLoading,
    error,
  } = useSelector((state: RootState) => state.subscriptions);

  const subscriptions1 = currentPageSubscriptions.map(
    (subscriptionID) => subscriptionsById[subscriptionID]
  );

  const subscriptions = Object.values(subscriptions1 ?? {}).filter(
    (p) => p.isDocsSigned === true && p.dealId === dealId
  );

  useEffect(() => {
    if (dealId && !(dealId in subscriptionsByDealId) && !requested) {
      dispatch(fetchSubscriptionsList());
      setRequested(true);
    }
  }, [dealId, subscriptions, dispatch]);

  //Closes
  const {
    currentPageCloses,
    currentPageDealId: closePage,
    closesById,
  } = useSelector((state: RootState) => state.closes);

  useEffect(() => {
    const closes = currentPageCloses.map((closeID) => closesById[closeID]);
    const valid = Object.values(closes ?? {}).filter(
      (p) => p.files && p.dealId === dealId && p.fundManagerSigned
    );
    setValidCloses(valid);
  }, [currentPageCloses]);

  useEffect(() => {
    if (dealId && closePage !== dealId && !requestedC) {
      dispatch(fetchClosesList());
      setRequestedC(true);
    }
  }, [dealId, dispatch]);

  return (
    <>
      {" "}
      {deal || subscriptions.length > 0 || validCloses.length > 0 ? (
        <Card>
          <Card.Content>
            <Card.Header>
              <Grid columns="equal">
                <Grid.Column width={13}>{deal?.name}</Grid.Column>
              </Grid>
            </Card.Header>
          </Card.Content>
          {!entity?.entityDocuments && !deal?.files ? (
            <Card.Content>
              <Header as="h5">No Documents</Header>
            </Card.Content>
          ) : (
            <Card.Content>
              <>
                {entity?.entityDocuments && (
                  <>
                    <Grid columns={2}>
                      <Grid.Column>
                        <Header as="h5">Fund Documents</Header>
                      </Grid.Column>
                      <Grid.Column textAlign={"right"}>
                        <DownloadMassButton
                          files={[
                            entity?.entityDocuments?.operatingAgreement,
                            entity?.entityDocuments?.privatePlacementMemorandum,
                            entity?.entityDocuments?.subscriptionAgreement,
                          ]}
                        />
                      </Grid.Column>
                    </Grid>
                    <Table basic="very" compact size="small" unstackable>
                      <Table.Body>
                        <DocumentRow
                          fileId={entity?.entityDocuments?.operatingAgreement}
                          documentTitle={"Operating Agreement"}
                        />
                        <DocumentRow
                          fileId={
                            entity?.entityDocuments?.privatePlacementMemorandum
                          }
                          documentTitle={"Private Placement Memorandum"}
                        />
                        <DocumentRow
                          fileId={
                            entity?.entityDocuments?.subscriptionAgreement
                          }
                          documentTitle={"Subscription Agreement"}
                        />
                      </Table.Body>
                    </Table>
                  </>
                )}
              </>
              {deal.files && deal.files.length > 0 && (
                <>
                  <Grid columns={2}>
                    <Grid.Column>
                      <Header as="h5">Deal Documents</Header>
                    </Grid.Column>
                    <Grid.Column textAlign={"right"}>
                      <DownloadMassButton files={deal.files} />
                    </Grid.Column>
                  </Grid>
                  <Table basic="very" compact size="small" unstackable>
                    <Table.Body>
                      {deal.files.map((fileId) => (
                        <DocumentRow key={fileId} fileId={fileId} />
                      ))}
                    </Table.Body>
                  </Table>
                </>
              )}
              {subscriptions && subscriptions.length > 0 && (
                <>
                  <Grid columns={2}>
                    <Grid.Column>
                      <Header as="h5">Investor Documents</Header>
                    </Grid.Column>
                    <Grid.Column textAlign={"right"}>
                      <DownloadMassButton
                        files={[].concat(
                          ...subscriptions.map((sub) => {
                            return sub.files;
                          })
                        )}
                      />
                    </Grid.Column>
                  </Grid>
                  <Table basic="very" compact size="small" unstackable>
                    <Table.Body>
                      {subscriptions.map(
                        (sub) =>
                          sub.files &&
                          sub.files.map((fileId) => (
                            <DocumentRow key={fileId} fileId={fileId} />
                          ))
                      )}
                    </Table.Body>
                  </Table>
                </>
              )}
              {validCloses && validCloses.length > 0 && (
                <>
                  <Grid columns={2}>
                    <Grid.Column>
                      <Header as="h5">Close Documents</Header>
                    </Grid.Column>
                    <Grid.Column textAlign={"right"}>
                      <DownloadMassButton
                        files={[].concat(
                          ...validCloses.map((close) => {
                            return close.files;
                          })
                        )}
                      />
                    </Grid.Column>
                  </Grid>
                  <Table basic="very" compact size="small" unstackable>
                    <Table.Body>
                      {validCloses.map(
                        (close) =>
                          close.files &&
                          close.files.map((fileId) => (
                            <DocumentRow key={fileId} fileId={fileId} />
                          ))
                      )}
                    </Table.Body>
                  </Table>
                </>
              )}
            </Card.Content>
          )}
        </Card>
      ) : (
        <>
          {showNoDocsMessage && (
            <Transition animation={"fade"}>
              <Container textAlign="center">
                <Card.Group itemsPerRow="one" stackable>
                  <Card>
                    <Card.Content>
                      <Segment placeholder basic>
                        <Header icon>
                          <Icon
                            name="file alternate outline"
                            color="grey"
                            circular
                          />
                          No Documents to Display
                        </Header>
                      </Segment>
                    </Card.Content>
                  </Card>
                </Card.Group>
              </Container>
            </Transition>
          )}
        </>
      )}
    </>
  );
};
DealDocsCard.defaultProps = {
  showNoDocsMessage: false,
};

export default DealDocsCard;
