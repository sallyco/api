import React, { useEffect } from "react";
import { Container, Card, Segment, Header, Icon } from "semantic-ui-react";

export const TaxTab = () => {
  return (
    <Container textAlign="center">
      <Card.Group itemsPerRow="one" stackable>
        <Card>
          <Card.Content>
            <Segment placeholder basic>
              <Header icon>
                <Icon name="file alternate outline" color="grey" circular />
                No Documents to Display
              </Header>
            </Segment>
          </Card.Content>
        </Card>
      </Card.Group>
    </Container>
  );
};
