import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { TenantContext } from "../contexts";
import {
  Icon,
  Grid,
  Responsive,
  Container,
  Header as Head,
  Image,
} from "semantic-ui-react";
import HeaderUserMenu from "./common/HeaderUserMenu";

const Header = ({
  showSideBar,
  showMenu = true,
}: {
  showSideBar: any;
  showMenu?: boolean;
}) => {
  const tenantProfile = useContext(TenantContext);

  return (
    <div className="ui dashboard-header">
      <Container>
        <Grid verticalAlign="middle" columns="equal" padded>
          <Grid.Row>
            <Grid.Column float="left">
              <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
                <Head>
                  <Icon
                    name="sidebar"
                    link
                    inverted
                    onClick={() => showSideBar(true)}
                  />
                  <Head.Content>
                    {tenantProfile.url ? (
                      <Link to="/dashboard">
                        <Image
                          float="left"
                          style={{
                            maxHeight: "120px",
                            width: "auto",
                          }}
                          src={
                            tenantProfile.inverted
                              ? tenantProfile.assets.images.logoInverted
                              : tenantProfile.assets.images.logo
                          }
                        />
                      </Link>
                    ) : (
                      <Image
                        float="left"
                        style={{
                          maxHeight: "120px",
                          width: "auto",
                        }}
                        src={
                          tenantProfile.inverted
                            ? tenantProfile.assets.images.logoInverted
                            : tenantProfile.assets.images.logo
                        }
                      />
                    )}
                  </Head.Content>
                </Head>
              </Responsive>
              <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                <Link to="/dashboard">
                  <Image
                    float="left"
                    style={{
                      maxHeight: "120px",
                      width: "auto",
                    }}
                    src={
                      tenantProfile.inverted
                        ? tenantProfile.assets.images.logoInverted
                        : tenantProfile.assets.images.logo
                    }
                  />
                </Link>
              </Responsive>
            </Grid.Column>
            <Grid.Column textAlign="right">
              {showMenu && <HeaderUserMenu />}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  );
};

export default Header;
