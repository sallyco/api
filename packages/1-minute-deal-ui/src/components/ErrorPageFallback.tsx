import React from "react";
import { Button, Container, Message, Segment } from "semantic-ui-react";
import { Helmet } from "react-helmet";

const ErrorPageFallback: React.FC<{
  error: any;
  componentStack: any;
  resetError: any;
}> = ({ error, componentStack, resetError }) => {
  return (
    <>
      <Helmet>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
          integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ=="
          crossOrigin="anonymous"
        />
      </Helmet>
      <Container>
        <Segment>
          <h1>You have encountered an application error</h1>
          <Message>
            This error has been reported and will be investigated
          </Message>
          {(process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) !==
            "production" && (
            <>
              <pre
                style={{
                  fontSize: "18pt",
                  lineHeight: "1.6",
                  padding: ".5rem",
                  whiteSpace: "pre-wrap",
                  width: "100%",
                  background: "#444",
                  color: "#E00",
                  borderRadius: "5px",
                }}
              >
                {error.toString()}
              </pre>
              <pre
                style={{
                  padding: ".5rem",
                  whiteSpace: "pre-wrap",
                  width: "100%",
                  background: "#333",
                  color: "#DDD",
                  borderRadius: "5px",
                }}
              >
                {componentStack}
              </pre>
            </>
          )}
          <Button
            onClick={() => {
              resetError();
            }}
          >
            Try To Reload
          </Button>
        </Segment>
      </Container>
    </>
  );
};

export default ErrorPageFallback;
