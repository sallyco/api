import React, { useState, useEffect } from "react";
import { Icon, Dropdown, Container, Header } from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";

import {
  setDealsListOpValue,
  setDealsListOpReset,
} from "../../slices/dealsListOperationSlice";
import {
  setSubscriptionsListOpReset,
  setSubscriptionsListOpValue,
} from "../../slices/subscriptionsListOperationSlice";
import { RootState } from "../../rootReducer";

export const SortInput = ({ type = "deals" }) => {
  const dispatch = useDispatch();
  const [searchInputValue, updteSearchValue] = useState("");

  const { opType, opValue } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  useEffect(() => {
    if (opType && opValue && opType === "sort") {
      updteSearchValue(opValue);
    }
  }, []);

  const sortOptions = [
    // {
    //   key: "01",
    //   value: "name-ASCENDING",
    //   icon: "times circle",
    //   text: "None",
    // },
    {
      key: "02",
      value: "name-DESCENDING",
      icon: "arrow down",
      text: "Deal-DESCENDING",
    },
    {
      key: "03",
      value: "name-ASCENDING",
      icon: "arrow up",
      text: "Deal-ASCENDING",
    },
    {
      key: "02",
      value: "targetRaiseAmount-DESCENDING",
      icon: "arrow down",
      text: "Target Raise-DESCENDING",
    },
    {
      key: "03",
      value: "targetRaiseAmount-ASCENDING",
      icon: "arrow up",
      text: "Target Raise-ASCENDING",
    },
    // {
    //   key: "04",
    //   value: "Amount Raised-Descending",
    //   icon: "arrow down",
    //   text: "Amount Raised-Descending",
    // },
    // {
    //   key: "05",
    //   value: "Amount Raised-Ascending",
    //   icon: "arrow up",
    //   text: "Amount Raised-Ascending",
    // },
    // {
    //   key: "06",
    //   value: "Investor Count-Descending",
    //   icon: "arrow down",
    //   text: "Investor Count-Descending",
    // },
    // {
    //   key: "07",
    //   value: "Investor Count-Ascending",
    //   icon: "arrow up",
    //   text: "Investor Count-Ascending",
    // },
    // {
    //   key: "ai",
    //   value: "Days to close-Soonest",
    //   icon: "calendar alternate outline",
    //   text: "Days to close-Soonest",
    // },
    // {
    //   key: "ag",
    //   value: "Days to close-Longest Out",
    //   icon: "calendar alternate outline",
    //   text: "Days to close-Longest Out",
    // },
  ];

  const subscriptionSortOptions = [
    {
      key: "02",
      value: "status-DESCENDING",
      icon: "arrow down",
      text: "Status-DESCENDING",
    },
    {
      key: "03",
      value: "status-ASCENDING",
      icon: "arrow up",
      text: "Status-ASCENDING",
    },
    {
      key: "02",
      value: "amount-DESCENDING",
      icon: "arrow down",
      text: "Amount-DESCENDING",
    },
    {
      key: "03",
      value: "amount-ASCENDING",
      icon: "arrow up",
      text: "Amount-ASCENDING",
    },
    {
      key: "02",
      value: "createdAt-DESCENDING",
      icon: "arrow down",
      text: "Subscribed On-DESCENDING",
    },
    {
      key: "03",
      value: "createdAt-ASCENDING",
      icon: "arrow up",
      text: "Subscribed On-ASCENDING",
    },
  ];

  const handleOnChange = (e, data) => {
    updteSearchValue(data.value);
    if (type === "deals") {
      dispatch(setDealsListOpValue(data.value));
    } else {
      dispatch(setSubscriptionsListOpValue(data.value));
    }
  };

  const onClose = () => {
    if (type === "deals") {
      dispatch(setDealsListOpReset());
    } else {
      dispatch(setSubscriptionsListOpReset());
    }
    updteSearchValue("");
  };
  const inputStyle = { color: "#00909A" };
  return (
    <>
      <Container textAlign={"center"} className="search-container">
        <Header as="h5" className="sort-header">
          SORT {type.toUpperCase()}
        </Header>
        <Dropdown
          placeholder="Select a Sort..."
          className="sort-drop-down"
          search
          selection
          value={searchInputValue}
          options={type === "deals" ? sortOptions : subscriptionSortOptions}
          onChange={handleOnChange}
        />
        <Icon
          className="search-close-icon"
          name="close"
          size="large"
          onClick={() => onClose()}
        ></Icon>
      </Container>
    </>
  );
};
export default SortInput;
