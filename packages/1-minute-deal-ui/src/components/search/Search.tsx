import React, { useState } from "react";
import {
  Icon,
  Input,
  Grid,
  GridColumn,
  Header,
  Container,
} from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";

import {
  setDealsListOpValue,
  setDealsListOpReset,
} from "../../slices/dealsListOperationSlice";
import {
  setSubscriptionsListOpReset,
  setSubscriptionsListOpValue,
} from "../../slices/subscriptionsListOperationSlice";
import { RootState } from "../../rootReducer";
import { useEffect } from "react";

interface SearchInputProps {
  onChange: (typedValue: string) => void;
}

export const SearchInput = ({ type = "deals" }) => {
  const dispatch = useDispatch();
  const [searchInputValue, updteSearchValue] = useState("");

  const { opType, opValue } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  useEffect(() => {
    if (opType && opValue && opType === "search") {
      updteSearchValue(opValue);
    }
  }, []);

  const handleUpdate = (val) => {
    updteSearchValue(val);
    if (type === "deals") {
      dispatch(setDealsListOpValue(val));
    } else {
      dispatch(setSubscriptionsListOpValue(val));
    }
  };

  const onClose = () => {
    if (type === "deals") {
      dispatch(setDealsListOpReset());
    } else {
      dispatch(setSubscriptionsListOpReset());
    }
    updteSearchValue("");
  };
  return (
    <>
      <Container textAlign={"center"} className="search-container">
        <Header as="h5" className="search-header">
          SEARCH {type.toUpperCase()}
        </Header>
        <Input
          className="search-input"
          icon="search"
          iconPosition="left"
          onChange={(event) => handleUpdate(event.target.value)}
          size="small"
          placeholder="Search..."
          value={searchInputValue}
        ></Input>
        <Icon
          className="search-close-icon"
          name="close"
          size="large"
          onClick={onClose}
        ></Icon>
      </Container>
    </>
  );
};

export default SearchInput;
