import React from "react";
import {
  Container,
  Card,
  Transition,
  Segment,
  Header,
  Icon,
  SemanticICONS,
} from "semantic-ui-react";

interface NoItemsProps {
  icon: SemanticICONS;
  itemType: string;
}

const NoItems: React.FC<NoItemsProps> = ({
  icon = "dollar",
  itemType = "Deals",
}) => {
  return (
    <Transition animation={"fade"}>
      <Container textAlign="center">
        <Card.Group itemsPerRow="one" stackable>
          <Card>
            <Card.Content>
              <Segment placeholder basic>
                <Header icon>
                  <Icon name={icon} color="grey" circular />
                  {`No ${itemType} to Display`}
                </Header>
              </Segment>
            </Card.Content>
          </Card>
        </Card.Group>
      </Container>
    </Transition>
  );
};
export default NoItems;
