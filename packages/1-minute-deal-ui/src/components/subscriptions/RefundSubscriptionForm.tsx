import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  CircularProgress,
  Typography,
  Card,
  CardContent,
  FormControlLabel,
  Switch,
  Box,
  Alert,
} from "@mui/material";
import { toast } from "react-toastify";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import HelpIcon from "@mui/icons-material/Help";
import React, { useEffect, useState } from "react";
import { RootState } from "../../rootReducer";
import { useSelector, useDispatch } from "react-redux";
import * as Sentry from "@sentry/react";
import {
  fetchSubscriptionById,
  fetchSubscriptionsListByDeal,
  subscriptionACHReverseTransfer,
} from "../../slices/subscriptionsSlice";
import NumberFormat from "react-number-format";
import { Subscription } from "../../api/subscriptionsApi";
import { Formik } from "formik";
import * as Yup from "yup";
import { FormField } from "../../forms/common/FormField";
import { Button, Divider, Form } from "semantic-ui-react";
import FormHint from "../../forms/common/FormHint";
import SubformCurrency from "../../forms/subforms/SubformCurrency";
import numeral from "numeral";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

export interface ACHAccountDetails {
  routing?: string;
  account?: string;
  typeOfAccount?: string;
  nameOnAccount?: string;
  amount?: string;
}
interface RefundSubscriptionFormProps {
  open: boolean;
  subscriptionId: string;
  handleClose?: () => void;
  handleSuccess?: () => void;
  processing: boolean;
}

const RefundSubscriptionForm: React.FC<RefundSubscriptionFormProps> = ({
  open,
  subscriptionId,
  handleClose,
  handleSuccess,
  processing,
}) => {
  const dispatch = useDispatch();

  const refundSubscription = async (
    subscriptionId,
    accountDetails?: ACHAccountDetails
  ) => {
    try {
      await dispatch(
        subscriptionACHReverseTransfer(subscriptionId, accountDetails)
      );
    } catch (e) {
      const err: any = e;
      setFailureMessage(
        err?.response?.data?.error?.message ??
          "An error occurred with the refund attempt"
      );
      return;
    }
    await dispatch(fetchSubscriptionsListByDeal(subscription.dealId));
    toast.success("Refund Queued");
    handleSuccess();
    return;
  };

  const [failureMessage, setFailureMessage] = useState<null | string>();
  const [isOffPlatformSubscription, setIsOffPlatformSubscription] =
    useState(false);
  const [partialRefundEnabled, setPartialRefundEnabled] = useState(false);
  const [accountDetails, setAccountDetails] = useState<{
    account?: string;
    routing?: string;
    nameOnAccount?: string;
    typeOfAccount?: string;
    amount?: string;
  }>();
  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const checkOffPlatformSubscription = (subscription: Subscription) => {
    return (
      !subscription.transactionIds || subscription.transactionIds.length < 1
    );
  };

  useEffect(() => {
    if (!subscription) {
      dispatch(fetchSubscriptionById(subscriptionId));
    } else {
      setIsOffPlatformSubscription(checkOffPlatformSubscription(subscription));
    }
  }, [dispatch, subscription, subscriptionId]);

  const offPlatformValidationSchema = Yup.object({
    account: Yup.string()
      .matches(/^[0-9]+$/, "Must be only digits")
      .required("Account Number is required"),
    routing: Yup.string()
      .matches(/^[0-9]+$/, "Must be only digits")
      .length(9, "Routing Number should be 9 digits long")
      .required("Routing Number is required"),
    nameOnAccount: Yup.string().min(4).required("Name on Account is required"),
    typeOfAccount: Yup.string()
      .oneOf(["CHECKING", "SAVINGS"])
      .required("Account Type is Required"),
    amount: Yup.number()
      .transform((value, ogValue) => {
        if (ogValue) {
          return Number(ogValue.replace(/\D+/g, ""));
        }
        return ogValue;
      })
      .positive()
      .max(subscription.amount),
  });

  const onPlatformValidationSchema = Yup.object({
    amount: Yup.number()
      .transform((value, ogValue) => Number(ogValue.replace(/\D+/g, "")))
      .positive()
      .max(subscription.amount)
      .required(),
  });

  return (
    <>
      <Dialog
        open={open}
        onClose={() => {
          setAccountDetails(undefined);
          handleClose();
        }}
      >
        <DialogTitle>Confirm Subscription Refund</DialogTitle>
        {subscription && !processing && (
          <>
            <DialogContent>
              <DialogContentText>
                You are about to refund the subscription for <br />
                <b>
                  {subscription?.name} - ({subscription?.email})
                </b>
                <br />
                in the amount of{" "}
                <NumberFormat
                  style={{
                    color: "green",
                    fontWeight: "bold",
                  }}
                  value={subscription.amount}
                  displayType={"text"}
                  thousandSeparator={true}
                  decimalScale={2}
                  decimalSeparator={"."}
                  fixedDecimalScale={true}
                  prefix={"$"}
                />
              </DialogContentText>
              {isOffPlatformSubscription && (
                <>
                  <DialogContentText>
                    This subscription was funded off-platform. The refund amount
                    will be processed via ACH to the Account and Routing number
                    specified below
                  </DialogContentText>
                  <Card>
                    <CardContent>
                      {!accountDetails?.account && !accountDetails?.routing && (
                        <Formik
                          initialValues={{
                            account: undefined,
                            routing: undefined,
                            nameOnAccount: undefined,
                            typeOfAccount: undefined,
                            amount: undefined,
                          }}
                          validationSchema={
                            isOffPlatformSubscription
                              ? offPlatformValidationSchema
                              : onPlatformValidationSchema
                          }
                          onSubmit={(values) => {
                            setAccountDetails(values);
                          }}
                        >
                          {(props) => (
                            <Form onSubmit={props.handleSubmit}>
                              <Form.Group widths="equal">
                                <FormField
                                  name={"account"}
                                  placeholder={"Account Number"}
                                />
                              </Form.Group>
                              <Form.Group widths="equal">
                                <FormField
                                  name={"routing"}
                                  placeholder={"Routing Number"}
                                />
                              </Form.Group>
                              <Form.Group widths="equal">
                                <FormField
                                  name={"nameOnAccount"}
                                  placeholder={"Name on the Account"}
                                />
                                <FormHint
                                  content={
                                    <Typography style={{ padding: "1em" }}>
                                      The name of an owner on the account.
                                      <br />
                                      This is used by the bank to validate the
                                      transaction.
                                    </Typography>
                                  }
                                >
                                  <HelpIcon />
                                </FormHint>
                              </Form.Group>
                              <Form.Group widths="equal">
                                <FormField
                                  name={"typeOfAccount"}
                                  component={Form.Select}
                                  options={[
                                    {
                                      key: "CHECKING",
                                      value: "CHECKING",
                                      text: "CHECKING",
                                    },
                                    {
                                      key: "SAVINGS",
                                      value: "SAVINGS",
                                      text: "SAVINGS",
                                    },
                                  ]}
                                  placeholder={"Account Type"}
                                />
                                <FormHint
                                  content={
                                    <Typography style={{ padding: "1em" }}>
                                      The type of account.
                                      <br />
                                      This is used by the bank to validate the
                                      transaction.
                                    </Typography>
                                  }
                                >
                                  <HelpIcon />
                                </FormHint>
                              </Form.Group>
                              <Form.Group>
                                <FormControlLabel
                                  control={
                                    <Switch
                                      color="default"
                                      checked={partialRefundEnabled}
                                      onChange={() =>
                                        setPartialRefundEnabled(
                                          !partialRefundEnabled
                                        )
                                      }
                                    />
                                  }
                                  label="Partial Refund"
                                />
                              </Form.Group>
                              {partialRefundEnabled && (
                                <>
                                  <Form.Group widths="equal">
                                    <SubformCurrency
                                      name={"amount"}
                                      id={"amount"}
                                      label={"Subscription Amount"}
                                      placeholder={numeral(
                                        subscription.amount
                                      ).format("$0,0")}
                                    />
                                  </Form.Group>
                                </>
                              )}
                              <Form.Group widths="equal">
                                <Button type="submit" primary>
                                  Confirm Account
                                </Button>
                              </Form.Group>
                            </Form>
                          )}
                        </Formik>
                      )}
                      {accountDetails?.account && accountDetails?.routing && (
                        <>
                          <div>
                            <Typography align="center">
                              <AccountBalanceIcon style={{ fontSize: 40 }} />
                            </Typography>
                          </div>
                          <div>
                            <Typography align="center">
                              Account Number ending in{" "}
                              {accountDetails?.account.slice(-4)}
                            </Typography>
                          </div>
                          <div>
                            <Typography align="center">
                              Routing Number: {accountDetails?.routing}
                            </Typography>
                          </div>
                          <div>
                            <Typography align="center">
                              Name on Account: {accountDetails?.nameOnAccount}
                            </Typography>
                          </div>
                          <div>
                            <Typography align="center">
                              Type of Account: {accountDetails?.typeOfAccount}
                            </Typography>
                          </div>
                          {accountDetails?.amount && (
                            <div>
                              <Typography align="center">
                                Amount:{" "}
                                {numeral(accountDetails?.amount).format("$0,0")}
                              </Typography>
                            </div>
                          )}
                          <Box display={"flex"} justifyContent={"center"}>
                            <Button onClick={() => setAccountDetails({})}>
                              Edit
                            </Button>
                          </Box>
                        </>
                      )}
                    </CardContent>
                  </Card>
                </>
              )}
              {!isOffPlatformSubscription && (
                <DialogContentText>
                  The refund will be processed via ACH to the original account
                  that sent the funds
                  <Formik
                    initialValues={{
                      amount: null,
                    }}
                    validationSchema={onPlatformValidationSchema}
                    onSubmit={(values) => {
                      setAccountDetails(values);
                    }}
                  >
                    {(props) => (
                      <Form onSubmit={props.handleSubmit}>
                        <Form.Group>
                          <FormControlLabel
                            control={
                              <Switch
                                color="default"
                                checked={partialRefundEnabled}
                                onChange={() =>
                                  setPartialRefundEnabled(!partialRefundEnabled)
                                }
                              />
                            }
                            label="Partial Refund"
                          />
                        </Form.Group>
                        {partialRefundEnabled && (
                          <>
                            <Form.Group widths="equal">
                              <SubformCurrency
                                name={"amount"}
                                id={"amount"}
                                label={"Subscription Amount"}
                                placeholder={numeral(
                                  subscription.amount
                                ).format("$0,0")}
                                disabled={accountDetails?.amount}
                              />
                            </Form.Group>
                            <Form.Group widths="equal">
                              {accountDetails?.amount && (
                                <>
                                  <CheckCircleIcon />
                                  <Typography>
                                    Partial Refund of{" "}
                                    {numeral(accountDetails.amount).format(
                                      "$0,0"
                                    )}
                                  </Typography>
                                </>
                              )}
                              {!accountDetails?.amount && (
                                <Button
                                  type="submit"
                                  primary
                                  disabled={!props.isValid}
                                >
                                  Confirm Partial Refund
                                </Button>
                              )}
                            </Form.Group>
                          </>
                        )}
                      </Form>
                    )}
                  </Formik>
                </DialogContentText>
              )}
              {failureMessage && (
                <>
                  <Divider hidden />
                  <Alert
                    severity={"error"}
                    onClose={() => setFailureMessage(null)}
                  >
                    {failureMessage}
                  </Alert>
                </>
              )}
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => {
                  setAccountDetails(undefined);
                  handleClose();
                }}
              >
                Cancel Refund
              </Button>
              <Button
                disabled={
                  (isOffPlatformSubscription &&
                    !(
                      accountDetails?.account &&
                      accountDetails?.routing &&
                      accountDetails?.nameOnAccount &&
                      accountDetails?.typeOfAccount
                    )) ||
                  (!isOffPlatformSubscription &&
                    partialRefundEnabled &&
                    !accountDetails?.amount)
                }
                onClick={async () => {
                  if (partialRefundEnabled) {
                    accountDetails.amount = accountDetails.amount
                      ? accountDetails.amount.replace(/\D+/g, "")
                      : undefined;
                  }
                  await refundSubscription(subscriptionId, accountDetails);
                }}
                primary
              >
                Confirm Refund
              </Button>
            </DialogActions>
          </>
        )}
        {(!subscription || processing) && (
          <DialogContent>
            <Typography align={"center"}>
              <DialogContentText>
                Please wait while the refund is being processed
              </DialogContentText>
              <CircularProgress />
            </Typography>
          </DialogContent>
        )}
      </Dialog>
    </>
  );
};

export default RefundSubscriptionForm;
