import React, { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import moment from "moment";
import NumberFormat from "react-number-format";
import Skeleton from "@mui/material/Skeleton";
import { Box } from "@mui/material";
import { RootState } from "../../rootReducer";

import {
  Header,
  Label,
  Card,
  Image,
  Transition,
  Icon,
  Table,
  List,
  Divider,
} from "semantic-ui-react";

import { fetchDealById } from "../../slices/dealsSlice";
import { fetchSubscriptionById } from "../../slices/subscriptionsSlice";
import { fetchAssetById } from "../../slices/assetsSlice";
import {
  fetchProfilesList,
  fetchProfileById,
} from "../../slices/profilesSlice";
import ImageFile from "../../components/ImageFile";

import { SubscriptionStatusColors as ssc } from "../../tools/statusColors";
import { getProfileName } from "../../api/profileApiWrapper";
import { API, useRequest } from "../../api/swrApi";

type SCProps = {
  subscriptionId: string;
  context: "signer" | "investor";
};

export default function SubscriptionCard({ subscriptionId, context }: SCProps) {
  const dispatch = useDispatch();
  // SUBSCRIPTION
  const { isLoading: subscriptionsLoading } = useSelector(
    (state: RootState) => state.subscriptions
  );
  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );
  useEffect(() => {
    if (subscription?.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  // DEAL
  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );
  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });
  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [dispatch, subscription, deal]);

  // ASSET
  const { isLoading: assetsLoading } = useSelector(
    (state: RootState) => state.assets
  );
  const assets = useSelector((state: RootState) => {
    return !deal
      ? undefined
      : Object.values(state.assets.assetsById).filter(
          (asset) => asset.dealId === deal.id
        );
  });
  useEffect(() => {
    if (deal && deal.assetIds) {
      deal.assetIds.forEach((assetId) => {
        if (assets.findIndex((asset) => asset.id === assetId) !== -1) {
          dispatch(fetchAssetById(assetId));
        }
      });
    }
  }, [dispatch, deal, assets]);

  //PROFILE
  const { isLoading: profilesLoading } = useSelector(
    (state: RootState) => state.profiles
  );

  const profile = useSelector((state: RootState) => {
    return !subscription?.profileId
      ? undefined
      : state.profiles.profilesById[subscription.profileId];
  });
  useEffect(() => {
    if (subscription?.profileId && !profile) {
      dispatch(fetchProfilesList());
    }
  }, [dispatch, subscription, profile]);

  const { data: self } = useRequest(API.SELF);

  const hasSigned = useCallback(() => {
    return (
      self &&
      !!(subscription?.signers ?? []).find(
        (signatory) => signatory.email === self.email
      )?.dateSigned
    );
  }, [subscription, self]);

  const timeRemaining = calculateTimeRemaining(deal?.estimatedCloseDate);

  return (
    <>
      <Transition>
        <Card
          link
          color="grey"
          as={Link}
          to={
            context === "investor"
              ? `/subscriptions/${subscription?.id}`
              : `/signing/${subscription?.id}`
          }
        >
          {assets &&
            assets.map(
              (asset) => asset?.images && <ImageFile fileId={asset.images[0]} />
            )}
          <Card.Content>
            {!subscription && (
              <>
                <Skeleton animation="wave" height={40} />
                <Divider />
                <Skeleton animation="wave" height={40} width={100} />
                <Box
                  display={"flex"}
                  flexDirection={"row"}
                  justifyContent={"space-between"}
                >
                  <Box>
                    <Skeleton animation="wave" height={40} width={50} />
                  </Box>
                  <Box
                    display={"flex"}
                    flexDirection={"column"}
                    alignItems={"flex-end"}
                  >
                    <Skeleton animation="wave" height={40} width={150} />
                    <Skeleton animation="wave" height={30} width={200} />
                  </Box>
                </Box>
              </>
            )}
            {subscription && (
              <Table basic="very" celled compact unstackable>
                <Table.Body>
                  {assets &&
                    assets.map((asset) => (
                      <Table.Row key={asset.id}>
                        <Table.Cell width={12}>
                          <Header as="h2" className="deal-card-header">
                            {asset?.name ? asset?.name : deal?.name}
                            {asset?.artist?.name && (
                              <Header.Subheader content={asset.artist.name} />
                            )}
                          </Header>
                        </Table.Cell>
                        {deal?.marketing?.logo && (
                          <Table.Cell rowSpan="2" textAlign="center">
                            <Image centered fluid src={deal?.marketing?.logo} />
                          </Table.Cell>
                        )}
                      </Table.Row>
                    ))}
                  {(!assets || assets.length < 1) && (
                    <Table.Row>
                      <Table.Cell width={12}>
                        <Header as="h2" className="deal-card-header">
                          {deal?.name}
                        </Header>
                      </Table.Cell>
                      {deal?.marketing?.logo && (
                        <Table.Cell rowSpan="2" textAlign="center">
                          <Image centered fluid src={deal?.marketing?.logo} />
                        </Table.Cell>
                      )}
                    </Table.Row>
                  )}

                  <Table.Row>
                    <Table.Cell>
                      <Label circular color={ssc(subscription?.status)}>
                        {subscription?.status}
                      </Label>
                    </Table.Cell>
                    <Table.Cell>
                      {subscription?.signers &&
                        subscription.signers.length > 0 && (
                          <Label
                            circular
                            color={hasSigned() ? "green" : "blue"}
                          >
                            {hasSigned() ? "SIGNED" : "AWAITING SIGNATURE"}
                          </Label>
                        )}
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            )}

            {subscription && (
              <Table basic="very" compact unstackable widths="equal">
                <Table.Body>
                  <Table.Row>
                    <Table.Cell>
                      <Header as="h1" color="orange">
                        {timeRemaining}
                        <Header.Subheader>days to close</Header.Subheader>
                      </Header>
                    </Table.Cell>
                    <Table.Cell textAlign="right">
                      <Header as="h1" color="grey">
                        {moment.utc(deal?.estimatedCloseDate).format("L")}
                        <Header.Subheader>
                          estimated close date
                        </Header.Subheader>
                      </Header>
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            )}
          </Card.Content>
          {subscription?.profileId && profile && (
            <Card.Content extra textAlign="left">
              <Header as="h4" className="no-spacing">
                Investment Profile
              </Header>
              <List>
                <List.Item>
                  <List.Content>{getProfileName(profile)}</List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>{profile.email}</List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    Invested as{" "}
                    {profile.taxDetails.registrationType.toLowerCase()}
                  </List.Content>
                </List.Item>
              </List>
            </Card.Content>
          )}
          {subscription?.amount && (
            <Card.Content
              extra
              textAlign="left"
              className="ui green-background"
            >
              <Header>
                <span style={{ color: "white" }}>
                  <Icon name="check circle" /> Subscription for{" "}
                  <NumberFormat
                    value={subscription?.amount}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"$"}
                  />
                </span>
              </Header>
            </Card.Content>
          )}
          {subscription && (
            <Card.Content extra textAlign="center">
              View Details
            </Card.Content>
          )}
        </Card>
      </Transition>
    </>
  );
}

function calculateTimeRemaining(estimatedCloseDate) {
  const oneDay = 24 * 60 * 60 * 1000;
  let timeRemaining = 0;
  if (estimatedCloseDate !== undefined) {
    const closingDate = new Date(
      estimatedCloseDate.slice(0, 4),
      estimatedCloseDate.slice(5, 7) - 1,
      estimatedCloseDate.slice(8, 10),
      0,
      0,
      0,
      0
    );
    const today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    if ((closingDate.getTime() - today.getTime()) / oneDay > 0) {
      timeRemaining = Math.ceil(
        (closingDate.getTime() - today.getTime()) / oneDay
      );
    } else {
      timeRemaining = 0;
    }
  }
  return timeRemaining;
}
