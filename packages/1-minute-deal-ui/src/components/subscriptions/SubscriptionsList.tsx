import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import { Container, Card, Divider } from "semantic-ui-react";

import { Subscription } from "../../api/subscriptionsApi";
import NoItems from "./NoItems";
import SubscriptionCard from "./SubscriptionCard";
import SubscriptionOperations from "./SubscriptionOperations";
import SubscriptionsListActions from "./SubscriptionsListActions";

interface Props {
  subscriptions: Subscription[];
  context: "investor" | "signer";
}

export const SubscriptionsList = ({
  subscriptions,
  context = "investor",
}: Props) => {
  const { opType } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  return (
    <>
      {subscriptions.length > 0 || opType ? (
        <>
          <SubscriptionsListActions />
          {opType && <SubscriptionOperations />}
          <Divider hidden />
          <Container>
            <Card.Group itemsPerRow="two" centered stackable>
              {subscriptions.map((subscription) => (
                <SubscriptionCard
                  key={subscription.id}
                  subscriptionId={subscription.id}
                  context={context}
                />
              ))}
            </Card.Group>
          </Container>
        </>
      ) : (
        <NoItems
          icon={context === "investor" ? "dollar" : "edit"}
          itemType={context === "investor" ? "Investments" : "Signing Requests"}
        />
      )}
    </>
  );
};
