import React, { useState } from "react";
import { Menu, Input, Icon, Button, Label, Dropdown } from "semantic-ui-react";
import { SearchInput } from "../../components/search/Search";
import { SortInput } from "../../components/sort/Sort";
import { FilterInput } from "../../components/filter/Filter";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import FilterSubscriptions from "../filter/FilterSubscriptions";

export const SubscriptionOperation = () => {
  const { opType } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  return (
    <>
      <Menu borderless className="nav-bar filteration-menu">
        <Menu.Item>
          {opType === "search" ? (
            <SearchInput type={"subscriptions"} />
          ) : opType === "filter" ? (
            <FilterSubscriptions />
          ) : opType === "sort" ? (
            <SortInput type={"subscriptions"} />
          ) : (
            ""
          )}
        </Menu.Item>
      </Menu>
    </>
  );
};

export default SubscriptionOperation;
