import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import DealFundingButtons from "../../pages/DealFundingButtons";
import { DownloadGeneratorFile } from "../../tools/downloadUtils";
import { FeatureFlag } from "../featureflags/FeatureFlags";
import {
  canChangeSubscriptionAmount,
  canDeleteSubscription,
  DB_STATUSES,
} from "../../tools/subscriptionStatuses";
import { useSubscriptionMeta } from "../../hooks/subscriptionHooks";
import { Alert } from "@mui/material";
import { Divider, Button } from "semantic-ui-react";
import { insertNotification } from "../../slices/notificationsSlice";
import {
  deleteSubscriptionById,
  fetchSubscriptionById,
  updateSubscriptionById,
} from "../../slices/subscriptionsSlice";
import { useDispatch, useSelector } from "react-redux";
import { TenantContext } from "../../contexts";
import { RootState } from "../../rootReducer";
import { fetchSelf } from "../../slices/usersSlice";
import {
  DialogActions,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Typography,
  CircularProgress,
} from "@mui/material";
import { API, useRequest } from "../../api/swrApi";
import { createSubscription, Subscription } from "../../api/subscriptionsApi";

interface SubscriptionButtonsProps {
  subscriptionId: string;
}
const SubscriptionButtons: React.FC<SubscriptionButtonsProps> = ({
  subscriptionId,
}) => {
  const { dealIsAvailable } = useSubscriptionMeta(subscriptionId);
  const showNotAvailableMessage = !dealIsAvailable;
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  let history = useHistory();
  const [showCancelConfirmation, setShowCancelConfirmation] = useState(false);
  const { data: self } = useRequest(API.SELF);
  const { data: subscription, revalidate: revalidateSubscription } = useRequest(
    API.SUBSCRIPTION_BY_ID(subscriptionId)
  );
  const { data: deal } = useRequest(API.DEAL_BY_ID(subscription?.dealId));
  const { data: entity } = useRequest(API.ENTITY_BY_ID(deal?.entityId));

  const { isLoading: subscriptionsLoading, isSubmitting } = useSelector(
    (state: RootState) => state.subscriptions
  );

  useEffect(() => {
    revalidateSubscription();
  }, []);

  const shouldShowFundingButtons =
    !showNotAvailableMessage &&
    deal?.status !== "CLOSED" &&
    [DB_STATUSES.COMMITTED, DB_STATUSES.FUNDS_IN_TRANSIT_MANUAL].includes(
      subscription?.status
    );

  async function handleMarkFundsSentManually() {
    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        userId: self.id,
        event: {
          type: "subscription",
          id: subscription.id,
        },
        category: "INVESTMENTS",
        message: `You marked your funds as sent off-platform for Deal ${deal.name}`,
        acknowledged: false,
      })
    );
    await dispatch(
      updateSubscriptionById({
        ...subscription,
        status: "FUNDS_IN_TRANSIT-MANUAL",
      })
    );
  }

  async function handleClick() {
    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        userId: self.id,
        event: {
          type: "subscription",
          id: subscription.id,
        },
        category: "INVESTMENTS",
        message: `You have started the investment process for deal ${deal.name}`,
        acknowledged: false,
      })
    );
    await dispatch(
      updateSubscriptionById({
        ...subscription,
        status: "PENDING",
      })
    );
    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        profileId: deal.profileId,
        event: {
          type: "deal",
          id: deal.id,
        },
        category: "DEAL_STATUS",
        message: `An Investor, ${self.firstName} ${self.lastName}, has viewed Deal ${deal.name}`,
        acknowledged: false,
      })
    );
    if (!subscription.profileId) {
      history.push(
        `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
      );
    } else {
      history.push(`/subscriptions/${subscriptionId}/set-amount`);
    }
  }

  async function createAnotherSubscription() {
    let newSubscription: Subscription = await createSubscription({
      email: self.username,
      name: `${self.firstName} ${self.lastName}`,
      ownerId: self.id,
      dealId: deal.id,
      status: "PENDING",
      acquisitionMethod: "INVITATION",
    });

    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        userId: self.id,
        event: {
          type: "subscription",
          id: newSubscription.id,
        },
        category: "INVESTMENTS",
        message: `You have started the investment process for deal ${deal.name}`,
        acknowledged: false,
      })
    );

    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        profileId: deal.profileId,
        event: {
          type: "deal",
          id: deal.id,
        },
        category: "DEAL_STATUS",
        message: `An Investor, ${self.firstName} ${self.lastName}, has viewed Deal ${deal.name}`,
        acknowledged: false,
      })
    );

    history.push(
      `/profiles/create/subscriber?subscriptionId=${newSubscription.id}`
    );
  }

  return (
    <>
      {subscription?.status === "CLOSED" && (
        <>Your investment has been included in a deal close by the Organizer</>
      )}
      {subscription && (
        <>
          {showNotAvailableMessage && (
            <Alert severity="info">
              This deal has met its maximum number of allowed investors or is
              already fully funded, and is no longer available. For more
              information, please contact the deal organizer.
            </Alert>
          )}
          {!showNotAvailableMessage &&
            deal?.status !== "CLOSED" &&
            subscription?.status === "INVITED" && (
              <Button
                fluid
                color="green"
                onClick={handleClick}
                loading={isSubmitting}
                disabled={deal?.status === "DRAFT"}
                content="Invest Now"
              />
            )}
          {!showNotAvailableMessage &&
            deal?.status !== "CLOSED" &&
            subscription?.status === "PENDING" &&
            !subscription.amount && (
              <Button
                fluid
                color="green"
                as={Link}
                to={
                  subscription.profileId
                    ? `/subscriptions/${subscriptionId}/set-amount`
                    : `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
                }
                disabled={deal?.status === "DRAFT"}
                content="Complete Investment"
              />
            )}
          {!showNotAvailableMessage &&
            deal?.status !== "CLOSED" &&
            subscription?.status === "PENDING" &&
            subscription.amount && (
              <Button
                fluid
                color="green"
                as={Link}
                to={`/subscriptions/${subscriptionId}/set-amount?amount=${subscription.amount}`}
                disabled={deal?.status === "DRAFT"}
                content="Sign Agreement"
              />
            )}
          {shouldShowFundingButtons && (
            <DealFundingButtons
              subscription={subscription}
              dealStatus={deal?.status}
              handleMarkFundsSentManually={handleMarkFundsSentManually}
            />
          )}
          {subscription?.status === "COMPLETED" && (
            <>
              <Button
                fluid
                outline
                as={Link}
                to={`/profiles/create/subscriber?subscriptionId=${subscriptionId}&regenerate=true`}
                content="Change Investment Profile"
              />
              <Divider hidden fitted />
              <Button
                fluid
                outline
                onClick={() => createAnotherSubscription()}
                loading={isSubmitting}
                content="Make Investment with Separate Profile"
                data-testid="create-another-button"
              />
            </>
          )}
          <FeatureFlag name={"change_investment_amount"}>
            {subscription?.status === "COMPLETED" &&
              subscription?.transactionIds && (
                <>
                  <Divider hidden fitted />
                  <Button
                    fluid
                    color="green"
                    as={Link}
                    to={`/subscriptions/${subscription.id}/set-amount`}
                    content="Increase Funding Amount"
                  />
                </>
              )}
          </FeatureFlag>
          <FeatureFlag name={"reduce_investment"}>
            {subscription && canChangeSubscriptionAmount(subscription) && (
              <>
                <Divider hidden />
                <Button
                  fluid
                  outline
                  content="Change Investment Amount"
                  as={Link}
                  to={`/subscriptions/${subscriptionId}/set-amount?amount=${subscription.amount}`}
                />
              </>
            )}
          </FeatureFlag>
          <FeatureFlag name={"cancel_investment"}>
            {!showNotAvailableMessage &&
              subscription &&
              canDeleteSubscription(subscription) && (
                <>
                  <Divider />
                  <Button
                    fluid
                    outline
                    content="Cancel Investment"
                    onClick={(e) => setShowCancelConfirmation(true)}
                  />
                </>
              )}
            {subscription && subscription.status === "CANCELLED" && (
              <>
                <Alert severity="info">
                  This investment opportunity has been cancelled. Please contact
                  your Organizer if you require additional information on the
                  status of this investment.
                </Alert>
              </>
            )}
          </FeatureFlag>
          <Dialog
            open={showCancelConfirmation}
            onClose={() => {
              setShowCancelConfirmation(false);
            }}
          >
            <DialogTitle>Confirm Investment Cancel</DialogTitle>
            {subscription && (
              <>
                <DialogContent>
                  <DialogContentText>
                    You are about to cancel your Investment in deal{" "}
                    {`"${deal && deal.name}"`}
                  </DialogContentText>
                  <DialogContentText>
                    You may not be able to re-join this deal unless you are
                    invited again by an Organizer.
                    <Divider />
                    <h3>Are you sure you want to cancel?</h3>
                  </DialogContentText>
                  <DialogActions>
                    <Button
                      onClick={() => {
                        setShowCancelConfirmation(false);
                      }}
                    >
                      {"Don't Cancel"}
                    </Button>
                    <Button
                      onClick={async () => {
                        await dispatch(deleteSubscriptionById(subscription.id));
                        await dispatch(
                          insertNotification({
                            userId: entity.ownerId,
                            event: {
                              type: "subscription",
                              id: subscription.id,
                            },
                            category: "INVESTMENTS",
                            message: `The Subscription for ${subscription.name} (${subscription.email}) has been cancelled`,
                            acknowledged: false,
                          })
                        );
                        history.push("/dashboard");
                        setShowCancelConfirmation(false);
                      }}
                      primary
                    >
                      {subscriptionsLoading && (
                        <>
                          <DialogContent>
                            <Typography align={"center"}>
                              <DialogContentText>
                                Please wait while the cancellation is being
                                processed
                              </DialogContentText>
                              <CircularProgress />
                            </Typography>
                          </DialogContent>
                        </>
                      )}
                      {!subscriptionsLoading && <>Yes, Cancel my Investment</>}
                    </Button>
                  </DialogActions>
                </DialogContent>
              </>
            )}
          </Dialog>
        </>
      )}
    </>
  );
};

export default SubscriptionButtons;
