import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import PropTypes from "prop-types";
import { RootState } from "../../rootReducer";

import { Icon, Button, Menu } from "semantic-ui-react";
import { setSubscriptionsListOpType } from "../../slices/subscriptionsListOperationSlice";

export default function SubscriptionsListActions() {
  const dispatch = useDispatch();

  const { opType } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  return (
    <Menu borderless secondary className="nav-bar">
      <Menu.Item>
        <Button.Group icon size="tiny">
          <Button
            icon
            {...(opType === "search" && { className: "primaryColor" })}
            aria-label="search"
            onClick={() =>
              dispatch(setSubscriptionsListOpType({ opType: "search" }))
            }
          >
            <Icon name="search" />
          </Button>
          <Button
            icon
            {...(opType === "filter" && { className: "primaryColor" })}
            aria-label="filter"
            onClick={() =>
              dispatch(setSubscriptionsListOpType({ opType: "filter" }))
            }
          >
            <Icon name="filter" />
          </Button>
          <Button
            icon
            {...(opType === "sort" && { className: "primaryColor" })}
            aria-label="sort"
            onClick={() =>
              dispatch(setSubscriptionsListOpType({ opType: "sort" }))
            }
          >
            <Icon name="sort" />
          </Button>
        </Button.Group>
      </Menu.Item>
    </Menu>
  );
}
