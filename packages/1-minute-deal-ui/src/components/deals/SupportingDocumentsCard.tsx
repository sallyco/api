import React from "react";
import { Button, Card, Popup, Table } from "semantic-ui-react";
import { API, doChange, useRequest } from "../../api/swrApi";
import WaitForDependencies from "../WaitForDependencies";
import { Deal } from "../../api/dealsApi";
import { Box, Typography } from "@mui/material";
import { toast } from "react-toastify";
import DocumentRow from "../files/DocumentRow";
import {
  FileSizes,
  SubformFileUpload,
} from "../../forms/subforms/SubformFileUpload";

interface SupportingDocumentsCardProps {
  dealId?: string;
}

const SupportingDocumentsCard: React.FC<SupportingDocumentsCardProps> = ({
  dealId,
}) => {
  const { data: deal, mutate: mutateDeal } = useRequest<Deal>(
    API.ADD_FILTER(API.DEAL_BY_ID(dealId), { include: ["distribution"] })
  );

  const RemoveDocument = async (documentId: string, documentKey: string) => {
    const newData = {
      ...deal,
      ...{
        distribution: {
          ...deal.distribution,
          docs: {
            ...deal?.distribution?.docs,
            [documentKey]: [
              ...deal?.distribution?.docs[documentKey].filter(
                (doc) => doc !== documentId
              ),
            ],
          },
        },
      },
    };
    await mutateDeal(newData, false);
    await doChange(
      API.DISTRIBUTION_BY_ID(deal?.distribution?.id),
      {
        docs: {
          ...newData.distribution.docs,
        },
      },
      "PATCH"
    );
    toast.success("File Deleted");
    await mutateDeal();
  };

  const UploadDocuments = async (
    documentIds: string[],
    documentKey: string
  ) => {
    if (!deal.distribution) {
      const distribution = await doChange(
        API.DISTRIBUTIONS,
        {
          dealId: deal.id,
          docs: {
            [documentKey]: [...documentIds],
          },
          purchaseDetails: {},
        },
        "POST"
      );
      await mutateDeal(
        {
          ...deal,
          distribution: distribution,
        },
        true
      );
      return;
    }
    const newData = {
      ...deal,
      ...{
        distribution: {
          ...deal.distribution,
          docs: {
            ...(deal?.distribution?.docs ?? {}),
            [documentKey]: [
              ...(deal?.distribution?.docs[documentKey] ?? []),
              ...documentIds,
            ],
          },
        },
      },
    };
    await mutateDeal(newData, false);
    await doChange(
      API.DISTRIBUTION_BY_ID(deal?.distribution?.id),
      {
        docs: {
          ...newData.distribution.docs,
        },
      },
      "PATCH"
    );
    toast.success("File Uploaded");
    await mutateDeal();
  };

  return (
    <Card fluid>
      <Card.Content>
        <Card.Header>Supporting Documents</Card.Header>
        <WaitForDependencies items={[deal]}>
          <Table basic={"very"}>
            <DocumentUploadOrDisplay
              documentLabel={"Letter of Intent"}
              documentKey={"letterOfIntent"}
              documentIds={deal?.distribution?.docs?.letterOfIntent}
              onUpload={async (fileIds) => {
                await UploadDocuments(fileIds, "letterOfIntent");
              }}
              onDelete={async (fileId) => {
                await RemoveDocument(fileId, "letterOfIntent");
              }}
            />
            <DocumentUploadOrDisplay
              documentLabel={"Definitive Purchase Agreement"}
              documentKey={"definitivePurchaseAgreement"}
              documentIds={
                deal?.distribution?.docs?.definitivePurchaseAgreement
              }
              onUpload={async (fileIds) => {
                await UploadDocuments(fileIds, "definitivePurchaseAgreement");
              }}
              onDelete={async (fileId) => {
                await RemoveDocument(fileId, "definitivePurchaseAgreement");
              }}
            />
            <DocumentUploadOrDisplay
              documentLabel={"Memorandum of Understanding"}
              documentKey={"memorandumOfUnderstanding"}
              documentIds={deal?.distribution?.docs?.memorandumOfUnderstanding}
              onUpload={async (fileIds) => {
                await UploadDocuments(fileIds, "memorandumOfUnderstanding");
              }}
              onDelete={async (fileId) => {
                await RemoveDocument(fileId, "memorandumOfUnderstanding");
              }}
            />
          </Table>
        </WaitForDependencies>
      </Card.Content>
    </Card>
  );
};

const DocumentUploadOrDisplay: React.FC<{
  documentIds?: string[];
  documentKey: string;
  documentLabel: string;
  onUpload: (fileIds: string[]) => void;
  onDelete: (deletedFileId) => void;
}> = ({ documentIds, documentLabel, onUpload, onDelete }) => {
  return (
    <>
      {
        <Table.Row>
          <Table.Cell colSpan={3}>
            <Box
              display={"flex"}
              flexDirection={"row"}
              alignItems={"center"}
              m={2}
            >
              <Box flexGrow={1}>
                <Typography>{documentLabel}</Typography>
              </Box>
              <Box>
                <SubformFileUpload
                  maxSize={FileSizes.MB * 50}
                  onError={(errors) => {
                    errors.forEach((error) => {
                      toast.error(error);
                    });
                  }}
                  onSuccess={onUpload}
                >
                  {(props) => {
                    return (
                      <Popup
                        trigger={
                          <Button
                            secondary
                            content="Upload Files"
                            onClick={props.triggerFileUpload}
                            loading={props.uploading}
                            style={{
                              whiteSpace: "nowrap",
                            }}
                          />
                        }
                        basic
                        inverted
                        content={"Upload Files"}
                      />
                    );
                  }}
                </SubformFileUpload>
              </Box>
            </Box>
          </Table.Cell>
        </Table.Row>
      }
      {documentIds && documentIds.length > 0 && (
        <>
          {documentIds.map((documentId) => (
            <DocumentRow
              key={documentId}
              canDelete={true}
              fileId={documentId}
              documentTitle={documentLabel}
              onDelete={onDelete}
            />
          ))}
        </>
      )}
    </>
  );
};

export default SupportingDocumentsCard;
