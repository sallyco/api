import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById, updateEntityById } from "../../slices/entitySlice";
// import { fetchAssetById } from "../../slices/assetsSlice";
import { fetchFileById } from "../../slices/filesSlice";

import { Grid, Table, Card } from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";

interface Props {
  dealId: string;
}

const EntityDocumentsCard = ({ dealId }: Props) => {
  const dispatch = useDispatch();
  const [filesFetched, setFilesFetched] = useState([]);
  const [fileDeleting, setFileDeleting] = useState(false);

  const { error: entitiesError } = useSelector(
    (state: RootState) => state.entities
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const files = useSelector((state: RootState) => {
    return Object.values(state.files.filesById ?? {}).filter(
      (file) => file.isPublic && filesFetched.includes(file.id)
    );
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (deal && !entitiesError && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entitiesError, entity]);

  useEffect(() => {
    if (entity?.files?.length > 0 && !fileDeleting) {
      entity.files.forEach((fileId) => {
        if (!filesFetched.includes(fileId)) {
          dispatch(fetchFileById(fileId));
          setFilesFetched([...filesFetched, fileId]);
        }
      });
    }
  }, [dispatch, entity, filesFetched, fileDeleting]);

  const deleteFileFromEntity = async (deletedFileId) => {
    try {
      setFileDeleting(true);
      if (
        entity?.files &&
        entity?.files.filter((fileId) => fileId === deletedFileId).length > 0
      ) {
        const fileList = [...entity.files].filter(
          (val) => val !== deletedFileId
        );
        await dispatch(
          updateEntityById({
            ...entity,
            files: fileList,
          })
        );
      }
      setFileDeleting(false);
    } catch (err) {
      setFileDeleting(false);
    }
  };

  return (
    <>
      {files?.length ? (
        <Card fluid>
          <Card.Content>
            <Card.Header>
              <Grid columns="equal">
                <Grid.Column>Entity Documents</Grid.Column>
              </Grid>
            </Card.Header>
          </Card.Content>
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {files.map((file) => (
                  <DocumentRow
                    key={file.id}
                    fileId={file.id}
                    canDelete={true}
                    onDelete={async (deletedFileId) => {
                      await deleteFileFromEntity(deletedFileId);
                    }}
                  />
                ))}
              </Table.Body>
            </Table>
          </Card.Content>
        </Card>
      ) : (
        <></>
      )}
    </>
  );
};

export default EntityDocumentsCard;
