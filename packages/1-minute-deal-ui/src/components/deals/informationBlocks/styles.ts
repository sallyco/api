import makeStyles from "@mui/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  informationalBlockTitle: {
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: "1em",
  },
  informationalBlockSubTitle: {
    textTransform: "uppercase",
    textAlign: "center",
    color: "gray",
    fontSize: "1em",
  },
}));

export default useStyles;
