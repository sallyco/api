import React, { useContext } from "react";
import { ContentLoader, StatementTable } from "@packages/gbt-ui";
import { useFeesStatementSWR } from "../../swrConnector/deals";
import { TenantContext } from "../../contexts";
import { Card, CardContent, Typography } from "@mui/material";
import { API, useRequest } from "../../api/swrApi";

export default function FeesStatement({ dealId }) {
  const { data: feesStatement, isValidating } = useRequest(
    dealId && API.FEES_BY_DEAL_ID(dealId)
  );
  const tenantProfile = useContext(TenantContext);

  return !feesStatement ? (
    // TODO: Theme using material UI?
    <div style={{ color: "white" }}>
      <ContentLoader
        message="Generating closing statement, please wait..."
        size={64}
        timeoutMilliseconds={30000}
      />
    </div>
  ) : (
    <>
      {tenantProfile?.billingContact && (
        <>
          <Card>
            <CardContent>
              <Typography
                display="block"
                variant="subtitle1"
                color="textPrimary"
                data-testid="billing-contact"
              >
                Billing Contact: <b>{tenantProfile.billingContact}</b>
              </Typography>
            </CardContent>
          </Card>
        </>
      )}
      <StatementTable statementData={feesStatement} loading={isValidating} />
    </>
  );
}
