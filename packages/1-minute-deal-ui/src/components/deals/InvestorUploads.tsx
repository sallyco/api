import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Grid, Table, Button, Card, Icon, Popup } from "semantic-ui-react";

import DocumentRow from "../files/DocumentRow";
import WaitForDependencies from "../WaitForDependencies";
import { RootState } from "../../rootReducer";
import {
  FileSizes,
  SubformFileUpload,
} from "../../forms/subforms/SubformFileUpload";
import {
  fetchSubscriptionById,
  patchSubscriptionById,
} from "../../slices/subscriptionsSlice";

interface InvestorUploadsCardProps {
  subscriptionId: string;
  canUpload: boolean;
}

const InvestorUploadsCard = ({
  subscriptionId,
  canUpload = false,
}: InvestorUploadsCardProps) => {
  const dispatch = useDispatch();

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [subscription, subscriptionId, dispatch]);

  return (
    <Card fluid>
      <WaitForDependencies items={[subscription]}>
        <Card.Content style={{ flexGrow: 0 }}>
          <Card.Header>
            <Grid columns="2">
              <Grid.Column>Investor Uploads</Grid.Column>
              <Grid.Column textAlign={"right"}>
                {canUpload && (
                  <SubformFileUpload
                    maxSize={FileSizes.MB * 50}
                    allowMultiple={true}
                    onSuccess={async (fileIds) => {
                      await dispatch(
                        patchSubscriptionById(subscription.id, {
                          additionalFiles: [
                            ...(subscription.additionalFiles ?? []),
                            ...fileIds,
                          ],
                        })
                      );
                      toast.success(
                        `File${
                          fileIds.length > 1 ? "s" : ""
                        } uploaded to subscription`
                      );
                    }}
                    onError={(errors) => {
                      errors.forEach((error) => {
                        toast.error(error);
                      });
                    }}
                  >
                    {(props) => {
                      return (
                        <Popup
                          trigger={
                            <Icon
                              className={"primaryColor"}
                              link
                              bordered
                              circular
                              loading={props.uploading}
                              name={props.uploading ? "spinner" : "upload"}
                              size="small"
                              onClick={props.triggerFileUpload}
                            />
                          }
                          basic
                          inverted
                          content={"Upload Files"}
                        />
                      );
                    }}
                  </SubformFileUpload>
                )}
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        {canUpload &&
        (!subscription?.additionalFiles ||
          subscription?.additionalFiles.length <= 0) ? (
          <Card.Content textAlign="center">
            <SubformFileUpload
              maxSize={FileSizes.MB * 50}
              allowMultiple={true}
              onSuccess={async (fileIds) => {
                await dispatch(
                  patchSubscriptionById(subscription.id, {
                    additionalFiles: [
                      ...(subscription.additionalFiles ?? []),
                      ...fileIds,
                    ],
                  })
                );
                toast.success(
                  `File${
                    fileIds.length > 1 ? "s" : ""
                  } uploaded to subscription`
                );
              }}
              onError={(errors) => {
                errors.forEach((error) => {
                  toast.error(error);
                });
              }}
            >
              {(props) => {
                return (
                  <Popup
                    trigger={
                      <Button
                        secondary
                        content="Upload File"
                        onClick={props.triggerFileUpload}
                        loading={props.uploading}
                      />
                    }
                    basic
                    inverted
                    content={"Upload Files"}
                  />
                );
              }}
            </SubformFileUpload>
          </Card.Content>
        ) : (
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {subscription?.additionalFiles &&
                  Array.isArray(subscription?.additionalFiles) &&
                  subscription?.additionalFiles.map((doc) => (
                    <DocumentRow
                      key={doc}
                      canDelete={true}
                      fileId={doc}
                      onDelete={async (deletedFileId) => {
                        await dispatch(
                          patchSubscriptionById(subscription.id, {
                            additionalFiles: [
                              ...subscription?.additionalFiles.filter(
                                (doc) => doc !== deletedFileId
                              ),
                            ],
                          })
                        );
                        toast.success(`File deleted from subscription`);
                      }}
                    />
                  ))}
              </Table.Body>
            </Table>
          </Card.Content>
        )}
      </WaitForDependencies>
    </Card>
  );
};

export default InvestorUploadsCard;
