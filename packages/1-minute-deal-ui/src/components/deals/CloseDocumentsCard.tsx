import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { RootState } from "../../rootReducer";
import { fetchDealById, updateDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";

import Log from "../../tools/Log";

import {
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Table,
  Button,
  Label,
  Container,
  Segment,
  Item,
  Transition,
  Loader,
  Card,
  Message,
} from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";

import { DealStatusColors as dsc } from "../../tools/statusColors";
import NumberFormat from "react-number-format";
import moment from "moment";
import {
  fetchClosesList,
  fetchClosesListByDeal,
  updateCloseById,
} from "../../slices/closesSlice";
import { Close } from "../../api/closesApi";
import PdfDocument from "../PdfDocument";
import Document from "../Document";
import { fetchSubscriptionById } from "../../slices/subscriptionsSlice";

interface Props {
  dealId: string;
}

const CloseDocumentsCard = ({ dealId }: Props) => {
  const dispatch = useDispatch();
  const [validCloses, setValidCloses] = useState([]);

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const { isLoading: entitiesLoading, error: entitiesError } = useSelector(
    (state: RootState) => state.entities
  );

  const {
    currentPageCloses,
    currentPageDealId: closePage,
    closesById,
  } = useSelector((state: RootState) => state.closes);

  useEffect(() => {
    const closes = currentPageCloses.map((closeID) => closesById[closeID]);
    const valid = Object.values(closes ?? {}).filter(
      (p) => p.dealId === dealId
    );
    setValidCloses(valid);
  }, [currentPageCloses]);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dealId]);

  useEffect(() => {
    if (dealId) {
      dispatch(fetchClosesListByDeal(dealId));
    }
  }, [dealId, dispatch]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, entity]);

  const { subscriptionsById, isLoading: subscriptionsLoading } = useSelector(
    (state: RootState) => state.subscriptions
  );

  useEffect(() => {
    if (validCloses) {
      for (const validClose of validCloses) {
        for (const subscriptionId of validClose.subscriptions) {
          if (!subscriptionsById[subscriptionId]) {
            dispatch(fetchSubscriptionById(subscriptionId));
          }
        }
      }
    }
  }, [dispatch, validCloses]);

  const deleteFileFromClose = async (deletedFileId) => {
    try {
      for (let i = 0; i < validCloses.length; i++) {
        let close = validCloses[i];
        if (
          close?.files &&
          close?.files.filter((fileId) => fileId === deletedFileId).length > 0
        ) {
          const fileList = [...close.files].filter(
            (val) => val !== deletedFileId
          );
          dispatch(
            updateCloseById({
              ...close,
              files: fileList,
            })
          );
          break;
        }
      }
    } catch (err) {
      return;
    }
  };

  return (
    <>
      {validCloses.length > 0 && (
        <Card>
          <Card.Content>
            <Card.Header>
              <Grid columns="equal">
                <Grid.Column>Close Documents</Grid.Column>
              </Grid>
            </Card.Header>
          </Card.Content>
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {validCloses.map((close: Close) =>
                  close.subscriptions.map((subscriptionId) => (
                    <>
                      {subscriptionsById[subscriptionId]?.closeDoc && (
                        <>
                          <DocumentRow
                            fileId={subscriptionsById[subscriptionId]?.closeDoc}
                            canDelete={false}
                            onDelete={async (deletedFileId) => {
                              await deleteFileFromClose(deletedFileId);
                            }}
                          />
                        </>
                      )}
                    </>
                  ))
                )}
              </Table.Body>
            </Table>
          </Card.Content>
        </Card>
      )}
    </>
  );
};

export default CloseDocumentsCard;
