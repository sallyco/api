import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Tooltip } from "@mui/material";
import numeral from "numeral";
import { Grid, Table, Card, Icon } from "semantic-ui-react";
import NumberFormat from "react-number-format";

import WaitForDependencies from "../WaitForDependencies";
import { RootState } from "../../rootReducer";
import { fetchSubscriptionById } from "../../slices/subscriptionsSlice";
import { DownloadGeneratorFile } from "../../tools/downloadUtils";

interface CapitalAccountStatementCardProps {
  subscriptionId: string;
}

const CapitalAccountStatementCard = ({
  subscriptionId,
}: CapitalAccountStatementCardProps) => {
  const dispatch = useDispatch();
  const [downloading, setDownloading] = useState(false);

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const deal = useSelector((state: RootState) =>
    !subscription ? null : state.deals.dealsById[subscription.dealId]
  );

  const entity = useSelector((state: RootState) =>
    !deal ? null : state.entities.entitiesById[deal.entityId]
  );

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [subscription, subscriptionId, dispatch]);

  return (
    <Card fluid>
      <WaitForDependencies items={[subscription]}>
        <Card.Content style={{ flexGrow: 0 }}>
          <Card.Header>
            <Grid columns="2">
              <Grid.Column>Capital Account Statement</Grid.Column>
              <Grid.Column textAlign={"right"}>
                <Tooltip
                  title="Export Capital Account Statement"
                  placement="bottom"
                  arrow
                >
                  <Icon
                    name={downloading ? "spinner" : "cloud download"}
                    loading={downloading}
                    color={"black"}
                    link
                    bordered
                    circular
                    size="small"
                    onClick={async () => {
                      setDownloading(true);
                      DownloadGeneratorFile(
                        `/subscriptions/${subscriptionId}/capital-account-statement/pdf`,
                        "statement.pdf"
                      );
                      setDownloading(false);
                    }}
                  />
                </Tooltip>
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        {subscription && (
          <Card.Content textAlign="center">
            <Table definition basic compact size="small" unstackable>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>Deal Name</Table.Cell>
                  <Table.Cell>{deal?.name}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Series Name</Table.Cell>
                  <Table.Cell>{entity?.name}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Amount invested</Table.Cell>
                  <Table.Cell>
                    <NumberFormat
                      value={subscription?.amount}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                      data-testid="invested-amount"
                    ></NumberFormat>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Percentage of Ownership</Table.Cell>
                  <Table.Cell>
                    {numeral(
                      subscription?.ownershipPercentageAtClose ?? 0
                    ).format("0.0000%")}
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </Card.Content>
        )}
      </WaitForDependencies>
    </Card>
  );
};

export default CapitalAccountStatementCard;
