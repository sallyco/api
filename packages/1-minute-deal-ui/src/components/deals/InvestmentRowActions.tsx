import React from "react";
import { Dropdown } from "semantic-ui-react";
import { FeatureFlag } from "../featureflags/FeatureFlags";

interface InvestmentRowActionsInterface {
  canComplete: boolean;
  canMoveToCommitted: boolean;
  canDelete: boolean;
  canRefund: boolean;
  canNudge: boolean;
  canReinvite: boolean;
  multiple: boolean;
  noteHandler: () => void;
  nudgeHandler: () => void;
  committedHandler: () => void;
  completeHandler: () => void;
  deleteHandler: () => void;
  refundHandler: () => void;
  reinviteHandler: () => void;
  createSideLetterHandler: () => void;
  deleteSideLetterHandler: () => void;
  resetHandler: () => void;
}

export default function InvestmentRowActions({
  nudgeHandler,
  canComplete = false,
  canMoveToCommitted = false,
  canDelete = false,
  canRefund = false,
  canNudge = false,
  canReinvite = false,
  multiple = false,
  noteHandler = () => { },
  committedHandler = () => { },
  completeHandler = () => { },
  deleteHandler = () => { },
  refundHandler = () => { },
  reinviteHandler = () => { },
  createSideLetterHandler = () => { },
  deleteSideLetterHandler = () => { },
  resetHandler = () => { },
}: InvestmentRowActionsInterface) {
  return (
    <Dropdown
      className="icon"
      icon={"ellipsis vertical"}
      floating
      direction="left"
      trigger={<React.Fragment />}
    >
      <Dropdown.Menu>
        {canNudge && (
          <Dropdown.Item
            text="Nudge Investor"
            key="nudge"
            icon="mail"
            onClick={nudgeHandler}
          />
        )}
        <Dropdown.Item
          text="Add a Note to this Investor"
          key="note"
          icon="pencil"
          onClick={noteHandler}
        />
        {(multiple || canComplete) && (
          <Dropdown.Item
            text="Mark Investment Completed"
            key="complete"
            icon="check circle"
            onClick={completeHandler}
          />
        )}
        {canMoveToCommitted && (
          <Dropdown.Item
            text="Move Back to Committed"
            key="committed"
            icon="chevron circle left"
            onClick={committedHandler}
          />
        )}
        {canRefund && (
          <FeatureFlag name={"investment_refund"}>
            <Dropdown.Item
              text="Refund this Investment"
              key="refund"
              icon="delete"
              onClick={refundHandler}
            />
          </FeatureFlag>
        )}
        {(multiple || canDelete) && (
          <Dropdown.Item
            text="Cancel this Investment"
            key="delete"
            icon="delete"
            onClick={deleteHandler}
          />
        )}
        {canReinvite && (
          <Dropdown.Item
            text="Re-invite this Investor"
            key="reinvite"
            icon="envelope outline"
            onClick={reinviteHandler}
          />
        )}
        <Dropdown.Item
          text="Create side letter for this Investor"
          key="sideLetter"
          icon="pencil"
          onClick={createSideLetterHandler}
        />
        <Dropdown.Item
          text="Delete side letter for this Investor"
          key="deleteSideLetter"
          icon="delete"
          onClick={deleteSideLetterHandler}
        />
        {(process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) !==
          "production" && (
            <Dropdown.Item
              text={
                <span style={{ color: "red" }}>
                  Completely Reset Investment [FOR DEMO/TESTING]
                </span>
              }
              key="reset"
              icon="undo"
              onClick={resetHandler}
            />
          )}
      </Dropdown.Menu >
    </Dropdown >
  );
}
