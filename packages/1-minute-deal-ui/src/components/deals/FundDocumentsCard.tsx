import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { RootState } from "../../rootReducer";
import { fetchDealById, updateDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import {
  Grid,
  Table,
  Button,
  Card,
  Icon,
  Popup,
  Divider,
} from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";
import WaitForDependencies from "../WaitForDependencies";
import { DownloadMassButton } from "../files/MassDownloadButton";
import {
  FileSizes,
  SubformFileUpload,
} from "../../forms/subforms/SubformFileUpload";
import { toast } from "react-toastify";

interface Props {
  dealId: string;
  hasUpload?: boolean;
}

const FundDocumentsCard = ({ dealId, hasUpload = false }: Props) => {
  const dispatch = useDispatch();
  let history = useHistory();

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entity]);

  return (
    <Card>
      <WaitForDependencies items={[deal, entity]}>
        <Card.Content>
          <Card.Header>
            <Grid columns="2">
              <Grid.Column>Fund Documents</Grid.Column>

              <Grid.Column textAlign={"right"}>
                {entity?.entityDocuments && (
                  <Popup
                    trigger={
                      <Icon
                        name="refresh"
                        color={"black"}
                        link
                        bordered
                        circular
                        size="small"
                        onClick={async () => {
                          history.push(
                            `/deals/${deal?.id}/review-docs?generate=true&regenerate=true`
                          );
                        }}
                      />
                    }
                    basic
                    inverted
                    content="Re-generate Documents"
                  />
                )}
                <DownloadMassButton
                  files={[
                    entity?.entityDocuments?.operatingAgreement,
                    entity?.entityDocuments?.privatePlacementMemorandum,
                    entity?.entityDocuments?.subscriptionAgreement,
                  ]
                    .concat(deal?.files ?? [])
                    .concat(deal?.organizerFiles ?? [])}
                />
                {hasUpload && (
                  <SubformFileUpload
                    maxSize={FileSizes.MB * 50}
                    onSuccess={async (fileIds) => {
                      const upload = deal.organizerFiles
                        ? [...deal.organizerFiles, ...fileIds]
                        : fileIds;
                      await dispatch(
                        updateDealById({
                          id: deal.id,
                          organizerFiles: upload,
                        })
                      );
                      toast.success(
                        `File${fileIds.length > 1 ? "s" : ""} uploaded to deal`
                      );
                    }}
                    onError={(errors) => {
                      errors.forEach((error) => {
                        toast.error(error);
                      });
                    }}
                  >
                    {(props) => {
                      return (
                        <Popup
                          trigger={
                            <Icon
                              className={"primaryColor"}
                              link
                              bordered
                              circular
                              loading={props.uploading}
                              name={props.uploading ? "spinner" : "upload"}
                              size="small"
                              onClick={props.triggerFileUpload}
                            />
                          }
                          basic
                          inverted
                          content={"Upload Files"}
                        />
                      );
                    }}
                  </SubformFileUpload>
                )}
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        {deal?.status === "DRAFT" || !entity?.entityDocuments ? (
          <Card.Content textAlign="center">
            <Button
              as={Link}
              to={`/deals/${deal?.id}/review-docs?generate=true`}
              secondary
              content="Generate Fund Documents"
            />
          </Card.Content>
        ) : (
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                <DocumentRow
                  fileId={entity?.entityDocuments?.operatingAgreement}
                  documentTitle={"Operating Agreement"}
                />
                <DocumentRow
                  fileId={entity?.entityDocuments?.privatePlacementMemorandum}
                  documentTitle={"Private Placement Memorandum"}
                />
                <DocumentRow
                  fileId={entity?.entityDocuments?.subscriptionAgreement}
                  documentTitle={"Subscription Agreement"}
                />
                {entity?.entityDocuments?.einLetter && (
                  <DocumentRow
                    fileId={entity?.entityDocuments?.einLetter}
                    documentTitle={"EIN Letter"}
                  />
                )}
                <Table.Row>
                  <Table.Cell colspan={3}></Table.Cell>
                </Table.Row>
                {deal?.organizerFiles &&
                  deal.organizerFiles.map((fileId) => (
                    <DocumentRow
                      key={fileId}
                      fileId={fileId}
                      canDelete={true}
                      onDelete={async (deletedFileId) => {
                        const fileList = [...deal.organizerFiles].filter(
                          (val) => val !== deletedFileId
                        );
                        await dispatch(
                          updateDealById({
                            ...deal,
                            organizerFiles: fileList,
                          })
                        );
                        toast.success(`File deleted from deal`);
                      }}
                    />
                  ))}
              </Table.Body>
            </Table>
          </Card.Content>
        )}
      </WaitForDependencies>
    </Card>
  );
};

export default FundDocumentsCard;
