import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import PropTypes from "prop-types";
import { RootState } from "../../rootReducer";

import { Icon, Button, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

import {
  setDealsListPage,
  setDealsListDisplayType,
} from "../../slices//dealsListDisplaySlice";

import { setDealsListOpType } from "../../slices/dealsListOperationSlice";

export default function DealsListActions({}) {
  const dispatch = useDispatch();

  const { page, displayType } = useSelector(
    (state: RootState) => state.dealsListDisplay
  );

  const { opType } = useSelector(
    (state: RootState) => state.dealsListOpeartions
  );

  return (
    <Menu borderless secondary className="nav-bar">
      <Menu.Item>
        <Link to="/profiles/create">
          <Button primary size="tiny">
            + New Deal
          </Button>
        </Link>
      </Menu.Item>

      <Menu.Item>
        <Button.Group icon size="tiny">
          <Button
            icon
            {...(opType === "search" && { className: "primaryColor" })}
            aria-label="search"
            onClick={() => dispatch(setDealsListOpType({ opType: "search" }))}
          >
            <Icon name="search" />
          </Button>
          <Button
            icon
            {...(opType === "filter" && { className: "primaryColor" })}
            aria-label="filter"
            onClick={() => dispatch(setDealsListOpType({ opType: "filter" }))}
          >
            <Icon name="filter" />
          </Button>
          <Button
            icon
            {...(opType === "sort" && { className: "primaryColor" })}
            aria-label="sort"
            onClick={() => dispatch(setDealsListOpType({ opType: "sort" }))}
          >
            <Icon name="sort" />
          </Button>
        </Button.Group>
      </Menu.Item>

      <Menu.Item>
        <Button.Group icon size="tiny">
          <Button
            icon
            {...(displayType === "cards" && { className: "primaryColor" })}
            onClick={() =>
              dispatch(setDealsListDisplayType({ displayType: "cards" }))
            }
            aria-label="card view"
          >
            <Icon name="th" />
          </Button>
          <Button
            icon
            {...(displayType === "table" && { className: "primaryColor" })}
            onClick={() =>
              dispatch(setDealsListDisplayType({ displayType: "table" }))
            }
            aria-label="card view"
          >
            <Icon name="list" />
          </Button>
        </Button.Group>
      </Menu.Item>
    </Menu>
  );
}
