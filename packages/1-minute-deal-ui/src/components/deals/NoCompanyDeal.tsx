import React from "react";
import {
  Container,
  Card,
  Transition,
  Segment,
  Header,
  Icon,
} from "semantic-ui-react";

export default function NoCompanyDeal() {
  return (
    <Transition animation={"fade"}>
      <Container textAlign="center">
        <Card.Group itemsPerRow="one" stackable>
          <Card>
            <Card.Content>
              <Segment placeholder basic>
                <Header icon>
                  <Icon name="dollar" color="grey" circular />
                  No Deals to Display
                </Header>
              </Segment>
            </Card.Content>
          </Card>
        </Card.Group>
      </Container>
    </Transition>
  );
}
