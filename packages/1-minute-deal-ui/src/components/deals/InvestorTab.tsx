import React from "react";
import { Card, Container, Transition, Segment } from "semantic-ui-react";

import SubscribedCard from "./SubscribedCard";
import InvestmentsCard from "./InvestmentsCard";
import InvitesCard from "./InvitesCard";
import DealCard from "./DealCard";

interface Props {
  dealId: string;
}

export const InvestorTab = ({ dealId }: Props) => {
  return (
    <Transition>
      <Card.Group itemsPerRow="one" stackable>
        <SubscribedCard dealId={dealId} />
        <InvitesCard dealId={dealId} />
        <InvestmentsCard dealId={dealId} />
      </Card.Group>
    </Transition>
  );
};
