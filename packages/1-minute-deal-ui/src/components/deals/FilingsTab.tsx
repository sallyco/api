import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  List,
  ListItem,
  Typography,
} from "@mui/material";
import { Theme } from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import moment from "moment";
import React from "react";
import { Container, Segment, Transition } from "semantic-ui-react";
import numeral from "numeral";
import useSWR from "swr";
import { fetcher } from "../../swrConnector";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

interface Props {
  entityId: string;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    paddingBottom: 0,
    paddingTop: theme.spacing(1),
  },
  content: {
    paddingTop: 0,
  },
  listItem: {
    padding: theme.spacing(1, 0),
    justifyContent: "space-between",
  },
  sublistItem: {
    padding: 0,
    justifyContent: "space-between",
  },
  sublistDivider: {
    marginTop: theme.spacing(1),
  },
}));

export const FilingsTab = ({ entityId }: Props) => {
  const classes = useStyles();

  const { data, error } = useSWR(
    () => baseUrl + `/entities-to-file/${entityId}`,
    fetcher
  );

  const { data: formD, error: ferr } = useSWR(
    () => baseUrl + `/entities/${entityId}/form-d`,
    fetcher
  );

  const renderHeader = (value) => (
    <CardHeader
      className={classes.header}
      title={
        <Typography display="block" variant="subtitle1" color="textPrimary">
          {value}
        </Typography>
      }
    />
  );

  const renderItem = (key, value) => (
    <React.Fragment>
      <ListItem className={classes.listItem} disableGutters>
        <Typography variant="subtitle2" color="textPrimary">
          {key}
        </Typography>
        <Typography variant="subtitle2" color="textSecondary">
          {value}
        </Typography>
      </ListItem>
      <Divider className={classes.sublistDivider} />
    </React.Fragment>
  );

  return (
    <>
      {data && (
        <Transition>
          <Container>
            <Card className={classes.root}>
              {renderHeader("Filing Status")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Central Index Key (CIK)", formD?.cik)}
                  {renderItem("CIK Password", formD?.cikPassword)}
                  {renderItem("CIK Confirmation Code", formD?.ccc)}
                  {renderItem("Accession Number", formD?.accessionNumber)}
                  {renderItem("PMAC", formD?.pmac)}
                  {renderItem("File Number", formD?.fileNumber)}
                  {renderItem(
                    "Filed Date",
                    formD?.filedDate
                      ? moment(formD?.filedDate).format("MMM DD, YYYY")
                      : ""
                  )}
                  {renderItem(
                    "Blue Sky Paid Date",
                    formD?.blueSkyPaidDate
                      ? moment(formD?.blueSkyPaidDate).format("MMM DD, YYYY")
                      : ""
                  )}
                  {renderItem("Blue Sky Reciept Number", formD?.blueSkyReceipt)}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("Form D Issuer's Information")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Contact Person for this Filing",
                    "Richard Thoms"
                  )}
                  {renderItem("Phone", "(801) 419-0677")}
                  {renderItem("Email Address", "funds@glassboardtech.com")}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 1. Issuer's Identity")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("CIK (Filer ID Number)", data?.formD?.cik)}
                  {renderItem("CCC (Confermation Code)", data?.formD?.ccc)}
                  {renderItem("Name of Issuer", data?.name)}
                  {renderItem(
                    "Jurisdiction",
                    data?.stateOfFormation ?? "Delaware"
                  )}
                  {renderItem("Entity Type", data?.entityType)}
                  {renderItem("Year of Incorporation", moment().format("YYYY"))}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader(
                "ITEM 2. Principal Place of Buisness and Contact Information"
              )}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Street Address 1",
                    data?.manager?.address?.address1
                  )}
                  {renderItem(
                    "Street Address 2",
                    data?.manager?.address?.address2
                  )}
                  {renderItem("City", data?.manager?.address?.city)}
                  {renderItem("State", data?.manager?.address?.state)}
                  {renderItem(
                    "ZIP/Postal Code",
                    data?.manager?.address?.postalCode
                  )}
                  {renderItem("Phone No.", data?.manager?.phone)}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 3. Related Persons")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Last Name", data?.manager?.name)}
                  {renderItem("First Name", "")}
                  {renderItem("Middle Name", "")}
                  {renderItem(
                    "Street Address 1",
                    data?.manager?.address?.address1
                  )}
                  {renderItem(
                    "Street Address 2",
                    data?.manager?.address?.address2
                  )}
                  {renderItem("City", data?.manager?.address?.city)}
                  {renderItem("State", data?.manager?.address?.state)}
                  {renderItem(
                    "ZIP/Postal Code",
                    data?.manager?.address?.postalCode
                  )}
                  {renderItem("Phone No.", data?.manager?.phone)}
                  {renderItem("Relationship(s)", "Manager")}
                  {renderItem("Clarification of Response (if necessary)", "")}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 4. Industry Group (Select One)")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Banking and Financial Services",
                    "Pooled Investment Fund"
                  )}
                  {renderItem("Fund Type", "Other Investment Fund")}
                  {renderItem(
                    "Is the Issuer registered as an investment company under the Investment Company Act of 1940?",
                    "No"
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 5. Issuer Size")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    'Aggregate Net Asset Value Range (for issuer specifying "Hedge" or "Other Investment" fund in Item 4 above)',
                    "Decline to Disclose"
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader(
                "ITEM 6. Federal Exemptions and Exclusions Claimed"
              )}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Federal Exemptions and Exclusions Claimed (Select all that Apply)",
                    `Rule 506(${data.regDExemption ?? "b"})`
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 7. Type of Filing")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Type of Filing", "New Notice")}
                  {renderItem(
                    "Date of First Sale in this offering",
                    "First Sale Has Occurred"
                  )}
                  {renderItem("Date", data?.close?.createdAt)}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 8. Duration of Offering")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Does the issuer intend this offering to last more than one year?",
                    "No"
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 9. Type(s) of Securities Offered")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Types of Securities Offered (Select all that apply)",
                    "Pooled Investment Fund Interests"
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 10. Business Combination Transaction")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Is this offering being made in connection with a business combination transaction, such as a merger, acquisition or exchange offer?",
                    "No"
                  )}
                  {renderItem("Clarification of Response (if necessary)", "")}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 11. Minimum Investment")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Minimum investment accepted from any outside investor",
                    numeral(data?.minInvestmentAmount).format("$0,0")
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 12. Sales Compensation")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Recipient", "N/A")}
                  {renderItem("CRD NUMBER (Recipient)", "N/A")}
                  {renderItem("No Broker or Dealer", "N/A")}
                  {renderItem("(Associated) Broker or Dealer", "N/A")}
                  {renderItem("CRD NUMBER (Associate)", "N/A")}
                  {renderItem("Street Address 1", "N/A")}
                  {renderItem("Street Address 2", "N/A")}
                  {renderItem("City", "N/A")}
                  {renderItem("State", "N/A")}
                  {renderItem("ZIP Code", "N/A")}
                  {renderItem("States of Solicitation", "N/A")}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 13. Offering and Sales Amounts")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "(a) Total offering Amount",
                    numeral(
                      data.subscriptions
                        ? data.subscriptions
                            .reduce(
                              (totalOffering, subscription) =>
                                subscription?.amount > 0
                                  ? totalOffering +
                                    parseFloat(subscription.amount)
                                  : totalOffering,
                              0
                            )
                            .toString()
                        : "0"
                    ).format("$0,0")
                  )}
                  {renderItem("Indefinite", "")}
                  {renderItem(
                    "(b) Total Amount Sold",
                    numeral(
                      data.subscriptions
                        ? data.subscriptions
                            .reduce(
                              (totalOffering, subscription) =>
                                subscription?.amount > 0
                                  ? totalOffering +
                                    parseFloat(subscription.amount)
                                  : totalOffering,
                              0
                            )
                            .toString()
                        : "0"
                    ).format("$0,0")
                  )}
                  {renderItem(
                    "(c) Total Remaining to be Sold ((a) - (b))",
                    "0"
                  )}
                  {renderItem("Indefinite", "")}
                  {renderItem("Clarification of Response (if necessary)", "")}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("ITEM 14. Investors")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Have securities been sold, or may securities be sold to persons who do not qualify as accredited investors?",
                    "No"
                  )}
                  {renderItem(
                    "Enter the total number of investors who already have invested in the offering:",
                    data.subscriptions
                      ? data.subscriptions.length.toString()
                      : ""
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader(
                "ITEM 15. Sales Commissions and Finders' Fees Expenses"
              )}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Sales Commissions", "0")}
                  {renderItem("Estimate", "")}
                  {renderItem("Finder's Fees", "0")}
                  {renderItem("Estimate", "")}
                  {renderItem("Clarification of Response (if necessary)", "")}
                </List>
              </CardContent>
            </Card>

            <Card className={classes.root}>
              {renderHeader("ITEM 16. Use of Proceeds")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem(
                    "Provide the amount of the gross proceeds of the offering that has been or is proposed to be used for payments to any of the persons required to be named as executive officers, directors or promoters in response to Item 3 above. If the amount is unknown, provide an estimate and check the box next to the amount ",
                    data?.close?.statement?.spvFee ?? "0"
                  )}
                  {renderItem("Estimate", "")}
                  {renderItem(
                    "Clarification of Response (if necessary)",
                    "This amount represents a one-time cost to cover fund organizational and operating expenses."
                  )}
                </List>
              </CardContent>
            </Card>
            <Divider />
            <Card className={classes.root}>
              {renderHeader("Signature and Submission")}
              <CardContent className={classes.content}>
                <List>
                  {renderItem("Signature", "Richard Thoms")}
                  {renderItem("Name of Signature", "Richard Thoms")}
                  {renderItem("Title", "Officer of the Issuer's Manager")}
                  {renderItem("Date", moment().format("YYYY-MM-DD"))}
                </List>
              </CardContent>
            </Card>
          </Container>
        </Transition>
      )}
    </>
  );
};
