import React from "react";
import asset from "../../assets/images/loadingDocsImage.png";
import PropTypes from "prop-types";
import {
  Image,
  Header,
  Progress,
  Transition,
  Container,
} from "semantic-ui-react";

export default function DocsSuccess() {
  return (
    <Transition animation={"fade left"} duration={750}>
      <Container>
        <Image src={asset.src} centered size="small" />
        <Header as="h3" inverted textAlign="center">
          You&#39;ve approved your docs
        </Header>
        <Header as="h2" inverted textAlign="center">
          Congratulations!
          <Header.Subheader>Your deal has been created</Header.Subheader>
        </Header>
        <Header as="h3" inverted textAlign="center">
          Get ready to invite investors...
        </Header>
      </Container>
    </Transition>
  );
}
