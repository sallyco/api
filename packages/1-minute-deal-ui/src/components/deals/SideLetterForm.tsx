import React from "react";
import { Field, Formik } from "formik";
import _ from "lodash";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Divider, Form } from "semantic-ui-react";
import * as Yup from "yup";

import { Deal } from "../../api/dealsApi";
import { MaskedFormField } from "../../components/FormField";
import SubformManagementFee, {
  testValidFeeAmount,
  validationSchema,
  fixAmountValue,
} from "../../forms/subforms/SubformManagementFee";
import { regexSkipDigitAndDecimals } from "../../forms/validation/ValidationHelper";
import { RootState } from "../../rootReducer";

interface SideLetterFormProps {
  deal: Deal;
  onSubmit: (data) => void;
  sideLetter?: any;
}

const transformCarryToNumeral = (value) => {
  return typeof value === "number"
    ? value
    : Number(value.replace(/[^0-9.]+/g, ""));
};

export default function SideLetterForm({
  deal,
  onSubmit,
  sideLetter,
}: SideLetterFormProps) {
  const entity = useSelector(
    (state: RootState) => state.entities.entitiesById[deal.entityId]
  );

  const formValidationSchema = () => {
    const managementFeeValidationSchema = validationSchema(
      deal?.targetRaiseAmount ?? 0
    );
    const schema = Yup.object()
      .shape({
        enableCarryPercentageEdit: Yup.boolean(),
        enableManagementFeeEdit: Yup.boolean(),
        organizerCarryPercentage: Yup.number()
          .transform((value, ogValue) => {
            if (typeof ogValue === "string") {
              return Number(ogValue.replace(regexSkipDigitAndDecimals, ""));
            }
            return ogValue;
          })
          .min(0)
          .max(20),
        ...(Array.isArray(deal?.additionalCarryRecipients) &&
        deal?.additionalCarryRecipients?.length > 0
          ? deal.additionalCarryRecipients
              .map((carry, index) => {
                return {
                  [carry.individual.name]: Yup.number().transform(
                    (value, ogValue) => {
                      if (typeof ogValue === "string") {
                        return Number(
                          ogValue.replace(regexSkipDigitAndDecimals, "")
                        );
                      }
                      return ogValue;
                    }
                  ),
                };
              })
              .reduce((obj, item) => Object.assign(obj, { ...item }), {})
          : {}),
        managementFee: Yup.object().when("enableManagementFeeEdit", {
          is: true,
          then: managementFeeValidationSchema.managementFee,
          otherwise: Yup.object(),
        }),
      })
      .test(
        "Check if amount is above fee %",
        "Amount cannot be above 4.5% of the target",
        (obj) => {
          if (!obj.enableManagementFeeEdit) {
            return true;
          }
          return testValidFeeAmount(
            deal?.targetRaiseAmount ?? 0,
            obj.managementFee["amount"],
            "managementFee.amount"
          );
        }
      );

    return schema;
  };

  const initialData = () => {
    if (!!sideLetter && _.isEqual(sideLetter, {}) === false) {
      return {
        ...(sideLetter.carryPercent ?? {}),
        managementFee: sideLetter.managementFee ?? {},
      };
    }
    return {
      organizerCarryPercentage: deal?.organizerCarryPercentage ?? 0,
      ...(Array.isArray(deal?.additionalCarryRecipients) &&
      deal?.additionalCarryRecipients?.length > 0
        ? deal.additionalCarryRecipients
            .map((carry, index) => {
              return {
                [carry.individual.name]: carry.carryPercentage ?? 0,
              };
            })
            .reduce((obj, item) => Object.assign(obj, { ...item }), {})
        : {}),
      managementFee: entity?.managementFee ?? {},
    };
  };

  const generateInitialValues = () => {
    const schema = {
      ...initialData(),
      enableCarryPercentageEdit: false,
      enableManagementFeeEdit: false,
    };
    return schema;
  };

  const getUpdatedData = (data, initialData) => {
    const dataToUpdate = {
      carryPercent: {
        ...(sideLetter.carryPercent ?? {}),
        organizerCarryPercentage: transformCarryToNumeral(
          data?.organizerCarryPercentage ?? 0
        ),
        ...(Array.isArray(deal?.additionalCarryRecipients) &&
        deal?.additionalCarryRecipients?.length > 0
          ? deal.additionalCarryRecipients
              .map((carry, index) => {
                return {
                  [carry.individual.name]: transformCarryToNumeral(
                    data?.[carry.individual.name] ?? 0
                  ),
                };
              })
              .reduce((obj, item) => Object.assign(obj, { ...item }), {})
          : {}),
      },
      managementFee: {
        ...(sideLetter.managementFee ?? {}),
        ...data?.managementFee,
        amount: fixAmountValue(data?.managementFee),
      },
    };

    const dataNotChanged = _.isEqual(initialData, dataToUpdate);
    if (dataNotChanged) {
      toast.warning(
        "None of the data is edited. Please update the data to continue."
      );
      return false;
    }
    console.info("data", data);
    console.info("dataToUpdate", dataToUpdate);
    return dataToUpdate;
  };

  const onFormSubmit = async (data) => {
    const initValues = initialData();

    if (!!sideLetter && _.isEqual(sideLetter, {}) === false) {
      const updatedSideLetter = getUpdatedData(data, initValues);
      if (!updatedSideLetter) {
        return;
      }

      await onSubmit(updatedSideLetter);
      return;
    }

    const dataToUpdate = {
      ...(data?.enableCarryPercentageEdit
        ? {
            carryPercent: {
              organizerCarryPercentage: transformCarryToNumeral(
                data?.organizerCarryPercentage ?? 0
              ),
              ...(Array.isArray(deal?.additionalCarryRecipients) &&
              deal?.additionalCarryRecipients?.length > 0
                ? deal.additionalCarryRecipients
                    .map((carry, index) => {
                      return {
                        [carry.individual.name]: transformCarryToNumeral(
                          data?.[carry.individual.name] ?? 0
                        ),
                      };
                    })
                    .reduce((obj, item) => Object.assign(obj, { ...item }), {})
                : {}),
            },
          }
        : {}),
      ...(data?.enableManagementFeeEdit
        ? {
            managementFee: {
              ...data?.managementFee,
              amount: fixAmountValue(data?.managementFee),
            },
          }
        : {}),
    };

    const carryCheckData = { ...initValues };
    delete carryCheckData.managementFee;

    const carryNotEdited = _.isEqual(carryCheckData, dataToUpdate.carryPercent);
    const managementFeeNotEdited = _.isEqual(
      initValues.managementFee,
      dataToUpdate.managementFee
    );

    if (carryNotEdited) {
      delete dataToUpdate.carryPercent;
    }
    if (managementFeeNotEdited) {
      delete dataToUpdate.managementFee;
    }
    if (_.isEqual(dataToUpdate, {})) {
      toast.warning(
        "None of the data is edited. Please update the data to continue."
      );
      return;
    }

    await onSubmit(dataToUpdate);
  };

  return (
    <>
      <Formik
        initialValues={generateInitialValues()}
        enableReinitialize={false}
        validationSchema={formValidationSchema()}
        onSubmit={onFormSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} className="dark-labels">
            <label>
              <Field
                type="checkbox"
                name="enableCarryPercentageEdit"
                id="enableCarryPercentageEdit"
                style={{ margin: "0px 0px 20px 0px" }}
              />
              {"   Change carry percent"}
            </label>
            <MaskedFormField
              id="organizerCarryPercentage"
              name="organizerCarryPercentage"
              thousandSeparator={true}
              allowNegative={false}
              decimalScale={2}
              type="tel"
              label="Organizer Carry Percentage"
              placeholder={"0.00%"}
              suffix={"%"}
              disabled={!props.values.enableCarryPercentageEdit}
            />
            {Array.isArray(deal?.additionalCarryRecipients) &&
              deal?.additionalCarryRecipients?.length > 0 &&
              deal.additionalCarryRecipients.map((carry, index) => (
                <MaskedFormField
                  id={carry.individual.name}
                  name={carry.individual.name}
                  thousandSeparator={true}
                  allowNegative={false}
                  decimalScale={2}
                  type="tel"
                  label={carry.individual.name}
                  placeholder={"0.00%"}
                  suffix={"%"}
                  key={index}
                  disabled={!props.values.enableCarryPercentageEdit}
                />
              ))}
            <Divider />
            <label>
              <Field
                type="checkbox"
                name="enableManagementFeeEdit"
                id="enableManagementFeeEdit"
                style={{ margin: "0px 0px 20px 0px" }}
              />
              {"   Change management fee"}
            </label>
            <div
              style={
                !props.values.enableManagementFeeEdit
                  ? { pointerEvents: "none", opacity: "0.4" }
                  : {}
              }
            >
              <SubformManagementFee managementFee={entity?.managementFee} />
            </div>
            <Divider hidden />
            <Button
              secondary
              fluid
              content="Submit"
              type="submit"
              disabled={
                !props.values.enableCarryPercentageEdit &&
                !props.values.enableManagementFeeEdit
              }
              data-testid="submit-button"
            />
          </Form>
        )}
      </Formik>
    </>
  );
}
