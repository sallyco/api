import React from "react";
import { Card, Container, Divider, Transition } from "semantic-ui-react";
import { DealStatusColors as dsc } from "../../tools/statusColors";

import DealDescriptionCard from "./DealDescriptionCard";
import DealDetailsCard from "./DealDetailsCard";
import CarryRecipientsCard from "./CarryRecipientsCard";
import ManagementFeeCard from "./ManagementFeeCard";
import ExpenseReserveCard from "./ExpenseReserveCard";
import { Box } from "@mui/material";
import { DealLinkShareCard } from "./DealLinkShareCard";
import { FeatureFlag, useFeatureFlag } from "../featureflags/FeatureFlags";

interface Props {
  dealId: string;
}

export const DealTab = ({ dealId }: Props) => {
  const expenseReserveEnabled = useFeatureFlag("expense_reserve");
  return (
    <Transition>
      <Container>
        <Box display={"flex"} flexDirection={"column"}>
          <DealDetailsCard dealId={dealId} />
          <DealDescriptionCard dealId={dealId} />
          <Card.Group itemsPerRow={expenseReserveEnabled ? 3 : 2} stackable>
            <CarryRecipientsCard dealId={dealId} />
            <ManagementFeeCard dealId={dealId} />
            <FeatureFlag name={"expense_reserve"}>
              <ExpenseReserveCard dealId={dealId} />
            </FeatureFlag>
          </Card.Group>
        </Box>
      </Container>
    </Transition>
  );
};
