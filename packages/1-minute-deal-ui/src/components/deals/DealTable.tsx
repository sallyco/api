import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Deal } from "../../api/dealsApi";
import moment from "moment";
import NumberFormat from "react-number-format";
import { TableWithTools } from "../common/TableWithTools";

interface Props {
  deals: Deal[];
  column?: any;
  direction?: any;
}

const DealTable = ({ deals, column, direction }: Props) => {
  const history = useHistory();
  const handleRowClick = (e) => {
    history.push(`/deals/${e.currentTarget.id}`);
  };

  const [state, setState] = useState({
    column: column,
    data: deals,
    direction: direction,
  });

  function calculateTimeRemaining(estimatedCloseDate) {
    const oneDay = 24 * 60 * 60 * 1000;
    let timeRemaining = 0;
    if (estimatedCloseDate !== undefined) {
      const closingDate = new Date(
        estimatedCloseDate.slice(0, 4),
        estimatedCloseDate.slice(5, 7) - 1,
        estimatedCloseDate.slice(8, 10),
        0,
        0,
        0,
        0
      );
      const today = new Date();
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);
      today.setMilliseconds(0);
      if ((closingDate.getTime() - today.getTime()) / oneDay > 0) {
        timeRemaining = Math.ceil(
          (closingDate.getTime() - today.getTime()) / oneDay
        );
      } else {
        timeRemaining = 0;
      }
    }
    return timeRemaining;
  }

  return (
    <TableWithTools
      data={state.data}
      columnDefinitions={[
        {
          key: "name",
          headerTitle: "Deal Name",
          rowDisplayValue: (rowData) => rowData["name"],
        },
        {
          key: "estimatedCloseDate",
          headerTitle: "Closing Date",
          rowDisplayValue: (rowData) =>
            moment.utc(rowData["estimatedCloseDate"]).format("MMMM DD, YYYY"),
        },
        {
          key: "targetRaiseAmount",
          headerTitle: "Target Raise",
          // eslint-disable-next-line react/display-name
          rowDisplayValue: (rowData) => (
            <NumberFormat
              value={rowData["targetRaiseAmount"] ?? 0}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
              name={"targetRaiseAmount"}
            />
          ),
        },
        {
          key: "estimatedCloseDate",
          headerTitle: "Days to Close",
          rowDisplayValue: (rowData) =>
            calculateTimeRemaining(rowData["estimatedCloseDate"]),
        },
        {
          key: "status",
          headerTitle: "Status",
          rowDisplayValue: (rowData) => rowData["status"],
        },
      ]}
      direction={state.direction}
      entityName="deals"
      selectedColumn={state.column}
      exportDataMap={(deal) => ({
        "Deal Name": deal.name,
        "Closing Date": moment
          .utc(deal.estimatedCloseDate)
          .format("MMMM DD, YYYY"),
        "Target Raise": deal.targetRaiseAmount ?? 0,
        "Days to Close": calculateTimeRemaining(deal.estimatedCloseDate),
        Status: deal.status,
      })}
      rowProps={{
        onClick: handleRowClick,
      }}
    />
  );
};

export default DealTable;
