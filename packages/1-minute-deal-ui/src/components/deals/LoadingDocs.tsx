import React from "react";
import asset from "../../assets/images/loadingDocsImage.png";
import { Image, Header, Progress } from "semantic-ui-react";

interface Props {
  value: number;
  total: number;
  message: string;
}

export default function LoadingDocs({ value, total, message }: Props) {
  return (
    <>
      <Image src={asset.src} centered size="small" />
      <Header as="h2" inverted textAlign="center">
        Generating Documents
      </Header>
      <Progress value={value} total={total} progress="ratio" active success />
      <Header as="h3" inverted textAlign="center">
        {message}
      </Header>
    </>
  );
}
