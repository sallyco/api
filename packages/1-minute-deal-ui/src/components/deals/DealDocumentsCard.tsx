import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";

import { RootState } from "../../rootReducer";
import { fetchDealById, updateDealById } from "../../slices/dealsSlice";
import { uploadFileToServer } from "../../slices/filesSlice";

import Log from "../../tools/Log";

import {
  Grid,
  Icon,
  Table,
  Segment,
  Card,
  Popup,
  Button,
  Divider,
} from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";
import {
  SubformFileUpload,
  FileSizes,
} from "../../forms/subforms/SubformFileUpload";
import WaitForDependencies from "../WaitForDependencies";
import { Alert } from "@mui/material";
import { Box } from "@mui/material";

interface Props {
  dealId: string;
  title?: string;
  hasUpload?: boolean;
  hasDelete?: boolean;
}

const DealDocumentsCard = ({
  dealId,
  title = "Deal Documents",
  hasUpload = false,
  hasDelete = false,
}: Props) => {
  const dispatch = useDispatch();
  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  return (
    <WaitForDependencies items={[deal]}>
      <Card fluid>
        <Card.Content style={{ flexGrow: 0 }}>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column css={{ flexGrow: 0 }} width={13}>
                {title}
              </Grid.Column>
              {hasUpload && (
                <Grid.Column textAlign="right">
                  <SubformFileUpload
                    maxSize={FileSizes.MB * 50}
                    onSuccess={async (fileIds) => {
                      const upload = deal.files
                        ? [...deal.files, ...fileIds]
                        : fileIds;
                      await dispatch(
                        updateDealById({
                          id: dealId,
                          files: upload,
                        })
                      );
                      toast.success(
                        `File${fileIds.length > 1 ? "s" : ""} uploaded to deal`
                      );
                    }}
                    onError={(errors) => {
                      errors.forEach((error) => {
                        toast.error(error);
                      });
                    }}
                  >
                    {(props) => {
                      return (
                        <Popup
                          trigger={
                            <Icon
                              className={"primaryColor"}
                              link
                              bordered
                              circular
                              loading={props.uploading}
                              name={props.uploading ? "spinner" : "upload"}
                              size="small"
                              onClick={props.triggerFileUpload}
                            />
                          }
                          basic
                          inverted
                          content={"Upload Files"}
                        />
                      );
                    }}
                  </SubformFileUpload>
                </Grid.Column>
              )}
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          {!deal?.files || deal?.files?.length === 0 ? (
            <Box
              display={"flex"}
              flexDirection={"column"}
              alignItems={"center"}
            >
              <Alert severity={"info"}>
                There are no additional documents for this deal
              </Alert>
              <Divider hidden />
              <SubformFileUpload
                maxSize={FileSizes.MB * 50}
                onSuccess={async (fileIds) => {
                  const upload = deal.files
                    ? [...deal.files, ...fileIds]
                    : fileIds;
                  await dispatch(
                    updateDealById({
                      id: dealId,
                      files: upload,
                    })
                  );
                  toast.success(
                    `File${fileIds.length > 1 ? "s" : ""} uploaded to deal`
                  );
                }}
                onError={(errors) => {
                  errors.forEach((error) => {
                    toast.error(error);
                  });
                }}
              >
                {(props) => {
                  return (
                    <Popup
                      trigger={
                        <Button
                          secondary
                          content="Upload Files for Investors"
                          onClick={props.triggerFileUpload}
                          loading={props.uploading}
                          style={{
                            whiteSpace: "nowrap",
                          }}
                        />
                      }
                      basic
                      inverted
                      content={"Upload Files for Investors"}
                    />
                  );
                }}
              </SubformFileUpload>
            </Box>
          ) : (
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {deal.files.map((fileId) => (
                  <DocumentRow
                    key={fileId}
                    fileId={fileId}
                    canDelete={hasDelete}
                    onDelete={async (deletedFileId) => {
                      const fileList = [...deal.files].filter(
                        (val) => val !== deletedFileId
                      );
                      await dispatch(
                        updateDealById({
                          id: dealId,
                          files: fileList,
                        })
                      );
                      toast.success(`File deleted from deal`);
                    }}
                  />
                ))}
              </Table.Body>
            </Table>
          )}
        </Card.Content>
      </Card>
    </WaitForDependencies>
  );
};

export default DealDocumentsCard;
