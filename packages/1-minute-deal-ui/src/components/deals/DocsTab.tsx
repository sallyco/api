import React from "react";
import { Card, Container, Transition } from "semantic-ui-react";
import FundDocumentsCard from "./FundDocumentsCard";
import PurchaseAgreementCard from "./PurchaseAgreementCard";
import DealDocumentsCard from "./DealDocumentsCard";
import CloseDocumentsCard from "./CloseDocumentsCard";
import CompanyDocumentsCard from "./CompanyDocumentsCard";
import EntityDocumentsCard from "./EntityDocumentsCard";
import SupportingDocumentsCard from "./SupportingDocumentsCard";
import { FeatureFlag } from "../featureflags/FeatureFlags";
import DocsFromInvestorsCard from "./DocsFromInvestors";

interface Props {
  dealId: string;
}

export const DocsTab = ({ dealId }: Props) => {
  return (
    <Transition>
      <Container>
        <Card.Group itemsPerRow="two" stackable>
          <FundDocumentsCard dealId={dealId} hasUpload={true} />
          <PurchaseAgreementCard dealId={dealId} />
          <DealDocumentsCard
            dealId={dealId}
            title="Uploaded for Investors"
            hasUpload={true}
            hasDelete={true}
          />
          {/*
          <DealDocumentsCard dealId={dealId} title="Your Private Documents" />
*/}
          <CloseDocumentsCard dealId={dealId} />
          <FeatureFlag name={"distributions"}>
            <SupportingDocumentsCard dealId={dealId} />
          </FeatureFlag>
        </Card.Group>
        <Card.Group itemsPerRow={"two"} stackable>
          <CompanyDocumentsCard dealId={dealId} hasDelete={true} />
          <EntityDocumentsCard dealId={dealId} />
        </Card.Group>
        <Card.Group itemsPerRow={"two"} stackable>
          <DocsFromInvestorsCard dealId={dealId} />
        </Card.Group>
      </Container>
    </Transition>
  );
};
