import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { Table, Card, Grid, Modal, Icon } from "semantic-ui-react";
import PortfolioCompanyForm from "../../forms/deals/PortfolioCompanyForm";

interface Props {
  dealId: string;
}

const DealPortfolioCompanyCard = ({ dealId }: Props) => {
  const [showEdit, setShowEdit] = useState(false);
  const dispatch = useDispatch();

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  useEffect(() => {
    if (!dealsLoading && !dealsError && (!deal || deal.id !== dealId)) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dealsError, dealsLoading, dispatch]);

  return (
    <>
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Header>Edit Portfolio Company</Modal.Header>
        <Modal.Content>
          <PortfolioCompanyForm deal={deal} setShowEdit={setShowEdit} />
        </Modal.Content>
      </Modal>
      <Card>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column width={13}>Portfolio Company</Grid.Column>
              <Grid.Column textAlign="right">
                <Icon
                  className={"primaryColor"}
                  link
                  bordered
                  inverted
                  name="pencil"
                  size="small"
                  onClick={() => setShowEdit(true)}
                />
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Table definition basic compact size="small" unstackable>
            <Table.Body>
              <Table.Row>
                <Table.Cell>Name</Table.Cell>
                <Table.Cell>
                  {deal?.portfolioCompanyName ?? "Not set"}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Entity</Table.Cell>
                <Table.Cell>
                  {deal?.portfolioCompanyEntity ?? "Not set"}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>State</Table.Cell>
                <Table.Cell>
                  {deal?.portfolioCompanyState ?? "Not set"}
                </Table.Cell>
              </Table.Row>
              {deal?.portfolioCompanyContact && (
                <>
                  <Table.Row>
                    <Table.Cell>Contact: First Name</Table.Cell>
                    <Table.Cell>
                      {deal?.portfolioCompanyContact?.firstName ?? "Not set"}
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Contact: Last Name</Table.Cell>
                    <Table.Cell>
                      {deal?.portfolioCompanyContact?.lastName ?? "Not set"}
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Contact: Email</Table.Cell>
                    <Table.Cell>
                      {deal?.portfolioCompanyContact?.email ?? "Not set"}
                    </Table.Cell>
                  </Table.Row>
                </>
              )}
            </Table.Body>
          </Table>
        </Card.Content>
      </Card>
    </>
  );
};

export default DealPortfolioCompanyCard;
