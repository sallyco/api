import React, { useState } from "react";
import { getBaseUrl } from "../../tools/urls";
import { FeatureFlag } from "../featureflags/FeatureFlags";
import { Header, Segment, Input } from "semantic-ui-react";

export const DealLinkShareCard: React.FC<{ dealId: string }> = ({ dealId }) => {
  const [showCopied, setShowCopied] = useState(false);
  const [showCopyError, setShowCopyError] = useState(false);
  const dealUrl = getBaseUrl() + `/invest-now?deal=${String(dealId) ?? ""}`;

  return (
    <FeatureFlag name="direct_link_urls">
      <Segment>
        <Header textAlign="center">Share this deal</Header>
        <Input
          action={{
            icon: "copy",
            className: "secondary",
            onClick: async () => {
              setShowCopied(false);
              setShowCopyError(false);
              try {
                await navigator.clipboard.writeText(dealUrl);
                setShowCopied(true);
              } catch (err) {
                setShowCopyError(true);
              }
            },
          }}
          defaultValue={dealUrl}
          readOnly
          fluid
        />
        <div>
          {showCopied && (
            <span
              className="ui"
              style={{ color: "#71b73c", fontSize: "0.85em" }}
            >
              Link Copied!
            </span>
          )}
          {showCopyError && (
            <span className="ui" style={{ color: "red", fontSize: "0.85em" }}>
              Link failed to copy...
            </span>
          )}
        </div>
      </Segment>
    </FeatureFlag>
  );
};
