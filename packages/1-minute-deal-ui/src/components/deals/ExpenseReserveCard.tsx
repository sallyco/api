import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { Table, Card, Grid, Modal } from "semantic-ui-react";
import ExpenseReserveForm from "../../forms/deals/ExpenseReserveForm";
import { Box, Button, Typography } from "@mui/material";
import { PencilIcon } from "react-line-awesome";
import NumberFormat from "react-number-format";
import useStyles from "./informationBlocks/styles";
interface Props {
  dealId: string;
  editable?: boolean;
}

const ExpenseReserveCard = ({ dealId, editable = true }: Props) => {
  const dispatch = useDispatch();
  const [showEdit, setShowEdit] = useState(false);
  const classes = useStyles();

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });
  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);
  useEffect(() => {
    if (!dealsLoading && !dealsError && (!deal || deal.id !== dealId)) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dealsError, dealsLoading, dispatch]);

  return (
    <>
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Header>Expense Reserve</Modal.Header>
        <Modal.Content>
          <ExpenseReserveForm
            entity={entity}
            deal={deal}
            setShowEdit={setShowEdit}
          />
        </Modal.Content>
      </Modal>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Box display={"flex"} flexDirection={"row"}>
              <Box flexGrow={1} textAlign={"left"}>
                Expense Reserve
              </Box>
              <Box>
                {editable && (
                  <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<PencilIcon />}
                    onClick={() => setShowEdit(true)}
                  >
                    Edit
                  </Button>
                )}
              </Box>
            </Box>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Typography
            variant="h2"
            color={"secondary"}
            className={classes.informationalBlockTitle}
            data-testid="expense-reserve"
          >
            {entity?.expenseReserve?.amount === null ||
            entity?.expenseReserve?.amount === undefined ? (
              "Not set"
            ) : (
              <>
                {entity?.expenseReserve?.type === "Flat Fee" && (
                  <NumberFormat
                    value={entity?.expenseReserve?.amount}
                    displayType={"text"}
                    thousandSeparator={true}
                    decimalScale={2}
                    fixedDecimalScale={true}
                    prefix={"$"}
                    data-testid="expense-reserve-flat"
                  />
                )}
                {entity?.expenseReserve?.type === "Percent" && (
                  <NumberFormat
                    value={entity?.expenseReserve?.amount}
                    displayType={"text"}
                    suffix={"%"}
                    data-testid="expense-reserve-percent"
                  />
                )}
              </>
            )}
          </Typography>
          {/*<Table definition basic compact size="small" unstackable>*/}
          {/*  <Table.Body>*/}
          {/*    <Table.Row>*/}
          {/*      <Table.Cell>Amount</Table.Cell>*/}
          {/*      <Table.Cell>*/}
          {/*        {entity?.expenseReserve?.amount ?? "Not set"}*/}
          {/*      </Table.Cell>*/}
          {/*    </Table.Row>*/}
          {/*    <Table.Row>*/}
          {/*      <Table.Cell>Type</Table.Cell>*/}
          {/*      <Table.Cell>*/}
          {/*        {entity?.expenseReserve?.type ?? "Not set"}*/}
          {/*      </Table.Cell>*/}
          {/*    </Table.Row>*/}
          {/*  </Table.Body>*/}
          {/*</Table>*/}
        </Card.Content>
      </Card>
    </>
  );
};

export default ExpenseReserveCard;
