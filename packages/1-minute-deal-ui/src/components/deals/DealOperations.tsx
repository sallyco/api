import React, { useState } from "react";
import { Menu, Input, Icon, Button, Label, Dropdown } from "semantic-ui-react";
import { SearchInput } from "../../components/search/Search";
import { SortInput } from "../../components/sort/Sort";
import { FilterInput } from "../../components/filter/Filter";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";

export const DealOperation = () => {
  const { opType } = useSelector(
    (state: RootState) => state.dealsListOpeartions
  );

  return (
    <>
      <Menu borderless className="nav-bar filteration-menu">
        <Menu.Item>
          {opType === "search" ? (
            <SearchInput />
          ) : opType === "filter" ? (
            <FilterInput />
          ) : opType === "sort" ? (
            <SortInput />
          ) : (
            ""
          )}
        </Menu.Item>
      </Menu>
    </>
  );
};

export default DealOperation;
