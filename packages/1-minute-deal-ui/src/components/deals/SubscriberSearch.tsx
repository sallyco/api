import React, { useEffect, useState } from "react";
import { IconButton } from "@mui/material";
import { TableWithTools } from "@packages/gbt-ui";
import { CheckIcon } from "react-line-awesome";
import { Card, Grid } from "semantic-ui-react";

interface SubscriberSearchProps {
  subscriptions: any[];
  onSelection: (s) => void;
}

const ID_COLUMN_INDEX = 0;

export default function SubscriberSearch({
  subscriptions,
  onSelection,
}: SubscriberSearchProps) {
  const [filteredSubscriptions, setFilteredSubscriptions] = useState([]);

  useEffect(() => {
    if (subscriptions) {
      setFilteredSubscriptions(subscriptions);
    }
  }, [subscriptions]);

  const actionMenu = (rowData) => {
    return (
      <>
        <IconButton
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            onSelection(rowData[ID_COLUMN_INDEX]);
          }}
          style={{ color: "green" }}
        >
          <CheckIcon />
        </IconButton>
      </>
    );
  };

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column>Select Investor</Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <TableWithTools
            data={filteredSubscriptions}
            columnDefinitions={[
              {
                name: "id",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "profileId",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "name",
                label: "PROFILE NAME",
              },
              {
                name: "email",
                label: "EMAIL",
              },
              {
                name: "amount",
                label: "AMOUNT",
              },
              {
                name: "actions",
                label: " ",
                // eslint-disable-next-line react/display-name
                options: {
                  filter: false,
                  // eslint-disable-next-line react/display-name
                  customBodyRender: (
                    value: any,
                    tableMeta: any,
                    updateValue: any
                  ) => {
                    return actionMenu(tableMeta.rowData);
                  },
                },
              },
            ]}
            selectable={false}
            clickable
            enableNestedDataAccess={"."}
          />
        </Card.Content>
      </Card>
    </>
  );
}
