import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { TenantContext } from "../../contexts";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import EditDealForm from "../../forms/EditDealForm";
import {
  Grid,
  Icon,
  Modal,
  Card,
  Image,
  Segment,
  Header,
} from "semantic-ui-react";
import { useFeatureFlag } from "../featureflags/FeatureFlags";
import EntityBankingInformation from "../banks/EntityBankingInformation";
import { Profile } from "../../api/profilesApi";
import DealDetailsTable from "./DealDetailsTable";
import { Box, Button } from "@mui/material";
import { PencilIcon } from "react-line-awesome";
import { DealLinkShareCard } from "./DealLinkShareCard";
import { fetchClosesListByDeal } from "../../slices/closesSlice";

interface Props {
  dealId: string;
  investorProfile?: Profile;
}

const DealDetailsCard = ({ dealId, investorProfile = null }: Props) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const bankingFeatureEnabled = useFeatureFlag("banking");

  const [showEdit, setShowEdit] = useState(false);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const { isSubmitting: entityUpdating } = useSelector((state: RootState) => {
    return state.entities;
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, dealId, deal]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  const [closes, setCloses] = useState([]);

  const { closesById } = useSelector((state: RootState) => state.closes);
  useEffect(() => {
    if (deal) {
      setCloses(
        Object.values(closesById).filter((close) => close.dealId === deal.id)
      );
    }
  }, [closesById]);
  useEffect(() => {
    if (deal) {
      dispatch(fetchClosesListByDeal(deal.id));
    }
  }, [deal, dispatch]);

  return (
    <>
      <Card fluid>
        <Segment>
          <Grid>
            <Grid.Row columns={1}>
              <Grid.Column textAlign={"center"}>
                <Header as="h2" icon textAlign="center">
                  {deal?.name}
                  <Header.Subheader>
                    {deal?.marketing?.tagline}
                  </Header.Subheader>
                </Header>
              </Grid.Column>
            </Grid.Row>
            {deal?.marketing?.logo && (
              <Grid.Row>
                <Grid.Column width={16}>
                  <Image
                    centered
                    rounded
                    src={deal?.marketing?.logo}
                    style={{ width: 400, maxWidth: "100%" }}
                  />
                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>
        </Segment>
      </Card>
      <DealLinkShareCard dealId={dealId} />
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Header>Details</Modal.Header>
        <Modal.Content>
          <EditDealForm
            deal={deal}
            entity={entity}
            setShowEdit={setShowEdit}
            fieldSettings={{
              maxTargetRaiseAmount:
                tenantProfile?.dealLimits?.perDealMaxTargetRaiseAmount || null,
            }}
          />
        </Modal.Content>
      </Modal>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Box display={"flex"} flexDirection={"row"}>
              <Box flexGrow={1}>Deal Details</Box>
              <Box textAlign="right">
                <Button
                  variant="contained"
                  color="secondary"
                  startIcon={<PencilIcon />}
                  onClick={() => setShowEdit(true)}
                >
                  Edit
                </Button>
              </Box>
            </Box>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Card.Group itemsPerRow={1} stackable>
            <Card fluid>
              <DealDetailsTable
                deal={{ ...deal, ...{ closes } }}
                entity={entity}
              />
            </Card>
            {entity?.bankAccount && (
              <Card fluid>
                <EntityBankingInformation
                  entity={entity}
                  investorProfile={investorProfile}
                />
              </Card>
            )}
          </Card.Group>
        </Card.Content>
      </Card>
    </>
  );
};

export default DealDetailsCard;
