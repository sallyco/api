import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Card,
  Container,
  Transition,
  Table,
  Header,
  Message,
  Loader,
} from "semantic-ui-react";
import { fetchEntityBankAccountTransactions } from "../../slices/entitySlice";
import { RootState } from "../../rootReducer";
import TransactionTable from "../entities/bankAccounts/TransactionTable";

interface Props {
  dealId: string;
  entityId: string;
}

export const DealTransactionsTab = ({ dealId, entityId }: Props) => {
  const dispatch = useDispatch();

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const entity = useSelector(
    (state: RootState) => state.entities.entitiesById[entityId]
  );
  const { isSubmitting: entityUpdating } = useSelector((state: RootState) => {
    return state.entities;
  });

  useEffect(() => {
    if (entity?.bankAccount?.providerMeta?.accountId) {
      dispatch(fetchEntityBankAccountTransactions(entity));
    }
  }, [deal, entity?.id]);

  return (
    <Transition>
      {!entity ? (
        <Loader active inline="centered" size="massive" />
      ) : (
        <Container>
          {!entityUpdating && (
            <>
              {entity?.transactions && Array.isArray(entity.transactions) && (
                <TransactionTable transactions={entity.transactions} />
              )}
              {!entityUpdating && !entity?.transactions && (
                <>
                  <Message
                    icon={"money bill alternate outline"}
                    header={"Banking information is not yet available"}
                    content={"Account setup is in progress"}
                    color={"yellow"}
                  />
                </>
              )}
            </>
          )}
          {entityUpdating && <Loader active />}
        </Container>
      )}
    </Transition>
  );
};
