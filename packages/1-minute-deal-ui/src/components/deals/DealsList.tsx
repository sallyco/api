import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { Container, Card, Divider } from "semantic-ui-react";

import { RootState } from "../../rootReducer";

import { Deal } from "../../api/dealsApi";
import DealListActions from "./DealsListActions";
import NoDeals from "./NoDeals";
import DealCard from "./DealCard";
import DealTable from "./DealTable";
import DealOperations from "./DealOperations";
import NoCompanyDeal from "./NoCompanyDeal";
import { fetchSelf } from "../../slices/usersSlice";

interface Props {
  deals: Deal[];
}

export const DealsList = ({ deals }: Props) => {
  const dispatch = useDispatch();
  const [tempDeals, settempDeals] = useState(deals);

  useEffect(() => {
    settempDeals(deals);
  }, [deals]);

  const handleonBottomPassed = (eventname) => {
    const len = tempDeals.length;
    if (len <= deals.length) {
      const data = deals.slice(0, len + 2);
      settempDeals(data);
    }
  };

  const { displayType } = useSelector(
    (state: RootState) => state.dealsListDisplay
  );

  const { opType } = useSelector(
    (state: RootState) => state.dealsListOpeartions
  );

  const { self, isSubmitting } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  return (
    <>
      {deals.length > 0 || opType ? (
        <>
          <DealListActions />
          {opType && <DealOperations />}
          <Divider hidden />
          <Container>
            {displayType === "cards" ? (
              <Card.Group itemsPerRow="two" centered stackable>
                {tempDeals.map((deal) => (
                  <DealCard key={deal.id} dealId={deal.id} />
                ))}
              </Card.Group>
            ) : (
              <DealTable deals={deals} />
            )}
          </Container>
        </>
      ) : self?.type === "Founder" ? (
        <>
          <Divider hidden />
          <NoCompanyDeal />
        </>
      ) : (
        <NoDeals />
      )}
      <div style={{ height: 30 }} />
    </>
  );
};
