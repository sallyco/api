import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Label } from "semantic-ui-react";
import {
  DealStatusColors as DealStatusColor,
  DealStatusColors as dsc,
} from "../../tools/statusColors";
import NumberFormat from "react-number-format";
import moment from "moment";
import { FeatureFlag } from "../featureflags/FeatureFlags";
import { DealStatusText } from "../../tools/dealStatuses";
import { getProfileName } from "../../api/profileApiWrapper";
import { RootState } from "../../rootReducer";
import { fetchProfileById } from "../../slices/profilesSlice";

const QP_FEATURE_FLAG = "qualified_purchaser";

export default function DealDetailsTable({ deal, entity }) {
  const dispatch = useDispatch();

  const organizer = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });

  useEffect(() => {
    if (
      deal &&
      deal.profileId &&
      (!organizer || deal.profileId !== organizer.id)
    ) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [deal, dispatch, organizer]);

  return (
    <Table definition basic compact size="small" unstackable>
      <Table.Body>
        {organizer && (
          <>
            <Table.Row>
              <Table.Cell>Name of Organizer</Table.Cell>
              <Table.Cell>{getProfileName(organizer)}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Email of Organizer</Table.Cell>
              <Table.Cell>{organizer.email}</Table.Cell>
            </Table.Row>
          </>
        )}
        <FeatureFlag name={QP_FEATURE_FLAG}>
          <Table.Row>
            <Table.Cell>Qualified Purchasers Only</Table.Cell>
            <Table.Cell>
              {deal?.requireQualifiedPurchaser ? "Yes" : "No"}
            </Table.Cell>
          </Table.Row>
        </FeatureFlag>
        <Table.Row>
          <Table.Cell>Current Status</Table.Cell>
          <Table.Cell>
            <Label circular color={DealStatusColor(DealStatusText(deal))}>
              {DealStatusText(deal)}
            </Label>
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Closing</Table.Cell>
          <Table.Cell>
            {moment.utc(deal?.estimatedCloseDate).format("LL")}
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Created</Table.Cell>
          <Table.Cell>{moment(deal?.createdAt).format("LL")}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Target Raise</Table.Cell>
          <Table.Cell>
            <NumberFormat
              value={deal?.targetRaiseAmount ?? 0}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />
          </Table.Cell>
        </Table.Row>
        {deal?.previouslyRaisedAmount > 0 && (
          <Table.Row>
            <Table.Cell className="deal-card-header">
              Previously Raised Off Platform
            </Table.Cell>
            <Table.Cell>
              <NumberFormat
                value={deal?.previouslyRaisedAmount}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"$"}
              />
            </Table.Cell>
          </Table.Row>
        )}
        <Table.Row>
          <Table.Cell>Min Investment</Table.Cell>
          <Table.Cell>
            <NumberFormat
              value={entity?.minInvestmentAmount ?? 0}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Reg D Type</Table.Cell>
          <Table.Cell>Reg D 506({entity?.regDExemption ?? "b"})</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>EIN Number</Table.Cell>
          <Table.Cell>{entity?.ein ?? "Pending Assignment"}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Entity Name</Table.Cell>
          <Table.Cell>{entity?.name ?? ""}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  );
}
