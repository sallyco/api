import React, { useCallback, useState } from "react";
import { Subscription } from "../../api/subscriptionsApi";
import { Profile } from "../../api/profilesApi";
import {
  Paper,
  List,
  ListItem,
  ListItemText,
  Box,
  ListItemSecondaryAction,
  CircularProgress,
  Button,
} from "@mui/material";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import { Header } from "semantic-ui-react";
import { API, useRequest } from "../../api/swrApi";
import { Link } from "react-router-dom";
import { getUserEmailExist } from "../../api/usersApi";
import { sendInviteSigner } from "../../api/emailsApi";
import { toast } from "react-toastify";

const SignersDetails: React.FC<{
  profile: Profile;
  subscription: Subscription;
  showActions?: boolean;
}> = ({ profile, subscription, showActions = true }) => {
  const { data: self } = useRequest(API.SELF);
  const { data: deal } = useRequest(API.DEAL_BY_ID(subscription?.dealId));
  const { data: invites } = useRequest(API.INVITES);
  const [nudgeLoading, setNudgeLoading] = useState(false);
  const hasSigned = useCallback(() => {
    return !!subscription?.signers?.find(
      (signatory) => signatory.email === self.email
    )?.dateSigned;
  }, [subscription, self]);

  const nudgeSigner = async (email) => {
    setNudgeLoading(true);
    const exists = await getUserEmailExist(email);
    const isUser = !!exists.data.email;
    const invite = invites.data.find(
      (invite) =>
        invite.acceptableBy === email && invite.invitedRole === "signer"
    );
    await sendInviteSigner(
      email,
      self.email,
      deal,
      invite,
      isUser,
      subscription.id
    );
    setNudgeLoading(false);
    toast.success("Nudge Sent");
  };

  return (
    <>
      {subscription?.signers && profile && self && (
        <Paper>
          <Box mt={1} p={2}>
            <Box pb={1}>
              <Header
                as="h3"
                color="grey"
                className="no-spacing"
                style={{ textAlign: "left" }}
              >
                Signer Details
              </Header>
            </Box>
            {`${
              subscription.signers.filter((signer) => signer.dateSigned).length
            } of ${subscription.signers.length} required parties have signed`}
            <List>
              {subscription.signers.map((signatory) => (
                <ListItem key={signatory.email}>
                  <ListItemText>
                    <Box display={"flex"}>
                      {signatory.dateSigned ? (
                        <CheckCircleOutlineIcon />
                      ) : (
                        <RadioButtonUncheckedIcon />
                      )}{" "}
                      &nbsp; {signatory.name} -{" "}
                      {signatory.email === self.email ? "You" : signatory.email}
                    </Box>
                  </ListItemText>
                  {showActions && self.id === subscription.ownerId && (
                    <ListItemSecondaryAction>
                      {!signatory.dateSigned && (
                        <Button
                          variant={"outlined"}
                          onClick={async () =>
                            await nudgeSigner(signatory.email)
                          }
                        >
                          {nudgeLoading ? (
                            <CircularProgress size={20} />
                          ) : (
                            "Nudge"
                          )}
                        </Button>
                      )}
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              ))}
            </List>
            {showActions && !hasSigned() && (
              <>
                <Button
                  variant={"contained"}
                  fullWidth={true}
                  color={"primary"}
                  component={Link}
                  to={`/profiles/create/subscriber?subscriptionId=${subscription?.id}`}
                >
                  Sign Now
                </Button>
              </>
            )}
          </Box>
        </Paper>
      )}
    </>
  );
};

export default SignersDetails;
