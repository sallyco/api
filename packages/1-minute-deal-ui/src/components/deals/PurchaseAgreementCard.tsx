import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById, patchEntityById } from "../../slices/entitySlice";
import { Grid, Table, Button, Card, Icon, Popup } from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";
import WaitForDependencies from "../WaitForDependencies";
import {
  FileSizes,
  SubformFileUpload,
} from "../../forms/subforms/SubformFileUpload";
import { toast } from "react-toastify";

interface Props {
  dealId: string;
}

const PurchaseAgreementCard = ({ dealId }: Props) => {
  const dispatch = useDispatch();

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entity]);

  return (
    <Card>
      <WaitForDependencies items={[deal, entity]}>
        <Card.Content style={{ flexGrow: 0 }}>
          <Card.Header>
            <Grid columns="2">
              <Grid.Column>Purchase Agreement</Grid.Column>
              <Grid.Column textAlign={"right"}>
                <SubformFileUpload
                  maxSize={FileSizes.MB * 50}
                  allowMultiple={true}
                  onSuccess={async (fileIds) => {
                    await dispatch(
                      patchEntityById(deal.entityId, {
                        entityDocuments: {
                          ...entity.entityDocuments,
                          purchaseAgreement: [
                            ...(entity.entityDocuments.purchaseAgreement
                              ? [...entity.entityDocuments.purchaseAgreement]
                              : []),
                            ...fileIds,
                          ],
                        },
                      })
                    );
                    toast.success(
                      `File${fileIds.length > 1 ? "s" : ""} uploaded to deal`
                    );
                  }}
                  onError={(errors) => {
                    errors.forEach((error) => {
                      toast.error(error);
                    });
                  }}
                >
                  {(props) => {
                    return (
                      <Popup
                        trigger={
                          <Icon
                            className={"primaryColor"}
                            link
                            bordered
                            circular
                            loading={props.uploading}
                            name={props.uploading ? "spinner" : "upload"}
                            size="small"
                            onClick={props.triggerFileUpload}
                          />
                        }
                        basic
                        inverted
                        content={"Upload Files"}
                      />
                    );
                  }}
                </SubformFileUpload>
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        {!entity?.entityDocuments?.purchaseAgreement ||
        entity?.entityDocuments?.purchaseAgreement.length <= 0 ? (
          <Card.Content textAlign="center">
            <SubformFileUpload
              maxSize={FileSizes.MB * 50}
              allowMultiple={true}
              onSuccess={async (fileIds) => {
                await dispatch(
                  patchEntityById(deal.entityId, {
                    entityDocuments: {
                      ...entity.entityDocuments,
                      purchaseAgreement: [
                        ...(entity.entityDocuments.purchaseAgreement ?? []),
                        ...fileIds,
                      ],
                    },
                  })
                );
                toast.success(
                  `File${fileIds.length > 1 ? "s" : ""} uploaded to deal`
                );
              }}
              onError={(errors) => {
                errors.forEach((error) => {
                  toast.error(error);
                });
              }}
            >
              {(props) => {
                return (
                  <Popup
                    trigger={
                      <Button
                        secondary
                        content="Upload File"
                        onClick={props.triggerFileUpload}
                        loading={props.uploading}
                      />
                    }
                    basic
                    inverted
                    content={"Upload Files"}
                  />
                );
              }}
            </SubformFileUpload>
          </Card.Content>
        ) : (
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {entity?.entityDocuments?.purchaseAgreement &&
                  Array.isArray(entity?.entityDocuments?.purchaseAgreement) &&
                  entity?.entityDocuments?.purchaseAgreement.map((doc) => (
                    <DocumentRow
                      key={doc}
                      canDelete={true}
                      fileId={doc}
                      documentTitle={"Purchase Agreement"}
                      onDelete={async (deletedFileId) => {
                        await dispatch(
                          patchEntityById(deal.entityId, {
                            entityDocuments: {
                              ...entity.entityDocuments,
                              purchaseAgreement: [
                                ...entity.entityDocuments.purchaseAgreement.filter(
                                  (doc) => doc !== deletedFileId
                                ),
                              ],
                            },
                          })
                        );
                        toast.success(`File deleted from deal`);
                      }}
                    />
                  ))}
              </Table.Body>
            </Table>
          </Card.Content>
        )}
      </WaitForDependencies>
    </Card>
  );
};

export default PurchaseAgreementCard;
