import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchSubscriptionsListByDeal } from "../../slices/subscriptionsSlice";
import { fetchInvitesListByDeal } from "../../slices/invitesSlice";
import { toast } from "react-toastify";
import { Loader, Card } from "semantic-ui-react";
import InviteCardTable from "./InviteCardTable";
interface Props {
  dealId: string;
}

const InvitesCard = ({ dealId }: Props) => {
  const dispatch = useDispatch();

  //SUBSCRIPTIONS
  const {
    subscriptionsByDealId,
    isLoading: subsLoading,
    error: subscriptionsListError,
  } = useSelector((state: RootState) => state.subscriptions);

  useEffect(() => {
    if (subscriptionsListError) {
      toast.error("An Error has occurred. Please reload the page to continue.");
    } else if (
      dealId &&
      !(dealId in subscriptionsByDealId) &&
      !subscriptionsListError
    )
      dispatch(fetchSubscriptionsListByDeal(dealId));
  }, [dealId, subscriptionsByDealId, dispatch, subscriptionsListError]);

  //Invites
  const {
    isLoading: invitesLoading,
    currentPageDealId: currentPageDealInvites,
    error: invitesError,
    invitesById,
  } = useSelector((state: RootState) => state.invites);

  useEffect(() => {
    if (dealId && currentPageDealInvites !== dealId && !invitesError)
      dispatch(fetchInvitesListByDeal(dealId));
  }, [dealId, currentPageDealInvites, dispatch, invitesError]);

  return (
    <Card>
      {subsLoading || invitesLoading ? (
        <Loader />
      ) : (
        <InviteCardTable
          dealId={dealId}
          subscriptionsByDealId={subscriptionsByDealId}
          invitesById={invitesById}
          key={
            Object.values(subscriptionsByDealId).length +
            Object.values(invitesById).length
          }
        />
      )}
    </Card>
  );
};

export default InvitesCard;
