import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Table, Card } from "semantic-ui-react";

import DocumentRow from "../files/DocumentRow";
import WaitForDependencies from "../WaitForDependencies";
import { RootState } from "../../rootReducer";
import { fetchSubscriptionsListByDeal } from "../../slices/subscriptionsSlice";
import { fetchDealById } from "../../slices/dealsSlice";

interface DocsFromInvestorsCardProps {
  dealId: string;
}

const DocsFromInvestorsCard = ({ dealId }: DocsFromInvestorsCardProps) => {
  const dispatch = useDispatch();
  const [docsByInvestors, setDocsByInvestors] = useState<
    {
      id: string;
      profileId?: string;
      displayName?: string;
      email?: string;
      additionalFiles?: string[];
    }[]
  >([]);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const subscriptionsForDeal = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsByDealId[dealId]
  );

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
      dispatch(fetchSubscriptionsListByDeal(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (subscriptionsForDeal) {
      const filesData = Object.keys(subscriptionsForDeal).map(
        (subscriptionId) => {
          return {
            id: subscriptionsForDeal[subscriptionId].id,
            profileId: subscriptionsForDeal[subscriptionId].profileId,
            displayName: subscriptionsForDeal[subscriptionId].name,
            email: subscriptionsForDeal[subscriptionId].email,
            additionalFiles:
              subscriptionsForDeal[subscriptionId].additionalFiles,
          };
        }
      );
      setDocsByInvestors(filesData);
    }
  }, [subscriptionsForDeal, setDocsByInvestors]);

  return (
    <Card fluid>
      <WaitForDependencies items={[deal, subscriptionsForDeal]}>
        <Card.Content style={{ flexGrow: 0 }}>
          <Card.Header>
            <Grid columns="2">
              <Grid.Column>Documents Received from Investors</Grid.Column>
              <Grid.Column textAlign={"right"}></Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content textAlign="center">
          <Table basic="very" compact size="small" unstackable>
            <Table.Body>
              {docsByInvestors &&
                docsByInvestors.map((docsData) => {
                  return (
                    docsData?.additionalFiles &&
                    docsData.additionalFiles.map((doc) => {
                      return (
                        <DocumentRow
                          key={doc}
                          fileId={doc}
                          documentTitle={`${docsData.displayName}(${docsData.email})`}
                        />
                      );
                    })
                  );
                })}
            </Table.Body>
          </Table>
        </Card.Content>
        )
      </WaitForDependencies>
    </Card>
  );
};

export default DocsFromInvestorsCard;
