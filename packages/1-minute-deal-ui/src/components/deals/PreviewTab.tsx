import React from "react";
import PropTypes from "prop-types";
import { Button, Icon, Divider } from "semantic-ui-react";
import moment from "moment";
import NumberFormat from "react-number-format";

export default function PreviewTab({ history, deal }) {
  return (
    <div>
      <div className="preview-tab-header">
        <div className="preview-tab-header-title">
          {deal?.name ? deal?.name : "Deal Name"}
        </div>
        <div className="preview-tab-header-subtitle">
          Portfolio Company Name
        </div>

        <div className="preview-details-container">
          <div>
            <div className="preview-tab-header-subtitle">Target Fundraise</div>
            <NumberFormat
              className="preview-header-currency"
              value={deal?.targetRaise ? deal?.targetRaise : 800000}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            ></NumberFormat>
          </div>
          <div>
            <div className="preview-tab-header-subtitle">Close Date</div>
            <div className="preview-header-date">
              {moment.utc(deal?.estimatedCloseDate).format("LL")}
            </div>
          </div>
        </div>
      </div>

      <div className="preview-background-image"></div>

      <div className="preview-logo-image">
        <Icon
          className="preview-image-icon-placeholder"
          name="image"
          size="huge"
        ></Icon>
      </div>

      <div className="preview-title">
        {deal?.name ? deal?.name : "Deal Name"}
      </div>
      <div className="preview-summary-text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat...
        <span className="preview-links"> Read more</span>
      </div>

      <div className="preview-summary-decoration">
        Industry • City, State • Employees
      </div>

      <div className="preview-center">
        <Button
          style={{
            backgroundColor: "#71B73C",
            color: "#FFFFFF",
            width: "300px",
          }}
        >
          Invest Now
        </Button>
      </div>

      <Divider></Divider>

      <div className="preview-flex">
        <div className="preview-flex-items">
          <div className="preview-details-label">FOUNDER</div>
          <div className="preview-details-content">William Jones</div>
        </div>

        <div className="preview-flex-items">
          <div className="preview-details-label">FOUNDED</div>
          <div className="preview-details-content">October 2019</div>
        </div>
      </div>

      <div className="preview-flex">
        <div className="preview-flex-items">
          <div className="preview-details-label">MIN. INVESTMENT</div>
          <NumberFormat
            className="preview-details-content"
            value={10000}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"$"}
            suffix={".00"}
          ></NumberFormat>
          {/* <div className="preview-details-content">$10,000.00</div> */}
        </div>

        <div className="preview-flex-items">
          <div className="preview-details-label">INCORPORATION TYPE</div>
          <div className="preview-details-content">LLC</div>
        </div>
      </div>

      <div className="preview-flex">
        <div className="preview-flex-items">
          <div className="preview-details-label">DEAL ADDED</div>
          <div className="preview-details-content">FEB 7, 2020</div>
        </div>
      </div>

      <div className="preview-content-titles preview-content-blocks">
        Pitch Deck and Files
      </div>

      <div className="preview-pitch-deck-img">
        <div className="preview-download-button">
          <Button
            style={{
              width: "40px",
              height: "40px",
              backgroundColor: "#FFFFFF",
            }}
          >
            <Icon
              className="preview-download-icon"
              name="download"
              size="large"
              style={{ marginLeft: "-11px" }}
            ></Icon>
          </Button>
        </div>
      </div>
      <div className="preview-pitch-deck-container">
        <div className="preview-pitch-deck-file">Deal-Name.pptx</div>
        <div className="preview-pitch-deck-date">Feb 1, 2020</div>
      </div>

      <div className="preview-details-label preview-left-align">
        SUPPORTING FILES
      </div>
      <div className="preview-flex preview-left-align">
        <Icon className="preview-file" name="file" size="large"></Icon>
        <div className="preview-pitch-deck-download">deal-name-1pager.pdf</div>
      </div>

      <div className="preview-flex preview-left-align">
        <Icon className="preview-file" name="file" size="large"></Icon>
        <div className="preview-pitch-deck-download">
          deal-name-product-sheet.pdf
        </div>
      </div>

      <Divider></Divider>

      <div className="preview-content-titles preview-content-blocks">
        Executive Summary
      </div>

      <div className="preview-content-blocks">
        <div className="preview-details-executive-summary-subtitle">
          Management
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Problem
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Products / Services
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Target Market / Size
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Customers - Current / Potential
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Sales / Marketing Strategy
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Competitors
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Competitive Advantage
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>

        <div className="preview-details-executive-summary-subtitle">
          Annual Financials
        </div>
        <div className="preview-details-executive-summary-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </div>
      </div>

      <Divider></Divider>

      <div className="preview-content-blocks">
        <div className="preview-content-titles">Team</div>

        <div className="preview-exec-team-content">
          <div className="preview-team-img-placeholder">
            <Icon
              name="user"
              size="huge"
              style={{
                color: "#8E8F90",
                marginTop: "20px",
                marginLeft: "15px",
              }}
            ></Icon>
          </div>
          <div className="preview-details-team-name">William Jones </div>
          <div className="preview-details-team-title">
            Chief Executive Officer
          </div>
          <div className="preview-details-team-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <div className="preview-team-img-placeholder">
            <Icon
              name="user"
              size="huge"
              style={{
                color: "#8E8F90",
                marginTop: "20px",
                marginLeft: "15px",
              }}
            ></Icon>
          </div>
          <div className="preview-details-team-name">Lacy Christensen</div>
          <div className="preview-details-team-title">
            Chief Executive Officer
          </div>
          <div className="preview-details-team-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <div className="preview-team-img-placeholder">
            <Icon
              name="user"
              size="huge"
              style={{
                color: "#8E8F90",
                marginTop: "20px",
                marginLeft: "15px",
              }}
            ></Icon>
          </div>
          <div className="preview-details-team-name">Timothy Roberts</div>
          <div className="preview-details-team-title">
            Chief Executive Officer
          </div>
          <div className="preview-details-team-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>
        </div>
      </div>

      <Divider></Divider>

      <div className="preview-content-blocks">
        <div className="preview-content-titles">Company Contact</div>

        <div className="preview-details-label preview-contact-padding margin-top-10">
          FOUNDER
        </div>
        <div className="preview-details-team-name preview-contact-padding">
          Wayne Larson
        </div>
        <div className="preview-details-team-name preview-contact-padding">
          (344) 221-0000
        </div>
        <div className="preview-details-email preview-contact-padding">
          wlarson@lion.co
        </div>

        <div className="preview-details-label preview-contact-padding">
          ADVISORS
        </div>
        <div className="preview-details-team-name preview-contact-padding">
          Carrie Wright
        </div>
        <div className="preview-details-team-name preview-contact-padding">
          Suresh Patel
        </div>
        <div className="preview-details-team-name preview-contact-padding margin-bottom-20">
          Leo McReal
        </div>
      </div>
    </div>
  );
}

PreviewTab.propTypes = {
  history: PropTypes.object.isRequired,
  deal: PropTypes.object, //.isRequired
};
