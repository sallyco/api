import React from "react";
import { Icon, Button, Modal } from "semantic-ui-react";
import PropTypes from "prop-types";

const DeleteDocumentModal = ({ showModal, handleModal }) => {
  return (
    <Modal
      open={showModal}
      className={"delete-document-modal"}
      onMount={() => {
        const elem = document.getElementsByClassName(
          "ui page modals dimmer transition visible active"
        )[0];
        elem.addEventListener("click", () => handleModal());
      }}
      trigger={
        <Icon
          onClick={handleModal}
          name="delete"
          size="small"
          className={"documents-tab-document-icon-height"}
        ></Icon>
      }
    >
      <Modal.Header>
        Delete fund docs and revert deal to <q>pending</q> status?
      </Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Button onClick={handleModal} secondary>
            Delete &#38; Revert
          </Button>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
};

DeleteDocumentModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  handleModal: PropTypes.func.isRequired,
};
export default DeleteDocumentModal;
