import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "../../rootReducer";
import { useLoggedInUser } from "../../hooks/userHooks";
import {
  deleteSubscriptionById,
  updateSubscriptionById,
  resetSubscriptionById,
  fetchSubscriptionsListByDeal,
  patchSubscriptionById,
} from "../../slices/subscriptionsSlice";
import { AuditListBackend, InvestmentsTable } from "@packages/gbt-ui";
import {
  Card,
  Grid,
  Icon,
  Label,
  LabelDetail,
  Form,
  TextArea,
  Segment,
  Button,
} from "semantic-ui-react";
import { AuthContext, TenantContext } from "../../contexts";
import {
  RECEIVED_STATUSES,
  SubscriptionStatusColors,
  SubscriptionStatusText,
  canDeleteSubscription,
  canReinviteSubscription,
  canMarkCompleteSubscription,
  canMarkCommittedSubscription,
  getStatusOrder,
  isReceivedStatus,
  canRefundSubscription,
  canNudgeSubscription,
} from "../../tools/subscriptionStatuses";
import NumberFormat from "react-number-format";
import {
  sendNudgeInvestorEmail,
  sendInviteInvestorExisting,
} from "../../api/emailsApi";
import { toast } from "react-toastify";
import { getInvitesByDeal } from "../../api/invitesApi";
import { getDealById } from "../../api/dealsApi";
import { User, getUserByEmail } from "../../api/usersApi";
import { Subscription, createSubscription } from "../../api/subscriptionsApi";
import { fetchClosesList } from "../../slices/closesSlice";
import { PopupModal } from "../common/PopupModal";
import InvestmentRowActions from "./InvestmentRowActions";
import RefundSubscriptionForm from "../subscriptions/RefundSubscriptionForm";
import { insertNotification } from "../../slices/notificationsSlice";
import { isQPSubscriberProfile } from "./../../controllers/profiles";
import FormHint from "../../forms/common/FormHint";
import { useFeatureFlag } from "../featureflags/FeatureFlags";
import FeesStatement from "./FeesStatement";
import { CSVLink } from "react-csv";
import { Profile } from "../../api/profilesApi";
import { Box, Chip, CircularProgress, Typography } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { Theme } from "@mui/material/styles";
import { API, doChange, useRequest } from "../../api/swrApi";
import AccreditationBadge from "../accreditation/AccreditationBadge";
import SideLetterForm from "./SideLetterForm";
import SideLetterCard from "./SideLetterCard";
import _ from "lodash";

// These could be moved to a controller file
const MULTI_ACTIONS = {
  NUDGE: 1,
  COMPLETE: 2,
  DELETE: 3,
  COMMITTED: 3,
  RESET: 4,
  CREATE_SIDE_LETTER: 5,
  DELETE_SIDE_LETTER: 6,
};

interface Props {
  dealId: string;
}

/* ID_COLUMN_INDEX
 * mui-datatables index that is holding the subscription's 'id'
 * It is used for finding the correct subscription object
 */
const ID_COLUMN_INDEX = 1;
const PROFILE_COLUMN_INDEX = 6;
const SIDE_LETTER_COLUMN_INDEX = 15;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    redChip: {
      backgroundColor: "red",
      color: "white",
    },
    orangeChip: {
      backgroundColor: "orange",
      color: "white",
    },
    greenChip: {
      backgroundColor: "green",
      color: "white",
    },
    blueChip: {
      backgroundColor: "#1a6088",
      color: "white",
    },
    grayChip: {
      backgroundColor: "#808080",
      color: "white",
    },
    yellowChip: {
      backgroundColor: "#daa70c",
      color: "black",
    },
  })
);

const InvestmentsCard = ({ dealId }: Props) => {
  // Hooks used
  const showFeeStatementsFeature = useFeatureFlag("show_fee_statements");
  const userProfile = useContext(AuthContext);
  const [canAccept, setCanAccept] = useState(true);
  const csvLinkRef: any = useRef();

  // Redux related state
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const loggedInUser = useLoggedInUser();
  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const { data: entity } = useRequest(deal && API.ENTITY_BY_ID(deal?.entityId));
  const { closesById } = useSelector((state: RootState) => state.closes);
  const {
    subscriptionsByDealId,
    subscriptionsById,
    isLoading: subscriptionsLoading,
  } = useSelector((state: RootState) => state.subscriptions);

  // Component State Management
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const [showConfirmRefund, setShowConfirmRefund] = useState(false);
  const [showNoteEntry, setShowNoteEntry] = useState(false);
  const [showMultipleWarning, setShowMultipleWarning] = useState(false);
  const [showClosingPreview, setShowClosingPreview] = useState(false);
  const [showSideLetterPopup, setShowSideLetterPopup] = useState(false);
  const [
    showDeleteSideLetterWarning,
    setShowDeleteSideLetterWarning,
  ] = useState(false);
  const [showEditSideLetterPopup, setShowEditSideLetterPopup] = useState(false);
  const [noteSubscription, setNoteSubscription] = useState(null);
  const [noteText, setNoteText] = useState("");
  const [deleteSubscriptionId, setDeleteSubscriptionId] = useState(null);
  const [refundSubscriptionId, setRefundSubscriptionId] = useState(null);
  const [sideLetterSubscriptionIds, setSideLetterSubscriptionIds] = useState(
    []
  );
  const [editSideLetterSubscription, setEditSideLetterSubscription] = useState<{
    id: string;
    sideLetter: any;
  }>(null);
  const [popupName, setPopupName] = useState({ name: "" });
  const initialRowsSelected: number[] = [];
  const [rowsSelected, setRowsSelected] = useState(initialRowsSelected);
  const [selectedDataIndexes, setSelectedDataIndexes] = useState([]);
  const [multiActionMessage, setMultiActionMessage] = useState("");
  const [confirmActionType, setConfirmActionType] = useState(0);
  const [receivedFundsValue, setReceivedFundsValue] = useState(0);

  const styles = useStyles();
  const mapStatusToChipStyle = {
    CLEAR: styles.greenChip,
    EXCEPTION: styles.grayChip,
    CONSIDER: styles.yellowChip,
  };

  // Rendering and state change Effects
  useEffect(() => {
    dispatch(fetchClosesList());
  }, []);

  const { data: bankBalance, isValidating: bankBalanceLoading } = useRequest(
    deal?.entityId && API.ENTITY_BANK_ACCOUNT_BALANCE(deal.entityId)
  );

  // Ok click action when deleting an Investor from a deal
  const confirmSubscriptionRemoval = useCallback(async () => {
    await dispatch(deleteSubscriptionById(deleteSubscriptionId));
    await dispatch(fetchSubscriptionsListByDeal(dealId));
    await dispatch(
      insertNotification({
        userId: subscriptionsById[deleteSubscriptionId].ownerId,
        event: {
          type: "subscription",
          id: deleteSubscriptionId,
        },
        category: "INVESTMENTS",
        message: `Your investment in ${deal.name} has been cancelled`,
        acknowledged: false,
      })
    );

    setShowConfirmDelete(false);
    setDeleteSubscriptionId(null);
  }, [dispatch, deleteSubscriptionId, dealId]);

  const subscriptions = useCallback(() => {
    return Object.values(subscriptionsByDealId[dealId] ?? {}).map(
      (subscription) => {
        return {
          ...subscription,
          close: Object.values(closesById).find((el) =>
            el?.subscriptions?.includes(subscription.id)
          ),
        };
      }
    );
  }, [closesById, dealId, subscriptionsByDealId]);

  const auditSubscriptions = Object.values(subscriptionsByDealId[dealId] ?? {})
    .map((subscription) => {
      return {
        ...subscription,
        closes: Object.values(closesById).filter((el) =>
          el?.subscriptions?.includes(subscription?.id)
        ),
      };
    })
    .filter((subscription) => subscription.status === "COMPLETED");

  const { data: auditSubscriptionsIds } = useRequest(
    dealId &&
    API.ADD_FILTER(API.SUBSCRIPTIONS, {
      where: {
        dealId: dealId,
        status: "COMPLETED",
      },
    })
  );

  // Using this so we don't misuse table data by accident
  const tableSubscriptions = useCallback(() => {
    return subscriptions().filter(
      (subscription) => subscription.status !== "INVITED"
    );
  }, [subscriptions]);

  // Returns the data for the rows that are selected
  const getSubscriptionSelections = useCallback(() => {
    // Given a list of dataIndex, GET the real data objects
    return selectedDataIndexes.map((selectedIndexNumber) => {
      return tableSubscriptions().find(
        (val, index) => selectedIndexNumber === index
      );
    });
  }, [selectedDataIndexes, tableSubscriptions]);

  useEffect(() => {
    setRowsSelected(initialRowsSelected);
  }, []);

  useEffect(() => {
    if (dealId && !(dealId in subscriptionsByDealId))
      dispatch(fetchSubscriptionsListByDeal(dealId));
  }, [dealId, subscriptionsByDealId, dispatch]);

  const receivedFunds = useCallback(() => {
    return calculateTotalFromStatus(RECEIVED_STATUSES, subscriptions());
  }, [subscriptions]);

  useEffect(() => {
    setReceivedFundsValue(receivedFunds());
  }, [receivedFunds, subscriptions]);

  const [closeAuditData, setCloseAuditData] = useState();
  const getAudit = async () => {
    setCloseAuditData(undefined);
    setCloseAuditData(
      await doChange(
        API.CLOSE_AUDIT,
        {
          subscriptionIds: auditSubscriptionsIds.data.map((sub) => sub.id),
          dealId: dealId,
        },
        "POST"
      )
    );
  };

  const inTransitFunds = useCallback(() => {
    return calculateTotalFromStatus(
      ["FUNDING SENT", "FUNDS_IN_TRANSIT-MANUAL"],
      subscriptions()
    );
  }, [subscriptions]);

  const fundsNeedingSource = useCallback(() => {
    let x = calculateTotalFromStatus(
      ["COMMITTED", "NEEDS SIGNING"],
      subscriptions()
    );
    return x;
  }, [subscriptions]);

  /**
   *
   * @param nudgeList Subscription-ish[] (not quite a Subscription but close)
   */
  const nudgeSelectedRows = async () => {
    const nudgeList = getSubscriptionSelections();
    for (let nudgeItem of nudgeList) {
      sendNudgeEmail(dealId, userProfile?.email, nudgeItem, false);
    }

    toast.success(`An email was sent to selected Investors`);
  };

  const createSideLetterForSelectedRows = async () => {
    const selectedList = getSubscriptionSelections();
    setSideLetterSubscriptionIds(selectedList.map((s) => s.id));
    setShowSideLetterPopup(true);
  };

  const deleteSideLetterForSelectedRows = async () => {
    const selectedList = getSubscriptionSelections();
    await deleteSideLetter(selectedList.map((s) => s.id));
  };

  const deleteSideLetter = async (subscriptionIds) => {
    for (const id of subscriptionIds) {
      const subscriptionToUpdate = tableSubscriptions().find(
        (s) => s.id === id
      );
      if (subscriptionToUpdate.sideLetter) {
        delete subscriptionToUpdate.sideLetter;
        // eslint-disable-next-line no-await-in-loop
        await dispatch(patchSubscriptionById(id, { sideLetter: {} }));
      }
    }
    toast.success(
      `Side letter data updated for selected subscription${subscriptionIds.length > 1 ? "s" : ""
      }`
    );
  };

  const markComplete = (rowData) => {
    const { closes, ...sub } = rowData;
    dispatch(
      updateSubscriptionById({
        ...sub,
        status: "COMPLETED",
      })
    );
  };

  const reinviteSubscriber = async (rowData) => {
    let userData: User | boolean = false;
    try {
      userData = await getUserByEmail(rowData.email);
    } catch (e) {
      toast.error(`Reinvite failed: ${e}`);
      return;
    }

    let subscription: Subscription = await createSubscription({
      email: userData.username,
      name: rowData.name,
      ownerId: userData.id,
      dealId: dealId,
      status: "INVITED",
      acquisitionMethod: "INVITATION",
    });
    await sendInviteInvestorExisting(
      userData,
      loggedInUser?.email ?? "",
      deal,
      subscription
    );
    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        userId: userData.id,
        event: {
          type: "subscription",
          id: subscription.id,
        },
        category: "INVESTMENTS",
        message: `You have been re-invited to a deal ${deal.name}`,
        acknowledged: false,
      })
    );
    dispatch(fetchSubscriptionsListByDeal(dealId));
  };

  const markCommitted = (rowData) => {
    const { closes, ...sub } = rowData;
    dispatch(
      updateSubscriptionById({
        ...sub,
        status: "COMMITTED",
      })
    );
  };

  const completeReset = (rowData) => {
    dispatch(resetSubscriptionById(rowData.id));
  };

  const completeSelectedRows = async () => {
    const rows = getSubscriptionSelections();
    for (let row of rows) {
      if (canMarkCompleteSubscription(row)) {
        markComplete(row);
      }
    }
    toast.success("Selected Investments were marked as complete");
  };

  const markCommittedSelectedRows = async () => {
    const rows = getSubscriptionSelections();
    for (let row of rows) {
      if (canMarkCommittedSubscription(row)) {
        markCommitted(row);
      }
    }
    toast.success("Selected Investments were marked as complete");
  };

  const removeInvestors = async () => {
    const rows = getSubscriptionSelections();
    for (let row of rows) {
      if (row?.id && canDeleteSubscription(row)) {
        // eslint-disable-next-line no-await-in-loop
        await dispatch(deleteSubscriptionById(row.id));
        // eslint-disable-next-line no-await-in-loop
        await dispatch(
          insertNotification({
            userId: row.ownerId,
            event: {
              type: "subscription",
              id: row.id,
            },
            category: "INVESTMENTS",
            message: `Your investment in ${deal.name} has been cancelled`,
            acknowledged: false,
          })
        );
      }
    }

    // Reload the page data by fetching the new list of subscriptions
    await dispatch(fetchSubscriptionsListByDeal(dealId));

    toast.success("Selected Investors were removed from the Deal");
  };

  const confirmMultiAction = async (type) => {
    switch (type) {
      case MULTI_ACTIONS.NUDGE:
        nudgeSelectedRows();
        break;
      case MULTI_ACTIONS.COMPLETE:
        completeSelectedRows();
        break;
      case MULTI_ACTIONS.COMMITTED:
        markCommittedSelectedRows();
        break;
      case MULTI_ACTIONS.DELETE:
        await removeInvestors();
        break;
      case MULTI_ACTIONS.CREATE_SIDE_LETTER:
        createSideLetterForSelectedRows();
        break;
      case MULTI_ACTIONS.DELETE_SIDE_LETTER:
        deleteSideLetterForSelectedRows();
        break;
      default:
      // console.error("bad multi-action type");
    }
  };

  const today = new Date();
  const baseFilename = `${"Investor"}_${today.getMonth() + 1
    }-${today.getDate()}-${today.getFullYear()}`;

  const headers = [
    { label: "Id", key: "id" },
    { label: "Name", key: "name" },
    { label: "Entity", key: "profile.name" },
    { label: "TenantId", key: "tenantId" },
    { label: "Email", key: "email" },
    { label: "Phone", key: "profile.phone" },
    { label: "Amount", key: "amount" },
    { label: "Identity Check", key: "profile?.kycAml" },
    { label: "KYC Check", key: "profile?.kycAml" },
    { label: "Status", key: "status" },
    { label: "Note", key: "additionalProperties?.organizersNote" },
    { label: "Broker Name", key: "profile?.brokerageAccount?.brokerName" },
    { label: "Broker DTC Number", key: "profile?.brokerageAccount?.dtcNumber" },
    {
      label: "Brokerage Account Number",
      key: "profile?.brokerageAccount?.accountNumber",
    },
  ];

  const columnIndexMapping = new Map();
  columnIndexMapping.set("profile", PROFILE_COLUMN_INDEX);
  columnIndexMapping.set("id", ID_COLUMN_INDEX);
  columnIndexMapping.set("sideLetter", SIDE_LETTER_COLUMN_INDEX);

  const formatKycStatus = (
    profile: Profile,
    report_name = "identity_enhanced"
  ) => {
    const report = profile?.kycAml?.providerMeta?.reports?.find(
      (report) => report.name === report_name
    );

    if (profile?.kycAml?.status === "EXCEPTION") {
      return (
        <>
          <FormHint
            content={
              <>
                <Box p={1}>Unable to run this check</Box>
              </>
            }
          >
            <Chip
              label="EXCEPTION"
              className={styles.orangeChip}
              size="small"
            />
          </FormHint>
        </>
      );
    }
    if (profile?.kycAml?.status === "IN_PROGRESS") {
      return (
        <>
          <FormHint
            content={
              <>
                <Box p={1}>This check is in progress</Box>
              </>
            }
          >
            <Chip
              label="IN PROGRESS"
              className={styles.blueChip}
              size="small"
            />
          </FormHint>
        </>
      );
    }

    if (!report) {
      return (
        <>
          <Chip
            label="NOT RUN"
            color="default"
            size="small"
            className={styles.orangeChip}
          />
        </>
      );
    }
    if (profile?.kycAml?.result === "CLEAR") {
      return (
        <Chip
          label={"CLEAR"}
          color="default"
          size="small"
          className={mapStatusToChipStyle["CLEAR"]}
        />
      );
    }

    return (
      <Chip
        label={report?.result.toUpperCase()}
        color="default"
        size="small"
        className={mapStatusToChipStyle[report?.result.toUpperCase()]}
      />
    );
  };

  const actionMenu = (rowData) => {
    return (
      <InvestmentRowActions
        canComplete={canMarkCompleteSubscription(rowData)}
        canMoveToCommitted={canMarkCommittedSubscription(rowData)}
        canDelete={canDeleteSubscription(rowData)}
        canRefund={canRefundSubscription(rowData)}
        canReinvite={canReinviteSubscription(rowData)}
        canNudge={canNudgeSubscription(rowData)}
        multiple={rowsSelected.length > 1}
        noteHandler={async () => {
          const { close, ...sub } = rowData;
          setNoteSubscription(sub);
          setNoteText(sub?.additionalProperties?.organizersNote ?? "");
          setShowNoteEntry(true);
        }}
        nudgeHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `"Nudge" ${rowsSelected.length} Investors, continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.NUDGE);
            setShowMultipleWarning(true);
          } else {
            sendNudgeEmail(dealId, userProfile?.email, rowData);
          }
        }}
        refundHandler={() => {
          if (rowsSelected.length > 1) {
            toast.error("Refunds must be entered one at a time");
            return;
          }
          setRefundSubscriptionId(rowData.id);
          setShowConfirmRefund(true);
        }}
        reinviteHandler={() => {
          if (rowsSelected.length > 1) {
            toast.error("Invitations must be done one at a time");
            return;
          }
          reinviteSubscriber(rowData);
        }}
        completeHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `"Mark ${rowsSelected.length} Investments as Completed", continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.COMPLETE);
            setShowMultipleWarning(true);
          } else {
            markComplete(rowData);
          }
        }}
        committedHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `"Move ${rowsSelected.length} Investments to committed", continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.COMMITTED);
            setShowMultipleWarning(true);
          } else {
            markCommitted(rowData);
          }
        }}
        resetHandler={() => {
          if (rowsSelected.length > 1) {
            toast.error("Complete Reset must be done one at a time");
            return;
          }
          completeReset(rowData);
        }}
        deleteHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `"Remove ${rowsSelected.length} Investors from this Deal", continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.DELETE);
            setShowMultipleWarning(true);
          } else {
            setShowConfirmDelete(true);
            setPopupName({
              name: rowData["name"] || "",
            });
            setDeleteSubscriptionId(rowData.id);
          }
        }}
        createSideLetterHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `"Create Side Letters for ${rowsSelected.length} Investors from this Deal", continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.CREATE_SIDE_LETTER);
            setShowMultipleWarning(true);
          } else {
            setShowSideLetterPopup(true);
            setSideLetterSubscriptionIds([rowData.id]);
          }
        }}
        deleteSideLetterHandler={() => {
          if (rowsSelected.length > 1) {
            setMultiActionMessage(
              `delete side letter. This will cause a material change to the subscription of ${rowsSelected.length} investors. Are you sure you want to continue?`
            );
            setConfirmActionType(MULTI_ACTIONS.DELETE_SIDE_LETTER);
            setShowMultipleWarning(true);
          } else {
            setShowDeleteSideLetterWarning(true);
            setSideLetterSubscriptionIds([rowData.id]);
          }
        }}
      />
    );
  };

  const hiddenColumns = ["tenantId"];

  const onSideLetterClick = (
    colData: any,
    cellMeta: { colIndex: number; rowIndex: number; dataIndex: number }
  ) => {
    const clickedSubscription = tableSubscriptions()[cellMeta.dataIndex];
    if (
      clickedSubscription.sideLetter &&
      _.isEqual(clickedSubscription.sideLetter, {}) === false
    ) {
      setEditSideLetterSubscription({
        id: clickedSubscription.id,
        sideLetter: clickedSubscription.sideLetter,
      });
      setShowEditSideLetterPopup(true);
    }
  };

  return (
    <>
      <Card>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column>Investments</Grid.Column>
              <Grid.Column textAlign="right">
                <CSVLink
                  data={tableSubscriptions()}
                  headers={headers}
                  filename={`${baseFilename}.csv`}
                  target={"_blank"}
                  className={"no-padding"}
                  ref={csvLinkRef}
                />
                {showFeeStatementsFeature && (
                  <Button
                    onClick={async () => {
                      setShowClosingPreview(true);
                      await getAudit();
                    }}
                    secondary
                    style={{ marginBottom: "4px" }}
                  >
                    Preview Closing Statement
                  </Button>
                )}
                <Button
                  as={Link}
                  to={`/closes/${dealId}/close-deal`}
                  secondary
                  {...((!subscriptions() ||
                    subscriptions().filter(
                      (subscription) =>
                        [
                          "COMPLETED",
                          "REFUNDED",
                          "CANCELLED",
                          "COMPLETED_FUNDS_RECEIVED",
                        ].includes(subscription.status) && !subscription?.close
                    ).length === 0 ||
                    deal.status === "CLOSED") && { disabled: true })}
                >
                  Close Investors Into Deal
                </Button>
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          {!subscriptions() || tableSubscriptions().length === 0 ? (
            <Segment inverted className={"primaryBackground"}>
              There are no investments in the deal. You can{" "}
              <Link
                className={"bold underline"}
                to={`/deals/${dealId}/invite-investors`}
              >
                invite investors
              </Link>{" "}
              to this deal.
            </Segment>
          ) : (
            <React.Fragment>
              <InvestmentsTable
                data={tableSubscriptions()}
                selectable={true}
                rowsSelected={rowsSelected}
                onRowSelectionChange={(
                  currentRowsSelected: any[],
                  allRowsSelected: any[],
                  rowsSelected: any[]
                ) => {
                  // This holds the indexes of the displayed data index
                  // It will differ from the master data ordering
                  setRowsSelected(rowsSelected);

                  // Set the dataIndexes that have been selected
                  // These can be used for multi-select actions
                  // Because they reference the index of the data
                  const dataIndexes = allRowsSelected.map((selection) => {
                    return selection.dataIndex;
                  });
                  setSelectedDataIndexes(dataIndexes);
                }}
                columnIndexMapping={columnIndexMapping}
                checkQualifiedPurchaser={isQPSubscriberProfile}
                checkAccreditationStatus={(profile) => {
                  return (
                    <AccreditationBadge
                      profileId={profile.id}
                      showOnlyChip={true}
                    />
                  );
                }}
                receivedFundsValue={receivedFundsValue}
                statusFunctions={{
                  statusText: SubscriptionStatusText,
                  statusColor: SubscriptionStatusColors,
                  statusOrder: getStatusOrder,
                }}
                actionMenu={actionMenu}
                formatKycStatus={(data) =>
                  formatKycStatus(data, "watchlist_enhanced")
                }
                formatIdentityStatus={(data) =>
                  formatKycStatus(data, "identity_enhanced")
                }
                isReceivedStatus={isReceivedStatus}
                hiddenColumns={hiddenColumns}
                customToolbarSelect={function (selectedRows) {
                  return <></>;
                }}
                {...{
                  overrideDefaultDownload: (
                    buildHead,
                    buildBody,
                    columns,
                    data
                  ) => {
                    csvLinkRef.current.link.click();
                    return false;
                  },
                }}
                onSideLetterClick={onSideLetterClick}
              />
              <Segment basic>
                <Grid columns={3} stackable textAlign="center">
                  <Grid.Row>
                    <Grid.Column>
                      <Label color={"orange"}>
                        <Icon name={"money bill alternate outline"} /> Funds
                        Needing Source
                        <LabelDetail>
                          <NumberFormat
                            value={fundsNeedingSource()}
                            displayType={"text"}
                            thousandSeparator={true}
                            decimalSeparator={"."}
                            fixedDecimalScale={true}
                            decimalScale={2}
                            prefix={"$"}
                          />
                        </LabelDetail>
                      </Label>
                    </Grid.Column>
                    <Grid.Column>
                      <Label color={"olive"}>
                        <Icon name={"money bill alternate outline"} /> Funds In
                        Transit
                        <LabelDetail>
                          <NumberFormat
                            value={inTransitFunds()}
                            displayType={"text"}
                            thousandSeparator={true}
                            decimalSeparator={"."}
                            fixedDecimalScale={true}
                            decimalScale={2}
                            prefix={"$"}
                          />
                        </LabelDetail>
                      </Label>
                    </Grid.Column>
                    <Grid.Column>
                      <Label color={"grey"}>
                        <Icon name={"money bill alternate outline"} /> Funds
                        Received
                        <LabelDetail>
                          <NumberFormat
                            value={receivedFunds()}
                            displayType={"text"}
                            thousandSeparator={true}
                            decimalSeparator={"."}
                            fixedDecimalScale={true}
                            decimalScale={2}
                            prefix={"$"}
                          />
                        </LabelDetail>
                      </Label>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Segment>
            </React.Fragment>
          )}
        </Card.Content>
      </Card>
      {refundSubscriptionId && (
        <RefundSubscriptionForm
          open={showConfirmRefund}
          subscriptionId={refundSubscriptionId}
          processing={subscriptionsLoading}
          handleSuccess={() => setShowConfirmRefund(false)}
          handleClose={() => setShowConfirmRefund(false)}
        />
      )}
      <PopupModal
        open={showConfirmDelete}
        size="tiny"
        heading="Remove Investor Confirmation"
        content={
          <p>
            Are you sure you want to remove &quot;{popupName.name}&quot; from
            the deal &quot;
            {deal?.name || ""}&quot;?
          </p>
        }
        okcaption="Yes, Cancel the Subscription"
        closecaption="Cancel"
        onClickOk={confirmSubscriptionRemoval}
        onClickClose={() => setShowConfirmDelete(false)}
      />
      <PopupModal
        open={showNoteEntry}
        size="tiny"
        heading="Add a note"
        content={
          <Form>
            <TextArea
              placeholder="Add a short text note to this investor"
              value={noteText}
              onChange={(e, { value }) => setNoteText(String(value))}
            />
          </Form>
        }
        okcaption="Update Note"
        closecaption="Cancel"
        onClickOk={() => {
          dispatch(
            updateSubscriptionById({
              ...noteSubscription,
              additionalProperties: {
                ...noteSubscription.additionalProperties,
                organizersNote: noteText,
              },
            })
          );
          setNoteSubscription(null);
          setNoteText("");
          setShowNoteEntry(false);
        }}
        onClickClose={() => setShowNoteEntry(false)}
      />
      <PopupModal
        open={showMultipleWarning}
        size="tiny"
        heading="Multiple Investors Selected..."
        content={<p>You are about to {multiActionMessage}</p>}
        okcaption="Yes, Proceed"
        closecaption="Cancel"
        onClickOk={() => {
          confirmMultiAction(confirmActionType);
          setShowMultipleWarning(false);
        }}
        onClickClose={() => setShowMultipleWarning(false)}
      />
      <PopupModal
        open={showClosingPreview}
        size="fullscreen"
        heading="Closing Statement Preview"
        content={
          <>
            {closeAuditData ? (
              <>
                <Typography variant={"h6"}>Deal Audit Results</Typography>
                <AuditListBackend
                  auditData={closeAuditData}
                  setCanApprove={setCanAccept}
                />
              </>
            ) : (
              <>
                <Typography variant={"h6"}>Auditing Your Deal...</Typography>
                <CircularProgress />
              </>
            )}
            <FeesStatement dealId={dealId} />
          </>
        }
        okcaption="Done"
        onClickOk={() => {
          setShowClosingPreview(false);
        }}
        onClickClose={() => setShowClosingPreview(false)}
      />
      <PopupModal
        open={showSideLetterPopup}
        size="fullscreen"
        heading="Create Side Letter"
        content={
          <>
            <SideLetterCard
              deal={deal}
              subscriptions={tableSubscriptions()}
              selectedSubscriptionIds={sideLetterSubscriptionIds}
              onSuccess={() => {
                setShowSideLetterPopup(false);
              }}
            />
          </>
        }
        closecaption="Cancel"
        onClickClose={() => setShowSideLetterPopup(false)}
      />
      {editSideLetterSubscription && editSideLetterSubscription.sideLetter && (
        <PopupModal
          open={showEditSideLetterPopup}
          size="fullscreen"
          heading="Edit Side Letter"
          content={
            <>
              <SideLetterForm
                deal={deal}
                onSubmit={async (updatedSideLetter) => {
                  setShowEditSideLetterPopup(false);
                  await dispatch(
                    patchSubscriptionById(editSideLetterSubscription.id, {
                      sideLetter: updatedSideLetter,
                    })
                  );
                  toast.success(`Side letter data updated for subscription`);
                }}
                sideLetter={editSideLetterSubscription.sideLetter}
              />
            </>
          }
          closecaption="Cancel"
          onClickClose={() => setShowEditSideLetterPopup(false)}
        />
      )}
      <PopupModal
        open={showDeleteSideLetterWarning}
        size="tiny"
        heading="Delete side letter"
        content={
          <p>
            You are about to delete a side letter. This will cause a material
            change to the subscription. Are you sure you want to continue?
          </p>
        }
        okcaption="Yes, Proceed"
        closecaption="Cancel"
        onClickOk={async () => {
          setShowDeleteSideLetterWarning(false);
          await deleteSideLetter(sideLetterSubscriptionIds);
        }}
        onClickClose={() => setShowDeleteSideLetterWarning(false)}
      />
    </>
  );
};

// Functions that potentially could be moved to other files
function calculateTotalFromStatus(
  statuses: string[],
  subscriptionList: Subscription[]
) {
  const amounts = subscriptionList
    .filter(
      (subscription) =>
        statuses?.includes(SubscriptionStatusText(subscription)) ||
        statuses?.includes(subscription.status)
    )
    .map((subscription) => subscription.amount || 0);
  return amounts.length ? amounts.reduce((total, amount) => total + amount) : 0;
}

const sendNudgeEmail = async (
  dealId: string,
  email: string,
  subscription,
  showToast = true
) => {
  const allInvites = await getInvitesByDeal(dealId);
  const user = await getUserByEmail(subscription.email);
  const deal = await getDealById(dealId);
  const invite = allInvites.data.find(
    (obj) => obj.acceptableBy === subscription.email
  );

  await sendNudgeInvestorEmail(
    user,
    email ?? "",
    deal,
    subscription,
    SubscriptionStatusText(subscription),
    invite
  );

  if (showToast) {
    toast.success(`Reminder Request sent to ${subscription.email}`);
  }
};

export default InvestmentsCard;
