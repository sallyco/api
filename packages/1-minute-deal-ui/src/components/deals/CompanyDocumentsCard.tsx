import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { fetchAssetById, updateAssetById } from "../../slices/assetsSlice";
import { fetchFileById } from "../../slices/filesSlice";

import { Grid, Table, Card } from "semantic-ui-react";
import DocumentRow from "../files/DocumentRow";

interface Props {
  dealId: string;
  hasDelete?: boolean;
}

const CompanyDocumentsCard = ({ dealId, hasDelete = false }: Props) => {
  const dispatch = useDispatch();
  const [filesFecthed, setFilesFetched] = useState([]);
  const [fileDeleting, setFileDeleting] = useState(false);

  const { error: entitiesError } = useSelector(
    (state: RootState) => state.entities
  );

  const { error: assetsError, isEmpty: assetsEmpty } = useSelector(
    (state: RootState) => state.assets
  );

  const assets = useSelector((state: RootState) =>
    Object.values(state.assets.assetsById).filter(
      (asset) => asset.dealId === dealId
    )
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const files = useSelector((state: RootState) => {
    return assets.reduce(
      (reducedFiles, asset) =>
        reducedFiles.concat(
          asset?.files?.length
            ? Object.values(state.files.filesById).filter(
                (file) => asset.files.includes(file.id) && file.isPublic
              )
            : []
        ),
      []
    );
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (deal && !entitiesError && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entitiesError, entity]);

  useEffect(() => {
    if (deal?.assetIds && !assets && !assetsError && !assetsEmpty) {
      deal.assetIds.forEach((assetId) => {
        if (assets.findIndex((asset) => asset.id === assetId) !== -1) {
          dispatch(fetchAssetById(assetId));
        }
      });
    }
  }, [assets, assetsError, assetsEmpty, deal, dispatch]);

  useEffect(() => {
    assets.forEach((asset) => {
      if (asset?.files && !fileDeleting) {
        asset.files.forEach((fileId) => {
          if (!filesFecthed.includes(fileId)) {
            dispatch(fetchFileById(fileId));
            setFilesFetched([...filesFecthed, fileId]);
          }
        });
      }
    });
  }, [assets, dispatch, filesFecthed, fileDeleting]);

  const deleteFileFromAsset = async (deletedFileId) => {
    try {
      setFileDeleting(true);
      for (let i = 0; i < assets.length; i++) {
        let asset = assets[i];
        if (
          asset?.files &&
          asset?.files.filter((fileId) => fileId === deletedFileId).length > 0
        ) {
          const fileList = [...asset.files].filter(
            (val) => val !== deletedFileId
          );
          dispatch(
            updateAssetById({
              ...asset,
              files: fileList,
            })
          );
          break;
        }
      }
      setFileDeleting(false);
    } catch (err) {
      setFileDeleting(false);
    }
  };

  return (
    <>
      {files?.length ? (
        <Card fluid>
          <Card.Content>
            <Card.Header>
              <Grid columns="equal">
                <Grid.Column>Company Documents</Grid.Column>
              </Grid>
            </Card.Header>
          </Card.Content>
          <Card.Content textAlign="center">
            <Table basic="very" compact size="small" unstackable>
              <Table.Body>
                {files.map((file) => (
                  <DocumentRow
                    key={file.id}
                    fileId={file.id}
                    canDelete={hasDelete}
                    onDelete={async (deletedFileId) => {
                      await deleteFileFromAsset(deletedFileId);
                    }}
                  />
                ))}
              </Table.Body>
            </Table>
          </Card.Content>
        </Card>
      ) : (
        <></>
      )}
    </>
  );
};

export default CompanyDocumentsCard;
