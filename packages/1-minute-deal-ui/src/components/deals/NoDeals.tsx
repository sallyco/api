import React from "react";
import { Link } from "react-router-dom";
import dealsHome from "../../assets/images/deal_home_graphic.png";
import {
  Header,
  Button,
  Icon,
  Image,
  Container,
  Transition,
} from "semantic-ui-react";

export default function NoDeals() {
  return (
    <Transition animation={"fade"}>
      <Container textAlign="center">
        <Image src={dealsHome.src} size="medium" centered />
        <Header as="h2">
          <Header.Subheader>
            Welcome to the leading deal platform for organizers and investors.
          </Header.Subheader>
        </Header>
        <Button primary as={Link} to="/profiles/create">
          <Icon name="add" />
          Create A Deal
        </Button>
      </Container>
    </Transition>
  );
}
