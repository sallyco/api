import React, { useEffect, useState } from "react";
import { IconButton } from "@mui/material";
import { TimesIcon } from "react-line-awesome";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card, Grid } from "semantic-ui-react";
import { TableWithTools } from "@packages/gbt-ui";

import SubscriberSearch from "./SubscriberSearch";
import { Deal } from "../../api/dealsApi";
import { PopupModal } from "../../components/common/PopupModal";
import { patchSubscriptionById } from "../../slices/subscriptionsSlice";
import SideLetterForm from "./SideLetterForm";

interface SideLetterCardProps {
  deal: Deal;
  subscriptions: any[];
  selectedSubscriptionIds: string[];
  onSuccess: () => void;
}

const ID_COLUMN_INDEX = 0;

export default function SideLetterCard({
  deal,
  subscriptions,
  selectedSubscriptionIds,
  onSuccess,
}: SideLetterCardProps) {
  const dispatch = useDispatch();
  const [filteredSubscriptions, setFilteredSubscriptions] = useState([]);
  const [removedSubscriptionIds, setRemovedSubscriptionIds] = useState([]);
  const [selectedIds, setSelectedIds] = useState([...selectedSubscriptionIds]);
  const [showAddSubscriber, setShowAddSubscriber] = useState(false);

  useEffect(() => {
    if (subscriptions) {
      setFilteredSubscriptions(
        subscriptions.filter(
          (s) =>
            !removedSubscriptionIds.includes(s.id) && selectedIds.includes(s.id)
        )
      );
    }
  }, [subscriptions, removedSubscriptionIds, selectedIds]);

  const onSubmit = async (sideLetter) => {
    try {
      for (const subscription of filteredSubscriptions) {
        // eslint-disable-next-line no-await-in-loop
        await dispatch(patchSubscriptionById(subscription.id, { sideLetter }));
      }

      toast.success(
        `Side letter data saved for ${
          filteredSubscriptions.length
        } subscription${filteredSubscriptions.length > 1 ? "s" : ""}`
      );
      onSuccess();
    } catch (e) {
      console.info("error", e);
    }
  };

  const actionMenu = (rowData) => {
    return (
      <>
        {filteredSubscriptions.length > 1 && (
          <IconButton
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              const removedList = [
                ...(removedSubscriptionIds ?? []),
                rowData[ID_COLUMN_INDEX],
              ];
              setRemovedSubscriptionIds(removedList);
            }}
            style={{ color: "red" }}
          >
            <TimesIcon />
          </IconButton>
        )}
      </>
    );
  };

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column>Selected Investors</Grid.Column>
              <Grid.Column textAlign="right">
                <Button
                  onClick={() => setShowAddSubscriber(true)}
                  secondary
                  style={{ marginBottom: "4px" }}
                >
                  Add Investors
                </Button>
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <TableWithTools
            data={filteredSubscriptions}
            columnDefinitions={[
              {
                name: "id",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "profileId",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "ownerId",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "name",
                label: "PROFILE NAME",
              },
              {
                name: "email",
                label: "EMAIL",
              },
              {
                name: "profile.taxDetails.registrationType",
                label: "TYPE",
                options: {
                  // eslint-disable-next-line react/display-name
                  customBodyRender: (
                    value: any,
                    tableMeta: any,
                    updateValue: any
                  ) => (value ? value.toUpperCase() : "---"),
                },
              },
              {
                name: "amount",
                label: "AMOUNT",
              },
              {
                name: "actions",
                label: " ",
                // eslint-disable-next-line react/display-name
                options: {
                  filter: false,
                  // eslint-disable-next-line react/display-name
                  customBodyRender: (
                    value: any,
                    tableMeta: any,
                    updateValue: any
                  ) => {
                    return actionMenu(tableMeta.rowData);
                  },
                },
              },
            ]}
            selectable={false}
            clickable
            enableNestedDataAccess={"."}
          />
        </Card.Content>
      </Card>
      <Card fluid>
        <Card.Content>
          <Card.Header>Side Letter Properties</Card.Header>
        </Card.Content>
        <Card.Content>
          <SideLetterForm deal={deal} onSubmit={onSubmit} />
        </Card.Content>
      </Card>
      <PopupModal
        open={showAddSubscriber}
        size="fullscreen"
        heading="Add Investors"
        content={
          <>
            <SubscriberSearch
              subscriptions={subscriptions.filter(
                (s) =>
                  filteredSubscriptions.filter((f) => f.id === s.id).length <= 0
              )}
              onSelection={(selectedId) => {
                const isSelectedInRemoved =
                  removedSubscriptionIds.filter((s) => s === selectedId)
                    .length > 0;
                if (isSelectedInRemoved) {
                  const updatedRemovedSubscriptionIds =
                    removedSubscriptionIds.filter((s) => s !== selectedId);
                  setRemovedSubscriptionIds(updatedRemovedSubscriptionIds);
                } else {
                  if (!selectedIds.includes(selectedId)) {
                    const updatedSelectedIds = [...selectedIds, selectedId];
                    setSelectedIds(updatedSelectedIds);
                  }
                }
                setShowAddSubscriber(false);
              }}
            />
          </>
        }
        closecaption="Close"
        onClickClose={() => setShowAddSubscriber(false)}
      />
    </>
  );
}
