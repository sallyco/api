import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "../../rootReducer";
import { fetchDealById, setCurrentDealContext } from "../../slices/dealsSlice";
import { fetchAssetById } from "../../slices/assetsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { getBaseUrl } from "../../tools/urls";
import {
  Button,
  Card,
  Divider,
  Grid,
  Header,
  Image as SImage,
  Segment,
  Table,
} from "semantic-ui-react";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import { Alert } from "@mui/material";

interface Props {
  dealId: string;
}

const DealDescriptionCard = ({ dealId }: Props) => {
  const dispatch = useDispatch();

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const assets = useSelector((state: RootState) => {
    return !deal
      ? []
      : Object.values(state.assets.assetsById).filter(
          (asset) => asset.dealId === deal.id
        );
  });

  const useStyles = makeStyles((theme) => {
    return {
      root: {
        "& img": {
          maxWidth: "100%",
        },
      },
    };
  });

  const classes = useStyles();

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
      dispatch(setCurrentDealContext(dealId));
    }
  }, [dispatch, dealId, deal]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  useEffect(() => {
    if (deal && assets && Array.isArray(deal?.assetIds)) {
      deal.assetIds.forEach((val, idx, arr) => {
        if (assets.findIndex((el) => el.id === val) === -1) {
          dispatch(fetchAssetById(val));
        }
      });
    }
  }, [deal]);

  const dealurl = getBaseUrl() + `/invest-now?deal=${deal?.id ?? ""}`;

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Grid columns="equal">
            <Grid.Column>
              <Header as={"h2"}>{deal?.name}</Header>
              <Header.Subheader>{deal?.marketing?.tagline}</Header.Subheader>
              <Card.Meta
                className={classes.root}
                dangerouslySetInnerHTML={{ __html: deal?.description }}
              />
            </Grid.Column>
          </Grid>
        </Card.Content>

        {assets && assets?.length > 0 ? (
          <Card.Content textAlign="center" fluid>
            <Table compact>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Asset Name</Table.HeaderCell>
                  <Table.HeaderCell>Asset Type</Table.HeaderCell>
                  <Table.HeaderCell />
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {assets.map((asset) => (
                  <Table.Row key={asset.id}>
                    <Table.Cell
                      style={{
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                      }}
                    >
                      {asset?.name}
                    </Table.Cell>
                    <Table.Cell>{asset?.type}</Table.Cell>
                    {/*<Table.Cell>*/}
                    {/*  <Segment basic content={asset?.allocationAmount} />*/}
                    {/*</Table.Cell>*/}
                    {/*<Table.Cell>*/}
                    {/*  <Segment basic content={asset?.subType} />*/}
                    {/*</Table.Cell>*/}
                    <Table.Cell>
                      <Button
                        as={Link}
                        // This needs to be changed to a better resolver, and not use the asset type in url
                        to={`/assets/${
                          { COMPANY: "companies", ARTWORK: "artwork" }[
                            asset?.type
                          ]
                        }/${asset?.id}`}
                        primary
                        content="View"
                      />
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
            <Divider hidden />
            <Button
              as={Link}
              to={`/assets/create?dealId=${deal?.id}`}
              primary
              content="Add an Asset"
            />
          </Card.Content>
        ) : (
          <Card.Content textAlign="center">
            <Segment
              basic
              content="Inform your investors by adding details about the company or asset being purchased"
            />
            <Button
              as={Link}
              to={`/assets/create?dealId=${deal?.id}`}
              primary
              content="Add an Asset"
            />
          </Card.Content>
        )}
      </Card>
    </>
  );
};

export default DealDescriptionCard;
