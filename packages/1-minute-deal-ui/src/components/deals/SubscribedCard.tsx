import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts";
import {
  insertSubscription,
  fetchSubscriptionsListByDeal,
} from "../../slices/subscriptionsSlice";
import { fetchSelf } from "../../slices/usersSlice";

import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";

import Log from "../../tools/Log";

import {
  Grid,
  Table,
  Accordion,
  Segment,
  Card,
  Button,
} from "semantic-ui-react";

import { toast } from "react-toastify";
import { fetchClosesList } from "../../slices/closesSlice";
import { useDealPerformanceSummarySWR } from "../../swrConnector/deals";

interface Props {
  dealId: string;
}

const SubscribedCard: React.FC<Props> = ({ dealId }) => {
  const dispatch = useDispatch();
  const [showDetails, setShowDetails] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const userProfile: any = useContext(AuthContext);
  const financeSummary: any = useDealPerformanceSummarySWR(dealId);

  // DEALS
  const { dealsById } = useSelector((state: RootState) => state.deals);
  const deal = dealsById[dealId];

  useEffect(() => {
    if (dealId && !deal) dispatch(fetchDealById(dealId));
  }, [dealId, deal, dispatch]);

  // CLOSES
  const { closesById } = useSelector((state: RootState) => state.closes);
  useEffect(() => {
    dispatch(fetchClosesList());
  }, []);

  // SUBBSCRIPTIONS
  const { subscriptionsByDealId, error } = useSelector(
    (state: RootState) => state.subscriptions
  );

  const subscriptions = Object.values(subscriptionsByDealId[dealId] ?? {}).map(
    (subscription) => {
      return {
        ...subscription,
        closes: Object.values(closesById).filter((el) =>
          el?.subscriptions?.includes(subscription.id)
        ),
      };
    }
  );
  const ownSub = subscriptions.find((obj) => {
    return obj.email === userProfile.email;
  });

  async function addSelf() {
    setSubmitting(true);
    const user: any = await dispatch(fetchSelf());
    Log.debug("user", user);

    await dispatch(
      insertSubscription({
        email: user.username,
        name: `${user.firstName} ${user.lastName}`,
        ownerId: user.id,
        dealId: dealId,
        status: "INVITED",
        acquisitionMethod: "INVITATION",
      })
    );

    setSubmitting(false);
  }

  useEffect(() => {
    if (error) {
      toast.error("Service Unavailable");
    } else if (dealId && !(dealId in subscriptionsByDealId) && !error)
      dispatch(fetchSubscriptionsListByDeal(dealId));
  }, [dealId, subscriptions, dispatch]);

  return (
    <Card>
      <Card.Content>
        <Card.Header>
          <Accordion>
            <Accordion.Title
              active={showDetails}
              index={0}
              onClick={() => setShowDetails(!showDetails)}
            >
              <Grid columns="equal">
                <Grid.Column>Total Subscribed</Grid.Column>
                <Grid.Column textAlign="right">
                  <>
                    {!ownSub ? (
                      <Button type="button" secondary onClick={addSelf}>
                        + Add yourself as investor
                      </Button>
                    ) : (
                      <Segment
                        basic
                        inverted
                        size="tiny"
                        className="no-padding"
                      >
                        {submitting ? (
                          <span style={{ color: "green" }}>Adding...</span>
                        ) : (
                          <Button
                            as={Link}
                            to={`/subscriptions/${ownSub.id}`}
                            secondary
                          >
                            View as investor
                          </Button>
                        )}
                      </Segment>
                    )}
                  </>
                </Grid.Column>
                {/*
                <Grid.Column textAlign="right">
                  <NumberFormat
                    value={0}
                    style={{ color: "green" }}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"$"}
                  ></NumberFormat>
                  <Icon name="dropdown" size="small"></Icon>
                </Grid.Column>
*/}
              </Grid>
            </Accordion.Title>
            <Accordion.Content active={showDetails}>
              {/*<PerformanceSummaryWidget*/}
              {/*  primaryNumber={financeSummary?.data?.raisedAmount ?? 0}*/}
              {/*  secondaryNumber={financeSummary?.data?.committedAmount ?? 0}*/}
              {/*  targetNumber={financeSummary?.data?.targetAmount ?? 0}*/}
              {/*/>*/}
            </Accordion.Content>
          </Accordion>
        </Card.Header>
      </Card.Content>
    </Card>
  );
};

export default SubscribedCard;
