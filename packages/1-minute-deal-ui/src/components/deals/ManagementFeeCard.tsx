import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { Table, Card, Modal, Grid, Icon } from "semantic-ui-react";
import ManagementFeeForm from "../../forms/deals/ManagementFeeForm";
import NumberFormat from "react-number-format";
import { Button, Typography } from "@mui/material";
import { PencilIcon } from "react-line-awesome";
import { Box } from "@mui/material";
import useStyles from "./informationBlocks/styles";

interface Props {
  dealId: string;
  editable?: boolean;
}

const ManagementFeeCard = ({ dealId, editable = true }: Props) => {
  const dispatch = useDispatch();
  const [showEdit, setShowEdit] = useState(false);
  const classes = useStyles();

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });
  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  useEffect(() => {
    if (!dealsLoading && !dealsError && (!deal || deal.id !== dealId)) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dealsError, dealsLoading, dispatch]);

  return (
    <>
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Header>Management Fee</Modal.Header>
        <Modal.Content>
          <ManagementFeeForm
            entity={entity}
            setShowEdit={setShowEdit}
            targetAmount={deal?.targetRaiseAmount ?? 0}
          />
        </Modal.Content>
      </Modal>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Box display={"flex"} flexDirection={"row"}>
              <Box flexGrow={1} textAlign={"left"}>
                Management Fee
              </Box>
              <Box>
                {editable && (
                  <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<PencilIcon />}
                    onClick={() => setShowEdit(true)}
                  >
                    Edit
                  </Button>
                )}
              </Box>
            </Box>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Typography
            color={"secondary"}
            className={classes.informationalBlockTitle}
            data-testid="management-fee"
          >
            {entity?.managementFee?.amount === null ||
            entity?.managementFee?.amount === undefined ? (
              "Not set"
            ) : (
              <>
                {entity?.managementFee?.type === "flat" && (
                  <NumberFormat
                    value={entity?.managementFee?.amount}
                    displayType={"text"}
                    thousandSeparator={true}
                    decimalScale={2}
                    fixedDecimalScale={true}
                    prefix={"$"}
                    data-testid="management-fee-flat"
                  />
                )}
                {entity?.managementFee?.type === "percent" && (
                  <NumberFormat
                    value={entity?.managementFee?.amount}
                    displayType={"text"}
                    suffix={"%"}
                    data-testid="management-fee-percent"
                  />
                )}
              </>
            )}
          </Typography>
          <Typography
            variant="h5"
            className={classes.informationalBlockSubTitle}
          >
            {!entity?.managementFee?.amount ||
            entity?.managementFee?.amount === 0 ? (
              ""
            ) : (
              <>{entity?.managementFee?.isRecurring ? "Annually" : ""}</>
            )}
          </Typography>
        </Card.Content>
      </Card>
    </>
  );
};

export default ManagementFeeCard;
