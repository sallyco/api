import React from "react";
import { Icon, Modal } from "semantic-ui-react";
import PropTypes from "prop-types";
//import Upload from "../../components/fileUpload/Upload";

const DeleteDocumentModal = ({ showModal, handleModal }) => (
  <Modal
    open={showModal}
    className={"delete-document-modal"}
    onMount={() => {
      const elem = document.getElementsByClassName(
        "ui page modals dimmer transition visible active"
      )[0];
      elem.addEventListener("click", () => handleModal());
    }}
    trigger={
      <Icon
        onClick={handleModal}
        name="upload"
        size="small"
        className={
          "documents-tab-document-icon-height documents-tab-upload-icon"
        }
      />
    }
  >
    {/* <Modal.Header>
      Delete fund docs and revert deal to <q>pending</q> status?
    </Modal.Header> */}
    <Modal.Content>
      <Modal.Description>Todo</Modal.Description>
    </Modal.Content>
  </Modal>
);

DeleteDocumentModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  handleModal: PropTypes.func.isRequired,
};
export default DeleteDocumentModal;
