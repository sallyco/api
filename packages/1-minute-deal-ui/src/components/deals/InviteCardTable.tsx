import React, { useState, useContext, useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import {
  deleteSubscriptionById,
  fetchSubscriptionsListByDeal,
} from "../../slices/subscriptionsSlice";
import { deleteInviteById } from "../../slices/invitesSlice";
import { toast } from "react-toastify";
import { TableWithTools } from "@packages/gbt-ui";
import moment from "moment";
import {
  Grid,
  Dropdown,
  Segment,
  Card,
  Button,
  Label,
} from "semantic-ui-react";

import { getDealById } from "../../api/dealsApi";
import { AuthContext } from "../../contexts";

import { SubscriptionStatusColors as ssc } from "../../tools/statusColors";

import {
  sendInviteInvestor,
  sendInviteInvestorExisting,
} from "../../api/emailsApi";
import { getUserByEmail } from "../../api/usersApi";
import { Invite } from "../../api/invitesApi";
import { Subscription } from "../../api/subscriptionsApi";
import { PopupModal } from "../common/PopupModal";

const MULTI_ACTIONS = {
  DELETE: 1,
  REINVITE: 2,
};

interface Props {
  subscriptionsByDealId: any;
  invitesById: any;
  dealId: string;
}

const ID_COLUMN_INDEX = 1;
const TYPE_COLUMN_INDEX = 2;
const EMAIL_COLUMN_INDEX = 4;
const STATUS_COLUMN_INDEX = 6;

const InviteCardTable = ({
  subscriptionsByDealId,
  invitesById,
  dealId,
}: Props) => {
  const dispatch = useDispatch();
  const [selectedDataIndexes, setSelectedDataIndexes] = useState([]);
  const initialRowsSelected: number[] = [];
  const [rowsSelected, setRowsSelected] = useState(initialRowsSelected);
  const [multiActionMessage, setMultiActionMessage] = useState("");
  const [confirmActionType, setConfirmActionType] = useState(0);
  const [showMultipleWarning, setShowMultipleWarning] = useState(false);

  let invites: Invite[] = Object.values(invitesById);
  invites = invites.filter(
    (invite: Invite) =>
      invite?.invitedTo?.filter((i) => i.id === dealId).length > 0
  );

  const subscriptions: () => Subscription[] = useCallback(
    () => Object.values(subscriptionsByDealId[dealId] ?? {}),
    [dealId, subscriptionsByDealId]
  );

  const [state, setState] = useState({
    column: "daysOpen",
    data: [],
    direction: "descending",
  });
  const userProfile: any = useContext(AuthContext);

  useEffect(() => {
    setRowsSelected(initialRowsSelected);
  }, []);

  const displayedInvites = () => {
    return invites.filter(
      (invite) =>
        invite.invitedTo.filter((i) => i.id === dealId).length > 0 &&
        !subscriptions()
          .filter((sub) => sub.status !== "INVITED")
          .map((sub) => sub.email)
          .includes(invite.acceptableBy)
    );
  };

  const invitationData = useCallback(() => {
    return subscriptions()
      .filter((subscription) => subscription.status === "INVITED")
      .map((subscription, index) => ({
        id: subscription.id,
        name: subscription.name,
        email: subscription.email,
        phone: "not registered",
        status: subscription.status,
        daysOpen: moment(new Date()).diff(
          moment(new Date(subscription.createdAt)),
          "days"
        ),
        type: "subscription",
      }))
      .concat(
        displayedInvites().map((invite, index) => ({
          id: invite.id,
          name: "not registered",
          email: invite.acceptableBy,
          phone: "not registered",
          status: "INVITED",
          daysOpen: moment(new Date()).diff(
            moment(new Date(invite.createdAt)),
            "days"
          ),
          type: "invite",
        }))
      );
  }, [subscriptions, subscriptionsByDealId]);

  const getDropdownOptions = (rowData) => {
    let options = [];
    switch (rowData[TYPE_COLUMN_INDEX]) {
      case "subscription":
        options = [
          {
            key: "resendExisting",
            text: "Resend Invitation",
            value: "resendExisting",
            icon: "mail",
            onClick: async () => {
              if (rowsSelected.length > 1) {
                setMultiActionMessage(
                  `"Resend ${rowsSelected.length} Invitations for this Deal", continue?`
                );
                setConfirmActionType(MULTI_ACTIONS.REINVITE);
                setShowMultipleWarning(true);
              } else {
                const deal = await getDealById(dealId);
                const user = await getUserByEmail(rowData[EMAIL_COLUMN_INDEX]);
                await sendInviteInvestorExisting(
                  user,
                  rowData[EMAIL_COLUMN_INDEX] ?? "",
                  deal,
                  {
                    id: rowData[ID_COLUMN_INDEX],
                  }
                );
                toast.success(
                  `Invitation resent to ${rowData[EMAIL_COLUMN_INDEX]}`
                );
              }
            },
          },
        ];
        if (["INVITED", "PENDING"].includes(rowData[STATUS_COLUMN_INDEX])) {
          options.push({
            key: "delete",
            text: "Delete Invitation",
            value: "delete",
            icon: "delete",
            onClick: async (e, data) => {
              if (rowsSelected.length > 1) {
                setMultiActionMessage(
                  `"Delete ${rowsSelected.length} Invitations from this Deal", continue?`
                );
                setConfirmActionType(MULTI_ACTIONS.DELETE);
                setShowMultipleWarning(true);
              } else {
                await dispatch(
                  deleteSubscriptionById(rowData[ID_COLUMN_INDEX])
                );
                await dispatch(fetchSubscriptionsListByDeal(dealId));
              }
            },
          });
        }
        return options;
      case "invite":
        return [
          {
            key: "resend",
            text: "Resend Invitation",
            icon: "mail",
            value: "resend",
            onClick: async () => {
              if (rowsSelected.length > 1) {
                setMultiActionMessage(
                  `"Resend ${rowsSelected.length} Invitations for this Deal", continue?`
                );
                setConfirmActionType(MULTI_ACTIONS.REINVITE);
                setShowMultipleWarning(true);
              } else {
                const deal = await getDealById(dealId);
                await sendInviteInvestor(
                  rowData[EMAIL_COLUMN_INDEX],
                  userProfile.email ?? "",
                  deal,
                  rowData[ID_COLUMN_INDEX]
                );
                toast.success(
                  `Invitation resent to ${rowData[EMAIL_COLUMN_INDEX]}`
                );
              }
            },
          },
          {
            key: "delete",
            text: "Delete Invitation",
            value: "delete",
            icon: "delete",
            onClick: (e, data) => {
              if (rowsSelected.length > 1) {
                setMultiActionMessage(
                  `"Delete ${rowsSelected.length} Invitations from this Deal", continue?`
                );
                setConfirmActionType(MULTI_ACTIONS.DELETE);
                setShowMultipleWarning(true);
              } else {
                dispatch(deleteInviteById(rowData[ID_COLUMN_INDEX]));
              }
            },
          },
        ];
      default:
        return [];
    }
  };

  const confirmMultiAction = async (type) => {
    switch (type) {
      case MULTI_ACTIONS.DELETE:
        removeSelectedInvites();
        break;
      case MULTI_ACTIONS.REINVITE:
        resendSelectedInvites();
        break;
      default:
    }
  };

  const getSubscriptionSelections = useCallback(() => {
    return selectedDataIndexes.map((selectedIndexNumber) => {
      return invitationData().find(
        (val, index) => selectedIndexNumber === index
      );
    });
  }, [selectedDataIndexes, invitationData]);

  const removeSelectedInvites = async () => {
    const rows = getSubscriptionSelections();
    for (let row of rows) {
      if (row?.id) {
        if (row?.type === "subscription") {
          // eslint-disable-next-line no-await-in-loop
          await dispatch(deleteSubscriptionById(row.id));
        } else if (row?.type === "invite") {
          // eslint-disable-next-line no-await-in-loop
          await dispatch(deleteInviteById(row.id));
        }
      }
    }
    await dispatch(fetchSubscriptionsListByDeal(dealId));
  };

  const resendSelectedInvites = async () => {
    const rows = getSubscriptionSelections();
    const deal = await getDealById(dealId);
    const sentEmails: string[] = [];
    for (let row of rows) {
      if (row?.id) {
        if (row?.type === "subscription") {
          // eslint-disable-next-line no-await-in-loop
          const user = await getUserByEmail(row.email);
          // eslint-disable-next-line no-await-in-loop
          await sendInviteInvestorExisting(user, row.email ?? "", deal, {
            id: row.id,
          });
          sentEmails.push(row.email);
        } else if (row?.type === "invite") {
          const rowId: any = row.id;
          // eslint-disable-next-line no-await-in-loop
          await sendInviteInvestor(
            row.email,
            userProfile.email ?? "",
            deal,
            rowId
          );
          sentEmails.push(row.email);
        }
      }
    }
    toast.success(`Invitation resent to ${sentEmails.length} email ids`);
  };

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column>Invitations</Grid.Column>
              <Grid.Column textAlign="right">
                <Button
                  as={Link}
                  to={`/deals/${dealId}/invite-investors`}
                  secondary
                  icon="add user"
                  content="Invite Investors"
                />
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content style={{ overflowX: "scroll" }}>
          {(!subscriptions() ||
            subscriptions().filter(
              (subscription) => subscription.status === "INVITED"
            ).length === 0) &&
          displayedInvites().length === 0 ? (
            <Segment inverted className={"primaryBackground"}>
              There are no open invitations in this deal. You can{" "}
              <Link
                className={"secondaryColor bold"}
                to={`/deals/${dealId}/invite-investors`}
              >
                invite investors
              </Link>{" "}
              to this deal.
            </Segment>
          ) : (
            <React.Fragment>
              <TableWithTools
                rowsPerPage={10}
                data={invitationData()}
                columnDefinitions={[
                  {
                    name: "#",
                    options: {
                      display: true,
                      filter: false,
                      download: false,
                      customBodyRender: (value, tableMeta) => {
                        const rowNumber = invitationData().findIndex(
                          (x) => x.id === tableMeta.rowData[ID_COLUMN_INDEX]
                        );
                        return rowNumber > -1 ? rowNumber + 1 : "-";
                      },
                      sortCompare: (order) => {
                        return (obj1, obj2) => {
                          // Search the large list once
                          const obj1Index = invitationData().findIndex(
                            (x) => x.id === obj1.rowData[ID_COLUMN_INDEX]
                          );
                          const obj2Index = invitationData().findIndex(
                            (x) => x.id === obj2.rowData[ID_COLUMN_INDEX]
                          );

                          return obj1Index > obj2Index && order === "asc"
                            ? 1
                            : -1;
                        };
                      },
                    },
                  },
                  {
                    name: "id",
                    options: {
                      display: false,
                      filter: false,
                    },
                  },
                  {
                    name: "type",
                    options: {
                      display: "excluded",
                      filter: false,
                    },
                  },
                  {
                    name: "name",
                    label: "Name",
                    options: {
                      filter: false,
                    },
                  },
                  {
                    name: "email",
                    label: "E-mail",
                    options: {
                      filter: false,
                    },
                  },
                  {
                    name: "phone",
                    label: "Phone",
                    options: {
                      filter: false,
                    },
                  },
                  {
                    name: "status",
                    label: "Status",
                    options: {
                      // eslint-disable-next-line react/display-name
                      customBodyRender: (value, tableMeta, updateValue) => {
                        return <Label color={ssc(value)}>{value}</Label>;
                      },
                    },
                  },
                  {
                    name: "daysOpen",
                    label: "Days Open",
                    options: {
                      filter: false,
                    },
                  },
                  {
                    name: "actions",
                    label: "Actions",
                    // eslint-disable-next-line react/display-name
                    options: {
                      filter: false,
                      // eslint-disable-next-line react/display-name
                      customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                          <Dropdown
                            className="icon"
                            icon={"ellipsis vertical"}
                            floating
                            direction={
                              window.outerWidth < 768 ? "right" : "left"
                            }
                            trigger={<React.Fragment />}
                          >
                            <Dropdown.Menu>
                              {getDropdownOptions(tableMeta.rowData).map(
                                (option) => (
                                  <Dropdown.Item
                                    key={option.value}
                                    {...option}
                                  />
                                )
                              )}
                            </Dropdown.Menu>
                          </Dropdown>
                        );
                      },
                    },
                  },
                ]}
                customToolbarSelect={function (selectedRows) {
                  return <></>;
                }}
                selectable
                rowsSelected={rowsSelected}
                onRowSelectionChange={(
                  currentRowsSelected: any[],
                  allRowsSelected: any[],
                  rowsSelected: any[]
                ) => {
                  setRowsSelected(rowsSelected);
                  const dataIndexes = allRowsSelected.map((selection) => {
                    return selection.dataIndex;
                  });
                  setSelectedDataIndexes(dataIndexes);
                }}
              />
            </React.Fragment>
          )}
        </Card.Content>
      </Card>
      <PopupModal
        open={showMultipleWarning}
        size="tiny"
        heading="Multiple Investors Selected..."
        content={<p>You are about to {multiActionMessage}</p>}
        okcaption="Yes, Proceed"
        closecaption="Cancel"
        onClickOk={() => {
          confirmMultiAction(confirmActionType);
          setShowMultipleWarning(false);
        }}
        onClickClose={() => setShowMultipleWarning(false)}
      />
    </>
  );
};

export default InviteCardTable;
