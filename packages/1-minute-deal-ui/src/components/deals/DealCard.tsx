import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { RootState } from "../../rootReducer";
import {
  fetchSubscriptionsListByDeal,
  insertSubscription,
} from "../../slices/subscriptionsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchSelf } from "../../slices/usersSlice";
import {
  Header,
  Label,
  Card,
  Image,
  Button,
  Progress,
  Transition,
  Table,
} from "semantic-ui-react";
import { AuthContext } from "../../contexts";
import { DealStatusColors as DealStatusColor } from "../../tools/statusColors";
import moment from "moment";
import NumberFormat from "react-number-format";
import Log from "../../tools/Log";
import { FeatureFlag } from "./../../components/featureflags/FeatureFlags";
import { useDealPerformanceSummarySWR } from "../../swrConnector/deals";
import { PerformanceSummaryWidget } from "@packages/gbt-ui";
import { DealStatusText } from "../../tools/dealStatuses";
import {
  fetchClosesList,
  fetchClosesListByDeal,
} from "../../slices/closesSlice";

const INVESTOR_LABEL_TEXT = "Investors Must Be A Qualified Purchaser";
const QP_FEATURE_FLAG = "qualified_purchaser";

type DCProps = {
  dealId: string;
};

const DealCard = ({ dealId }: DCProps) => {
  const userProfile = useContext(AuthContext);
  const dispatch = useDispatch();
  const history = useHistory();
  const [showMore, setShowMore] = useState(false);
  const financeSummary: any = useDealPerformanceSummarySWR(dealId);

  const [closes, setCloses] = useState([]);

  const { subscriptionsByDealId } = useSelector(
    (state: RootState) => state.subscriptions
  );

  //  const { isLoading: entitiesLoading, error: entitiesListError } = useSelector(
  //    (state: RootState) => state.entities
  //  );

  //  const { isLoading: dealsLoading, error: dealssListError } = useSelector(
  //    (state: RootState) => state.deals
  //  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const subscriptions = useSelector((state: RootState) => {
    return !deal
      ? []
      : Object.values(state.subscriptions.subscriptionsByDealId[deal.id] ?? []);
  });

  const { closesById } = useSelector((state: RootState) => state.closes);
  useEffect(() => {
    if (deal) {
      setCloses(
        Object.values(closesById).filter((close) => close.dealId === deal.id)
      );
    }
  }, [closesById]);
  useEffect(() => {
    if (dealId) {
      dispatch(fetchClosesListByDeal(dealId));
    }
  }, [dealId, dispatch]);

  // const subscriptions = Object.values(subscriptionsByDealId[deal.id] ?? {});
  const committedAmount =
    subscriptions.reduce((sum, key) => sum + (key.amount | 0), 0) +
    (deal.previouslyRaisedAmount ? deal.previouslyRaisedAmount : 0);

  Log.debug("subscriptions", subscriptions);

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  useEffect(() => {
    if (deal && deal.entityId && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  useEffect(() => {
    if (dealId) {
      dispatch(fetchSubscriptionsListByDeal(dealId));
    }
  }, [dispatch, deal, dealId]);

  const committedPercent =
    !(subscriptions || subscriptions.length === 0) &&
    !deal?.previouslyRaisedAmount
      ? 0
      : calculateCommittedPercent(committedAmount, deal?.targetRaiseAmount);
  //const committedPercent = (
  //  (!subscriptions || subscriptions === {}) ? 0 :(
  //calculateCommittedPercent(
  //  subscriptions.values.reduce( function(amt,entry){ return amt + entry.amount; }, 0)??0,
  //  deal?.targetRaiseAmount
  //)));

  const timeRemaining = calculateTimeRemaining(deal?.estimatedCloseDate);

  async function addSelf(e) {
    e.preventDefault();
    const user: any = await dispatch(fetchSelf());
    const subscription: any = await dispatch(
      insertSubscription({
        email: user.username,
        name: `${user.firstName} ${user.lastName}`,
        ownerId: user.id,
        dealId: dealId,
        status: "INVITED",
        acquisitionMethod: "INVITATION",
      })
    );
    subscription?.id
      ? history.push(`/subscriptions/${subscription.id}`)
      : history.push(`/deals/${dealId}`);
  }

  function canInvest() {
    return (
      deal.status === "OPEN" &&
      subscriptions.reduce(
        (hasMatch, obj) => hasMatch + (obj.email === userProfile.email ? 1 : 0),
        0
      ) === 0
    );
  }

  return (
    <Transition animation={"fade up"}>
      <Card color="grey" raised>
        <Card.Content
          as={Link}
          to={`/deals/${dealId}`}
          style={{ display: "block" }}
        >
          <Table basic="very" celled compact unstackable>
            <Table.Body>
              <Table.Row>
                <Table.Cell width={12}>
                  <Header as="h2" className="deal-card-header">
                    {deal?.name}
                  </Header>
                  <FeatureFlag name={QP_FEATURE_FLAG}>
                    {deal?.requireQualifiedPurchaser && INVESTOR_LABEL_TEXT}
                  </FeatureFlag>
                </Table.Cell>
                {deal?.marketing?.logo && (
                  <Table.Cell rowSpan="2" textAlign="center">
                    <Image centered fluid src={deal?.marketing?.logo} />
                  </Table.Cell>
                )}
              </Table.Row>

              <Table.Row>
                <Table.Cell>
                  <Label
                    circular
                    color={DealStatusColor(
                      DealStatusText({ ...deal, ...{ closes } })
                    )}
                  >
                    {DealStatusText({ ...deal, ...{ closes } })}
                  </Label>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                {/* <Table.Cell>
                  <Progress
                    percent={committedPercent}
                    className={"primaryColor"}
                    size="small"
                    style={{ margin: 0 }}
                  />
                </Table.Cell> */}
                {canInvest() && (
                  <Table.Cell>
                    <Button
                      size="tiny"
                      onClick={addSelf}
                      primary
                      content="Invest Now"
                    />
                  </Table.Cell>
                )}
              </Table.Row>
            </Table.Body>
          </Table>

          <PerformanceSummaryWidget
            primaryNumber={financeSummary?.data?.raisedAmount ?? 0}
            secondaryNumber={financeSummary?.data?.committedAmount ?? 0}
            targetNumber={financeSummary?.data?.targetAmount ?? 0}
          />

          <Table basic="very" compact unstackable widths="equal">
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <Header as="h2" color="green">
                    {
                      subscriptions.filter(
                        (subscription) => subscription.status === "COMPLETED"
                      ).length
                    }
                    <Header.Subheader>investors funded</Header.Subheader>
                  </Header>
                </Table.Cell>
                <Table.Cell textAlign="right">
                  <Header as="h2" color="grey">
                    {
                      subscriptions.filter(
                        (subscription) => subscription.status !== "COMPLETED"
                      ).length
                    }
                    <Header.Subheader>in process</Header.Subheader>
                  </Header>
                </Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>
                  <Header as="h2" color="orange">
                    {timeRemaining}
                    <Header.Subheader>days to close</Header.Subheader>
                  </Header>
                </Table.Cell>
                <Table.Cell textAlign="right">
                  <Header as="h2" color="grey">
                    {moment.utc(deal?.estimatedCloseDate).format("L")}
                    <Header.Subheader>close date</Header.Subheader>
                  </Header>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Card.Content>

        <Card.Content
          extra
          as={Link}
          to={`/deals/${dealId}`}
          textAlign="center"
        >
          {/*
          <Accordion>
            <Accordion.Title
              active={showMore}
              index={0}
              onClick={() => setShowMore(!showMore)}
            >
              {showMore ? "Show Less" : "Show More"}
              <Icon name="dropdown" size="small"></Icon>
            </Accordion.Title>
            <Accordion.Content active={showMore}>
              <List divided relaxed>
                <List.Item>
                  <List.Content>
                    <List.Header as="a"></List.Header>
                    <List.Description as="a">Invites</List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="a"></List.Header>
                    <List.Description as="a">
                      Investor Onboarding Status
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="a"></List.Header>
                    <List.Description as="a">Action Items</List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </Accordion.Content>
          </Accordion>
*/}
        </Card.Content>
      </Card>
    </Transition>
  );
};

function calculateTimeRemaining(estimatedCloseDate) {
  const oneDay = 24 * 60 * 60 * 1000;
  let timeRemaining = 0;
  if (estimatedCloseDate !== undefined) {
    const closingDate = new Date(
      estimatedCloseDate.slice(0, 4),
      estimatedCloseDate.slice(5, 7) - 1,
      estimatedCloseDate.slice(8, 10),
      0,
      0,
      0,
      0
    );
    const today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    if ((closingDate.getTime() - today.getTime()) / oneDay > 0) {
      timeRemaining = Math.ceil(
        (closingDate.getTime() - today.getTime()) / oneDay
      );
    } else {
      timeRemaining = 0;
    }
  }
  return timeRemaining;
}

function calculateCommittedPercent(committed, raised) {
  if (typeof committed === "string") {
    committed = parseInt(committed, 10);
  }
  const percent = ((committed ?? 0) / raised) * 100;
  if (percent.toFixed) {
    return percent.toFixed(2);
  }
  return percent;
}

export default DealCard;
