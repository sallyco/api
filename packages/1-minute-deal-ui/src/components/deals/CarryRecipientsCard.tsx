import React, { useContext, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchDealById, updateDealById } from "../../slices/dealsSlice";
import {
  fetchEntityById,
  generateEntityDocument,
  patchEntityById,
} from "../../slices/entitySlice";
import { fetchProfileById } from "../../slices/profilesSlice";
import { getProfileName } from "../../api/profileApiWrapper";
import CarryRecipientsForm from "../../forms/deals/CarryRecipientsForm";
import AddCarryRecipientForm from "../../forms/deals/AddCarryRecipientForm";
import IndividualCarryRecipientsForm from "../../forms/deals/IndividualCarryRecipientForm";
import { Table, Card, Segment, Modal, Button } from "semantic-ui-react";

import NumberFormat from "react-number-format";
import { Box, Button as MButton } from "@mui/material";
import { PencilIcon } from "react-line-awesome";
import { TenantContext } from "../../contexts";

interface Props {
  dealId: string;
  editable?: boolean;
}

const CarryRecipientsCard = ({ dealId, editable = true }: Props) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const [showEdit, setShowEdit] = useState(false);
  const [showAddNew, setShowAddNew] = useState(false);
  const [showEditCarry, setShowEditCarry] = useState(false);
  const [selectedCarry, setSelectedCarry] = useState(null);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const profile = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entity]);

  useEffect(() => {
    if (deal && (!profile || profile.id !== deal.profileId)) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [deal, dispatch, profile]);

  const deleteCarryIndividual = async (carry) => {
    await dispatch(
      updateDealById({
        ...deal,
        additionalCarryRecipients: deal.additionalCarryRecipients.filter(
          (carryInQuestion) =>
            carryInQuestion.individual.name !== carry.individual.name &&
            carryInQuestion.carryPercentage !== carry.carryPercentage
        ),
      })
    );

    const oaId = await updateOA();
    await dispatch(
      patchEntityById(entity.id, {
        entityDocuments: {
          ...entity.entityDocuments,
          operatingAgreement: oaId,
        },
      })
    );
  };

  const updateOA = async () => {
    const fileData = await dispatch(
      generateEntityDocument(deal.entityId, "oa")
    );
    return fileData["id"];
  };

  const editCarryIndividual = (carry) => {
    setSelectedCarry(carry);
    setShowEditCarry(true);
  };

  return (
    <>
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Header>Carry Recipients</Modal.Header>
        <Modal.Content>
          <CarryRecipientsForm deal={deal} setShowEdit={setShowEdit} />
        </Modal.Content>
      </Modal>
      <Modal open={showAddNew} onClose={() => setShowAddNew(false)} closeIcon>
        <Modal.Header>Add Carry Recipients</Modal.Header>
        <Modal.Content>
          <AddCarryRecipientForm deal={deal} setShowAddNew={setShowAddNew} />
        </Modal.Content>
      </Modal>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Box display={"flex"} flexDirection={"row"}>
              <Box flexGrow={1} textAlign={"left"}>
                Carry Recipients
              </Box>
              <Box>
                {editable && (
                  <MButton
                    variant="contained"
                    color="secondary"
                    startIcon={<PencilIcon />}
                    onClick={() => setShowEdit(true)}
                  >
                    Edit
                  </MButton>
                )}
              </Box>
            </Box>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          {typeof deal?.organizerCarryPercentage === "number" &&
          deal?.organizerCarryPercentage > 0 ? (
            <>
              <Table definition basic compact size="small" unstackable>
                <Table.Body>
                  {(editable ||
                    tenantProfile?.settings
                      ?.showCarryRecipientsForInvestors) && (
                    <>
                      <Table.Row>
                        <Table.Cell>Organizer</Table.Cell>
                        <Table.Cell>{getProfileName(profile)}</Table.Cell>
                        <Table.Cell>
                          <NumberFormat
                            value={deal?.organizerCarryPercentage ?? 0}
                            displayType={"text"}
                            thousandSeparator={true}
                            suffix={"%"}
                            data-testid="organizer-carry"
                          ></NumberFormat>
                        </Table.Cell>
                      </Table.Row>
                      {Array.isArray(deal?.additionalCarryRecipients) &&
                        deal?.additionalCarryRecipients?.length > 0 &&
                        deal.additionalCarryRecipients.map((carry, index) => (
                          <Table.Row
                            key={`${carry.carryPercentage}_${carry.individual.name}`}
                          >
                            <Table.Cell>
                              <>{carry.individual.type}</>
                              {editable && (
                                <>
                                  <Button
                                    icon="x"
                                    onClick={() => deleteCarryIndividual(carry)}
                                    negative
                                    size={"mini"}
                                    floated={"right"}
                                    compact
                                    data-testid={`delete-carry-${index}-button`}
                                  />
                                  <Button
                                    icon="pencil"
                                    onClick={() => editCarryIndividual(carry)}
                                    positive
                                    size={"mini"}
                                    floated={"right"}
                                    compact
                                  />
                                </>
                              )}
                            </Table.Cell>
                            <Table.Cell>{carry.individual.name}</Table.Cell>
                            <Table.Cell>
                              <NumberFormat
                                value={carry.carryPercentage ?? 0}
                                displayType={"text"}
                                thousandSeparator={true}
                                suffix={"%"}
                                data-testid={`additional-carry-${index}`}
                              ></NumberFormat>
                            </Table.Cell>
                          </Table.Row>
                        ))}
                    </>
                  )}
                  <Table.Row>
                    <Table.Cell>Total</Table.Cell>
                    <Table.Cell></Table.Cell>
                    <Table.Cell>
                      <NumberFormat
                        value={
                          Array.isArray(deal?.additionalCarryRecipients) &&
                          deal?.additionalCarryRecipients?.length > 0
                            ? deal.additionalCarryRecipients.reduce(
                                (additionalTotal, recipient) =>
                                  recipient.carryPercentage + additionalTotal,
                                0
                              ) + (deal?.organizerCarryPercentage ?? 0)
                            : deal?.organizerCarryPercentage ?? 0
                        }
                        displayType={"text"}
                        thousandSeparator={true}
                        suffix={"%"}
                      ></NumberFormat>
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
              {editable && (
                <Button
                  secondary
                  content="+ Add New Recipient"
                  onClick={() => {
                    setShowEdit(false);
                    setShowAddNew(true);
                  }}
                />
              )}
            </>
          ) : (
            <Segment basic content="Carry Percentage is 0%" />
          )}
        </Card.Content>
      </Card>
      <Modal
        open={showEditCarry}
        onClose={() => setShowEditCarry(false)}
        closeIcon
      >
        <Modal.Header>Carry Recipients - Edit</Modal.Header>
        <Modal.Content>
          <IndividualCarryRecipientsForm
            deal={deal}
            setShowEditCarry={setShowEditCarry}
            carry={selectedCarry}
          />
        </Modal.Content>
      </Modal>
    </>
  );
};

export default CarryRecipientsCard;
