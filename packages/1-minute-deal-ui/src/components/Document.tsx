import React, { useEffect, useState } from "react";
import { Document as DocumentPreview, Page } from "react-pdf";
import axios from "axios";
import { toast } from "react-toastify";
import { fileIcons } from "../tools/fileIcons";
import * as parseCookie from "../api/parseCookie";
import {
  Card,
  Label,
  Loader,
  Modal,
  Button,
  Divider,
  Message,
} from "semantic-ui-react";
import { Box, LinearProgress, Typography } from "@mui/material";
import moment from "moment";
import { DownloadFile } from "../tools/downloadUtils";
import PdfDocument from "../components/PdfDocument";
import useSWR from "swr";
import { getFileById } from "../api/filesApi";

interface Props {
  fileId: string;
  size?:
    | "medium"
    | "big"
    | "small"
    | "large"
    | "mini"
    | "tiny"
    | "huge"
    | "massive"
    | undefined;
  loadingTitle: string;
  pageNumber?: number;
  setIsLoaded?: any;
}

export default function Document({
  fileId,
  size = "medium",
  loadingTitle = "Document",
  pageNumber = 1,
  setIsLoaded,
}: Props) {
  const [modalOpen, setModalOpen] = React.useState(false);
  const [fileData, setFileData] = useState("");
  const [numPages, setNumPages] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState(pageNumber);
  const token = parseCookie.getToken("__session");
  const [currentPercent, setCurrentPercent] = useState(0);

  const { data: file } = useSWR(
    () => (fileId ? `/files/${fileId}` : null),
    () => getFileById(fileId),
    {
      refreshInterval: 3000,
      refreshWhenHidden: true,
    }
  );

  const downloadDocument = async (fileId: string) => {
    const response = await axios({
      url:
        process.env.API_URL ??
        process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      onDownloadProgress: (progressEvent) => {
        const total = parseFloat(progressEvent.total);
        const current = progressEvent.loaded;
        let percentCompleted = Math.floor((current / total) * 100);
        setCurrentPercent(percentCompleted);
        if (percentCompleted >= 100 && setIsLoaded) {
          setIsLoaded(true);
        }
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return "";
    }
    return window.URL.createObjectURL(new Blob([response.data]));
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    if (file && !file.generating) {
      downloadFile();
    }
  }, [fileId, file]);

  const onPage = (type) => {
    var newPage = type ? currentPage + 1 : currentPage - 1;
    if (newPage > numPages) {
      newPage = numPages;
    } else if (newPage < 1) {
      newPage = 1;
    }
    setCurrentPage(newPage);
  };

  return (
    <>
      {!file || file.generating || fileData === "" ? (
        <>
          <Card>
            <Card.Header>
              <div style={{ aspectRatio: "1 / 1.2941" }}>
                <Divider hidden />
                {(!file || file.generating) && (
                  <>
                    <Box>
                      <Loader active inline="centered" />
                    </Box>
                    {file?.generating && (
                      <Box textAlign="center">
                        <Typography variant="overline" display="block">
                          Generating
                        </Typography>
                      </Box>
                    )}
                  </>
                )}
                {file && !file?.generating && currentPercent !== 100 && (
                  <>
                    <Box mx={2}>
                      <LinearProgress
                        variant="determinate"
                        value={currentPercent}
                      />
                    </Box>
                    <Box textAlign="center">
                      <Typography variant="overline" display="block">
                        Loading ({currentPercent}%)
                      </Typography>
                    </Box>
                  </>
                )}
                <Divider hidden />
              </div>
            </Card.Header>
            <Card.Content textAlign={"center"}>
              <Card.Header>
                Loading <br />
                &quot;{loadingTitle}&quot;
              </Card.Header>
            </Card.Content>
          </Card>
        </>
      ) : (
        <Modal
          onClose={() => setModalOpen(false)}
          onOpen={() => setModalOpen(true)}
          open={modalOpen}
          size={"large"}
          trigger={
            <Card>
              <div
                style={{
                  aspectRatio: "1 / 1.2941",
                  margin: "auto",
                }}
              >
                <PdfDocument
                  fileId={fileId}
                  width={210}
                  initialFileData={fileData}
                  disableClick
                />
              </div>
              <Card.Content>
                <Card.Header>{file.name.replace(/\.[^/.]+$/, "")}</Card.Header>
                <Card.Meta>{moment(file.createdAt).format("ll")}</Card.Meta>
                <Label attached="bottom right">{fileIcons(file)}</Label>
              </Card.Content>
            </Card>
          }
        >
          <Modal.Header>{file?.name}</Modal.Header>
          <Modal.Content style={{ display: "flex" }}>
            <div style={{ margin: "auto" }}>
              <DocumentPreview
                file={fileData}
                onLoadSuccess={({ numPages }) => setNumPages(numPages)}
                loading={
                  <Loader active inverted inline content="Loading File..." />
                }
                error={
                  <Message negative>
                    Failed to load file. Please refresh this page
                  </Message>
                }
              >
                <Page pageNumber={currentPage} width={600} />
              </DocumentPreview>
            </div>
          </Modal.Content>
          <Modal.Actions>
            <Button disabled={currentPage <= 1} onClick={() => onPage(0)}>
              Previous
            </Button>
            <Button
              disabled={currentPage >= numPages}
              onClick={() => onPage(1)}
            >
              Next
            </Button>
            <Button onClick={() => setModalOpen(false)}>Close</Button>
            <Button
              content="Download"
              labelPosition="right"
              icon="download"
              onClick={() => DownloadFile(file?.id, file?.name)}
              positive
            />
          </Modal.Actions>
        </Modal>
      )}
    </>
  );
}
