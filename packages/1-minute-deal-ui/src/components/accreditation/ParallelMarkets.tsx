import React, { useContext, useEffect, useState } from "react";
import {
  ParallelProvider,
  useParallel,
  PassportButton,
} from "@parallelmarkets/react";
import { loadParallel } from "@parallelmarkets/vanilla";
import {
  Avatar,
  Button,
  Card,
  CircularProgress,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { BuildingIcon, UserTieIcon } from "react-line-awesome";
import { API, doChange, useRequest } from "../../api/swrApi";
import { Alert } from "@mui/material";
import { TenantContext } from "../../contexts";

interface ProfileInterface {
  access_expires_at?: null;
  access_revoked_by?: null;
  id: string;
  profile: {
    email: string;
    first_name: string;
    last_name: string;
  };
  type: "individual";
  user_id: string;
  user_profile: {
    email: string;
    first_name: string;
    last_name: string;
  };
  user_providing_for: "self";
}

interface AccreditationsInterface {
  accreditations: [
    {
      assertion_type: "evaluator-assertion";
      certified_at: number;
      created_at: number;
      documents: [];
      expires_at?: null;
      first_name: string;
      id: string;
      last_name: string;
      status: "pending";
    }
  ];
  id: string;
  indicated_unaccredited: null;
  type: "individual";
  user_id: string;
  user_providing_for: "self";
}

interface AuthInterface {
  access_token: string;
  expires_in: number;
  refresh_expires_in: number;
  refresh_token: string;
  token_type: string;
}

const ParallelMarkets: React.FC = (props) => {
  const [parallel, setParallel] = useState();
  const tenantProfile = useContext(TenantContext);

  useEffect(() => {
    if (tenantProfile?.settings?.use506cAccreditation) {
      // Prevent script from loading twice in page
      const V1_URL = "https://app.parallelmarkets.com/sdk/v1/parallel.js";
      const findScript = () => {
        return document.querySelectorAll(`script[src^="${V1_URL}"]`).length > 1;
      };
      const parallelConfig = {
        client_id:
          tenantProfile?.settings?.parallelMarketsClientId ??
          "UZjxH53KEZrpGw4Og8U88",
        environment:
          (process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) ===
          "production"
            ? "production"
            : "demo",
        show_dismiss_button: true,
        force_accreditation_check: true,
      };
      if (!findScript()) {
        setParallel(loadParallel(parallelConfig));
      }
    }
  }, []);

  return (
    <>
      {tenantProfile?.settings?.parallelMarketsClientId && parallel ? (
        <ParallelProvider parallel={parallel}>
          {props.children}
        </ParallelProvider>
      ) : (
        <>{props.children}</>
      )}
    </>
  );
};

const ParallelMarketsButton: React.FC<{
  onLink: (linkData: {
    profile: ProfileInterface;
    accreditations: AccreditationsInterface;
    auth: AuthInterface;
  }) => Promise<void>;
}> = ({ onLink }) => {
  const {
    parallel,
    loginStatus,
    getAccreditations,
    getProfile,
    logout,
    login,
  } = useParallel();
  const { data: self } = useRequest(API.SELF);
  const [entity, setEntity] = useState(null);
  const [clicked, setClicked] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleButtonClick = (e) => {
    setClicked(true);
    login({
      email: self.email,
      first_name: self.firstName,
      last_name: self.lastName,
    });
  };

  useEffect(() => {
    if (loginStatus?.status === "connected" && clicked) {
      Promise.all([getProfile(), getAccreditations()]).then(
        async ([profile, accreditations]) => {
          setLoading(true);
          setEntity(accreditations);
          await onLink({
            profile: profile,
            accreditations: accreditations,
            auth: loginStatus.authResponse,
          });
          setLoading(false);
          logout();
        }
      );
    }
  }, [loginStatus]);

  return (
    <>
      {/*<h2>Status: {loginStatus?.status}</h2>*/}
      {loginStatus?.status === "not_authorized" && clicked && (
        <Alert severity={"warning"}>
          Unable to link accreditation details: You denied access
        </Alert>
      )}
      <Button
        variant={"outlined"}
        color={"primary"}
        onClick={handleButtonClick}
      >
        {loading ? <CircularProgress /> : "Add Accreditation"}
      </Button>
    </>
  );
};

export default ParallelMarkets;
export { ParallelMarketsButton };
