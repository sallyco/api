import React, { useContext } from "react";
import { Accreditation } from "../../api/accreditationsApi";
import {
  Box,
  Card,
  CardHeader,
  Chip,
  Hidden,
  Tooltip,
  Typography,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import VerifiedUserIcon from "@mui/icons-material/VerifiedUser";
import AutoRenewIcon from "@mui/icons-material/Autorenew";
import { API, doChange, useRequest } from "../../api/swrApi";
import moment from "moment";
import { ParallelMarketsButton } from "./ParallelMarkets";
import { TenantContext } from "../../contexts";
import { Alert } from "@mui/material";
import BrokerageAccountButton from "../../components/brokerageAccount/BrokerageAccountButton";

const useStyles = makeStyles((theme) => ({
  chip: {
    marginRight: "0.5em",
  },
  rotate: {
    animation: `$rotate 15000ms linear infinite`,
  },
  "@keyframes rotate": {
    "0%": {
      transform: "rotate(0deg)",
    },
    "100%": {
      transform: "rotate(360deg)",
    },
  },
}));

const AccreditationBadge: React.FC<{
  profileId?: string;
  onUpdate?: () => Promise<void>;
  showOnlyChip?: boolean;
}> = ({ profileId, onUpdate, showOnlyChip = false }) => {
  const { data: accreditations, revalidate: revalidateAccreditations } =
    useRequest<Accreditation[]>(
      API.ADD_FILTER(API.ACCREDITATIONS, {
        where: {
          profileId: profileId,
        },
      })
    );

  const tenantProfile = useContext(TenantContext);

  const styles = useStyles();

  const currentAccreditation =
    accreditations &&
    accreditations?.length > 0 &&
    accreditations.sort((a, b) =>
      moment(a.certificationDate).isAfter(moment(b.certificationDate)) ? 1 : 0
    )[0];

  let label = "NOT COMPLETED";
  let color = "white";
  let bgColor = "grey";
  let icon = <></>;
  let title = "";

  if (currentAccreditation?.status === "CURRENT") {
    if (currentAccreditation?.type === "506c") {
      label = "EXTERNALLY ACCREDITED";
      color = "white";
      bgColor = "green";
      icon = <VerifiedUserIcon />;
      title = ``;
    }
  } else if (currentAccreditation?.status === "PENDING") {
    label = "PROCESSING";
    color = "white";
    bgColor = "#1b1b5e";
    icon = <AutoRenewIcon className={styles.rotate} />;
    title = `Your Accreditation is being validated. Please check back later`;
  }
  return (
    <>
      {tenantProfile.settings.use506cAccreditation && (
        <>
          {showOnlyChip ? (
            <Chip
              label={label}
              clickable
              style={{
                backgroundColor: bgColor,
                color: color,
              }}
              onDelete={() => {}}
              deleteIcon={
                <>
                  {React.cloneElement(icon, {
                    style: {
                      color: color,
                    },
                    className: `${styles.chip} ${icon.props.className}`,
                  })}
                </>
              }
            />
          ) : (
            <Card>
              <CardHeader
                title={
                  <Box
                    display={"flex"}
                    justifyItems={"center"}
                    flexDirection={"column"}
                  >
                    <Typography
                      align={"center"}
                      variant={"h6"}
                      color={"textSecondary"}
                    >
                      Accreditation Status
                    </Typography>
                    <Tooltip title={title}>
                      <Chip
                        label={label}
                        clickable
                        style={{
                          backgroundColor: bgColor,
                          color: color,
                        }}
                        onDelete={() => {}}
                        deleteIcon={
                          <>
                            {React.cloneElement(icon, {
                              style: {
                                color: color,
                              },
                              className: `${styles.chip} ${icon.props.className}`,
                            })}
                          </>
                        }
                      />
                    </Tooltip>
                  </Box>
                }
              />
              <Box
                display={"flex"}
                justifyItems={"center"}
                flexDirection={"column"}
              >
                <Typography color={"textSecondary"} align={"center"}>
                  {currentAccreditation?.certificationDate && (
                    <>
                      Certified on{" "}
                      {moment(currentAccreditation?.certificationDate).format(
                        "MM/DD/YYYY"
                      )}
                    </>
                  )}
                </Typography>
                <Typography color={"textSecondary"} align={"center"}>
                  {currentAccreditation?.expirationDate && (
                    <>
                      Expires{" "}
                      {moment(currentAccreditation?.expirationDate).fromNow()}
                    </>
                  )}
                </Typography>
                {!currentAccreditation && (
                  <>
                    <Hidden mdUp>
                      <Alert severity={"warning"}>
                        Accreditation linking is currently only available on
                        desktop.
                      </Alert>
                    </Hidden>
                    <Hidden mdDown>
                      <ParallelMarketsButton
                        onLink={async (data) => {
                          await doChange(
                            currentAccreditation?.id
                              ? API.ACCREDITATION_BY_ID(
                                  currentAccreditation?.id
                                )
                              : API.ACCREDITATIONS,
                            {
                              id: currentAccreditation?.id,
                              profileId: profileId,
                              providerMeta: {
                                typeId: "PARALLELMARKETS",
                                auth: data.auth,
                              },
                            },
                            currentAccreditation?.id ? "PUT" : "POST"
                          );
                          if (onUpdate) {
                            await onUpdate();
                          }
                          await revalidateAccreditations();
                        }}
                      />
                    </Hidden>
                    <Hidden smDown>
                      <BrokerageAccountButton
                        profileId={profileId}
                        // onClick={() => { }}
                      />
                    </Hidden>
                  </>
                )}
              </Box>
            </Card>
          )}
        </>
      )}
    </>
  );
};
export default AccreditationBadge;
