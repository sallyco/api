import React from "react";
import { Link } from "react-router-dom";
import customerHome from "../../assets/images/deal_home_graphic.png";
import {
  Header,
  Button,
  Icon,
  Image,
  Container,
  Transition,
  Divider,
} from "semantic-ui-react";

const NoArtwork = () => {
  return (
    <Transition animation={"fade"}>
      <Container textAlign="center">
        <Image src={customerHome.src} size="medium" centered />
        <Header as="h2">
          <Header.Subheader>
            Welcome to the leading deal platform for organizers, investors and
            founders.
          </Header.Subheader>
        </Header>
        <Divider hidden />
        <Button primary as={Link} to="/assets/artwork/create" size="small">
          <Icon name="add" />
          List New Artwork
        </Button>
        <Divider hidden />
      </Container>
    </Transition>
  );
};
export default NoArtwork;
