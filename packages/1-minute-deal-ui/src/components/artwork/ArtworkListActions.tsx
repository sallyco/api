import React from "react";

import { Button, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

export default function ArtworkListActions() {
  return (
    <Menu borderless secondary className="nav-bar">
      <Menu.Item>
        <Link to="/assets/artwork/create">
          <Button primary size="tiny">
            + Add Artwork
          </Button>
        </Link>
      </Menu.Item>
    </Menu>
  );
}
