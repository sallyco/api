import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import NumberFormat from "react-number-format";
import { RootState } from "../../rootReducer";
import ImageFile from "../../components/ImageFile";

import {
  Header,
  Label,
  Card,
  Image,
  Table,
  List,
  Transition,
  Icon,
  Divider,
  Button,
  Modal,
  Grid,
} from "semantic-ui-react";
import { fetchAssetById } from "../../slices/assetsSlice";

type CCProps = {
  assetId: string;
};

const ArtworkCard = ({ assetId }: CCProps) => {
  const dispatch = useDispatch();
  const [firstOpen, setFirstOpen] = React.useState(false);
  const [secondOpen, setSecondOpen] = React.useState(false);

  const asset = useSelector(
    (state: RootState) => state.assets.assetsById[assetId]
  );

  useEffect(() => {
    if (!asset || asset.id !== assetId) {
      dispatch(fetchAssetById(assetId));
    }
  }, [dispatch, asset, assetId]);

  return (
    <Transition animation={"fade up"}>
      <Card color="grey" raised>
        {asset?.images && <ImageFile fileId={asset.images[0]} />}
        <Card.Content
          as={Link}
          to={`/assets/${assetId}`}
          style={{ display: "block" }}
        >
          <Table basic="very" celled compact unstackable>
            <Table.Body>
              <Table.Row>
                <Table.Cell width={12}>
                  <Header as="h2" className="deal-card-header">
                    {asset.name}
                    {asset?.artist?.name && (
                      <Header.Subheader content={asset.artist.name} />
                    )}
                  </Header>
                </Table.Cell>
                {asset.logo && (
                  <Table.Cell rowSpan="2" textAlign="center">
                    <Image centered fluid src={asset.logo} />
                  </Table.Cell>
                )}
              </Table.Row>
            </Table.Body>
          </Table>
        </Card.Content>
        <Card.Content extra>
          <List horizontal size="small" relaxed>
            <List.Item>
              <List.Content>
                <List.Header>Appraised Value</List.Header>
                <NumberFormat
                  value={asset?.appraisedValue}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"$"}
                />
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Content>
                <List.Header>Fractional Ownership Amount</List.Header>
                <NumberFormat
                  value={asset?.fractionalOwnershipAmount}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"$"}
                />
              </List.Content>
            </List.Item>
          </List>
        </Card.Content>
        <Card.Content extra>
          <Card.Meta>Advisors</Card.Meta>
          <Card.Description>{asset.advisors}</Card.Description>
        </Card.Content>
        <Card.Content textAlign="center">
          {/* onClick={() => setFirstOpen(true)} */}
          <Modal
            onClose={() => setFirstOpen(false)}
            onOpen={() => setFirstOpen(true)}
            open={firstOpen}
            size="tiny"
            dimmer={"blurring"}
            closeIcon
            trigger={<Button secondary fluid content=" Publish to GLX" />}
          >
            <Modal.Content>
              <Grid>
                <Grid.Column textAlign="center">
                  <Button icon="search" positive />
                </Grid.Column>
              </Grid>
              <Divider hidden />

              <Modal.Description>
                <Header as="h3" textAlign="center">
                  Is your asset ready to publish to the private exchange?
                </Header>
                <Header as="h4" textAlign="center">
                  The best companies have a video, pitch deck, team, and
                  financial information filled out.{" "}
                </Header>
                <Header as="h5" textAlign="center" disabled>
                  This helps investors perform proper due dillegence. They can
                  also contact you to request more information.
                </Header>
              </Modal.Description>
              <Divider hidden />
              <Button secondary fluid onClick={() => setSecondOpen(true)}>
                Publish
              </Button>
            </Modal.Content>
            <Modal
              onClose={() => setSecondOpen(false)}
              open={secondOpen}
              size="tiny"
              dimmer={"blurring"}
              closeIcon
            >
              <Modal.Content>
                <Divider hidden />
                <Grid>
                  <Grid.Column textAlign="center">
                    <Icon
                      name="file alternate outline"
                      className={"primaryColor"}
                      size="huge"
                    />
                  </Grid.Column>
                </Grid>

                <Modal.Description>
                  <Header as="h3" textAlign="center">
                    Your asset page is live!
                  </Header>
                  <Header as="h4" textAlign="center">
                    Deal organizers now will able to review your asset and apply
                    to be the lead investor.
                  </Header>
                  <Header as="h4" textAlign="center" disabled>
                    You can unpublish your deal anytime.
                  </Header>
                </Modal.Description>
                <Divider hidden />
                <Button
                  secondary
                  fluid
                  content="View My Artwork"
                  as={Link}
                  to={`/assets/${assetId}`}
                />
              </Modal.Content>
            </Modal>
          </Modal>
          <br />
          <Button
            secondary
            fluid
            as={Link}
            to={`/assets/inviteorganizer/${asset.id}`}
            className="company-button company-colr"
          >
            <Icon name="user plus" />
            Send to an Organizer
          </Button>
        </Card.Content>
      </Card>
    </Transition>
  );
};

export default ArtworkCard;
