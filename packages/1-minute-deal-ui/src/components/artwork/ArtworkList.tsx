import React, { useState, useEffect } from "react";

import {
  Container,
  Card,
  Divider,
  Loader,
  Segment,
  Visibility,
} from "semantic-ui-react";
import { Asset } from "../../api/assetsApi";
import ArtworkListActions from "./ArtworkListActions";
import NoArtwork from "./NoArtwork";
import ArtworkCard from "./ArtworkCard";

interface Props {
  assets: Asset[];
}

export const ArtworkList = ({ assets }: Props) => {
  const [tempCompanies, settempCompanies] = useState(assets);

  useEffect(() => {
    settempCompanies(assets);
  }, [assets]);

  const handleonBottomPassed = (eventname) => {
    const len = tempCompanies.length;
    if (len <= assets.length) {
      const data = assets.slice(0, len + 2);
      settempCompanies(data);
    }
  };

  return (
    <>
      {assets && assets.length > 0 ? (
        <>
          <ArtworkListActions />
          <Divider hidden />
          <Container>
            <Card.Group itemsPerRow="two" centered stackable>
              {tempCompanies.map((asset) => (
                <ArtworkCard key={asset.id} assetId={asset.id} />
              ))}
            </Card.Group>
          </Container>
        </>
      ) : (
        <NoArtwork />
      )}
      <div style={{ height: 30 }} />
    </>
  );
};
