import React, { Dispatch, SetStateAction } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Document as DocumentPreview, Page } from "react-pdf";
import { RootState } from "../rootReducer";
import axios from "axios";
import asset from "../assets/images/loadingDocsImage.png";
import { fetchFileById } from "../slices/filesSlice";
import { toast } from "react-toastify";
import { fileIcons } from "../tools/fileIcons";
import * as parseCookie from "../api/parseCookie";
import { SubformFileUpload } from "../forms/subforms/SubformFileUpload";
import ImageFile from "../components/ImageFile";
import {
  Card,
  Image,
  Icon,
  Loader,
  Header,
  Button,
  Divider,
  Segment,
} from "semantic-ui-react";
import moment from "moment";
import { DownloadFile } from "../tools/downloadUtils";

interface Props {
  fileArray: Array<string>;
  setFileArray: Dispatch<SetStateAction<Array<string>>>;
}

export default function GalleryUpload({ fileArray, setFileArray }: Props) {
  const dispatch = useDispatch();

  // const file = useSelector((state: RootState) => state.files.filesById[fileId]);
  //const { data: file } = useSWR(
  //  () => (fileId ? `/files/${fileId}` : null),
  //  () => getFileById(fileId)
  //);

  //const downloadDocument = async (fileId: string) => {
  //  const response = await axios({
  //    url: process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
  //    method: "GET",
  //    responseType: "blob",
  //    headers: {
  //      Authorization: `Bearer ${token}`,
  //    },
  //  });
  //  if (response.status !== 200) {
  //    toast.error("Download is unavailable at this time for this document");
  //    return "";
  //  }
  //  return window.URL.createObjectURL(new Blob([response.data]));
  //};

  //useEffect(() => {
  //  const downloadFile = async () => {
  //    setFileData(await downloadDocument(fileId));
  //  };
  //  if (file && !file.generating) {
  //    downloadFile();
  //  }
  //}, [fileId, file]);

  //const onPage = (type) => {
  //  var newPage = type ? currentPage + 1 : currentPage - 1;
  //  if (newPage > numPages) {
  //    newPage = numPages;
  //  } else if (newPage < 1) {
  //    newPage = 1;
  //  }
  //  setCurrentPage(newPage);
  //};

  return (
    <>
      {!fileArray || fileArray.length === 0 ? (
        <>
          <Segment placeholder>
            <Header icon>
              <Icon name="images outline" color="grey" />
            </Header>
            <SubformFileUpload
              allowMultiple
              acceptMime="image/*"
              onSuccess={async (fileIds) => {
                setFileArray([...fileArray, ...fileIds]);
                //toast.success(`File uploaded to deal`);
              }}
            >
              {({ triggerFileUpload, uploading }) => (
                <Button
                  primary
                  loading={uploading}
                  onClick={triggerFileUpload}
                  content="Upload Images"
                />
              )}
            </SubformFileUpload>
          </Segment>
        </>
      ) : (
        <>
          <Segment placeholder>
            <Image.Group size="small">
              {fileArray.map((fileId) => (
                <ImageFile key={fileId} fileId={fileId} />
              ))}
            </Image.Group>
          </Segment>
        </>
      )}
    </>
  );
}
