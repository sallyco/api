import React, { useState, useEffect } from "react";
import { TableWithTools } from "@packages/gbt-ui";
import { User } from "../api/usersApi";

// Data specific...
import {
  getContactListByOwnerId,
  ContactListItem,
  ContactList,
} from "../api/contactListApi";

import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
const CONTACT_LIST_FLAG = "contact_list_feature";

function useContactsList(self: User) {
  const contactListEnabled = useFeatureFlag(CONTACT_LIST_FLAG);
  const initialContacts: ContactListItem[] = [];
  const [contacts, setContacts] = useState(initialContacts);
  const initialContactsList: ContactList = null;
  const [contactList, setContactList] = useState(initialContactsList);
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedContacts, setSelectedContacts] = useState([]);

  useEffect(() => {
    // Only fetch once self has been obtained
    if (contactListEnabled) {
      if (self?.id) {
        getContactListByOwnerId(self.id).then((contactList) => {
          if (contactList) {
            setContactList(contactList);
            setContacts(contactList.contacts);
          }
        });
      }
    }
  }, [self, contactListEnabled]);

  useEffect(() => {
    // set the actual values from the contacts list
    // use the dataIndex from TableWithTools to get
    // the real data
    const newData = selectedRows.map((selected) => {
      return contacts[selected.dataIndex];
    });
    setSelectedContacts(newData);
  }, [selectedRows]);

  return {
    contacts,
    contactList,
    selectedContacts,
    setSelectedRows,
    setContactList,
  };
}

function generateEmailFieldText(
  emailField: string,
  contacts: ContactListItem[]
) {
  const contactEmails = getEmailListFromContacts(contacts);
  const enteredEmails = parseEmailField(emailField);
  return combineEmails(enteredEmails, contactEmails);
}

function getEmailListFromContacts(contacts: ContactListItem[]) {
  const emails = contacts.map((contact) => {
    return contact.email;
  });

  return emails;
}

function parseEmailField(stringField: string) {
  const emails = stringField.split(/[\s,]+/).filter((email) => {
    return email !== "";
  });

  return emails;
}

function combineEmails(enteredEmails: string[], selectedEmails: string[]) {
  const allEmails = enteredEmails.concat(selectedEmails);
  const set = new Set(allEmails);
  return Array.from(set).join(",\r\n");
}

function ContactListTable({ contacts, setSelectedRows }) {
  // This is just a number, the passed in value is an object
  const [rowsSelected, setRowsSelected] = useState([]);

  return (
    <TableWithTools
      data={contacts}
      rowsSelected={rowsSelected}
      onRowSelectionChange={(
        currentRowsSelected: any[],
        allRowsSelected: any[],
        rowsSelected: any[]
      ) => {
        setRowsSelected(rowsSelected);
        setSelectedRows(allRowsSelected);
      }}
      columnDefinitions={[
        {
          name: "name",
        },
        {
          name: "email",
        },
        {
          name: "phone",
        },
      ]}
      customToolbarSelect={() => {}}
    />
  );
}

export default ContactListTable;
export { useContactsList, generateEmailFieldText, CONTACT_LIST_FLAG };
