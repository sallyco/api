import React from "react";
import { Container } from "semantic-ui-react";

export default function TermsOfUse() {
  return (
    <Container text>
      <div style={{ height: "80%", marginBottom: "20%" }}>
        <em>Last Modified: July, 31 2020</em>
        <p>
          &zwnj;<u> </u>
        </p>
        <p>
          Welcome to Glassboard. By using our products and services available at{" "}
          <a
            className="register-sidebar-links"
            href="https://glassboardtech.com"
            style={{ textDecoration: "underline", fontWeight: "bold" }}
          >
            glassboardtech.com
          </a>{" "}
          and any of our other websites (&ldquo;Services&rdquo;) you are
          agreeing to these terms. It is in your best interest to read them
          carefully, as they are binding upon you.
        </p>
        <p>
          <strong>Acceptance of the Terms of Use</strong>
        </p>
        <p>
          These terms of use are entered into by and between You and Glassboard
          Technology, LLC. and its affiliates (&ldquo;
          <strong>Glassboard</strong>
          &rdquo;, &ldquo;<strong>we</strong>&rdquo;, or &ldquo;
          <strong>us</strong>&rdquo;). The following terms and conditions (these
          &ldquo;<strong>Terms of Use</strong>&rdquo;),
          gove&shy;&shy;&shy;&shy;&shy;rn your access to and use of{" "}
          <a
            className="register-sidebar-links"
            href="https://glassboardtech.com"
            style={{ textDecoration: "underline", fontWeight: "bold" }}
          >
            glassboardtech.com
          </a>{" "}
          (the &ldquo;<strong>Website</strong>&rdquo;), including any content,
          functionality, and Services offered on or through the Site.
        </p>
        <p>
          Please read the Terms of Use carefully before you start to use the
          Website.{" "}
          <strong>
            By using the Website, you accept and agree to be bound and abide by
            these Terms of Use and our Privacy Policy.
          </strong>
          (the &ldquo;<strong>Privacy Policy</strong>&rdquo;)<strong>. </strong>
          If you do not want to agree to these Terms of Use or the Privacy
          Policy, you must not access or use the Website.
        </p>
        <p>
          <strong>Types of Users</strong>
        </p>
        <p>
          All who access our Website, use our Services, and/or agree to the
          Terms are considered &ldquo;<strong>Users</strong>&rdquo;. Type of
          Users include:
        </p>
        <p>
          &ldquo;<em>Investors</em>&rdquo; include individuals or entities that
          have established an investor account on the Website. Investors are
          able make new investments, view past investments, access investment
          documents, tax statements, and utilize other Services the Website may
          offer to Users that have investor accounts.
        </p>
        <p>
          &ldquo;<em>Organizers</em>&rdquo; include individuals or entities that
          have established an organizer account on the Website. Organizers are
          able to setup new special purpose vehicles (&ldquo;
          <strong>SPV</strong>&rdquo;), grant access to an SPV, view past
          SPV&rsquo;s, manage past investments, review deal documents, send
          messages to investors, send invites to view an SPV, and utilize other
          Services the Website may offer to Users that have Organizer accounts.
        </p>
        <p>
          &ldquo;<em>Visitors</em>&rdquo; include individuals or entities that
          visit the Website for any reason.
        </p>
        <p>
          <strong>Changes to the Terms of Use</strong>
        </p>
        <p>
          We may amend and update these Terms of Use from time to time in our
          sole discretion. All changes are effective immediately when we post
          them and apply to all access to and use of the Website thereafter.
          Your continued use of the Website and Services following the posting
          of revised Terms of Use means that you accept and agree to the
          changes. You are expected to check this page frequently, so you are
          aware of any changes, as they are binding on you.
        </p>
        <p>
          <strong>Accessing the Website and Account Security</strong>
        </p>
        <p>
          We reserve the right to withdraw or amend this Website, and any
          Service or material we provide on the Website, in our sole discretion
          without notice. We will not be liable if for any reason all or any
          part of the Website is unavailable at any time or for any period. From
          time to time, we may restrict access to some parts of the Website, or
          the entire Website, to Users, including registered Users.
        </p>
        <p>You are responsible for:</p>
        <ul>
          <li>
            Making all arrangements necessary for you to have access to the
            Website.
          </li>
          <li>
            Ensuring that all persons who access the Website through your
            internet connection are aware of these Terms of Use and comply with
            them.
          </li>
        </ul>
        <p>
          To access the Website or some of the resources it offers, you may be
          asked to provide certain registration details or other information. It
          is a condition of your use of the Website that all the information you
          provide on the Website is correct, current, and complete. You agree
          that all information you provide to register with this Website or
          otherwise, including but not limited to through the use of any
          interactive features on the Website, is governed by the Privacy
          Policy, and you consent to all actions we take with respect to your
          information consistent with our Privacy Policy.
        </p>
        <p>
          If you choose, or are provided with, a user name, password, or any
          other piece of information as part of our security procedures, you
          must treat this information as confidential, and you must not disclose
          it to any other person or entity. You also acknowledge that your
          account is personal to you and agree not to provide any other person
          with access to this Website or portions of it using your username,
          password, or other security information. You agree to notify us
          immediately of any unauthorized access to or use of your user name or
          password or any other breach of security. You also agree to ensure
          that you exit from your account at the end of each session. You should
          use caution when accessing your account from a public or shared
          computer so that others are not able to view or record your password
          or other personal information. We have the right to disable any user
          name, password, or other identifier, whether chosen by you or provided
          by us, at any time in our sole discretion for any or no reason,
          including if, in our opinion, you have violated any provision of these
          Terms of Use.
        </p>
        <p>
          <strong>Intellectual Property Rights</strong>
        </p>
        <p>
          The Website and its entire contents, features, and functionality
          (including but not limited to all information, software, text,
          displays, images, video, and audio, and the design, selection, and
          arrangement thereof) are owned by Glassboard, its licensors, or other
          providers of such material and are protected by United States and
          international copyright, trademark, patent, trade secret, and other
          intellectual property or proprietary rights laws.
        </p>
        <p>
          These Terms of Use permit you to use the Website for your personal,
          non-commercial use only. You must not reproduce, distribute, modify,
          create derivative works of, publicly display, publicly perform,
          republish, download, store, or transmit any of the material on our
          Website, except as follows:
        </p>
        <ul>
          <li>
            Your computer may temporarily store copies of such materials in RAM
            incidental to your accessing and viewing those materials.
          </li>
          <li>
            You may store files that are automatically cached by your Web
            browser for display enhancement purposes.
          </li>
        </ul>
        <p>You must not:</p>
        <ul>
          <li>Modify copies of any materials from this Website.</li>
          <li>
            Delete or alter any copyright, trademark, or other proprietary
            rights notices from copies of materials from this Website.
          </li>
        </ul>
        <p>
          You must not access or use for any commercial purposes any part of the
          Website or any Services or materials available through the Website.
        </p>
        <p>
          If you print, copy, modify, download, or otherwise use or provide any
          other person with access to any part of the Website in breach of the
          Terms of Use, your right to use the Website will stop immediately and
          you must, at our option, return or destroy any copies of the materials
          you have made. No right, title, or interest in or to the Website or
          any content on the Website is transferred to you, and all rights not
          expressly granted are reserved by Glassboard. Any use of the Website
          not expressly permitted by these Terms of Use is a breach of these
          Terms of Use and may violate copyright, trademark, and other laws.
        </p>
        <p>
          <strong>Prohibited Uses</strong>
        </p>
        <p>
          You may use the Website only for lawful purposes and in accordance
          with these Terms of Use. You agree not to use the Website:
        </p>
        <ul>
          <li>
            In any way that violates any applicable federal, state, local, or
            international law or regulation (including, without limitation, any
            laws regarding the export of data or software to and from the US or
            other countries).
          </li>
          <li>
            For the purpose of exploiting, harming, or attempting to exploit or
            harm minors in any way by exposing them to inappropriate content,
            asking for personally identifiable information, or otherwise.
          </li>
          <li>
            To transmit, or procure the sending of, any advertising or
            promotional material without our prior written consent, including
            any &ldquo;junk mail&rdquo;, &ldquo;chain letter&rdquo;,
            &ldquo;spam&rdquo;, or any other similar solicitation.
          </li>
          <li>
            To impersonate or attempt to impersonate Glassboard, a Company
            employee, another user, or any other person or entity (including,
            without limitation, by using email addresses or screen names
            associated with any of the foregoing).
          </li>
          <li>
            To engage in any other conduct that restricts or inhibits
            anyone&rsquo;s use or enjoyment of the Website, or which, as
            determined by us, may harm Glassboard or Users of the Website or
            expose them to liability.
          </li>
        </ul>
        <p>Additionally, you agree not to:</p>
        <ul>
          <li>
            Use the Website in any manner that could disable, overburden,
            damage, or impair the Website or interfere with any other
            party&rsquo;s use of the Website, including their ability to engage
            in real time activities through the Website.
          </li>
          <li>
            Use any robot, spider, or other automatic device, process, or means
            to access the Website for any purpose, including monitoring or
            copying any of the material on the Website.
          </li>
          <li>
            Use the Services or the Website in a way that violates the
            securities laws and regulations of the United States, or in the
            jurisdiction in which you are operating, or that violates any
            judgements, agreements, or obligations to which you are bound, or
            that knowingly causes other Users of the Website to do so.
          </li>
          <li>
            Use the Website is any way that might require Glassboard to register
            as a broker dealer or investment adviser with the SEC.
          </li>
          <li>
            Expect Glassboard to evaluate or recommend investment opportunities
            found on the Website.
          </li>
          <li>
            Use any manual process to monitor or copy any of the material on the
            Website or for any other unauthorized purpose without our prior
            written consent.
          </li>
          <li>
            Use any device, software, or routine that interferes with the proper
            working of the Website.
          </li>
          <li>
            Introduce any viruses, Trojan horses, worms, logic bombs, or other
            material that is malicious or technologically harmful.
          </li>
          <li>
            Attempt to gain unauthorized access to, interfere with, damage, or
            disrupt any parts of the Website, the server on which the Website is
            stored, or any server, computer, or database connected to the
            Website.
          </li>
          <li>
            Attack the Website via a denial-of-service attack or a distributed
            denial-of-service attack.
          </li>
          <li>
            Otherwise attempt to interfere with the proper working of the
            Website.
          </li>
        </ul>
        <p>&nbsp;</p>
        <p>
          <strong>User Submissions</strong>
        </p>
        <p>
          The Website may contain message boards, and other interactive features
          (collectively, &ldquo;<strong>Interactive Services</strong>&rdquo;)
          that allow Users to post, submit, publish, display, or transmit to
          other Users or other persons (hereinafter, &ldquo;
          <strong>post</strong>&rdquo;) content or materials (collectively,
          &ldquo;<strong>User Submissions</strong>&rdquo;) on or through the
          Website.
        </p>
        <p>
          All User Submissions must comply with the Content Standards set out in
          these Terms of Use.
        </p>
        <p>
          Some User Submissions (such as data you contribute to Glassboard) are
          viewable by other Users. In order to display your User Submissions on
          the Website, you grant us certain rights in those User
          Submissions.&nbsp;Please note that all of the following licenses are
          subject to our Privacy Policy&nbsp;to the extent they relate to User
          Submissions that are also your personally-identifiable information.
        </p>
        <p>
          For all User Submissions, you hereby grant Glassboard a royalty-free,
          perpetual, sublicenseable, irrevocable, and worldwide license to
          translate, modify (including, without limitation, for technical
          purposes), reproduce, and otherwise act with respect to such User
          Submissions, in each case to enable us to operate the Website and
          provide Services, as described in more detail below.&nbsp;This is a
          license only &ndash; your ownership in User Submissions is not
          affected.
        </p>
        <p>
          If you share a User Submission publicly on the Website and/or in a
          manner that more than just you or specified specific set of Users can
          view, or if you provide us with any feedback, suggestions,
          improvements, enhancements, and/or feature requests relating to the
          Service (each of the foregoing, a &ldquo;Public User
          Submission&rdquo;), then you grant Glassboard the licenses above, as
          well as a royalty-free, perpetual, sublicenseable, irrevocable, and
          worldwide license to display, perform, and distribute your Public User
          Submission for the purpose of making that Public User Submission
          accessible to all Users and providing the Service necessary to do so,
          as well as all other rights necessary to use and exercise all rights
          in that Public User Submission in connection with the Service and/or
          otherwise in connection with Glassboard&rsquo;s business.
        </p>
        <p>
          You understand and acknowledge that you are responsible for any User
          Submissions you submit or contribute, and you, not Glassboard, have
          full responsibility for such content, including its legality,
          reliability, accuracy, and appropriateness.
        </p>
        <p>
          We are not responsible or liable to any third party for the content or
          accuracy of any User Submissions posted by you or any other user of
          the Website.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Monitoring and Enforcement; Termination</strong>
        </p>
        <p>We have the right to:</p>
        <ul>
          <li>
            Remove or refuse to post any User Submissions for any or no reason
            in our sole discretion.
          </li>
          <li>
            Take any action with respect to any User Submission that we deem
            necessary or appropriate in our sole discretion, including if we
            believe that such User Submission violates the Terms of Use,
            including the Content Standards, infringes any intellectual property
            right or other right of any person or entity, threatens the personal
            safety of Users of the Website or the public, or could create
            liability for Glassboard.
          </li>
          <li>
            Disclose your identity or other information about you to any third
            party who claims that material posted by you violates their rights,
            including their intellectual property rights or their right to
            privacy.
          </li>
          <li>
            Take appropriate legal action, including without limitation,
            referral to law enforcement, for any illegal or unauthorized use of
            the Website.
          </li>
          <li>
            Terminate or suspend your access to all or part of the Website for
            any or no reason, including without limitation, any violation of
            these Terms of Use.
          </li>
        </ul>
        <p>
          Without limiting the foregoing, we have the right to cooperate fully
          with any law enforcement authorities or court order requesting or
          directing us to disclose the identity or other information of anyone
          posting any materials on or through the Website. YOU WAIVE AND HOLD
          HARMLESS ASSURE AND ITS AFFILIATES FROM ANY CLAIMS RESULTING FROM ANY
          ACTION TAKEN BY FOREGOING PARTIES DURING, OR TAKEN AS A CONSEQUENCE
          OF, INVESTIGATIONS BY EITHER PARTIES OR LAW ENFORCEMENT AUTHORITIES.
        </p>
        <p>
          However, we cannot review all material before it is posted on the
          Website, and cannot ensure prompt removal of objectionable material
          after it has been posted. Accordingly, we assume no liability for any
          action or inaction regarding transmissions, communications, or content
          provided by any user or third party. We have no liability or
          responsibility to anyone for performance or nonperformance of the
          activities described in this section.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Content Standards</strong>
        </p>
        <p>
          These content standards apply to any and all User Submissions and use
          of Interactive Services. User Submissions must in their entirety
          comply with all applicable federal, state, local, and international
          laws and regulations. Without limiting the foregoing, User Submissions
          must not:
        </p>
        <ul>
          <li>
            Contain any material that is defamatory, obscene, indecent, abusive,
            offensive, harassing, violent, hateful, inflammatory, or otherwise
            objectionable.
          </li>
          <li>
            Promote sexually explicit or pornographic material, violence, or
            discrimination based on race, sex, religion, nationality,
            disability, sexual orientation, or age.
          </li>
          <li>
            Infringe any patent, trademark, trade secret, copyright, or other
            intellectual property or other rights of any other person.
          </li>
          <li>
            Violate the legal rights (including the rights of publicity and
            privacy) of others or contain any material that could give rise to
            any civil or criminal liability under applicable laws or regulations
            or that otherwise may be in conflict with these Terms of Use and our
            Privacy Policy.
          </li>
          <li>Be likely to deceive any person.</li>
          <li>
            Promote any illegal activity, or advocate, promote, or assist any
            unlawful act.
          </li>
          <li>
            Cause annoyance, inconvenience, or needless anxiety or be likely to
            upset, embarrass, alarm, or annoy any other person.
          </li>
          <li>
            Impersonate any person, or misrepresent your identity or affiliation
            with any person or organization.
          </li>
          <li>
            Involve commercial activities or sales, such as contests,
            sweepstakes, and other sales promotions, barter, or advertising.
          </li>
          <li>
            Give the impression that they emanate from or are endorsed by us or
            any other person or entity, if this is not the case.
          </li>
        </ul>
        <p>
          <strong>Securities Regulations</strong>
        </p>
        <p>
          None of the Services provided by the Website should be construed as
          investment advice or brokering investments. By using the Website, you
          acknowledge that we do not recommend any investments that you are
          invited to view or participate in, or investments submitted by an
          Organizer. There are significant regulatory requirements when making
          an investment and when forming an investment entity or SPV. We make no
          representations or warranties to Users that all regulatory
          requirements have been met. Glassboard may be involved in setting up
          an SPV and performing regulatory filings, and in those cases, the
          service agreement and legal documents created for that specific SPV
          will govern the relationship between those parties. By using the
          Website and its Services you fully understand that Glassboard does not
          endorse the validity the underlying investment an SPV may make.
          Additionally, you understand that federal securities law requires that
          securities sold in the United States must be registered with the
          Securities and Exchange Commission, unless the sale qualifies for an
          exemption. Most startups raise capital by complying with a private
          placements exemption. Additionally, most SPV&rsquo;s rely on
          exemptions from having to register as an &ldquo;investment
          company&rdquo; under the Investment Company Act of 1940. Glassboard is
          not a law firm and does not provide legal advice. You are responsible
          for ensuring that your offering, or an offering you participate in,
          complies with all relevant federal and state securities law and
          regulations.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Reliance on Information Posted</strong>
        </p>
        <p>
          The information presented on or through the Website is made available
          solely for general information purposes. We do not warrant the
          accuracy, completeness, or usefulness of this information. Any
          reliance you place on such information is strictly at your own risk.
          We disclaim all liability and responsibility arising from any reliance
          placed on such materials by you or any other visitor to the Website,
          or by anyone who may be informed of any of its contents.
        </p>
        <p>
          This Website may include content provided by third parties, including
          materials provided by other Users, and third-party licensors,
          syndicators, aggregators, and/or reporting services. All statements
          and opinions expressed in these materials, and all articles and
          responses to questions and other content, other than the content
          provided by Glassboard, are solely the opinions and the responsibility
          of the person or entity providing those materials. These materials do
          not necessarily reflect the opinion of Glassboard. We are not
          responsible, or liable to you or any third party, for the content or
          accuracy of any materials provided by any third parties.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Changes to the Website</strong>
        </p>
        <p>
          We may update the content on this Website from time to time, but its
          content is not necessarily complete or up-to-date. Any of the material
          on the Website may be out of date at any given time, and we are under
          no obligation to update such material.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Information About You and Your Visits to the Website</strong>
        </p>
        <p>
          All information we collect on this Website is subject to our Privacy
          Policy. By using the Website, you consent to all actions taken by us
          with respect to your information in compliance with the Privacy
          Policy. &zwnj;<u> </u>
        </p>
        <p>
          <strong>Links from the Website</strong>
        </p>
        <p>
          If the Website contains links to other sites and resources provided by
          third parties, these links are provided for your convenience only.
          This includes links contained in advertisements, including banner
          advertisements and sponsored links. We have no control over the
          contents of those sites or resources, and accept no responsibility for
          them or for any loss or damage that may arise from your use of them.
          If you decide to access any of the third-party websites linked to this
          Website, you do so entirely at your own risk and subject to the terms
          and conditions of use for such websites.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Geographic Restrictions</strong>
        </p>
        <p>
          The owner of the Website is based in the state of Utah in the United
          States. We provide this Website for use only by persons located in the
          United States. We make no claims that the Website or any of its
          content is accessible or appropriate outside of the United States.
          Access to the Website may not be legal by certain persons or in
          certain countries. If you access the Website from outside the United
          States, you do so on your own initiative and are responsible for
          compliance with local laws.
        </p>
        <p>
          <strong>Disclaimer of Warranties</strong>
        </p>
        <p>
          You understand that we cannot and do not guarantee or warrant that
          files available for downloading from the internet or the Website will
          be free of viruses or other destructive code. You are responsible for
          implementing sufficient procedures and checkpoints to satisfy your
          particular requirements for anti-virus protection and accuracy of data
          input and output, and for maintaining a means external to our Website
          for any reconstruction of any lost data. TO THE FULLEST EXTENT
          PROVIDED BY LAW, WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED
          BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES, OR OTHER
          TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER
          EQUIPMENT, COMPUTER PROGRAMS, DATA, OR OTHER PROPRIETARY MATERIAL DUE
          TO YOUR USE OF THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH
          THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON
          ANY WEBSITE LINKED TO IT. YOUR USE OF THE WEBSITE, ITS CONTENT, AND
          ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN
          RISK. THE WEBSITE, ITS CONTENT, AND ANY SERVICES OR ITEMS OBTAINED
          THROUGH THE WEBSITE ARE PROVIDED ON AN &ldquo;AS IS&rdquo; AND
          &ldquo;AS AVAILABLE&rdquo; BASIS, WITHOUT ANY WARRANTIES OF ANY KIND,
          EITHER EXPRESS OR IMPLIED. NEITHER ASSURE NOR ANY PERSON ASSOCIATED
          WITH ASSURE MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE
          COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY, OR
          AVAILABILITY OF THE WEBSITE. WITHOUT LIMITING THE FOREGOING, NEITHER
          ASSURE NOR ANYONE ASSOCIATED WITH ASSURE REPRESENTS OR WARRANTS THAT
          THE WEBSITE, ITS CONTENT, OR ANY SERVICES OR ITEMS OBTAINED THROUGH
          THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE, OR UNINTERRUPTED,
          THAT DEFECTS WILL BE CORRECTED, THAT OUR WEBSITE OR THE SERVER THAT
          MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS, OR
          THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE
          WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.
        </p>
        <p>
          TO THE FULLEST EXTENT PROVIDED BY LAW, ASSURE HEREBY DISCLAIMS ALL
          WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY, OR
          OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
          MERCHANTABILITY, NON-INFRINGEMENT, AND FITNESS FOR PARTICULAR PURPOSE.
        </p>
        <p>
          THE FOREGOING DOES NOT AFFECT ANY WARRANTIES THAT CANNOT BE EXCLUDED
          OR LIMITED UNDER APPLICABLE LAW.
        </p>
        <p>
          <strong>Limitation on Liability</strong>
        </p>
        <p>
          TO THE FULLEST EXTENT PROVIDED BY LAW, IN NO EVENT WILL ASSURE, ITS
          AFFILIATES, OR THEIR LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS,
          OFFICERS, OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY
          LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR
          INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT
          ON THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT,
          SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES, INCLUDING BUT
          NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL
          DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR
          ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND
          WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT, OR
          OTHERWISE, EVEN IF FORESEEABLE.
        </p>
        <p>
          THE FOREGOING DOES NOT AFFECT ANY LIABILITY THAT CANNOT BE EXCLUDED OR
          LIMITED UNDER APPLICABLE LAW.
        </p>
        <p>
          <strong>Indemnification</strong>
        </p>
        <p>
          You agree to defend, indemnify, and hold harmless Glassboard, its
          affiliates, licensors, and service providers, and its and their
          respective officers, directors, employees, contractors, agents,
          licensors, suppliers, successors, and assigns from and against any
          claims, liabilities, damages, judgments, awards, losses, costs,
          expenses, or fees (including reasonable attorneys&rsquo; fees) arising
          out of or relating to your violation of these Terms of Use or your use
          of the Website, including, but not limited to, your User Submissions,
          any use of the Website&rsquo;s content, Services, and products other
          than as expressly authorized in these Terms of Use or your use of any
          information obtained from the Website.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Dispute Resolution</strong>
        </p>
        <p>
          The Terms shall be governed by Utah law and subject to the exclusive
          jurisdiction of the state and federal courts located in the City and
          County of Salt Lake City, Utah, without regard to the choice or
          conflicts of law provisions of any jurisdiction. All claims arising
          from use of the Website will be exclusively resolved by binding
          arbitration. YOU UNDERSTAND THAT BY REQUIRING ARBITRATION, NEITHER
          PARTY WILL HAVE THE RIGHT TO SUE IN COURE OR HAVE A JURY TRIAL.
        </p>
        <p>
          Any dispute, controversy or claim arising between You and Glassboard
          will be settled by binding arbitration, before three arbitrators,
          administered by the American Arbitration Association under and in
          accordance with its Commercial Arbitration Rules, and judgment on the
          award rendered by the arbitrators may be entered in any court having
          jurisdiction. Any arbitration will be held in Salt Lake City, Utah.
          Each of the Parties will equally bear any arbitration fees and
          administrative costs associated with the arbitration. The prevailing
          Party, as determined by the arbitrators, will be awarded its costs and
          reasonable attorneys&rsquo; fees incurred in connection with the
          arbitration.
        </p>
        <p>
          <strong>Limitation on Time to File Claims</strong>
        </p>
        <p>
          ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING
          TO THESE TERMS OF USE OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1)
          YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF
          ACTION OR CLAIM IS PERMANENTLY BARRED.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Waiver and Severability</strong>
        </p>
        <p>
          No waiver by Glassboard of any term or condition set out in these
          Terms of Use shall be deemed a further or continuing waiver of such
          term or condition or a waiver of any other term or condition, and any
          failure of Glassboard to assert a right or provision under these Terms
          of Use shall not constitute a waiver of such right or provision.
        </p>
        <p>
          If any provision of these Terms of Use is held by a court or other
          tribunal of competent jurisdiction to be invalid, illegal, or
          unenforceable for any reason, such provision shall be eliminated or
          limited to the minimum extent such that the remaining provisions of
          the Terms of Use will continue in full force and effect.
        </p>
        <p>&nbsp;</p>
        <p>
          <strong>Entire Agreement</strong>
        </p>
        <p>
          The Terms of Use are the entire agreement between you and Glassboard
          with respect to the Service and use of the Website and supersede all
          prior or contemporaneous communications and proposals (whether oral,
          written or electronic) between you and Glassboard with respect to the
          Website. &zwnj;<u> </u>
        </p>
      </div>
    </Container>
  );
}
