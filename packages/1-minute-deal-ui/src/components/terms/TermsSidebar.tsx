/* eslint-disable react/display-name */
import React, { useEffect, useContext } from "react";
import { Header, Tab, Segment, Button, Menu } from "semantic-ui-react";
import { TenantContext } from "../../contexts";

//import ServiceAgreement from "./ServiceAgreement";
import TermsOfUse from "./TermsOfUse";
import PrivacyPolicy from "./PrivacyPolicy";
import TermsOfUseVaria from "./TermsOfUseVaria";
import PrivacyPolicyVaria from "./PrivacyPolicyVaria";

interface Props {
  setShowTerms: (bool) => void;
  setTermsChecked: (bool) => void;
}

export function TermsSidebar({ setShowTerms, setTermsChecked }: Props) {
  const tenantProfile = useContext(TenantContext);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const panes = [
    // {
    //   menuItem: "Service Agreement",
    //   style: { color: "blue" },
    //   render: () => (
    //     <Tab.Pane id="hihi" attached={false}>
    //       <ServiceAgreement />
    //     </Tab.Pane>
    //   ),
    // },
    {
      menuItem: "Terms of Use",
      render: () => (
        <Tab.Pane inverted attached={false}>
          {tenantProfile.id === "varia" ? <TermsOfUseVaria /> : <TermsOfUse />}
        </Tab.Pane>
      ),
    },
    {
      menuItem: "Privacy Policy",
      render: () => (
        <Tab.Pane inverted attached={false}>
          {tenantProfile.id === "varia" ? (
            <PrivacyPolicyVaria />
          ) : (
            <PrivacyPolicy />
          )}
        </Tab.Pane>
      ),
    },
  ];
  return (
    <>
      <Tab
        menu={{
          secondary: true,
          pointing: true,
          inverted: true,
          className: "nav-bar",
        }}
        panes={panes}
      />
      <Menu
        secondary={true}
        fixed="bottom"
        style={{ marginBottom: "24px" }}
        fluid
      >
        <Segment basic textAlign="center" style={{ margin: "auto" }}>
          <Header as="h4">I agree to all of the above.</Header>
          <Button
            secondary
            onClick={() => {
              setTermsChecked(true);
              setShowTerms(false);
            }}
            content="Agree"
          />
        </Segment>
      </Menu>
    </>
  );
}
