import React from "react";
import { Container } from "semantic-ui-react";

export default function ServiceAgreement() {
  return (
    <Container className="mobile-container">
      <div style={{ height: "80%" }}>
        <div>
          <p className="c77">
            <span className="c52">Software-as-a-Service (SaaS) Agreement </span>
          </p>
          <p className="c26">
            <span className="c3">
              This Software as a Service Agreement (this &#34;{" "}
            </span>
            <span className="c0">Agreement </span>
            <span className="c3">
              &#34;), effective as of [ Date ] (the &#34;{" "}
            </span>
            <span className="c0">Effective Date </span>
            <span className="c3">
              &#34;), is by and between Assure Technology, LLC, a Utah limited
              liability company with offices located at 6510 S. Millrock Dr.
              Suite 400, Salt Lake City, Utah 84121 (&#34;{" "}
            </span>
            <span className="c0">Provider </span>
            <span className="c3">
              &#34;) and [ Customer Name ], a [ State ] [ entity type ] with
              offices located at [ Address ] (&#34;{" "}
            </span>
            <span className="c0">Customer </span>
            <span className="c3">
              &#34;). Provider and Customer may be referred to herein
              collectively as the &#34;{" "}
            </span>
            <span className="c0">Parties </span>
            <span className="c3">&#34; or individually as a &#34; </span>
            <span className="c0">Party </span>
            <span className="c3">.&#34; </span>
          </p>
          <p className="c43">
            <span className="c3">The Parties agree as follows: </span>
          </p>
          <p className="c8">
            <span className="c5">1. </span>
            <span className="c3">Access and Use . </span>
          </p>
          <p className="c20 c112">
            <span className="c1">(a) </span>
            <span className="c3">
              Provision of Access . Subject to and conditioned on Customer&#39;s
              payment of Fees and compliance with all other terms and conditions
              of this Agreement, Provider hereby grants Customer a
              non-exclusive, non-transferable (except in compliance with Section
              12 (g) ) right to access and use the Services during the Term,
              solely for use by Authorized Users in accordance with the terms
              and conditions herein. Such use is limited to Customer&#39;s
              internal use. Provider shall provide to Customer the necessary
              passwords and network links or connections to allow Customer to
              access the Services. The total number of Authorized Users will not
              exceed the number set forth in{" "}
            </span>
            <span className="c0">Exhibit A </span>
            <span className="c3">
              , except as expressly agreed to in writing by the Parties and
              subject to any appropriate adjustment of the Fees payable
              hereunder.{" "}
            </span>
          </p>
          <p className="c20 c23">
            <span className="c1">(b) </span>
            <span className="c3">
              Documentation License . Subject to the terms and conditions
              contained in this Agreement, Provider hereby grants to Customer a
              non-exclusive, non-sublicenseable, non-transferable (except in
              compliance with Section 12 (g) ) license to use the Documentation
              during the Term solely for Customer&#39;s internal business
              purposes in connection with its use of the Services.{" "}
            </span>
          </p>
          <p className="c17">
            <span className="c1">(c) </span>
            <span className="c3">
              Use Restrictions . Customer shall not use the Services for any
              purposes beyond the scope of the access granted in this Agreement.
              Customer shall not at any time, directly or indirectly, and shall
              not permit any Authorized Users to: (i) copy, modify, or create
              derivative works of the Services or Documentation, in whole or in
              part; (ii) rent, lease, lend, sell, license, sublicense, assign,
              distribute, publish, transfer, or otherwise make available the
              Services or Documentation; (iii) reverse engineer, disassemble,
              decompile, decode, adapt, or otherwise attempt to derive or gain
              access to any software component of the Services, in whole or in
              part; (iv) remove any proprietary notices from the Services or
              Documentation; or (v) use the Services or Documentation in any
              manner or for any purpose that infringes, misappropriates, or
              otherwise violates any intellectual property right or other right
              of any person, or that violates any applicable law.{" "}
            </span>
          </p>
          <p className="c20 c86">
            <span className="c1">(d) </span>
            <span className="c3">
              Reservation of Rights . Provider reserves all rights not expressly
              granted to Customer in this Agreement. Except for the limited
              rights and licenses expressly granted under this Agreement,
              nothing in this Agreement grants, by implication, waiver,
              estoppel,{" "}
            </span>
          </p>
          <p className="c32">
            <span className="c3">
              or otherwise, to Customer or any third party any intellectual
              property rights or other right, title, or interest in or to the
              Provider IP.{" "}
            </span>
          </p>
          <p className="c31">
            <span className="c1">(e) </span>
            <span className="c3">
              Suspension . Notwithstanding anything to the contrary in this
              Agreement, Provider may temporarily suspend Customer&#39;s and any
              Authorized End User&#39;s access to any portion or all of the
              Services if: (i) Provider reasonably determines that (A) there is
              a threat or attack on any of the Provider IP; (B) Customer&#39;s
              or any Authorized End User&#39;s use of the Provider IP disrupts
              or poses a security risk to the Provider IP or to any other
              customer or vendor of Provider; (C) Customer, or any Authorized
              End User, is using the Provider IP for fraudulent or illegal
              activities; (D) subject to applicable law, Customer has ceased to
              continue its business in the ordinary course, made an assignment
              for the benefit of creditors or similar disposition of its assets,
              or become the subject of any bankruptcy, reorganization,
              liquidation, dissolution, or similar proceeding; or (E)
              Provider&#39;s provision of the Services to Customer or any
              Authorized End User is prohibited by applicable law; (ii) any
              vendor of Provider has suspended or terminated Provider&#39;s
              access to or use of any third-party services or products required
              to enable Customer to access the Services; or (iii) in accordance
              with Section 5 (a) (iii) (any such suspension described in
              subclause (i), (ii), or (iii), a “{" "}
            </span>
            <span className="c0">Service Suspension </span>
            <span className="c3">
              ”). Provider shall use commercially reasonable efforts to provide
              written notice of any Service Suspension to Customer and to
              provide updates regarding resumption of access to the Services
              following any Service Suspension. Provider shall use commercially
              reasonable efforts to resume providing access to the Services as
              soon as reasonably possible after the event giving rise to the
              Service Suspension is cured. Provider will have no liability for
              any damage, liabilities, losses (including any loss of data or
              profits), or any other consequences that Customer or any
              Authorized User may incur as a result of a Service Suspension.{" "}
            </span>
          </p>
          <p className="c20 c68">
            <span className="c1">(f) </span>
            <span className="c3">
              Aggregated Statistics . Notwithstanding anything to the contrary
              in this Agreement, Provider may monitor Customer&#39;s use of the
              Services and collect and compile Aggregated Statistics. As between
              Provider and Customer, all right, title, and interest in
              Aggregated Statistics, and all intellectual property rights
              therein, belong to and are retained solely by Provider. Customer
              acknowledges that Provider may compile Aggregated Statistics based
              on Customer Data input into the Services. Customer agrees that
              Provider may (i) make Aggregated Statistics publicly available in
              compliance with applicable law, and (ii) market, sell and use
              Aggregated Statistics to the extent and in the manner permitted
              under applicable law.{" "}
            </span>
          </p>
          <p className="c53">
            <span className="c5">2. </span>
            <span className="c3">
              Customer Responsibilities . Customer is responsible and liable for
              all uses of the Services and Documentation resulting from access
              provided by Customer, directly or indirectly, whether such access
              or use is permitted by or in violation of this Agreement. Without
              limiting the generality of the foregoing, Customer is responsible
              for all acts and omissions of Authorized Users, and any act or
              omission by an Authorized User that would constitute a breach of
              this Agreement if taken by Customer will be deemed a breach of
              this Agreement by Customer. Customer shall use reasonable efforts
              to make all Authorized Users aware of this Agreement&#39;s{" "}
            </span>
          </p>
          <p className="c24">
            <span className="c5">2 </span>
          </p>
          <p className="c34">
            <span className="c3">
              provisions as applicable to such Authorized User&#39;s use of the
              Services, and shall cause Authorized Users to comply with such
              provisions.{" "}
            </span>
          </p>
          <p className="c11">
            <span className="c5">3. </span>
            <span className="c3">Service Levels and Support . </span>
          </p>
          <p className="c20 c96">
            <span className="c1">(a) </span>
            <span className="c3">
              Service Levels . Subject to the terms and conditions of this
              Agreement, Provider shall use commercially reasonable efforts to
              make the Services available in accordance with the service levels
              set out in{" "}
            </span>
            <span className="c0">Exhibit B </span>
            <span className="c3">. </span>
          </p>
          <p className="c20 c59">
            <span className="c1">(b) </span>
            <span className="c3">
              Support . The access rights granted hereunder entitles Customer to
              the support services described on{" "}
            </span>
            <span className="c0">Exhibit B </span>
            <span className="c3">
              following the Effective Date under this Agreement..{" "}
            </span>
          </p>
          <p className="c20 c73">
            <span className="c5">4. </span>
            <span className="c3">Fees and Payment . </span>
          </p>
          <p className="c27">
            <span className="c1">(a) </span>
            <span className="c3">
              Fees . Customer shall pay Provider the fees (&#34;{" "}
            </span>
            <span className="c0">Fees </span>
            <span className="c3">&#34;) as set forth in </span>
            <span className="c0">Exhibit A </span>
            <span className="c3">
              without offset or deduction. Customer shall make all payments
              hereunder in US dollars on or before the due date set forth in{" "}
            </span>
            <span className="c0">Exhibit A </span>
            <span className="c3">
              . If Customer fails to make any payment when due, without limiting
              Provider&#39;s other rights and remedies: (i) Provider may charge
              interest on the past due amount at the rate of 1.5% per month
              calculated daily and compounded monthly or, if lower, the highest
              rate permitted under applicable law; (ii) Customer shall reimburse
              Provider for all costs incurred by Provider in collecting any late
              payments or interest, including attorneys&#39; fees, court costs,
              and collection agency fees; and (iii) if such failure continues
              for 15 days or more, Provider may suspend Customer&#39;s and its
              Authorized Users&#39; access to any portion or all of the Services
              until such amounts are paid in full.{" "}
            </span>
          </p>
          <p className="c55">
            <span className="c1">(b) </span>
            <span className="c3">
              Taxes . All Fees and other amounts payable by Customer under this
              Agreement are exclusive of taxes and similar assessments. Customer
              is responsible for all sales, use, and excise taxes, and any other
              similar taxes, duties, and charges of any kind imposed by any
              federal, state, or local governmental or regulatory authority on
              any amounts payable by Customer hereunder, other than any taxes
              imposed on Provider&#39;s income.{" "}
            </span>
            <span className="c12">(c) </span>
            <span className="c33">
              Auditing Rights and Required Records . Customer agrees to maintain{" "}
            </span>
            <span className="c3">
              complete and accurate records in accordance with generally
              accepted accounting principles during the Term and for a period of
              two years after the termination or expiration of this Agreement
              with respect to matters necessary for accurately determining
              amounts due hereunder. Provider may, at its own expense, on
              reasonable prior notice, periodically inspect and audit
              Customer&#39;s records with respect to matters covered by this
              Agreement, provided that if such inspection and audit reveals that
              Customer has underpaid Provider with respect to any amounts due
              and payable during the Term, Customer shall promptly pay the
              amounts necessary to rectify such underpayment, together with
              interest in accordance with Section 5 (a) . Customer shall pay for
              the costs of the audit if the audit determines that Customer&#39;s
              underpayment equals or exceeds 5% for{" "}
            </span>
          </p>
          <p className="c19">
            <span className="c5">3 </span>
          </p>
          <p className="c56">
            <span className="c3">
              any quarter. Such inspection and auditing rights will extend
              throughout the Term of this Agreement and for a period of two
              years after the termination or expiration of this Agreement.{" "}
            </span>
          </p>
          <p className="c60">
            <span className="c5">5. </span>
            <span className="c3">
              Professional Services . During the term of this Agreement,
              Customer may request Provider to perform professional services
              listed in Exhibit A2, and/or integration services (hereinafter,
              “Professional Services”). Upon receipt of a request, Provider may
              provide Customer with a written proposal, and when the parties
              agree to all requirements of the proposed Professional Services, a
              Task Order for the Professional Services, in the form of{" "}
            </span>
            <span className="c0">Exhibit C </span>
            <span className="c3">
              , shall be executed by the parties. All Task Orders shall be
              subject to the terms and conditions of this Agreement. Services
              performed by Provider are not exclusive to Customer, and Providor
              may perform services of any type or nature for any other person or
              entity at any time.{" "}
            </span>
          </p>
          <p className="c29">
            <span className="c5">6. </span>
            <span className="c3">
              Confidential Information . From time to time during the Term,
              either Party may disclose or make available to the other Party
              information about its business affairs, products, confidential
              intellectual property, trade secrets, third-party confidential
              information, and other sensitive or proprietary information,
              whether orally or in written, electronic, or other form or media,
              and whether or not marked, designated or otherwise identified as
              &#34;confidential&#34; (collectively, &#34;{" "}
            </span>
            <span className="c0">Confidential Information </span>
            <span className="c3">
              &#34;). Confidential Information does not include information
              that, at the time of disclosure is: (a) in the public domain; (b)
              known to the receiving Party at the time of disclosure; (c)
              rightfully obtained by the receiving Party on a non-confidential
              basis from a third party; or (d) independently developed by the
              receiving Party. The receiving Party shall not disclose the
              disclosing Party&#39;s Confidential Information to any person or
              entity, except to the receiving Party&#39;s employees who have a
              need to know the Confidential Information for the receiving Party
              to exercise its rights or perform its obligations hereunder.
              Notwithstanding the foregoing, each Party may disclose
              Confidential Information to the limited extent required (i) in
              order to comply with the order of a court or other governmental
              body, or as otherwise necessary to comply with applicable law,
              provided that the Party making the disclosure pursuant to the
              order shall first have given written notice to the other Party and
              made a reasonable effort to obtain a protective order; or (ii) to
              establish a Party&#39;s rights under this Agreement, including to
              make required court filings. On the expiration or termination of
              the Agreement, the receiving Party shall promptly return to the
              disclosing Party all copies, whether in written, electronic, or
              other form or media, of the disclosing Party&#39;s Confidential
              Information, or destroy all such copies and certify in writing to
              the disclosing Party that such Confidential Information has been
              destroyed. Each Party&#39;s obligations of non-disclosure with
              regard to Confidential Information are effective as of the
              Effective Date and will expire five years from the date first
              disclosed to the receiving Party; provided, however, with respect
              to any Confidential Information that constitutes a trade secret
              (as determined under applicable law), such obligations of
              non-disclosure will survive the termination or expiration of this
              Agreement for as long as such Confidential Information remains
              subject to trade secret protection under applicable law.{" "}
            </span>
          </p>
          <p className="c20 c75">
            <span className="c5">7. </span>
            <span className="c3">
              Intellectual Property Ownership; Feedback .{" "}
            </span>
          </p>
          <p className="c28">
            <span className="c5">4 </span>
          </p>
          <p className="c18">
            <span className="c1">(a) </span>
            <span className="c3">
              Provider IP . Customer acknowledges that, as between Customer and
              Provider, Provider owns all right, title, and interest, including
              all intellectual property rights, in and to the Provider IP.{" "}
            </span>
          </p>
          <p className="c20 c67">
            <span className="c1">(b) </span>
            <span className="c3">
              Customer Data . Provider acknowledges that, as between Provider
              and Customer, Customer owns all right, title, and interest,
              including all intellectual property rights, in and to the Customer
              Data. Customer hereby grants to Provider a non-exclusive,
              royalty-free, worldwide license to reproduce, distribute, and
              otherwise use and display the Customer Data and perform all acts
              with respect to the Customer Data as may be necessary for Provider
              to provide the Services to Customer, and a non-exclusive,
              perpetual, irrevocable, royalty-free, worldwide license to
              reproduce, distribute, modify, and otherwise use and display
              Customer Data incorporated within the Aggregated Statistics.{" "}
            </span>
          </p>
          <p className="c20 c48">
            <span className="c1">(c) </span>
            <span className="c3">
              Feedback . If Customer or any of its employees or contractors
              sends or transmits any communications or materials to Provider by
              mail, email, telephone, or otherwise, suggesting or recommending
              changes to the Provider IP, including without limitation, new
              features or functionality relating thereto, or any comments,
              questions, suggestions, or the like (&#34;{" "}
            </span>
            <span className="c0">Feedback </span>
            <span className="c3">
              &#34;), Provider is free to use such Feedback irrespective of any
              other obligation or limitation between the Parties governing such
              Feedback. Customer hereby assigns to Provider on Customer&#39;s
              behalf, and on behalf of its employees, contractors and/or agents,
              all right, title, and interest in, and Provider is free to use,
              without any attribution or compensation to any party, any ideas,
              know-how, concepts, techniques, or other intellectual property
              rights contained in the Feedback, for any purpose whatsoever,
              although Provider is not required to use any Feedback.{" "}
            </span>
          </p>
          <p className="c68 c99">
            <span className="c5">8. </span>
            <span className="c3">
              Warranty Disclaimer . THE PROVIDER IP IS PROVIDED &#34;AS IS&#34;
              AND PROVIDER HEREBY DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS,
              IMPLIED, STATUTORY, OR OTHERWISE. PROVIDER SPECIFICALLY DISCLAIMS
              ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
              PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT, AND ALL
              WARRANTIES ARISING FROM COURSE OF DEALING, USAGE, OR TRADE
              PRACTICE. PROVIDER MAKES NO WARRANTY OF ANY KIND THAT THE PROVIDER
              IP, OR ANY PRODUCTS OR RESULTS OF THE USE THEREOF, WILL MEET
              CUSTOMER&#39;S OR ANY OTHER PERSON&#39;S REQUIREMENTS, OPERATE
              WITHOUT INTERRUPTION, ACHIEVE ANY INTENDED RESULT, BE COMPATIBLE
              OR WORK WITH ANY SOFTWARE, SYSTEM OR OTHER SERVICES, OR BE SECURE,
              ACCURATE, COMPLETE, FREE OF HARMFUL CODE, OR ERROR FREE.{" "}
            </span>
          </p>
          <p className="c44">
            <span className="c5">9. </span>
            <span className="c3">Indemnification . </span>
          </p>
          <p className="c14">
            <span className="c1">(a) </span>
            <span className="c3">Provider Indemnification . </span>
          </p>
          <p className="c9">
            <span className="c5">(i) </span>
            <span className="c3">
              Provider shall indemnify, defend, and hold harmless Customer from
              and against any and all losses, damages, liabilities, costs
              (including reasonable attorneys&#39; fees) (&#34;{" "}
            </span>
            <span className="c0">Losses </span>
            <span className="c3">
              &#34;) incurred by Customer resulting from any{" "}
            </span>
          </p>
          <p className="c95">
            <span className="c5">5 </span>
          </p>
          <p className="c68 c87">
            <span className="c3">
              third-party claim, suit, action, or proceeding (&#34;{" "}
            </span>
            <span className="c0">Third-Party Claim </span>
            <span className="c3">
              &#34;) that the Services, or any use of the Services in accordance
              with this Agreement, infringes or misappropriates such third
              party&#39;s US intellectual property rights, provided that
              Customer promptly notifies Provider in writing of the claim,
              cooperates with Provider, and allows Provider sole authority to
              control the defense and settlement of such claim.{" "}
            </span>
          </p>
          <p className="c94">
            <span className="c5">(ii) </span>
            <span className="c3">
              If such a claim is made or appears possible, Customer agrees to
              permit Provider, at Provider&#39;s sole discretion, to (A) modify
              or replace the Services, or component or part thereof, to make it
              non-infringing, or (B) obtain the right for Customer to continue
              use. If Provider determines that neither alternative is reasonably
              available, Provider may terminate this Agreement, in its entirety
              or with respect to the affected component or part, effective
              immediately on written notice to Customer.{" "}
            </span>
          </p>
          <p className="c35">
            <span className="c5">(iii) </span>
            <span className="c3">
              This Section 9 (a) will not apply to the extent that the alleged
              infringement arises from: (A) use of the Services in combination
              with data, software, hardware, equipment, or technology not
              provided by Provider or authorized by Provider in writing; (B)
              modifications to the Services not made by Provider; or (C)
              Customer Data.{" "}
            </span>
          </p>
          <p className="c50">
            <span className="c1">(b) </span>
            <span className="c3">
              Customer Indemnification . Customer shall indemnify, hold
              harmless, and, at Provider&#39;s option, defend Provider from and
              against any Losses resulting from any Third-Party Claim that the
              Customer Data, or any use of the Customer Data in accordance with
              this Agreement, infringes or misappropriates such third
              party&#39;s intellectual property rights and any Third-Party
              Claims based on Customer&#39;s or any Authorized User&#39;s (i)
              negligence or willful misconduct; (ii) use of the Services in a
              manner not authorized by this Agreement; (iii) use of the Services
              in combination with data, software, hardware, equipment or
              technology not provided by Provider or authorized by Provider in
              writing; or (iv) modifications to the Services not made by
              Provider, provided that Customer may not settle any Third-Party
              Claim against Provider unless Provider consents to such
              settlement, and further provided that Provider will have the
              right, at its option, to defend itself against any such
              Third-Party Claim or to participate in the defense thereof by
              counsel of its own choice.{" "}
            </span>
          </p>
          <div className="c13">
            <span className="c1">(c) </span>
            <span className="c3">
              Sole Remedy . THIS SECTION 9 SETS FORTH CUSTOMER&#39;S SOLE
              REMEDIES AND PROVIDER&#39;S SOLE LIABILITY AND OBLIGATION FOR ANY
              ACTUAL, THREATENED, OR ALLEGED CLAIMS THAT THE SERVICES INFRINGE,
              MISAPPROPRIATE, OR OTHERWISE VIOLATE ANY INTELLECTUAL PROPERTY
              RIGHTS OF ANY THIRD PARTY. IN NO EVENT WILL PROVIDER&#39;S
              LIABILITY UNDER THIS SECTION 9 EXCEED $10,000.{" "}
            </span>
            <p className="c91">
              <span className="c5">10. </span>
              <span className="c3">
                Limitations of Liability . IN NO EVENT WILL PROVIDER BE LIABLE
                UNDER OR IN CONNECTION WITH THIS AGREEMENT UNDER ANY LEGAL OR
                EQUITABLE THEORY, INCLUDING BREACH OF CONTRACT, TORT (INCLUDING{" "}
              </span>
            </p>
            <p className="c122">
              <span className="c5">6 </span>
            </p>
            <p className="c68 c106">
              <span className="c3">
                NEGLIGENCE), STRICT LIABILITY, AND OTHERWISE, FOR ANY: (a)
                CONSEQUENTIAL, INCIDENTAL, INDIRECT, EXEMPLARY, SPECIAL,
                ENHANCED, OR PUNITIVE DAMAGES; (b) INCREASED COSTS, DIMINUTION
                IN VALUE OR LOST BUSINESS, PRODUCTION, REVENUES, OR PROFITS; (c)
                LOSS OF GOODWILL OR REPUTATION; (d) USE, INABILITY TO USE, LOSS,
                INTERRUPTION, DELAY OR RECOVERY OF ANY DATA, OR BREACH OF DATA
                OR SYSTEM SECURITY; OR (e) COST OF REPLACEMENT GOODS OR
                SERVICES, IN EACH CASE REGARDLESS OF WHETHER PROVIDER WAS
                ADVISED OF THE POSSIBILITY OF SUCH LOSSES OR DAMAGES OR SUCH
                LOSSES OR DAMAGES WERE OTHERWISE FORESEEABLE. IN NO EVENT WILL
                PROVIDER&#39;S AGGREGATE LIABILITY ARISING OUT OF OR RELATED TO
                THIS AGREEMENT UNDER ANY LEGAL OR EQUITABLE THEORY, INCLUDING
                BREACH OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT
                LIABILITY, AND OTHERWISE EXCEED TWO TIMES THE TOTAL AMOUNTS PAID
                TO PROVIDER UNDER THIS AGREEMENT IN THE THREE MONTH PERIOD
                PRECEDING THE EVENT GIVING RISE TO THE CLAIM OR $10,000,
                WHICHEVER IS LESS.{" "}
              </span>
            </p>
            <p className="c63">
              <span className="c5">11. </span>
              <span className="c3">Term and Termination . </span>
            </p>
            <p className="c2">
              <span className="c1">(a) </span>
              <span className="c3">
                Term . The initial term of this Agreement begins on the
                Effective Date and, unless terminated earlier pursuant to this
                Agreement&#39;s express provisions, will continue in effect
                until one year from such date (the &#34;{" "}
              </span>
              <span className="c0">Initial Term </span>
              <span className="c3">
                &#34;). This Agreement will automatically renew for up to five
                additional successive one year terms unless earlier terminated
                pursuant to this Agreement&#39;s express provisions or either
                Party gives the other Party written notice of non-renewal at
                least 30 days prior to the expiration of the then-current term
                (each a &#34;{" "}
              </span>
              <span className="c0">Renewal Term </span>
              <span className="c3">
                &#34; and together with the Initial Term, the &#34;{" "}
              </span>
              <span className="c0">Term </span>
              <span className="c3">&#34;). </span>
              <span className="c12">(b) </span>
              <span className="c33">
                Termination . In addition to any other express termination right
                set forth in{" "}
              </span>
              <span className="c3">this Agreement: </span>
              <span className="c70">(i) </span>
              <span className="c33">
                Provider may terminate this Agreement, effective on written
                notice{" "}
              </span>
            </p>
            <p className="c117">
              <span className="c3">
                to Customer, if Customer: (A) fails to pay any amount when due
                hereunder, and such failure continues more than 30 days after
                Provider&#39;s delivery of written notice thereof; or (B)
                breaches any of its obligations under Section 2 (c) or Section 6
                ;{" "}
              </span>
            </p>
            <p className="c120">
              <span className="c5">(ii) </span>
              <span className="c3">
                either Party may terminate this Agreement, effective on written
                notice to the other Party, if the other Party materially
                breaches this Agreement, and such breach: (A) is incapable of
                cure; or (B) being capable of cure, remains uncured 30 days
                after the non-breaching Party provides the breaching Party with
                written notice of such breach; or{" "}
              </span>
            </p>
            <p className="c45">
              <span className="c5">(iii) </span>
              <span className="c3">
                either Party may terminate this Agreement, effective immediately
                upon written notice to the other Party, if the other Party: (A)
                becomes insolvent or is generally unable to pay, or fails to
                pay, its debts as they become due; (B) files{" "}
              </span>
            </p>
            <p className="c95">
              <span className="c5">7 </span>
            </p>
            <p className="c87 c88">
              <span className="c3">
                or has filed against it, a petition for voluntary or involuntary
                bankruptcy or otherwise becomes subject, voluntarily or
                involuntarily, to any proceeding under any domestic or foreign
                bankruptcy or insolvency law; (C) makes or seeks to make a
                general assignment for the benefit of its creditors; or (D)
                applies for or has appointed a receiver, trustee, custodian, or
                similar agent appointed by order of any court of competent
                jurisdiction to take charge of or sell any material portion of
                its property or business.{" "}
              </span>
            </p>
            <p className="c38">
              <span className="c1">(c) </span>
              <span className="c3">
                Effect of Expiration or Termination . Upon expiration or earlier
                termination of this Agreement, Customer shall immediately
                discontinue use of the Provider IP and, without limiting
                Customer&#39;s obligations under Section 6 , Customer shall
                delete, destroy, or return all copies of the Provider IP and
                certify in writing to the Provider that the Provider IP has been
                deleted or destroyed. No expiration or termination will affect
                Customer&#39;s obligation to pay all Fees that may have become
                due before such expiration or termination, or entitle Customer
                to any refund.{" "}
              </span>
            </p>
            <p className="c20 c82">
              <span className="c3">
                (d) Survival . This Section 11(d) and Sections 1, 5, 6, 7, 8, 9,
                10, and 12 survive any termination or expiration of this
                Agreement. No other provisions of this Agreement survive the
                expiration or earlier termination of this Agreement.{" "}
              </span>
            </p>
            <p className="c20 c85">
              <span className="c5">12. </span>
              <span className="c3">Miscellaneous . </span>
            </p>
            <p className="c92 c111">
              <span className="c1">(a) </span>
              <span className="c3">
                Entire Agreement . This Agreement, together with any other
                documents incorporated herein by reference and all related
                Exhibits, constitutes the sole and entire agreement of the
                Parties with respect to the subject matter of this Agreement and
                supersedes all prior and contemporaneous understandings,
                agreements, and representations and warranties, both written and
                oral, with respect to such subject matter. In the event of any
                inconsistency between the statements made in the body of this
                Agreement, the related Exhibits, and any other documents
                incorporated herein by reference, the following order of
                precedence governs: (i) first, this Agreement, excluding its
                Exhibits; (ii) second, the Exhibits to this Agreement as of the
                Effective Date; and (iii) third, any other documents
                incorporated herein by reference.{" "}
              </span>
            </p>
            <p className="c20 c39">
              <span className="c1">(b) </span>
              <span className="c3">
                Notices . All notices, requests, consents, claims, demands,
                waivers, and other communications hereunder (each, a &#34;{" "}
              </span>
              <span className="c0">Notice </span>
              <span className="c3">
                &#34;) must be in writing and addressed to the Parties at the
                addresses set forth on the first page of this Agreement (or to
                such other address that may be designated by the Party giving
                Notice from time to time in accordance with this Section). All
                Notices must be delivered by personal delivery, nationally
                recognized overnight courier (with all fees pre-paid), facsimile
                or email (with confirmation of transmission) or certified or
                registered mail (in each case, return receipt requested, postage
                pre-paid). Except as otherwise provided in this Agreement, a
                Notice is effective only: (i) upon receipt by the receiving
                Party; and (ii) if the Party giving the Notice has complied with
                the requirements of this Section.{" "}
              </span>
            </p>
            <p className="c46">
              <span className="c5">8 </span>
            </p>
            <p className="c66">
              <span className="c1">(c) </span>
              <span className="c3">
                Force Majeure . In no event shall Provider be liable to
                Customer, or be deemed to have breached this Agreement, for any
                failure or delay in performing its obligations under this
                Agreement, if and to the extent such failure or delay is caused
                by any circumstances beyond Provider&#39;s reasonable control,
                including but not limited to acts of God, flood, fire,
                earthquake, explosion, war, terrorism, invasion, riot or other
                civil unrest, strikes, labor stoppages or slowdowns or other
                industrial disturbances, or passage of law or any action taken
                by a governmental or public authority, including imposing an
                embargo.{" "}
              </span>
            </p>
            <p className="c20 c92">
              <span className="c1">(d) </span>
              <span className="c3">
                Amendment and Modification; Waiver . No amendment to or
                modification of this Agreement is effective unless it is in
                writing and signed by an authorized representative of each
                Party. No waiver by any Party of any of the provisions hereof
                will be effective unless explicitly set forth in writing and
                signed by the Party so waiving. Except as otherwise set forth in
                this Agreement, (i) no failure to exercise, or delay in
                exercising, any rights, remedy, power, or privilege arising from
                this Agreement will operate or be construed as a waiver thereof
                and (ii) no single or partial exercise of any right, remedy,
                power, or privilege hereunder will preclude any other or further
                exercise thereof or the exercise of any other right, remedy,
                power, or privilege.{" "}
              </span>
            </p>
            <p className="c20">
              <span className="c1">(e) </span>
              <span className="c3">
                Severability . If any provision of this Agreement is invalid,
                illegal, or unenforceable in any jurisdiction, such invalidity,
                illegality, or unenforceability will not affect any other term
                or provision of this Agreement or invalidate or render
                unenforceable such term or provision in any other jurisdiction.
                Upon such determination that any term or other provision is
                invalid, illegal, or unenforceable, the Parties shall negotiate
                in good faith to modify this Agreement so as to effect their
                original intent as closely as possible in a mutually acceptable
                manner in order that the transactions contemplated hereby be
                consummated as originally contemplated to the greatest extent
                possible.{" "}
              </span>
            </p>
            <p className="c20 c23">
              <span className="c1">(f) </span>
              <span className="c3">
                Governing Law; Submission to Jurisdiction . This Agreement is
                governed by and construed in accordance with the internal laws
                of the State of Utah without giving effect to any choice or
                conflict of law provision or rule that would require or permit
                the application of the laws of any jurisdiction other than those
                of the State of Utah. Any legal suit, action, or proceeding
                arising out of or related to this Agreement or the licenses
                granted hereunder will be instituted exclusively in the federal
                courts of the United States or the courts of the State of Utah
                in each case located in the city of Salt Lake City and County of
                Salt Lake City, and each Party irrevocably submits to the
                exclusive jurisdiction of such courts in any such suit, action,
                or proceeding.{" "}
              </span>
            </p>
            <p className="c16">
              <span className="c1">(g) </span>
              <span className="c3">
                Assignment . Customer may not assign any of its rights or
                delegate any of its obligations hereunder, in each case whether
                voluntarily, involuntarily, by operation of law or otherwise,
                without the prior written consent of Provider, which consent
                shall not be unreasonably withheld, conditioned, or delayed. Any
                purported assignment or delegation in violation of this Section
                will be null and void. No assignment or delegation will relieve
                the assigning or delegating Party of any of its obligations
                hereunder. This{" "}
              </span>
            </p>
            <p className="c37">
              <span className="c5">9 </span>
            </p>
            <p className="c36">
              <span className="c3">
                Agreement is binding upon and inures to the benefit of the
                Parties and their respective permitted successors and assigns.{" "}
              </span>
            </p>
            <p className="c81">
              <span className="c1">(h) </span>
              <span className="c3">
                Export Regulation . The Services utilize software and technology
                that may be subject to US export control laws, including the US
                Export Administration Act and its associated regulations.
                Customer shall not, directly or indirectly, export, re-export,
                or release the Services or the underlying software or technology
                to, or make the Services or the underlying software or
                technology accessible from, any jurisdiction or country to which
                export, re-export, or release is prohibited by law, rule, or
                regulation. Customer shall comply with all applicable federal
                laws, regulations, and rules, and complete all required
                undertakings (including obtaining any necessary export license
                or other governmental approval), prior to exporting,
                re-exporting, releasing, or otherwise making the Services or the
                underlying software or technology available outside the US.{" "}
              </span>
            </p>
            <p className="c20 c86">
              <span className="c1">(i) </span>
              <span className="c3">
                Equitable Relief . Each Party acknowledges and agrees that a
                breach or threatened breach by such Party of any of its
                obligations under Section 6 or, in the case of Customer, Section
                2 (c) , would cause the other Party irreparable harm for which
                monetary damages would not be an adequate remedy and agrees
                that, in the event of such breach or threatened breach, the
                other Party will be entitled to equitable relief, including a
                restraining order, an injunction, specific performance and any
                other relief that may be available from any court, without any
                requirement to post a bond or other security, or to prove actual
                damages or that monetary damages are not an adequate remedy.
                Such remedies are not exclusive and are in addition to all other
                remedies that may be available at law, in equity or otherwise.{" "}
              </span>
            </p>
            <p className="c20 c72">
              <span className="c1">(j) </span>
              <span className="c3">
                Counterparts . This Agreement may be executed in counterparts,
                each of which is deemed an original, but all of which together
                are deemed to be one and the same agreement.{" "}
              </span>
            </p>
            <p className="c20 c51">
              <span className="c3">13. Definitions . </span>
            </p>
            <p className="c55">
              <span className="c1">(a) </span>
              <span className="c3">&#34; </span>
              <span className="c0">Aggregated Statistics </span>
              <span className="c3">
                &#34; means data and information related to Customer&#39;s use
                of the Services that is used by Provider in an aggregate and
                anonymized manner, including to compile statistical and
                performance information.{" "}
              </span>
            </p>
            <p className="c41">
              <span className="c1">(b) </span>
              <span className="c3">&#34; </span>
              <span className="c0">Authorized User </span>
              <span className="c3">
                &#34; means Customer&#39;s employees, consultants, contractors,
                and agents (i) who are authorized by Customer to access and use
                the Services under the rights granted to Customer pursuant to
                this Agreement and (ii) for whom access to the Services has been
                purchased hereunder.{" "}
              </span>
            </p>
            <p className="c15">
              <span className="c3">(c) &#34; </span>
              <span className="c0">Customer Data </span>
              <span className="c3">
                &#34; means, other than Aggregated Statistics, information,
                data, and other content, in any form or medium, that is
                submitted, posted, or otherwise transmitted by or on behalf of
                Customer or an Authorized User through the Services.{" "}
              </span>
            </p>
            <p className="c7">
              <span className="c5">10 </span>
            </p>
            <p className="c74">
              <span className="c3">(d) &#34; </span>
              <span className="c0">Documentation </span>
              <span className="c3">
                &#34; means Provider&#39;s user manuals, handbooks, and guides
                relating to the Services provided by Provider to Customer either
                electronically or in hard copy form.{" "}
              </span>
            </p>
            <p className="c20">
              <span className="c3">(e) &#34; </span>
              <span className="c0">Provider IP </span>
              <span className="c3">
                &#34; means the Services, the Documentation, and any and all
                intellectual property provided to Customer or any Authorized
                User in connection with the foregoing. For the avoidance of
                doubt, Provider IP includes Aggregated Statistics and any
                information, data, or other content derived from Provider&#39;s
                monitoring of Customer&#39;s access to or use of the Services,
                but does not include Customer Data.{" "}
              </span>
            </p>
            <p className="c10">
              <span className="c1">(f) </span>
              <span className="c3">&#34; </span>
              <span className="c0">Services </span>
              <span className="c3">
                &#34; means the software-as-a-service offering described in{" "}
              </span>
              <span className="c0">Exhibit A </span>
              <span className="c3">. </span>
            </p>
            <p className="c10 c40">
              <span className="c5"></span>
            </p>
            <p className="c76">
              <span className="c0">EXHIBIT A1 - SAAS F </span>
            </p>
            <p className="c89">
              <span className="c3">
                Capitalized terms used but not defined in this Exhibit A have
                the meaning given to those terms in the Agreement.{" "}
              </span>
            </p>
            <p className="c84">
              <span className="c5">A. </span>
              <span className="c3">TERM AND FEES </span>
            </p>
            <p className="c78">
              <span className="c4">Billing Start Date: September 1, 2019 </span>
            </p>
            <p className="c114">
              <span className="c4">Service Start Date: September 1, 2019 </span>
            </p>
            <p className="c105">
              <span className="c1">1. </span>
              <span className="c4">Term: </span>
              <span className="c1">
                The term of this Schedule will commence as of the Billing Start
                Date and will continue for one (1) year (“Initial Term”). After
                the Initial Term, the Agreement shall automatically renew unless
                a formal termination notice is sent via email to{" "}
              </span>
              <span className="c119">ap@assure.com </span>
              <span className="c1">
                at least 30 days before the Initial Term expires.{" "}
              </span>
            </p>
            <p className="c69 c116">
              <span className="c1">2. </span>
              <span className="c4">Annual Recurring Fees: </span>
              <span className="c1">
                $12,000 quarterly paid in advance. Regulatory, Blue Sky{" "}
              </span>
            </p>
            <p className="c103">
              <span className="c1">
                filings and other fees will be charged on an actual basis,
                monthly to an approved credit card on file. Fees may be higher
                is the Client is based in California. Service is limited to
                creating a maximum of 15 Special Purpose Vehicles (SPV) per
                year. Additional SPV’s can purchase for $3,000 each.{" "}
              </span>
            </p>
            <p className="c21">
              <span className="c1">3. </span>
              <span className="c4">
                Authorized Glassboard Enterprise Modules:{" "}
              </span>
            </p>
            <p className="c71">
              <span className="c1">
                All modules are self-service and no professional services are
                included in the SaaS offering. Customer agrees to process and
                pay directly for Blue Sky filings, Delaware Corp, Utah Foreign
                Entity and all post closing activities such as distributions,
                etc. as required to create and maintain the SPV. Any additional
                SaaS modules will have an increase to the annual recurring fees
                based on the current market rates of each module.{" "}
              </span>
            </p>
            <p className="c110">
              <span className="c1">
                [ X ] Glassboard SPV [ ] Glassboard Ventures [ ] Glassboard
                Insights [ ] Glassboard Community [ ] Glassboard Deal Tracker [
                ] Glassboard Documents [ ] Glassboard Exchange (GLX) [ ]
                Glassboard Cap Table Management [ ] Glassboard Post Closing
                Actions [ ] Glassboard Merger &#38; Acquisitions{" "}
              </span>
            </p>
            <p className="c69 c93">
              <span className="c1">4. </span>
              <span className="c4">Maximum Number of Authorized Users: </span>
              <span className="c1">
                1 Organizer, up to 100 view only users{" "}
              </span>
            </p>
            <p className="c58">
              <span className="c1">5. </span>
              <span className="c4">Access: </span>
              <span className="c1">
                Access is limited to Authorized Users.{" "}
              </span>
            </p>
            <p className="c23 c69">
              <span className="c1">6. </span>
              <span className="c4">
                SaaS Fees for Additional Users to be co-terminous with the Term
                of the Agreement:{" "}
              </span>
            </p>
            <p className="c107">
              <span className="c1">
                Each additional Admin User $149 per user, per month, Additional
                100 User Investor Pack $499 per month,{" "}
              </span>
            </p>
            <p className="c62">
              <span className="c4">
                EXHIBIT A2 - Value-add Professional Services Fees{" "}
              </span>
            </p>
            <p className="c30">
              <span className="c4">1. Value added Professional Services </span>
            </p>
            <p className="c47">
              <span className="c1">
                [ X ] SPV Regulatory/Blue Sky Filings [ ] Tax [ ] Post Closing
                Activities (PCA) [ ] Bookkeeping [ ] Venture Fund Administration
                [ ] Glassboard Documents [ ] Glassboard JumpStartup [ ] Due
                Diligence [ ] Legal Services [ ] Reg CE / Gaap Conversion [ ]
                High Frequency Distribution [ ] ERA{" "}
              </span>
            </p>
            <p className="c113">
              <span className="c4">2. Value added Professional Services </span>
            </p>
            <p className="c57">
              <span className="c1">
                Fees will be based on the individual Tasks Orders defined in
                Exhibit C for each service that is requested and delivered.{" "}
              </span>
            </p>
            <p className="c57">
              <span className="c0">EXHIBIT B </span>
              <span className="c90">
                <br></br>
              </span>
              <span className="c0">SERVICE LEVELS AND SUPPORT </span>
            </p>
            <p className="c22">
              <span className="c3">
                Provider will provide the Service to Customer, not including
                scheduled maintenance time. Scheduled maintenance time will not
                exceed five (5) hours a month, and will take place during
                Non-Peak Hours. “Non-Peak Hours” will be the hours between 10:00
                PM and 4:00 AM MT. The Service and Work Products will function
                and be available as provided in this Agreement with the uptime
                specified below (with the exception of any scheduled maintenance
                performed by Provider). “uptime” will mean, the time in which
                the core functions are available and working in the system.
                Provider will provide the Appropriate Response Time for the
                Service as set forth herein. “Appropriate Response Time” will
                mean within 4 business hours from receiving a completed Service
                ticket.{" "}
              </span>
            </p>
            <p className="c108">
              <span className="c3">
                Technical Contacts . During the Term and any renewal term of the
                Agreement, Provider will make available a technical point of
                contact for Customer technical support inquiries.{" "}
              </span>
            </p>
            <p className="c100">
              <span className="c3">
                Deductions from Monthly Fees as a result of Service Level
                Non-Performance . If Provider has failed to meet any of the
                Service Levels set forth herein, Customer will be entitled to
                receive deductions from its monthly fee as set forth below.
                Deductions are expressed as a percentage of Provider’s total
                monthly charges during the month in which the service deduction
                applies, with each section being applied separately against the
                total monthly charges and then the totals of each chart
                aggregated.{" "}
              </span>
            </p>
            <p className="c65">
              <span className="c3">
                Uptime . Provider will provide an Uptime of 99% or better, and
                Uptime as defined herein will include any significant third
                party product included in the Service. If Provider’s
                noncompliance is from 1% to 2%, Company will be credited five
                (5) percent (5%) of its monthly fee. If noncompliance is from 2%
                to 3%, the credit will be ten percent (10%) of the monthly fee.
                There will be an additional two percent (2%) credit of the
                monthly fee for each additional one percent (1%) of
                noncompliance beyond 3%.
              </span>
              <span className="c102">
                <br></br>
              </span>
            </p>
            <p className="c97">
              <span className="c0">EXHIBIT C </span>
            </p>
            <p className="c115">
              <span className="c0">FORM OF TASK ORDER </span>
            </p>
            <p className="c109">
              <span className="c4">Task Order #___ </span>
            </p>
            <p className="c61">
              <span className="c1">
                THIS IS TASK ORDER #___, BEING EXECUTED PURSUANT TO SAAS
                AGREEMENT BETWEEN ALLVEST INFORMATION SERVICES (“PROVIDER”) AND
                [insert name] (“CUSTOMER”) DATED AS OF [insert date] (THE
                “AGREEMENT”).{" "}
              </span>
            </p>
            <p className="c42">
              <span className="c4">Project Description: </span>
            </p>
            <p className="c118">
              <span className="c4">Deliverables: </span>
            </p>
            <p className="c25">
              <span className="c4">Estimated Hours </span>
            </p>
            <p className="c104">
              <span className="c4">Hourly Rate:_______ </span>
            </p>
            <p className="c79">
              <span className="c4">Total: </span>
            </p>
            <p className="c64">
              <span className="c121">ESTIMATE FOR PROJECT </span>
            </p>
            <p className="c83">
              <span className="c4">Notes: </span>
            </p>
            <p className="c98">
              <span className="c4">1. Above fees are billed in advance. </span>
            </p>
            <p className="c80">
              <span className="c4">
                2. All work is performed on a time and materials basis. This
                Task Order is a good faith estimate of the work required to
                complete the Task Order based on our past experience and basic
                analysis of the project.{" "}
              </span>
            </p>
            <p className="c54">
              <span className="c4">
                3. Intellectual Property Ownership Provision: The Intellectual
                Property Ownership Provisions of the Agreement are incorporated
                by reference herein.{" "}
              </span>
            </p>
            <p className="c61">
              <span className="c4">
                4. Acceptance: The Deliverables described in the Project Scope
                of this Task Order will be deemed accepted by Customer upon
                receipt by Provider of written notice by Customer that the
                Deliverables have been accepted.{" "}
              </span>
            </p>
            <p className="c40 c49">
              <span className="c1"></span>
            </p>
            <p className="c6">
              <span className="c5">17 </span>
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
}
