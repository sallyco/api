import React from "react";
import { Container } from "semantic-ui-react";

export default function PrivacyPolicy() {
  return (
    <Container text>
      <div style={{ height: "80%" }}>
        <div>
          <p>
            <strong>Effective Date of Privacy Policy</strong>
          </p>
          <p>July, 31 2020</p>
          <p>
            This privacy policy (&ldquo;Policy&rdquo;) applies to Glassboard
            Technology, LLC and its related and/or affiliated companies
            (&ldquo;Glassboard&rdquo;, &ldquo;we&rdquo; or &ldquo;us&rdquo;)
            professional services, platforms and applications (the
            &ldquo;Services&rdquo;),{" "}
            <a
              className="register-sidebar-links"
              href="https://glassboardtech.com"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              glassboardtech.com
            </a>{" "}
            and other Glassboard websites (the &ldquo;Websites&rdquo;).
            Glassboard has developed this Policy to inform you how we collect,
            use, share, or otherwise process any information that is unique to
            you and may be used to identify you, or your usage data (&ldquo;
            <strong>Personal Information</strong>&rdquo;). It also describes the
            choices available to you regarding our use of your Personal
            Information and how you can update this information.
          </p>
          <p>
            <strong>Why We Collect Information</strong>
          </p>
          <p>
            <em>To Provide Services</em>: We perform various services for you
            and for third-parties that provide us with your information,
            including through the Websites. In many cases we need your Personal
            Information to perform these services. For example, if you invest in
            a special purpose vehicle (&ldquo;SPV&rdquo;), we need your Personal
            Information to properly administer that entity and provide you with
            important tax information.
          </p>
          <p>
            <em>To Personalize your Experience</em>: We are always looking for
            ways to improve your experience with our Site. We may use your
            Personal Information and usage data to make your experience more
            personal and relevant. This may include sending you reminders and
            updates about the Services you have requested or may have interest
            in.
          </p>
          <p>
            <strong>Information We Collect</strong>
          </p>
          <p>
            <em>Personal Data</em>: name, username, date of birth, gender,
            marital status, title, social security number and/or tax
            identification number, home address, work address, billing address,
            email address, telephone numbers, professional history, password,
            picture, and copies of identification cards or other forms of
            identification.
          </p>
          <p>
            <em>Financial Data</em>: bank account information, money transfer
            instructions, funds transfer accounts, financial objectives,
            transaction history, salary, credit reports, investment documents,
            and financial statements.
          </p>
          <p>
            <strong>How We Collect Information</strong>
          </p>
          <p>
            We generally collect information by asking you to enter data into
            forms on our Websites, by observing your activity on our Websites
            and use of our services, from third-parties that have engaged us to
            provide services that require our collection of your Personal
            Information, or from documents uploaded directly by you or by a
            third-party from whom you have requested services, from third party
            information providers that provide background checks, verify your
            identity, creditworthiness, and your eligibility and suitability to
            participate in certain transactions or services.
          </p>
          <p>
            We may also rely on your consent as a legal basis for using your
            Personal Information where we have expressly sought it for a
            specific purpose. If we do rely on your consent to a use of your
            Personal Information, you have the right to change your mind at any
            time (but this will not affect any processing that has already taken
            place).
          </p>
          <p>
            <strong>Provide services and support</strong>: It is in our
            legitimate interest to market, sell and provide our services,
            respond to your service requests, including using the information to
            register you as a user and verify your identity or to contact you in
            order to discuss support, and the purchase of services. This also
            includes, for example, establishing your eligibility to invest on
            the Websites under applicable laws, third-party contractual
            obligations and internal policies, providing tax and other
            investment related reporting for investors, and as otherwise needed
            to manage our business.
          </p>
          <p>
            <strong>Cookies &amp; Tracking Technologies</strong>
          </p>
          <p>
            Glassboard uses cookies, web beacons and log files to automatically
            gather, analyze, and store technical information about
            Website&rsquo;s visitors. This information includes the
            visitor&rsquo;s IP address, browser type, ISP, referring page,
            operating system, date/time, and clickstream data (meaning data
            about the pages you visit on our Websites). This information is used
            to improve the Services and enhance the experience of the
            Website&rsquo;s visitors. You can control the use of cookies at the
            individual browser level, but if you choose to disable cookies, it
            will limit your use of certain features or functions on the Websites
            or the Services, affecting your Website experience.
          </p>
          <p>
            <strong>Sharing Your Information</strong>
          </p>
          <p>
            Glassboard may use third party sources to confirm or supplement the
            information that it obtains from you, including information about
            Individuals.
          </p>
          <p>
            We may share your Personal Information with third parties including
            these categories of recipients:
          </p>
          <ul>
            <li>
              Service providers that provide software and services to store our
              customer information;
            </li>
            <li>
              Affiliates of Glassboard so that we may better provide the
              Services;
            </li>
            <li>
              Our lawyers, accountants, auditors, insurers, or other similar
              service providers;
            </li>
            <li>
              Credit card and payment providers that help process payments for
              us (note that we do not store any provided credit card
              information).
            </li>
          </ul>
          <p>
            Glassboard may share your information with law enforcement agencies,
            public authorities, or other organizations if legally required to do
            so, including to meet national security or law enforcement
            requirements, or if we have a good faith belief that such use is
            reasonably necessary to:
          </p>
          <ul>
            <li>comply with a legal obligation, process, or request;</li>
            <li>
              enforce our terms and conditions and other agreements, including
              investigation of any potential violation thereof;
            </li>
            <li>
              detect, prevent, or otherwise address security, fraud, or
              technical issues; or
            </li>
            <li>
              protect the rights, property, or safety of us, our users, a third
              party, or the public as required or permitted by law.
            </li>
          </ul>
          <p>
            In the event of a corporate sale, merger, reorganization,
            dissolution or similar event, we may transfer your Personal
            Information as part of the transferred assets without your consent
            or notice to you. We may use and disclose aggregate information that
            does not identify or otherwise relate to an individual for any
            purpose, unless we are prohibited from doing so under applicable
            law.
          </p>
          <p>
            <strong>Where We Store Your Data</strong>
          </p>
          <p>
            Glassboard has its company based in the United States and our
            Websites are hosted in the United States. Therefore, if you are
            located outside the United States, the information that you submit
            to us through our Websites will be transferred to the United States.
            Likewise, your data and your Personal Information will be accessible
            from and transferred to the United States.
          </p>
          <p>
            Where you have a dispute or complaint regarding Glassboard&rsquo;s
            collection, storage, or use of your Personal Information, you may
            make a complaint to Glassboard by sending it to&nbsp;
            <a
              className="register-sidebar-links"
              href="mailto:privacy@glassboardtech.com"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              privacy@glassboardtech.com
            </a>
            . If you are an EU resident, where the dispute or complaint is not
            satisfactorily resolved or you don&rsquo;t receive a timely
            response, you may escalate the matter to your European data
            protection authority free of charge, and Glassboard commits to
            cooperate with the relevant European data protection authority and
            will comply with the advice given by this authority with regard to
            your information which was transferred from the European Union in
            the context of our Websites.
          </p>
          <p>
            <strong>Individual Rights over Personal Information</strong>
          </p>
          <p>
            Generally, you can review, cure inaccuracies, and update Personal
            Information through your account, if you have one, or by contacting{" "}
            <a
              className="register-sidebar-links"
              href="mailto:privacy@glassboardtech.com"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              privacy@glassboardtech.com
            </a>
            .
          </p>
          <p>
            In certain circumstances, individuals also have the following
            rights:
          </p>
          <ul>
            <li>
              <strong>Access and portability</strong>: You have the right to
              know whether we process Personal Information about you, and if we
              do, to access data we hold about you and certain information about
              how we use it and who we share it with.
            </li>
            <li>
              <strong>Correction, erasure and restriction of processing</strong>
              : You have the right to require us to correct any Personal
              Information held about you that is inaccurate and have incomplete
              data completed or request we delete data: (i) where you believe it
              is no longer necessary for us to hold the Personal Information;
              (ii) where we are processing your data on the basis of our
              legitimate interest and you object to such processing; or (iii) if
              you believe the Personal Information we hold about you is being
              unlawfully processed by us. You can ask us to restrict processing
              data we hold about you other than for storage purposes if: (i) you
              believe the Personal Information is not accurate (whilst we verify
              accuracy); (ii) where we want to erase the Personal Information
              because the processing we are doing is unlawful, but you want us
              to continue to store; (iii) where we no longer need the Personal
              Information for the purposes of the processing, but you require us
              to retain the data for the establishment, exercise, or defense of
              legal claims or where you have objected to us processing Personal
              Information and we are considering your objection.
            </li>
            <li>
              <strong>Objection</strong>: You have the right to object to our
              processing of data about you and we will consider your request.
              Please provide us with detail as to your reasoning so that we can
              assess whether there is a compelling overriding interest in us
              continuing to process such data or we need to process it in
              relation to legal claims.
            </li>
            <li>
              <strong>Complaints</strong>: In the event that you wish to make a
              complaint about how we process your Personal Information, please
              contact us&nbsp;and we will endeavor to deal with your request. If
              you are an EU resident, this is without prejudice to your right to
              launch a claim with the data protection supervisory authority in
              the EU country in which you live or work where you think we have
              infringed data protection laws.
            </li>
          </ul>
          <p>
            You can exercise these rights by sending an email to&nbsp;
            <a
              className="register-sidebar-links"
              href="mailto:privacy@glassboardtech.com"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              privacy@glassboardtech.com
            </a>
            &nbsp;or by mailing Glassboard at the address listed in this Policy.
            Before we respond to your request, we will ask you to verify your
            identity. Note that these rights in their entirety only apply to EU
            residents and subject to the applicable law of the jurisdiction
            where you reside. Where exercise of a particular data subject&apos;s
            right is not required by law, your request will be handled on a
            case-by-case basis.
          </p>
          <p>
            <strong>Applicability</strong>
          </p>
          <p>
            Our Websites include links to third party websites whose privacy
            practices may differ from those of Glassboard. If you submit
            Personal Information to any of those websites, your information is
            governed by their privacy policies. We encourage you to carefully
            read the privacy policies of those third-party websites before you
            submit any information to those websites.
          </p>
          <p>
            <strong>Changes to This Privacy Policy</strong>
          </p>
          <p>
            Each time you visit the Websites or use the Services, the current
            version of this Policy will apply. Please check the date of this
            Policy if you wish to view if changes have been made. You should
            check the Site regularly to be aware of changes. We encourage you to
            periodically review this page for the latest information on our
            privacy practices. Revisions this Policy are effective immediately
            after being posted.
          </p>
          <p>
            <strong>Contact</strong>
          </p>
          <p>
            Please contact Glassboard with any questions or concerns about this
            Policy or our data collection practices:
          </p>
          <p>
            Email:{" "}
            <a
              className="register-sidebar-links"
              href="mailto:privacy@glassboardtech.com"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              privacy@glassboardtech.com
            </a>
          </p>
          <p>Address:</p>
          <p>6510 S. Millrock Dr., Suite 400</p>
          <p>Salt Lake City, UT 84121</p>
        </div>
      </div>
    </Container>
  );
}

PrivacyPolicy.propTypes = {};
