import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchSelf, updateUserById } from "../../slices/usersSlice";

import * as Yup from "yup";
import Log from "../../tools/Log";

import {
  Card,
  List,
  Form,
  Button,
  Icon,
  Grid,
  Loader,
  Transition,
  Divider,
  Container,
} from "semantic-ui-react";

import { Formik } from "formik";
import { FormField } from "../../components/FormField";
import { toast } from "react-toastify";
import { ToastError } from "../../tools/ToastMessage";
export const CompanySettingsCard = () => {
  const dispatch = useDispatch();

  const { isLoading, self, isSubmitting, error } = useSelector(
    (state: RootState) => state.users
  );

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  async function onSubmit(data) {
    if (self) {
      try {
        const { ...userData } = self;
        await dispatch(
          updateUserById({
            ...userData,
            firstName: data.firstName,
            lastName: data.lastName,
            password: data.password,
            email: data.email,
          })
        );
        toast.success(`Settings Updated`);
      } catch (e: any) {
        ToastError(e.response);
      }
    }
  }
  const [showPassword, setShowPassword] = useState(false);

  const validation = Yup.object().shape({
    firstName: Yup.string()

      .trim()
      .required(),
    lastName: Yup.string().trim().required(),
    email: Yup.string()

      .email()
      .trim()
      .required(),
    password: Yup.string()

      .trim()
      .required()
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Minimum eight characters, at least one letter, one number and one special character @$!%*#?&"
      ),
  });

  return (
    <Transition animation={"fade up"}>
      <Container>
        <Card.Group itemsPerRow="two" centered stackable>
          <Card fluid>
            <Card.Content>
              <Card.Header>Your account settings</Card.Header>
            </Card.Content>
            <Card.Content>
              {isLoading || !self ? (
                <Loader active inline="centered" size="large" />
              ) : (
                <Formik
                  initialValues={{
                    firstName: self?.firstName,
                    lastName: self?.lastName,
                    email: self?.email,
                  }}
                  validationSchema={validation}
                  onSubmit={onSubmit}
                >
                  {(props) => (
                    <Form onSubmit={props.handleSubmit} className="dark-labels">
                      <Grid columns={2}>
                        <Grid.Row>
                          <Grid.Column>
                            <FormField
                              name="firstName"
                              component={Form.Input}
                              label="First Name"
                              placeholder="First Name"
                              required
                            />
                          </Grid.Column>
                          <Grid.Column>
                            <FormField
                              name="lastName"
                              component={Form.Input}
                              label="Last Name"
                              placeholder="Last Name"
                              required
                            />
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                      <FormField
                        name="email"
                        component={Form.Input}
                        label="Email"
                        placeholder="Email"
                        required
                      />
                      <FormField
                        name="password"
                        component={Form.Input}
                        label="Password"
                        type={showPassword ? "text" : "password"}
                        autoComplete="off"
                        placeholder="Password"
                        required
                        action={{
                          icon: showPassword ? "eye slash" : "eye",
                          onClick: () => setShowPassword(!showPassword),
                          type: "button",
                        }}
                      />
                      <Button
                        fluid
                        content="Save Changes"
                        type="submit"
                        loading={isSubmitting}
                      />
                    </Form>
                  )}
                </Formik>
              )}
            </Card.Content>
          </Card>
        </Card.Group>
      </Container>
    </Transition>
  );
};
