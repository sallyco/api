import React, { useState } from "react";
import { Link } from "react-router-dom";
import { API, useRequest } from "../../api/swrApi";
import {
  Box,
  Card,
  CardActionArea,
  CardHeader,
  CardContent,
  Avatar,
  CardMedia,
  CircularProgress,
  Typography,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { Asset } from "../../api/assetsApi";
import { toTitleCase } from "../../tools/helpers";
import { Skeleton } from "@mui/material";

type CCProps = {
  companyId: string;
};

const useStyles = makeStyles((theme) => ({
  banner: {
    backgroundColor: "#EEEEEE",
    height: "120px",
  },
  logo: {
    height: 60,
    width: 60,
    backgroundColor: "#FFFFFF",
  },
}));

const CompanyCard = ({ companyId }: CCProps) => {
  const styles = useStyles();
  const getProperty = (company: Asset, propertyKey: string) => {
    return (
      (company?.properties ?? []).find((prop) => prop.key === propertyKey)
        ?.value ?? null
    );
  };
  const companyRequestFilter = {
    include: [
      {
        relation: "deal",
        scope: {
          include: [
            {
              relation: "owner",
            },
          ],
        },
      },
    ],
    fields: {
      logo: false,
      banner: false,
    },
  };
  const logoRequestFilter = {
    fields: ["logo"],
  };
  const bannerRequestFilter = {
    fields: ["banner"],
  };
  const { data: company } = useRequest<Asset>(
    companyId &&
      API.ADD_FILTER(API.ASSET_BY_ID(companyId), companyRequestFilter)
  );
  // Waits for Company object to resolve before fetching additional assets
  const { data: logo } = useRequest<Asset>(
    company && API.ADD_FILTER(API.ASSET_BY_ID(companyId), logoRequestFilter)
  );
  const { data: banner } = useRequest<Asset>(
    company && API.ADD_FILTER(API.ASSET_BY_ID(companyId), bannerRequestFilter)
  );

  const [elevation, setElevation] = useState(1);

  return (
    <>
      <Card
        elevation={elevation}
        onMouseEnter={() => setElevation(4)}
        onMouseLeave={() => setElevation(1)}
      >
        <Link to={companyId ? `/assets/companies/${companyId}` : undefined}>
          <CardActionArea>
            {banner ? (
              <CardMedia
                src={`${banner?.banner}`}
                title={company?.name}
                className={styles.banner}
                style={{
                  backgroundImage: `url('${banner?.banner}')`,
                }}
              />
            ) : (
              <Skeleton variant="rectangular" height={120} animation="wave" />
            )}

            <Box>
              <Box mt={-4} display={"flex"} justifyContent={"center"}>
                <Card style={{}}>
                  {logo ? (
                    <Avatar className={styles.logo} src={logo?.logo} />
                  ) : (
                    <Skeleton
                      height={60}
                      width={60}
                      variant="rectangular"
                      animation="wave"
                    >
                      <Avatar className={styles.logo} />
                    </Skeleton>
                  )}
                </Card>
              </Box>
            </Box>
            <CardHeader
              titleTypographyProps={{ variant: "h6" }}
              title={company?.name ?? <Skeleton animation="wave" />}
              subheader={
                company ? (
                  toTitleCase(getProperty(company, "Market Sector"))
                ) : (
                  <Skeleton animation="wave" />
                )
              }
            />
            <CardContent>
              <Typography variant="body2" color="textSecondary">
                {company ? (
                  company?.deal?.owner?.firstName ? (
                    `Organizer: ${company?.deal?.owner?.firstName} ${company?.deal?.owner?.lastName}`
                  ) : (
                    ""
                  )
                ) : (
                  <Skeleton animation="wave" />
                )}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Link>
      </Card>
    </>
  );
};

export default CompanyCard;
