import React from "react";
import { Link } from "react-router-dom";
import customerHome from "../../assets/images/deal_home_graphic.png";
import {
  Header,
  Button,
  Icon,
  Image,
  Container,
  Transition,
  Divider,
} from "semantic-ui-react";

const NoCompany = () => {
  return (
    <Transition animation={"fade"}>
      <Container textAlign="center">
        <Image src={customerHome.src} size="medium" centered />
        <Header as="h2">
          <Header.Subheader>
            Welcome to the leading deal platform for organizers, investors and
            founders.
          </Header.Subheader>
        </Header>
        <Divider hidden />
        {/*<Button primary as={Link} to="/assets/companies/create" size="small">*/}
        {/*  <Icon name="add" />*/}
        {/*  Create Company Profile*/}
        {/*</Button>*/}
        {/* <Header as="h3">-OR-</Header>
        <Button primary size="small">
          <Icon name="upload" className={"primaryColor"} />
          Import A Profile
        </Button> */}
        <Divider hidden />
      </Container>
    </Transition>
  );
};
export default NoCompany;
