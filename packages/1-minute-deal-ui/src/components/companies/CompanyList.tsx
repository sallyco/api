import React from "react";
import { Container, Divider } from "semantic-ui-react";
import { Asset } from "../../api/assetsApi";
import { Redirect } from "react-router-dom";
import CompanyListActions from "./CompanyListActions";
import CompanyCard from "./CompanyCard";
import { Grid } from "@mui/material";
import NoItems from "../subscriptions/NoItems";
import Cookies from "js-cookie";
import Log from "../../tools/Log";

interface Props {
  companies: Asset[];
}

const getSession = () => {
  const jwt = Cookies.get("__session");
  let session;
  try {
    if (jwt) {
      const base64Url = jwt.split(".")[1];
      const base64 = base64Url.replace("-", "+").replace("_", "/");
      session = JSON.parse(window.atob(base64));
    }
  } catch (error) {
    Log.debug("getSession", error);
  }
  return session;
};

export const CompanyList = ({ companies }: Props) => {
  const session = getSession();
  return (
    <>
      <CompanyListActions />
      <Divider hidden />
      <Container>
        <Grid container spacing={3}>
          {companies.map((company) => (
            <Grid item xs={12} key={company?.id}>
              <CompanyCard companyId={company?.id} />
            </Grid>
          ))}
          {companies.length === 0 &&
            (session.resource_access.account.roles.includes("founder") ? (
              <Redirect to={{ pathname: "/assets/companies/create" }} />
            ) : (
              <NoItems icon={"building"} itemType={"Companies"} />
            ))}
        </Grid>
      </Container>
    </>
  );
};
