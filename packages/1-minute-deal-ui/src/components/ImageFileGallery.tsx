import React, { useEffect, useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import * as parseCookie from "../api/parseCookie";
import { Image, Loader } from "semantic-ui-react";

interface Props {
  fileIds: string[];
}

export default function ImageFileGallery({ fileIds }: Props) {
  const [isLoading, setIsLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  const [fileData, setFileData] = useState([]);
  const token = parseCookie.getToken("__session");

  const downloadDocument = async (fileId: string) => {
    const response = await axios({
      url:
        process.env.API_URL ??
        process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return "";
    }
    return window.URL.createObjectURL(new Blob([response.data]));
  };

  useEffect(() => {
    const downloadFile = async (fId) => {
      let fd = await downloadDocument(fId);
      setFileData((fileData) => [...fileData, fd]);
    };
    if ((!fileData || fileData.length === 0) && !isLoading) {
      setIsLoading(true);
      fileIds.forEach((fId) => {
        downloadFile(fId);
      });
      setIsLoading(false);
    }
  }, [fileIds, isLoading]);

  return (
    <>
      {!fileData || fileData.length === 0 || isLoading ? (
        <Loader />
      ) : (
        <>
          <Image src={fileData[0]} bordered onClick={() => setIsOpen(true)} />
          {isOpen && (
            <Lightbox
              mainSrc={fileData[imageIndex]}
              nextSrc={fileData[(imageIndex + 1) % fileData.length]}
              prevSrc={
                fileData[(imageIndex + fileData.length - 1) % fileData.length]
              }
              onCloseRequest={() => setIsOpen(false)}
              onMovePrevRequest={() =>
                setImageIndex(
                  (imageIndex + fileData.length - 1) % fileData.length
                )
              }
              onMoveNextRequest={() =>
                setImageIndex((imageIndex + 1) % fileData.length)
              }
            />
          )}
        </>
      )}
    </>
  );
}
