import React, { useState } from "react";
import { TextArea, Form } from "semantic-ui-react";
import { PopupModal } from "./common/PopupModal";

export interface SendMessageModalProps {
  open: boolean; // open - true/ close - false
  heading: string;
  onClickClose: () => void;
  onClickSend: (message: string) => void;
  messageCaption?: string;
  messagePlaceholder?: string;
  okcaption?: string | JSX.Element;
  size?: "mini" | "tiny" | "small" | "large" | "fullscreen";
  closecaption?: string;
  rows?: number;
  characterLimit?: number;
}

function SendMessageModal({
  open,
  heading = "Send a Message",
  onClickClose = () => {},
  onClickSend = (message: string) => {},
  messageCaption = "Message",
  messagePlaceholder = "Enter your message",
  okcaption = "Send Message",
  size = "tiny",
  closecaption = "Cancel",
  rows = 10,
  characterLimit = 1000,
}: SendMessageModalProps) {
  const [messageText, setMessageText] = useState("");
  return (
    <PopupModal
      open={open}
      size={size}
      heading={heading}
      content={
        <Form>
          <label>{messageCaption}</label>
          <TextArea
            placeholder={messagePlaceholder}
            value={messageText}
            onChange={(e, { value }) => setMessageText(String(value))}
            maxLength={characterLimit}
            rows={rows}
          />
        </Form>
      }
      okcaption={okcaption}
      closecaption={closecaption}
      onClickOk={() => {
        onClickSend(messageText);
        setMessageText("");
      }}
      onClickClose={onClickClose}
    />
  );
}

export default SendMessageModal;
