import React, { Component } from "react";

class AuthRedirect extends Component {
  componentDidMount() {
    const hn = window.location.hostname;
    let tid: string | undefined = undefined;
    const parts = hn.split(".");
    switch (parts[0]) {
      case "app":
      case "omd":
        tid = "gbt";
        break;
      default:
        tid = window.location.host.split(".")[0];
        break;
    }

    let baseUrl = "api.glassboardtech.com";
    if (hn.includes("localhost")) {
      baseUrl = "localhost:8081";
    } else if (hn.includes("dev")) {
      baseUrl = "api.dev.glassboardtech.com";
    } else if (hn.includes("qa")) {
      baseUrl = "api.qa.glassboardtech.com";
    } else if (hn.includes("staging") || hn.includes("sandbox")) {
      baseUrl = "api.sandbox.glassboardtech.com";
    }

    const protocol = window.location.protocol;
    const redirect = encodeURIComponent(
      window.location.origin.replace("/#/", "")
    );
    window.location.assign(
      `${protocol}//${baseUrl}/auth/realms/${tid}/protocol/openid-connect/auth?response_type=code&scope=openid&redirect_uri=${redirect}&client_id=account`
    );
  }
  render() {
    return <span data-test-component="DomainUrlRedirect"></span>;
  }
}

export default AuthRedirect;
