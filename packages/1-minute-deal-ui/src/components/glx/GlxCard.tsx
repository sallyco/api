import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import NumberFormat from "react-number-format";
import { RootState } from "../../rootReducer";
import {
  Header,
  Label,
  Card,
  Image,
  Table,
  List,
  Transition,
  Icon,
  Divider,
  Button,
  Modal,
  Grid,
} from "semantic-ui-react";
import { fetchAssetById } from "../../slices/assetsSlice";

type Props = {
  companyId: string;
};

const GlxCard = ({ companyId }: Props) => {
  const dispatch = useDispatch();
  const [firstOpen, setFirstOpen] = React.useState(false);
  const [secondOpen, setSecondOpen] = React.useState(false);

  const company = useSelector(
    (state: RootState) => state.assets.assetsById[companyId]
  );

  useEffect(() => {
    if (!company || company.id !== companyId) {
      dispatch(fetchAssetById(companyId));
    }
  }, [dispatch, company, companyId]);

  return (
    <Transition animation={"fade up"}>
      <Card color="grey" raised>
        <Card.Content>
          <Table basic="very" celled compact unstackable>
            <Table.Body>
              <Table.Row>
                <Table.Cell width={12}>
                  <Header as="h2" className="deal-card-header">
                    {company.name}
                  </Header>
                  <Card.Meta>
                    {
                      company.properties?.filter(
                        (entry) => entry.key === "Location"
                      )[0].value
                    }
                    {" · "}
                    {
                      company.properties?.filter(
                        (entry) => entry.key === "No. Of Employees"
                      )[0].value
                    }
                    {" Employees"}
                  </Card.Meta>
                </Table.Cell>
                {company.logo && (
                  <Table.Cell rowSpan="2" textAlign="center">
                    <Image centered fluid src={company.logo} />
                  </Table.Cell>
                )}
              </Table.Row>
            </Table.Body>
          </Table>
          <Card.Description>{company.description}</Card.Description>
        </Card.Content>
        <Card.Content extra>
          <List horizontal size="small" relaxed>
            <List.Item>
              <List.Content>
                <List.Header>Target Raise Amount</List.Header>
                <NumberFormat
                  value={company.funding?.targetRaiseAmount}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"$"}
                  style={{ color: "green" }}
                />
              </List.Content>
            </List.Item>
            {company.funding?.round !== "None" && (
              <List.Item>
                <List.Content>
                  <List.Header>Round</List.Header>
                  {company?.funding?.round}
                </List.Content>
              </List.Item>
            )}
          </List>
        </Card.Content>
        <Card.Content extra>
          <Card.Meta>Advisors</Card.Meta>
          <Card.Description>{company.advisors}</Card.Description>
        </Card.Content>
      </Card>
    </Transition>
  );
};

export default GlxCard;
