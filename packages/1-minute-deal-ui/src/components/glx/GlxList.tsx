import React, { useState } from "react";

import {
  Container,
  Card,
  Divider,
  Loader,
  Icon,
  Header,
  Segment,
  Visibility,
} from "semantic-ui-react";
import { Asset } from "../../api/assetsApi";
import GlxCard from "./GlxCard";

interface Props {
  companies: Asset[];
}

export const GlxList = ({ companies }: Props) => {
  const [tempCompanies, settempCompanies] = useState(companies.slice(0, 2));

  const handleonBottomPassed = (eventname) => {
    const len = tempCompanies.length;
    if (len <= companies.length) {
      const data = companies.slice(0, len + 2);
      settempCompanies(data);
    }
  };

  return (
    <>
      {companies.filter((entry) => entry.isPublic).length > 0 ? (
        <>
          <Container>
            <Divider hidden />
            <Visibility
              continuous={true}
              onBottomVisible={() => handleonBottomPassed("onBottomVisible")}
            >
              <Card.Group itemsPerRow="two" centered stackable>
                {tempCompanies
                  .filter((entry) => entry.isPublic)
                  .map((company) => (
                    <GlxCard key={company.id} companyId={company.id} />
                  ))}
              </Card.Group>
            </Visibility>
          </Container>
        </>
      ) : (
        <Segment placeholder basic>
          <Header icon>
            <Icon name="searchengin" color="grey" />
            No Opportunities to Display
          </Header>
        </Segment>
      )}
      <div style={{ height: 30 }} />
    </>
  );
};
