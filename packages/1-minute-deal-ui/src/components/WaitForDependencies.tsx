import React, { useCallback, useEffect, useState } from "react";
import { Loader } from "semantic-ui-react";

interface WaitForDependenciesProps {
  items: any[];
  loadingElement?: React.ReactNode;
}

const WaitForDependencies: React.FC<WaitForDependenciesProps> = ({
  items,
  children,
  loadingElement,
}) => {
  const [showChildren, setShowChildren] = useState(false);
  useEffect(() => {
    setShowChildren(items.every(Boolean));
  }, [items]);
  return (
    <>
      {showChildren && children}
      {!showChildren &&
        (loadingElement ? loadingElement : <Loader active={true} />)}
    </>
  );
};
export default WaitForDependencies;
