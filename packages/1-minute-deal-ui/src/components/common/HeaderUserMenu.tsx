import React, { useContext, useState, useEffect } from "react";
import Cookies from "js-cookie";
import { AuthContext, TenantContext } from "../../contexts";
import { Link } from "react-router-dom";
import { Icon, Dropdown } from "semantic-ui-react";
import Avatar from "react-avatar";
import { NotificationModal } from "../notifications/NotificationModal";
import { JiraIssueCollector } from "./JiraIssueCollector";
import Log from "../../tools/Log";
import { InviteAdminModal } from "../users/InviteAdminModal";
import { useAccountRole } from "../../contexts";
import { useFeatureFlag } from "../../components/featureflags/FeatureFlags";
import useSelf from "../../hooks/useSelf";

const HeaderUserMenu = () => {
  const userProfile = useContext(AuthContext);
  const tenantProfile = useContext(TenantContext);
  const [inviteModalOpen, setInviteModalOpen] = useState(false);
  const isOrganizer = useAccountRole("organizer");
  const outboundWiresFeature = useFeatureFlag("outbound_wires");
  const self = useSelf();
  const [defaultValues, setDefaultValues] = useState({
    email: userProfile?.email ?? "",
    fullname: `${userProfile?.given_name ?? ""} ${
      userProfile?.family_name ?? ""
    }`,
    customfield_10449: self?.attributes?.phone[0] ?? "",
    reporter: userProfile?.email ?? "",
    customfield_10450: userProfile?.email ?? "",
    customfield_10451: `${userProfile?.given_name ?? ""} ${
      userProfile?.family_name ?? ""
    }`,
    customfield_10446: `${tenantProfile?.name ?? ""} (${tenantProfile?.id})`,
    recordWebInfo: "1",
    recordWebInfoConsent: ["1"],
  });

  useEffect(() => {
    if (self) {
      setDefaultValues({
        ...defaultValues,
        customfield_10449: self?.attributes?.phone[0] ?? "",
      });
    }
  }, [self]);

  const trigger = (
    <Avatar
      name={`${userProfile.given_name} ${userProfile.family_name}`}
      email={userProfile.email}
      size="34px"
      round={true}
    />
  );

  const getSession = () => {
    const jwt = Cookies.get("__session");
    let session;
    try {
      if (jwt) {
        const base64Url = jwt.split(".")[1];
        const base64 = base64Url.replace("-", "+").replace("_", "/");
        session = JSON.parse(window.atob(base64));
      }
    } catch (error) {
      Log.error("Error", error);
    }
    return session;
  };

  const isAdmin = () => {
    const session = getSession();
    return session?.resource_access?.account?.roles.includes("admin");
  };

  const options = [
    {
      key: "user",
      text: "Account",
      icon: "user",
      as: Link,
      to: "/dashboard/settings",
    },
    {
      key: "profiles",
      text: "Profiles",
      icon: "users",
      as: Link,
      to: "/profiles",
    },
    // TODO (Andrew) this feature is not ready yet
    //{
    //  key: "inviteUsers",
    //  text: "Invite users",
    //  icon: "user plus",
    //  onClick: () => setInviteModalOpen(!inviteModalOpen),
    //},
    {
      key: "logout",
      text: "Sign Out",
      icon: "sign out",
      as: Link,
      to: "/logout",
    },
  ];

  if (isAdmin()) {
    options.splice(1, 0, {
      key: "admin",
      text: "Admin",
      icon: "user circle",
      as: Link,
      to: "/admin",
    });
  }

  if (outboundWiresFeature) {
    options.splice(-1, 0, {
      key: "banking",
      text: "Banking",
      icon: "money",
      as: Link,
      to: "/dashboard/organizer-banking",
    });
  }

  return (
    <>
      <InviteAdminModal
        open={inviteModalOpen}
        changeOpen={setInviteModalOpen}
      />
      <NotificationModal />
      {self !== null ? (
        <JiraIssueCollector
          defaultValues={defaultValues}
          env={{
            url: window.location.href,
          }}
        />
      ) : null}
      <Dropdown
        trigger={trigger}
        options={options}
        pointing="top right"
        icon={<Icon name="dropdown" inverted={tenantProfile.inverted} />}
      />
    </>
  );
};

export default HeaderUserMenu;
