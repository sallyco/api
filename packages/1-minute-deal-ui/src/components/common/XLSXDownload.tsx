import React from "react";
import { Button, Icon } from "semantic-ui-react";
import * as XLSX from "xlsx";
interface Props {
  data: [];
  filename: string;
}

export const XLSXDownload: React.FunctionComponent<Props> = ({
  data,
  filename,
}) => {
  function exportToXLSX(sheetData, filename) {
    const ws = XLSX.utils.json_to_sheet(sheetData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    const fileURL = URL.createObjectURL(data);
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.href = fileURL;
    a.download = filename;
    a.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(fileURL);
      document.body.removeChild(a);
    }, 0);
  }

  return (
    <Button
      secondary
      size="tiny"
      onClick={() => {
        exportToXLSX(data, filename);
      }}
    >
      <Icon name="cloud download"></Icon>
      XLSX
    </Button>
  );
};
