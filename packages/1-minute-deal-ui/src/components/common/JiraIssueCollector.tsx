/** Reference: https://support.atlassian.com/jira-cloud-administration/docs/customize-the-jira-issue-collector/ */

import React from "react";
import { Icon } from "semantic-ui-react";

const ELEMENT_ID = "jira-feedback-button";
const WINDOW_VAR_NAME = "jiraIssueCollector";
const SHOW_COLLECTOR_DIALOG = "showCollectorDialog";
const ATL_JQ_PAGE_PROPS = "ATL_JQ_PAGE_PROPS";

const setCollector = (defaultValues, env) => {
  // Set up the default values before calling the collector snippet
  window[ATL_JQ_PAGE_PROPS] = {
    triggerFunction: function (showCollectorDialog) {
      // To open the collector,
      // we just have to call window.showCollectorDialog()
      window[SHOW_COLLECTOR_DIALOG] = showCollectorDialog;
    },
    fieldValues: defaultValues,
    environment: env,
  };

  const appElement = document.querySelector("body");
  if (appElement) {
    const snippet = document.createElement("script");
    snippet.type = "text/javascript";
    snippet.src =
      "https://glassboardtech.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/sb53l8/b/24/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=a7bb58d3";
    appElement.appendChild(snippet);
  }
};

export const JiraIssueCollector = ({ defaultValues, env }) => {
  // We only need to set it once
  if (!window[WINDOW_VAR_NAME]) {
    setCollector(defaultValues, env);
    window[WINDOW_VAR_NAME] = "set";
  } else {
    setIssueCollectorDefaults(defaultValues);
  }

  return (
    <Icon.Group>
      <Icon
        style={{ marginRight: "8px" }}
        inverted
        size={"large"}
        name={"bug"}
        id={ELEMENT_ID}
        onClick={() => {
          if (window && window[SHOW_COLLECTOR_DIALOG]) {
            window[SHOW_COLLECTOR_DIALOG]();
          } else {
            console.warn("Issue Collector Unnavailable");
          }
        }}
      />
    </Icon.Group>
  );
};

/**
 *
 * @param defaultValues object mapping of field: value
 */
export function setIssueCollectorDefaults(defaultValues) {
  const originalValues = window[ATL_JQ_PAGE_PROPS].fieldValues;
  window[ATL_JQ_PAGE_PROPS].fieldValues = {
    ...originalValues,
    ...defaultValues,
  };
}
