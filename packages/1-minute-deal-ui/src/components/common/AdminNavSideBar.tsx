import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Icon, Sidebar, Menu } from "semantic-ui-react";
import { AuthContext } from "../../contexts";

const AdminNavSideBar = ({
  isVisible,
  setVisible,
}: {
  isVisible: any;
  setVisible: any;
}) => {
  const userProfile = useContext(AuthContext);

  return (
    <Sidebar
      as={Menu}
      animation="overlay"
      className={"primaryBackground"}
      direction="left"
      icon="labeled"
      onHide={() => setVisible(false)}
      inverted
      vertical
      visible={isVisible}
    >
      <Menu.Item
        name="users"
        as={NavLink}
        active={true}
        to="/admin/users"
        onClick={() => setVisible(false)}
      >
        <Icon name="users" />
        Users
      </Menu.Item>
      <Menu.Item
        name="emails"
        as={NavLink}
        to="/admin/emails"
        onClick={() => setVisible(false)}
      >
        <Icon name="mail" />
        Emails
      </Menu.Item>
      <Menu.Item
        name="configurations"
        as={NavLink}
        to="/admin/configurations"
        onClick={() => setVisible(false)}
      >
        <Icon name="cogs" />
        Configurations
      </Menu.Item>
    </Sidebar>
  );
};

export default AdminNavSideBar;
