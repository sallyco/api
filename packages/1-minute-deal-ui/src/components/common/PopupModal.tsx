import React from "react";
import { Button, Modal } from "semantic-ui-react";
import { CircularProgress } from "@mui/material";

export interface Props {
  open: boolean; // open - true/ close - false
  size?: "mini" | "tiny" | "small" | "large" | "fullscreen";
  heading: string;
  content: string | JSX.Element;
  okcaption?: string | JSX.Element;
  closecaption?: string;
  onClickClose?: () => void;
  onClickOk?: () => void;
  processing?: boolean;
}

export const PopupModal: React.FunctionComponent<Props> = ({
  open,
  size,
  heading,
  content,
  okcaption = "",
  closecaption = "",
  onClickClose = () => {},
  onClickOk = () => {},
  processing = false,
}) => {
  return (
    <>
      <Modal size={size} open={open} closeIcon onClose={() => onClickClose()}>
        <Modal.Header>{heading}</Modal.Header>
        <Modal.Content>
          <div>{content}</div>
        </Modal.Content>
        <Modal.Actions>
          {!processing ? (
            <>
              {closecaption ? (
                <Button type="Button" negative onClick={onClickClose}>
                  {closecaption}
                </Button>
              ) : null}
              {okcaption && (
                <Button
                  type="Button"
                  positive
                  content={okcaption}
                  onClick={onClickOk}
                />
              )}
            </>
          ) : (
            <CircularProgress />
          )}
        </Modal.Actions>
      </Modal>
    </>
  );
};
