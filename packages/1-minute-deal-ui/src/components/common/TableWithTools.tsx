import React, { useState } from "react";
import {
  Transition,
  Card,
  Table,
  Button,
  Icon,
  Segment,
} from "semantic-ui-react";
import _ from "lodash";
import { CSVLink } from "react-csv";

export interface TableProps {
  data: object[];
  columnDefinitions: ColumnDefinition[];
  selectedColumn?: any;
  direction?: any;
  exportDataMap?: CallableFunction;
  entityName?: string;
  rowProps?: any;
  allowDownload?: boolean;
}

export interface ColumnDefinition {
  key: string;
  headerClassName?: string;
  headerTitle?: string;
  headerProps?: any;
  cellProps?: any;
  rowDisplayValue?: any;
}

export const TableWithTools = ({
  data,
  columnDefinitions,
  selectedColumn,
  direction,
  exportDataMap,
  entityName,
  rowProps,
  allowDownload = true,
}: TableProps) => {
  const [state, setState] = useState({
    selectedColumn: selectedColumn,
    data: data,
    direction: direction,
  });

  const handleSort = (clickedColumn) => {
    if (state.selectedColumn !== clickedColumn) {
      setState({
        selectedColumn: clickedColumn,
        data: _.sortBy(state.data, [clickedColumn]),
        direction: "ascending",
      });

      return;
    }

    setState({
      selectedColumn: state.selectedColumn,
      data: state.data.reverse(),
      direction: state.direction === "ascending" ? "descending" : "ascending",
    });
  };

  function csvData() {
    return _.map(state.data, exportDataMap);
  }

  const keyToHeader = (key: string) => {
    var result = key.replace(/([A-Z])/g, " $1");
    return result.charAt(0).toUpperCase() + result.slice(1);
  };

  const today = new Date();
  const baseFilename = `${entityName}_${
    today.getMonth() + 1
  }-${today.getDate()}-${today.getFullYear()}`;

  return (
    <Transition animation={"fade up"}>
      <Card fluid className={"primary"}>
        {allowDownload && (
          <Segment.Group compact horizontal>
            <Segment padded compact floated="left">
              <CSVLink
                data={csvData()}
                filename={`${baseFilename}.csv`}
                target={"_blank"}
                className={"no-padding"}
              >
                <Button secondary size="tiny">
                  <Icon name="cloud download"></Icon>CSV
                </Button>
              </CSVLink>
            </Segment>
          </Segment.Group>
        )}
        <Table sortable striped selectable compact>
          <Table.Header>
            <Table.Row>
              {columnDefinitions.map((column) => (
                <Table.HeaderCell
                  key={column.key}
                  sorted={
                    state.selectedColumn === column.key ? state.direction : null
                  }
                  onClick={() => handleSort(column.key)}
                  className={column.headerClassName}
                >
                  {column.headerTitle ?? keyToHeader(column.key)}
                </Table.HeaderCell>
              ))}
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {_.map(state.data, (rowData: any) => (
              <Table.Row key={rowData.id} id={rowData.id} {...rowProps}>
                {columnDefinitions.map((column) => (
                  <Table.Cell key={column.key} {...column.cellProps}>
                    {column.rowDisplayValue(rowData)}
                  </Table.Cell>
                ))}
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </Card>
    </Transition>
  );
};
