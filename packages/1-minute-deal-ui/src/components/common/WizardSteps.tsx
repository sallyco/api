import React from "react";
import { Header, Progress } from "semantic-ui-react";
import { LinearProgress } from "@mui/material";

/**
Example Usage.

Give it child elements
Tell it which one to render

it will ignore null elements to support conditional rendering

const flag = true | false;

<WizardSteps currentStep={stepNumber}>
    <Header content="HELLO 1" />
    <Header content="HELLO 2" />
    {flag ? "Hello 3" : null}
</WizardSteps>

 */

function WizardStep({ children }) {
  return <>{children}</>;
}

export default function WizardSteps({ children, currentStep = 1 }) {
  // For convenience, filter out empty children elements (feature flags should render as null)
  const desiredStepIndex = currentStep - 1;
  const realSteps = React.Children.toArray(children).filter(
    (child) => child !== null
  );
  if (!Array.isArray(children) || children.length <= 0) {
    return null;
  }

  if (desiredStepIndex >= realSteps.length) {
    // Render the last step
    return <>{realSteps[realSteps.length - 1]}</>;
  }

  if (desiredStepIndex < 0) {
    // Render first step
    return <WizardStep>{realSteps[0]}</WizardStep>;
  }

  const renderStep = realSteps[currentStep - 1];
  return (
    <>
      <Header as="h3" inverted>
        {`Section ${currentStep} of ${realSteps.length}`}
      </Header>
      <Progress
        value={currentStep}
        total={realSteps.length}
        size="small"
        success
      />
      <WizardStep>{renderStep}</WizardStep>
    </>
  );
}
