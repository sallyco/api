import React from "react";
import { Icon, Popup } from "semantic-ui-react";
import {
  SemanticCOLORS,
  SemanticICONS,
} from "semantic-ui-react/dist/commonjs/generic";

interface Props {
  labelText?: string;
  popupText?: string;
  iconName?: SemanticICONS;
  iconColor?: SemanticCOLORS;
}

export const LabelwithTooltip: React.FunctionComponent<Props> = ({
  labelText,
  popupText,
  iconName,
  iconColor,
}) => {
  return (
    <>
      <label>
        {labelText}
        <Popup
          trigger={
            <Icon color={iconColor} name={iconName} className="Icon-f-right" />
          }
          content={popupText}
          position="right center"
        />
      </label>
    </>
  );
};
