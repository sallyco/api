import React from "react";
import { NavLink } from "react-router-dom";
import { Header, Container, Menu } from "semantic-ui-react";

export const AdminBanner = () => {
  return (
    <Menu size="massive" inverted borderless>
      <Container>
        <Menu.Item>
          <Header as={"h2"} inverted>
            Administration
          </Header>
        </Menu.Item>

        <Menu.Menu position="right">
          <Menu.Item name="exit" as={NavLink} to="/dashboard">
            Exit Admin
          </Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>
  );
};
