import React from "react";
import { Header, Container } from "semantic-ui-react";

export const CopyrightFooter = () => {
  return (
    <Container fluid columns={1} id={"footer"} textAlign="center">
      <Header
        as="h4"
        style={{ fontSize: "8pt", fontWeight: "200", color: "#777" }}
      >
        {`© ${new Date().getFullYear()} Assure Services, Inc. All rights reserved. Glassboard is a registered trademark of MMAC, Inc. DBA Glassboard Technology`}
      </Header>
    </Container>
  );
};
