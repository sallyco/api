import React from "react";
import { NavLink } from "react-router-dom";
import { Icon, Menu, Responsive } from "semantic-ui-react";

const AdminNavBar = (): JSX.Element => {
  return (
    <Responsive minWidth={Responsive.onlyTablet.minWidth}>
      <div className="ui main-navigation">
        <Menu
          size="tiny"
          icon="labeled"
          inverted
          className={"navbar items-center"}
        >
          <Menu.Item name="users" as={NavLink} to="/admin/users">
            <Icon name="users" />
            Users
          </Menu.Item>

          <Menu.Item name="emails" as={NavLink} to="/admin/emails">
            <Icon name="mail" />
            Emails
          </Menu.Item>

          <Menu.Item name="emails" as={NavLink} to="/admin/signings">
            <Icon name="pencil alternate" />
            Signings
          </Menu.Item>

          <Menu.Item
            name="configurations"
            as={NavLink}
            to="/admin/configurations"
          >
            <Icon name="cogs" />
            Configurations
          </Menu.Item>
        </Menu>
      </div>
    </Responsive>
  );
};

export default AdminNavBar;
