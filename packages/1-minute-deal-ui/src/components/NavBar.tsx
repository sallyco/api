import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Icon, Menu, Responsive } from "semantic-ui-react";
import { AuthContext } from "../contexts";
import { CONTACT_LIST_FLAG } from "./ContactListTable";
import { FeatureFlag } from "./featureflags/FeatureFlags";

const NavBar = (): JSX.Element => {
  const userProfile = useContext(AuthContext);

  return (
    <Responsive minWidth={Responsive.onlyTablet.minWidth}>
      <div className="ui main-navigation">
        {userProfile.resource_access.account.roles.includes("account-admin") ? (
          <></>
        ) : userProfile.resource_access.account.roles.includes("founder") ||
          userProfile.resource_access.account.roles.includes("dealer") ? (
          <Menu
            size="tiny"
            icon="labeled"
            inverted
            className={"navbar items-center"}
          >
            {userProfile.resource_access.account.roles.includes("founder") && (
              <Menu.Item
                name="companies"
                as={NavLink}
                to="/dashboard/assets/companies"
              >
                <Icon name="building" />
                Companies
              </Menu.Item>
            )}

            {userProfile.resource_access.account.roles.includes("dealer") && (
              <Menu.Item
                name="artwork"
                as={NavLink}
                to="/dashboard/assets/artwork"
              >
                <Icon name="image" />
                Artwork
              </Menu.Item>
            )}

            {/*Temporarily Removed*/}
            {/*<Menu.Item*/}
            {/*  name="organizers"*/}
            {/*  as={NavLink}*/}
            {/*  to="/dashboard/companyOrganizer"*/}
            {/*>*/}
            {/*  <Icon name="users" />*/}
            {/*  Organizers*/}
            {/*</Menu.Item>*/}
          </Menu>
        ) : (
          <Menu
            size="tiny"
            icon="labeled"
            inverted
            className={"navbar items-center"}
          >
            {userProfile.resource_access.account.roles.includes("signer") && (
              <Menu.Item name="signing" as={NavLink} to="/dashboard/signing">
                <Icon name="edit" />
                Signing
              </Menu.Item>
            )}

            {userProfile.resource_access.account.roles.includes(
              "organizer"
            ) && (
              <>
                <Menu.Item name="deals" as={NavLink} to="/dashboard/deals">
                  <Icon name="dollar" />
                  Deals
                </Menu.Item>
                <FeatureFlag name="founders_assessment">
                  <Menu.Item
                    name="companies"
                    as={NavLink}
                    to="/dashboard/assets/companies"
                  >
                    <Icon name="building" />
                    Companies
                  </Menu.Item>
                </FeatureFlag>
              </>
            )}

            {userProfile.resource_access.account.roles.includes("investor") && (
              <Menu.Item
                name="subscriptions"
                as={NavLink}
                to="/dashboard/subscriptions"
              >
                <Icon name="money" />
                Investments
              </Menu.Item>
            )}

            {userProfile.resource_access.account.roles.includes("organizer") ||
              (userProfile.resource_access.account.roles.includes(
                "investor"
              ) && (
                <Menu.Item name="docs" as={NavLink} to="/dashboard/docs">
                  <Icon name="file alternate" />
                  Docs
                </Menu.Item>
              ))}

            {userProfile.resource_access.account.roles.includes(
              "organizer"
            ) && (
              <Menu.Item
                name="investors"
                as={NavLink}
                to="/dashboard/investors"
              >
                <Icon name="users" />
                Investors
              </Menu.Item>
            )}

            <FeatureFlag name={CONTACT_LIST_FLAG}>
              {userProfile.resource_access.account.roles.includes(
                "organizer"
              ) && (
                <Menu.Item
                  name="contacts"
                  as={NavLink}
                  to="/dashboard/contacts"
                >
                  <Icon name="address book" />
                  Contacts
                </Menu.Item>
              )}
            </FeatureFlag>
          </Menu>
        )}
      </div>
    </Responsive>
  );
};

export default NavBar;
