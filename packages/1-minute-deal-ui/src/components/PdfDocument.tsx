import React, { useEffect, useState } from "react";

import { Page, pdfjs } from "react-pdf";
import * as ReactPDF from "react-pdf";
import "react-pdf/dist/esm/Page/AnnotationLayer.css";

import axios from "axios";
import { toast } from "react-toastify";
import * as parseCookie from "../api/parseCookie";
import useSWR from "swr";
import { getFileById } from "../api/filesApi";
import { DownloadFile } from "../tools/downloadUtils";
import {
  Card,
  Loader,
  Modal,
  Button,
  Divider,
  Message,
} from "semantic-ui-react";
import { Box, LinearProgress, Typography } from "@mui/material";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

interface Props {
  fileId: any;
  pageNumber?: number;
  width?: number;
  disableClick?: boolean;
  initialFileData?: string;
}

export default function PdfDocument({
  fileId,
  pageNumber = 1,
  width = 380,
  disableClick = false,
  initialFileData = "",
}: Props) {
  const [modalOpen, setModalOpen] = React.useState(false);
  const [numPages, setNumPages] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState(pageNumber);
  const token = parseCookie.getToken("__session");
  const [fileData, setFileData] = useState(initialFileData);
  const [filename, setFilename] = useState("");
  const [currentPercent, setCurrentPercent] = useState(0);

  const { data: file } = useSWR(
    () => (fileId && !fileData ? `/files/${fileId}` : null),
    () => getFileById(fileId),
    {
      refreshInterval: 3000,
      refreshWhenHidden: true,
    }
  );

  const onPDFLoadSuccess = ({ numPages }) => setNumPages(numPages);

  const onPage = (type) => {
    var newPage = type ? currentPage + 1 : currentPage - 1;
    if (newPage > numPages) {
      newPage = numPages;
    } else if (newPage < 1) {
      newPage = 1;
    }
    setCurrentPage(newPage);
  };

  const downloadDocument = async (fileId: string) => {
    const response = await axios({
      url:
        process.env.API_URL ??
        process.env.NEXT_PUBLIC_API_URL + "/files/" + fileId + "/download",
      method: "GET",
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      onDownloadProgress: (progressEvent) => {
        const total = parseFloat(progressEvent.total);
        const current = progressEvent.loaded;
        let percentCompleted = Math.floor((current / total) * 100);
        setCurrentPercent(percentCompleted);
      },
    });
    if (response.status !== 200) {
      toast.error("Download is unavailable at this time for this document");
      return "";
    }
    return window.URL.createObjectURL(new Blob([response.data]));
  };

  useEffect(() => {
    const downloadFile = async () => {
      setFileData(await downloadDocument(fileId));
    };
    if (file && !file?.generating && fileData === "") {
      setFilename(file?.name ?? "");
      downloadFile();
    }
  }, [file, fileData]);

  //  goToPrevPage = () =>
  //    this.setState(state => ({ pageNumber: state.pageNumber - 1 }));
  //  goToNextPage = () =>
  //    this.setState(state => ({ pageNumber: state.pageNumber + 1 }));

  //  const file = useSelector(
  //    (state: RootState) => state.files.filesById[fileId]
  //  )
  //
  //  useEffect(() => {
  //    if (fileId && (!file || file.id !== fileId)) {
  //      dispatch(fetchFileById(fileId))
  //    }
  //  }, [file, fileId])

  return (
    <Card fluid>
      {!fileData ? (
        <Card.Header>
          <div style={{ aspectRatio: "1 / 1.2941" }}>
            <Divider hidden />
            {(!file || file.generating) && (
              <>
                <Box>
                  <Loader active inline="centered" />
                </Box>
                {file?.generating && (
                  <Box textAlign="center">
                    <Typography variant="overline" display="block">
                      Generating
                    </Typography>
                  </Box>
                )}
              </>
            )}
            {file && !file?.generating && currentPercent !== 100 && (
              <>
                <Box mx={2}>
                  <LinearProgress
                    variant="determinate"
                    value={currentPercent}
                  />
                </Box>
                <Box textAlign="center">
                  <Typography variant="overline" display="block">
                    Loading ({currentPercent}%)
                  </Typography>
                </Box>
              </>
            )}
            <Divider hidden />
          </div>
        </Card.Header>
      ) : (
        <Modal
          onClose={() => setModalOpen(false)}
          onOpen={() => setModalOpen(!disableClick && true)}
          open={modalOpen}
          size="small"
          trigger={
            <div style={{ margin: "auto", cursor: "pointer" }}>
              <ReactPDF.Document
                file={fileData}
                onLoadSuccess={onPDFLoadSuccess}
                loading={
                  <Loader active inverted inline content="Loading File..." />
                }
                error={
                  <Message negative>
                    Failed to load file. Please refresh this page
                  </Message>
                }
              >
                <Page pageNumber={currentPage} width={width} />
              </ReactPDF.Document>
            </div>
          }
        >
          <Modal.Header>{filename}</Modal.Header>
          <Modal.Content style={{ display: "flex" }}>
            <div style={{ margin: "auto" }}>
              <ReactPDF.Document
                file={fileData}
                onLoadSuccess={onPDFLoadSuccess}
                loading={
                  <Loader active inverted inline content="Loading File..." />
                }
                error={
                  <Message negative>
                    Failed to load file. Please refresh this page
                  </Message>
                }
              >
                <Page pageNumber={currentPage} width={500} />
              </ReactPDF.Document>
            </div>
          </Modal.Content>
          <Modal.Actions>
            <Button disabled={currentPage <= 1} onClick={() => onPage(0)}>
              Previous
            </Button>
            <Button
              disabled={currentPage >= numPages}
              onClick={() => onPage(1)}
            >
              Next
            </Button>
            <Button
              content="Download"
              labelPosition="right"
              icon="download"
              onClick={() => DownloadFile(fileId, filename)}
              positive
            />
            <Button onClick={() => setModalOpen(false)}>Close</Button>
          </Modal.Actions>
        </Modal>
      )}
    </Card>
  );
}
