import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import {
  Grid,
  Header,
  Icon,
  Loader,
  Container,
  Image,
} from "semantic-ui-react";
import HeaderUserMenu from "./common/HeaderUserMenu";
import { TenantContext } from "../contexts";

interface Props {
  title: string;
  hideBackArrow?: boolean;
  hideHome?: boolean;
  backArrowLink?: string;
  loading?: boolean;
}

const ShortHeader = ({
  title,
  hideBackArrow = false,
  hideHome = false,
  backArrowLink = "",
  loading = false,
}: Props) => {
  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  return (
    <div className="ui dashboard-header">
      <Container fluid className="form-page-header">
        <Container>
          <Grid padded className="form-page-header" verticalAlign="middle">
            <Grid.Row columns={"equal"}>
              <Grid.Column>
                {tenantProfile.url ? (
                  <Link to="/dashboard">
                    <Image
                      float="left"
                      size={"small"}
                      style={{
                        maxWidth: "150px",
                        maxHeight: "55px",
                        width: "auto",
                        display: "inline",
                      }}
                      src={
                        tenantProfile.inverted
                          ? tenantProfile.assets.images.logoInverted
                          : tenantProfile.assets.images.logo
                      }
                    />
                  </Link>
                ) : (
                  <Image
                    float="left"
                    size={"small"}
                    style={{
                      maxWidth: "150px",
                      maxHeight: "55px",
                      width: "auto",
                      display: "inline",
                    }}
                    src={
                      tenantProfile.inverted
                        ? tenantProfile.assets.images.logoInverted
                        : tenantProfile.assets.images.logo
                    }
                  />
                )}
                {!hideBackArrow && (
                  <Link
                    to={backArrowLink ?? ""}
                    onClick={() => (!backArrowLink ? history.goBack() : null)}
                  >
                    <Icon name="arrow left" size="large" />
                  </Link>
                )}
                {!hideHome && (
                  <Link to="/dashboard">
                    <Icon name="home" size="large" />
                  </Link>
                )}
              </Grid.Column>
              <Grid.Column textAlign="right">
                <HeaderUserMenu />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={"equal"}>
              <Grid.Column>
                {loading ? (
                  <Loader inline="centered" active inverted />
                ) : (
                  <Header
                    as="h3"
                    inverted
                    className="deal-card-header"
                    textAlign="center"
                  >
                    {title}
                  </Header>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Container>
    </div>
  );
};

export default ShortHeader;
