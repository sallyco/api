import React, { useState } from "react";
import { toast } from "react-toastify";
import useSWR from "swr";
import {
  Button,
  Divider,
  Dropdown,
  Header,
  Icon,
  Modal,
} from "semantic-ui-react";
import { TableWithTools } from "@packages/gbt-ui";
import EditWireBeneficiaryForm from "./EditWireBeneficiaryForm";
import NoBeneficiaries from "./NoBeneficiaries";
import { deleteBankingBeneficiaryById } from "../../api/bankingApi";
import { PopupModal } from "../common/PopupModal";
import { fetcher } from "../../swrConnector";
import { IconButton } from "@mui/material";
import {
  ExternalLinkSquareAltIcon,
  PencilIcon,
  TrashIcon,
} from "react-line-awesome";
import EditACHBeneficiaryForm from "./EditACHBeneficiaryForm";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
const ID_COLUMN_INDEX = 0;
const TYPE_COLUMN_INDEX = 4;

const BeneficiaryListTable = () => {
  const [showWireEdit, setShowWireEdit] = useState(false);
  const [showACHEdit, setShowACHEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [selectedBeneficiary, setSelectedBeneficiary] = useState<{
    id: string;
    type: string;
  }>(null);

  const {
    data: beneficiaries,
    error,
    mutate: mutateList,
  } = useSWR(() => baseUrl + `/banking/beneficiaries`, fetcher);

  const deleteHandler = async () => {
    try {
      const result = await deleteBankingBeneficiaryById(
        selectedBeneficiary?.id
      );
      setShowDelete(false);
      toast.success("Beneficiary was removed.");
      mutateList(baseUrl + `/banking/beneficiaries`);
    } catch (err) {
      toast.error("Beneficiary was not removed.");
    }
  };

  const actionMenu = (rowData) => {
    return (
      <>
        <IconButton
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            const type = rowData[TYPE_COLUMN_INDEX];
            setSelectedBeneficiary({
              id: rowData[ID_COLUMN_INDEX],
              type,
            });
            if (type === "wire") {
              setShowWireEdit(true);
            } else if (type === "ach") {
              setShowACHEdit(true);
            }
          }}
          size="large"
        >
          <PencilIcon />
        </IconButton>
        <IconButton
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setSelectedBeneficiary({
              id: rowData[ID_COLUMN_INDEX],
              type: rowData[TYPE_COLUMN_INDEX],
            });
            setShowDelete(true);
          }}
          size="large"
        >
          <TrashIcon />
        </IconButton>
      </>
    );
  };

  return (
    <>
      {!beneficiaries ? (
        <NoBeneficiaries />
      ) : (
        <TableWithTools
          data={beneficiaries.data}
          columnDefinitions={[
            {
              name: "id",
              options: {
                display: false,
                filter: false,
              },
            },
            {
              name: "name",
              label: "BENEFICIARY NAME",
            },
            {
              name: "email",
              label: "EMAIL",
            },
            {
              name: "phone",
              label: "PHONE",
            },
            {
              name: "type",
              label: "TYPE",
              // eslint-disable-next-line react/display-name
              options: {
                // eslint-disable-next-line react/display-name
                customBodyRender: (
                  value: any,
                  tableMeta: any,
                  updateValue: any
                ) => (value ? value.toUpperCase() : ""),
              },
            },
            {
              name: "routingNumber",
              label: "ROUTING #",
            },
            {
              name: "accountNumber",
              label: "ACCOUNT #",
              // eslint-disable-next-line react/display-name
              options: {
                // eslint-disable-next-line react/display-name
                customBodyRender: (
                  value: any,
                  tableMeta: any,
                  updateValue: any
                ) => {
                  if (value && typeof value === "string") {
                    const last4digits =
                      "XXXXXXXXXXXXX" + value.substring(value.length, 4);
                    return last4digits.substring(
                      last4digits.length - value.length
                    );
                  }
                  return "";
                },
              },
            },
            {
              name: "actions",
              label: " ",
              // eslint-disable-next-line react/display-name
              options: {
                // eslint-disable-next-line react/display-name
                customBodyRender: (
                  value: any,
                  tableMeta: any,
                  updateValue: any
                ) => {
                  return actionMenu(tableMeta.rowData);
                },
              },
            },
          ]}
          selectable={false}
          clickable
        />
      )}
      <PopupModal
        open={showWireEdit}
        size="fullscreen"
        heading="Beneficiary Details"
        content={
          <>
            <EditWireBeneficiaryForm
              beneficiaryId={selectedBeneficiary?.id}
              type={selectedBeneficiary?.type}
              onSuccessHandler={(updatedBeneficiary) => {
                setShowWireEdit(false);
                mutateList(baseUrl + `/banking/beneficiaries`);
              }}
            />
          </>
        }
        onClickClose={() => setShowWireEdit(false)}
      />
      <PopupModal
        open={showACHEdit}
        size="fullscreen"
        heading="Beneficiary Details"
        content={
          <>
            <EditACHBeneficiaryForm
              beneficiaryId={selectedBeneficiary?.id}
              type={selectedBeneficiary?.type}
              onSuccessHandler={(updatedBeneficiary) => {
                setShowACHEdit(false);
                mutateList(baseUrl + `/banking/beneficiaries`);
              }}
            />
          </>
        }
        onClickClose={() => setShowACHEdit(false)}
      />
      {showDelete && (
        <Modal open={showDelete} onClose={() => setShowDelete(false)} closeIcon>
          <Modal.Content>
            <Header as="h3" icon textAlign="center">
              <Icon name="exclamation circle" color="orange" />
              Are you sure you want to remove selected beneficiary?
            </Header>
            <Divider hidden />
            <Button
              secondary
              fluid
              onClick={deleteHandler}
              content="Remove Beneficiary"
            />
          </Modal.Content>
        </Modal>
      )}
    </>
  );
};

export default BeneficiaryListTable;
