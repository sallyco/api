import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CheckIcon, TrashIcon } from "react-line-awesome";
import { toast } from "react-toastify";
import useSWR from "swr";
import {
  Button,
  Card,
  Divider,
  Dropdown,
  Grid,
  Header,
  Icon,
  Loader,
  Modal,
} from "semantic-ui-react";
import { IconButton, Tooltip } from "@mui/material";
import { TableWithTools } from "@packages/gbt-ui";

import NoProfileAccounts from "./NoProfileAccounts";
import ProfileAccountCreateButton from "./ProfileAccountCreateButton";
import { getProfileById } from "../../api/profilesApi";
import { getProfileName } from "../../api/profileApiWrapper";
import { RootState } from "../../rootReducer";
import {
  fetchPlaidAccountsList,
  removeSubAccountById,
} from "../../slices/accountsSlice";
import {
  fetchProfileById,
  fetchProfilesList,
  removeBankingAccount,
  updateProfileById,
} from "../../slices/profilesSlice";
import { fetcher } from "../../swrConnector";
import FormHint from "../../forms/common/FormHint";
import { PopupModal } from "../common/PopupModal";
import { API, useRequest } from "../../api/swrApi";

const ID_COLUMN_INDEX = 0;
const ACCOUNT_ID_COLUMN_INDEX = 1;
const SUB_ACCOUNT_ID_COLUMN_INDEX = 2;

const LinkedAccountsTable = ({ plaidLinkToken }) => {
  const dispatch = useDispatch();
  const [showDelete, setShowDelete] = useState(false);
  const [showDefault, setShowDefault] = useState(false);
  const [showProfileSelection, setShowProfileSelection] = useState(false);
  const [profileAccounts, setProfileAccounts] = useState([]);
  const [selectedAccount, setSelectedAccount] = useState<{
    profileId: string;
    accountId: string;
    subAccountId: string;
    [key: string]: any;
  }>(null);
  const [profileOptions, setProfileOptions] = useState([]);
  const [selectedProfile, setSelectedProfile] = useState(null);
  const [submitting, setSubmitting] = useState(false);

  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );

  const { data: profileData, mutate: mutateList } = useRequest(
    API.INVESTOR_PROFILES_OF_USER
  );

  useEffect(() => {
    if (
      !profiles ||
      (Object.keys(profiles).length === 0 && profiles.constructor === Object)
    ) {
      dispatch(fetchProfilesList());
    }
  }, []);

  useEffect(() => {
    if (profileData) {
      const data: any[] = [];
      profileData.forEach((profile) => {
        if (profile.bankingUser.accounts) {
          profile.bankingUser.accounts.forEach((account) => {
            data.push({
              profileId: profile.id,
              name: getProfileName(profile),
              email: profile.email,
              profileType: profile.profileType,
              ...account,
            });
          });
        }
      });

      setProfileAccounts(data.sort((a, b) => (a.name < b.name ? -1 : 1)));
    }
  }, [profileData]);

  useEffect(() => {
    const options: any[] = Object.values(profiles)
      .filter((profile) => profile.profileType === "INVESTOR")
      .map((profile) => ({
        key: profile.id,
        value: profile.id,
        text: getProfileName(profile),
      }));
    setProfileOptions(options.sort((a, b) => (a.text < b.text ? -1 : 1)));
  }, [profiles]);

  const deleteHandler = async () => {
    try {
      setShowDelete(false);
      setSubmitting(true);
      if (
        !selectedAccount.profileId ||
        !selectedAccount.accountId ||
        !selectedAccount.subAccountId
      ) {
        toast.error(
          "Error occured in account deletion. Please try again later."
        );
        return;
      }

      await dispatch(
        removeBankingAccount(
          selectedAccount.profileId,
          selectedAccount.accountId,
          selectedAccount.subAccountId
        )
      );
      await dispatch(
        removeSubAccountById(
          selectedAccount.accountId,
          selectedAccount.subAccountId
        )
      );
      await dispatch(fetchPlaidAccountsList());
      await dispatch(fetchProfileById(selectedAccount.profileId));
      await mutateList();

      toast.success("Account was removed from profile.");
      setSubmitting(false);
    } catch (err) {
      toast.error("Account was not removed from profile.");
    }
  };

  const actionMenu = (rowData) => {
    return (
      <>
        <Tooltip
          title="Set this account as default (for distribution) for the linked profile"
          placement="bottom"
          arrow
        >
          <IconButton
            aria-label="status-button"
            data-testid="status-button"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              const profileRow = profileAccounts.filter(
                (account) =>
                  account.profileId === rowData[ID_COLUMN_INDEX] &&
                  account.plaidAccountId === rowData[ACCOUNT_ID_COLUMN_INDEX] &&
                  account.plaidSubAccountId ===
                    rowData[SUB_ACCOUNT_ID_COLUMN_INDEX]
              );
              if (profileRow && profileRow.length) {
                if (profileRow[0].isDefaultForDistributions) {
                  return;
                }
                setSelectedAccount({
                  profileId: rowData[ID_COLUMN_INDEX],
                  accountId: rowData[ACCOUNT_ID_COLUMN_INDEX],
                  subAccountId: rowData[SUB_ACCOUNT_ID_COLUMN_INDEX],
                  profileName: profileRow[0].name,
                  accountName: profileRow[0].accountName,
                });
                setShowDefault(true);
              }
            }}
            size="large"
          >
            <CheckIcon />
          </IconButton>
        </Tooltip>
        <IconButton
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setSelectedAccount({
              profileId: rowData[ID_COLUMN_INDEX],
              accountId: rowData[ACCOUNT_ID_COLUMN_INDEX],
              subAccountId: rowData[SUB_ACCOUNT_ID_COLUMN_INDEX],
            });
            setShowDelete(true);
          }}
          size="large"
        >
          <TrashIcon />
        </IconButton>
      </>
    );
  };

  const onDialogClose = () => {
    setSelectedProfile(null);
  };

  const formatDefault = (value) => {
    const colorMapping = {
      IsDefault: "green",
    };
    const statusMapping = {
      IsDefault:
        "The account is set as default for distributions for the linked profile",
    };
    return (
      <>
        <FormHint
          content={
            <div
              style={{
                padding: "8px",
              }}
            >
              {statusMapping[value ? "IsDefault" : ""] ??
                "Not default account for distributions"}
            </div>
          }
        >
          <div
            data-testid="status-color"
            style={{
              width: 20,
              height: 20,
              backgroundColor: colorMapping[value ? "IsDefault" : ""] ?? "gray",
              borderRadius: "50%",
              boxShadow: "0px 0px 4px rgba(0,0,0,0.5)",
            }}
          />
        </FormHint>
      </>
    );
  };

  const updateDefaultHandler = async () => {
    try {
      setShowDefault(false);
      setSubmitting(true);
      if (
        !selectedAccount.profileId ||
        !selectedAccount.accountId ||
        !selectedAccount.subAccountId
      ) {
        throw "Please try again later.";
      }

      const profile = await getProfileById(selectedAccount.profileId);
      const accounts = profile.bankingUser.accounts.map((account) => {
        if (
          account["plaidAccountId"] === selectedAccount.accountId &&
          account["plaidSubAccountId"] === selectedAccount.subAccountId
        ) {
          return {
            ...account,
            isDefaultForDistributions: true,
          };
        } else if (account.isDefaultForDistributions) {
          return {
            ...account,
            isDefaultForDistributions: false,
          };
        } else {
          return account;
        }
      });

      await dispatch(
        updateProfileById({
          id: profile.id,
          bankingUser: {
            accounts: [...accounts],
          },
        })
      );
      await dispatch(fetchProfileById(selectedAccount.profileId));
      await mutateList();

      toast.success(
        "Selected account set as default (for distributions) for profile."
      );
      setSubmitting(false);
    } catch (err) {
      toast.error(`Error occured while setting account as default.\n${err}`);
      setSubmitting(false);
    }
  };

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Card.Header>
            <Grid columns="equal">
              <Grid.Column>Accounts</Grid.Column>
              <Grid.Column textAlign="right">
                <Button
                  secondary
                  fluid
                  onClick={() => {
                    if (profileOptions && profileOptions.length) {
                      setShowProfileSelection(true);
                    } else {
                      toast.warn(
                        "User have no investor profiles created. Please create an investor profile to continue."
                      );
                    }
                  }}
                >
                  + Add a Bank Account
                </Button>
              </Grid.Column>
            </Grid>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <React.Fragment>
            {submitting ? (
              <Loader data-testid="loader" active inline="centered" />
            ) : (
              <>
                {!profileAccounts ? (
                  <NoProfileAccounts />
                ) : (
                  <TableWithTools
                    data={profileAccounts}
                    columnDefinitions={[
                      {
                        name: "profileId",
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: "plaidAccountId",
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: "plaidSubAccountId",
                        options: {
                          display: false,
                          filter: false,
                        },
                      },
                      {
                        name: "name",
                        label: "PROFILE NAME",
                      },
                      {
                        name: "email",
                        label: "EMAIL",
                      },
                      {
                        name: "profileType",
                        label: "TYPE",
                      },
                      {
                        name: "accountName",
                        label: "ACCOUNT",
                      },
                      {
                        name: "isDefaultForDistributions",
                        label: "IS DEFAULT",
                        // eslint-disable-next-line react/display-name
                        options: {
                          hint: "Whether the account is set as default for distributions for the linked profile",
                          filter: false,
                          // eslint-disable-next-line react/display-name
                          customBodyRender: (
                            value: any,
                            tableMeta: any,
                            updateValue: any
                          ) => {
                            return formatDefault(value);
                          },
                        },
                      },
                      {
                        name: "actions",
                        label: " ",
                        // eslint-disable-next-line react/display-name
                        options: {
                          filter: false,
                          // eslint-disable-next-line react/display-name
                          customBodyRender: (
                            value: any,
                            tableMeta: any,
                            updateValue: any
                          ) => {
                            return actionMenu(tableMeta.rowData);
                          },
                        },
                      },
                    ]}
                    selectable={false}
                    clickable
                  />
                )}
              </>
            )}
          </React.Fragment>
        </Card.Content>
      </Card>
      {showDelete && (
        <Modal open={showDelete} onClose={() => setShowDelete(false)} closeIcon>
          <Modal.Content>
            <Header as="h3" icon textAlign="center">
              <Icon name="exclamation circle" color="orange" />
              Are you sure you want to remove selected account?
            </Header>
            <Divider hidden />
            <Button
              secondary
              fluid
              onClick={deleteHandler}
              content="Remove Account"
            />
          </Modal.Content>
        </Modal>
      )}
      {showProfileSelection && (
        <Modal
          open={showProfileSelection}
          onClose={() => {
            setShowProfileSelection(false);
            setSelectedProfile(null);
          }}
          closeIcon
        >
          <Modal.Content>
            <Header as="h3" icon textAlign="center">
              <Icon name="exclamation circle" color="orange" />
              Please select an investor profile to link with the account.
            </Header>
            <Divider hidden />
            {profiles && (
              <Dropdown
                placeholder={"Investor Profile"}
                selection
                scrolling
                fluid
                options={profileOptions}
                onChange={(e, { value }) => {
                  return typeof value === "string" && setSelectedProfile(value);
                }}
              />
            )}
            <br />
            {selectedProfile && (
              <ProfileAccountCreateButton
                {...{
                  plaidLinkToken,
                  profileId: selectedProfile,
                  mutateList,
                  setShowProfileSelection,
                  dialogCloseHandler: onDialogClose,
                  setSubmitting,
                }}
              />
            )}
          </Modal.Content>
        </Modal>
      )}
      {showDefault && (
        <PopupModal
          open={showDefault}
          size="tiny"
          heading="Set as default"
          content={
            <>
              <p>
                Are you sure to set this account as default (for distributions)
                for the selected profile?
              </p>
              <br />
              <p>
                Profile: <b>{selectedAccount.profileName}</b>
                <br />
                Account to set as default: <b>{selectedAccount.accountName}</b>
              </p>
            </>
          }
          okcaption={"Confirm"}
          closecaption="Cancel"
          onClickOk={updateDefaultHandler}
          onClickClose={() => setShowDefault(false)}
        />
      )}
    </>
  );
};

export default LinkedAccountsTable;
