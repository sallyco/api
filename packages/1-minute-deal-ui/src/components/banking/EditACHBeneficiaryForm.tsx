import { Formik, Field } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { Button, Container, Divider, Form, Header } from "semantic-ui-react";
import { toast } from "react-toastify";
import { Alert } from "@mui/material";
import { FormField } from "../../forms/common/FormField";
import SubformPhone, {
  validationSchema as phoneValidation,
} from "../../forms/subforms/SubformPhone";
import {
  BankingBeneficiary,
  updateACHBankingBeneficiary,
  updateBankingBeneficiary,
} from "../../api/bankingApi";
import useSWR, { mutate } from "swr";
import { fetcher } from "../../swrConnector";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export default function EditACHBeneficiaryForm({
  beneficiaryId,
  type = "ach",
  onSuccessHandler = (beneficiary: BankingBeneficiary) => {},
  onErrorHandler = () => {},
}) {
  const [failureMessage, setFailureMessage] = useState<null | string>();

  const { data: initialData, error } = useSWR(
    () => baseUrl + `/banking/beneficiaries/${beneficiaryId}`,
    fetcher
  );

  const validation = Yup.object().shape({
    nameOnAccount: Yup.string().trim().required(),
    routingNumber: Yup.string()
      .length(9, "Must be a 9 digit routing number")
      .test("Digits", "Only numbers are allowed", (value) =>
        /^\d+$/.test(value)
      )
      .required(),
    accountNumber: Yup.string().required(),
    verifyAccountNumber: Yup.string()
      .oneOf([Yup.ref("accountNumber")], "Account numbers must match")
      .required(),
    email: Yup.string().email().required(),
    ...phoneValidation({ isRequired: false }),
    accountType: Yup.string().required(),
  });

  async function onSubmit(data) {
    try {
      const submitData = {
        ...data,
        ownerId: initialData.ownerId,
        tenantId: initialData.tenantId,
        serviceObjectId: initialData.serviceObjectId,
        email: data.email,
        phone: data.phone,
        name: data.nameOnAccount,
        nameOnAccount: data.nameOnAccount,
        accountNumber: data.accountNumber,
        routingNumber: data.routingNumber,
        accountType: (data.accountType ?? "").toUpperCase(),
      };
      const result = await updateACHBankingBeneficiary(
        beneficiaryId,
        submitData
      );
      if (!result.id) {
        throw new Error("Benficiary updation failed");
      }

      toast.success("Beneficiary was updated");
      mutate(baseUrl + `/banking/beneficiaries/${beneficiaryId}`);
      onSuccessHandler(result);
    } catch (err: any) {
      setFailureMessage(
        `Beneficiary was not saved: \n${
          err?.response?.data?.error?.message ?? err.message
        }`
      );
      onErrorHandler();
    }
  }

  return (
    <>
      {initialData && (
        <Container className="text">
          <Divider hidden />
          <Formik
            initialValues={{
              email: initialData.email,
              phone: initialData.phone,
              nameOnAccount: initialData.providerMeta?.name_on_account ?? "",
              routingNumber:
                initialData.providerMeta[type]?.routing_number ?? "",
              accountNumber:
                initialData.providerMeta[type]?.account_number ?? "",
              verifyAccountNumber: "",
              accountType: initialData.providerMeta[type]?.account_type ?? "",
            }}
            onSubmit={onSubmit}
            validationSchema={validation}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <FormField
                  name="email"
                  component={Form.Input}
                  label="Recipient Email"
                  placeholder="beneficiary@email.com"
                  required
                />
                <SubformPhone />

                <Divider hidden />
                <Header content="Account Information" />
                <FormField
                  name="routingNumber"
                  component={Form.Input}
                  label="ABA Routing Number"
                  placeholder="ABA Routing Number"
                  required
                />

                <FormField
                  name="nameOnAccount"
                  component={Form.Input}
                  label="Name on Account"
                  placeholder="Account Name"
                  required
                />
                <FormField
                  name={"accountNumber"}
                  placeholder={"Account Number"}
                  label="Account Number"
                  required
                />
                <FormField
                  name={"verifyAccountNumber"}
                  placeholder={"Verify Account Number"}
                  required
                />
                <Header content="Check or Savings" />

                <label>
                  <Field type="radio" name="accountType" value="checking" />
                  <span> Check </span>
                </label>
                <label>
                  <Field type="radio" name="accountType" value="savings" />
                  <span> Savings </span>
                </label>
                <Divider horizontal />
                {failureMessage && (
                  <>
                    <Alert
                      severity={"error"}
                      onClose={() => setFailureMessage(null)}
                    >
                      {failureMessage}
                    </Alert>
                    <Divider hidden />
                  </>
                )}
                <Button type="submit" primary fluid content="Add Beneficiary" />
              </Form>
            )}
          </Formik>
        </Container>
      )}
    </>
  );
}
