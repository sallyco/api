import { Formik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { Button, Container, Divider, Form, Header } from "semantic-ui-react";
import { toast } from "react-toastify";
import { Alert } from "@mui/material";

import { FormField } from "../../forms/common/FormField";
import SubformPhone, {
  validationSchema as phoneValidation,
} from "../../forms/subforms/SubformPhone";
import {
  BankingBeneficiary,
  BankingBeneficiaryRequest,
  updateBankingBeneficiary,
} from "../../api/bankingApi";
import useSWR, { mutate } from "swr";
import { fetcher } from "../../swrConnector";
import SubformAddress, {
  validationSchema,
} from "../../forms/subforms/SubformAddress";
import {
  getCodeByStateName,
  getStateNameByCode,
} from "../../tools/stateISO3166-2";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export default function EditWireBeneficiaryForm({
  beneficiaryId,
  type = "wire",
  onSuccessHandler = (beneficiary: BankingBeneficiary) => {},
  onErrorHandler = () => {},
}) {
  const [failureMessage, setFailureMessage] = useState<null | string>();

  const { data: initialData, error } = useSWR(
    () => baseUrl + `/banking/beneficiaries/${beneficiaryId}`,
    fetcher
  );

  const validation = Yup.object().shape({
    nameOnAccount: Yup.string().trim().required(),
    routingNumber: Yup.string()
      .length(9, "Must be a 9 digit routing number")
      .test("Digits", "Only numbers are allowed", (value) =>
        /^\d+$/.test(value)
      )
      .required(),
    accountNumber: Yup.string().required(),
    verifyAccountNumber: Yup.string()
      .oneOf([Yup.ref("accountNumber")], "Account numbers must match")
      .required(),
    bankAddress: Yup.object().shape({
      ...validationSchema(),
      address1: Yup.string()
        .min(2)
        .required("Address Line 1 is Required")
        .max(35, "Address is limited to 35 characters"),
    }),
    bankName: Yup.string().required(),
    name: Yup.string().required(),
    accountAddress: Yup.object().shape({
      ...validationSchema(),
      address1: Yup.string()
        .min(2)
        .required("Address Line 1 is Required")
        .max(35, "Address is limited to 35 characters"),
    }),
    email: Yup.string().email().required(),
    ...phoneValidation({ isRequired: false }),
  });

  async function onSubmit(data) {
    const { bankAddress, accountAddress, fixedCountry, bankName } = data;

    const submitData: BankingBeneficiaryRequest = {
      ...data,
      ownerId: initialData.ownerId,
      tenantId: initialData.tenantId,
      serviceObjectId: initialData.serviceObjectId,
      type,
      bankAddress: {
        street_1: bankAddress?.address1,
        street_2: bankAddress?.address2,
        city: bankAddress?.city,
        postal_code: bankAddress?.postalCode,
        region: getCodeByStateName(bankAddress?.state),
        country: fixedCountry ? "US" : bankAddress?.country,
      },
      addressOnAccount: {
        street_1: accountAddress?.address1,
        street_2: accountAddress?.address2,
        city: accountAddress?.city,
        postal_code: accountAddress?.postalCode,
        region: getCodeByStateName(accountAddress?.state),
        country: fixedCountry ? "US" : accountAddress?.country,
      },
    };

    try {
      const result = await updateBankingBeneficiary(beneficiaryId, submitData);
      if (!result.id) {
        throw new Error("Benficiary updation failed");
      }

      toast.success("Beneficiary was updated");
      mutate(baseUrl + `/banking/beneficiaries/${beneficiaryId}`);
      onSuccessHandler(result);
    } catch (err: any) {
      setFailureMessage(
        `Beneficiary was not updated: \n${
          err?.response?.data?.error?.message ?? err.message
        }`
      );
      onErrorHandler();
    }
  }

  return (
    <>
      {initialData && (
        <Container className="text">
          <Divider hidden />
          <Formik
            initialValues={{
              name: initialData?.name ?? "",
              email: initialData.email ?? "",
              phone: initialData.phone ?? "",
              bankName: initialData.providerMeta[type]?.bank_name ?? "",
              nameOnAccount: initialData.providerMeta?.name_on_account ?? "",
              routingNumber:
                initialData.providerMeta[type]?.routing_number ?? "",
              accountNumber:
                initialData.providerMeta[type]?.account_number ?? "",
              verifyAccountNumber: "",
              bankAddress: {
                address1:
                  initialData.providerMeta[type]?.bank_address?.street_line_1 ??
                  "",
                address2:
                  initialData.providerMeta[type]?.bank_address?.street_line_2 ??
                  "",
                city: initialData.providerMeta[type]?.bank_address?.city ?? "",
                state:
                  getStateNameByCode(
                    initialData.providerMeta[type]?.bank_address?.state
                  ) ?? "",
                postalCode:
                  initialData.providerMeta[type]?.bank_address?.postal_code ??
                  "",
                country: "United States of America",
                fixedCountry: false,
              },
              accountAddress: {
                address1:
                  initialData.providerMeta[type]?.address_on_account
                    ?.street_line_1 ?? "",
                address2:
                  initialData.providerMeta[type]?.address_on_account
                    ?.street_line_2 ?? "",
                city:
                  initialData.providerMeta[type]?.address_on_account?.city ??
                  "",
                state:
                  getStateNameByCode(
                    initialData.providerMeta[type]?.address_on_account?.state
                  ) ?? "",
                postalCode:
                  initialData.providerMeta[type]?.address_on_account
                    ?.postal_code ?? "",
                country: "United States of America",
                fixedCountry: false,
              },
              fixedCountry: true,
            }}
            onSubmit={onSubmit}
            validationSchema={validation}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <FormField
                  name="name"
                  component={Form.Input}
                  label="Beneficiary Name"
                  placeholder="Name"
                  required
                />
                <FormField
                  name="email"
                  component={Form.Input}
                  label="Recipient Email"
                  placeholder="beneficiary@email.com"
                  required
                />
                <SubformPhone />

                <Divider hidden />
                <Header content="Bank Information" />
                <FormField
                  name="bankName"
                  component={Form.Input}
                  label="Bank Name"
                  placeholder="My Bank"
                  required
                />
                <FormField
                  name={"routingNumber"}
                  placeholder={"Routing Number"}
                  label="Routing Number"
                  required
                />
                <SubformAddress
                  namespace="bankAddress"
                  fieldsRequired={true}
                  {...props}
                />

                <Divider hidden />
                <Header content="Account Information" />
                <FormField
                  name="nameOnAccount"
                  component={Form.Input}
                  label="Name on Account"
                  placeholder="Account Name"
                  required
                />
                <FormField
                  name={"accountNumber"}
                  placeholder={"Account Number"}
                  label="Account Number"
                  required
                />
                <FormField
                  name={"verifyAccountNumber"}
                  placeholder={"Verify Account Number"}
                  required
                />
                <SubformAddress
                  namespace="accountAddress"
                  fieldsRequired={true}
                  {...props}
                />
                {failureMessage && (
                  <>
                    <Alert
                      severity={"error"}
                      onClose={() => setFailureMessage(null)}
                    >
                      {failureMessage}
                    </Alert>
                    <Divider hidden />
                  </>
                )}
                <Button
                  type="submit"
                  primary
                  fluid
                  content="Edit Beneficiary"
                />
              </Form>
            )}
          </Formik>
        </Container>
      )}
    </>
  );
}
