import React, { useCallback } from "react";
import { usePlaidLink } from "react-plaid-link";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button } from "semantic-ui-react";

import { convertToAccount } from "../../api/plaidApi";
import { fetchPlaidAccountsList } from "../../slices/accountsSlice";
import {
  fetchProfileById,
  insertBankingAccount,
} from "../../slices/profilesSlice";

interface ProfileAccountCreateButtonProps {
  plaidLinkToken: any;
  profileId: string;
  dialogCloseHandler: (() => void) | null;
  mutateList: () => Promise<any>;
  setShowProfileSelection: any;
  setSubmitting: any;
}

export default function ProfileAccountCreateButton({
  plaidLinkToken,
  profileId,
  mutateList,
  setShowProfileSelection,
  dialogCloseHandler = null,
  setSubmitting,
}: ProfileAccountCreateButtonProps) {
  const dispatch = useDispatch();

  const onSuccess = useCallback(async (token, metadata) => {
    setShowProfileSelection(false);
    setSubmitting(true);

    if (!profileId) {
      toast.error(
        "Error occured in profile selection. Cannot add account now."
      );
      return;
    }

    const newAccount = await convertToAccount(metadata);
    if (newAccount && newAccount.data) {
      const accounts = newAccount.data.plaidData.accounts;
      if (accounts && accounts.length > 0) {
        for (const account of accounts) {
          // eslint-disable-next-line no-await-in-loop
          await dispatch(
            insertBankingAccount(profileId, newAccount.data.id, account.id)
          );
        }
      }
    }

    await dispatch(fetchPlaidAccountsList());
    await dispatch(fetchProfileById(profileId));
    mutateList();
    setSubmitting(false);
    dialogCloseHandler();
  }, []);

  const onExit = useCallback((error, metaData) => {
    setShowProfileSelection(false);
    dialogCloseHandler();
  }, []);

  const config = {
    token: plaidLinkToken,
    onSuccess,
    onExit,
  };

  const { open, ready, error, exit } = usePlaidLink(config);

  return (
    <Button
      secondary
      fluid
      onClick={() => {
        if (profileId) {
          open();
        }
      }}
      content="Proceed"
    />
  );
}
