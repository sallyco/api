import React, { useState } from "react";
import { useSelector } from "react-redux";
import { TableWithTools } from "@packages/gbt-ui";
import useSWR from "swr";
import moment from "moment";
import numeral from "numeral";
import { Loader } from "semantic-ui-react";

import { fetcher } from "../../swrConnector";
import { RootState } from "../../rootReducer";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

const TransactionLogTable = () => {
  const { self } = useSelector((state: RootState) => state.users);

  const { data: transactionLogs, error } = useSWR(
    () => baseUrl + `/transactions-log/${self.id}`,
    fetcher
  );

  return (
    <>
      {!transactionLogs ? (
        <Loader active inline="centered" />
      ) : (
        <TableWithTools
          data={transactionLogs}
          columnDefinitions={[
            {
              name: "id",
              options: {
                display: false,
                filter: false,
              },
            },
            {
              name: "type",
              label: "TYPE",
            },
            {
              name: "beneficiary.name",
              label: "BENEFICIARY NAME",
            },
            {
              name: "beneficiary.accountNumber",
              label: "ACCOUNT #",
            },
            {
              name: "createdAt",
              label: "CREATED AT",
              options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                  moment(value).format("LL"),
              },
            },
            {
              name: "direction",
              label: "DIRECTION",
            },
            {
              name: "amount",
              label: "AMOUNT",
              options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                  numeral(value).format("$0,0"),
              },
            },
            {
              name: "status",
              label: "STATUS",
            },
            {
              name: "reason",
              label: "REASON",
            },
          ]}
          onRowClick={(r, m) => {}}
          selectable={false}
          clickable
          enableNestedDataAccess={"."}
        />
      )}
    </>
  );
};

export default TransactionLogTable;
