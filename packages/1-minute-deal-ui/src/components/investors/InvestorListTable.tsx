import React, { useState } from "react";
import { TableWithTools } from "@packages/gbt-ui";
import numeral from "numeral";
import { PopupModal } from "../common/PopupModal";
import { InvestorForUser } from "../../api/subscriptionsApi";
import InvestorDetail from "./InvestorDetail";

interface Props {
  ownerId: string;
  investors: InvestorForUser[];
}

const InvestorListTable = ({ ownerId, investors }: Props) => {
  const [showDetail, setShowDetail] = useState(false);
  const [selectedInvestor, setSelectedInvestor] = useState(null);

  return (
    <>
      <TableWithTools
        data={investors}
        columnDefinitions={[
          {
            name: "investorId",
            options: {
              display: false,
              filter: false,
            },
          },
          {
            name: "investorName",
            label: "INVESTOR NAME",
          },
          {
            name: "investorEmail",
            label: "EMAIL",
          },
          {
            name: "investorType",
            label: "TYPE OF INVESTOR",
          },
          {
            name: "entityName",
            label: "ENTITY NAME",
          },
          {
            name: "subscriptionCount",
            label: "NO: OF SUBSCRIPTIONS",
          },
          {
            name: "amountSubscribed",
            label: "TOTAL AMOUNT",
            options: {
              customBodyRender: (value, tableMeta, updateValue) =>
                numeral(value).format("$0,0"),
            },
          },
        ]}
        onRowClick={(r, m) => {
          setSelectedInvestor(r[0]);
          setShowDetail(true);
        }}
        selectable={false}
        clickable
      />
      <PopupModal
        open={showDetail}
        size="fullscreen"
        heading="Investor Details"
        content={
          <>
            <InvestorDetail investorId={selectedInvestor} ownerId={ownerId} />
          </>
        }
        onClickClose={() => setShowDetail(false)}
      />
    </>
  );
};

export default InvestorListTable;
