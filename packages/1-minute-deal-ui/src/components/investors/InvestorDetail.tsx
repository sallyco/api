import React, { useState, useEffect } from "react";
import { Header, Table, Label, Divider } from "semantic-ui-react";
import moment from "moment";
import numeral from "numeral";
import { TableWithTools } from "@packages/gbt-ui";
import useSWR from "swr";
import { fetcher } from "../../swrConnector";
import {
  DealStatusColors as dsc,
  SubscriptionStatusColors as ssc,
} from "../../tools/statusColors";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

const getAddressString = (address) => {
  if (address) {
    let result = "";
    result += address.address1 ? address.address1 : "";
    result += address.address2 ? ", " + address.address2 : "";
    result += address.city ? ", " + address.city : "";
    result += address.state ? ", " + address.state : "";
    result += address.country ? ", " + address.country : "";
    result += address.postalCode ? ", Postal Code: " + address.postalCode : "";
    return result;
  }
  return "";
};

interface Props {
  ownerId: string;
  investorId: string;
}

const InvestorDetail = ({ ownerId, investorId }: Props) => {
  const [investorDetail, setInvestorDetail] = useState(null);

  const { data: investorSubscriptions, error: error } = useSWR(
    () => baseUrl + `/investors/${ownerId}/${investorId}`,
    fetcher
  );

  useEffect(() => {
    if (investorSubscriptions && investorSubscriptions[0]) {
      setInvestorDetail({
        name: investorSubscriptions[0].name,
        email: investorSubscriptions[0].email,
        phone: investorSubscriptions[0].phone,
        address: getAddressString(investorSubscriptions[0].address),
        investorId: investorSubscriptions[0].profileId,
        investorType: investorSubscriptions[0].investorType,
        entityName: investorSubscriptions[0].entityName,
      });
    }
  }, [investorSubscriptions]);

  return (
    <>
      {investorDetail && (
        <>
          <Table definition basic compact size="small" unstackable>
            <Table.Body>
              <Table.Row>
                <Table.Cell>Name of Investor</Table.Cell>
                <Table.Cell>{investorDetail.name}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Email</Table.Cell>
                <Table.Cell>{investorDetail.email}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Phone</Table.Cell>
                <Table.Cell>{investorDetail.phone}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Type of Investor</Table.Cell>
                <Table.Cell>{investorDetail.investorType}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Entity Name</Table.Cell>
                <Table.Cell>{investorDetail.entityName}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Address</Table.Cell>
                <Table.Cell>{investorDetail.address}</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <Divider />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              paddingBottom: "8px",
            }}
          >
            <Header size="medium">Deals</Header>
          </div>
          <TableWithTools
            data={investorSubscriptions}
            columnDefinitions={[
              {
                name: "dealId",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "subscriptionId",
                options: {
                  display: false,
                  filter: false,
                },
              },
              {
                name: "dealName",
                label: "DEAL NAME",
              },
              {
                name: "dealStatus",
                label: "DEAL STATUS",
                options: {
                  // eslint-disable-next-line react/display-name
                  customBodyRender: (dealStatus, tableMeta, updateValue) => (
                    <Label color={dsc(dealStatus)}>{dealStatus}</Label>
                  ),
                },
              },
              {
                name: "subscribedOn",
                label: "SUBSCRIBED ON",
                options: {
                  customBodyRender: (subscribedOn, tableMeta, updateValue) =>
                    moment(subscribedOn).format("LL"),
                },
              },
              {
                name: "subscriptionStatus",
                label: "SUBSCRIPTION STATUS",
                options: {
                  // eslint-disable-next-line react/display-name
                  customBodyRender: (
                    subscriptionStatus,
                    tableMeta,
                    updateValue
                  ) => (
                    <Label color={ssc(subscriptionStatus)}>
                      {subscriptionStatus}
                    </Label>
                  ),
                },
              },
              {
                name: "subscriptionAmount",
                label: "AMOUNT",
                options: {
                  customBodyRender: (
                    subscriptionAmount,
                    tableMeta,
                    updateValue
                  ) => numeral(subscriptionAmount).format("$0,0"),
                },
              },
            ]}
            selectable={false}
          />
        </>
      )}
    </>
  );
};

export default InvestorDetail;
