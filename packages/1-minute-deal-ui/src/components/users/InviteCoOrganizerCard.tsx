import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import * as Yup from "yup";
import { Card, Form, Button, Loader } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";

export const InviteCoOrganizerCard = () => {
  const { currentPageProfiles, isLoading, isSubmitting, profilesById } =
    useSelector((state: RootState) => state.profiles);

  const profiles = currentPageProfiles.map(
    (profileId) => profilesById[profileId]
  );

  async function onSubmit(data) {
    console.log("Submit data", data);
  }

  const validation = Yup.object().shape({
    firstName: Yup.string()

      .trim()
      .required(),
    lastName: Yup.string().trim().required(),
    email: Yup.string().trim().required(),
  });

  return (
    <Card color="grey" fluid>
      <Card.Content>
        <Card.Header>Invite Co-Organizer</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !profiles ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <>
            <Formik
              initialValues={{}}
              validationSchema={validation}
              onSubmit={onSubmit}
            >
              {(props) => (
                <Form onSubmit={props.handleSubmit} className="dark-labels">
                  <FormField
                    name="firstName"
                    component={Form.Input}
                    label="Co-organizer First Name"
                    placeholder="First Name"
                    required
                  />
                  <FormField
                    name="lastName"
                    component={Form.Input}
                    label="Co-organizer Last Name"
                    placeholder="Last Name"
                    required
                  />

                  <FormField
                    name="email"
                    component={Form.Input}
                    label="Co-organizer Email"
                    placeholder="johndoe@email.com"
                    required
                  />

                  <Button
                    secondary
                    fluid
                    content="Invite"
                    type="submit"
                    loading={isSubmitting}
                  />
                </Form>
              )}
            </Formik>
          </>
        )}
      </Card.Content>
    </Card>
  );
};
