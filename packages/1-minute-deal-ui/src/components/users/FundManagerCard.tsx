import React, { useContext, useState } from "react";
import { useSelector } from "react-redux";
import { AuthContext } from "../../contexts";
import { RootState } from "../../rootReducer";
import { Formik } from "formik";
import {
  Card,
  Form,
  Loader,
  Input,
  Divider,
  Table,
  Pagination,
} from "semantic-ui-react";
import { FormField } from "../../components/FormField";
import Avatar from "react-avatar";

export const FundManagerCard = () => {
  const userProfile = useContext(AuthContext);
  const { isLoading, self } = useSelector((state: RootState) => state.users);
  async function onSubmit(data) {}
  const tableData = [
    {
      name: "Fund Manger1",
      avtar: (
        <Avatar
          name={`${userProfile.given_name} ${userProfile.family_name}`}
          email={userProfile.email}
          size="34px"
          round={true}
          color="teal"
        />
      ),
      check: <FormField name="check" component={Form.Checkbox} />,
    },
    {
      name: "Fund Manger2",
      avtar: (
        <Avatar
          name={`${userProfile.given_name} ${userProfile.family_name}`}
          email={userProfile.email}
          size="34px"
          round={true}
          color="teal"
        />
      ),
      check: <FormField name="check" component={Form.Checkbox} />,
    },
    {
      name: "Fund Manger3",
      avtar: (
        <Avatar
          name={`${userProfile.given_name} ${userProfile.family_name}`}
          email={userProfile.email}
          size="34px"
          round={true}
          color="teal"
        />
      ),
      check: <FormField name="check" component={Form.Checkbox} />,
    },
    {
      name: "Fund Manger4",
      avtar: (
        <Avatar
          name={`${userProfile.given_name} ${userProfile.family_name}`}
          email={userProfile.email}
          size="34px"
          round={true}
          color="teal"
        />
      ),
      check: <FormField name="check" component={Form.Checkbox} />,
    },
  ];

  const [data, setData] = useState(tableData);
  const [activePage, setActivePage] = useState(1);

  const onChange = (e, pageInfo) => {
    setActivePage(pageInfo.activePage);
    if (pageInfo.activePage === 1) {
      setData(tableData.slice(0, 2));
    } else {
      setData(tableData.slice(2, 4));
    }
  };

  return (
    <Card fluid>
      <Card.Content>
        <Input
          size="small"
          icon="search"
          iconPosition="left"
          placeholder="Search..."
        />

        <Divider hidden />
        <Card.Header>Fund Managers</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !self ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <Formik initialValues={{}} onSubmit={onSubmit}>
            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>
                    <FormField name="check" component={Form.Checkbox} />
                  </Table.HeaderCell>
                  <Table.HeaderCell></Table.HeaderCell>
                  <Table.HeaderCell>Name</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {data.slice(0, 2).map(({ check, avtar, name }) => (
                  <Table.Row key={name}>
                    <Table.Cell>{check}</Table.Cell>
                    <Table.Cell>{avtar}</Table.Cell>
                    <Table.Cell>{name}</Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </Formik>
        )}
        <Pagination
          boundaryRange={0}
          defaultActivePage={1}
          activePage={activePage}
          totalPages={2}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          pointing
          secondary
          onPageChange={onChange}
        />
      </Card.Content>
    </Card>
  );
};
export default FundManagerCard;
