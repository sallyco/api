import { getProfileName } from "../../api/profileApiWrapper";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { List, Card, Transition, Container, Dropdown } from "semantic-ui-react";

import { RootState } from "../../rootReducer";

import { fetchProfilesList } from "../../slices/profilesSlice";

export const ProfilesTab = () => {
  const dispatch = useDispatch();
  const [selectedProfileId, setSelectedProfileId] = useState("");
  //PROFILES
  const { currentPageProfiles, isLoading, profilesById } = useSelector(
    (state: RootState) => state.profiles
  );

  const profiles = currentPageProfiles
    .map((profileId) => profilesById[profileId])
    .filter((profile) => profile.profileType === "INVESTOR");

  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  return (
    <Transition animation={"fade up"}>
      <Container>
        <Card.Group itemsPerRow="two" stackable>
          <Card fluid>
            <Card.Content>
              <Card.Header>Create and edit your investing profiles</Card.Header>
              <Card.Meta>
                Profiles such as an individual, LLC, and Trusts are used to
                invest in deals
              </Card.Meta>
            </Card.Content>
            <Card.Content>
              <Dropdown
                fluid
                selection
                search
                placeholder="Select a Profile"
                loading={isLoading}
                options={profiles.map((profile) => ({
                  value: profile.id,
                  text: getProfileName(profile),
                }))}
                onChange={(e, data) => {
                  if (data.value && typeof data.value === "string")
                    setSelectedProfileId(data.value);
                }}
              />
              {selectedProfileId && (
                <List>
                  <List.Item style={{ color: "black" }}>
                    <List.Icon name="university" />
                    <List.Content>
                      KYC/AML Status:{" "}
                      {profilesById[selectedProfileId].kycAml?.result ??
                        "Not Verified"}
                    </List.Content>
                  </List.Item>
                </List>
              )}
            </Card.Content>
          </Card>
        </Card.Group>
      </Container>
    </Transition>
  );
};
