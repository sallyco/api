import React from "react";
import { Tab, Card, Button, Grid } from "semantic-ui-react";
import { FundManagerCard } from "./FundManagerCard";
import { MasterEntityCard } from "./MasterEntityCard";

export const EntityCard = () => {
  const panes = [
    {
      menuItem: "Fund Mangers",
      style: { color: "blue" },
      pane: (
        <Tab.Pane inverted attached={false}>
          <FundManagerCard />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "Master Entities",
      style: { color: "blue" },
      pane: (
        <Tab.Pane inverted attached={false}>
          <MasterEntityCard />
        </Tab.Pane>
      ),
    },
  ];

  return (
    <>
      <Card color="grey" fluid>
        <Card.Content>
          <Card.Header>Your account settings</Card.Header>
        </Card.Content>
        <Card.Content>
          <>
            <Tab
              menu={{ secondary: true, pointing: true, className: "nav-bar" }}
              panes={panes}
              renderActiveOnly={false}
            />

            <Grid textAlign="center">
              <Grid.Column>
                <Button secondary type="submit" fluid>
                  + Add Fund Manager
                </Button>
              </Grid.Column>
            </Grid>
          </>
        </Card.Content>
      </Card>
    </>
  );
};
