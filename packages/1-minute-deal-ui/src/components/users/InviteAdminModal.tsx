import React, { useContext } from "react";
import { Button, Form, Modal } from "semantic-ui-react";
import { useDispatch } from "react-redux";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";
import * as Yup from "yup";
import { insertInvite } from "../../slices/invitesSlice";
import { sendInviteAccountAdminUser } from "../../api/emailsApi";
import { AuthContext } from "../../contexts";

interface Props {
  open: boolean;
  changeOpen: any;
}

export const InviteAdminModal = ({ open, changeOpen }: Props) => {
  const dispatch = useDispatch();
  const userProfile = useContext(AuthContext);

  const validation = Yup.object().shape({
    firstName: Yup.string().trim(),
    lastName: Yup.string().trim(),
    email: Yup.string().email().trim().required(),
    phone: Yup.string().trim(),
    linkedinURL: Yup.string().trim(),
  });

  const handleSubmit = async (data) => {
    let invite: any = await dispatch(
      insertInvite({
        email: data.email,
        type: "user",
        relation: "account-admin",
        role: "investor",
      })
    );
    await sendInviteAccountAdminUser(
      invite.acceptableBy,
      userProfile.email,
      data,
      invite
    );
    changeOpen(false);
  };

  return (
    <>
      <Formik
        initialValues={{}}
        validationSchema={validation}
        onSubmit={handleSubmit}
      >
        {(props) => (
          <Form onSubmit={props.handleSubmit} className="dark-labels">
            <Modal
              open={open}
              onOpen={() => changeOpen(true)}
              onClose={() => changeOpen(false)}
            >
              <Modal.Header>
                Invite User to Administer Your Account
              </Modal.Header>
              <Modal.Content>
                <Form.Group unstackable widths={"equal"}>
                  <FormField
                    fluid
                    label={"First Name"}
                    name="firstName"
                    component={Form.Input}
                    placeholder="First Name"
                  />
                  <FormField
                    fluid
                    label={"Last Name"}
                    name="lastName"
                    component={Form.Input}
                    placeholder="Last Name"
                  />
                  <FormField
                    fluid
                    label={"Email"}
                    name="email"
                    component={Form.Input}
                    placeholder="Email"
                    required
                  />
                  <FormField
                    fluid
                    label={"Phone"}
                    name="phone"
                    component={Form.Input}
                    placeholder="Phone"
                  />
                  <FormField
                    fluid
                    label={"LinkedIn URL"}
                    name="linkedinURL"
                    component={Form.Input}
                    placeholder="LinkedIn URL"
                  />
                </Form.Group>
              </Modal.Content>
              <Modal.Actions>
                <Button
                  negative
                  content={"CANCEL"}
                  onClick={() => changeOpen(false)}
                />
                <Button
                  secondary
                  type="submit"
                  content="INVITE"
                  onClick={props.submitForm}
                />
              </Modal.Actions>
            </Modal>
          </Form>
        )}
      </Formik>
    </>
  );
};
