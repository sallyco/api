import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";

import { Card, Icon, Loader, Header } from "semantic-ui-react";
export const OrganizerLogoCard = () => {
  const { currentPageProfiles, isLoading, profilesById } = useSelector(
    (state: RootState) => state.profiles
  );

  const profiles = currentPageProfiles.map(
    (profileId) => profilesById[profileId]
  );

  return (
    <Card color="grey" fluid>
      <Card.Content>
        <Card.Header>Organizer Logo</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !profiles ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <>
            <Header color="teal">Upload Organizer Logo</Header>
            <Icon.Group size="huge">
              <Icon color="teal" name="image outline" />
              <Icon
                corner
                color="teal"
                name="arrow alternate circle up outline"
              ></Icon>
            </Icon.Group>
          </>
        )}
      </Card.Content>
    </Card>
  );
};
