import React from "react";

import Log from "../../tools/Log";

import {
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Table,
  Button,
  Label,
  Container,
  Segment,
  Item,
  Transition,
  Loader,
  Card,
} from "semantic-ui-react";

import moment from "moment";

export const SystemInformationCard = () => {
  const buildDate =
    process.env.BUILD_DATE ??
    process.env.NEXT_PUBLIC_BUILD_DATE ??
    new Date().toISOString();

  return (
    <Card color="grey">
      <Card.Content>
        <Card.Header>System Information</Card.Header>
      </Card.Content>
      <Card.Content>
        <Header size="tiny">Application</Header>
        <Table basic="very" compact size="small" unstackable>
          <Table.Body>
            {(process.env.ENVIRONMENT ??
              process.env.NEXT_PUBLIC_ENVIRONMENT) && (
              <Table.Row>
                <Table.Cell>Environment</Table.Cell>
                <Table.Cell>
                  <Label>
                    {process.env.ENVIRONMENT ??
                      process.env.NEXT_PUBLIC_ENVIRONMENT}
                  </Label>
                </Table.Cell>
              </Table.Row>
            )}

            {(process.env.BUILD_NAME ?? process.env.NEXT_PUBLIC_BUILD_NAME) && (
              <Table.Row>
                <Table.Cell>Build Name</Table.Cell>
                <Table.Cell>
                  {process.env.BUILD_NAME ?? process.env.NEXT_PUBLIC_BUILD_NAME}
                </Table.Cell>
              </Table.Row>
            )}

            {(process.env.BUILD_VERSION ??
              process.env.NEXT_PUBLIC_BUILD_VERSION) &&
              (process.env.ENVIRONMENT ??
                process.env.NEXT_PUBLIC_ENVIRONMENT) !== "production" && (
                <Table.Row>
                  <Table.Cell>Build Version</Table.Cell>
                  <Table.Cell>
                    {process.env.BUILD_VERSION ??
                      process.env.NEXT_PUBLIC_BUILD_VERSION}
                  </Table.Cell>
                </Table.Row>
              )}

            {(process.env.BUILD_NOTE ?? process.env.NEXT_PUBLIC_BUILD_NOTE) &&
              (process.env.ENVIRONMENT ??
                process.env.NEXT_PUBLIC_ENVIRONMENT) !== "production" && (
                <Table.Row>
                  <Table.Cell>Release Note</Table.Cell>
                  <Table.Cell>
                    {process.env.BUILD_NOTE ??
                      process.env.NEXT_PUBLIC_BUILD_NOTE}
                  </Table.Cell>
                </Table.Row>
              )}

            <Table.Row>
              <Table.Cell>Build Date</Table.Cell>
              <Table.Cell>
                {moment.utc(buildDate).local().format("lll")} (
                {moment.utc(buildDate).local().fromNow()})
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        {/*
        <Header size='tiny'>API</Header>
        <Table basic="very" compact size="small" unstackable>
          <Table.Body>

          { (process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT) &&
            <Table.Row>
              <Table.Cell>Environment</Table.Cell>
              <Table.Cell><Label>{(process.env.ENVIRONMENT ?? process.env.NEXT_PUBLIC_ENVIRONMENT)}</Label></Table.Cell>
            </Table.Row>
          }

          { process.env.BUILD_NAME ?? process.env.NEXT_PUBLIC_BUILD_NAME &&
            <Table.Row>
              <Table.Cell>Build Name</Table.Cell>
              <Table.Cell>{process.env.BUILD_NAME ?? process.env.NEXT_PUBLIC_BUILD_NAME}</Table.Cell>
            </Table.Row>
          }

          { process.env.BUILD_VERSION ?? process.env.NEXT_PUBLIC_BUILD_VERSION &&
            <Table.Row>
              <Table.Cell>Build Version</Table.Cell>
              <Table.Cell>{process.env.BUILD_VERSION ?? process.env.NEXT_PUBLIC_BUILD_VERSION}</Table.Cell>
            </Table.Row>
          }

          { process.env.BUILD_NOTE ?? process.env.NEXT_PUBLIC_BUILD_NOTE &&
            <Table.Row>
              <Table.Cell>Release Note</Table.Cell>
              <Table.Cell>{process.env.BUILD_NOTE ?? process.env.NEXT_PUBLIC_BUILD_NOTE}</Table.Cell>
            </Table.Row>
          }

          <Table.Row>
            <Table.Cell>Build Date</Table.Cell>
            <Table.Cell>
              {moment.utc(buildDate).local().format("lll")} ({moment.utc(buildDate).local().fromNow()})
            </Table.Cell>
          </Table.Row>

          </Table.Body>
        </Table>
*/}
      </Card.Content>
    </Card>
  );
};
