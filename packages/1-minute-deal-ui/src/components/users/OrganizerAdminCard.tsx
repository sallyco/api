import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import * as Yup from "yup";
import {
  Card,
  Form,
  Button,
  Dropdown,
  Loader,
  Divider,
  Segment,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";
import { toast } from "react-toastify";
import Log from "../../tools/Log";
import {
  fetchProfilesList,
  updateProfileById,
} from "../../slices/profilesSlice";
import SubformState from "../../forms/subforms/SubformState";
import SubformCountry from "../../forms/subforms/SubformCountry";
import { getProfileName } from "../../api/profileApiWrapper";

export const OrganizerAdminCard = () => {
  const dispatch = useDispatch();

  const [selectedProfileId, setSelectedProfileId]: any = useState(null);
  const { currentPageProfiles, isLoading, isSubmitting, profilesById } =
    useSelector((state: RootState) => state.profiles);

  const entityTypes = [
    {
      key: "1",
      value: "LIMITED_LIABILITY_COMPANY",
      text: "Limited Liability Company",
    },
    { key: "2", value: "LIMITED_PARTNERSHIP", text: "Limited Partnership" },
    { key: "3", value: "C_CORPORATION", text: "C Corporation" },
    { key: "4", value: "S_CORPORATION", text: "S Corporation" },
    { key: "5", value: "GENERAL_PARTNERSHIP", text: "General Partnership" },
    { key: "6", value: "501_C_NONPROFIT", text: "501(c) Non-Profit" },
    { key: "7", value: "FOREIGN_ENTITY", text: "Foreign Entity" },
  ];

  const eraStatus = [
    {
      key: "1",
      value: "Apply_To_Me",
      text: "This Applies To Me",
    },
    {
      key: "2",
      value: "Doesnt_Apply_To_Me",
      text: "This Doesn't Apply To Me",
    },
  ];

  const profiles = currentPageProfiles.map(
    (profileId) => profilesById[profileId]
  );

  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  async function onSubmit(data) {
    Log.debug("data", data);
    await dispatch(
      updateProfileById({
        ...profilesById[selectedProfileId],
        firstName: data.firstName,
        lastName: data.lastName,
        typeOfEntity: data.typeOfEntity,
        countryOfFormation: data.countryOfFormation,
        stateOfFormation: data.stateOfFormation,
      })
    );
    toast.success(`Account updated successfully`);
  }

  const validation = Yup.object().shape({
    // type: Yup.string().required("Generate Investor Signing Docs is required"),
    // typeOfEntity: Yup.string().trim().required(),
    // organizerName: Yup.string().trim().required(),
    firstName: Yup.string()

      .trim()
      .required(),
    lastName: Yup.string().trim().required(),
    email: Yup.string().trim().required(),
    countryOfFormation: Yup.string()

      .trim()
      .required(),
    stateOfFormation: Yup.string()

      .trim()
      .required(),
    // eraStatus: Yup.string().trim().required(),
  });

  return (
    <Card color="grey" fluid>
      <Card.Content>
        <Card.Header>Organizer Administration</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !profiles ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <>
            <Dropdown
              fluid
              selection
              options={profiles
                .filter((profile) => profile.profileType === "ORGANIZER")
                .map((profile) => ({
                  value: profile.id,
                  text: getProfileName(profile),
                }))}
              search
              placeholder="Select a Profile"
              onChange={(e, data) => {
                if (data.value && typeof data.value === "string")
                  setSelectedProfileId(data.value);
              }}
            />
            <Divider hidden />
            {selectedProfileId && (
              <Formik
                initialValues={{
                  firstName: profilesById[selectedProfileId]?.firstName,
                  lastName: profilesById[selectedProfileId]?.lastName,
                  email: profilesById[selectedProfileId]?.email,
                  typeOfEntity: profilesById[selectedProfileId]?.typeOfEntity,
                  countryOfFormation:
                    profilesById[selectedProfileId]?.countryOfFormation ||
                    profilesById[selectedProfileId]?.address?.country,
                  stateOfFormation:
                    profilesById[selectedProfileId]?.stateOfFormation ||
                    profilesById[selectedProfileId]?.address?.state,
                }}
                enableReinitialize={true}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form onSubmit={props.handleSubmit} className="dark-labels">
                    {/*<FormField*/}
                    {/*  name="typeOfEntity"*/}
                    {/*  component={Form.Select}*/}
                    {/*  label="Organizer Entity Type"*/}
                    {/*  options={entityTypes}*/}
                    {/*  value={profilesById[selectedProfileId].typeOfEntity}*/}
                    {/*  placeholder="Select a Type"*/}
                    {/*  search*/}
                    {/*/>*/}
                    {/*<FormField*/}
                    {/*  name="organizerName"*/}
                    {/*  component={Form.Input}*/}
                    {/*  label="Organizer Name"*/}
                    {/*  placeholder="Organizer Name"*/}
                    {/*/>*/}
                    <FormField
                      name="firstName"
                      component={Form.Input}
                      label="Contact First Name"
                      placeholder="First Name"
                      required
                    />
                    <FormField
                      name="lastName"
                      component={Form.Input}
                      label="Contact Last Name"
                      placeholder="Last Name"
                      required
                    />
                    <FormField
                      name="email"
                      component={Form.Input}
                      label="Contact Email"
                      placeholder="johndoe@email.com"
                      required
                    />
                    <SubformCountry
                      name={"countryOfFormation"}
                      id={"countryOfFormation"}
                      label={"Country Of Formation"}
                      placeholder={"Select a Country"}
                    />
                    <SubformState
                      name={"stateOfFormation"}
                      id={"stateOfFormation"}
                      label={"State Of Formation"}
                      placeholder={"Select a State"}
                      country={props.values.countryOfFormation}
                    />
                    {/*<FormField*/}
                    {/*  name="eraStatus"*/}
                    {/*  component={Form.Select}*/}
                    {/*  label="ERA Status"*/}
                    {/*  options={eraStatus}*/}
                    {/*  placeholder="Select a Status"*/}
                    {/*  search*/}
                    {/*/>*/}

                    <Button
                      secondary
                      fluid
                      content="Save Changes"
                      type="submit"
                      loading={isSubmitting}
                    />
                  </Form>
                )}
              </Formik>
            )}
          </>
        )}
      </Card.Content>
    </Card>
  );
};
