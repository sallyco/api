import React from "react";
import { Link } from "react-router-dom";
import { useFeatureFlag } from "../featureflags/FeatureFlags";

import { Card, Button, Icon, Container } from "semantic-ui-react";

import { AccountSettingsCard } from "./AccountSettingsCard";
import { SystemInformationCard } from "./SystemInformationCard";

export const AccountTab = () => {
  const foundersAssessment = useFeatureFlag("founders_assessment");
  return (
    <Container>
      <Card.Group itemsPerRow="one" stackable>
        {foundersAssessment && (
          <Button primary as={Link} fluid to="/profiles/assessment">
            <Icon name="add" />
            Take Founder’s Assessment
          </Button>
        )}
      </Card.Group>
      <Card.Group itemsPerRow="two" stackable>
        <AccountSettingsCard />
        <SystemInformationCard />
      </Card.Group>
    </Container>
  );
};
