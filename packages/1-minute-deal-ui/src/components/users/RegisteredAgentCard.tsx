import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
// import * as Countries from "../../tools/address.utils/countries.js";
// import * as States from "../../tools/address.utils/states.js";
import * as Yup from "yup";
import { Card, Form, Button, Loader } from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";

export const RegisteredAgentCard = () => {
  const countries = []; //Countries.countries;
  const states = []; //States.states;
  const { isLoading, self, isSubmitting } = useSelector(
    (state: RootState) => state.users
  );
  async function onSubmit(data) {
    console.log("Submitted Successfully", data);
  }
  const validation = Yup.object().shape({
    registeredAgentName: Yup.string()

      .trim()
      .required(),
    street: Yup.string().trim().required(),
    city: Yup.string().trim().required(),
    countryOfFormation: Yup.string()

      .trim()
      .required(),
    stateOfFormation: Yup.string()

      .trim()
      .required(),
    postalCode: Yup.string()

      .email()
      .trim()
      .required(),
  });

  return (
    <Card color="grey" fluid>
      <Card.Content>
        <Card.Header>Registered Agent</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !self ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <Formik
            initialValues={{}}
            validationSchema={validation}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <FormField
                  name="registeredAgentName"
                  component={Form.Input}
                  label="Registered Agent Name"
                  placeholder="Registered Agent Name"
                  required
                />
                <FormField
                  name="street"
                  component={Form.Input}
                  label="Street"
                  placeholder="Street"
                  required
                />
                <FormField
                  name="city"
                  component={Form.Input}
                  label="City"
                  placeholder="City"
                  required
                />
                zz
                <FormField
                  name="countryOfFormation"
                  component={Form.Select}
                  label="Country"
                  options={countries}
                  placeholder="Select a Country"
                  search
                  required
                />
                <FormField
                  name="stateOfFormation"
                  component={Form.Select}
                  label="State"
                  options={states}
                  placeholder="Select a State"
                  search
                  required
                />
                <FormField
                  name="postalCode"
                  component={Form.Input}
                  label="Postal Code"
                  placeholder="Postal Code"
                  required
                />
                <Button
                  secondary
                  fluid
                  content="Save Changes"
                  type="submit"
                  loading={isSubmitting}
                />
              </Form>
            )}
          </Formik>
        )}
      </Card.Content>
    </Card>
  );
};
