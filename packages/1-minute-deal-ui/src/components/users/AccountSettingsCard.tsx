import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "../../rootReducer";
import { fetchSelf, updateUserById } from "../../slices/usersSlice";
import { toast } from "react-toastify";

import * as Yup from "yup";
import Log from "../../tools/Log";
import SubformPhone, * as SubformPhoneMeta from "../../forms/subforms/SubformPhone";

import {
  Card,
  List,
  Form,
  Button,
  Icon,
  Grid,
  Loader,
  Transition,
} from "semantic-ui-react";

import { Formik } from "formik";
import { FormField } from "../../components/FormField";

export const AccountSettingsCard = () => {
  const dispatch = useDispatch();

  const { isLoading, self, isSubmitting, error } = useSelector(
    (state: RootState) => state.users
  );

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  async function onSubmit(data) {
    if (self) {
      const { ...userData } = self;
      await dispatch(
        updateUserById({
          ...userData,
          firstName: data.firstName,
          lastName: data.lastName,
          username: data.email,
          email: data.email,
          attributes: { ...userData.attributes, phone: data.phone },
        })
      );
      toast.success(`Profile updated successfully`);
    }
  }

  const validation = Yup.object().shape({
    firstName: Yup.string()

      .trim()
      .required(),
    lastName: Yup.string().trim().required(),
    ...SubformPhoneMeta.validationSchema({ isRequired: true }),
    email: Yup.string()

      .email()
      .trim()
      .required(),
  });

  return (
    <Card fluid>
      <Card.Content>
        <Card.Header>Your account settings</Card.Header>
      </Card.Content>
      <Card.Content>
        {isLoading || !self ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <Formik
            initialValues={{
              firstName: self?.firstName,
              lastName: self?.lastName,
              phone: self?.attributes?.phone
                ? self?.attributes?.phone[0] ?? ""
                : "",
              email: self?.email,
            }}
            enableReinitialize={false}
            validationSchema={validation}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <FormField
                  name="firstName"
                  component={Form.Input}
                  label="First Name"
                  placeholder="First Name"
                  required
                />
                <FormField
                  name="lastName"
                  component={Form.Input}
                  label="Last Name"
                  placeholder="Last Name"
                  required
                />
                <SubformPhone />

                <FormField
                  name="email"
                  component={Form.Input}
                  label="Email"
                  placeholder="Email"
                  disabled={true}
                />
                <Button
                  secondary
                  fluid
                  content="Save Changes"
                  type="submit"
                  disabled={!props.dirty}
                  loading={isSubmitting}
                />
              </Form>
            )}
          </Formik>
        )}
      </Card.Content>
    </Card>
  );
};
