import React from "react";
import { Card, Transition, Container } from "semantic-ui-react";
import { OrganizerAdminCard } from "./OrganizerAdminCard";
//import { OrganizerLogoCard } from "./OrganizerLogoCard";
//import { InviteCoOrganizerCard } from "./InviteCoOrganizerCard";
//import { RegisteredAgentCard } from "./RegisteredAgentCard";
//import { EntityCard } from "./EntityCard";

export const OrganizerTab = () => {
  return (
    <Transition animation={"fade up"}>
      <Container>
        <Card.Group itemsPerRow="two" stackable>
          <OrganizerAdminCard />
          {/*
          <OrganizerLogoCard />
          <InviteCoOrganizerCard />
          <RegisteredAgentCard />
          <EntityCard />
*/}
        </Card.Group>
      </Container>
    </Transition>
  );
};
