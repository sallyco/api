import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "../../rootReducer";
import {
  fetchProfilesList,
  updateProfileById,
} from "../../slices/profilesSlice";

import * as Yup from "yup";

import {
  Card,
  Form,
  Icon,
  Grid,
  Loader,
  Divider,
  Segment,
} from "semantic-ui-react";

import { Formik } from "formik";
import { FormField } from "../../components/FormField";

export const InvestorProfileCard = () => {
  const dispatch = useDispatch();

  // const [selectedProfileId, setSelectedProfileId]: any = useState(null);

  const { currentPageProfiles, isLoading, profilesById } = useSelector(
    (state: RootState) => state.profiles
  );

  const profiles = currentPageProfiles.map(
    (profileId) => profilesById[profileId]
  );
  const options = [
    {
      value: "Joint1",
      text: "Joint 1",
    },
    {
      value: "Joint2",
      text: "Joint 2",
    },
  ];
  const optionsTrust = [
    {
      value: "trust1",
      text: "Trust 1",
    },
    {
      value: "trust2",
      text: "Trust 2",
    },
  ];
  const optionsEntities = [
    {
      value: "entity1",
      text: "Entity 1",
    },
    {
      value: "entity2",
      text: "Entity 2",
    },
  ];
  const optionsIndividuals = [
    {
      value: "individual1",
      text: "Individual 1",
    },
    {
      value: "individual2",
      text: "Individual 2",
    },
  ];
  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  async function onSubmit(data) {
    //   await dispatch(
    //     updateProfileById({
    //       ...profilesById[selectedProfileId],
    //       firstName: data.firstName,
    //       lastName: data.lastName,
    //     })
    //   );
  }

  const validation = Yup.object().shape({
    firstName: Yup.string()

      .trim()
      .required(),
    lastName: Yup.string().trim().required(),
  });

  return (
    <Card color="grey" fluid>
      <Card.Content>
        <Card.Header>Create and edit your investing profiles</Card.Header>
        <Divider hidden />
        <Card.Meta>
          Profiles such as an individual, LLC, and Trusts are used to invest in
          deals
        </Card.Meta>
      </Card.Content>
      <Card.Content>
        {isLoading || !profiles ? (
          <Loader active inline="centered" size="large" />
        ) : (
          <>
            <Divider hidden />
            <Formik
              initialValues={{}}
              validationSchema={validation}
              onSubmit={onSubmit}
            >
              {(props) => (
                <Form onSubmit={props.handleSubmit} className="dark-labels">
                  <Grid columns={2}>
                    <Grid.Column width="11">
                      <Segment>
                        <FormField
                          name="selectIndividual"
                          component={Form.Select}
                          label="Individuals"
                          options={optionsIndividuals}
                          placeholder="Select Individuals"
                          search
                        />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column width="2" verticalAlign="middle">
                      <Icon
                        className={"primaryColor"}
                        link
                        bordered
                        inverted
                        name="add"
                        size="large"
                      />
                    </Grid.Column>
                  </Grid>
                  <Grid columns={2}>
                    <Grid.Column width="11">
                      <Segment>
                        <FormField
                          label="Entity"
                          search
                          name="selectEntity"
                          component={Form.Select}
                          placeholder="Select Entity"
                          options={optionsEntities}
                        />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column width="2" verticalAlign="middle">
                      <Icon
                        color="teal"
                        link
                        bordered
                        inverted
                        name="add"
                        size="large"
                      />
                    </Grid.Column>
                  </Grid>
                  <Grid columns={2}>
                    <Grid.Column width="11">
                      <Segment>
                        <FormField
                          label="Trust"
                          search
                          name="selectTrust"
                          component={Form.Select}
                          placeholder="Select Trust"
                          options={optionsTrust}
                        />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column width="2" verticalAlign="middle">
                      <Icon
                        className={"primaryColor"}
                        link
                        bordered
                        inverted
                        name="add"
                        size="large"
                      />
                    </Grid.Column>
                  </Grid>

                  <Grid columns={2}>
                    <Grid.Column width="11">
                      <Segment>
                        <FormField
                          label="Joint"
                          search
                          name="selectJoint"
                          component={Form.Select}
                          placeholder="Select Joint "
                          options={optionsTrust}
                        />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column width="2" verticalAlign="middle">
                      <Icon
                        color="teal"
                        link
                        bordered
                        inverted
                        name="add"
                        size="large"
                      />
                    </Grid.Column>
                  </Grid>
                </Form>
              )}
            </Formik>
          </>
        )}
      </Card.Content>
    </Card>
  );
};
