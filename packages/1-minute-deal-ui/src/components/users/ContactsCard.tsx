/* eslint-disable react/display-name */
import React from "react";

import {
  Grid,
  Icon,
  Divider,
  Segment,
  Transition,
  Card,
} from "semantic-ui-react";

export const ContactsCard = () => {
  const supported = "contacts" in navigator && "ContactsManager" in window;

  return (
    <>
      <Transition animation={"fade up"}>
        <Card link className={"primaryColor"}>
          <Card.Content>
            <Card.Header>
              <Grid columns="equal">
                <Grid.Column>Contacts</Grid.Column>
                <Grid.Column textAlign="right">
                  {supported && (
                    <Icon
                      link
                      name="upload"
                      className={"primaryColor"}
                      bordered
                    ></Icon>
                  )}
                  <Icon
                    link
                    name="plus circle"
                    className={"primaryColor"}
                  ></Icon>
                </Grid.Column>
              </Grid>
            </Card.Header>
            <Divider />
            <Card.Description>
              <Segment>You haven&#39;t added any contacts yet</Segment>
            </Card.Description>
          </Card.Content>
        </Card>
      </Transition>
    </>
  );
};
