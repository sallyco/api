import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import { Card, Button, Form, Segment } from "semantic-ui-react";
import { FormField } from "../../forms/common/FormField";
import { BrokerageAccountInterface } from "./BrokerageAccountTypes";
import { useDispatch } from "react-redux";
import { updateProfileById } from "../../slices/profilesSlice";
import { Profile } from "../../api/profilesApi";

const BrokerageAccountCard: React.FC<{
  profile: Profile;
  setShow: () => void;
}> = ({ profile, setShow }) => {
  const dispatch = useDispatch();

  const onSubmit = async (data) => {
    await dispatch(
      updateProfileById({
        id: profile.id,
        brokerageAccount: {
          ...data,
        },
      })
    );
    setShow();
  };
  const [initialValues, setInitialValues] = useState<BrokerageAccountInterface>(
    {}
  );
  const onCancel = () => {
    setShow();
  };

  useEffect(() => {
    if (profile) {
      if (profile?.brokerageAccount) {
        setInitialValues(profile?.brokerageAccount);
      }
    }
  }, [profile]);

  return (
    <>
      <Card fluid>
        <Segment>
          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            enableReinitialize
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <FormField
                  name="brokerName"
                  label="Broker Name"
                  placeholder="Broker Name"
                  value={profile?.brokerageAccount?.brokerName}
                  required
                />
                <FormField
                  name="dtcNumber"
                  label="DTC Number"
                  placeholder="DTC Number"
                  value={profile?.brokerageAccount?.dtcNumber}
                  required
                />
                <FormField
                  name="accountNumber"
                  label="Account Number"
                  placeholder="Account Number"
                  value={profile?.brokerageAccount?.accountNumber}
                  required
                />

                <Button primary content="Submit" type="submit" />
                <Button onClick={onCancel} content="Cancel" type="button" />
              </Form>
            )}
          </Formik>
        </Segment>
      </Card>
    </>
  );
};

export default BrokerageAccountCard;
