import { Button, Dialog } from "@mui/material";
import { useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";
import BrokerageAccountCard from "./BrokerageAccountCard";

const BrokerageAccountButton: React.FC<{
  profileId: string;
}> = ({ profileId }) => {
  const [show, setShow] = useState(false);
  const profile = useSelector(
    (state: RootState) => state.profiles.profilesById[profileId]
  );

  const handleClick = (e) => {
    setShow(true);
  };

  const handleClose = () => {
    setShow(false);
  };

  return (
    <>
      <Button variant={"outlined"} color="primary" onClick={handleClick}>
        {profile?.brokerageAccount
          ? "Edit Brokerage Account"
          : "Add Brokerage Account"}
      </Button>
      <Dialog open={show} onClose={handleClose}>
        <BrokerageAccountCard
          setShow={() => {
            setShow(!show);
          }}
          profile={profile}
        />
      </Dialog>
    </>
  );
};

export default BrokerageAccountButton;
