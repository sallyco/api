export interface BrokerageAccountInterface {
  brokerName?: string;
  dtcName?: string;
  accountNumber?: string;
}
