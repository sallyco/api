import React, { useState, useEffect, useCallback } from "react";
import { getCountries } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";

import * as Yup from "yup";
import {
  Form,
  Container,
  Button,
  Dropdown,
  Divider,
  Header,
  Loader,
  Icon,
  List,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";
import Log from "../../tools/Log";
import { getProfileName } from "../../api/profileApiWrapper";
import { FormControlLabel, Checkbox, Typography } from "@mui/material";
import { API, doChange, useRequest } from "../../api/swrApi";
import FounderProfileForm from "../../forms/profiles/founder/FounderProfileForm";
import { useHistory, useLocation } from "react-router-dom";
import { mutate } from "swr";

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function FounderProfile({
  onSubmit,
  submitting = false,
}: Props) {
  const [showNew, setShowNew] = useState(false);
  const profileType = "INDIVIDUAL";
  const [country, setCountry]: any = useState("");
  const [stepNumber, setStepNumber] = useState(1);
  const { data: self } = useRequest(API.SELF);
  const query = new URLSearchParams(useLocation().search);
  const { data: company } = useRequest(
    query.get("companyId") &&
      API.ADD_FILTER(API.ASSET_BY_ID(query.get("companyId")), {
        fields: ["name"],
      })
  );
  const history = useHistory();

  useEffect(() => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        const countries = getCountries();
        const foundCountry = countries.find(
          (el) =>
            el?.code?.toLowerCase() === response?.countryCode?.toLowerCase()
        );
        if (foundCountry) setCountry(foundCountry.name);
      })
      .catch((err) => {});
  }, []);

  const { data: profiles } = useRequest(
    self &&
      API.ADD_FILTER(API.PROFILES, {
        where: {
          ownerId: self.id,
        },
      })
  );

  const validation = Yup.object().shape({
    profile: Yup.string().required("Select a profile is required"),
  });

  async function handleSelect(data) {
    const updatedAsset = await doChange(
      API.ASSET_BY_ID(query.get("companyId")),
      {
        founderProfileId: data?.profile,
      },
      "PATCH"
    );
    await mutate(
      API.ADD_FILTER(API.ASSET_BY_ID(query.get("companyId")), {
        include: ["deal"],
        fields: {
          logo: false,
          banner: false,
        },
      }),
      updatedAsset
    );
    await history.push(`/assets/companies/${query.get("companyId")}`);
  }

  const onHandleSection = (data) => {
    setStepNumber(data);
  };

  const [backgroundCheckAcceptance, setBackgroundCheckAcceptance] =
    useState(false);

  return (
    <>
      <Header as="h2" icon inverted textAlign="center">
        <Icon name="user circle" />
        Create {profiles?.data?.length > 0 && "or select "} a founder profile
        <Header.Subheader>
          This will be connected to the Asset {`"${company?.name}"`}
        </Header.Subheader>
      </Header>
      <Divider hidden />
      {!profiles ? (
        <Container textAlign="center">
          <Loader size="massive" inverted inline active />
        </Container>
      ) : (
        <>
          {showNew || (profiles && profiles?.data?.length === 0) ? (
            <Container>
              {
                <>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={backgroundCheckAcceptance}
                        onChange={() => setBackgroundCheckAcceptance(true)}
                      />
                    }
                    color={"white"}
                    label={
                      <Typography style={{ color: "white" }}>
                        I consent to background checks and identity verification
                        to help safeguard my investment.
                      </Typography>
                    }
                  />
                </>
              }
              <Divider />
              {backgroundCheckAcceptance && (
                <>
                  <Header as="h3" inverted>
                    My country of residence is...
                  </Header>
                  <Dropdown
                    name="country"
                    placeholder="Select a Country"
                    clearable
                    selection
                    fluid
                    search={startsWithSearch}
                    onChange={(e, d) => {
                      setCountry(d.value);
                      if (d.value === "")
                        window.scrollTo({ top: 0, behavior: "smooth" });
                    }}
                    options={getCountries().map((item) => {
                      return {
                        key: item.code,
                        value: item.name,
                        text: item.name,
                      };
                    })}
                    value={country}
                  />

                  <Divider hidden />

                  <Divider hidden />

                  {profileType === "INDIVIDUAL" && (
                    <div>
                      <FounderProfileForm
                        onSubmit={onSubmit}
                        stepNumber={stepNumber}
                        onHandleSection={onHandleSection}
                        submitting={submitting}
                        useAccountDefaultValues={true}
                        {...(country ? { country: country } : {})}
                      />
                    </div>
                  )}
                </>
              )}
            </Container>
          ) : (
            <>
              <Container className="mobile-container">
                <List relaxed>
                  {profiles?.data?.length > 0 && (
                    <>
                      <List.Item>
                        <Formik
                          initialValues={{
                            profile: undefined,
                          }}
                          validationSchema={validation}
                          onSubmit={handleSelect}
                        >
                          {(props) => (
                            <Form onSubmit={props.handleSubmit}>
                              <FormField
                                name="profile"
                                id="profile"
                                component={Form.Select}
                                options={profiles?.data?.map((profile) => ({
                                  value: profile.id,
                                  text: getProfileName(profile),
                                }))}
                                placeholder="Select a Profile"
                                data-testid="profile-selection"
                              />
                              <Button
                                secondary
                                fluid
                                type="submit"
                                content="Select"
                              />
                            </Form>
                          )}
                        </Formik>
                      </List.Item>
                      <Divider horizontal inverted>
                        Or
                      </Divider>
                    </>
                  )}
                  <List.Item>
                    <Button
                      secondary
                      fluid
                      content="Create New"
                      onClick={() => setShowNew(true)}
                    />
                  </List.Item>
                </List>
              </Container>
            </>
          )}
        </>
      )}
    </>
  );
}
