import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "../../rootReducer";
import { fetchProfilesList } from "../../slices/profilesSlice";
import { useAccountRole } from "../../contexts";
import { getProfileName } from "../../api/profileApiWrapper";

function useProfileOptions(selectedType) {
  // Redux state data
  const dispatch = useDispatch();
  const selectedId = useSelector(
    (state: RootState) => state.profiles.profileManagementSelectedId
  );
  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );

  // State specific to this component
  const [profileOptions, setProfileOptions] = useState([]);
  const [selectedProfile, setSelectedProfile] = useState(undefined);

  useEffect(() => {
    let options = Object.values(profiles)
      .filter((profile) =>
        selectedType ? profile.profileType === selectedType : true
      )
      .map((profile) => ({
        key: profile.id,
        value: profile.id,
        text: getProfileName(profile),
        selected: profile.id === selectedId,
        active: profile.id === selectedId,
      }));

    setProfileOptions(options);
    if (selectedId) {
      setSelectedProfile(profiles[selectedId]);
    }
  }, [selectedType, profiles]);

  useEffect(() => {
    if (!profiles) {
      dispatch(fetchProfilesList());
    }
  }, [profiles]);

  return {
    profileOptions,
    profiles,
    selectedType,
    selectedProfile,
    setSelectedProfile,
  };
}

function useProfileTypes() {
  const isOrganizer = useAccountRole("organizer");
  const isInvestor = useAccountRole("investor");

  // Test values
  // const isOrganizer = false;
  // const isInvestor = true;

  const [profileTypeOptions, setProfileTypeOptions] = useState([]);
  const [selectedType, setSelectedType] = useState(undefined);
  const selectedId = useSelector(
    (state: RootState) => state.profiles.profileManagementSelectedId
  );
  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );

  const currentOptions = [];

  // Set the options if the profiles or selectedId change
  useEffect(() => {
    if (isOrganizer) {
      currentOptions.push({
        key: "ORGANIZER",
        value: "ORGANIZER",
        text: "ORGANIZER",
        selected: selectedId
          ? profiles[selectedId].profileType === "ORGANIZER"
          : false,
        active: selectedId
          ? profiles[selectedId].profileType === "ORGANIZER"
          : false,
      });
    }

    if (isInvestor) {
      currentOptions.push({
        key: "INVESTOR",
        value: "INVESTOR",
        text: "INVESTOR",
        selected: selectedId
          ? profiles[selectedId].profileType === "INVESTOR"
          : false,
        active: selectedId
          ? profiles[selectedId].profileType === "INVESTOR"
          : false,
      });
    }

    setProfileTypeOptions(currentOptions);
    if (profiles[selectedId]) {
      setSelectedType(profiles[selectedId].profileType);
    }
  }, [profiles, selectedId]);

  return { selectedType, setSelectedType, profileTypeOptions };
}

export { useProfileOptions, useProfileTypes };
