import React, { useState } from "react";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Collapse,
  Dialog,
  Grid,
  IconButton,
  List,
  ListItem,
  Typography,
} from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Theme } from "@mui/material/styles";
import { Profile } from "../../api/profilesApi";
import { API, useRequest } from "../../api/swrApi";
import { Skeleton } from "@mui/material";
import AccreditationBadge from "../accreditation/AccreditationBadge";
import { PencilIcon } from "react-line-awesome";
import { ProfileEditCard } from "./ProfileEditCard";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      marginLeft: "auto",
      transform: "rotate(180deg)",
    },
  })
);

const ProfileCard: React.FC<{
  profileId: string;
  showAccreditation?: boolean;
  onUpdate?: () => Promise<any>;
}> = ({ profileId, showAccreditation = "true", onUpdate }) => {
  const classes = useStyles();
  const [showAddressForm, setShowAddressForm] = useState(false);

  const { data: profile, revalidate: revalidateProfile, mutate } = useRequest<Profile>(
    API.ADD_FILTER(API.PROFILE_BY_ID(profileId), {
      include: [
        {
          relation: "accreditations",
        },
      ],
    })
  );

  const [editOpen, setEditOpen] = useState<false | string>(false);
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Box mb={2}>
      <Card
        style={{
          textAlign: "left",
        }}
      >
        <Box m={1}>
          <Grid container>
            <Grid item md={8} sm={1}>
              <Typography variant={"h6"} color={"textPrimary"}>
                {profile ? (
                  profile?.displayName ?? "No Display Name Set"
                ) : (
                  <Skeleton />
                )}
              </Typography>
              <Typography color={"textSecondary"}>
                {profile ? (
                  profile?.name ?? `${profile?.firstName} ${profile?.lastName}`
                ) : (
                  <Skeleton />
                )}
              </Typography>
            </Grid>
            <Grid item md={4} sm={1}>
              {showAccreditation && (
                <AccreditationBadge profileId={profileId} onUpdate={onUpdate} />
              )}
            </Grid>
          </Grid>
        </Box>

        {/*<CardContent>*/}
        <List>
          <ListItem>
            <Typography color="textSecondary">
              {profile ? (
                `${profile?.stateOfFormation &&
                  profile?.stateOfFormation.charAt(0) === "A"
                  ? "An"
                  : "A"
                }
              ${profile?.stateOfFormation} ${profile?.typeOfEntity ?? "INDIVIDUAL"
                } from 
              ${profile?.countryOfFormation}`
              ) : (
                <Skeleton />
              )}
            </Typography>
          </ListItem>
        </List>
        {/*</CardContent>*/}

        <CardActions>
          <IconButton
            className={expanded ? classes.expandOpen : classes.expand}
            onClick={handleExpandClick}
            size="large"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>

        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <List>
              <ListItem>
                {profile ? (
                  <>
                    <Typography
                      color="textSecondary"
                      style={{ marginRight: "1em" }}
                    >
                      Phone:
                    </Typography>
                    <Typography>{profile?.phone}</Typography>
                  </>
                ) : (
                  <Skeleton />
                )}
              </ListItem>
              <ListItem>
                {profile ? (
                  <>
                    <Typography
                      color="textSecondary"
                      style={{ marginRight: "1em" }}
                    >
                      Email:
                    </Typography>
                    <Typography>{profile?.email}</Typography>
                  </>
                ) : (
                  <Skeleton />
                )}
              </ListItem>
              <ListItem>
                {profile ? (
                  <>
                    <Typography color="textSecondary">Address:</Typography>
                    <List dense>
                      <ListItem>
                        <Typography
                          color="textSecondary"
                          style={{ marginRight: "1em" }}
                        >
                          Line 1:
                        </Typography>
                        <Typography>{profile?.address?.address1}</Typography>
                      </ListItem>
                      {profile?.address?.address2 && (
                        <ListItem>
                          <Typography
                            color="textSecondary"
                            style={{ marginRight: "1em" }}
                          >
                            Line 2:
                          </Typography>
                          <Typography>{profile?.address?.address2}</Typography>
                        </ListItem>
                      )}
                      <ListItem>
                        <Typography
                          color="textSecondary"
                          style={{ marginRight: "1em" }}
                        >
                          City:
                        </Typography>
                        <Typography>{profile?.address?.city}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography
                          color="textSecondary"
                          style={{ marginRight: "1em" }}
                        >
                          Region:
                        </Typography>
                        <Typography>{profile?.address?.state}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography
                          color="textSecondary"
                          style={{ marginRight: "1em" }}
                        >
                          Postal Code:
                        </Typography>
                        <Typography>{profile?.address?.postalCode}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography
                          color="textSecondary"
                          style={{ marginRight: "1em" }}
                        >
                          Country:
                        </Typography>
                        <Typography>{profile?.address?.country}</Typography>
                      </ListItem>
                    </List>
                  </>
                ) : (
                  <Skeleton />
                )}
              </ListItem>
              <ListItem>
                {profile ? (
                  <>
                    {profile?.brokerageAccount && (
                      <>
                        <Typography color="textSecondary">
                          BrokerageAccount:
                        </Typography>
                        <List dense>
                          <ListItem>
                            <Typography
                              color="textSecondary"
                              style={{ marginRight: "1em" }}
                            >
                              Broker Name:
                            </Typography>
                            <Typography>
                              {profile?.brokerageAccount?.brokerName}
                            </Typography>
                          </ListItem>
                          <ListItem>
                            <Typography
                              color="textSecondary"
                              style={{ marginRight: "1em" }}
                            >
                              DTC Number:
                            </Typography>
                            <Typography>
                              {profile?.brokerageAccount?.dtcNumber}
                            </Typography>
                          </ListItem>
                          <ListItem>
                            <Typography
                              color="textSecondary"
                              style={{ marginRight: "1em" }}
                            >
                              Account Number:
                            </Typography>
                            <Typography>
                              {profile?.brokerageAccount?.accountNumber}
                            </Typography>
                          </ListItem>
                        </List>
                      </>
                    )}
                  </>
                ) : (
                  <Skeleton />
                )}
              </ListItem>
            </List>
          </CardContent>
        </Collapse>
        <CardActions>
          <Dialog open={!!editOpen} onClose={() => setEditOpen(false)}>
            <ProfileEditCard
              profile={profile}
              profileType={profile?.profileType}
              onSaveSuccess={async () => {
                setEditOpen(false);
                if (onUpdate) {
                  await revalidateProfile();
                  await onUpdate();
                }
              }}
            />
          </Dialog>
          <Button
            variant={"outlined"}
            size={"small"}
            startIcon={<PencilIcon />}
            onClick={() => setEditOpen(profile?.id)}
          >
            Edit this Profile
          </Button>
        </CardActions >
      </Card >
    </Box >
  );
};

export default ProfileCard;
