import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import { Button, Card, Form } from "semantic-ui-react";
import * as Yup from "yup";
import { Profile } from "../../api/profilesApi";
import SubformAddress, {
  validationSchema as addressValidationSchema,
} from "../../forms/subforms/SubformAddress";
import { FormField } from "../../forms/common/FormField";
import SubformDateOfBirth, {
  validationSchema as dateOfBirthValidationSchema,
} from "../../forms/subforms/SubformDateOfBirth";
import moment from "moment";
import SubformCountry from "../../forms/subforms/SubformCountry";
import SubformState from "../../forms/subforms/SubformState";
import useSWR from "swr";
import { fetcher } from "../../swrConnector";
import CommonDelete from "../../forms/common/CommonDelete";

const baseUrl = process.env.API_URL;

interface ProfileEditFormProps {
  profile: Profile;
  onSuccess: (profile) => void;
  onDelete: (address) => void;
}

const ProfileEditForm = ({
  profile,
  onSuccess,
  onDelete,
}: ProfileEditFormProps) => {
  const [deleting, setDeleting] = useState(false);
  const [showDelete, setShowDelete] = useState(false);

  const isIndividual =
    profile?.taxDetails?.registrationType &&
    profile?.taxDetails?.registrationType === "INDIVIDUAL";

  const { data: subscriptions, error: error } = useSWR(
    () =>
      profile.profileType === "INVESTOR"
        ? baseUrl + `/subscriptions/${profile?.id}/subscriptions-meta`
        : null,
    fetcher
  );

  const { data: deals, error: error1 } = useSWR(
    () =>
      profile.profileType === "ORGANIZER"
        ? baseUrl + `/deals/${profile?.id}/deals-meta`
        : null,
    fetcher
  );

  useEffect(() => {
    if (subscriptions) {
      subscriptions.totalCount > 0 ? setShowDelete(false) : setShowDelete(true);
    } else if (deals) {
      deals.totalCount > 0 ? setShowDelete(false) : setShowDelete(true);
    } else {
      setShowDelete(false);
    }
  }, [subscriptions, deals]);

  const formValidationSchema = Yup.object().shape({
    firstName: Yup.string().label("First Name").trim().required(),
    displayName: Yup.string()
      .label("Display Name")
      .trim()
      .max(64, "Display Name is limited to 64 characters")
      .required(),
    lastName: Yup.string().label("Last Name").trim().required(),
    email: Yup.string().email().label("Email").trim().required(),
    ...(isIndividual ? dateOfBirthValidationSchema({ isRequired: true }) : {}),
    address: Yup.object().shape(addressValidationSchema()),
    ...(["ENTITY", "TRUST"].includes(profile.taxDetails?.registrationType)
      ? {
          countryOfFormation: Yup.string()
            .label("Country Of Formation")
            .trim()
            .required(),
          stateOfFormation: Yup.string()
            .label("State Of Formation")
            .trim()
            .required(),
        }
      : {}),
  });

  const onSubmit = (data) => {
    const profileUpdateData = {
      displayName: data?.displayName,
      email: data?.email,
      phone: data?.phone,
      address: data?.address,
      ...(isIndividual
        ? {
            dateOfBirth: data?.dateOfBirth,
            firstName: data?.firstName,
            lastName: data?.lastName,
          }
        : {}),
      ...(profile.primarySignatory
        ? {
            primarySignatory: {
              ...profile.primarySignatory,
              name: `${data.firstName} ${data.lastName}`,
              dateOfBirth: data?.dateOfBirth,
            },
          }
        : {}),
      ...(["ENTITY", "TRUST"].includes(profile.taxDetails?.registrationType)
        ? {
            countryOfFormation: data.countryOfFormation,
            stateOfFormation: data.stateOfFormation,
          }
        : {}),
    };
    onSuccess(profileUpdateData);
  };

  return (
    <>
      {profile && (
        <Card fluid>
          <Card.Content>
            <Formik
              initialValues={{
                ...profile,
                ...(profile.dateOfBirth
                  ? {
                      day: moment(profile.dateOfBirth).format("DD"),
                      month: moment(profile.dateOfBirth).format("MM"),
                      year: moment(profile.dateOfBirth).format("YYYY"),
                    }
                  : { day: "", month: "", year: "" }),
                ...(!isIndividual && {
                  firstName:
                    profile?.primarySignatory?.name.split(" ")[0] ?? "",
                  lastName: profile?.primarySignatory?.name.split(" ")[1] ?? "",
                }),
              }}
              onSubmit={onSubmit}
              validationSchema={formValidationSchema}
              validateOnChange
            >
              {(props) => (
                <Form onSubmit={props.handleSubmit} className="dark-labels">
                  {showDelete && (
                    <>
                      <CommonDelete
                        objectType={`${profile?.profileType} PROFILE`}
                        name={profile?.firstName}
                        processing={deleting}
                        onClickOk={async () => {
                          setDeleting(false);
                          await onDelete(profile);
                        }}
                      />
                    </>
                  )}
                  <FormField
                    name="displayName"
                    id="displayName"
                    label="Display Name"
                    placeholder="Display Name"
                    value={profile?.displayName}
                    required
                  />
                  <FormField
                    name="firstName"
                    label="First Name"
                    placeholder="First Name"
                    value={profile?.firstName}
                    required
                  />
                  <FormField
                    name="lastName"
                    label="Last Name"
                    placeholder="Last Name"
                    value={profile?.lastName}
                    required
                  />
                  <FormField
                    name="email"
                    label="Email"
                    placeholder="Email"
                    value={profile?.email}
                    required
                  />
                  <FormField
                    name="phone"
                    label="Phone"
                    placeholder="Phone"
                    value={profile?.phone}
                  />
                  <SubformDateOfBirth required={true} />
                  {["ENTITY", "TRUST"].includes(
                    profile.taxDetails?.registrationType
                  ) && (
                    <>
                      <SubformCountry
                        name={"countryOfFormation"}
                        id={"countryOfFormation"}
                        label={"Country Of Formation"}
                        placeholder={"Select a Country"}
                      />
                      <SubformState
                        name={"stateOfFormation"}
                        id={"stateOfFormation"}
                        label={"State Of Formation"}
                        placeholder={"Select a State"}
                        country={props?.values?.countryOfFormation}
                      />
                    </>
                  )}
                  <SubformAddress
                    fieldsRequired={true}
                    {...props}
                    namespace="address"
                  />
                  <Button
                    primary
                    fluid
                    content="Save Changes"
                    type="submit"
                    disabled={!(props.dirty && props.isValid)}
                    data-testid="submit-button"
                  />
                </Form>
              )}
            </Formik>
          </Card.Content>
        </Card>
      )}
    </>
  );
};

export default ProfileEditForm;
