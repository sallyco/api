import SubformFirstLastName from "../../forms/subforms/SubformFirstLastName";
import { Form } from "semantic-ui-react";
import React from "react";
import SubformEmail from "../../forms/subforms/SubformEmail";
import * as Yup from "yup";

export interface SignerInterface {
  firstName?: string;
  lastName?: string;
  email?: string;
}

export const validationSchema = () => {
  return {
    firstName: Yup.string().required("First Name is Required"),
    lastName: Yup.string().required("Last Name is Required"),
    email: Yup.string()
      .email("Must be a valid Email")
      .required("Email is Required"),
  };
};

export const defaultValues: SignerInterface = {
  firstName: "",
  lastName: "",
  email: "",
};

export interface SignerCardProps {
  name: string;
}
const SignerCard: React.FC<SignerCardProps> = ({ name }) => {
  return (
    <>
      <Form.Group widths="equal">
        <SubformFirstLastName namePrefix={name} />
      </Form.Group>
      <Form.Group widths="equal">
        <SubformEmail namePrefix={name} />
      </Form.Group>
    </>
  );
};

export default SignerCard;
