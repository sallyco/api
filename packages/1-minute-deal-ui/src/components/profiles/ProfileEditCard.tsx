import React from "react";
import { useDispatch } from "react-redux";
import { Card, Segment } from "semantic-ui-react";
import { toast } from "react-toastify";
import {
  deleteProfileById,
  fetchProfilesList,
  insertProfile,
  updateProfileById,
} from "../../slices/profilesSlice";
import SubformState from "../../forms/subforms/SubformState";
import SubformCountry from "../../forms/subforms/SubformCountry";
import SubformDateOfBirth, {
  validationSchema as dateOfBirthValidationSchema,
} from "../../forms/subforms/SubformDateOfBirth";
import moment from "moment";
import {
  createKycAmlCheck,
  deleteProfileByProfileId,
  Profile,
} from "../../api/profilesApi";
import CommonDelete from "../../forms/common/CommonDelete";
import useSWR from "swr";
import { fetcher } from "../../swrConnector";
import { useHistory } from "react-router-dom";
import { API, doChange, useRequest } from "../../api/swrApi";
import ProfileEditForm from "./ProfileEditForm";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
interface Props {
  profile: any;
  profileType?: string;
  onSaveSuccess?: () => Promise<void>;
}

export function ProfileEditCard({
  profile,
  profileType,
  onSaveSuccess,
}: Props) {
  let history = useHistory();
  const dispatch = useDispatch();

  const { revalidate } = useRequest(API.PROFILES);
  const { data: self } = useRequest(API.SELF);
  const { revalidate: revalidateOwnProfiles } = useRequest(
    self &&
    API.ADD_FILTER(API.PROFILES, {
      where: {
        ownerId: self.id,
      },
    })
  );

  async function onSubmit(data) {
    if (profile?.id) {
      await dispatch(
        updateProfileById({
          ...data,
          id: profile.id,
        })
      );
      try {
        await createKycAmlCheck(profile.id);
      } catch (e) {
        toast.warn("KYC/AML check returned error.")
      }
      await dispatch(fetchProfilesList());
      await revalidate();
      await revalidateOwnProfiles();
      if (onSaveSuccess) {
        await onSaveSuccess();
      }
    } else {
      await dispatch(
        insertProfile({
          ...data,
          profileType: profileType,
        })
      );
    }
    toast.success(`Profile Saved`);
    if (onSaveSuccess) {
      await onSaveSuccess();
    }
  }

  return (
    <>
      <Card fluid>
        <Segment>
          <ProfileEditForm
            profile={profile}
            onSuccess={async (updatedProfile) => {
              onSubmit(updatedProfile);
            }}
            onDelete={async (profile) => {
              await dispatch(deleteProfileById(profile));
              await revalidateOwnProfiles();
              toast.success(`Profile deleted successfully.`);
            }}
          />
        </Segment>
      </Card>
    </>
  );
}
