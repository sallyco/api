import React, { useEffect } from "react";
import { Dropdown, Segment } from "semantic-ui-react";
import { ProfileEditCard } from "./ProfileEditCard";
import { useProfileOptions, useProfileTypes } from "./profileSelectorHooks";
import { Link } from "react-router-dom";
import { useAccountRole } from "../../contexts";
import { Alert, AlertTitle } from "@mui/material";
import {
  Card,
  Box,
  Button,
  Divider,
  Paper,
  Tab,
  Tabs,
  Typography,
} from "@mui/material";
import ParallelMarkets from "../accreditation/ParallelMarkets";
import ProfileCard from "./ProfileCard";
import { API, useRequest } from "../../api/swrApi";

export function ProfileSelector() {
  const isOrganizer = useAccountRole("organizer");

  // Logic for determining what to display is inside useProfileOptions()
  const { profileTypeOptions, selectedType, setSelectedType } =
    useProfileTypes();

  // const {
  //   profileOptions,
  //   selectedProfile,
  //   setSelectedProfile,
  //   profiles,
  // } = useProfileOptions(selectedType);

  const { data: profiles, revalidate: revalidateProfiles } = useRequest(
    API.ADD_FILTER(API.PROFILES, {
      fields: {
        id: true,
        profileType: true,
      },
    })
  );

  // const profileMissingDataWarning = (profileOptions) => {
  //   interface alert {
  //     profileId: string;
  //     message: string;
  //   }
  //
  //   const alerts: alert[] = [];
  //   for (const evaluatedProfileObject of profileOptions) {
  //     const evaluatedProfile = profiles[evaluatedProfileObject.value];
  //     if (!evaluatedProfile.dateOfBirth) {
  //       alerts.push({
  //         profileId: evaluatedProfile.id,
  //         message: `Your ${evaluatedProfile.profileType} Profile "${evaluatedProfile.displayName}" is missing
  //       a Date of Birth, which is required.`,
  //       });
  //     }
  //   }
  //
  //   if (alerts.length < 1) {
  //     return <></>;
  //   }
  //   return (
  //     <Box mb={2}>
  //       {alerts.map((alert) => (
  //         <Box key={alert.profileId} mb={1}>
  //           <Alert
  //             severity={"warning"}
  //             action={
  //               <Button
  //                 onClick={() => {
  //                   setSelectedType(profiles[alert.profileId].profileType);
  //                   setSelectedProfile(profiles[alert.profileId]);
  //                 }}
  //                 variant={"outlined"}
  //                 size="small"
  //               >
  //                 Fix Now
  //               </Button>
  //             }
  //           >
  //             <AlertTitle>Profile Update Required</AlertTitle>
  //             <Box width={"100%"}>
  //               {alert.message}
  //               <Divider variant={"fullWidth"} />
  //             </Box>
  //           </Alert>
  //         </Box>
  //       ))}
  //     </Box>
  //   );
  // };

  const [profileType, setProfileType] = React.useState(0);
  useEffect(() => {
    if (profileTypeOptions.length > 1) {
      setSelectedType(profileTypeOptions[profileType].value);
    }
  }, [profileTypeOptions]);

  return (
    <>
      <Box mb={2}>
        <Typography variant={"h5"}>Manage Profiles</Typography>
      </Box>
      <Card>
        <Box>
          <Tabs
            value={profileType}
            indicatorColor="primary"
            textColor="primary"
            onChange={(e, n) => {
              setProfileType(n);
              setSelectedType(profileTypeOptions[n].value);
            }}
          >
            {profileTypeOptions.map((type) => (
              <Tab label={type.text} key={type.key} />
            ))}
          </Tabs>
          <Box mx={2}>
            {profiles &&
              profiles.data
                .filter((p) => p.profileType === selectedType)
                .map((profile) => (
                  <ProfileCard
                    key={profile?.id}
                    profileId={profile?.id}
                    onUpdate={async () => {
                      await revalidateProfiles();
                    }}
                  />
                ))}
          </Box>
        </Box>
        <br />
        <br />
        {/*<Dropdown*/}
        {/*  placeholder={"Select Profile Type"}*/}
        {/*  fluid*/}
        {/*  selection*/}
        {/*  options={profileTypeOptions}*/}
        {/*  onChange={(e, { value }) => {*/}
        {/*    setSelectedProfile(undefined);*/}
        {/*    setSelectedType(value);*/}
        {/*  }}*/}
        {/*/>*/}
        {/*{selectedType && (*/}
        {/*  <>*/}
        {/*    <br />*/}
        {/*    <Dropdown*/}
        {/*      placeholder={"Select a Profile"}*/}
        {/*      fluid*/}
        {/*      selection*/}
        {/*      options={profileOptions}*/}
        {/*      onChange={(e, { value }) =>*/}
        {/*        typeof value === "string" &&*/}
        {/*        setSelectedProfile(profiles[value])*/}
        {/*      }*/}
        {/*    />*/}
        {/*  </>*/}
        {/*)}*/}
        {/*{selectedType && selectedProfile && (*/}
        {/*  <ProfileEditCard*/}
        {/*    key={selectedProfile.id}*/}
        {/*    profile={selectedProfile}*/}
        {/*    profileType={selectedType}*/}
        {/*  />*/}
        {/*)}*/}
      </Card>
    </>
  );
}
