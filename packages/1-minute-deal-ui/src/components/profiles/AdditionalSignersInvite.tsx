import { Field, FieldArray, useFormikContext } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import SignerCard, {
  SignerInterface,
  validationSchema as SignerValidationSchema,
} from "./SignerCard";
import { Box, Divider, Grid } from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import { Button } from "semantic-ui-react";

export const validationSchema = (minimumSigners = 1, isRequired = false) => {
  let signerSchema = Yup.array()
    .of(Yup.object().shape(SignerValidationSchema()))
    .min(minimumSigners, `Minimum of ${minimumSigners} Signers`);
  return {
    signers: signerSchema,
  };
};

export interface AdditionalSignersInviteProps {
  minSigners?: number;
  maxSigners?: number;
}

const AdditionalSignersInvite: React.FC<AdditionalSignersInviteProps> = ({
  minSigners,
  maxSigners,
}) => {
  const formik = useFormikContext<{ signers?: SignerInterface[] }>();
  if (!formik.values.signers || !formik.values.signers.length) {
    formik.values.signers = Array(minSigners);
    formik.values.signers.fill({
      firstName: "",
      lastName: "",
      email: "",
    });
  }
  return (
    <>
      <FieldArray
        name="signers"
        render={(arrayHelpers) => (
          <>
            {formik.values.signers &&
              formik.values.signers.map((values, index) => (
                <Box key={`additional-signer-${index}`}>
                  <Box display={"flex"} my={2}>
                    <Box flexGrow={1}>
                      <SignerCard name={`signers[${index}].`} />
                    </Box>
                    <Box flexGrow={0} mx={2}>
                      {minSigners !== maxSigners && index > minSigners - 1 && (
                        <Button
                          circular
                          icon
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          <RemoveIcon />
                        </Button>
                      )}
                    </Box>
                  </Box>
                  <Divider />
                </Box>
              ))}
            <Box
              my={2}
              display={"flex"}
              flexDirection={"row"}
              justifyContent={"flex-end"}
            >
              <Button
                secondary
                type={"button"}
                onClick={async () => {
                  arrayHelpers.push({});
                }}
                primary
                disabled={
                  !(
                    minSigners !== maxSigners &&
                    formik.values.signers.length < maxSigners
                  )
                }
              >
                {!(
                  minSigners !== maxSigners &&
                  formik.values.signers.length < maxSigners
                )
                  ? `Max Signer Limit of ${maxSigners} Reached`
                  : "Add Another Signer"}
              </Button>
            </Box>
          </>
        )}
      />
    </>
  );
};

export default AdditionalSignersInvite;
