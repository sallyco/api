import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { Icon, Button, Menu } from "semantic-ui-react";
import { setProfileManagementDisplayType } from "../../slices/profilesSlice";

export default function ProfileManagementActions() {
  const dispatch = useDispatch();

  const { profileManagementDisplayType } = useSelector(
    (state: RootState) => state.profiles
  );

  return (
    <Menu borderless secondary className="nav-bar">
      <Menu.Item>
        <Button.Group icon size="tiny">
          <Button
            icon
            {...(profileManagementDisplayType === "edit" && {
              className: "primaryColor",
            })}
            onClick={() => dispatch(setProfileManagementDisplayType("edit"))}
            aria-label="card view"
          >
            <Icon name="pencil" />
          </Button>
          <Button
            icon
            {...(profileManagementDisplayType === "list" && {
              className: "primaryColor",
            })}
            onClick={() => dispatch(setProfileManagementDisplayType("list"))}
            aria-label="card view"
          >
            <Icon name="list" />
          </Button>
        </Button.Group>
      </Menu.Item>
    </Menu>
  );
}
