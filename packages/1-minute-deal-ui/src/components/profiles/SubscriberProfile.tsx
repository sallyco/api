import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Alert, AlertTitle } from "@mui/material";
import { fetchEntityById } from "../../slices/entitySlice";
import { fetchDealById } from "../../slices/dealsSlice";
import { getCountries } from "country-state-picker";
import { startsWithSearch } from "../../tools/helpers";

import * as Yup from "yup";
import {
  Form,
  Transition,
  Message,
  Button,
  Dropdown,
  Divider,
  Header,
  Loader,
  Icon,
  List,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";
import { useHistory, useLocation } from "react-router-dom";
import Log from "../../tools/Log";

import { RootState } from "../../rootReducer";

import { fetchProfilesList } from "../../slices/profilesSlice";
import {
  fetchSubscriptionById,
  patchSubscriptionById,
  updateSubscriptionById,
} from "../../slices/subscriptionsSlice";
import { getProfileName } from "../../api/profileApiWrapper";

import IndividualProfileForm from "../../forms/profiles/subscriber/IndividualProfileForm";
import EntityProfileForm from "../../forms/profiles/subscriber/EntityProfileForm";
import TrustProfileForm from "../../forms/profiles/subscriber/TrustProfileForm";
import JointProfileForm from "../../forms/profiles/subscriber/JointProfileForm";
import { useFeatureFlag } from "../../components/featureflags/FeatureFlags";
import { isQPSubscriberProfile } from "../../controllers/profiles";
import {
  Box,
  ListItem,
  FormControlLabel,
  Checkbox,
  Typography,
  Grid,
  Container,
} from "@mui/material";
import { API, useNormalizedRequest, useRequest } from "../../api/swrApi";
import { Profile } from "../../api/profilesApi";
import ParallelMarkets from "../accreditation/ParallelMarkets";
import ProfileCard from "./ProfileCard";
import {
  is506cAccredited,
  is506cEntity,
} from "../../tools/accreditation/accreditationHelpers";
import { Entity } from "../../api/entitiesApi";

const QP_FEATURE_FLAG = "qualified_purchaser";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

interface Props {
  onSubmit: (any) => void;
  submitting: boolean;
}

export default function SubscriberProfile({
  onSubmit,
  submitting = false,
}: Props) {
  const qualifiedPurchaserFeature = useFeatureFlag(QP_FEATURE_FLAG);
  const dispatch = useDispatch();
  const [showNew, setShowNew] = useState(false);
  const [profileType, setProfileType]: any = useState("");
  const [country, setCountry]: any = useState("");
  const [stepNumber, setStepNumber] = useState(1);
  const { data: self } = useRequest(API.SELF);
  const [profiles, setProfiles] = useState<Profile[]>([]);

  useEffect(() => {
    fetch("https://extreme-ip-lookup.com/json/")
      .then((res) => res.json())
      .then((response) => {
        const countries = getCountries();
        const foundCountry = countries.find(
          (el) =>
            el?.code?.toLowerCase() === response?.countryCode?.toLowerCase()
        );
        if (foundCountry) setCountry(foundCountry.name);
      })
      .catch((err) => {});
  }, []);

  let history = useHistory();

  const multiSignerEnabled = useFeatureFlag("multi_signer");
  const [registrationTypes, setRegistrationTypes] = useState([
    { key: "1", value: "INDIVIDUAL", text: "Individual" },
    { key: "2", value: "ENTITY", text: "Entity" },
    { key: "3", value: "TRUST", text: "Trust" },
  ]);
  useEffect(() => {
    if (multiSignerEnabled) {
      setRegistrationTypes([
        { key: "1", value: "INDIVIDUAL", text: "Individual" },
        { key: "2", value: "ENTITY", text: "Entity" },
        { key: "3", value: "TRUST", text: "Trust" },
        { key: "4", value: "JOINT", text: "Joint" },
      ]);
    } else {
      setRegistrationTypes([
        { key: "1", value: "INDIVIDUAL", text: "Individual" },
        { key: "2", value: "ENTITY", text: "Entity" },
        { key: "3", value: "TRUST", text: "Trust" },
      ]);
    }
  }, [multiSignerEnabled, setRegistrationTypes]);

  const query = new URLSearchParams(useLocation().search);
  const selectedProfileId = query.get("selected");
  const subscriptionId = query.get("subscriptionId") ?? "";
  const isRegenerate = query.get("regenerate") ?? false;

  const [showGreyReason, setShowGreyReason] = useState(false);

  const { data: profilesList, revalidate: revalidateProfilesList } =
    useNormalizedRequest(
      API.ADD_FILTER(API.PROFILES, {
        include: [
          {
            relation: "accreditations",
          },
        ],
      })
    );

  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  const { data: subscription } = useRequest(
    subscriptionId && API.SUBSCRIPTION_BY_ID(subscriptionId)
  );

  useEffect(() => {
    if (
      (!subscription || subscription.id !== subscriptionId) &&
      subscriptionId
    ) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  const { data: deal } = useRequest(
    subscription && API.DEAL_BY_ID(subscription.dealId)
  );

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [dispatch, subscription, deal]);

  useEffect(() => {
    if (deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  const { data: profilesAlreadyUsed } = useRequest(
    subscription &&
      API.ADD_FILTER(API.PROFILES_FOR_DEAL(subscription.dealId), {
        include: ["accreditations"],
      })
  );

  useEffect(() => {
    if (profilesAlreadyUsed?.data && profilesList) {
      const usedProfiles = [...profilesAlreadyUsed.data].map((p) => p.id);
      const profileData = profilesList.filter(
        (profile) =>
          profile.profileType === "INVESTOR" &&
          !usedProfiles.includes(profile.id)
      );
      setProfiles(profileData);
    }
  }, [profilesAlreadyUsed, setProfiles, profilesList]);

  useEffect(() => {
    if (profiles) {
      for (let i = 0; i < profiles.length; i++) {
        if (!isQPSubscriberProfile(profiles[i])) {
          setShowGreyReason(true);
          break;
        }
      }
    }
  }, [profiles]);

  const isPrimarySigner = useCallback(() => {
    return self && subscription && self.id === subscription?.ownerId;
  }, [self, subscription]);

  const selectedProfile = useCallback(
    (profileId) => {
      return profiles.find((profile) => profile.id === profileId);
    },
    [profiles]
  );

  const validation = Yup.object().shape({
    profile: Yup.string().required("Select a profile is required"),
  });

  async function handleSelect(data) {
    Log.debug("data", data);
    let signers = [];
    const profile = selectedProfile(data.profile);
    if (
      isPrimarySigner() &&
      profile?.additionalSignatories &&
      (!subscription.signers || subscription.signers.length < 1)
    ) {
      signers = [
        {
          name: profile.primarySignatory.name,
          email: self.email,
          profileId: profile.id,
        },
      ];
      for (const signatory of profile.additionalSignatories) {
        signers.push({
          email: signatory.email,
          name: signatory.name,
          dateSigned: undefined,
        });
      }
      await dispatch(
        patchSubscriptionById(subscription.id, {
          profileId: data.profile,
          signers: signers,
        })
      );
    } else if (isPrimarySigner()) {
      await dispatch(
        patchSubscriptionById(subscription.id, {
          profileId: data.profile,
        })
      );
    } else if (!isPrimarySigner()) {
      const signerIndex = subscription.signers.findIndex(
        (signer) => signer.email === self.email
      );
      signers = [...subscription.signers];
      signers[signerIndex] = {
        ...signers[signerIndex],
        profileId: data.profile,
      };
      await dispatch(
        patchSubscriptionById(subscription.id, {
          signers: signers,
        })
      );
    }

    let url = `/subscriptions/${subscriptionId}/set-amount`;
    if (isRegenerate) {
      url = `/subscriptions/${subscriptionId}/set-bank-only`;
    }
    if (!isPrimarySigner()) {
      url = `/subscriptions/${subscriptionId}/sign-documents?generate=true`;
    }
    history.push(url);
  }

  const onHandleSection = (data) => {
    setStepNumber(data);
  };

  useEffect(() => {
    if (!isPrimarySigner()) {
      setProfileType("INDIVIDUAL");
    }
  }, [subscription, setProfileType]);

  const [backgroundCheckAcceptance, setBackgroundCheckAcceptance] =
    useState(false);

  const accreditationCheck = (profileId, entity: Entity) => {
    if (is506cEntity(entity)) {
      return is506cAccredited(
        profiles.find((p) => p.id === profileId),
        true
      );
    }
    return true;
  };

  return (
    <>
      <Header as="h2" icon inverted textAlign="center">
        <Icon name="user circle" />
        {profilesList && (
          <>
            Create {profiles.length > 0 && "or select "}an investment profile
            <Header.Subheader>
              This will be the entity you invest as.
            </Header.Subheader>
          </>
        )}
      </Header>
      <Divider hidden />

      {!profilesList || !self ? (
        <>
          <Container
            style={{
              textAlign: "center",
            }}
          >
            <Loader size="massive" inverted inline active />
          </Container>
        </>
      ) : (
        <>
          {showNew || (profiles && profiles.length === 0) ? (
            <Container>
              <Grid
                container
                direction="column"
                justifyContent="flex-start"
                alignItems="center"
                spacing={2}
              >
                <Grid item xs>
                  {
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={backgroundCheckAcceptance}
                          onChange={() => setBackgroundCheckAcceptance(true)}
                        />
                      }
                      color={"white"}
                      label={
                        <Typography style={{ color: "white" }}>
                          I consent to background checks and identity
                          verification to help safeguard my investment.
                        </Typography>
                      }
                    />
                  }
                  <Divider />
                  {backgroundCheckAcceptance && (
                    <>
                      <Header as="h3" inverted>
                        My country of residence is...
                      </Header>
                      <Dropdown
                        name="country"
                        placeholder="Select a Country"
                        clearable
                        selection
                        fluid
                        disabled={
                          profileType !== undefined && profileType !== ""
                        }
                        search={startsWithSearch}
                        onChange={(e, d) => {
                          setCountry(d.value);
                          if (d.value === "")
                            window.scrollTo({ top: 0, behavior: "smooth" });
                        }}
                        options={getCountries().map((item) => {
                          return {
                            key: item.code,
                            value: item.name,
                            text: item.name,
                          };
                        })}
                        value={country}
                      />

                      <Divider hidden />

                      <Transition
                        visible={
                          entity?.regulationType === "REGULATION_S" &&
                          country === "United States of America"
                        }
                      >
                        <Message negative size="small">
                          <Message.Header>
                            This deal is only available to investors outside of
                            the US.
                          </Message.Header>
                          <p>
                            Under SEC regulations, this opportunity is only
                            available to investors outside the United States of
                            America
                          </p>
                        </Message>
                      </Transition>

                      <Transition
                        visible={
                          country !== "" &&
                          !(
                            entity?.regulationType === "REGULATION_S" &&
                            country === "United States of America"
                          )
                        }
                        duration={250}
                      >
                        <div>
                          <Header as="h3" inverted>
                            I will invest as a...
                          </Header>
                          <Dropdown
                            name="registrationType"
                            placeholder="Select a Type"
                            fluid
                            clearable
                            selection
                            search={startsWithSearch}
                            disabled={stepNumber !== 1 || !isPrimarySigner()}
                            onChange={(e, d) => {
                              setProfileType(d.value);
                              if (d.value === "")
                                window.scrollTo({ top: 0, behavior: "smooth" });
                            }}
                            options={
                              isPrimarySigner()
                                ? registrationTypes
                                : registrationTypes.filter(
                                    (type) => type.value === "INDIVIDUAL"
                                  )
                            }
                            value={
                              !isPrimarySigner() ? "INDIVIDUAL" : undefined
                            }
                          />
                        </div>
                      </Transition>

                      <Divider hidden />

                      {profileType === "INDIVIDUAL" && (
                        <div>
                          <IndividualProfileForm
                            onSubmit={onSubmit}
                            stepNumber={stepNumber}
                            onHandleSection={onHandleSection}
                            submitting={submitting}
                            regType={entity?.regulationType}
                            useAccountDefaultValues={true}
                            {...(country ? { country: country } : {})}
                            isQualifiedPurchaserForm={
                              deal?.requireQualifiedPurchaser
                            }
                          />
                        </div>
                      )}

                      {profileType === "ENTITY" && (
                        <div>
                          <EntityProfileForm
                            onSubmit={onSubmit}
                            stepNumber={stepNumber}
                            onHandleSection={onHandleSection}
                            submitting={submitting}
                            regType={entity?.regulationType}
                            {...(country ? { country: country } : {})}
                            isQualifiedPurchaserForm={
                              deal?.requireQualifiedPurchaser
                            }
                          />
                        </div>
                      )}
                      {profileType === "TRUST" && (
                        <div>
                          <TrustProfileForm
                            onSubmit={onSubmit}
                            stepNumber={stepNumber}
                            onHandleSection={onHandleSection}
                            submitting={submitting}
                            regType={entity?.regulationType}
                            {...(country ? { country: country } : {})}
                            isQualifiedPurchaserForm={
                              deal?.requireQualifiedPurchaser
                            }
                          />
                        </div>
                      )}

                      {profileType === "JOINT" && (
                        <div>
                          <JointProfileForm
                            onSubmit={onSubmit}
                            stepNumber={stepNumber}
                            onHandleSection={onHandleSection}
                            submitting={submitting}
                            regType={entity?.regulationType}
                            {...(country ? { country: country } : {})}
                            isQualifiedPurchaserForm={
                              deal?.requireQualifiedPurchaser
                            }
                          />
                        </div>
                      )}
                    </>
                  )}
                </Grid>
              </Grid>
            </Container>
          ) : (
            <>
              <Container className="mobile-container">
                <List relaxed>
                  {profiles.length > 0 && (
                    <>
                      <List.Item>
                        <Formik
                          initialValues={{
                            profile: selectedProfileId ?? "",
                          }}
                          validationSchema={validation}
                          onSubmit={handleSelect}
                        >
                          {(props) => (
                            <Form onSubmit={props.handleSubmit}>
                              <FormField
                                name="profile"
                                id="profile"
                                component={Form.Select}
                                options={profiles.map((profile) => ({
                                  value: profile.id,
                                  text: getProfileName(profile),
                                  disabled:
                                    qualifiedPurchaserFeature &&
                                    deal?.requireQualifiedPurchaser &&
                                    !isQPSubscriberProfile(profile),
                                }))}
                                placeholder="Select a Profile"
                                data-testid="profile-selection"
                              />
                              {props.values.profile &&
                                selectedProfile(props.values.profile)
                                  ?.additionalSignatories?.length && (
                                  <>
                                    <Box mb={2}>
                                      <Alert severity="info">
                                        <AlertTitle>
                                          The following people will be required
                                          to sign:
                                        </AlertTitle>
                                        <List>
                                          <ListItem>
                                            {`${
                                              selectedProfile(
                                                props.values.profile
                                              )?.primarySignatory.name
                                            } - You`}
                                          </ListItem>
                                          {selectedProfile(
                                            props.values.profile
                                          )?.additionalSignatories.map(
                                            (signatory) => (
                                              <ListItem key={signatory.email}>
                                                {`${signatory.name} - ${signatory.email}`}
                                              </ListItem>
                                            )
                                          )}
                                        </List>
                                      </Alert>
                                    </Box>
                                  </>
                                )}
                              {props.values.profile && (
                                <ProfileCard
                                  profileId={props.values.profile}
                                  onUpdate={async () => {
                                    await revalidateProfilesList();
                                  }}
                                />
                              )}
                              <Button
                                secondary
                                fluid
                                type="submit"
                                content={"Select"}
                                disabled={
                                  !accreditationCheck(
                                    props.values.profile,
                                    entity
                                  )
                                }
                              />
                              <Box mt={1}>
                                {!accreditationCheck(
                                  props.values.profile,
                                  entity
                                ) && (
                                  <Alert severity={"warning"}>
                                    You must be accredited to invest in this
                                    deal. Please add your Accreditation to
                                    continue.
                                  </Alert>
                                )}
                              </Box>
                              {showGreyReason &&
                                qualifiedPurchaserFeature &&
                                deal?.requireQualifiedPurchaser && (
                                  <Alert
                                    style={{ marginTop: "14px" }}
                                    severity="info"
                                  >
                                    <p>
                                      To invest in this deal, it requires using
                                      a Qualified Purchaser (QP) investor
                                      profile.
                                    </p>
                                    <p>
                                      Greyed out profiles in the drop-down exist
                                      on your account but are not currently QP.
                                    </p>
                                  </Alert>
                                )}
                            </Form>
                          )}
                        </Formik>
                      </List.Item>
                      <Divider horizontal inverted>
                        Or
                      </Divider>
                    </>
                  )}
                  <List.Item>
                    <Button
                      secondary
                      fluid
                      content="Create New"
                      onClick={() => setShowNew(true)}
                    />
                  </List.Item>
                </List>
              </Container>
            </>
          )}
        </>
      )}
    </>
  );
}
