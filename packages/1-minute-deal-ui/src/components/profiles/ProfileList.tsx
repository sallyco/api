import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { TableWithTools } from "../common/TableWithTools";
import {
  fetchProfilesList,
  setProfileManagementSelectedId,
  setProfileManagementDisplayType,
} from "../../slices/profilesSlice";

export const ProfileList = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //PROFILES
  const profiles = useSelector(
    (state: RootState) => state.profiles.profilesById
  );
  useEffect(() => {
    if (!profiles) {
      dispatch(fetchProfilesList());
    }
  }, [dispatch, profiles]);

  const [state, setState] = useState({
    data: Object.values(profiles),
  });

  const handleRowClick = (e) => {
    dispatch(setProfileManagementSelectedId(e.currentTarget.id));
    dispatch(setProfileManagementDisplayType("edit"));
    history.push(`/profiles`);
  };

  return (
    <TableWithTools
      data={state.data}
      entityName="profiles"
      columnDefinitions={[
        {
          key: "profileType",
          headerTitle: "Type",
          rowDisplayValue: (rowData) => rowData["profileType"],
        },
        {
          key: "firstName",
          headerTitle: "First Name",
          rowDisplayValue: (rowData) => rowData["firstName"],
        },
        {
          key: "lastName",
          headerTitle: "Last Name",
          rowDisplayValue: (rowData) => rowData["lastName"],
        },
        {
          key: "phone",
          headerTitle: "Phone",
          rowDisplayValue: (rowData) => rowData["phone"],
        },
        {
          key: "email",
          headerTitle: "Email",
          rowDisplayValue: (rowData) => rowData["email"],
        },
        {
          key: "stateOfFormation",
          headerTitle: "State of Formation",
          rowDisplayValue: (rowData) => rowData["stateOfFormation"],
        },
        {
          key: "countryOfFormation",
          headerTitle: "Country of Formation",
          rowDisplayValue: (rowData) => rowData["countryOfFormation"],
        },
      ]}
      exportDataMap={(profile) => ({
        Type: profile.profileType,
        "First Name": profile.firstName,
        "Last Name": profile.lastName,
        Phone: profile.phone,
        Email: profile.email,
        "State of Formation": profile.stateOfFormation,
        "Country of Formation": profile.countryOfFormation,
      })}
      rowProps={{
        onClick: handleRowClick,
      }}
    />
  );
};
