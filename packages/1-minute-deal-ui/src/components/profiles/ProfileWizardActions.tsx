import React from "react";
import { Form, Header, Divider } from "semantic-ui-react";

export default function ProfileWizardActions({
  currentStep,
  goToStepHandler,
  nextDisabled,
  submitting,
  canSubmitForm = false,
}) {
  return (
    <Form.Group widths="equal">
      <Form.Button
        type="button"
        fluid
        size="mini"
        onClick={() => goToStepHandler(currentStep - 1)}
        content="Back"
      />
      {canSubmitForm && (
        <Form.Button
          secondary
          type="submit"
          loading={submitting}
          fluid
          size="mini"
          disabled={nextDisabled || submitting}
          content="Create Profile"
        />
      )}
      {!canSubmitForm && (
        <Form.Button
          secondary
          type="button"
          fluid
          size="mini"
          disabled={nextDisabled}
          onClick={() => goToStepHandler(currentStep + 1)}
          content="Next Section"
        />
      )}
    </Form.Group>
  );
}

export function ProfileWizardStep({
  stepNumber,
  onHandleSection,
  nextDisabled,
  header,
  children,
  canSubmitForm = false,
  submitting = false,
}) {
  return (
    <>
      <Header as="h3" inverted content={header} />
      <Divider />
      {children}
      <ProfileWizardActions
        currentStep={stepNumber}
        goToStepHandler={onHandleSection}
        nextDisabled={nextDisabled}
        submitting={submitting}
        canSubmitForm={canSubmitForm}
      />
    </>
  );
}
