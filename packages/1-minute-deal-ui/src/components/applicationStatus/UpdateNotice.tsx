import React from "react";

const UpdateNotice = () => {
  function reload() {
    window.location.reload();
  }

  return (
    <>
      <div className={"ui grid"}>
        <div className={"eight wide column center aligned middle aligned"}>
          A new version is available
        </div>
        <div className={"eight wide column center aligned middle aligned"}>
          <button onClick={reload} className={"ui button"}>
            Reload to Update
          </button>
        </div>
      </div>
    </>
  );
};

export default UpdateNotice;
