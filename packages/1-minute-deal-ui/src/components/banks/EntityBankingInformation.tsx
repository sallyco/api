import { Entity } from "../../api/entitiesApi";
import React, { useContext, useEffect } from "react";
import { Skeleton, Alert } from "@mui/material";
import {
  Card,
  CardContent,
  Typography,
  Icon,
  Box,
  TableContainer,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableFooter,
} from "@mui/material";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import { Profile } from "../../api/profilesApi";
import { RootState } from "../../rootReducer";
import { fetchDealById } from "../../slices/dealsSlice";
import { fetchEntityById } from "../../slices/entitySlice";
import { fetchProfileById } from "../../slices/profilesSlice";
import { fetchCompanyById } from "../../slices/companiesSlice";
import { useSelector, useDispatch } from "react-redux";
import { TenantContext } from "../../contexts";
import { getProfileName } from "../../api/profileApiWrapper";

export interface EntityBankingInformationProps {
  entity?: Entity;
  investorProfile?: Profile;
}

const EntityBankingInformation: React.FC<EntityBankingInformationProps> = ({
  entity,
  investorProfile,
  ...props
}) => {
  const tenantProfile = useContext(TenantContext);
  const dispatch = useDispatch();

  const dealId = entity?.dealId;

  // DEAL
  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  // PROFILE
  const profile = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });

  useEffect(() => {
    if (deal && (!profile || profile.id !== deal.profileId)) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [dispatch, profile, deal]);

  // MASTER
  const masterEntity = useSelector((state: RootState) => {
    return !profile ||
      (!profile.masterEntityId && !tenantProfile.masterEntityId)
      ? undefined
      : state.companies.companiesById[
          profile.masterEntityId
            ? profile.masterEntityId
            : tenantProfile.masterEntityId
        ];
  });
  const manager = useSelector((state: RootState) => {
    return !profile || (!profile.managerId && !tenantProfile.managerId)
      ? undefined
      : state.companies.companiesById[
          profile.managerId ? profile.managerId : tenantProfile.managerId
        ];
  });

  useEffect(() => {
    if (profile && !masterEntity) {
      if (profile.masterEntityId) {
        dispatch(fetchCompanyById(profile.masterEntityId));
      } else if (tenantProfile.masterEntityId) {
        dispatch(fetchCompanyById(tenantProfile.masterEntityId));
      }
    }
    if (profile && !manager) {
      if (profile.managerId) {
        dispatch(fetchCompanyById(profile.managerId));
      } else if (tenantProfile.managerId) {
        dispatch(fetchCompanyById(tenantProfile.managerId));
      }
    }
  }, [dispatch, profile, tenantProfile]);

  return (
    <>
      {!entity && (
        <>
          <Skeleton variant="rectangular" width={210} height={118} />
        </>
      )}
      {entity?.bankAccount && (
        <>
          <Card>
            <CardContent>
              <Box
                display="flex"
                justifyContent={"center"}
                alignItems={"center"}
              >
                <AccountBalanceIcon
                  fontSize="large"
                  style={{ paddingRight: "10px" }}
                />
                <Typography color={"textSecondary"} gutterBottom>
                  Banking &amp; Wire Settlement Details
                </Typography>
              </Box>
              <TableContainer>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Pay To
                      </TableCell>
                      <TableCell>
                        <Typography gutterBottom>{entity?.name}</Typography>
                        <Typography>
                          {manager?.address.address1 ?? "6510 Millrock Dr"}
                          &nbsp;
                          {manager?.address.address2 ?? "#400"}
                        </Typography>
                        <Typography>
                          {manager?.address.city ?? "Salt Lake City"},&nbsp;
                          {manager?.address.state ?? "UT"}&nbsp;
                          {manager?.address.country ?? "USA"}&nbsp;
                          {manager?.address.postalCode ?? "84121"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Bank
                      </TableCell>
                      <TableCell>
                        <Typography gutterBottom>
                          {entity.bankAccount.bankName ?? "Pending"}
                        </Typography>
                        <Typography gutterBottom>
                          {entity.bankAccount.bankAddress}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Routing (ABA) & Transit Number
                      </TableCell>
                      <TableCell>
                        <Typography>
                          {entity.bankAccount.routingNumber}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Credit Account Number
                      </TableCell>
                      <TableCell>
                        <Typography>
                          {entity.bankAccount.accountNumber}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Swift Code
                      </TableCell>
                      <TableCell>
                        <Typography>{entity.bankAccount.swiftCode}</Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Bank Contact
                      </TableCell>
                      <TableCell>
                        {entity.bankAccount.bankContact?.name
                          ? `Name: ${entity.bankAccount.bankContact?.name}`
                          : ""}
                        <br />
                        {entity.bankAccount.bankContact?.name
                          ? `Phone: ${entity.bankAccount.bankContact?.name}`
                          : ""}
                        <br />
                        {entity.bankAccount.bankContact?.email
                          ? `Email: ${entity.bankAccount.bankContact?.email}`
                          : ""}
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ textTransform: "uppercase" }}>
                        Reference
                      </TableCell>
                      <TableCell>
                        <Typography>
                          {investorProfile
                            ? getProfileName(investorProfile)
                            : `<investor profile name>`}{" "}
                          - {entity?.name ?? ""}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                <TableFooter>
                  &nbsp;
                  <Alert severity="info">
                    <p>
                      <strong>IMPORTANT</strong>: An investor MUST include
                      the&nbsp;
                      <em>Reference</em> information with the wire or memo field
                      on their check or it will not be credited properly.
                    </p>
                  </Alert>
                </TableFooter>
              </TableContainer>
            </CardContent>
          </Card>
        </>
      )}
    </>
  );
};

export default EntityBankingInformation;
