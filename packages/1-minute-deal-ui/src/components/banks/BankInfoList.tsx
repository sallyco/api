import React from "react";

import { List } from "semantic-ui-react";

function BankInfoList({ bankAccount, reference }) {
  const {
    bankName,
    bankAddress,
    accountName,
    accountNumber,
    routingNumber,
    bankContact,
  } = bankAccount;

  const { entityName, investorProfileName } = reference;

  return (
    <>
      <List bulleted size="small">
        <List.Item>Bank Name:{" " + bankName}</List.Item>
        {bankAddress && (
          <List.Item>
            Bank Address:
            {" " + bankAddress}
          </List.Item>
        )}
        <List.Item>Account Name:{" " + accountName}</List.Item>
        <List.Item>
          Account Number:
          {" " + accountNumber}
        </List.Item>
        <List.Item>
          Routing Number:
          {" " + routingNumber}
        </List.Item>
        {bankContact && (
          <List.Item>
            Bank Contact: {bankContact?.name ?? ""}
            <div style={{ paddingLeft: "16px" }}>
              <div>Phone: {bankContact?.phone ?? ""}</div>
              <div>Email: {bankContact?.email ?? ""}</div>
            </div>
          </List.Item>
        )}
        <List.Item>
          <strong>Reference:</strong> {entityName} {investorProfileName}
        </List.Item>
      </List>
      <p>
        <strong>IMPORTANT</strong>: You MUST include the reference information
        with the wire or memo field on your check or it will not be credited
        properly.
      </p>
    </>
  );
}

export default BankInfoList;
