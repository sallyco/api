import React from "react";
import moment from "moment";
import { TableWithTools } from "@packages/gbt-ui";
import numeral from "numeral";

const DESPOSIT = "deposit";
const DIRECTION_INDEX = 2;

interface Props {
  transactions: object[];
}

const TransactionTable = ({ transactions }: Props) => {
  return (
    <TableWithTools
      data={transactions}
      selectable={false}
      columnDefinitions={[
        {
          name: "id",
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: "date",
          label: "Transaction Date",
          options: {
            customBodyRender: (value, tableMeta, updateValue) =>
              moment.utc(value).format("MMMM DD, YYYY"),
          },
        },
        {
          // Direction is left in here so we can use this info on the row
          // To display a positive or negative amount
          name: "type",
          label: "Direction",
          options: {
            display: "excluded",
            download: false,
          },
        },
        {
          name: "investor_name",
          label: "Investor",
        },
        {
          name: "consolidated_description",
          label: "Description",
          options: {
            filter: false,
          },
        },
        {
          name: "amount",
          label: "Amount",
          options: {
            customBodyRender: function TransactionAmount(
              value,
              tableMeta,
              updateValue
            ) {
              // if row is a deposit (show positive)
              // if row is a withdraw (show negative)
              if (tableMeta.rowData[DIRECTION_INDEX] !== DESPOSIT) {
                return (
                  <span style={{ whiteSpace: "nowrap" }}>
                    {numeral(Math.abs(value) * -1).format("$0,0.00")}
                  </span>
                );
              }

              return (
                <span style={{ color: "darkblue" }}>
                  {numeral(value).format("$0,0.00")}
                </span>
              );
            },
            sortCompare: (order) => {
              return (obj1, obj2) => {
                const obj1Value =
                  obj1.rowData[DIRECTION_INDEX] !== DESPOSIT
                    ? obj1.data * -1
                    : obj1.data;
                const obj2Value =
                  obj2.rowData[DIRECTION_INDEX] !== DESPOSIT
                    ? obj2.data * -1
                    : obj2.data;

                return obj1Value > obj2Value && order === "asc" ? 1 : -1;
              };
            },
          },
        },
        {
          name: "balance",
          label: "Balance",
          options: {
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) =>
              numeral(value).format("$0,0.00"),
          },
        },
        {
          name: "transaction_data",
          label: "Transaction Data",
          options: {
            display: false,
            filter: false,
          },
        },
      ]}
      title="Account Transactions"
    />
  );
};

export default TransactionTable;
