import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import { Form } from "semantic-ui-react";
import Log from "../tools/Log";

const FileInputField = (props) => {
  const {
    className,
    field: { name, value },
    form: { setFieldValue, handleBlur },
    isError,
    errormessage,
  } = props;

  const fileInputRef: any = useRef();
  const errorStyle = isError ? "error" : "";

  const [iserror, seterror] = useState(props.isError);

  const handleInputBlur = (e) => {
    handleBlur(e);
  };

  const fileChange = (e) => {
    Log.debug("file ", e.target.files[0]);
    const file = e.target.files[0];
    let reader = new FileReader();
    if (file.size <= 2097152) {
      seterror(false);
      reader.onload = function (event) {
        Log.debug("event ", event);
        setFieldValue(name, event?.target?.result ?? undefined);
      };
      reader.readAsDataURL(file);
    } else {
      seterror(true);
      errormessage("Your file is too big");
      setFieldValue(name, undefined);
    }
  };

  return (
    <div className={`${className} ${errorStyle} field`}>
      <Form.Button
        fluid
        secondary
        onClick={() => fileInputRef.current.click()}
        content="UPLOAD A HEADSHOT"
        type="button"
        icon="user circle"
      />
      <input
        ref={fileInputRef}
        type="file"
        name="headshot"
        hidden
        onChange={fileChange}
        onBlur={handleInputBlur}
        accept="image/*"
      />

      {iserror && (
        <div className="ui pointing above prompt label">
          {props.errormessage}
        </div>
      )}
    </div>
  );
};

FileInputField.propTypes = {
  className: PropTypes.string,
  form: PropTypes.any.isRequired,
  field: PropTypes.any.isRequired,
  onChange: PropTypes.func,
  isError: PropTypes.bool,
  errormessage: PropTypes.string,
};

FileInputField.defaultProps = {
  className: "",
  onChange: null,
  isError: false,
  errormessage: "",
};

export default FileInputField;
