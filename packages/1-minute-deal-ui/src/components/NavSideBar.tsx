import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Icon, Sidebar, Menu } from "semantic-ui-react";
import { AuthContext } from "../contexts";
import { CONTACT_LIST_FLAG } from "./ContactListTable";
import { FeatureFlag } from "./featureflags/FeatureFlags";

const NavSideBar = ({
  isVisible,
  setVisible,
}: {
  isVisible: any;
  setVisible: any;
}) => {
  const userProfile = useContext(AuthContext);

  return (
    <Sidebar
      as={Menu}
      animation="overlay"
      className={"primaryBackground"}
      direction="left"
      icon="labeled"
      onHide={() => setVisible(false)}
      inverted
      vertical
      visible={isVisible}
    >
      {userProfile.resource_access.account.roles.includes("founder") ||
      userProfile.resource_access.account.roles.includes("dealer") ? (
        <>
          {userProfile.resource_access.account.roles.includes("founder") && (
            <Menu.Item
              name="companies"
              as={NavLink}
              active={true}
              to="/dashboard/assets/companies"
              onClick={() => setVisible(false)}
            >
              <Icon name="building" />
              Companies
            </Menu.Item>
          )}

          {userProfile.resource_access.account.roles.includes("dealer") && (
            <Menu.Item
              name="artwork"
              as={NavLink}
              active={true}
              to="/dashboard/assets/artwork"
              onClick={() => setVisible(false)}
            >
              <Icon name="image" />
              Artwork
            </Menu.Item>
          )}
          {/* Temporarily Removed */}
          {/*<Menu.Item*/}
          {/*  name="organizers"*/}
          {/*  as={NavLink}*/}
          {/*  to="/dashboard/companyOrganizer"*/}
          {/*  onClick={() => setVisible(false)}*/}
          {/*>*/}
          {/*  <Icon name="users" />*/}
          {/*  Organizers*/}
          {/*</Menu.Item>*/}
        </>
      ) : (
        <>
          {userProfile.resource_access.account.roles.includes("organizer") && (
            <>
              <Menu.Item
                name="deals"
                as={NavLink}
                to="/dashboard/deals"
                onClick={() => setVisible(false)}
              >
                <Icon name="dollar" />
                Open Deals
              </Menu.Item>

              <FeatureFlag name="founders_assessment">
                <Menu.Item
                  name="companies"
                  as={NavLink}
                  to="/dashboard/assets/companies"
                  onClick={() => setVisible(false)}
                >
                  <Icon name="building" />
                  Companies
                </Menu.Item>
              </FeatureFlag>
            </>
          )}
          <Menu.Item
            name="subscriptions"
            as={NavLink}
            to="/dashboard/subscriptions"
            onClick={() => setVisible(false)}
          >
            <Icon name="money" />
            Investments
          </Menu.Item>
          <Menu.Item
            name="docs"
            as={NavLink}
            to="/dashboard/docs"
            onClick={() => setVisible(false)}
          >
            <Icon name="file alternate" />
            Docs
          </Menu.Item>
          {userProfile.resource_access.account.roles.includes("organizer") && (
            <Menu.Item
              name="investors"
              as={NavLink}
              to="/dashboard/investors"
              onClick={() => setVisible(false)}
            >
              <Icon name="users" />
              Investors
            </Menu.Item>
          )}

          <FeatureFlag name={CONTACT_LIST_FLAG}>
            <Menu.Item name="contacts" as={NavLink} to="/dashboard/contacts">
              <Icon name="address book" />
              Contacts
            </Menu.Item>
          </FeatureFlag>
        </>
      )}
    </Sidebar>
  );
};

export default NavSideBar;
