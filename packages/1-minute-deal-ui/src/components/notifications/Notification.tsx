import React from "react";
import { useHistory } from "react-router-dom";
import {
  Card,
  Segment,
  Icon,
  Item,
  SemanticCOLORS,
  Divider,
} from "semantic-ui-react";
import { Notification as NotificationType } from "../../api/notificationsApi";
import moment from "moment";
import { updateNotifications } from "../../swrConnector";
interface NotificationProps {
  notification: NotificationType;
}

export const Notification = ({ notification }: NotificationProps) => {
  const history = useHistory();
  const iconColor = (): SemanticCOLORS => {
    if (notification?.acknowledged) {
      return "grey";
    } else {
      switch (notification?.category) {
        case "DEAL_STATUS":
          return "blue";
        case "INVESTMENTS":
          return "green";
        case "GENERAL":
          return "black";
        case "CLOSING_ACTIVITY":
          return "yellow";
        default:
          return "black";
      }
    }
  };

  const linkLocation = (): string => {
    switch (notification?.event?.type) {
      case "deal":
        return `/deals/${notification.event.id}/1`;
      case "subscription":
        return `/subscriptions/${notification.event.id}`;
      case "subscriptionAmount":
        return `/subscriptions/${notification.event.id}/set-amount`;
      default:
        return "/";
    }
  };

  const toggleAcknowledged = () => {
    updateNotifications({
      ...notification,
      acknowledged: !notification.acknowledged,
    });
  };

  const handleClick = () => {
    toggleAcknowledged();
    history.push(linkLocation());
  };

  return (
    <>
      <Card fluid onClick={handleClick}>
        <Card.Content textAlign={"left"}>
          <Item>
            <Item.Header style={{ color: "black" }}>
              <Icon
                fitted
                size="small"
                name={notification?.acknowledged ? "circle outline" : "circle"}
                color={iconColor()}
                onClick={toggleAcknowledged}
              />
              {"  " + notification?.category.replace(/[_]/gi, " ")}
            </Item.Header>
            <Divider hidden fitted className={"margin-top-20"} />
            <Item.Description style={{ color: "black" }}>
              {notification?.message}
            </Item.Description>
          </Item>
        </Card.Content>
        {notification["sender"] && notification["sender"]["name"] && (
          <Card.Content extra>
            <span style={{ textAlign: "right" }}>
              From: {notification["sender"]["name"]} (
              {notification["sender"]["email"]})
            </span>
          </Card.Content>
        )}
        <Card.Content extra>
          {moment(notification?.createdAt).format("MMM D, YYYY")}
        </Card.Content>
      </Card>
    </>
  );
};
