import React, { useState, useEffect } from "react";
import {
  Grid,
  Icon,
  Item,
  Dimmer,
  Card,
  Divider,
  Segment,
  List,
  Button,
  Message,
  Sidebar,
  Loader,
  Modal,
  Label,
} from "semantic-ui-react";
import { Notification } from "./Notification";
import {
  NotificationList as NotificationlistType,
  Notification as NotificationType,
} from "../../api/notificationsApi";
import { updateNotifications } from "../../swrConnector";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { useRouteMatch } from "react-router-dom";
import { ToggleButton, ToggleButtonGroup } from "@mui/material";

interface NotificationListProps {
  notificationList: NotificationlistType;
}

export const NotificationList = ({
  notificationList,
}: NotificationListProps) => {
  const [notifications, setNotifications] = useState(
    notificationList?.data ?? new Array<NotificationType>()
  );
  const [categoryFilter, setCategoryFilter] = useState<string | null>();

  const [hideAcknowledged, setHideAcknowledged] = useState(false);
  const [clearingNotifications, setClearingNotifications] = useState(false);
  const [numberOfUnreadNotifications, setNumberOfUnreadNotifications] =
    useState(0);
  const [viewableNotifications, setViewableNotifications] = useState(
    new Array<NotificationType>()
  );
  let match = useRouteMatch();

  useEffect(() => {
    setNumberOfUnreadNotifications(
      notifications.length > 0
        ? notifications.filter(
            (singleNotification) => !singleNotification.acknowledged
          ).length
        : 0
    );
  }, [notifications, notificationList]);

  useEffect(() => {
    setViewableNotifications(
      notifications.length > 0
        ? notifications
            .filter((singleNotification) => {
              if (!categoryFilter) {
                return true;
              }
              return categoryFilter === singleNotification.category;
            })
            .sort((a, b) => {
              let aDate = new Date(a.createdAt);
              let bDate = new Date(b.createdAt);
              if (aDate > bDate) return -1;
              if (aDate < bDate) return 1;
              return 0;
            })
        : new Array<NotificationType>()
    );
  }, [notifications, hideAcknowledged, notificationList, categoryFilter]);

  useEffect(() => {
    if (notificationList.data) {
      setNotifications(notificationList.data);
    }
  }, [notificationList]);

  const clearNotifications = async () => {
    setClearingNotifications(true);
    for (const notification of notifications
      .filter((notification) => !notification.acknowledged)
      .filter((notification) => {
        if (!categoryFilter) {
          return true;
        }
        return categoryFilter === notification.category;
      })) {
      // eslint-disable-next-line no-await-in-loop
      await updateNotifications({ ...notification, acknowledged: true });
    }
    setHideAcknowledged(true);
    setClearingNotifications(false);
  };

  return (
    <>
      <Modal
        closeIcon
        trigger={
          <Icon.Group style={{ cursor: "pointer", marginRight: "12px" }}>
            <Icon inverted size={"large"} name={"bell"} />
            {numberOfUnreadNotifications > 0 && (
              <Label
                color="red"
                floating
                style={{ padding: ".3em", marginLeft: "-10px" }}
              >
                {numberOfUnreadNotifications < 100
                  ? numberOfUnreadNotifications
                  : "99+"}
              </Label>
            )}
          </Icon.Group>
        }
      >
        <Modal.Header>
          <Grid columns={16}>
            <Grid.Row verticalAlign={"middle"}>
              <Grid.Column width={7}>
                Notifications ({numberOfUnreadNotifications})
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Divider hidden />
          <ToggleButtonGroup
            value={categoryFilter}
            exclusive
            onChange={(
              event: React.MouseEvent<HTMLElement>,
              newValue: string | null
            ) => setCategoryFilter(newValue)}
            aria-label="text alignment"
          >
            <ToggleButton value="DEAL_STATUS">Deal Status</ToggleButton>
            <ToggleButton value="INVESTMENTS">Investments</ToggleButton>
            <ToggleButton value="GENERAL">General</ToggleButton>
            <ToggleButton value="CLOSING_ACTIVITY">
              Closing Activity
            </ToggleButton>
          </ToggleButtonGroup>
        </Modal.Header>
        <Modal.Content scrolling>
          <Item>
            <Item.Description>
              {numberOfUnreadNotifications === 0 && (
                <Message icon>
                  <Icon name="bell outline" />
                  <Message.Content>
                    <Message.Header>{"You're all caught up"}</Message.Header>
                  </Message.Content>
                </Message>
              )}
              <List>
                {viewableNotifications.map((singleNotification) => (
                  <Notification
                    key={singleNotification.id}
                    notification={singleNotification}
                  />
                ))}
              </List>
            </Item.Description>
          </Item>
        </Modal.Content>
        <Modal.Actions>
          <Button
            primary
            onClick={() => clearNotifications()}
            loading={clearingNotifications}
            disabled={
              notifications.filter((notification) => !notification.acknowledged)
                .length < 1
            }
          >
            Acknowledge {!categoryFilter ? "All" : ""} Notifications{" "}
            {categoryFilter ? "In This Category" : ""}
          </Button>
        </Modal.Actions>
      </Modal>
    </>
  );
};
