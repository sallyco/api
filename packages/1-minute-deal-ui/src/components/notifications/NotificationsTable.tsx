import React, { useState } from "react";
import { Notification } from "../../api/notificationsApi";
import moment from "moment";
import { TableWithTools } from "../common/TableWithTools";

interface Props {
  notifications: Notification[];
  column?: any;
  direction?: any;
}

export const NotificationsTable = ({
  notifications,
  column,
  direction,
}: Props) => {
  const [state, setState] = useState({
    column: column,
    data: notifications,
    direction: direction,
  });

  return (
    <TableWithTools
      data={state.data}
      columnDefinitions={[
        {
          key: "createdAt",
          headerTitle: "Timestamp",
          rowDisplayValue: (rowData) =>
            moment.utc(rowData["createdAt"]).format("MMMM DD, YYYY"),
        },
        {
          key: "category",
          headerTitle: "Category",
          rowDisplayValue: (rowData) =>
            rowData["category"].toString().replace(/[_]/gi, " "),
        },
        {
          key: "message",
          headerTitle: "Message",
          rowDisplayValue: (rowData) => rowData["message"],
        },
        {
          key: "event.type",
          headerTitle: "Event Type",
          rowDisplayValue: (rowData) => rowData["event"]["type"],
        },
      ]}
      direction={state.direction}
      entityName="notifications"
      selectedColumn={state.column}
      exportDataMap={(notification) => ({
        Timestamp: moment.utc(notification.createdAt).format("MMMM DD, YYYY"),
        Category: notification.category,
        Message: notification.message,
        "Event Type": notification.event.type,
      })}
    />
  );
};
