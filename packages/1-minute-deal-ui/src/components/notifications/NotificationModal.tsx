import React, { useEffect, useState, Suspense } from "react";
import { Loader } from "semantic-ui-react";
import { NotificationList } from "./NotificationList";
import { getToken, useNotifications } from "../../swrConnector";
import { ErrorBoundary } from "@sentry/react";

export const NotificationModal = () => {
  const notificationList = useNotifications();
  const [token, setToken] = useState("");
  useEffect(() => {
    if (!token.length) {
      setToken(getToken("__session"));
    }
  }, [token]);

  return (
    <>
      {token.length > 0 && (
        <ErrorBoundary fallback={<Loader active />}>
          <Suspense fallback={<Loader active />}>
            <NotificationList notificationList={notificationList} />
          </Suspense>
        </ErrorBoundary>
      )}
    </>
  );
};
