import React, { useEffect, useState } from "react";
import { NotificationsTable } from "./NotificationsTable";
import { Container } from "semantic-ui-react";
import { useNotifications, getToken } from "../../swrConnector";

export const NotificationsTab = () => {
  const notificationList = useNotifications();
  const [token, setToken] = useState("");
  useEffect(() => {
    if (!token.length) {
      setToken(getToken("__session"));
    }
  }, [token]);

  return (
    <Container>
      <NotificationsTable notifications={notificationList.data} />
    </Container>
  );
};
