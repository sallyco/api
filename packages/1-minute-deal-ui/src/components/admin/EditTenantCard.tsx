import React, { useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import {
  Button,
  Card,
  Form,
  Icon,
  Image as SImage,
  Transition,
} from "semantic-ui-react";
import { TenantContext } from "../../contexts";
import { fetchTenantById, updateTenantById } from "../../slices/tenantSlice";
import { Formik } from "formik";
import { MaskedFormField, FormField } from "../FormField";
import * as Yup from "yup";
import Log from "../../tools/Log";

const EditTenantCard = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const tenant = useSelector((state: RootState) => state.tenant.tenant);
  const { isSubmitting } = useSelector((state: RootState) => state.tenant);

  useEffect(() => {
    if (!tenant || tenant.id !== tenantProfile.id) {
      dispatch(fetchTenantById(tenantProfile.id));
    }
  }, [tenant, dispatch]);

  async function onSubmit(data) {
    Log.debug("data", data);
    await dispatch(
      updateTenantById({
        ...tenant,
        inverted: data.inverted,
        name: data.name,
        url: data.url,
        assets: data.assets,
        settings: data.settings,
      })
    );
  }

  const validation = Yup.object().shape({
    inverted: Yup.boolean(),
    name: Yup.string().trim(),
    url: Yup.string().url().trim(),
    assets: Yup.object({
      images: Yup.object({
        logo: Yup.string().trim(),
        logoInverted: Yup.string().trim(),
        favicon: Yup.string().trim(),
      }),
      colors: Yup.object({
        primaryColor: Yup.string().trim(),
        secondaryColor: Yup.string().trim(),
      }),
    }),
    settings: Yup.object({
      emails: Yup.object({
        fromAddress: Yup.string().email().trim(),
      }),
    }),
  });

  const initialValues = {
    inverted: tenant?.inverted,
    name: tenant?.name,
    url: tenant?.url,
    assets: tenant?.assets,
    settings: tenant?.settings,
  };

  return (
    <Transition animation={"fade up"}>
      <Card color="grey" raised>
        <Card.Content>
          <Formik
            initialValues={initialValues}
            validationSchema={validation}
            onSubmit={onSubmit}
          >
            {(props) => (
              <Form onSubmit={props.handleSubmit} className="dark-labels">
                <Form.Group unstackable widths={"equal"}>
                  <FormField name="name" label="Name" placeholder="name" />
                  <FormField name="url" label="URL" placeholder="url" />
                </Form.Group>
                <Form.Group unstackable widths={"equal"}>
                  <FormField
                    name="settings.emails.fromAddress"
                    label="Email - From Address"
                    placeholder="email"
                    type={"email"}
                  />
                </Form.Group>
                <Form.Group unstackable widths={"equal"}>
                  <FormField
                    name="assets.images.logo"
                    label="Images - Logo"
                    placeholder="logo"
                  />
                  <FormField
                    name="assets.images.logoInverted"
                    label="Images - Logo Inverted"
                    placeholder="logo (inverted)"
                  />
                  <FormField
                    name="assets.images.favicon"
                    label="Images - Favicon"
                    placeholder="favicon"
                  />
                </Form.Group>
                <Form.Group unstackable widths={"equal"}>
                  <FormField
                    name="inverted"
                    label="inverted"
                    placeholder="inverted"
                    component={Form.Checkbox}
                  />
                  <FormField
                    name="assets.colors.primaryColor"
                    label="Colors - Primary"
                    placeholder="primary color"
                  />
                  <FormField
                    name="assets.colors.secondaryColor"
                    label="Colors - Secondary"
                    placeholder="secondary color"
                  />
                </Form.Group>

                <Button
                  secondary
                  fluid
                  content="Save Changes"
                  type="submit"
                  loading={isSubmitting}
                />
              </Form>
            )}
          </Formik>
        </Card.Content>
      </Card>
    </Transition>
  );
};

export default EditTenantCard;
