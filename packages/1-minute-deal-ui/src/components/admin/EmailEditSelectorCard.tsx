import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card, Dropdown, Segment, Transition } from "semantic-ui-react";
import { RootState } from "../../rootReducer";
import { setEmailTemplateKey } from "../../slices/emailTemplatesSlice";
import { EditEmailTemplateForm } from "../../forms/emails/EditEmailTemplateForm";

export const EmailEditSelectorCard = () => {
  const dispatch = useDispatch();
  const { emailTemplateKey } = useSelector(
    (state: RootState) => state.emailTemplates
  );

  const emailTypeOptions = [
    { key: "inviteInvestor", text: "Invite Investor", value: "inviteInvestor" },
    { key: "emailFooter", text: "Email Footer", value: "emailFooter" },
    {
      key: "inviteInvestorExisting",
      text: "Invite Existing Investor",
      value: "inviteInvestorExisting",
    },
    { key: "newAccount", text: "New Account", value: "newAccount" },
    {
      key: "nudgeInvestorExisting",
      text: "Nudge Investor",
      value: "nudgeInvestorExisting",
    },
    {
      key: "organizerInvite",
      text: "Invite Organizer",
      value: "organizerInvite",
    },
    { key: "resetPassword", text: "Reset Password", value: "resetPassword" },
  ];

  return (
    <Transition animation={"fade up"}>
      <Card color="grey" raised>
        <Card.Content>
          <Segment raised>
            <Dropdown
              placeholder={"Email"}
              fluid
              selection
              options={emailTypeOptions}
              onChange={(e, { value }) => dispatch(setEmailTemplateKey(value))}
            />
            {emailTemplateKey && <EditEmailTemplateForm />}
          </Segment>
        </Card.Content>
      </Card>
    </Transition>
  );
};
