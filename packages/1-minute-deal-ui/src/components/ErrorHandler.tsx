import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { ToastContainer, Slide, toast } from "react-toastify";

import { RootState } from "../rootReducer";

import Log from "../tools/Log";

const ErrorHandler = () => {
  const showError = (error) => {
    Log.debug("error", error);
    if (error?.statusCode === 404) {
      return;
    }

    if (error?.response?.data?.error?.message) {
      toast.error(error?.response?.data.error?.message);
    } else if (error && error.data && error.data.message) {
      toast.error(`${error.data.error.message}`);
      //toast.error(`${error.data.error.statusCode} - ${err.data.error.message}`);
    } else if (error && error.message) {
      toast.error(error.message);
    } else if (error) {
      toast.error(error);
    } else {
      toast.error("An error occured");
    }
  };

  const { error: assetsError } = useSelector(
    (state: RootState) => state.assets
  );
  useEffect(() => {
    if (assetsError) {
      showError(assetsError.data);
    }
  }, [assetsError]);

  const { error: closesError } = useSelector(
    (state: RootState) => state.closes
  );
  useEffect(() => {
    if (closesError) {
      showError(closesError.data);
    }
  }, [closesError]);

  const { error: dealsError } = useSelector((state: RootState) => state.deals);
  useEffect(() => {
    if (dealsError) {
      showError(dealsError.data);
    }
  }, [dealsError]);

  const { error: entitiesError } = useSelector(
    (state: RootState) => state.entities
  );
  useEffect(() => {
    if (entitiesError) {
      showError(entitiesError.data);
    }
  }, [entitiesError]);

  const { error: filesError } = useSelector((state: RootState) => state.files);
  useEffect(() => {
    if (filesError) {
      showError(filesError.data);
    }
  }, [filesError]);

  const { error: invitesError } = useSelector(
    (state: RootState) => state.invites
  );
  useEffect(() => {
    if (invitesError) {
      showError(invitesError.data);
    }
  }, [invitesError]);

  const { error: profileError } = useSelector(
    (state: RootState) => state.profiles
  );
  useEffect(() => {
    if (profileError) {
      showError(profileError.data);
    }
  }, [profileError]);

  const { error: subscriptionsError } = useSelector(
    (state: RootState) => state.subscriptions
  );
  useEffect(() => {
    if (subscriptionsError) {
      showError(subscriptionsError.data);
    }
  }, [subscriptionsError]);

  const { error: usersError } = useSelector((state: RootState) => state.users);
  useEffect(() => {
    if (usersError) {
      showError(usersError.data);
    }
  }, [usersError]);

  return (
    <ToastContainer
      position="bottom-center"
      transition={Slide}
      autoClose={5000}
      hideProgressBar={true}
      newestOnTop={false}
      limit={1}
      closeOnClick
      rtl={false}
    />
  );
};

export default ErrorHandler;
