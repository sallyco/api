/* eslint-disable import/no-named-as-default */
import React, { useContext, useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import {
  Divider,
  Icon,
  Header as Head,
  Sidebar,
  Segment,
  Image,
  Container,
} from "semantic-ui-react";
import { AuthContext, TenantContext } from "./contexts";
import Log from "./tools/Log";
import Cookies from "js-cookie";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { UpdateReset } from "./rootReducer";
import Favicon from "react-favicon";
import {
  Route,
  Switch,
  withRouter,
  Redirect,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import {
  useFeatureFlag,
  FeatureFlagsContext,
} from "./components/featureflags/FeatureFlags";

import Header from "./components/Header";
import NavBar from "./components/NavBar";
import NavSideBar from "./components/NavSideBar";
import ErrorHandler from "./components/ErrorHandler";
import { CopyrightFooter } from "./components/common/CopyrightFooter";
import { clearSessions, resetToken } from "./api/authApi";
import { ToastError } from "./tools/ToastMessage";
import { AdminRoutes } from "./routes/AdminRoutes";
import { PopupModal } from "./components/common/PopupModal";
import "react-toastify/dist/ReactToastify.min.css";
import useSWR from "swr";
import { getFeatureFlags } from "./api/featureFlags";
import RequiredAccountActionsGate from "./components/RequiredAccountActionsGate";

// Base Routes

const Login = React.lazy(() =>
  import("./pages/Login").then((module) => ({ default: module.Login }))
);
const Register = React.lazy(() =>
  import("./pages/Register").then((module) => ({ default: module.Register }))
);
const ForgotCredentials = React.lazy(() =>
  import("./pages/ForgotCredentials").then((module) => ({
    default: module.ForgotCredentials,
  }))
);
const ResetCredentials = React.lazy(() =>
  import("./pages/ResetCredentials").then((module) => ({
    default: module.ResetCredentials,
  }))
);
const Unverified = React.lazy(() =>
  import("./pages/UnverifiedPage").then((module) => ({
    default: module.Unverified,
  }))
);
const VerifyEmail = React.lazy(() =>
  import("./pages/VerifyEmailPage").then((module) => ({
    default: module.VerifyEmail,
  }))
);
const AccountChoicePage = React.lazy(() =>
  import("./pages/AccountChoicePage").then((module) => ({
    default: module.AccountChoicePage,
  }))
);

//  Dashboard
const DealsListPage = React.lazy(() =>
  import("./pages/DealsListPage").then((module) => ({
    default: module.DealsListPage,
  }))
);
const SubscriptionsListPage = React.lazy(() =>
  import("./pages/SubscriptionsListPage").then((module) => ({
    default: module.SubscriptionsListPage,
  }))
);
const DocsPage = React.lazy(() =>
  import("./pages/DocsPage").then((module) => ({ default: module.DocsPage }))
);
const ContactsPage = React.lazy(() =>
  import("./pages/ContactsPage").then((module) => ({
    default: module.ContactsPage,
  }))
);
const SettingsPage = React.lazy(() =>
  import("./pages/SettingsPage").then((module) => ({
    default: module.SettingsPage,
  }))
);
const GlxListPage = React.lazy(() =>
  import("./pages/GlxListPage").then((module) => ({
    default: module.GlxListPage,
  }))
);
const CompanyListPage = React.lazy(() =>
  import("./pages/CompanyListPage").then((module) => ({
    default: module.CompanyListPage,
  }))
);
const ArtworkListPage = React.lazy(() =>
  import("./pages/ArtworkListPage").then((module) => ({
    default: module.ArtworkListPage,
  }))
);
const CompanyDocPage = React.lazy(() =>
  import("./pages/CompanyDocPage").then((module) => ({
    default: module.CompanyDocPage,
  }))
);
const CompanyOrganizerPage = React.lazy(() =>
  import("./pages/CompanyOrganizerPage").then((module) => ({
    default: module.CompanyOrganizerPage,
  }))
);
const CompanySettingsPage = React.lazy(() =>
  import("./pages/CompanySettingsPage").then((module) => ({
    default: module.CompanySettingsPage,
  }))
);
const AccountAdminPage = React.lazy(() =>
  import("./pages/AccountAdminPage").then((module) => ({
    default: module.AccountAdminPage,
  }))
);
const InvestorsPage = React.lazy(() =>
  import("./pages/InvestorsPage").then((module) => ({
    default: module.InvestorsPage,
  }))
);

// ASSETS
const AssetChoicePage = React.lazy(() =>
  import("./pages/AssetChoicePage").then((module) => ({
    default: module.AssetChoicePage,
  }))
);
const AssetViewPage = React.lazy(() =>
  import("./pages/AssetViewPage").then((module) => ({
    default: module.AssetViewPage,
  }))
);
const CompanyProfilePage = React.lazy(() =>
  import("./pages/CreateCompanyProfilePage").then((module) => ({
    default: module.CompanyProfilePage,
  }))
);
const CreateArtworkAssetPage = React.lazy(() =>
  import("./pages/CreateArtworkAssetPage").then((module) => ({
    default: module.CreateArtworkAssetPage,
  }))
);
const InviteOrganizersPage = React.lazy(() =>
  import("./pages/InviteOrganizersPage").then((module) => ({
    default: module.InviteOrganizersPage,
  }))
);
const CompanySummaryPage = React.lazy(() =>
  import("./pages/CompanySummaryPage").then((module) => ({
    default: module.CompanySummaryPage,
  }))
);
const ExcecutiveSummaryEditPage = React.lazy(() =>
  import("./pages/ExcecutiveSummaryEditPage").then((module) => ({
    default: module.ExcecutiveSummaryEditPage,
  }))
);
const FoundersAssessmentPage = React.lazy(() =>
  import("./pages/FoundersAssessmentPage").then((module) => ({
    default: module.FoundersAssessmentPage,
  }))
);

// Deals
const DealViewPage = React.lazy(() =>
  import("./pages/DealViewPage").then((module) => ({
    default: module.DealViewPage,
  }))
);
const CreateDealPage = React.lazy(() =>
  import("./pages/CreateDealPage").then((module) => ({
    default: module.CreateDealPage,
  }))
);
const ReviewDocsPage = React.lazy(() =>
  import("./pages/ReviewDocsPage").then((module) => ({
    default: module.ReviewDocsPage,
  }))
);
const InviteInvestorsPage = React.lazy(() =>
  import("./pages/InviteInvestorsPage").then((module) => ({
    default: module.InviteInvestorsPage,
  }))
);

// Subscriptions
const DealSummaryPage = React.lazy(() =>
  import("./pages/DealSummaryPage").then((module) => ({
    default: module.DealSummaryPage,
  }))
);
const SigningSummaryPage = React.lazy(() =>
  import("./pages/SigningSummaryPage").then((module) => ({
    default: module.SigningSummaryPage,
  }))
);
const SubscriptionAmountPage = React.lazy(() =>
  import("./pages/SubscriptionAmountPage").then((module) => ({
    default: module.SubscriptionAmountPage,
  }))
);
const SubscriptionBankOnlyPage = React.lazy(() =>
  import("./pages/SubscriptionBankOnlyPage").then((module) => ({
    default: module.SubscriptionBankOnlyPage,
  }))
);
const SubscriptionDocumentsSignPage = React.lazy(() =>
  import("./pages/SubscriptionDocumentsSignPage").then((module) => ({
    default: module.SubscriptionDocumentsSignPage,
  }))
);
const SelectFundingPage = React.lazy(() =>
  import("./pages/SelectFundingPage").then((module) => ({
    default: module.SelectFundingPage,
  }))
);
const FundingPage = React.lazy(() =>
  import("./pages/FundingPage").then((module) => ({
    default: module.FundingPage,
  }))
);
const BankAccountPage = React.lazy(() =>
  import("./pages/BankAccountPage").then((module) => ({
    default: module.BankAccountPage,
  }))
);
const DirectDealLinkPage = React.lazy(() =>
  import("./pages/DirectDealLinkPage").then((module) => ({
    default: module.DirectDealLinkPage,
  }))
);

// Profiles
const ManageProfilePage = React.lazy(() =>
  import("./pages/ManageProfilesPage").then((module) => ({
    default: module.ManageProfilePage,
  }))
);
const SelectProfilePage = React.lazy(() =>
  import("./pages/SelectProfilePage").then((module) => ({
    default: module.SelectProfilePage,
  }))
);
const CreateProfilePage = React.lazy(() =>
  import("./pages/CreateProfilePage").then((module) => ({
    default: module.CreateProfilePage,
  }))
);
const KycAmlReviewPage = React.lazy(() =>
  import("./pages/KycAmlReviewPage").then((module) => ({
    default: module.KycAmlReviewPage,
  }))
);

// closes
const CloseDealPage = React.lazy(() =>
  import("./pages/CloseDealPage").then((module) => ({
    default: module.CloseDealPage,
  }))
);
const CloseSignPage = React.lazy(() =>
  import("./pages/CloseSignPage").then((module) => ({
    default: module.CloseSignPage,
  }))
);
const CloseFundPage = React.lazy(() =>
  import("./pages/CloseFundPage").then((module) => ({
    default: module.CloseFundPage,
  }))
);
const CloseWirePage = React.lazy(() => import("./pages/CloseWirePage"));
const OrganizerBanking = React.lazy(() => import("./pages/BankingPage"));

const RequiredAccountActionsPage = React.lazy(() =>
  import("./pages/RequiredAccountActions").then((module) => ({
    default: module.RequiredAccountActions,
  }))
);

//Todo: When try to committing the code get in a eslint error. So I commented this portion temporary
// eslint-disable-next-line
//declare const module: any;

const App = withRouter(() => {
  const dispatch = useDispatch();
  const history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const getSession = () => {
    const jwt = Cookies.get("__session");
    let session;
    try {
      if (jwt) {
        const base64Url = jwt.split(".")[1];
        const base64 = base64Url.replace("-", "+").replace("_", "/");
        session = JSON.parse(window.atob(base64));

        // TODO Check if exired
      }
    } catch (error) {
      Log.debug("getSession", error);
    }
    return session;
  };
  const UnprotectedRoute = ({ comp: Component, ...rest }) => {
    const { data: featureFlags } = useSWR("/feature-flags", getFeatureFlags, {
      focusThrottleInterval: 60000,
    });
    return (
      <Route
        {...rest}
        render={(props: any) =>
          getSession() &&
          getSession().exp > Date.now() / 1000 &&
          props.location.pathname !== "/invest-now" ? (
            //
            <Redirect
              to={{ pathname: "/dashboard", state: { from: props.location } }}
            />
          ) : (
            <FeatureFlagsContext.Provider value={featureFlags}>
              <Component {...props} />
            </FeatureFlagsContext.Provider>
          )
        }
      />
    );
  };
  UnprotectedRoute.propTypes = {
    // comp: PropTypes.func,
    location: PropTypes.object,
  };

  const ProtectedRoute = ({ comp: Component, roles, ...rest }) => {
    const session = getSession();
    const { data: featureFlags } = useSWR("/feature-flags", getFeatureFlags);
    Log.debug("session", session);
    if (
      session &&
      session.exp > Date.now() / 1000 &&
      !session.email_verified &&
      tenantProfile.realm.verifyEmail
    ) {
      return <Unverified email={session.email} />;
    }

    if (session && session.exp > Date.now() / 1000) {
      if (
        roles.includes("*") ||
        roles.some((r) => session.resource_access.account.roles.includes(r))
      ) {
        Log.debug("roles", "here");
        return (
          <AuthContext.Provider value={session}>
            <FeatureFlagsContext.Provider value={featureFlags}>
              <Route {...rest} render={(props) => <Component {...props} />} />
            </FeatureFlagsContext.Provider>
          </AuthContext.Provider>
        );
      } else {
        let home = "/dashboard/subscriptions";
        const redirectPath = Cookies.get("__postlogin-url");
        if (redirectPath) {
          document.cookie =
            "__postlogin-url= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
          home = decodeURIComponent(redirectPath);
        } else {
          if (session.resource_access.account.roles.includes("founder"))
            home = "/dashboard/assets/companies";
          if (session.resource_access.account.roles.includes("dealer"))
            home = "/dashboard/assets/artwork";
          else if (session.resource_access.account.roles.includes("organizer"))
            home = "/dashboard/deals";
          else if (
            session.resource_access.account.roles.includes("account-admin")
          )
            home = "/dashboard/account-admin";
          else if (
            session.resource_access.account.roles.includes("signer") &&
            !session.resource_access.account.roles.includes("investor")
          )
            home = "/dashboard/signing";
        }

        return (
          <Route
            {...rest}
            render={(props) => (
              <Redirect
                to={{ pathname: home, state: { from: props.location } }}
              />
            )}
          />
        );
      }
    } else {
      return (
        <Route
          {...rest}
          render={(props) => {
            document.cookie = `__postlogin-url=${encodeURIComponent(
              props.location.pathname
            )};`;
            return (
              <Redirect
                to={{ pathname: `/`, state: { from: props.location } }}
              />
            );
          }}
        />
      );
    }
  };
  ProtectedRoute.propTypes = {
    component: PropTypes.func,
    roles: PropTypes.array,
    location: PropTypes.object,
  };

  const DashboardRouting = ({ ...rest }) => {
    const session = getSession();
    let path = "/dashboard/deals";
    if (session.resource_access.account.roles.includes("founder")) {
      path = "/dashboard/assets/companies";
    } else if (session.resource_access.account.roles.includes("dealer")) {
      path = "/dashboard/assets/artwork";
    } else if (
      session.resource_access.account.roles.includes("account-admin")
    ) {
      path = "/dashboard/account-admin";
    } else if (
      session.resource_access.account.roles.includes("signer") &&
      !session.resource_access.account.roles.includes("investor")
    ) {
      path = "/dashboard/signing";
    }

    return (
      <Route
        {...rest}
        render={(props) => (
          <Redirect to={{ pathname: path, state: { from: props.location } }} />
        )}
      />
    );
  };

  const LogOutAction = async () => {
    const refreshToken = Cookies.get("__sessionrefresh");
    if (refreshToken) {
      const protocol = window.location.protocol;

      const hn = window.location.hostname;
      let baseUrl = "api.glassboardtech.com";
      if (hn.includes("localhost")) {
        baseUrl = "localhost:8081";
      } else if (hn.includes("dev")) {
        baseUrl = "api.dev.glassboardtech.com";
      } else if (hn.includes("qa")) {
        baseUrl = "api.qa.glassboardtech.com";
      } else if (hn.includes("staging")) {
        baseUrl = "api.sandbox.glassboardtech.com";
      } else if (hn.includes("sandbox")) {
        baseUrl = "api.sandbox.glassboardtech.com";
      }

      const fullurl = `${protocol}//${baseUrl}/auth/realms/${tenantProfile.id}/protocol/openid-connect/logout`;
      fetch(fullurl, {
        body: new URLSearchParams({
          client_id: "account",
          refresh_token: refreshToken,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        mode: "cors",
        method: "POST",
      });
    }

    await clearSessions();
    await dispatch(UpdateReset());
    history.push("/");
  };
  const LogOut: React.FC<{ location: string }> = (props) => {
    useEffect(() => {
      LogOutAction();
    }, []);
    return <></>;
  };

  const LoggedOut = () => {
    const tenantProfile = useContext(TenantContext);
    return (
      <>
        <Divider hidden />
        {tenantProfile.url && tenantProfile.assets.images.logo && (
          <Container textAlign={"center"}>
            <a href={tenantProfile.url} style={{ display: "inline-block" }}>
              <Image
                centered
                size={"medium"}
                src={
                  tenantProfile.inverted
                    ? tenantProfile.assets.images.logoInverted
                    : tenantProfile.assets.images.logo
                }
                style={{ maxHeight: "112px", width: "auto" }}
              />
            </a>
          </Container>
        )}
        <Divider hidden />
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/forgot-credentials" component={ForgotCredentials} />
          <Route path="/reset-credentials" component={ResetCredentials} />
          <Route path="/register" exact component={Register} />
          <Route path="/register/:inviteId" component={Register} />
          <Route
            path="/account-portal"
            // component={AccountChoicePage}
            component={Login}
          />
          <Route path="/invest-now" component={DirectDealLinkPage} />
        </Switch>
      </>
    );
  };

  const Signing = () => {
    let match = useRouteMatch();
    return (
      <RequiredAccountActionsGate>
        <Switch>
          <Route
            path={`${match.path}/:subscriptionId`}
            exact
            component={DealSummaryPage}
          />
        </Switch>
      </RequiredAccountActionsGate>
    );
  };

  const Deals = () => {
    let match = useRouteMatch();
    return (
      <RequiredAccountActionsGate>
        <Switch>
          <Route path={`${match.path}`} exact component={DealsListPage}>
            <Helmet bodyAttributes={{ class: "grey-background" }} />
          </Route>
          <Route
            path={`${match.path}/create`}
            exact
            component={CreateDealPage}
          />

          <Route
            path={`${match.path}/:dealId/review-docs`}
            exact
            component={ReviewDocsPage}
          />
          <Route
            path={`${match.path}/:dealId/invite-investors`}
            exact
            component={InviteInvestorsPage}
          />
          <Route
            path={[`${match.path}/:dealId/:tabIndex`, `${match.path}/:dealId`]}
            component={DealViewPage}
          />
        </Switch>
      </RequiredAccountActionsGate>
    );
  };

  const Assets = () => {
    let match = useRouteMatch();
    return (
      <RequiredAccountActionsGate>
        <Switch>
          <Route path={`${match.path}`} exact component={CompanyListPage}>
            <Helmet bodyAttributes={{ class: "grey-background" }} />
          </Route>
          <Route
            path={`${match.path}/create`}
            exact
            component={AssetChoicePage}
          />
          <Route
            path={`${match.path}/inviteorganizer/:assetId`}
            exact
            component={InviteOrganizersPage}
          />
          <Route
            path={`${match.path}/:assetId`}
            exact
            component={AssetViewPage}
          />
          <Route path={`${match.path}/companies`} component={Companies} />
          <Route path={`${match.path}/artwork`} component={Artwork} />
        </Switch>
      </RequiredAccountActionsGate>
    );
  };

  const Companies = () => {
    let match = useRouteMatch();
    return (
      <>
        <Switch>
          <Route
            path={`${match.path}/create`}
            exact
            component={CompanyProfilePage}
          />
          <Route
            path={`${match.path}/:companyId`}
            exact
            component={CompanySummaryPage}
          />
          <Route
            path={`${match.path}/companyexcecutivesummary/:companyId`}
            exact
            component={ExcecutiveSummaryEditPage}
          />
        </Switch>
      </>
    );
  };

  const Artwork = () => {
    let match = useRouteMatch();
    return (
      <>
        <Switch>
          <Route
            path={`${match.path}/create`}
            exact
            component={CreateArtworkAssetPage}
          />
        </Switch>
      </>
    );
  };

  const Closes = () => {
    let match = useRouteMatch();
    return (
      <RequiredAccountActionsGate>
        <Switch>
          <Route
            path={`${match.path}/:dealId/close-deal`}
            exact
            component={CloseDealPage}
          />
          <Route
            path={`${match.path}/:dealId/close-sign/:closeId`}
            component={CloseSignPage}
          />
          <Route
            path={`${match.path}/:dealId/close-funding/:closeId`}
            component={CloseFundPage}
          />
          <Route
            path={`${match.path}/:dealId/close-wire/:closeId`}
            component={CloseWirePage}
          />
        </Switch>
      </RequiredAccountActionsGate>
    );
  };

  const Subscriptions = () => {
    let match = useRouteMatch();
    return (
      <>
        <RequiredAccountActionsGate>
          <Switch>
            <Route
              path={`${match.path}`}
              exact
              component={SubscriptionsListPage}
            >
              <Helmet bodyAttributes={{ class: "grey-background" }} />
            </Route>
            <Route
              path={`${match.path}/:subscriptionId`}
              exact
              component={DealSummaryPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/set-amount`}
              exact
              component={SubscriptionAmountPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/set-bank-only`}
              exact
              component={SubscriptionBankOnlyPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/sign-documents`}
              exact
              component={SubscriptionDocumentsSignPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/funding-select`}
              exact
              component={SelectFundingPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/funding`}
              exact
              component={FundingPage}
            />
            <Route
              path={`${match.path}/:subscriptionId/bank_account/:displayReason?`}
              component={BankAccountPage}
            />
          </Switch>
        </RequiredAccountActionsGate>
      </>
    );
  };

  const Profiles = () => {
    let match = useRouteMatch();
    return (
      <>
        <Switch>
          <Route path={`${match.path}`} exact component={ManageProfilePage} />
          <Route
            path={`${match.path}/create`}
            exact
            component={SelectProfilePage}
          />
          <Route
            path={`${match.path}/create/select`}
            component={SelectProfilePage}
          />
          <Route
            path={`${match.path}/create/:type`}
            component={CreateProfilePage}
          />
          <Route
            path={`${match.path}/kycaml/:profileId`}
            exact
            component={KycAmlReviewPage}
          />
          <Route
            path={`${match.path}/assessment`}
            component={FoundersAssessmentPage}
          />
          <Route component={NotFound} />
        </Switch>
      </>
    );
  };

  const Dashboard = () => {
    let match = useRouteMatch();
    const outboundWiresAreEnabled = useFeatureFlag("outbound_wires");

    // Disabling because we add the key prop
    // when the array is rendered
    /* eslint-disable react/jsx-key */
    const routes = [
      <Route path={`${match.path}`} exact component={DashboardRouting} />,
      <ProtectedRoute
        path={`${match.path}/deals`}
        exact
        comp={DealsListPage}
        roles={["organizer"]}
      />,
      <ProtectedRoute
        path={`${match.path}/subscriptions`}
        exact
        comp={SubscriptionsListPage}
        roles={["investor"]}
      />,
      <ProtectedRoute
        path={`${match.path}/assets/companies`}
        exact
        comp={CompanyListPage}
        roles={["founder", "organizer"]}
      />,
      <ProtectedRoute
        path={`${match.path}/assets/artwork`}
        exact
        comp={ArtworkListPage}
        roles={["dealer"]}
      />,
      <ProtectedRoute
        path={`${match.path}/account-admin`}
        exact
        comp={AccountAdminPage}
        roles={["account-admin"]}
      />,
      <ProtectedRoute
        path={`${match.path}/glx`}
        exact
        comp={GlxListPage}
        roles={["*"]}
      />,
      <ProtectedRoute
        path={`${match.path}/investors`}
        exact
        comp={InvestorsPage}
        roles={["organizer"]}
      />,
      <Route path={`${match.path}/docs`} component={DocsPage} />,
      <Route path={`${match.path}/contacts`} component={ContactsPage} />,
      <Route path={`${match.path}/settings`} component={SettingsPage} />,
      <Route path={`${match.path}/companydocs`} component={CompanyDocPage} />,
      <Route
        path={`${match.path}/companyOrganizer`}
        component={CompanyOrganizerPage}
      />,
      <Route
        path={`${match.path}/companysettings`}
        component={CompanySettingsPage}
      />,
      <ProtectedRoute
        path={`${match.path}/signing`}
        exact
        comp={SigningSummaryPage}
        roles={["signer"]}
      />,
    ];

    if (outboundWiresAreEnabled) {
      routes.push(
        <Route
          exact
          path={`${match.path}/organizer-banking/:closeId`}
          component={OrganizerBanking}
        />
      );

      routes.push(
        <Route
          exact
          path={`${match.path}/organizer-banking`}
          component={OrganizerBanking}
        />
      );
    }
    /* eslint-enable react/jsx-key */

    return (
      <>
        {/*<Helmet bodyAttributes={{style: 'background-color : #f3f3f3'}}/>*/}
        <Helmet bodyAttributes={{ class: "grey-background" }} />
        <Sidebar.Pushable>
          <NavSideBar
            isVisible={sideBarVisible}
            setVisible={setSideBarVisible}
          />
          <Sidebar.Pusher dimmed={sideBarVisible}>
            <Header showSideBar={setSideBarVisible} />
            <NavBar />
            <RequiredAccountActionsGate>
              <Switch>
                {routes.map((route) => {
                  // The clone is used to add keys to the routes
                  return React.cloneElement(route, { key: route.props.path });
                })}
              </Switch>
            </RequiredAccountActionsGate>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </>
    );
  };

  const AdminSection = () => {
    let match = useRouteMatch();
    return <></>;
  };

  const NotFound = () => {
    return (
      <>
        <Helmet title={`404 | ${tenantProfile.name}`}></Helmet>
        <Segment placeholder>
          <Head icon>
            <Icon name="question circle outline" color="red" />
            <Head.Subheader> 404 </Head.Subheader>
          </Head>
        </Segment>
      </>
    );
  };

  //const NotImplemented = () => {
  //  return (
  //    <>
  //      <Helmet>
  //        <title>501 | Glassboard Technology</title>
  //      </Helmet>
  //      <Segment placeholder>
  //        <Head icon>
  //          <Icon name="question circle outline" color="yellow" />
  //          <Head.Subheader> 501 </Head.Subheader>
  //        </Head>
  //      </Segment>
  //    </>
  //  );
  //};

  const auth = getSession();
  const [state, setstate] = useState(false);
  const [sideBarVisible, setSideBarVisible] = useState(false);
  useEffect(() => {
    if (auth && auth.exp) {
      const checkId = window.setInterval(() => checkExpiration(auth), 60000);
      return () => window.clearInterval(checkId);
    }
    return () => {};
  }, [auth]); // WARNING: make sure this runs after index.html loads

  function checkExpiration(auth: any) {
    if (auth.exp <= Date.now() / 1000) {
      setstate(false);
      LogOutAction();
    }
    /**
    let newAuth = getSession();
    if (newAuth.exp * 1000 - new Date().getTime() < 10000) {
      if (actionCounter.getCount() > 0) {
        resetToken();
      } else {
        if (newAuth.exp * 1000 < new Date().getTime()) {
          setstate(false);
          LogOutAction();
        } else {
          toast.error("Your session is about to expire.", {
            closeOnClick: true,
            autoClose: false,
            onClick: () => resetToken(),
          });
        }
      }
    }
**/
  }

  const onClickClose = () => {
    setstate(false);
    LogOutAction();
  };
  async function onClickOk() {
    try {
      setstate(false);
      await resetToken();
    } catch (e) {
      ToastError(e);
    }
  }
  return (
    <>
      <Favicon
        url={
          window.location.hostname === "sages.variaventures.com"
            ? "https://omd-varia-sages.s3.us-east-2.amazonaws.com/favicon.ico"
            : tenantProfile.id === "varia"
            ? "https://s3.us-east-2.amazonaws.com/glassboardtech.com/tenant-assets/varia/varia-favicon.png"
            : "https://marketing-images-gbt.s3.us-east-2.amazonaws.com/favicon.ico"
        }
      />
      <div id={"main-content"}>
        <Switch>
          <Route path="/verify-email/:email" component={VerifyEmail} />

          <UnprotectedRoute path="/" exact comp={LoggedOut} />
          <UnprotectedRoute path="/register" comp={LoggedOut} />
          <UnprotectedRoute path="/forgot-credentials" comp={LoggedOut} />
          <UnprotectedRoute path="/reset-credentials" comp={LoggedOut} />
          <UnprotectedRoute path="/account-portal" comp={AccountChoicePage} />
          <UnprotectedRoute path="/invest-now" comp={LoggedOut} />

          <ProtectedRoute path="/dashboard" comp={Dashboard} roles={["*"]} />
          <ProtectedRoute
            path="/required-account-actions"
            comp={RequiredAccountActionsPage}
            roles={["*"]}
          />
          <ProtectedRoute
            path="/assets"
            comp={Assets}
            roles={["founder", "organizer", "dealer"]}
          />
          <ProtectedRoute path="/deals" comp={Deals} roles={["*"]} />
          <ProtectedRoute path="/signing" comp={Signing} roles={["signer"]} />
          <ProtectedRoute path="/profiles" comp={Profiles} roles={["*"]} />
          <ProtectedRoute
            path="/subscriptions"
            comp={Subscriptions}
            roles={["*"]}
          />
          <ProtectedRoute path="/closes" comp={Closes} roles={["*"]} />
          <ProtectedRoute
            path="/admin"
            comp={() => {
              let match = useRouteMatch();
              return AdminRoutes(match.path);
            }}
            roles={["admin"]}
          />

          <Route path="/logout" component={LogOut} exact={true} />
          <Route component={NotFound} />
        </Switch>
        <PopupModal
          open={state}
          size="tiny"
          heading="Session Expired"
          content={
            <p>For your protection, your session will expire in 1 minute</p>
          }
          closecaption="Logout now"
          okcaption="Stay Logged In"
          onClickClose={onClickClose}
          onClickOk={onClickOk}
        />
        <ErrorHandler />
      </div>
      {tenantProfile.id !== "varia" && <CopyrightFooter />}
    </>
  );
});

export default App;
