import useSwr, { mutate } from "swr";
import * as parseCookie from "./parseCookie";
import { TenantContext } from "../contexts";
import { useContext } from "react";
import axios from "axios";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;
const token = parseCookie.getToken("__session");

export const useRequest = <M = any>(path, ...rest) => {
  const tenantContext = useContext(TenantContext);
  return useSwr<M>(
    tenantContext.id && path ? path : null,
    (url) => {
      const token = parseCookie.getToken("__session");
      return axios
        .get(url, {
          baseURL: baseUrl,
          headers: {
            Authorization: `Bearer ${token}`,
            "x-tenant-id": tenantContext.id,
          },
        })
        .then((res) => res.data);
    },
    {
      shouldRetryOnError: false,
      ...rest,
    }
  );
};

/**
 * Used for Normalizing Listing Responses while we don't support true pagination
 * @param path
 * @param rest
 */
export const useNormalizedRequest = <M = any>(path, ...rest) => {
  const tenantContext = useContext(TenantContext);
  return useSwr<M>(
    tenantContext.id && path ? path : null,
    (url) => {
      const token = parseCookie.getToken("__session");
      return axios
        .get(url, {
          baseURL: baseUrl,
          headers: {
            Authorization: `Bearer ${token}`,
            "x-tenant-id": tenantContext.id,
          },
        })
        .then((res) => res.data?.data ?? res.data);
    },
    {
      shouldRetryOnError: false,
      ...rest,
    }
  );
};

export const doChange = async (
  path: string,
  body = undefined,
  method: "PATCH" | "PUT" | "POST" | "DELETE" = "PATCH",
  mutateLocally = false
) => {
  let url;
  if (path) {
    url = path.startsWith("http") ? path : baseUrl + path;
  }
  // Mutate local state immediately
  if (mutateLocally) {
    await mutate(path, body);
  }
  const response = await axios(url, {
    method: method,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    ...(body ? { data: body } : {}),
  });

  if (response && response.data) {
    if (mutateLocally) {
      await mutate(path, response.data);
    }
  }
  return response.data;
};

export const API = {
  SELF: "/users/me",
  SIGNING_REQUESTS: "/signing/requests",
  NOTIFICATIONS: "/notifications",
  SUBSCRIPTIONS: `/subscriptions`,
  PROFILES: `/profiles`,
  ASSETS: `/assets`,
  DISTRIBUTIONS: `/distributions`,
  ACCREDITATIONS: `/accreditations`,
  CLOSES: `/closes`,
  INVESTOR_PROFILES_OF_USER: `/investorprofiles-of-user`,
  FILES_UPLOAD: `/files/upload`,
  PING: `/ping`,
  INVITES: `/invites`,
  USERS_VERIFY_EMAIL: `/users/verify-email`,
  DEAL_BY_ID: (dealId) => waitForId(dealId, `/deals/${dealId}`),
  FEES_BY_DEAL_ID: (dealId) =>
    waitForId(dealId, `/deals/${dealId}/fees-statement`),
  CLOSES_BY_ID: (closeId) => waitForId(closeId, `/closes/${closeId}`),
  SUBSCRIPTION_BY_ID: (subscriptionId) =>
    waitForId(subscriptionId, `/subscriptions/${subscriptionId}`),
  DISTRIBUTION_BY_ID: (distributionId) =>
    waitForId(distributionId, `/distributions/${distributionId}`),
  ASSET_BY_ID: (assetId) => waitForId(assetId, `/assets/${assetId}`),
  ACCREDITATION_BY_ID: (accreditationId) =>
    waitForId(accreditationId, `/accreditations/${accreditationId}`),
  ENTITY_BY_ID: (entityId) => waitForId(entityId, `/entities/${entityId}`),
  ENTITY_BANK_ACCOUNT_BALANCE: (entityId) =>
    waitForId(entityId, `/entities/${entityId}/bank-account/balance`),
  ENTITY_DOCUMENT: (entityId, documentId) =>
    waitForId(entityId, `/entities/${entityId}/documents/${documentId}`),
  PROFILE_BY_ID: (profileId) => waitForId(profileId, `/profiles/${profileId}`),
  PROFILE_CREATE_KYCAML_APPLICANT: (profileId) =>
    waitForId(profileId, `/profiles/${profileId}/createKycAmlApplicant`),
  PROFILES_FOR_DEAL: (dealId) =>
    waitForId(dealId, `/profiles-for-deal/${dealId}`),
  ADD_FILTER: (url, filter) =>
    waitForId(url, `${url}?filter=${encodeURI(JSON.stringify(filter))}`),
  FULL_PATH: (url) => `${baseUrl}${url}`,
  NOTIFICATION_BY_ID: (notificationId) =>
    `${API.NOTIFICATIONS}/${notificationId}`,
  DECISIONS: (decisionId) => `/decisions/${decisionId}/evaluate`,
  CLOSE_AUDIT: `/closes/audit-potential-close`,
};

const waitForId = (id: string | null | undefined, builtPath: string) => {
  if (!id) {
    return null;
  }
  return builtPath;
};
