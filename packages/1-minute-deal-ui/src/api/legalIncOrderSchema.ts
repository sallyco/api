export interface LegalIncOrder {
  status: string | number;
  ein?: string;
  orderId: number;
  orderPlacedDate?: Date;
  orderFulfilledDate?: Date;
}
