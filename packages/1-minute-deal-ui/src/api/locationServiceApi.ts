import axios from "axios";

const hereApiKey =
  process.env.HERE_API_KEY ?? process.env.NEXT_PUBLIC_HERE_API_KEY;

export interface Suggestion {
  address: {
    country: string;
    state: string;
    county: string;
    city: string;
    district: string;
    street: string;
    houseNumber: string;
    unit: string;
    postalCode: string;
  };
  countryCode: string;
  label: string;
  language: string;
  locationId: string;
  matchLevel: string;
}

export async function getAutosuggest(query: string): Promise<Suggestion[]> {
  const url = "https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json";

  const { data } = await axios.get<any>(url, {
    transformRequest: (data, headers) => {
      delete headers.common["x-tenant-id"];
    },
    params: {
      apiKey: hereApiKey,
      query: query,
      country: "USA,CAN,MEX",
    },
  });

  return data.suggestions;
}
