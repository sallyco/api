import axios from "axios";
import * as parseCookie from "./parseCookie";
import actionCounter from "../tools/actionCounter";
const url = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const clearSessions = async () => {
  const temp = window.location.hostname.split(".").reverse();
  const root_domain = `.${temp[1]}.${temp[0]}`;
  // Clear Impersonation Cookies
  document.cookie = `__session=;  path=/; domain=${root_domain}; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
  document.cookie = `__sessionrefresh=; path=/; domain=${root_domain}; expires=Thu, 01 Jan 1970 00:00:00 GMT`;

  // Clear Application Login Cookies
  document.cookie = `__session=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
  document.cookie = `__sessionrefresh=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
};

export const getToken = async (email, pwd) => {
  await clearSessions();
  const authToken = await apiCallForToken(email, pwd);
  document.cookie = `__session=${authToken.data.access_token}`;
  document.cookie = `__sessionrefresh=${authToken.data.refresh_token}`;
  return true;
};

export const apiCallForToken = (email, pwd) => {
  const credentials = { username: email, password: pwd };
  return axios.post(`${url}/oauth2/token`, credentials);
};

export const resetToken = async () => {
  const token = parseCookie.getToken("__session");
  const authToken = await apiCallForresetToken(token);
  document.cookie = `__session=${authToken.data.access_token}`;
  document.cookie = `__sessionrefresh=${authToken.data.refresh_token}`;
  actionCounter.resetCount();
  return true;
};

export const apiCallForresetToken = (token) => {
  // return axios.get(`${url}/oauth2/token/refresh`, {
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //   },
  // });
  const tokenrefresh = parseCookie.getToken("__sessionrefresh");
  const credentials = {
    grant_type: "refresh_token",
    refresh_token: tokenrefresh,
  };
  const baseUrl = `${url}/oauth2/refresh`;
  const data = axios.post<any>(baseUrl, credentials, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
};
