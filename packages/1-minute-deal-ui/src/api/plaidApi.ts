import axios from "axios";
import * as parseCookie from "./parseCookie";

const url = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const getLinkToken = async () => {
  const token = parseCookie.getToken("__session");
  return await axios.get(`${url}/plaid/get-link-token`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const convertToAccount = async (data) => {
  const token = parseCookie.getToken("__session");
  return await axios.post(`${url}/plaid/convert-to-account`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const checkIfVaildPlaidLink = async (data) => {
  const token = parseCookie.getToken("__session");
  return await axios.post(`${url}/plaid/check-if-valid`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getUpdateLinkToken = async (data) => {
  const token = parseCookie.getToken("__session");
  return await axios.post(`${url}/plaid/get-update-link-token`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const resetLogin = async (data) => {
  const token = parseCookie.getToken("__session");
  return await axios.post(`${url}/plaid/reset-login`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
