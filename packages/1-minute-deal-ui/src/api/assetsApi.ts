import axios from "axios";
import { Individual } from "./individualSchema";
import * as parseCookie from "./parseCookie";
import { Deal } from "./dealsApi";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface TeamMember {
  name?: string;
  role?: string;
  linkedInUrl?: string;
  image?: string;
}

export interface Asset {
  id: string;
  ownerId?: string;
  // tenantId: string;
  dealId?: string;
  deal?: Deal;
  type?: string;
  subType?: string;
  unitCount?: number;
  allocationAmount?: number;
  name: string;
  sector: string;
  primaryContact?: Individual;
  companyMarketSector?: string;
  description?: string;
  size?: string;
  category?: string;
  advisors?: string;
  details?: [
    {
      heading: string;
      body: string;
    }
  ];
  properties?: [
    {
      key: string;
      value: string;
    }
  ];
  assetUrl?: string;
  appraisedValue?: number;
  fractionalOwnershipAmount?: number;
  artist?: {
    name?: string;
    location?: string;
    images?: string[];
  };
  contact?: {
    name?: string;
    email?: string;
    phone?: string;
  };
  funding?: {
    round?: string;
    targetRaiseAmount?: string;
    securityType?: string;
    preValuation?: string;
    noOfShare?: string;
    sharePrice?: string;
  };
  team?: TeamMember[];
  revenueHistory?: string[];
  costHistory?: string[];
  invitedOrganizers?: string[];
  founderIds?: string[];
  founderProfileId?: string;
  foundersAssessment?: object;
  files?: string[];
  images?: string[];
  video?: string;
  pitchDoc?: string;
  videoURL?: string;
  isPublic: boolean;
  logo?: string;
  banner?: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  isDeleted: boolean;
}

export interface AssetsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Asset[];
}

export async function getAssets(page = 1, perPage = 10): Promise<AssetsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/assets`;

  const { data } = await axios.get<AssetsList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getAssetById(assetId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/assets/${assetId}`;

  const { data } = await axios.get<Asset>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createAsset(asset: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/assets/`;

  const { data } = await axios.post<Asset>(url, asset, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateAsset(asset: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/assets/${asset.id}`;

  const { data } = await axios.put<Asset>(url, asset, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getAssetByDealId(dealId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/assets/by-deal-id/${dealId}`;

  const { data } = await axios.get<Asset>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
