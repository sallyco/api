import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface ContactListItem {
  name?: string;
  email?: string;
  phone?: string;
}

export interface ContactList {
  _id: string;
  ownerId: string;
  contacts: ContactListItem[];
  createAt: string;
  updatedAt: string;
}

export async function createContactList(contactList: {
  ownerId: string;
  contacts: ContactListItem[];
}) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/contact-lists/`;

  const { data } = await axios.post<ContactList>(url, contactList, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getContactListByOwnerId(ownerId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/contact-lists/by-owner/${ownerId}`;

  const { data } = await axios.get<ContactList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function putContactList(contactList: ContactList) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/contact-lists/${contactList._id}`;

  const { data } = await axios.put<ContactList>(url, contactList, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
