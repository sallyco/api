import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface File {
  id: string;
  name: string;
  type: string;
  key: string;
  lastModified: number;
  createdAt: string;
  updatedAt: string;
  generating?: boolean;
  isPublic?: boolean;
}

export interface FilesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: File[];
}

export async function getFiles(page = 1, perPage = 10): Promise<FilesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files`;

  const { data } = await axios.get<FilesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getFileById(fileId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files/${fileId}`;

  const { data } = await axios.get<File>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createFile(file: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files/`;

  const { data } = await axios.post<File>(url, file, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateFile(file: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files/${file.id}`;

  const { data } = await axios.put<File>(url, file, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function uploadFile(file: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files/upload`;
  const { data } = await axios.post<any>(url, file, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  });

  return data;
}

export async function uploadDocCompany(file: any, companyId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/documents-upload/${companyId}`;
  const { data } = await axios.put<any>(url, file, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  });

  return data;
}

export async function DeleteDocCompany(fileId: string, companyId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/${companyId}/delete-documents/${fileId}`;
  const { data } = await axios.delete<any>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}

export async function updateDocCompany(
  file: any,
  fileId: string,
  companyId: string
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/${companyId}/update-documents/${fileId}`;
  const { data } = await axios.put<any>(url, file, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  });

  return data;
}

export async function deleteFile(fileId: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/files/${fileId}`;

  const { data } = await axios.delete<File>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
