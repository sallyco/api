import axios from "axios";
import * as parseCookie from "./parseCookie";
import { Close } from "./closesApi";
import { Profile } from "./profilesApi";
import { ACHAccountDetails } from "../components/subscriptions/RefundSubscriptionForm";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

interface SubscriptionTransaction {
  id: string;
  ach_id?: string;
  amount: number;
  bankAccountTransactionId?: string;
  direction: "CREDIT" | "DEBIT";
}

export interface Subscription {
  bankAccount?: {
    counterpartyId: string;
    name: string;
    accountId: string;
    subAccountId: string;
  };
  id: string;
  ownerId: string;
  dealId: string;
  profileId?: string;
  transactionIds?: string[];
  transactions?: SubscriptionTransaction[];
  bankAccountTransactionId?: string;
  reverseTransactionId?: string;
  name: string;
  email: string;
  files: string[];
  status: string;
  isDocsSigned: boolean;
  isKycAmlPassed: boolean;
  amount: number;
  close?: Close;
  closeDoc?: string;
  createdAt: string;
  updatedAt: string;
  isDeleted?: boolean;
  acquisitionMethod?: string;
  additionalProperties?: any;
  profile?: Profile;
  signature?: string;
  signers?: {
    profileId?: string;
    email: string;
    name: string;
    dateSigned?: Date;
    signature?: string;
    signerId?: string;
  }[];
  totalRequiredSigners?: number;
  ownershipPercentageAtClose?: number;
  ownershipPercentageAtDistribution?: number;
  additionalFiles?: string[];
  sideLetter?: {
    managementFee?: {
      amount?: number;
      duration?: number;
      frequency?: string;
      percent?: number;
      type?: "percent" | "flat";
      feesPaymentMethod?: string;
      isRecurring?: boolean;
    };
    carryPercent?: {
      organizerCarryPercentage: number;
      [key: string]: number;
    };
  };
}

export interface SubscriptionsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Subscription[];

  dealId?: string;
}

export interface SubscriptionsDealMeta {
  investorLimitReached: boolean;
  includedInDeal: boolean;
  /** maxCommitmentAmount
   * 0 = no limit
   * -1 = deal is fully funded
   * > 0 = what's left for funding
   */
  maxCommitmentAmount: number;
}

export interface InvestorForUser {
  organizerId: string;
  investorId: string;
  investorName: string;
  investorEmail: string;
  subscriptionCount: number;
  amountSubscribed: number;
}

export interface InvestorSubscription {
  profileId: string;
  name: string;
  email: string;
  address: any;
  phone: string;
  dealId: string;
  dealName: string;
  dealStatus: string;
  subscriptionId: string;
  subscriptionStatus: string;
  subscribedOn: string;
  subscriptionAmount: number;
}

export async function getSubscriptions(
  page = 0,
  perPage = 10
): Promise<SubscriptionsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions`;

  const { data } = await axios.get<SubscriptionsList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getSubscriptionsByDeal(
  dealId,
  page = 0,
  perPage = 10,
  includeProfile = false
): Promise<SubscriptionsList> {
  const token = parseCookie.getToken("__session");
  let url = `${baseUrl}/subscriptions-by-deal/${dealId}`;
  if (includeProfile) {
    url += `?filter=${encodeURI(
      JSON.stringify({
        include: [
          {
            relation: "profile",
            scope: {
              include: [{ relation: "kycAml" }, { relation: "accreditations" }],
            },
          },
        ],
      })
    )}`;
  }

  const { data } = await axios.get<SubscriptionsList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getSubscriptionById(
  subscriptionId: string,
  filter?: any
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}${
    filter ? "?filter=" + encodeURIComponent(JSON.stringify(filter)) : ""
  }`;

  const { data } = await axios.get<Subscription>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getSubscriptionDealMeta(subscriptionId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/deal-meta`;

  const { data } = await axios.get<{ meta: SubscriptionsDealMeta }>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createSubscription(subscription: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/`;

  const { data } = await axios.post<Subscription>(url, subscription, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateSubscription(subscription: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscription.id}`;

  // Remove `null` properties to avoid triggering schema filter
  subscription = Object.entries(subscription).reduce(
    (a, [k, v]) => (v == null ? a : ((a[k] = v), a)),
    {}
  );

  // Remove related profile data (really need to be using PATCH)
  if (subscription.profile) {
    delete subscription.profile;
  }
  if (subscription.transactions) {
    delete subscription.transactions;
  }

  const { data } = await axios.put<Subscription>(url, subscription, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
export async function resetSubscription(subscriptionId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/reset`;

  const { data } = await axios.put<Subscription>(
    url,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function patchSubscription(
  subscriptionId: string,
  subscription: Partial<Subscription>
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}`;

  const { data } = await axios.patch<Subscription>(url, subscription, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function generateSigningDocument(
  subscriptionId: string,
  documentType: string,
  signature?: object
) {
  const token = parseCookie.getToken("__session");
  if (signature) {
    const url = `${baseUrl}/subscriptions/${subscriptionId}/signing-documents/${documentType}/sign`;

    const { data } = await axios.post<any>(url, signature, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  } else {
    const url = `${baseUrl}/subscriptions/${subscriptionId}/signing-documents/${documentType}`;

    const { data } = await axios.get<any>(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  }
}

export async function initSubscriptionACHTransfer(subscriptionId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/initTransfer`;

  const { data } = await axios.post<any>(
    url,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}
export async function initSubscriptionACHReverseTransfer(
  subscriptionId: string,
  accountDetails?: ACHAccountDetails
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/reverseTransfer`;

  const { data } = await axios.post<any>(
    url,
    {
      ...(accountDetails ? accountDetails : {}),
      ...(accountDetails.amount
        ? { amount: Number(accountDetails.amount) }
        : {}),
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function deleteSubscription(subscriptionId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/cancel`;

  const { data } = await axios.post<Subscription>(
    url,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function readSubscriptionByProfileId(profileId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${profileId}/subscriptions-meta`;

  const { data } = await axios.get<{ meta: SubscriptionsDealMeta }>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function searchBySubscription(
  search: any
): Promise<SubscriptionsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/search`;

  const { data } = await axios.post<SubscriptionsList>(url, search, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateSubscriptionSigners(
  subscriptionId: string
): Promise<SubscriptionsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/subscriptions/${subscriptionId}/update-signers`;

  const { data } = await axios.post<SubscriptionsList>(
    url,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}
