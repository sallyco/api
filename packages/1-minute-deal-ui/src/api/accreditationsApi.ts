import { ProviderEntity } from "./interfaces";
import { Profile } from "./profilesApi";

type AccreditationTypes = "506c" | "Self" | "QP";

export interface Accreditation extends ProviderEntity {
  certificationDate: string;
  expirationDate: string;
  files: string[];
  status:
    | "PENDING"
    | "CURRENT"
    | "THIRD_PARTY_PENDING"
    | "EXPIRED"
    | "REJECTED";
  type: AccreditationTypes;
  profileId: string;
  profile?: Profile;
}
