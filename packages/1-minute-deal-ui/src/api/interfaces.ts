export interface GenericEntity {
  id?: string;
  ownerId?: string;
  tenantId?: string;
  createdAt: string;
  updatedAt: string;
}
export interface ProviderEntity extends GenericEntity {
  providerMeta: {
    typeId: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}
export type NewEntity<T> = Omit<T, "id" | "ownerId" | "providerMeta">;

export interface EntityList<T> {
  totalCount: number;
  perPage: number;
  page: number;
  data: T[];
}
