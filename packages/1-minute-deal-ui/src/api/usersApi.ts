import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface User {
  id: string;
  username: string;
  enabled: boolean;
  totp: boolean;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  notBefore: number;
  createdTimestamp: string;
  contacts: object[];
  attributes: {
    phone: string;
    tenantId: string;
    termsOfService: string;
    files: string[];
    accountAdminId?: string;
  };
  type: string;
}

export async function getUsers(page = 1, perPage = 10): Promise<User[]> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/users`;

  const { data } = await axios.get<User[]>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getUserById(userId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/users/${userId}`;

  const { data } = await axios.get<User>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getSelf() {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/users/me`;

  const { data } = await axios.get<User>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getUserByEmail(userEmail: string) {
  //const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/users-by-email/${userEmail}`;

  const { data } = await axios.get<User>(url, {
    headers: {},
  });

  return data;
}

export async function createUser(user: any) {
  const url = `${baseUrl}/users/`;

  const { data } = await axios.post<any>(url, user, {
    headers: {},
  });

  return data;
}

export async function updateUser(user: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/users/${user.id}`;

  const { data } = await axios.put<User>(url, user, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export const getUserProfiles = () => {
  const token = parseCookie.getToken("__session");
  return axios.get(`${baseUrl}/users/:id/profiles`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const createUserProfile = (userId, profile) => {
  const token = parseCookie.getToken("__session");
  return axios.post(`${baseUrl}/users/${userId}/profiles`, profile, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const requsetPassword = (email) => {
  return axios.post(`${baseUrl}/users/request-password`, email, {
    headers: {},
  });
};

export const resetPassword = (token) => {
  return axios.post(`${baseUrl}/users/reset-password`, token, {
    headers: {},
  });
};

export const verifyEmail = (email) => {
  return axios.post(`${baseUrl}/users/verify-email`, email, {
    headers: {},
  });
};

export const getUserEmailExist = (email): Promise<any> => {
  const result: any = axios
    .get(`${baseUrl}/users-by-email/${email}`, {
      headers: {},
    })
    .catch(function (error) {
      if (error.response) {
        return error.response;
      }
    });
  return result;
};

export async function impersonateUser(realmId: string, userId: string) {
  const token = parseCookie.getToken("__adminsession");
  const url = `${baseUrl}/users/${realmId}/${userId}/impersonate/`;

  const { data } = await axios.post<any>(url, "", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
