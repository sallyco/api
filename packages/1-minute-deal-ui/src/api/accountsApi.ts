import axios from "axios";
import * as parseCookie from "./parseCookie";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Account {
  id: string;
  ownerId: string;
  type: string;
  [prop: string]: any;
}

export interface IPlaidAccount extends Account {
  institution: IPlaidInstitution;
  account: Account;
  account_id: null;
  accounts: IPlaidBankAccount[];
  link_session_id: string;
  public_token: string;
}

export interface IPlaidBankAccount {
  id: null | string;
  name: null | string;
  type: null | string;
  subtype: null | string;
  mask: null | string;
}

export interface IPlaidInstitution {
  name: string;
  institution_id: string;
}

export interface AccountsList {
  totalCount: number;
  data: Account[];
}

export async function getAccounts(
  page = 1,
  limit = 10,
  filter?: object
): Promise<AccountsList> {
  const token = parseCookie.getToken("__session");
  const pagingFilter = {
    limit: limit,
    skip: page * limit - limit,
  };
  const url = `${baseUrl}/accounts?filter=${encodeURI(
    JSON.stringify({ ...pagingFilter, ...filter })
  )}`;

  const { data, headers } = await axios.get<Account[]>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return {
    totalCount: headers["x-total-count"],
    data: data,
  };
}

export async function getPlaidAccounts(
  page = 1,
  limit = 10
): Promise<AccountsList> {
  return getAccounts(page, limit, { where: { type: "PLAID" } });
}

export async function getAccountById(accountId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/accounts/${accountId}`;

  const { data } = await axios.get<Account>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function deleteAccountById(accountId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/accounts/${accountId}`;

  const { data } = await axios.delete<Account>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function deleteSubAccountById(
  accountId: string,
  subAccountId: string
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/accounts/${accountId}/${subAccountId}`;

  const { data } = await axios.delete<Account>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
