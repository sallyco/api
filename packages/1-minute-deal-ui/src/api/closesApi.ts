import axios from "axios";
import * as parseCookie from "./parseCookie";
import { Subscription } from "./subscriptionsApi";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Close {
  id: string;
  // ownerId: string;
  // tenantId?: string;
  dealId: string;
  entityId: string;
  files: string[];
  dateClosed?: Date;
  fundsWiredDate?: Date;
  readyToCounterSign?: boolean;
  fundManagerSigned?: boolean;
  formD?: {
    cik?: string;
    cikPassword?: string;
    ccc?: string;
    accessionNumber?: string;
    fileNumber?: string;
    filedDate?: Date;
  };
  blueSky?: {
    filedDate?: Date;
  };
  purchaseDocsSignedDate?: Date;
  subscriptions?: string[];
  statement?: object;
  sequence?: number;
  createdAt: string;
  updatedAt: string;
}

export interface ClosesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Close[];

  dealId?: string;
}

export async function getCloses(page = 1, perPage = 10): Promise<ClosesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes`;

  const { data } = await axios.get<ClosesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getClosesByDeal(
  dealId,
  page = 1,
  perPage = 10
): Promise<ClosesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes-by-deal/${dealId}`;

  const { data } = await axios.get<ClosesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getCloseById(closeId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes/${closeId}`;

  const { data } = await axios.get<Close>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createClose(
  dealId: string,
  organizerSignature: string,
  statement?: object,
  subscriptions?: string[]
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes`;

  const { data } = await axios.post(
    url,
    {
      dealId: dealId,
      organizerSignature: organizerSignature,
      statement: statement ?? null,
      subscriptions: subscriptions ?? [],
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
}

export async function updateClose(close: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes/${close.id}`;

  const { data } = await axios.put<Close>(url, close, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function generateClosingDocument(
  closeId: string,
  documentType: string,
  payload: any
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes/${closeId}/generate-closing-document/${documentType}`;

  const { data } = await axios.post<any>(url, payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function fundManagerSign(
  closeId: string,
  managerSignature: string
) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/closes/${closeId}/fund-manager-sign`;

  const { data } = await axios.post<Close>(
    url,
    {
      managerSignature,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}
