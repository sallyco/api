import axios from "axios";
import * as parseCookie from "./parseCookie";
import { Deal } from "./dealsApi";
import { Subscription } from "./subscriptionsApi";
import { Invite } from "./invitesApi";
import { User } from "./usersApi";
import { Asset } from "./assetsApi";
const url = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

interface EmailData {
  to: string;
  subject: string;
  templateId: string;
}

export const sendEmail = (message) => {
  const token = parseCookie.getToken("__session");
  return axios.post(`${url}/email/sendEmail`, message, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export interface NewAccountEmailData extends EmailData {
  recipient: {
    firstName: string;
    email: string;
  };
  verifyUrl: string;
}

export const sendVerifyAccountEmail = async (
  email: string,
  firstName: string
) => {
  const emailData: NewAccountEmailData = {
    templateId: "newAccount",
    to: email,
    subject: "Verify New Account",
    recipient: {
      firstName: firstName,
      email: email,
    },
    verifyUrl: window.location.origin + "/#/verify-email/" + email,
  };
  await sendEmail(emailData);
};

export interface ResetPasswordEmailData extends EmailData {
  recipient: {
    email: string;
  };
  resetUrl: string;
}

export const sendResetPasswordEmail = async (email: string, token: string) => {
  const emailData: ResetPasswordEmailData = {
    templateId: "resetPassword",
    to: email,
    subject: "Password Reset",
    recipient: {
      email: email,
    },
    resetUrl:
      window.location.href.split("#")[0] + "#/reset-credentials?token=" + token,
  };
  await sendEmail(emailData);
};

export interface NudgeInvestorEmailData extends EmailData {
  recipient: {
    email: string;
    firstName: string;
  };
  deal: {
    name: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
  subscription: {
    status: string;
  };
}

export const sendNudgeInvestorEmail = async (
  toUser: User,
  fromEmail: string,
  deal: Deal,
  subscription: Subscription,
  subscriptionStatus: string,
  invite: Invite
) => {
  const emailData: NudgeInvestorEmailData = {
    templateId: "nudgeInvestor",
    subject: `Deal Reminder for ${deal.name}`,
    to: toUser.email,
    recipient: {
      email: toUser.email,
      firstName: toUser.firstName,
    },
    deal: {
      name: deal.name,
    },
    sender: {
      email: fromEmail,
    },
    subscription: {
      status: subscriptionStatus,
    },
    returnUrl:
      subscriptionStatus === "INVITED"
        ? window.location.href.split("#")[0] + "#/register/" + invite.id
        : window.location.href.split("#")[0] +
          "#/subscriptions/" +
          subscription.id,
  };
  await sendEmail(emailData);
};

export interface InviteInvestorEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
    id: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export const sendInviteInvestor = async (
  toEmail: string,
  fromEmail: string,
  deal: Deal,
  invite: Invite
) => {
  const dealName = deal.name.toLowerCase();
  const emailData: InviteInvestorEmailData = {
    deal: {
      name: deal.name,
      id: deal.id,
    },
    sender: {
      email: fromEmail,
    },
    templateId: "inviteInvestor",
    to: toEmail,
    subject: `Investment Invitation for ${deal.name}`,
    recipient: {
      email: toEmail,
    },
    returnUrl: window.location.href.split("#")[0] + "#/register/" + invite.id,
  };
  await sendEmail(emailData);
};
export const sendInviteInvestorExisting = async (
  toUser: User,
  fromEmail: string,
  deal: Deal,
  subscription: Subscription | { id: string }
) => {
  const emailData: InviteInvestorEmailData = {
    deal: {
      name: deal.name,
      id: deal.id,
    },
    sender: {
      email: fromEmail,
    },
    templateId: "inviteInvestorExisting",
    to: toUser.email,
    subject: `Investment Invitation for ${deal.name}`,
    recipient: {
      email: toUser.email,
      firstName: toUser.firstName,
    },
    returnUrl:
      window.location.href.split("#")[0] + "#/subscriptions/" + subscription.id,
  };
  await sendEmail(emailData);
};

export const sendInviteAccountAdminUser = async (
  toEmail: string,
  fromEmail: string,
  recipient: {
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
    linkedinURL?: string;
  },
  invite: Invite
) => {
  const emailData = {
    sender: {
      email: fromEmail,
    },
    templateId: "inviteAccountAdminUser",
    to: toEmail,
    subject: "Account Administration Invitation",
    recipient: recipient,
    returnUrl: window.location.href.split("#")[0] + "#/register/" + invite.id,
  };
  await sendEmail(emailData);
};

// Example Usage
// let signInvite: any = await dispatch(
//   insertInvite({
//     email: additionalSignerEmail,
//     id: multiSignerProfileId,
//     type: "profile",
//     relation: "individual",
//     role: "investor",
//   })
// );
// await sendInviteSigner(
//   invite.acceptableBy,
//   fromEmail,
//   deal,
//   signInvite,
//   existingUser,
//   subscription.id
// );
export const sendInviteSigner = async (
  toEmail: string,
  fromEmail: string,
  deal: Deal,
  invite: Invite,
  isExistingUser = false,
  subscriptionId = ""
) => {
  const subscriptionParam = subscriptionId ? `/${subscriptionId}` : "";
  const emailData: InviteInvestorEmailData = {
    deal: {
      name: deal.name,
      id: deal.id,
    },
    sender: {
      email: fromEmail,
    },
    templateId: "inviteSigner",
    to: toEmail,
    subject: `Investment Signing Invitation for ${deal?.name ?? ""}`,
    recipient: {
      email: toEmail,
    },
    returnUrl: !isExistingUser
      ? window.location.href.split("#")[0] + "#/register/" + invite.id
      : window.location.href.split("#")[0] +
        `#/subscriptions${subscriptionParam}`,
  };
  await sendEmail(emailData);
};

export interface InviteFounderEmailData extends EmailData {
  recipient: {
    email: string;
    firstName?: string;
  };
  deal: {
    name: string;
    id: string;
  };
  asset: {
    name: string;
    id: string;
  };
  sender: {
    email: string;
  };
  returnUrl: string;
}

export const sendInviteFounder = async (
  toEmail: string,
  fromEmail: string,
  deal: Deal,
  company: Asset,
  invite: Invite,
  isExistingUser = false
) => {
  const assetLink = company ? `${company?.id}` : "";
  const emailData: InviteFounderEmailData = {
    deal: {
      name: deal?.name,
      id: deal?.id,
    },
    asset: {
      name: company?.name,
      id: company?.id,
    },
    sender: {
      email: fromEmail,
    },
    templateId: "inviteFounder",
    to: toEmail,
    subject: `Company Asset Management Invite for ${deal?.name ?? ""}`,
    recipient: {
      email: toEmail,
    },
    //TODO: UPDATE THIS
    returnUrl: !isExistingUser
      ? window.location.href.split("#")[0] + "#/register/" + invite.id
      : window.location.href.split("#")[0] + `#/assets/companies/${assetLink}`,
  };
  return sendEmail(emailData);
};

export interface ContactOrganizerEmailData extends EmailData {
  recipient: {
    email: string;
    firstName: string;
  };
  deal: {
    name: string;
  };
  sender: {
    name: string;
    email: string;
    phone: string;
  };
  messageContent: string;
  returnUrl: string;
}

export const sendContactOrganizerEmail = async (
  toUser: User,
  sender: {
    name: string;
    email: string;
    phone: string;
  },
  deal: Deal,
  messageContent: string
) => {
  const emailData: ContactOrganizerEmailData = {
    templateId: "contactOrganizer",
    subject: `Message from ${sender.name}`,
    to: toUser.email,
    recipient: {
      email: toUser.email,
      firstName: toUser.firstName,
    },
    deal: {
      name: deal.name,
    },
    sender: sender,
    messageContent,
    returnUrl: window.location.href.split("#")[0] + `#/deals/${deal.id}/1`,
  };
  await sendEmail(emailData);
};
