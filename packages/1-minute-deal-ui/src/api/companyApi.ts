import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Team {
  firstName?: string;
  lastName?: string;
  role?: string;
  linkedinUrl?: string;
  headshot?: string;
}

export interface Company {
  id: string;
  ownerId: string;
  type: string; //enum: ['FUND_MANAGER', 'REGISTERED_AGENT'],
  entityType: string;

  name: string;
  website?: string;
  email?: string;
  phone?: string;
  address?: {
    address1?: string;
    address2?: string;
    city?: string;
    state?: string;
    postalCode?: string;
    country?: string;
  };
  seriesNumber?: number;
  seriesPrefix?: string;
  arbitrationCity?: string;
  arbitrationState?: string;
  stateOfFormation?: string;

  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  properties?: { key: string; value: string }[];
}

export interface CompaniesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Company[];
}

export async function getCompanies(
  page = 1,
  perPage = 10
): Promise<CompaniesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies`;

  const { data } = await axios.get<CompaniesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getCompanyById(companyId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/${companyId}`;

  const { data } = await axios.get<Company>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createCompany(company: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/`;

  const { data } = await axios.post<Company>(url, company, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateCompany(company: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/companies/${company.id}`;

  const { data } = await axios.put<Company>(url, company, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
