import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Tenant {
  id: string;
  inverted: boolean;
  name: string;
  url: string;
  masterEntityId?: string;
  managerId?: string;
  assets: TenantAssetsState;
  settings: TenantSettingsState;
  bankingLimits: TenantBankingLimits;
  dealLimits?: TenantDealLimits;
  realm?: TenantRealm;
  billingContact?: string;
  whiteLabel?: TenantAssetsState & object;
}
interface TenantRealm {
  verifyEmail: boolean;
}
interface TenantAssetsState {
  images: TenantAssetsImages;
  colors: TenantAssetsColors;
}

interface TenantAssetsImages {
  logo: string;
  logoInverted?: string;
  favicon?: string;
}

interface TenantAssetsColors {
  primaryColor: string;
  secondaryColor: string;
}
interface TenantSettingsState {
  emails?: TenantSettingsEmails;
  showCarryRecipientsForInvestors?: boolean;
  use506cAccreditation?: boolean;
  parallelMarketsClientId?: string;
}

interface TenantSettingsEmails {
  fromAddress?: string;
}

// daily and transaction are both $ amount (integer)
interface TenantBankingLimitSettings {
  daily: number;
  transaction: number;
}

interface TenantBankingLimits {
  ach?: {
    credit?: TenantBankingLimitSettings;
    debit?: TenantBankingLimitSettings;
    exposure?: number;
  };
  wire?: {
    credit?: TenantBankingLimitSettings;
    debit?: TenantBankingLimitSettings;
    exposure?: number;
  };
}

interface TenantDealLimits {
  maxDealCount?: number;
  maxInvestorCount?: number;
  maxTargetRaiseAmount?: number;
  maxExpenseReserve?: number;
  perDealMaxInvestorCount?: number;
  perDealMaxTargetRaiseAmount?: number;
}

export async function getTenantById(tenantId: string): Promise<Tenant> {
  const encodedFilter = encodeURIComponent(
    JSON.stringify({
      include: [
        {
          relation: "realm",
          scope: {
            fields: ["verifyEmail"],
          },
        },
        {
          relation: "whiteLabel",
        },
      ],
    })
  );
  const url = `${baseUrl}/tenants/${tenantId}?filter=${encodedFilter}`;

  const { data } = await axios.get<Tenant>(url, {
    headers: {},
  });

  return data;
}

export async function getTenantThemeById(tenantId: string): Promise<string> {
  //TODO (Andrew): Yoooooooo! I heard you like hacks! Get a load of this guy! (1 of 2)
  if (window.location.hostname === "sages.variaventures.com") {
    const { data } = await axios.get(
      "https://omd-varia-sages.s3.us-east-2.amazonaws.com/varia-sages.theme.css",
      {
        transformRequest: (data, headers) => {
          delete headers.common["x-tenant-id"];
        },
      }
    );
    return data;
  }
  ////////

  const url = `${baseUrl}/tenants/${tenantId}/theme`;
  const token = parseCookie.getToken("__session");

  try {
    const { data } = await axios.get(url, {
      headers: {
        Authorization: `Bearer ${token}`,
        "x-tenant-id": tenantId,
      },
    });

    return data;
  } catch (err) {
    const { data } = await axios.get(
      "https://cdn.jsdelivr.net/npm/semantic-ui@2/dist/semantic.min.css",
      {
        transformRequest: (data, headers) => {
          delete headers.common["x-tenant-id"];
        },
      }
    );

    return data;
  }
}

export async function updateTenant(tenant: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/tenants/${tenant.id}`;

  const { data } = await axios.put<Tenant>(url, tenant, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
