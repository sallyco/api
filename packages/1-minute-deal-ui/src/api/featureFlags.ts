import axios from "axios";
import { FeatureFlagInstance } from "../components/featureflags/FeatureFlags";
import * as parseCookie from "./parseCookie";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export async function getFeatureFlags(): Promise<FeatureFlagInstance[]> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/feature-flags`;

  const { data } = await axios.get<FeatureFlagInstance[]>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
