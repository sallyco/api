import axios from "axios";
import * as parseCookie from "./parseCookie";
import { Individual } from "./individualSchema";
import { Entity } from "./entitiesApi";
import { DEAL_DISPLAYED_STATUSES } from "../tools/dealStatuses";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Deal {
  id: string;
  gbtRole?: string;
  ownerId: string;
  owner?: {
    email: string;
    firstName: string;
    lastName: string;
    id: string;
  };
  //tenantId: string;
  coOwnerIds?: string[];
  entityId?: string;
  assetIds?: string[];
  profileId: string;
  name: string;
  description: string;
  // assetUrl: string;
  marketing?: {
    logo?: string;
    tagline?: string;
  };
  portfolioCompanyName: string;
  portfolioCompanyState: string;
  portfolioCompanyEntity: string;
  targetRaiseAmount: number;
  previouslyRaisedAmount?: number;
  securityType?: string;
  isPublic: boolean;
  files?: string[];
  organizerFiles?: string[];
  estimatedCloseDate?: Date;
  organizerCarryPercentage?: number;
  additionalCarryRecipients?: {
    individual: Individual;
    carryPercentage: number;
  }[];
  portfolioCompanyContact?: {
    firstName?: string;
    lastName?: string;
    email?: string;
  };
  status: string | DEAL_DISPLAYED_STATUSES;
  closes?: string[];
  requireQualifiedPurchaser?: boolean;
  ownersDealUrl?: string;
  createdAt: string;
  updatedAt: string;
  entity?: Entity;
  distribution?: Distribution;
}

export interface Distribution {
  id?: string;
  ownerId?: string;
  tenantId?: string;
  docs?: DistributionDocuments;
  purchaseDetails?: PurchaseDetails;
  dealId?: string;
}
interface DistributionDocuments {
  letterOfIntent?: string[];
  memorandumOfUnderstanding?: string[];
  definitivePurchaseAgreement?: string[];
}
interface PurchaseDetails {
  assetId?: string;
  purchaseUnit: string;
  purchaseAmount: string;
  proceeds?: object[];
  expenses?: object[];
}

export interface DealsList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Deal[];
}

export async function getDeals(page = 0, perPage = 10): Promise<DealsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals`;

  const { data } = await axios.get<DealsList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getDealById(dealId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals/${dealId}`;

  const { data } = await axios.get<Deal>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createDeal(deal: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals/`;

  const { data } = await axios.post<Deal>(url, deal, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateDeal(deal: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals/${deal.id}`;

  if (deal.entity) {
    delete deal.entity;
  }
  if (deal.profile) {
    delete deal.profile;
  }

  const { data } = await axios.put<Deal>(url, deal, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function searchByDeal(search: any): Promise<DealsList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals/search`;

  const { data } = await axios.post<DealsList>(url, search, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function atLimit() {
  const token = parseCookie.getToken("__session");

  const { data } = await axios.get<boolean>(`${baseUrl}/deals/at-limit`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}

export async function feesStatementForDeal(dealId) {
  const token = parseCookie.getToken("__session");

  const { data } = await axios.get<any>(
    `${baseUrl}/deals/${dealId}/fees-statement`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        "Cache-Control": "no-cache",
      },
    }
  );
  return data;
}

export interface DealPerformance {
  targetAmount: number;
  committedAmount: number;
  raisedAmount: number;
}
export async function getDealPerformanceSummary(dealId) {
  const token = parseCookie.getToken("__session");

  const { data } = await axios.get<DealPerformance>(
    `${baseUrl}/deals/${dealId}/performance-summary`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
}

export async function readDealsByProfileId(profileId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/deals/${profileId}/deals-meta`;

  const { data } = await axios.get<{ meta: DealsList }>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
