import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Invite {
  id: string;
  acceptableBy: string;
  invitedTo: [
    {
      id: string;
      type: string;
      relation: string;
    }
  ];
  invitedRole: string;
  redirectUrl?: string;
  acceptedAt: string;
  createdAt: string;
  updatedAt: string;
  isDeleted: boolean;
}

export interface InvitesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Invite[];

  dealId?: string;
}

export async function getInvites(page = 1, perPage = 10): Promise<InvitesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/invites`;

  const { data } = await axios.get<InvitesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getInvitesByDeal(
  dealId,
  page = 1,
  perPage = 10
): Promise<InvitesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/invites-by-deal/${dealId}`;

  const { data } = await axios.get<InvitesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getInviteById(inviteId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/invites/${inviteId}`;

  const { data } = await axios.get<Invite>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

/** Observations:
 * companyId is used in InviteOrganizersPage
 * profileId is desired
 *
 * Change - set id instead of specific one
 */
interface CreateInviteProps {
  email: string;
  id: string;
  type: string;
  relation: string;
  role: string;
}
export async function createInvite(invite: CreateInviteProps) {
  const inviteObj = {
    acceptableBy: invite.email,
    invitedTo: [
      {
        id: invite.id,
        type: invite.type,
        relation: invite.relation,
      },
    ],
    invitedRole: invite.role,
  };
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/invites/`;

  const { data } = await axios.post<Invite>(url, inviteObj, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateInvite(invite: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/invites/${invite.id}`;

  const { data } = await axios.put<Invite>(url, invite, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function acceptInvite(inviteId: string, userId: string) {
  const url = `${baseUrl}/invites/${inviteId}/accept?userId=${userId}`;

  const { data } = await axios.post(url, {});
  return data;
}

export async function deleteInvite(inviteId: string) {
  const url = `${baseUrl}/invites/${inviteId}`;
  const token = parseCookie.getToken("__session");

  const { data } = await axios.delete(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}
