import { Profile } from "./profilesApi";

/**
 *
 * @param profile
 * @returns string: entity name, first and last name, or just first or just last name
 */
export function getProfileName(profile: Profile): string {
  // This handles runtime errors where async calls haven't filled in "profile" yet.
  if (!profile) {
    return "";
  }
  // Prefer 'name' before using signer's "firtstName lastName"
  const { name, firstName, lastName, displayName, taxDetails } = profile;
  // Prefer 'displayName' before any other
  if (displayName) return `${displayName}`;

  if (taxDetails && taxDetails.registrationType) {
    if (taxDetails.registrationType === "INDIVIDUAL") {
      if (firstName && lastName) return `${firstName} ${lastName}`;
      if (lastName) return `${lastName}`;
      if (firstName) return `${firstName}`;
    }
    return `${name}`;
  }

  if (name) return `${name}`;
  if (firstName && lastName) return `${firstName} ${lastName}`;
  if (lastName) return `${lastName}`;
  if (firstName) return `${firstName}`;
  return "";
}
