import axios from "axios";
import * as parseCookie from "./parseCookie";
import store from "../store";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Notification {
  id: string;
  tenantId: string;
  userId: string;
  event: {
    type: string;
    id: string;
  };
  category: "DEAL_STATUS" | "INVESTMENTS" | "GENERAL" | "CLOSING_ACTIVITY";
  message: string;
  acknowledged: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface NotificationList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Notification[];
}

export async function getNotifications(
  page = 1,
  perPage = 10
): Promise<NotificationList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/notifications`;

  const { data } = await axios.get<NotificationList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getNotificationById(notificationId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/notifications/${notificationId}`;

  const { data } = await axios.get<Notification>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createNotification(notification: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/notifications/`;

  const { data } = await axios.post<Notification>(url, notification, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateNotification(notification: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/notifications/${notification.id}`;

  const { data } = await axios.put<Notification>(url, notification, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
