import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface EmailTemplate {
  id: string;
  tenantId: string;
  ownerId: string;
  key: string;
  data: {};
}

export async function getEmailTemplate(templateKey): Promise<EmailTemplate> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/emailtemplates/${templateKey}`;

  const { data } = await axios.get<EmailTemplate>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateEmailTemplate(emailTemplate: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/emailtemplates/${emailTemplate.id}`;

  const { data } = await axios.put<EmailTemplate>(url, emailTemplate, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
