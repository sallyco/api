import axios from "axios";
import * as parseCookie from "./parseCookie";
import { Individual } from "./individualSchema";
import { Deal } from "./dealsApi";
import { Accreditation } from "./accreditationsApi";
import { EntityList, GenericEntity } from "./interfaces";
import { API } from "./swrApi";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Signer {}

export interface ProfileBankingUser {
  id?: string;
  accounts: ProfileBankAccount[];
}
export interface ProfileBankAccount {
  isDefaultForDistributions?: boolean;
  counterpartyId: string;
  providerMeta?: {
    typeId?: string;
    accountStatus?: string;
    accountApplicationId?: string;
    accountId?: string;
    [prop: string]: any;
  };
  bankName: string;
  accountNumber?: string;
  routingNumber?: string;
  accountName?: string;
  accountId: string;
  subAccountId: string;
}

export interface Profile extends GenericEntity {
  profileType?: string | "FOUNDER" | "ORGANIZER" | "INVESTOR";
  stateOfFormation?: string;
  countryOfFormation?: string;
  displayName?: string;
  name?: string;
  jointAccountName?: string;
  typeOfEntity?:
    | "LIMITED_LIABILITY_COMPANY"
    | "LIMITED_PARTNERSHIP"
    | "C_CORPORATION"
    | "S_CORPORATION"
    | "GENERAL_PARTNERSHIP"
    | "501_C_NONPROFIT"
    | "FOREIGN_ENTITY";
  firstName?: string;
  lastName?: string;
  address?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };
  deliveryAddress?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };
  phone?: string;
  dateOfBirth?: string;
  dateOfFormation?: string;
  email?: string;
  image: string;
  taxDetails?: {
    registrationType?: "INDIVIDUAL" | "ENTITY" | "TRUST" | "JOINT";
    taxIdentification: {
      type: string;
      value: string;
    };
  };
  passportId?: string;
  title?: string;
  isUSBased?: boolean;
  isRIA?: boolean;
  isERA?: boolean;
  ERAStatus?: string;
  primaryContact?: Partial<Individual>;
  isSingleMemberLLC?: boolean;
  primarySignatory?: Partial<Individual>;
  additionalSignatories?: Individual[];
  beneficialOwner?: Partial<Individual>;
  signers?: Partial<Individual>[];
  kycAml?: {
    applicantId?: string;
    checkId: string;
    lastDateChecked?: Date;
    status: string;
    result: string;
    providerMeta: {
      [key: string]: any;
    };
  };
  deals: Deal[];
  investorStatus?: Number[];
  bankingUser?: ProfileBankingUser;
  masterEntityId?: string;
  managerId?: string;
  purchaserStatus?: Number[];
  createdAt: string;
  updatedAt: string;
  accreditations?: Accreditation[];
  brokerageAccount?: {
    brokerName: string;
    dtcNumber: string;
    accountNumber: string;
  };
}

export async function getProfiles(
  page = 1,
  perPage = 10
): Promise<EntityList<Profile>> {
  const token = parseCookie.getToken("__session");
  const url = API.ADD_FILTER(`${baseUrl}/profiles`, {
    include: [
      {
        relation: "accreditations",
      },
    ],
  });

  const { data } = await axios.get<EntityList<Profile>>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}

export async function getProfileById(profileId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/profiles/${profileId}`;

  const { data } = await axios.get<Profile>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createProfile(profile: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/profiles`;
  const { data } = await axios.post<Profile>(url, profile, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}

export async function updateProfile(profile: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/profiles/${profile.id}`;

  const { data } = await axios.put<Profile>(url, profile, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export const createKycAmlCheck = (profileId) => {
  const token = parseCookie.getToken("__session");
  return axios.post(
    `${baseUrl}/profiles/${profileId}/createKycAmlApplicant`,
    "",
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const updateKycAmlCheck = (profileId) => {
  const token = parseCookie.getToken("__session");
  return axios.post(
    `${baseUrl}/profiles/${profileId}/updateKycAmlApplicant`,
    "",
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export async function addBankingUser(profileId, data) {
  const token = parseCookie.getToken("__session");
  return axios.post(`${baseUrl}/profiles/${profileId}/addBankingUser`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export async function addBankingAccount(profileId, accountId, subAccountId) {
  const token = parseCookie.getToken("__session");
  return axios.post(
    `${baseUrl}/profiles/${profileId}/addBankAccount/${accountId}/${subAccountId}`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
}

export async function deleteProfileByProfileId(profileId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/profiles/${profileId}`;

  const { data } = await axios.delete<Profile>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function deleteBankingAccount(profileId, accountId, subAccountId) {
  const token = parseCookie.getToken("__session");
  return axios.delete(
    `${baseUrl}/profiles/${profileId}/removeBankAccount/${accountId}/${subAccountId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
}
