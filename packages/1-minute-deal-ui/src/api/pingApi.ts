import axios from "axios";
const url = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const apiCallPing = () => {
  return axios.get(`${url}/ping`);
};
