import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Team {
  id: string;
  ownerId: string;
  organizers: string[];
}

export async function getTeamByOwnerId(ownerId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/teams?filter[where][ownerId]=${ownerId}`;

  const { data } = await axios.get<Team>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createTeam(team: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/teams`;

  const { data } = await axios.post<Team>(url, team, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateTeam(team: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/teams/${team.id}`;

  const { data } = await axios.put<Team>(url, team, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
