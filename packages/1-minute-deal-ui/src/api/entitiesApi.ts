import axios from "axios";
import * as parseCookie from "./parseCookie";
import { LegalIncOrder } from "./legalIncOrderSchema";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface Entity {
  id: string;
  ownerId?: string;
  // tenantId?: string;
  dealId: string;
  name: string;
  entityType: string;
  regulationType: string;
  regDExemption: string;
  minInvestmentAmount: number;
  countryOfFormation: string;
  stateOfFormation?: string;
  entityDocuments?: {
    operatingAgreement?: string;
    privatePlacementMemorandum?: string;
    subscriptionAgreement?: string;
    einLetter?: string;
    purchaseAgreement?: string[];
  };
  files?: string[];
  ein?: string;
  assetComposition?: string;
  arbitration?: {
    city: string;
    state: string;
    country: string;
  };
  registeredAgent?: string;
  managerId?: string;
  masterEntityId?: string;
  bankAccount?: {
    providerMeta?: {
      typeId?: string;
      accountStatus?: string;
      accountApplicationId?: string;
      accountId?: string;
      [props: string]: any;
    };
    bankName: string;
    bankAddress: string;
    bankContact?: {
      name?: string;
      phone?: string;
      email?: string;
    };
    accountNumber: string;
    routingNumber: string;
    swiftCode: string;
    beneficiaryAddress: string;
    beneficiaryPhone: string;
    accountName: string;
    createdAt?: Date;
    updatedAt?: Date;
  };
  managementFee?: {
    amount?: number;
    duration?: number;
    frequency?: string;
    percent?: number;
    type?: string;
    feesPaymentMethod?: string;
    isRecurring?: boolean;
  };
  expenseReserve?: {
    amount?: number;
    type?: string;
  };
  LegalIncOrder?: Partial<LegalIncOrder>;
  createdAt: string;
  updatedAt: string;
  // Not actually entity model, but attached here anyway
  transactions?: object[];
}

export interface EntitiesList {
  totalCount: number;
  perPage: number;
  page: number;
  data: Entity[];
}

export async function getEntities(
  page = 1,
  perPage = 10
): Promise<EntitiesList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities`;

  const { data } = await axios.get<EntitiesList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getEntityById(entityId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}`;

  const { data } = await axios.get<Entity>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createEntity(entity: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities`;

  const { data } = await axios.post<Entity>(url, entity, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function updateEntity(entity: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entity.id}`;

  const { data } = await axios.put<Entity>(url, entity, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function patchEntity(entityId: string, entityData: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}`;

  const { data } = await axios.patch<Entity>(url, entityData, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getCurrentSeriesName() {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/current-series-name`;

  const { data } = await axios.get<string>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function setNextSeriesName() {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/next-series-name`;

  const { data } = await axios.post<string>(
    url,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function generateDocument(entityId: string, documentType: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}/documents/${documentType}`;

  const { data } = await axios.get<any>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function requestEntityEIN(entity: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entity.id}/series-ein`;

  const { data } = await axios.post<Entity>(url, entity, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function createEntityBankAccount(entity: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entity.id}/bank-account`;

  const { data } = await axios.post<Entity>(url, entity, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getEntityBankAccountTransactions(entity: any) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entity.id}/bank-account/transactions`;

  const { data } = await axios.get<any>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getEntityBankAccountBalance(entityId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}/bank-account/balance`;

  const { data } = await axios.get<any>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function postWireTransfer(
  entityId: string,
  postObject: {
    amount: number;
    beneficiaryId: string;
    instructions: string;
    scheduledForDate?: string;
  }
): Promise<void | boolean> {
  try {
    const token = parseCookie.getToken("__session");
    const url = `${baseUrl}/entities/${entityId}/initTransfer`;

    const { data } = await axios.post<void | boolean>(url, postObject, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return data;
  } catch (err: any) {
    throw err.response ?? err;
  }
}

export async function postACHTransfer(
  entityId: string,
  postObject: {
    amount: number;
    beneficiaryId: string;
    instructions: string;
    scheduledForDate?: string;
  }
): Promise<void | boolean> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}/initACHTransfer`;

  const { data } = await axios.post<void | boolean>(url, postObject, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getEntityBankAccountDetails(entityId: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/entities/${entityId}/bank-account/details`;

  const { data } = await axios.get<{
    accountNumber: string;
    routingNumber: string;
    currentBalance: number;
    availableBalance: number;
  }>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
