import axios from "axios";
import * as parseCookie from "./parseCookie";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export interface BankingBeneficiary {
  id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export interface BankingBeneficiaryList {
  data: BankingBeneficiary[];
  page: number;
  perPage: number;
  totalCount: number;
}

export interface BankingAddress {
  street_1: string;
  street_2: string;
  city: string;
  postal_code: string;
  region: string;
  country: string;
}

export interface BankingBeneficiaryRequest {
  // Our system
  name: string; // display name provide by user
  email: string;
  phone?: string;

  // Required for set up
  bankName: string;
  nameOnAccount: string;
  routingNumber: number;
  accountNumber: number;
  addressOnAccount: BankingAddress;
  bankAddress: BankingAddress;
}

export interface BankingACHBeneficiaryRequest {
  // Our system
  email: string;
  phone?: string;

  // Required for set up
  nameOnAccount: string;
  routingNumber: number;
  accountNumber: number;
}

export async function getBankingBeneficiaries(): Promise<BankingBeneficiaryList> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/beneficiaries`;

  const { data } = await axios.get<BankingBeneficiaryList>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function getBankingBeneficiaryById(
  id: string
): Promise<BankingBeneficiary> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/beneficiaries/${id}`;

  const { data } = await axios.get<BankingBeneficiary>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}

export async function postBankingBeneficiary(
  bankingBeneficiary: BankingBeneficiaryRequest
): Promise<BankingBeneficiary> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/beneficiaries`;

  const { data } = await axios.post<BankingBeneficiary>(
    url,
    bankingBeneficiary,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function postACHBankingBeneficiary(
  bankingBeneficiary: BankingACHBeneficiaryRequest
): Promise<BankingBeneficiary> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/ACHBeneficiaries`;

  const { data } = await axios.post<BankingBeneficiary>(
    url,
    bankingBeneficiary,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function updateBankingBeneficiary(
  id: string,
  bankingBeneficiary: BankingBeneficiaryRequest
): Promise<BankingBeneficiary> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/beneficiaries/${id}`;

  const { data } = await axios.put<BankingBeneficiary>(
    url,
    bankingBeneficiary,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function updateACHBankingBeneficiary(
  id: string,
  bankingBeneficiary: BankingBeneficiaryRequest
): Promise<BankingBeneficiary> {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/ach-beneficiaries/${id}`;

  const { data } = await axios.put<BankingBeneficiary>(
    url,
    bankingBeneficiary,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
}

export async function deleteBankingBeneficiaryById(id: string) {
  const token = parseCookie.getToken("__session");
  const url = `${baseUrl}/banking/beneficiaries/${id}`;

  const { data } = await axios.delete<BankingBeneficiary>(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  return data;
}
