export interface Individual {
  id: string;
  name?: string;
  type?: string;
  title?: string;
  isUSBased?: boolean;
  address?: {
    address1?: string;
    address2?: string;
    city: string;
    state: string;
    postalCode: string;
    country: string;
  };
  phone?: string;
  email?: string;
  taxDetails?: {
    type?: string;
    value?: string;
  };
  dateOfBirth?: string;
  stateOfFormation?: string;
  countryOfFormation?: string;
  taxIdType?: string;
  taxId?: string;
  passportId?: string;
  ownerId?: string;
}
