import {
  feesStatementForDeal,
  getDealPerformanceSummary,
  DealPerformance,
} from "../api/dealsApi";
import { getEntityBankAccountBalance } from "../api/entitiesApi";
import useSWR from "swr";

export function useFeesStatementSWR(dealId) {
  // It's bad practice in React to conditionally call hooks,
  // so we still call it but return "dummy data",
  let handler: (dealId) => Promise<any> = feesStatementForDeal;
  if (!dealId) {
    handler = (dealId) => {
      return Promise.resolve({});
    };
  }

  return useSWR(`/deals/${dealId}/fees-statement`, () => handler(dealId));
}

export function useDealPerformanceSummarySWR(dealId) {
  // It's bad practice in React to conditionally call hooks,
  // so we still call it but return "dummy data",
  let handler: (dealId) => Promise<any> = getDealPerformanceSummary;
  if (!dealId) {
    handler = (dealId) => {
      return Promise.resolve({});
    };
  }

  return useSWR<DealPerformance>(`/deals/${dealId}/finance-summary`, () =>
    handler(dealId)
  );
}

export function useBankBalanceSWR(entityId) {
  // It's bad practice in React to conditionally call hooks,
  // so we still call it but return "dummy data",
  let handler: (entityId) => Promise<any> = getEntityBankAccountBalance;
  if (!entityId) {
    handler = (entityId) => {
      return Promise.resolve({});
    };
  }

  return useSWR<any>(`/entities/${entityId}/bank-account/balance`, () =>
    handler(entityId)
  );
}
