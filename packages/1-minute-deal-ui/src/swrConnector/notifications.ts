import {
  NotificationList,
  Notification,
  updateNotification,
} from "../api/notificationsApi";
import useSWR, { mutate } from "swr";
import { fetcher } from "./index";
const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const useNotifications = (): NotificationList => {
  const defaultData: NotificationList = {
    totalCount: 0,
    perPage: 0,
    page: 0,
    data: [],
  };

  const { data, error } = useSWR<NotificationList>(
    `${baseUrl}/notifications`,
    fetcher,
    {
      initialData: defaultData,
      suspense: true,
      revalidateOnMount: true,
      refreshInterval: 60000,
      dedupingInterval: 10000,
    }
  );

  return data;
};

export const updateNotifications = async (notification: Notification) => {
  await mutate(
    `${baseUrl}/notifications`,
    async (cachedNotifications: NotificationList) => {
      cachedNotifications.data = cachedNotifications.data.map(
        (cachedNotification) => {
          if (notification.id === cachedNotification.id) {
            updateNotification(notification);
            return notification;
          } else {
            return cachedNotification;
          }
        }
      );
      return cachedNotifications;
    }
  );
};
