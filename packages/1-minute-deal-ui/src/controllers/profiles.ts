// TODO: Where should this logic exist?  Paas... the api
function includesQPOption(purchaserStatus: number[]): boolean {
  // Make sure option 0-7 has been selected
  // option 8 is "none apply"
  // option 9-10 "extra???"
  // The first 8 options an investor can choose
  const validQPOptions = [0, 1, 2, 3, 4, 5, 6, 7];
  for (let i = 0; i < purchaserStatus.length; i++) {
    if (validQPOptions.includes(purchaserStatus[i])) {
      return true;
    }
  }

  return false;
}

function isQPSubscriberProfile(profile): boolean {
  const purchaserStatus = profile?.purchaserStatus;

  // Check if the investor is QP
  if (!purchaserStatus) {
    return false;
  }

  if (Array.isArray(purchaserStatus)) {
    if (includesQPOption(purchaserStatus)) {
      return true;
    }
  }

  return false;
}

export { includesQPOption, isQPSubscriberProfile };
