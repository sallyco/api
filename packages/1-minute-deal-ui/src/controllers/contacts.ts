import {
  ContactListItem,
  ContactList,
  putContactList,
  createContactList,
} from "../api/contactListApi";
import Papa from "papaparse";

const ERRORS = {
  INCOMPAT: "Contacts File Incompatible",
  NO_ITEMS: "No items to import",
  MISSING_FIELDS: "CSV is missing required fields (name, email, phone)",
};

function readContactsFile(file: Blob, successCallback, errorCallback) {
  const fileReader = new FileReader();
  fileReader.onload = async function (event) {
    try {
      if (typeof event?.target?.result !== "string") {
        throw new Error("Error reading contact data");
      }
      const papaObject = Papa.parse(event.target.result, { header: true });
      const fileContactsList = getContactsArray(papaObject?.data ?? []);
      successCallback(fileContactsList);
    } catch (err) {
      errorCallback(err);
    }
  };

  fileReader.onerror = function (event) {
    errorCallback(event);
  };

  fileReader.readAsText(file);
}

/**
 *
 * @param contactsJson
 * @returns ContactListItem[]
 * @throws Error on incompatibility
 */
function getContactsArray(contactsJson: ContactListItem[]): ContactListItem[] {
  if (!Array.isArray(contactsJson)) {
    throw new Error(ERRORS.INCOMPAT);
  }
  if (contactsJson.length <= 0) {
    throw new Error(ERRORS.NO_ITEMS);
  }

  // Make sure expected headers are present
  const headers = Object.keys(contactsJson[0]);
  if (
    !headers.includes("name") ||
    !headers.includes("email") ||
    !headers.includes("phone")
  ) {
    throw new Error(ERRORS.MISSING_FIELDS);
  }

  const mapped = contactsJson.map(({ name, email, phone }) => {
    return {
      name,
      email,
      phone,
    };
  });
  return mapped;
}

/**
 *
 * @param contactList
 * @param contacts
 * @returns the uploaded contactList
 */
async function replaceContactsOnServer(
  contactList: ContactList,
  contacts: ContactListItem[],
  ownerId: string
) {
  // If this is a new contact List, UPLOAD a new one
  if (contactList?._id) {
    // Other wise, replace it
    contactList.contacts = contacts;
    return putContactList(contactList);
  } else {
    return createContactListFor(ownerId, contacts);
  }
}

/**
 *
 * @description when createing a contactList we only need the ownerId and contacts
 * @param ownerId
 * @param contacts
 * @returns
 */
async function createContactListFor(ownerId, contacts: ContactListItem[]) {
  const contactList = {
    ownerId,
    contacts,
  };
  return createContactList(contactList);
}

export { readContactsFile, replaceContactsOnServer, ERRORS };
