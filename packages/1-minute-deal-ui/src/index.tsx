import React from "react";
import { render } from "react-dom";
import { HashRouter as Router } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { PersistGate } from "redux-persist/integration/react";
import store, { persistor, history } from "./store";
import * as Sentry from "@sentry/react";
import {
  ThemeProvider,
  Theme,
  StyledEngineProvider,
} from "@mui/material/styles";

import { setApplicationUpdateStatus } from "./slices/applicationUpdateSlice";

import App from "./App";
import ThemeSelector from "./themes/ThemeSelector";
import ErrorPageFallback from "./components/ErrorPageFallback";

var font = require("./assets/webfonts/icons.woff2");
var font = require("./assets/webfonts/outline-icons.woff2");

import "./themes/global.css";
import ParallelMarkets from "./components/accreditation/ParallelMarkets";

if (process.env.NODE_ENV !== "production") {
  localStorage.setItem("debug", "omd:*");
}

// Main render
render(
  <ReduxProvider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <Router>
          <Sentry.ErrorBoundary fallback={ErrorPageFallback}>
            <ThemeSelector>
              <ParallelMarkets>
                <App />
              </ParallelMarkets>
            </ThemeSelector>
          </Sentry.ErrorBoundary>
        </Router>
      </ConnectedRouter>
    </PersistGate>
  </ReduxProvider>,
  document.getElementById("app")
);

if (process.env.NODE_ENV === "development") {
  // (module as any).hot?.accept("./App", render);
} else {
  // Service Worker setup and installation
  if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
      let refreshing;
      navigator.serviceWorker.addEventListener("controllerchange", function () {
        if (refreshing) return;
        refreshing = true;
        window.location.reload();
      });
      //      navigator.serviceWorker
      //        .register('/sw.js')
      //        .then(registration => {
      //
      //          console.log('SW registered: ', registration);
      ////          registration.onupdatefound = () => {
      ////            console.log('onupdatefoune: ', registration);
      //            registration.installing?.addEventListener('statechange', stateEvent => {
      //              let target:ServiceWorker = stateEvent.target;
      //              console.log('SW stateEvent: ', stateEvent);
      //              if(stateEvent.target:ServiceWorker.state === 'installed' && registration.active){
      //              //  window.location.reload();
      //              //  store.dispatch(setApplicationUpdateStatus(true));
      //              }
      //            });
      ////          }
      //          setInterval(()=>{
      //            registration.update();
      //          }, 30000);
      //
      //        });
    });
  }
}
