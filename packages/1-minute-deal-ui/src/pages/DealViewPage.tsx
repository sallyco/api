/* eslint-disable react/display-name */
import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { Helmet } from "react-helmet";
import { useHistory, useParams } from "react-router-dom";

import { RootState } from "../rootReducer";

import { Divider, Tab, Loader } from "semantic-ui-react";

import { fetchDealById, setCurrentDealContext } from "../slices/dealsSlice";
import { fetchEntityById } from "../slices/entitySlice";

import { InvestorTab } from "../components/deals/InvestorTab";
import { DealTab } from "../components/deals/DealTab";
import { DocsTab } from "../components/deals/DocsTab";
import { DealTransactionsTab } from "../components/deals/DealTransactionsTab";

import ShortHeader from "../components/ShortHeader";

import Log from "../tools/Log";
import { TenantContext } from "../contexts";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import { StrictTabProps } from "semantic-ui-react/dist/commonjs/modules/Tab/Tab";
import { FilingsTab } from "../components/deals/FilingsTab";

export const DealViewPage = () => {
  const dispatch = useDispatch();
  let history = useHistory();

  const { dealId, tabIndex } = useParams<{
    dealId: string;
    tabIndex: string;
  }>();
  const bankingFeatureEnabled = useFeatureFlag("banking");

  const tenantProfile = useContext(TenantContext);

  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const { isLoading: entitiesLoading } = useSelector(
    (state: RootState) => state.entities
  );

  // DEAL
  const deal = useSelector(
    (state: RootState) => state.deals.dealsById[dealId],
    shallowEqual
  );
  useEffect(() => {
    async function fetchDeal() {
      if (!deal && !dealsLoading) {
        //const res = await dispatch(fetchDealById(dealId));
        //if (!res) history.replace("/");
      }
    }

    fetchDeal();
    dispatch(setCurrentDealContext(dealId));
  }, [dispatch, dealId]);

  //ENTITY
  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  }, shallowEqual);

  useEffect(() => {
    if (deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  let panes = [
    {
      menuItem: "OVERVIEW",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <DealTab dealId={dealId} />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "INVESTORS",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <InvestorTab dealId={dealId} />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "DOCS",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <DocsTab dealId={dealId} />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "FILINGS",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <FilingsTab entityId={entity?.id} />
        </Tab.Pane>
      ),
    },
  ];
  const transactionsTab = {
    active: false,
    menuItem: "TRANSACTIONS",
    render: () => (
      <Tab.Pane inverted attached={false}>
        <DealTransactionsTab dealId={dealId} entityId={entity?.id} />
      </Tab.Pane>
    ),
  };
  if (bankingFeatureEnabled) {
    panes.push(transactionsTab);
  }

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "grey-background" }}
        title={`Create - Profiles  | ${tenantProfile.name}`}
      ></Helmet>
      <ShortHeader
        title={deal?.name}
        loading={dealsLoading}
        backArrowLink={"/dashboard/deals"}
      />
      {dealsLoading || entitiesLoading ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <Tab
          menu={{ secondary: true, pointing: true, className: "nav-bar" }}
          activeIndex={tabIndex}
          onTabChange={(e, data) => {
            history.push(`/deals/${dealId}/${data.activeIndex}`);
          }}
          panes={panes}
        />
      )}
    </>
  );
};
