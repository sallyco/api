import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { Container, Header, Segment, Divider, Icon } from "semantic-ui-react";
import { Link, useLocation, useHistory } from "react-router-dom";
import * as parseCookie from "../api/parseCookie";
import { getSubscriptions } from "../api/subscriptionsApi";
import { useDispatch } from "react-redux";
import { fetchSelf } from "../slices/usersSlice";
import { insertSubscription } from "../slices/subscriptionsSlice";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import { toast } from "react-toastify";

export const DirectDealLinkPage = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const query = new URLSearchParams(useLocation().search);
  const dealId = String(query.get("deal"));
  const session = parseCookie.getToken("__session");

  const ddNoChoice = useFeatureFlag("direct_deal_no_choice");

  async function createDealSubscription(subscriptionDealId) {
    const user: any = await dispatch(fetchSelf());
    return dispatch(
      insertSubscription({
        email: user.username,
        name: `${user.firstName} ${user.lastName}`,
        ownerId: user.id,
        dealId: subscriptionDealId,
        status: "INVITED",
        acquisitionMethod: "DIRECT_LINK",
      })
    );
  }

  const setCookieRedirect = () => {
    document.cookie = `__postlogin-url=${encodeURIComponent(
      props.location.pathname + props.location.search
    )};`;
  };

  useEffect(() => {
    if (session) {
      // Query the deal, if it was provided
      getSubscriptions().then((subs) => {
        const subscription = subs.data.find((sub) => sub.dealId === dealId);

        if (subscription === undefined) {
          createDealSubscription(dealId)
            .then((appThunk) => {
              // This appThunk is actually a Subscription...
              history.push(`/subscriptions/${appThunk["id"] ?? ""}`);
            })
            .catch(() => {
              toast.error("Something went wrong, please try the link again");
            });
        } else {
          history.push(`/subscriptions/${subscription.id}`);
        }
      });
    } else if (ddNoChoice) {
      setCookieRedirect();
      history.push("/");
    }
  }, [ddNoChoice]);

  return (
    <>
      <Helmet title={`Deal Investment`} />
      <Container className="mobile-container" textAlign="center">
        <Segment inverted={true}>
          {session && dealId ? (
            <>
              <Header content="Please wait while we load the deal..." />
              <Icon loading name="spinner" />
            </>
          ) : (
            <>
              <Header
                content={`To start investing in deals you must sign in or register`}
                as="h3"
              />
              <Divider hidden />
              <Divider hidden />
              <Link
                onClick={() => {
                  setCookieRedirect();
                }}
                to={{ pathname: "/register/investor" }}
              >
                Create an account
              </Link>
              <Divider hidden />
              <Link
                onClick={() => {
                  setCookieRedirect();
                }}
                to={{ pathname: "/" }}
              >
                Already have an account?
              </Link>
            </>
          )}
        </Segment>
      </Container>
    </>
  );
};
