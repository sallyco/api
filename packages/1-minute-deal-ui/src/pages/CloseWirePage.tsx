import React from "react";
import { Link, useParams } from "react-router-dom";
import { Button, Container, Divider, Header } from "semantic-ui-react";
// import useCloseDealMeta from "../hooks/closeDealMeta";
// import BankingBeneficiaryTab from "../pages/BankingBeneficiaryTab";
// import BankingWireTransfersTab from "../pages/BankingWireTransfersTab";

export default function CloseWirePage() {
  const { dealId } = useParams<{ dealId: string; closeId: string }>();
  const inverted = true;
  // The other option, don't redirect to existing page, build a new one
  //   const closeDealMeta = useCloseDealMeta(dealId, closeId);
  //   return (
  //     <Container>
  //       <Header
  //         inverted={inverted}
  //         textAlign="center"
  //         content="Sending funds to the portfolio company"
  //         as="h2"
  //       />
  //       <BankingWireTransfersTab
  //         inverted={inverted}
  //         earliestDate={closeDealMeta.earliestDate}
  //         maxAmount={closeDealMeta.maxAmount}
  //         initialValues={{ amount: closeDealMeta.maxAmount }}
  //       />
  //     </Container>
  //   );

  return (
    <Container textAlign="center" className="mobile-container text">
      <Divider hidden />
      <Header inverted={inverted} content="Closing Outbound Wire" as="h2" />
      <Divider hidden />
      <Header
        inverted={inverted}
        content="Would you like to select a date for the closing outbound wire now?"
        as="h3"
      />
      <Divider hidden />
      <Button
        secondary
        fluid
        content="Yes, Go to Banking Page"
        as={Link}
        to="/dashboard/organizer-banking"
      />
      <Divider inverted={inverted} horizontal content={"Or"} />
      <Button
        primary
        inverted={inverted}
        fluid
        content="No, go to deal summary"
        as={Link}
        to={`/deals/${dealId}`}
      />
    </Container>
  );
}
