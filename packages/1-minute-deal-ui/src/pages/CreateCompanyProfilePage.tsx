import React, { useContext, useEffect, useState } from "react";
import { Divider, Container, Transition } from "semantic-ui-react";
import { Helmet } from "react-helmet";
import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import ShortHeader from "../components/ShortHeader";
import { TenantContext } from "../contexts";
import { useHistory, useLocation, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { insertAsset } from "../slices/assetsSlice";
import { toast } from "react-toastify";
import { RootState } from "../rootReducer";
import FounderExitCompanyModel from "./company/FounderExitCompanyModel";
import ShortForm from "../forms/companyProfile/ShortForm";
import { fetchDealById, updateDealById } from "../slices/dealsSlice";
import { insertInvite } from "../slices/invitesSlice";
import { sendInviteFounder } from "../api/emailsApi";
import { Deal } from "../api/dealsApi";
import { Asset } from "../api/assetsApi";
import { Invite } from "../api/invitesApi";
import { API, useRequest } from "../api/swrApi";
import { getUserEmailExist } from "../api/usersApi";
import { mutate } from "swr";

export const CompanyProfilePage = () => {
  const tenantProfile = useContext(TenantContext);
  const [founderExitOpen, setFounderExitOpen] = useState(false);
  const query = new URLSearchParams(useLocation().search);

  let dealId = query.get("dealId") ?? undefined;
  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const dispatch = useDispatch();
  const { data: self } = useRequest(API.SELF);

  useEffect(() => {
    if (dealId && (!deal || deal.id !== dealId)) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  let history = useHistory();

  function navigateToCompany(companyID) {
    history.push(`/assets/companies/${companyID}`);
  }

  const { isSubmitting } = useSelector((state: RootState) => state.assets);

  const { data: companies, isValidating } = useRequest(
    API.ADD_FILTER(API.ASSETS, {
      fields: ["id"],
    })
  );

  async function onSubmit(data) {
    Log.debug("data", data);
    switch (data.profileform) {
      case 1: {
        const shortFormData = {
          name: data.name ? data.name : "",
          assetUrl: data.assetUrl ?? "",
          properties: [
            { key: "Location", value: data.properties.location ?? "" },
            // Below are included for defaults for Summary page to load properly
            { key: "Market Sector", value: "" },
            { key: "No. Of Employees", value: "" },
            { key: "Incorporation", value: "" },
            { key: "Revenue Stage", value: "" },
            { key: "Date Founded", value: "" },
          ],
          contact: {
            name: data.contactName,
            email: data.companyemail,
            phone: data.companyphone,
          },
          founderIds: [data.founderemail],
          funding: {
            round: "",
            targetRaiseAmount: data.targetRaiseAmount
              ? data.targetRaiseAmount.toString()
              : "",
            securityType: data.securityType ?? "",
            preValuation: "",
            noOfShare: data.noOfShare ? data.noOfShare.toString() : "",
            sharePrice: data.sharePrice ? data.sharePrice.toString() : "",
          },
          dealId: deal?.id ?? "",
          type: "COMPANY",
          isPublic: false,
          // advisors: data.advisors ?? "",
          team: [],
          details: [
            {
              heading: "Management Overview",
              body: "",
            },
          ],
          description: "",
        };

        try {
          // TODO figure out how to use proper type (Profile) not any
          // const company: any = await dispatch(insertAsset(profileData));
          const company: any = await dispatch(insertAsset(shortFormData));
          //update the list
          await mutate(API.ADD_FILTER(API.ASSETS, { fields: ["id"] }));
          toast.success(`Company "${company.name}" Created`);
          Log.debug("profile", company);

          if (deal) {
            await dispatch(
              updateDealById({
                ...deal,
                assetIds: (deal.assetIds ?? []).concat(company.id),
              })
            );
          }

          // Invite Founder to Asset
          let inviteObject: any = await dispatch(
            insertInvite({
              email: data.founderemail.toLowerCase(),
              id: company.id,
              type: "company",
              relation: "founder",
              role: "founder",
            })
          );

          await sendInviteFounder(
            data.founderemail.toLowerCase(),
            self.email,
            deal,
            company,
            inviteObject,
            !!(
              await getUserEmailExist(data.founderemail.toLowerCase())
            )?.data?.email
          );

          navigateToCompany(company.id);
        } catch (e) {
          if (e.response) {
            ToastError(e.response);
          }
        }
        break;
      }
      default:
        return;
    }
  }

  async function HandleDashboard(stats: boolean) {
    setFounderExitOpen(false);
  }

  return (
    <>
      <Helmet title={`Company Profile | ${tenantProfile.name}`} />
      <ShortHeader title={`Company Profile`} />
      {!isValidating && (
        <>
          {companies?.data.length === 0 ? (
            <Container text className="margin-bottom-container">
              <ShortForm onSubmit={onSubmit} submitting={isSubmitting} />
              {founderExitOpen && (
                <FounderExitCompanyModel
                  onSubmit={HandleDashboard}
                  isShowModal={founderExitOpen}
                />
              )}
            </Container>
          ) : (
            <Redirect to={{ pathname: "/dashboard/assets/companies" }} />
          )}
        </>
      )}
    </>
  );
};
