import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";

import { toast } from "react-toastify";
import { RootState } from "../rootReducer";
import { TenantContext } from "../contexts";

import { Redirect, useHistory, useLocation } from "react-router-dom";

import { insertAsset } from "../slices/assetsSlice";
import { fetchDealById, updateDealById } from "../slices/dealsSlice";

import { Container, Divider, Transition } from "semantic-ui-react";
import CreateArtworkAssetForm from "../forms/CreateArtworkAssetForm";
import ShortHeader from "../components/ShortHeader";

import { ToastError } from "../tools/ToastMessage";
import Log from "../tools/Log";

export const CreateArtworkAssetPage = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const history = useHistory();
  const query = new URLSearchParams(useLocation().search);
  let dealId = query.get("dealId") ?? undefined;

  const [submitting, setSubmitting] = useState(false);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  useEffect(() => {
    if (dealId && (!deal || deal.id !== dealId)) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  async function onSubmit(data) {
    Log.debug("data", data);
    setSubmitting(true);

    try {
      const assetResp: any = await dispatch(
        insertAsset({
          ...data,
          appraisedValue: Number(data.appraisedValue.replace(/\D+/g, "")),
          fractionalOwnershipAmount: Number(
            data.fractionalOwnershipAmount.replace(/\D+/g, "")
          ),
          ...(deal && { dealId: deal.id }),
        })
      );
      Log.debug("assetResp", assetResp);

      if (deal) {
        await dispatch(
          updateDealById({
            ...deal,
            assetId: assetResp.id,
          })
        );
      }

      setSubmitting(false);
      toast.success(`Artwork "${assetResp.name}" Added`);

      history.push(`/assets/${assetResp.id}`);
    } catch (e) {
      Log.error("error", e);
      setSubmitting(false);
      if (e.response) {
        ToastError(e.response);
      }
    }
  }

  return (
    <>
      <Helmet title={`Add Artwork | ${tenantProfile.name}`}></Helmet>
      <ShortHeader title={`Add Artwork Asset`} />
      <Divider hidden />
      <Transition transitionOnMount>
        <Container text className="margin-bottom-container">
          <CreateArtworkAssetForm onSubmit={onSubmit} submitting={submitting} />
        </Container>
      </Transition>
    </>
  );
};
