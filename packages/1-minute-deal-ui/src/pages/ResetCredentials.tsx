import React, { useState } from "react";
import { resetPassword } from "../api/usersApi";
import ResetCredentialsForm from "../forms/ResetCredentialsForm";

import {
  Icon,
  Button,
  Container,
  Divider,
  Transition,
} from "semantic-ui-react";
import { Link, Redirect, useHistory, useLocation } from "react-router-dom";

import Log from "../tools/Log";
import { toast } from "react-toastify";

export const ResetCredentials = () => {
  const [submitting, setSubmitting] = useState(false);
  const [success, setSuccess] = useState(false);
  const history = useHistory();

  let query = new URLSearchParams(useLocation().search);
  const token = query.get("token");

  const handleSubmit = async (data) => {
    data.token = token;
    Log.debug("data", data);
    setSubmitting(true);
    try {
      await resetPassword(data);
      toast.success("Your password has been updated");
      history.push("/");
    } catch (e) {
      toast.error("An Error has occurred when resetting your password");
    }
  };

  Log.debug("token", token);

  if (token === null) {
    return <Redirect to="/" />;
  }

  return (
    <>
      <Transition>
        <Container className="mobile-container">
          <ResetCredentialsForm
            submitting={submitting}
            onSubmit={handleSubmit}
          />
        </Container>
      </Transition>
    </>
  );
};
