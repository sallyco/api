import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { RootState } from "../rootReducer";
import { Loader, Divider } from "semantic-ui-react";
import { GlxList } from "../components/glx/GlxList";
import { fetchAssetsList } from "../slices/assetsSlice";
import { TenantContext } from "../contexts";

interface GlxProps {
  page: number;
  perPage: number;
  totalCount: number;
}

// TODO get pagination working
export const GlxListPage = ({
  page = 1,
  perPage = 10,
  totalCount = 0,
}: GlxProps) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const { currentPageAssets, isLoading, error, assetsById } = useSelector(
    (state: RootState) => state.assets
  );

  const companies = currentPageAssets.map((companyId) => assetsById[companyId]);

  useEffect(() => {
    dispatch(fetchAssetsList());
  }, [tenantProfile]);

  return (
    <>
      <Helmet title={`GLX - Dashboard | ${tenantProfile.name}`}></Helmet>
      {isLoading || !companies ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <GlxList companies={companies} />
      )}
    </>
  );
};
