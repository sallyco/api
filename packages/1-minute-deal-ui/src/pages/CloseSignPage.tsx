import React, { useContext, useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useParams, useLocation, useHistory } from "react-router-dom";
import { RootState } from "../rootReducer";
import { TenantContext } from "../contexts";
import {
  fetchCloseById,
  updateCloseById,
  generateCloseDocument,
} from "../slices/closesSlice";
import { fetchEntityById } from "../slices/entitySlice";
import { fetchProfileById } from "../slices/profilesSlice";
import { fetchDealById, updateDealById } from "../slices/dealsSlice";
import PdfDocument from "../components/PdfDocument";

import {
  Header,
  Button,
  Divider,
  Segment,
  Container,
  Loader,
} from "semantic-ui-react";

import SignatureCanvas from "react-signature-canvas";
import ShortHeader from "../components/ShortHeader";
import "react-pdf/dist/esm/Page/AnnotationLayer.css";
import moment from "moment";

import Log from "../tools/Log";

export const CloseSignPage = () => {
  const dispatch = useDispatch();
  const { dealId, closeId } = useParams<{ dealId: string; closeId: string }>();
  const [isAllSubmitting, setIsAllSubmitting] = useState(false);
  const [sigStage, setSigStage] = useState("");

  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const query = new URLSearchParams(useLocation().search);
  //TODO there has to b a better way to handle this in TS
  let isGenerate = query.get("generate") ?? false;
  Log.debug("isGenerate", isGenerate);

  const [canAccept, setCanAccept] = useState(false);

  const [generated, setGenerated] = useState(false);
  const [documents, setDocuments] = useState([]);

  const sigCanvas = useRef(SignatureCanvas) as React.MutableRefObject<any>;

  const sigClear = () => {
    setCanAccept(false);
    sigCanvas.current.clear();
  };

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const {
    isLoading: closesLoading,
    isSubmitting,
    error: closesError,
    currentClosingDocuments,
  } = useSelector((state: RootState) => state.closes);

  const close = useSelector(
    (state: RootState) => state.closes.closesById[closeId]
  );

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId]);

  useEffect(() => {
    if (!close || close.id !== closeId) {
      dispatch(fetchCloseById(closeId));
    }
  }, [close, closeId]);

  // ENTITY
  //
  const { isLoading: entitiesLoading, error: entitiesListError } = useSelector(
    (state: RootState) => state.entities
  );

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });
  useEffect(() => {
    if (deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, entity]);

  // PROFILE

  const profile = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });

  useEffect(() => {
    if (deal && !profile) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [deal, profile]);

  // DOCUMENTS
  async function operatingAgreement(dId, dl, prof, ent, signature = null) {
    const payload: any = {
      agreementText: "Limited Liability Company Agreement",
      entityLegalName: ent.name,
      fundSignatureBlock1: ent.name,
      fundSignatureBlock2: "a Delaware Limited Liability Company",
      fundSignatureBlock3: "Glassboard Management II, LLC, Manager", // FIXED

      assureManager: "",
      organizerManager: "",
      manager: "**memberIndividual**",

      managerSignatureBlock1: "Glassboard Management II, LLC,",
      managerSignatureBlock2: "a Utah Limited Liability Company",
      oaManagerName: "Glassboard Management II, LLC", // FIXED
      managerTitle: "Manager",

      organizerSignatureBlock1: `${prof.firstName} ${prof.lastName},`,
      organizerSignatureBlock2: `a ${prof.address.state} ${prof.profileType}`,
      organizer: "Organizer",
    };

    if (signature) {
      payload.signature = signature;
    }

    return dispatch(generateCloseDocument(dId, "oa", payload));
  }

  async function subscriptionAgreement(dId, dl, prof, ent, signature = null) {
    const payload: any = {
      documentHeader1: `${ent.name} A`,
      documentHeader2: "Delaware Limited Liability Company",
      entityLegalName: ent.name,
      managerName: "Glassboard Management II, LLC", // FIXED
      date: moment().format("MMM Do YYYY"),
      assureManager: "",
      organizerManager: "",
      manager: "**memberIndividual**",
    };

    if (signature) {
      payload.signature = signature;
    }

    return dispatch(generateCloseDocument(dId, "sub", payload));
  }

  useEffect(() => {
    if (isGenerate && !generated && close && profile && deal && entity) {
      setGenerated(true);
      operatingAgreement(dealId, deal, profile, entity);
      subscriptionAgreement(dealId, deal, profile, entity);
    }
  }, [dispatch, profile, close, closeId, deal, entity, isGenerate, generated]);

  async function handleClick() {
    setIsAllSubmitting(true);
    setGenerated(true);
    const createdSig = sigCanvas.current
      .getTrimmedCanvas()
      .toDataURL("image/png");
    Log.debug("signature", createdSig);

    if (close && deal && profile && entity) {
      setSigStage("Operating Agreement");
      const file1: any = await operatingAgreement(
        dealId,
        deal,
        profile,
        entity,
        createdSig
      );

      setSigStage("Subscription Agreement");
      const file2: any = await subscriptionAgreement(
        dealId,
        deal,
        profile,
        entity,
        createdSig
      );

      dispatch(
        updateDealById({
          ...deal,
          status: "CLOSED",
        })
      );
      await dispatch(
        updateCloseById({
          ...close,
          files: [file1, file2],
        })
      );
      setIsAllSubmitting(false);
    }
    history.push(`/closes/${dealId}/close-funding/${closeId}`);
  }

  return (
    <>
      <Helmet title={`Funding - Closes | ${tenantProfile.name}`}/>
      <ShortHeader title={deal?.name} loading={dealsLoading} hideBackArrow />
      {dealsLoading ||
      closesLoading ||
      isAllSubmitting ||
      currentClosingDocuments.length === 0 ? (
        <>
          <Divider hidden />
          <Loader active inverted inline="centered" size="massive" />
          <Segment basic inverted textAlign="center">
            {isAllSubmitting
              ? `Applying Signature to ${sigStage}`
              : "Generating Signing Documents"}
          </Segment>
        </>
      ) : (
        <>
          <Divider hidden />
          <Container className="wide-mobile-container" textAlign="center">
            <Header as="h4" textAlign="center" inverted>
              Draw your signature in the space below, it will be applied to all
              documents.
            </Header>
            <SignatureCanvas
              ref={sigCanvas}
              penColor="black"
              backgroundColor="white"
              canvasProps={{ width: 380, height: 200, className: "sigCanvas" }}
              onEnd={() => setCanAccept(true)}
            />
            <Button
              color="orange"
              fluid
              onClick={sigClear}
              attached="bottom"
              size="small"
            >
              Clear
            </Button>
            <Divider hidden />
            <Button
              secondary
              fluid
              disabled={!canAccept}
              onClick={handleClick}
              type="button"
              loading={isAllSubmitting}
              content="Accept Signature and Apply to All Documents"
            />
            <Divider />
            {currentClosingDocuments.map((doc) => (
              <>
                <PdfDocument key={doc} fileId={doc} />
                <Divider hidden />
              </>
            ))}
          </Container>
        </>
      )}
    </>
  );
};
