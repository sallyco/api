import React, { useCallback } from "react";
import Header from "../components/Header";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Chip,
  Container,
  Divider,
  Paper,
  Typography,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { Alert, AlertTitle } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Profile } from "../api/profilesApi";
import { API, useRequest } from "../api/swrApi";
import { ProfileEditCard } from "../components/profiles/ProfileEditCard";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import { Link, useLocation } from "react-router-dom";

const useStyles = makeStyles((theme) => {
  return {
    "#main-content": {
      width: 10,
    },
    root: {
      "& img": {
        maxWidth: "100%",
      },
    },
    container: {
      backgroundColor: "#f4f6fa",
      flexGrow: 1,
      marginTop: 50,
      paddingTop: 50,
      paddingBottom: 50,
      display: "flex",
      flexDirection: "column",
    },
    containerPaper: {
      marginTop: "-100px",
      flexGrow: 1,
      display: "flex",
      flexDirection: "column",

      // Keep labels from disappearing on error
      "& .ui.form .field.error label": {
        color: "black!important",
      },
    },
  };
});

export const RequiredAccountActions = () => {
  const styles = useStyles();
  const query = new URLSearchParams(useLocation().search);

  const { data: self } = useRequest(API.SELF);
  const { data: profiles } = useRequest(
    self &&
      API.ADD_FILTER(API.PROFILES, {
        // Scoped because admin returns all profiles for tenant currently
        where: {
          ownerId: self.id,
        },
      })
  );

  const profilesToUpdate = useCallback(() => {
    return (profiles?.data ?? []).filter((profile) =>
      hasRequiredActions(profile)
    );
  }, [profiles]);

  return (
    <>
      <Header showSideBar={() => {}} showMenu={false} />
      <Container className={styles.container} maxWidth={"xl"}>
        <Container>
          <Paper elevation={5} className={styles.containerPaper}>
            <Box m={2} flexGrow={1}>
              <Typography variant={"h4"} color={"textSecondary"}>
                Account Update
              </Typography>
              <Box m={2}>
                <Divider />
              </Box>
              {profilesToUpdate().length > 0 && (
                <>
                  <Alert severity={"warning"}>
                    <AlertTitle>
                      <strong>Update your Profiles</strong>
                    </AlertTitle>
                    You have one or more profiles that have missing or
                    out-of-date information.{" "}
                    <strong>Please update them to proceed.</strong>
                  </Alert>
                  <Box m={2}>
                    <Divider />
                  </Box>
                </>
              )}
              {profilesToUpdate().map((profile) =>
                (function profileForm() {
                  return (
                    <Accordion>
                      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Box display={"flex"} width={"100%"}>
                          <Typography>
                            <Chip
                              label={profile.profileType}
                              variant={"outlined"}
                            />
                            &nbsp;
                            {profile?.displayName ??
                              profile?.name ??
                              `${profile.firstName} ${profile.lastName}`}
                          </Typography>
                          <Box
                            flexGrow={1}
                            display={"flex"}
                            justifyContent={"flex-end"}
                          >
                            {checkMissingInfo(profile).map((info) =>
                              (function m() {
                                return (
                                  <Chip
                                    label={`Missing ${info}`}
                                    style={{
                                      backgroundColor: "#c50808",
                                      color: "#efcccc",
                                    }}
                                    size={"small"}
                                  />
                                );
                              })()
                            )}
                          </Box>
                        </Box>
                      </AccordionSummary>
                      <AccordionDetails>
                        <ProfileEditCard
                          profile={profile}
                          profileType={profile.profileType}
                        />
                      </AccordionDetails>
                    </Accordion>
                  );
                })()
              )}
            </Box>
            {profilesToUpdate().length < 1 && (
              <Box
                display={"flex"}
                flexDirection={"column"}
                alignItems={"center"}
                m={2}
              >
                <CheckCircleOutlineIcon
                  style={{ fontSize: 100 }}
                  color={"primary"}
                />
                <Typography>
                  Thank you, {self?.firstName}. Your account is up-to-date.
                </Typography>
                <Box my={4} width={"100%"}>
                  <Divider color={"black"} />
                </Box>
                <Button
                  component={Link}
                  to={query.get("return") ?? `/dashboard`}
                  color={"primary"}
                  variant={"contained"}
                >
                  Continue
                </Button>
              </Box>
            )}
          </Paper>
        </Container>
      </Container>
    </>
  );
};

const MISSING_INFO = {
  DATE_OF_BIRTH: "Date of Birth",
};

const checkMissingInfo = (profile: Profile) => {
  const missing = [];
  if (!profile.dateOfBirth) {
    missing.push(MISSING_INFO.DATE_OF_BIRTH);
  }
  return missing;
};

export const hasRequiredActions = (profile: Profile) => {
  return checkMissingInfo(profile).length > 0;
};
