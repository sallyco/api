import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Divider, Container, Transition } from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { RootState } from "../rootReducer";
import { TenantContext } from "../contexts";

import { fetchProfilesList } from "../slices/profilesSlice";

import SiteHeader from "../components/Header";
import ProfileManagementActions from "../components/profiles/ProfileManagementActions";
import { ProfileSelector } from "../components/profiles/ProfileSelector";
import { ProfileList } from "../components/profiles/ProfileList";

export const ManageProfilePage = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const { profileManagementDisplayType } = useSelector(
    (state: RootState) => state.profiles
  );

  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  return (
    <>
      <Helmet
        title={`Profile Management | ${tenantProfile.name}`}
        bodyAttributes={{ class: "grey-background" }}
      />
      <SiteHeader showSideBar={false} />
      <Transition animation={"fade up"}>
        <Container>
          <ProfileManagementActions />
          <Divider hidden />
          <Container>
            {profileManagementDisplayType === "edit" ? (
              <ProfileSelector />
            ) : (
              <ProfileList />
            )}
          </Container>
        </Container>
      </Transition>
    </>
  );
};
