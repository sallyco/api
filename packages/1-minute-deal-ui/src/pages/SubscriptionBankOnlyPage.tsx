import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams, useLocation } from "react-router-dom";
import { Helmet } from "react-helmet";

import { RootState } from "../rootReducer";
import * as Yup from "yup";
import numeral from "numeral";

import Log from "../tools/Log";

import { fetchSubscriptionById } from "../slices/subscriptionsSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchSelf } from "../slices/usersSlice";

import {
  Header,
  Message,
  Image,
  Button,
  Divider,
  Loader,
  Container,
  Segment,
  Form,
  Transition,
  Icon,
  Popup,
  Label,
  Grid,
} from "semantic-ui-react";
import ShortHeader from "../components/ShortHeader";
import { Formik } from "formik";
import money from "../assets/images/money.png";
import { TenantContext } from "../contexts";
import { FormField } from "../forms/common/FormField";
import { fetchEntityById } from "../slices/entitySlice";

export const SubscriptionBankOnlyPage = () => {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const [overMax, setOverMax] = useState(false);
  const tenantProfile = useContext(TenantContext);
  const query = new URLSearchParams(useLocation().search);

  let history = useHistory();

  const { isLoading: subscriptionsLoading, isSubmitting } = useSelector(
    (state: RootState) => state.subscriptions
  );

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const { self } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
    if (subscription && deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, subscription, deal, entity]);

  async function onSubmit(data) {
    Log.debug("data", data);

    history.push(
      `/subscriptions/${subscriptionId}/sign-documents?generate=true&regenerate=true&wiringBankName=${data.wiringBankName}&wiringBankInUS=${data.wiringBankInUS}&wiringBankSub=${data.wiringBankSub}`
    );
  }

  const validation = Yup.object().shape({
    wiringBankName: Yup.string().trim().required(),
  });

  return (
    <>
      <Helmet title={`Bank - Subscriptions | ${tenantProfile.name}`}></Helmet>

      <ShortHeader title={deal?.name} loading={dealsLoading} />
      {dealsLoading || subscriptionsLoading ? (
        <>
          <Divider hidden />
          <Loader active inverted inline="centered" size="massive" />
        </>
      ) : (
        <>
          <Transition>
            <Container textAlign="left" className="mobile-container">
              <Divider hidden />
              <Image src={money.src} size="small" centered />
              <Header as="h3" textAlign="center" inverted icon>
                Enter bank details
              </Header>
              <Formik
                initialValues={{}}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form onSubmit={props.handleSubmit}>
                    <FormField
                      name={"wiringBankName"}
                      id={"wiringBankName"}
                      label={
                        "Name of the bank from which you intend to send the funds from"
                      }
                      placeholder={"Name of Bank"}
                      required
                    />
                    <Grid stretched relaxed>
                      <Grid.Column width={11} stretched>
                        <FormField
                          inline
                          name={"wiringBankInUS"}
                          id={"wiringBankInUS"}
                          label={
                            'Is the Bank located in the United States or another "FATF COUNTRY"?'
                          }
                          component={Form.Checkbox}
                        />
                      </Grid.Column>
                      <Grid.Column textAlign={"right"}>
                        <Popup
                          trigger={<Icon color="grey" name="question circle" />}
                          content="The current list of countries that are members of the Financial Action Task Force on Money Laundering
(each an “FATF Country”) are: Argentina, Australia, Austria, Belgium, Brazil, Canada, China, Denmark, Finland,
France, Germany, Greece, Hong Kong, Iceland, India, Ireland, Italy, Japan, Korea, Luxembourg, Malaysia, Mexico,
Kingdom of the Netherlands, New Zealand, Norway, Portugal, Russian Federation, Singapore, South Africa, Spain,
Sweden, Switzerland, Turkey, United Kingdom, and the United States. The list of FATF Countries may be
expanded to include future FATF members and FATF compliant countries, as appropriate."
                          position="right center"
                        />
                      </Grid.Column>
                    </Grid>
                    <Divider hidden />
                    <FormField
                      name={"wiringBankSub"}
                      id={"wiringBankSub"}
                      label={
                        "If checked yes above, are you a customer of the Bank?"
                      }
                      component={Form.Checkbox}
                    />
                    <Transition.Group animation={"fade"}>
                      {overMax && (
                        <Message negative compact size="small">
                          Your committed investment amount exceeds the
                          investment available on the deal so you entire amount
                          may not be able to be accepted
                        </Message>
                      )}
                    </Transition.Group>
                    <Button
                      secondary
                      type="submit"
                      disabled={!props.dirty || !props.isValid}
                      loading={isSubmitting}
                      fluid
                    >
                      Review & Sign Docs
                    </Button>
                  </Form>
                )}
              </Formik>
            </Container>
          </Transition>
        </>
      )}
    </>
  );
};
