import React, { useState, useContext, useEffect } from "react";
import { TenantContext } from "../contexts";
import { Link, Redirect, useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import {
  Divider,
  Segment,
  List,
  Message,
  Container,
  Transition,
  Dimmer,
  Loader,
} from "semantic-ui-react";

import { sendEmail, sendVerifyAccountEmail } from "../api/emailsApi";
import { toast } from "react-toastify";

import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../rootReducer";
import { fetchSelf } from "../slices/usersSlice";

interface Props {
  email?: string;
}

export const Unverified = ({ email }: Props) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const { self, error } = useSelector((state: RootState) => state.users);

  let history = useHistory();

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  async function handleClick(e) {
    e.preventDefault();
    setIsSubmitting(true);
    try {
      if (self) {
        await sendVerifyAccountEmail(self.email, self.firstName);
      } else if (email) {
        await sendVerifyAccountEmail(email, "");
      }
      toast.success("Please check your email for a verification link");
      setIsSubmitting(false);
      history.push("/logout");
    } catch (e) {
      setIsSubmitting(false);
      ToastError(e.response);
      Log.error("error", e);
    }
  }

  if (!email) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "pages-unverified" }}
        title={`Verify Your Account | ${tenantProfile.name}`}
      />
      <Transition>
        <Container className="mobile-container">
          <Divider hidden />
          <Message>
            <Message.Header>Please verify your account</Message.Header>
            {isSubmitting && (
              <Dimmer active inverted>
                <Loader />
              </Dimmer>
            )}
            <p>
              Your email address is still pending verification. Please check
              your email for a verification link.
            </p>
            <p>
              Please{" "}
              <a href="./#" onClick={handleClick} className={"primaryColor"}>
                click here
              </a>{" "}
              if you need the email resent to you.
            </p>
          </Message>
          <Segment basic inverted={true} textAlign="center">
            <List>
              <List.Item>
                <Link to="/logout">Return to log in</Link>
              </List.Item>
            </List>
          </Segment>
        </Container>
      </Transition>
    </>
  );
};
