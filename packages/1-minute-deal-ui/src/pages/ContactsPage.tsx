import ContactListTable, {
  useContactsList,
} from "../components/ContactListTable";
import React, { useContext, useEffect, useState, useRef } from "react";
import { RootState } from "../rootReducer";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { Button, Container, Divider, Header } from "semantic-ui-react";
import { TenantContext } from "../contexts";
import { fetchSelf } from "../slices/usersSlice";
import { PopupModal } from "../components/common/PopupModal";
import {
  readContactsFile,
  replaceContactsOnServer,
} from "../controllers/contacts";

const TEMPLATE_BLOB = new Blob([`"name","email","phone"`], {
  type: "text/csv;charset=utf-8;",
});
const blobUrl = URL.createObjectURL(TEMPLATE_BLOB);

const CONTACT_UPLOAD_ERROR_MESSAGE = `Something went wrong during the import.  Please make sure the file is a CSV and try to import again.`;

export const ContactsPage = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const { self } = useSelector((state: RootState) => state.users);
  const { contactList, setContactList } = useContactsList(self);
  const [showImport, setShowImport] = useState(false);
  const [processing, setProcessing] = useState(false);
  const [actionMessage, setActionMessage] = useState("");
  const fileInputRef = useRef<HTMLInputElement>();

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  return (
    <>
      <Helmet title={`Contacts | ${tenantProfile.name}`}></Helmet>
      <Container>
        <Divider hidden />
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            paddingBottom: "8px",
          }}
        >
          <Header size="medium">Contacts</Header>
          <Button
            type="button"
            onClick={() => {
              setShowImport(true);
            }}
          >
            Import Contacts
          </Button>
        </div>
        <ContactListTable
          contacts={contactList?.contacts}
          setSelectedRows={() => {}}
        />
      </Container>
      <PopupModal
        open={showImport}
        size="tiny"
        heading="Import Contacts"
        processing={processing}
        content={
          <>
            <Header size="medium">1. Download Our Template</Header>
            <p>
              In order to import contacts from another system into ours, we
              created a CSV template you can download and fill out.
            </p>
            <a href={blobUrl} download="template.csv">
              <Button fluid type="button">
                Download Blank CSV Template
              </Button>
            </a>
            <Divider hidden />
            <Header size="medium">2. Import Your Contacts</Header>
            <p>
              After placing your contacts in the template from above, you can
              upload them here. In order for the import to succeed the file must
              be in CSV format.
            </p>
            <p>
              Warning - This will replace your current contact list. If you wish
              to edit your list you may export it from our system, edit it, and
              upload it again.
            </p>
            <div className="ui input">
              <input ref={fileInputRef} type="file" id="contacts_import" />
            </div>
            {actionMessage && (
              <>
                <Divider hidden />
                <p>{actionMessage}</p>
              </>
            )}
          </>
        }
        okcaption={"Begin Import"}
        closecaption="Close"
        onClickOk={() => {
          // Handle loading the file
          setActionMessage("");
          setProcessing(true);
          const selectedFile = fileInputRef.current.files[0];
          readContactsFile(
            selectedFile,
            (fileContacts) => {
              // Now that we have the list, upload it
              replaceContactsOnServer(contactList, fileContacts, self?.id)
                .then((contactListData) => {
                  setActionMessage(
                    `Success, your new list of ${fileContacts.length} contacts was saved.`
                  );
                  setContactList(contactListData);
                })
                .catch(() => {
                  setActionMessage(CONTACT_UPLOAD_ERROR_MESSAGE);
                });
              setProcessing(false);
            },
            (error) => {
              setActionMessage(CONTACT_UPLOAD_ERROR_MESSAGE);
              setProcessing(false);
            }
          );
        }}
        onClickClose={() => setShowImport(false)}
      />
    </>
  );
};
