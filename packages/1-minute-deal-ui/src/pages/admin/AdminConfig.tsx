import React, { useContext, useEffect } from "react";
import { RootState } from "../../rootReducer";
import { Helmet } from "react-helmet";
import { TenantContext } from "../../contexts";
import { Container, Transition, Card } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { fetchTenantById } from "../../slices/tenantSlice";
import EditTenantCard from "../../components/admin/EditTenantCard";

export const AdminConfig = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const tenant = useSelector((state: RootState) => state.tenant.tenant);

  useEffect(() => {
    if (!tenant || tenant.id !== tenantProfile.id) {
      dispatch(fetchTenantById(tenantProfile.id));
    }
  }, [tenant, tenantProfile.id, dispatch]);

  return (
    <>
      <Helmet title={`Administrator - Config | ${tenantProfile.name}`}></Helmet>
      <Transition animation={"fade"}>
        <Container textAlign="center">
          <Card.Group itemsPerRow={"1"} stackable>
            <EditTenantCard />
          </Card.Group>
        </Container>
      </Transition>
    </>
  );
};
