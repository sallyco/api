import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { TenantContext } from "../../contexts";
import { Container, Transition, Card } from "semantic-ui-react";
import { EmailEditSelectorCard } from "../../components/admin/EmailEditSelectorCard";

export const AdminEmails = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet title={`Administrator - Emails | ${tenantProfile.name}`}></Helmet>
      <Transition animation={"fade"}>
        <Container textAlign="center">
          <Card.Group itemsPerRow={"1"} stackable>
            <EmailEditSelectorCard />
          </Card.Group>
        </Container>
      </Transition>
    </>
  );
};
