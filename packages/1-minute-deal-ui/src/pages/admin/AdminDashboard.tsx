import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { TenantContext } from "../../contexts";

export const AdminDashboard = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet title={`Administrator | ${tenantProfile.name}`}></Helmet>
    </>
  );
};
