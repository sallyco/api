import React, { useState, useEffect, useContext } from "react";
import { Helmet } from "react-helmet";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { TenantContext } from "../../contexts";
import { fetchDealsList } from "../../slices/dealsSlice";
import {
  fetchClosesList,
  fundManagerSignClose,
  setSelectedClose,
} from "../../slices/closesSlice";
import { Close } from "../../api/closesApi";
import {
  Container,
  Transition,
  Grid,
  Dropdown,
  Header,
  Divider,
  Form,
  Button,
} from "semantic-ui-react";
import { Formik } from "formik";
import moment from "moment";
import SignatureForm, {
  validationSchema as SignatureFormValidation,
} from "../../forms/signatures/SignatureForm";
import PdfDocument from "../../components/PdfDocument";

export const AdminSignings = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const [showForm, setShowForm] = useState(false);

  const { dealsById } = useSelector((state: RootState) => state.deals);
  useEffect(() => {
    dispatch(fetchDealsList());
  }, []);

  const { closesById, selectedCloseId } = useSelector(
    (state: RootState) => state.closes
  );
  useEffect(() => {
    dispatch(fetchClosesList());
  }, []);

  const readyToSign = (el: Close) => {
    return !el.fundManagerSigned;
  };

  const validation = Yup.object().shape({
    ...SignatureFormValidation({
      fieldName: "managerSignature",
    }),
  });

  async function onSubmit(data) {
    dispatch(fundManagerSignClose(selectedCloseId, data));
    dispatch(setSelectedClose(""));
    setShowForm(false);
  }

  return (
    <>
      <Helmet
        title={`Administrator - Signings | ${tenantProfile.name}`}
      ></Helmet>
      <Transition animation={"fade"}>
        <Container text>
          <Grid style={{ paddingTop: "2rem" }}>
            <Grid.Row columns={"equal"}>
              <Grid.Column>
                <Header content={"SPVs Ready for Signing"} />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={"equal"}>
              <Grid.Column>
                {closesById && (
                  <Dropdown
                    key={Object.values(closesById).filter(readyToSign).length}
                    clearable
                    fluid
                    search
                    selection
                    options={Object.values(closesById)
                      .filter(readyToSign)
                      .map((close) => {
                        return {
                          key: close.id,
                          value: close.id,
                          text: `${dealsById[close.dealId]?.name} -- ${moment
                            .utc(close.createdAt)
                            .format("MMMM DD, YYYY")}`,
                        };
                      })}
                    closeOnChange
                    closeOnEscape
                    placeholder="Choose a close"
                    onChange={(e, { value }) => {
                      dispatch(setSelectedClose(value));
                      setShowForm(value !== "");
                    }}
                  />
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Divider />
          {showForm && (
            <Container textAlign={"center"}>
              <Formik
                initialValues={{}}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form
                    onSubmit={props.handleSubmit}
                    data-testid={"login-form"}
                  >
                    <SignatureForm
                      name={"managerSignature"}
                      id={"managerSignature"}
                    />
                    {closesById[selectedCloseId]?.files &&
                      closesById[selectedCloseId]?.files.map((doc) => (
                        <div key={doc}>
                          <PdfDocument fileId={doc} pageNumber={1} />
                          <Divider hidden />
                        </div>
                      ))}
                    <Button
                      primary
                      fluid
                      type="submit"
                      data-testid={"submit-button"}
                      loading={props.isSubmitting}
                      disabled={!props.dirty || !props.isValid}
                      content="Sign Documents"
                    />
                  </Form>
                )}
              </Formik>
            </Container>
          )}
        </Container>
      </Transition>
    </>
  );
};
