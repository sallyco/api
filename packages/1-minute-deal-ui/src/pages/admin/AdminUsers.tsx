import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { fetchUsersList } from "../../slices/usersSlice";
import { AdminBanner } from "../../components/common/AdminBanner";
import { RootState } from "../../rootReducer";

import {
  Segment,
  Icon,
  Table,
  Header,
  Container,
  Menu,
  Divider,
  Form,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../../components/FormField";

import { TenantContext } from "../../contexts";

export const AdminUsers = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const [searchTerm, setSearchTerm] = useState("");

  const { isLoading, isInitialized, usersById, currentPageUsers } = useSelector(
    (state: RootState) => state.users
  );

  const users = currentPageUsers.map((userID) => usersById[userID]);

  useEffect(() => {
    if (!isInitialized) {
      dispatch(fetchUsersList());
    }
  }, [dispatch, isInitialized]);

  async function startImpersonateSession() {}

  return (
    <>
      <Helmet title={`Administrator - Users | ${tenantProfile.name}`}></Helmet>

      <Menu borderless fluid compact size={"small"} stackable>
        <Container>
          {/*
          <Menu.Item fitted>
            <Item.Group>
              <Item>
                <Item.Content inverted>
                  <Item.Header>Search and impersonate users</Item.Header>
                  <Item.Extra>
                    Enter an Email, Phone, City, Username, <br /> Deal Name, or
                    other unique identifying <br /> information
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Menu.Item>
*/}
          <Menu.Item>
            <Formik
              initialValues={{ search: "" }}
              enableReinitialize={true}
              onSubmit={(val) => {
                setSearchTerm(val.search);
              }}
            >
              {(props) => (
                <Form onSubmit={props.handleSubmit} autoComplete="off">
                  <Form.Group widths="equal">
                    <FormField
                      name="search"
                      icon="search"
                      iconPosition="left"
                      placeholder="jwilson@venture.co"
                      onChange={props.handleSubmit}
                      onBlur={props.handleSubmit}
                    />
                    <Form.Button type="submit" primary fluid content="Search" />
                  </Form.Group>
                </Form>
              )}
            </Formik>
          </Menu.Item>
        </Container>
      </Menu>

      <Divider hidden />
      {!isInitialized ? (
        <Segment placeholder basic>
          <Header icon>
            <Icon name="find" />
            Conduct a search to view results
          </Header>
        </Segment>
      ) : (
        <Container>
          <Table
            celled
            size="small"
            selectable
            striped
            compact="very"
            unstackable
          >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Email</Table.HeaderCell>
                <Table.HeaderCell>First Name</Table.HeaderCell>
                <Table.HeaderCell>Last Name</Table.HeaderCell>
                {/* <Table.HeaderCell>Impersonate</Table.HeaderCell> */}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {users
                .filter((el) => {
                  if (!searchTerm || searchTerm.length === 0) {
                    return true;
                  }
                  return (
                    el.email.toLowerCase().includes(searchTerm.toLowerCase()) ||
                    el.firstName
                      .toLowerCase()
                      .includes(searchTerm.toLowerCase()) ||
                    el.lastName.toLowerCase().includes(searchTerm.toLowerCase())
                  );
                })
                .map((user) => (
                  <Table.Row key={user.id}>
                    <Table.Cell>{user.email}</Table.Cell>
                    <Table.Cell>{user.firstName}</Table.Cell>
                    <Table.Cell>{user.lastName}</Table.Cell>
                    {/* <Table.Cell>
                      <Button primary compact content="Impersonate" />
                    </Table.Cell> */}
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
        </Container>
      )}
    </>
  );
};
//onClick={() => startImpersonateSession(user.id)}
