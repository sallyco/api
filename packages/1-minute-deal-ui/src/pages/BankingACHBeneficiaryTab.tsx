import { Formik, Field } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { Button, Container, Divider, Form, Header } from "semantic-ui-react";
import { toast } from "react-toastify";
import { FormField } from "../forms/common/FormField";
import SubformPhone, {
  validationSchema as phoneValidation,
} from "../forms/subforms/SubformPhone";
import {
  BankingBeneficiary,
  postACHBankingBeneficiary,
} from "../api/bankingApi";
import { Alert, Box } from "@mui/material";

export default function BankingBeneficiaryTab({
  onSuccessHandler = (beneficiary: BankingBeneficiary) => {},
  onErrorHandler = () => {},
}) {
  const [failureMessage, setFailureMessage] = useState<null | string>();
  const [submitting, setSubmitting] = useState(false);
  const validation = Yup.object().shape({
    nameOnAccount: Yup.string().trim().required(),
    routingNumber: Yup.string()
      .length(9, "Must be a 9 digit routing number")
      .test("Digits", "Only numbers are allowed", (value) =>
        /^\d+$/.test(value)
      )
      .required(),
    accountNumber: Yup.string().required(),
    verifyAccountNumber: Yup.string()
      .oneOf([Yup.ref("accountNumber")], "Account numbers must match")
      .required(),
    email: Yup.string().email().required(),
    ...phoneValidation({ isRequired: false }),
    accountType: Yup.string().required(),
  });

  async function onSubmit(data) {
    try {
      const submitData = {
        email: data.email,
        phone: data.phone,
        nameOnAccount: data.nameOnAccount,
        accountNumber: data.accountNumber,
        routingNumber: data.routingNumber,
        accountType: data.accountType,
      };
      setSubmitting(true);
      const result = await postACHBankingBeneficiary(submitData);
      setSubmitting(false);
      toast.success("Beneficiary was created");
      onSuccessHandler(result);
    } catch (err) {
      const e: any = err;
      setSubmitting(false);
      setFailureMessage(
        `Beneficiary was not saved: \n${
          e?.response?.data?.error?.message ?? e.message
        }`
      );
      onErrorHandler();
    }
  }

  return (
    <>
      <Container className="text">
        <Divider hidden />
        <Formik
          initialValues={{
            nameOnAccount: "",
            routingNumber: "",
            accountNumber: "",
            verifyAccountNumber: "",
            accountType: "",
          }}
          onSubmit={onSubmit}
          validationSchema={validation}
        >
          {(props) => (
            <Form onSubmit={props.handleSubmit} className="dark-labels">
              <FormField
                name="email"
                component={Form.Input}
                label="Recipient Email"
                placeholder="beneficiary@email.com"
                required
              />
              <SubformPhone />

              <Divider hidden />
              <Header content="Account Information" />
              <FormField
                name="routingNumber"
                component={Form.Input}
                label="ABA Routing Number"
                placeholder="ABA Routing Number"
                required
              />

              <FormField
                name="nameOnAccount"
                component={Form.Input}
                label="Name on Account"
                placeholder="Account Name"
                required
              />
              <FormField
                name={"accountNumber"}
                placeholder={"Account Number"}
                label="Account Number"
                required
              />
              <FormField
                name={"verifyAccountNumber"}
                placeholder={"Verify Account Number"}
                required
              />
              <Header content="Checking or Savings Account?" />

              <Form.Group>
                <FormField
                  label={"Checking"}
                  component={Form.Radio}
                  name="accountType"
                  value="check"
                />
                <FormField
                  component={Form.Radio}
                  label={"Savings"}
                  name="accountType"
                  value="saving"
                />
              </Form.Group>

              <Divider horizontal />
              {failureMessage && (
                <>
                  <Alert
                    severity={"error"}
                    onClose={() => setFailureMessage(null)}
                  >
                    {failureMessage}
                  </Alert>
                  <Divider hidden />
                </>
              )}
              <Button
                loading={submitting}
                disabled={submitting}
                type="submit"
                primary
                fluid
                content="Add Beneficiary"
              />
            </Form>
          )}
        </Formik>
      </Container>
    </>
  );
}
