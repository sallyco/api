import React, { useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useLocation, useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Card, Container, Divider, Transition } from "semantic-ui-react";
import { ExcecutiveSummary } from "../forms/CompanyProfileEdit/ExcecutiveSummary";
import { TenantContext } from "../contexts";
import { RootState } from "../rootReducer";
import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import { updateAssetById } from "../slices/assetsSlice";
import { toast } from "react-toastify";

export const ExcecutiveSummaryEditPage = () => {
  const tenantProfile = useContext(TenantContext);
  const dispatch = useDispatch();
  let history = useHistory();
  const { isSubmitting } = useSelector((state: RootState) => state.assets);
  const { companyId } = useParams<{ companyId: string }>();

  async function onSubmit(data) {
    const profileData1 = {
      id: companyId,
      managementOverview: data.managementOverView
        ? data.managementOverView
        : "",
      customerProblem: data.customerProblem ? data.customerProblem : "",
      targetMarketSize: data.targetMarketSize ? data.targetMarketSize : "",
      customerPotential: data.customerPotential ? data.customerPotential : "",
      businessModel: data.businessModel ? data.businessModel : "",
      competitiveAdvantage: data.competitiveAdvantage
        ? data.competitiveAdvantage
        : "",
      productsServices: data.productsServices ? data.productsServices : "",
      marketingStrategy: data.marketingStrategy ? data.marketingStrategy : "",
      competitors: data.competitors ? data.competitors : "",
    };
    try {
      const company: any = await dispatch(updateAssetById(profileData1));
      toast.success(`Company "${company.name}" Updated`);
      Log.debug("Company", company);
      history.push(`/assets/companies/${companyId}`);
    } catch (e) {
      if (e.response) {
        ToastError(e.response);
      }
    }
    return;
  }
  return (
    <>
      <Helmet
        title={`Edit Excecutive Summary | ${tenantProfile.name}`}
      ></Helmet>
      <Transition animation={"fade left"}>
        <Container className="mobile-container">
          <Divider hidden />
          <Card.Group itemsPerRow="one" centered stackable>
            <ExcecutiveSummary
              onSubmit={onSubmit}
              submitting={isSubmitting}
              companyId={companyId}
            />
          </Card.Group>
        </Container>
      </Transition>
    </>
  );
};
