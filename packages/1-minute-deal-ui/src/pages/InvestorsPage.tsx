/* eslint-disable react/display-name */
import InvestorListTable from "../components/investors/InvestorListTable";
import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { useSelector } from "react-redux";
import { RootState } from "../rootReducer";
import { Container, Divider, Header } from "semantic-ui-react";
import useSWR from "swr";
import { TenantContext } from "../contexts";
import { fetcher } from "../swrConnector";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const InvestorsPage = () => {
  const tenantProfile = useContext(TenantContext);

  const { self } = useSelector((state: RootState) => state.users);

  const { data: investors, error } = useSWR(
    () => baseUrl + `/investors-for-user/${self.id}`,
    fetcher
  );

  return (
    <>
      <Helmet title={`Investors | ${tenantProfile.name}`} />
      <Container>
        <Divider hidden />
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            paddingBottom: "8px",
          }}
        >
          <Header size="medium">Investors</Header>
        </div>
        {investors && (
          <InvestorListTable investors={investors} ownerId={self.id} />
        )}
      </Container>
    </>
  );
};
