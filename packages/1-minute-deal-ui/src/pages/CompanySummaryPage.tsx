import React, { useContext, useState, useEffect, useRef } from "react";
import { Helmet } from "react-helmet";
import DocumentRow from "../components/files/DocumentRow";
import {
  Divider,
  Item,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Label,
  Container,
  Card,
  Segment,
  Table,
} from "semantic-ui-react";
import ShortHeader from "../components/ShortHeader";
import { TenantContext } from "../contexts";
import { updateAssetById } from "../slices/assetsSlice";
import { useHistory, useParams } from "react-router-dom";
import Log from "../tools/Log";
import { toast } from "react-toastify";
import { ToastError } from "../tools/ToastMessage";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import {
  SubformFileUpload,
  FileSizes,
} from "../forms/subforms/SubformFileUpload";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import useSelf from "../hooks/useSelf";
import { PencilIcon } from "react-line-awesome";
import { CompanyAssetEdit } from "../forms/CompanyProfileEdit/CompanyAssetEdit";
import { useDispatch } from "react-redux";
import { toTitleCase } from "../tools/helpers";
import { API, doChange, useRequest } from "../api/swrApi";
import { Asset } from "../api/assetsApi";
import Cookies from "js-cookie";
import NumberFormat from "react-number-format";

const getSession = () => {
  const jwt = Cookies.get("__session");
  let session;
  try {
    if (jwt) {
      const base64Url = jwt.split(".")[1];
      const base64 = base64Url.replace("-", "+").replace("_", "/");
      session = JSON.parse(window.atob(base64));
    }
  } catch (error) {
    Log.debug("getSession", error);
  }
  return session;
};

export const CompanySummaryPage = () => {
  const dispatch = useDispatch();
  const session = getSession();
  const history = useHistory();
  const { companyId } = useParams<{ companyId: string }>();
  const fileInputRef: any = useRef();
  const fileInputRef1: any = useRef();
  const tenantProfile = useContext(TenantContext);
  const [editing, setEditing] = useState(false);
  const [viewAssessment, setViewAssessment] = useState(false);
  const foundersAssessment = useFeatureFlag("founders_assessment");

  const { data: company, revalidate: revalidateCompany } = useRequest<
    Partial<Asset>
  >(
    API.ADD_FILTER(API.ASSET_BY_ID(companyId), {
      include: ["deal"],
      fields: {
        logo: false,
        banner: false,
      },
    })
  );

  useEffect(() => {
    if (
      session.resource_access.account.roles.includes("founder") &&
      company &&
      !company?.founderProfileId
    ) {
      history.push(`/profiles/create/founder?companyId=${companyId}`);
    } else if (
      session.resource_access.account.roles.includes("founder") &&
      company &&
      foundersAssessment &&
      !company?.foundersAssessment
    ) {
      history.push(`/profiles/assessment`);
    }
  }, [session, company, history]);

  const { data: logo } = useRequest<Asset>(
    company &&
      API.ADD_FILTER(API.ASSET_BY_ID(companyId), {
        fields: ["logo"],
      })
  );
  const { data: banner } = useRequest<Asset>(
    company &&
      API.ADD_FILTER(API.ASSET_BY_ID(companyId), {
        fields: ["banner"],
      })
  );

  const self = useSelf();

  const useStyles = makeStyles((theme) => {
    return {
      root: {
        "& .ui.form .field > label": {
          color: "#000",
        },
        "& .ui.form .field.error label": {
          color: "#D00 !important",
        },
      },
      content: {
        "& img": {
          maxWidth: "100%",
        },
      },
    };
  });

  async function onSubmitLogo(data) {
    try {
      await doChange(
        API.ASSET_BY_ID(companyId),
        {
          logo: data,
        },
        "PATCH"
      );
      await revalidateCompany();
      toast.success(`Company Logo Updated`);
    } catch (e) {
      const err: any = e;
      if (err?.response) {
        ToastError(err.response);
      }
    }
  }

  async function onSubmitBanner(data) {
    const profileData1 = {
      ...company,
      banner: data,
    };
    try {
      // TODO figure out how to use proper type (Profile) not any
      await dispatch(updateAssetById(profileData1));
      toast.success(`Company Banner Updated`);
      Log.debug("profile", company);
    } catch (e) {
      if (e.response) {
        ToastError(e.response);
      }
    }
  }

  const fileChange = (e) => {
    Log.debug("file ", e.target.files[0]);
    var file = fileInputRef.current.files[0];
    let reader = new FileReader();
    if (file.size <= 8388608) {
      reader.onload = async function (event) {
        Log.debug("event ", event);
        await onSubmitLogo(event?.target?.result ?? undefined);
      };
      reader.readAsDataURL(file);
    } else {
      toast.error("File is too large");
    }
  };

  const fileChange1 = (e) => {
    Log.debug("file ", e.target.files[0]);
    var file = fileInputRef1.current.files[0];
    let reader = new FileReader();
    if (file.size <= 8388608) {
      reader.onload = function (event) {
        Log.debug("event ", event);
        onSubmitBanner(event?.target?.result ?? undefined);
      };
      reader.readAsDataURL(file);
    } else {
      toast.error("File is too large");
    }
  };

  const classes = useStyles();

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "grey-background" }}
        title={`Company Profile Preview | ${tenantProfile.name}`}
      />
      <ShortHeader title="Company Profile Preview" />
      <Container>
        <Divider hidden />
        {(self?.id === company?.ownerId ||
          (company?.founderIds ?? []).includes(self?.email)) && (
          <Box textAlign="right">
            <Button
              variant="contained"
              color="secondary"
              startIcon={<PencilIcon />}
              onClick={() => setEditing(true)}
            >
              Edit
            </Button>
            {company?.foundersAssessment && (
              <Button
                variant="contained"
                color="secondary"
                onClick={() => setViewAssessment(true)}
                style={{ marginLeft: "4px" }}
              >
                View Assessment
              </Button>
            )}
          </Box>
        )}
        {session.resource_access.account.roles.includes("organizer") &&
          company?.foundersAssessment && (
            <Box textAlign="right">
              <Button
                variant="contained"
                color="secondary"
                onClick={() => setViewAssessment(true)}
                style={{ marginLeft: "4px" }}
              >
                View Assessment
              </Button>
            </Box>
          )}

        <Card color="grey" fluid>
          <Card.Content>
            <Segment
              basic
              clearing
              padded="very"
              style={{
                backgroundImage: `url(${banner?.banner})`,
                backgroundSize: "cover",
                fontWeight: "normal",
                margin: "-14px",
              }}
            >
              <Button
                size="small"
                variant={"contained"}
                onClick={() => fileInputRef1.current.click()}
              >
                Change Banner Image
              </Button>
              <input
                ref={fileInputRef1}
                type="file"
                name="headshot"
                hidden
                onChange={fileChange1}
                accept="image/*"
              />
            </Segment>
            <Card
              centered
              style={{
                width: 150,
                height: 150,
                marginTop: -70,
              }}
            >
              <Icon.Group size="tiny">
                {company && (
                  <Image src={logo?.logo} style={{ width: 150, height: 150 }} />
                )}
                {!logo?.logo && (
                  <Card.Content>
                    <h4 style={{ marginTop: 10, textAlign: "center" }}>LOGO</h4>
                  </Card.Content>
                )}
                <Label size="tiny" attached="top right">
                  <Button
                    size="small"
                    variant={"contained"}
                    onClick={() => fileInputRef.current.click()}
                  >
                    Change Logo
                  </Button>
                  <input
                    ref={fileInputRef}
                    type="file"
                    name="headshot"
                    hidden
                    onChange={fileChange}
                    accept="image/*"
                  />
                </Label>
              </Icon.Group>
            </Card>
            <Grid stackable>
              <Grid.Row>
                <Grid.Column>
                  <Header as={"h1"}>
                    {company?.name}
                    <Header.Subheader>
                      <Card.Meta>
                        {toTitleCase(
                          company?.properties?.filter(
                            (entry) => entry.key === "Market Sector"
                          )[0].value
                        )}
                      </Card.Meta>
                    </Header.Subheader>
                  </Header>
                  <Card.Description>
                    <Box
                      display={"flex"}
                      flexDirection={"row"}
                      flexWrap={"wrap"}
                      justifyContent={"space-between"}
                    >
                      {company?.properties?.map((entry) => (
                        <Box key={entry.key} mb={2}>
                          <Typography
                            variant={"subtitle1"}
                            color={"textSecondary"}
                            align={"center"}
                            style={{
                              fontSize: "1.1rem",
                              textTransform: "uppercase",
                            }}
                          >
                            {entry.key}
                          </Typography>
                          <Typography align={"center"}>
                            {(entry?.value ?? "").search("_") > -1
                              ? toTitleCase(entry?.value)
                              : entry?.value}
                          </Typography>
                        </Box>
                      ))}
                    </Box>
                    <Box
                      display={"flex"}
                      width={"100%"}
                      justifyContent={"space-between"}
                    >
                      <Box>
                        <Header as="h5" className="no-spacing">
                          Target Raise Amount
                        </Header>
                        <Header as="h2" color="green" className="no-spacing">
                          <NumberFormat
                            value={company?.funding?.targetRaiseAmount}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"$"}
                          />
                        </Header>
                      </Box>
                      <Box>
                        {company?.funding?.round &&
                          company?.funding?.round !== "None" && (
                            <>
                              <Header as="h5" className="no-spacing">
                                Round
                              </Header>
                              <Header as="h2" className="no-spacing">
                                {company?.funding?.round}
                              </Header>
                            </>
                          )}
                      </Box>
                      <Box>
                        <Header as="h5" className="no-spacing">
                          Share Price
                        </Header>
                        <Header as="h2" color="green" className="no-spacing">
                          <NumberFormat
                            value={company?.funding?.sharePrice}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"$"}
                          />
                        </Header>
                      </Box>
                      <Box>
                        <Header as="h5" className="no-spacing">
                          Number of Shares
                        </Header>
                        <Header as="h2" color="green" className="no-spacing">
                          <NumberFormat
                            value={company?.funding?.noOfShare}
                            displayType={"text"}
                            thousandSeparator={true}
                          />
                        </Header>
                      </Box>
                      <Box>
                        <Header as="h5" className="no-spacing">
                          Security Type
                        </Header>
                        <Header as="h2" color="green" className="no-spacing">
                          {toTitleCase(company?.funding?.securityType)}
                        </Header>
                      </Box>
                    </Box>
                    <Divider />
                    <div
                      className={classes.content}
                      dangerouslySetInnerHTML={{
                        __html: company?.description,
                      }}
                    />
                  </Card.Description>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Card.Content>
        </Card>

        <Grid stackable>
          <Grid.Row>
            <Grid.Column width={11}>
              <Card fluid>
                <Card.Content>
                  <Card.Header>Executive Summary</Card.Header>
                </Card.Content>
                <Card.Content>
                  <List relaxed>
                    {Array.isArray(company?.details) &&
                      company?.details?.map((entry) => (
                        <List.Item key={entry.heading}>
                          <List.Header>{entry.heading}</List.Header>
                          <div
                            className={classes.content}
                            dangerouslySetInnerHTML={{ __html: entry.body }}
                          />
                        </List.Item>
                      ))}
                    {typeof company?.details === "string" && (
                      <div
                        className={classes.content}
                        dangerouslySetInnerHTML={{ __html: company?.details }}
                      />
                    )}
                  </List>
                </Card.Content>
              </Card>

              <Card fluid>
                <Card.Content>
                  <Card.Header>Team</Card.Header>
                  <Item.Group>
                    {company?.team?.map((teamobj) => (
                      <Item key={teamobj.name}>
                        <Item.Image circular size="tiny" src={teamobj.image} />

                        <Item.Content>
                          <Item.Header>
                            {teamobj.name}{" "}
                            <Icon
                              name="linkedin"
                              link
                              to={teamobj.linkedInUrl}
                              style={{ color: "#0e76a8" }}
                            />
                          </Item.Header>
                          <Item.Meta>{teamobj.role}</Item.Meta>
                        </Item.Content>
                      </Item>
                    ))}
                  </Item.Group>
                </Card.Content>
              </Card>
            </Grid.Column>
            <Grid.Column width={5}>
              <Card fluid>
                <Card.Content>
                  <Card.Header>
                    <Grid columns="equal">
                      <Grid.Column width={13}>Pitch Deck and Files</Grid.Column>
                      <Grid.Column textAlign="right">
                        <SubformFileUpload
                          maxSize={FileSizes.MB * 50}
                          onSuccess={async (fileIds) => {
                            const upload = company?.deal?.files
                              ? [...company?.deal?.files, ...fileIds]
                              : fileIds;
                            await doChange(
                              API.DEAL_BY_ID(company?.deal?.id),
                              {
                                files: upload,
                              },
                              "PATCH"
                            );
                            await revalidateCompany();
                            toast.success(
                              `File${
                                fileIds.length > 1 ? "s" : ""
                              } uploaded to deal`
                            );
                          }}
                          onError={(errors) => {
                            errors.forEach((error) => {
                              toast.error(error);
                            });
                          }}
                        >
                          {(props) => {
                            return (
                              <Icon
                                className={"primaryColor"}
                                link
                                bordered
                                circular
                                loading={props.uploading}
                                name={props.uploading ? "spinner" : "upload"}
                                size="small"
                                onClick={props.triggerFileUpload}
                              />
                            );
                          }}
                        </SubformFileUpload>
                      </Grid.Column>
                    </Grid>
                  </Card.Header>
                </Card.Content>
                <Card.Content>
                  <List>
                    <Table>
                      {company?.deal?.files?.map((fileId) => (
                        <DocumentRow
                          key={fileId}
                          fileId={fileId}
                          canDelete={true}
                          onDelete={async () => {
                            const files = company?.deal?.files.filter(
                              (file) => file !== fileId
                            );
                            await doChange(
                              API.DEAL_BY_ID(company?.deal?.id),
                              {
                                files: files,
                              },
                              "PATCH"
                            );
                            await revalidateCompany();
                          }}
                        />
                      ))}
                    </Table>
                  </List>
                </Card.Content>
              </Card>

              <Card fluid>
                <Card.Content>
                  <Card.Header>Company Details</Card.Header>
                </Card.Content>
                <Card.Content>
                  <List>
                    <List.Item>
                      <List.Header>Contact</List.Header>
                    </List.Item>
                    <List.Item>{company?.contact?.name}</List.Item>
                    <List.Item>{company?.contact?.phone}</List.Item>
                    <List.Item>{company?.contact?.email}</List.Item>
                    <List.Item />
                    <List.Item>
                      <List.Header>Advisors</List.Header>
                    </List.Item>
                    <List.Item>{company?.advisors}</List.Item>
                  </List>
                </Card.Content>
              </Card>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Dialog
          open={editing}
          fullWidth={true}
          maxWidth={"lg"}
          onClose={() => setEditing(false)}
        >
          <DialogTitle>Editing Company &quot;{company?.name}&quot;</DialogTitle>
          <DialogContent className={classes.root}>
            <DialogContentText>
              <CompanyAssetEdit
                assetId={companyId}
                onSave={() => setEditing(false)}
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setEditing(false)} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={viewAssessment}
          fullWidth={true}
          maxWidth={"lg"}
          onClose={() => setViewAssessment(false)}
        >
          <DialogTitle>Assessment Responses</DialogTitle>
          <DialogContent className={classes.root}>
            <DialogContentText>
              {Object.keys(company?.foundersAssessment ?? {}).map(function (
                key,
                index
              ) {
                return (
                  <>
                    <Header as="h3" dividing>
                      {key}
                    </Header>
                    <List divided relaxed>
                      {Object.keys(company?.foundersAssessment[key] ?? {}).map(
                        function (key2, index) {
                          return (
                            <List.Item key={key2}>
                              <List.Content>
                                <List.Header>
                                  {company?.foundersAssessment[key][key2]
                                    .title ?? ""}
                                </List.Header>
                                <List.Description>
                                  {(Array.isArray(
                                    company?.foundersAssessment[key][key2]
                                      .response
                                  )
                                    ? company?.foundersAssessment[key][
                                        key2
                                      ].response.join(", ")
                                    : company?.foundersAssessment[key][key2]
                                        .response) ?? ""}
                                </List.Description>
                              </List.Content>
                            </List.Item>
                          );
                        }
                      )}
                    </List>
                  </>
                );
              })}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setViewAssessment(false)} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    </>
  );
};
