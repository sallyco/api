import React, { useContext, useEffect } from "react";
import { useSelector } from "react-redux";
import { Helmet } from "react-helmet";

import { RootState } from "../rootReducer";

import { Loader, Divider } from "semantic-ui-react";

import { SubscriptionsList } from "../components/subscriptions/SubscriptionsList";
import { TenantContext } from "../contexts";
import { API, useRequest } from "../api/swrApi";
import { searchBySubscription } from "../api/subscriptionsApi";

interface SLProps {
  page: number;
  perPage: number;
  totalCount: number;
}

export const SubscriptionsListPage = ({
  page = 1,
  perPage = 10,
  totalCount = 0,
}: SLProps) => {
  const tenantProfile = useContext(TenantContext);

  const { data: subscriptions, mutate: mutateList } = useRequest(
    API.SUBSCRIPTIONS
  );

  const { opType, opValue, opFilterValue } = useSelector(
    (state: RootState) => state.subscriptionsListOpeartions
  );

  useEffect(() => {
    const search = async (_opType, _opValue, _opFilterValue) => {
      switch (_opType) {
        case "search":
          if (_opValue) {
            const mainsearch = {
              dealSearch: [{ field: "name", value: _opValue }],
            };
            mutateList(await searchBySubscription(mainsearch), false);
          }
          break;
        case "filter":
          if (_opFilterValue) {
            const mainsearch = {
              search: _opFilterValue,
            };
            mutateList(await searchBySubscription(mainsearch), false);
          }
          break;
        case "sort":
          if (_opValue) {
            const splitdata = _opValue.split("-");
            const mainsearch = {
              sort: [{ field: splitdata[0], direction: splitdata[1] }],
            };
            mutateList(await searchBySubscription(mainsearch), false);
          }
          break;
        default:
          mutateList(await searchBySubscription({}), false);
          break;
      }
    };
    search(opType, opValue, opFilterValue);
  }, [opValue, opFilterValue]);

  return (
    <>
      <Helmet title={`Investments - Dashboard | ${tenantProfile.name}`} />
      {!subscriptions ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <SubscriptionsList
          subscriptions={subscriptions.data}
          context={"investor"}
        />
      )}
    </>
  );
};
