import React, { useState, useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { Link, useParams, useHistory } from "react-router-dom";
import DealDocumentsCard from "../components/deals/DealDocumentsCard";
import CompanyDocumentsCard from "../components/deals/CompanyDocumentsCard";

import { RootState } from "../rootReducer";

import {
  fetchSubscriptionById,
  updateSubscriptionById,
} from "../slices/subscriptionsSlice";
import { fetchAssetById } from "../slices/assetsSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchProfileById } from "../slices/profilesSlice";
import { insertNotification } from "../slices/notificationsSlice";
import { fetchSelf } from "../slices/usersSlice";
import ImageFileGallery from "../components/ImageFileGallery";

import {
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Button,
  Container,
  Segment,
  Transition,
  Loader,
  Card,
  List,
} from "semantic-ui-react";
import Mailto from "../components/Mailto";

import Document from "../components/Document";
import ShortHeader from "../components/ShortHeader";
import NumberFormat from "react-number-format";
import moment from "moment";
import { TenantContext } from "../contexts";

export const AssetViewPage = () => {
  const dispatch = useDispatch();
  const { assetId } = useParams<{ assetId: string }>();

  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  // ASSET
  const { isLoading: assetsLoading } = useSelector(
    (state: RootState) => state.assets
  );
  const asset = useSelector(
    (state: RootState) => state.assets.assetsById[assetId]
  );
  useEffect(() => {
    if ((!asset || asset.id !== assetId) && !assetsLoading) {
      dispatch(fetchAssetById(assetId));
    }
  }, [dispatch, asset, assetId, assetsLoading]);

  async function handleClick() {}

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "grey-background" }}
        title={`${asset?.name ?? "Loading..."} | ${tenantProfile.name}`}
      ></Helmet>
      <ShortHeader title="Artwork Details" hideBackArrow />
      {!asset || assetsLoading ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <Container text>
          <Divider hidden />
          <Grid stackable>
            <Grid.Row columns={2}>
              <Grid.Column width={10}>
                {asset?.images?.length && (
                  <ImageFileGallery fileIds={asset.images} />
                )}
              </Grid.Column>
              <Grid.Column width={6}>
                <Header as="h2">
                  {asset.name}
                  <Header.Subheader content={asset?.artist?.name} />
                </Header>
                <Divider hidden />
                <Header as="h3">
                  <NumberFormat
                    value={asset?.fractionalOwnershipAmount}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"$"}
                  />
                  <Header.Subheader>Fractional Ownership</Header.Subheader>
                </Header>
                <Button fluid color="green" content="Invest Now" disabled />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
              <Grid.Column>
                <Header as="h2" content="About" />
                {asset.description}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
              <Grid.Column>
                <Header as="h2" content="Details" />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
              <Grid.Column>
                <Header
                  as="h4"
                  content="Artist Name"
                  subheader={asset?.artist?.name}
                  dividing
                />
                <Header
                  as="h4"
                  content="Artist Location"
                  subheader={asset?.artist?.location}
                  dividing
                />
                <Header as="h4" dividing>
                  Appraised Value
                  <Header.Subheader>
                    <NumberFormat
                      value={asset?.appraisedValue}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                    />
                  </Header.Subheader>
                </Header>
              </Grid.Column>
              <Grid.Column>
                <Header
                  as="h4"
                  content="Size"
                  subheader={asset?.size}
                  dividing
                />
                <Header
                  as="h4"
                  content="Category"
                  subheader={asset?.category}
                  dividing
                />
                <Header as="h4" dividing>
                  Fractional Ownership Amount
                  <Header.Subheader>
                    <NumberFormat
                      value={asset?.fractionalOwnershipAmount}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                    />
                  </Header.Subheader>
                </Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      )}
    </>
  );
};
