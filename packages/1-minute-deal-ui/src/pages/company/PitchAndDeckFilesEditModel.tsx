import React, { useState } from "react";
import { Modal, Grid, Button, Header, Divider } from "semantic-ui-react";
import PropTypes from "prop-types";
import { PitchAndDeckFiles } from "../../forms/CompanyProfileEdit/PitchAndDeckFiles";

interface Props {
  isShowModal: boolean;
  companyId: string;
  filetype: number;
  fileId: string;
  onSubmit: (any) => void;
}

const PitchAndDeckFilesEditModel = ({
  isShowModal,
  companyId,
  filetype,
  fileId,
  onSubmit,
}: Props) => (
  <Modal
    onClose={() => onSubmit(false)}
    open={isShowModal}
    size="tiny"
    closeIcon
  >
    <Header textAlign="center">
      {filetype === 1 ? "Pitch Deck" : "Supporting File"}
    </Header>
    <Modal.Content>
      <Modal.Description>
        <PitchAndDeckFiles
          onSubmit={onSubmit}
          companyId={companyId}
          filetype={filetype}
          fileId={fileId}
        />
      </Modal.Description>
    </Modal.Content>
  </Modal>
);

PitchAndDeckFilesEditModel.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  companyId: PropTypes.string.isRequired,
  filetype: PropTypes.number.isRequired,
  fileId: PropTypes.string.isRequired,
  onSubmit: PropTypes.func,
};

export default PitchAndDeckFilesEditModel;
