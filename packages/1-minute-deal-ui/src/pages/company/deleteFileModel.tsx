import React from "react";
import { Modal, Grid, Button, Header, Divider, Image } from "semantic-ui-react";
import PropTypes from "prop-types";

const DeleteFileModel = ({ isShowModal, onSubmit, name }) => (
  <Modal
    onClose={() => onSubmit(false)}
    open={isShowModal}
    size="tiny"
    closeIcon
  >
    <Modal.Content>
      <p>Are you sure you want to remove {name}</p>
    </Modal.Content>
    <Modal.Actions>
      <Button color="teal" inverted circular onClick={() => onSubmit(false)}>
        Cancel
      </Button>
      <Button color="teal" circular onClick={() => onSubmit(true)}>
        Delete
      </Button>
    </Modal.Actions>
  </Modal>
);

DeleteFileModel.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func,
  name: PropTypes.string.isRequired,
};

export default DeleteFileModel;
