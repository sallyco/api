import React from "react";
import { Modal, Grid, Button, Header, Divider, Image } from "semantic-ui-react";
import PropTypes from "prop-types";
import glx from "../../assets/glx.png";
import { useSelector } from "react-redux";
import { RootState } from "../../rootReducer";

const CompanyPublishModel = ({ isShowModal, onSubmit }) => {
  const { isSubmitting } = useSelector((state: RootState) => state.assets);

  return (
    <Modal
      onClose={() => onSubmit(false)}
      open={isShowModal}
      size="tiny"
      closeIcon
    >
      <Header as="h1" textAlign="center">
        Company Publishing
      </Header>
      <Modal.Content>
        <Grid textAlign="center">
          <Grid.Column>
            <Image src={glx.src} size="tiny" centered />
          </Grid.Column>
        </Grid>
        <Divider hidden />
        <Modal.Description>
          <Header as="h3" textAlign="center">
            The GLX (Glassboard Exchange), allows your deal to be seen by top
            deal organizers already on the platform, allowing your company to
            get funding sooner.
          </Header>
          <Divider hidden />
          <Header as="h3" textAlign="center">
            As organizer will be able to view your company and reach out to you
            for any due diligence requests.
          </Header>
          <Divider hidden />
          <Header as="h3" textAlign="center">
            From here, organizers create a deal around your company and invite
            investors to this deal.
          </Header>

          <Header as="h5" textAlign="center" disabled>
            Get traction by having a video,pitch deck, team, and financial info
            visible
          </Header>
        </Modal.Description>
        <Divider hidden />
        <Grid>
          <Grid.Row>
            <Grid.Column textAlign="left" width={8}>
              <Button fluid onClick={() => onSubmit(false)}>
                Cancel
              </Button>
            </Grid.Column>
            <Grid.Column textAlign="right" width={8}>
              <Button
                secondary
                fluid
                onClick={() => onSubmit(true)}
                loading={isSubmitting}
              >
                Publish
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

export default CompanyPublishModel;
