import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../rootReducer";
import { fetchFileById } from "../../slices/filesSlice";

import { Table, Icon, Grid, Header, Loader, Button } from "semantic-ui-react";

interface Props {
  fileId: any; //string
  onEditClick: (any) => void;
  onDeleteClick: (any) => void;
}

const SupportingFiles = ({ fileId, onEditClick, onDeleteClick }: Props) => {
  const dispatch = useDispatch();

  const file = useSelector((state: RootState) => state.files.filesById[fileId]);

  useEffect(() => {
    if (fileId) {
      if (!file || file.id !== fileId) {
        dispatch(fetchFileById(fileId));
      }
    }
  }, [file, fileId]);

  return (
    <>
      {!file ? (
        <Table.Row>
          <Table.Cell>
            <Loader active inline="centered" />
          </Table.Cell>
        </Table.Row>
      ) : (
        <Grid>
          <Grid.Row>
            <Grid.Column width={2}>
              <Icon
                color="teal"
                link
                bordered
                inverted
                name="file outline"
                size="small"
              />
            </Grid.Column>
            <Grid.Column width={8}>
              <Header size="small">{file?.name}</Header>
            </Grid.Column>
            <Grid.Column width={2}>
              <Button
                icon="trash"
                color="teal"
                onClick={() => onDeleteClick(file)}
              />
            </Grid.Column>
            <Grid.Column width={2}>
              <Button
                color="teal"
                icon="external alternate"
                onClick={() => onEditClick(file?.id)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      )}
    </>
  );
};

export default SupportingFiles;
