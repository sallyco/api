import React from "react";
import { Modal, Grid, Button, Header, Divider } from "semantic-ui-react";
import PropTypes from "prop-types";

const UnPublishCompanyModel = ({ isShowModal, onSubmit }) => (
  <Modal
    onClose={() => onSubmit(false)}
    open={isShowModal}
    size="tiny"
    closeIcon
  >
    <Header textAlign="center">Unpublish Company?</Header>
    <Modal.Content>
      <Divider hidden />
      <Grid textAlign="center">
        <Grid.Column>
          <Button icon="search" />
        </Grid.Column>
      </Grid>
      <Divider hidden />
      <Modal.Description>
        <Header as="h3" textAlign="center">
          Remove your company from the GLX,making it no longer visible to
          organizers on the platform
        </Header>

        <Divider hidden />

        <Header as="h5" textAlign="center" disabled>
          You may update and publish your company when you see fit.
        </Header>
      </Modal.Description>
      <Divider hidden />
      <Grid>
        <Grid.Row>
          <Grid.Column textAlign="left" width={8}>
            <Button fluid onClick={() => onSubmit(false)}>
              Cancel
            </Button>
          </Grid.Column>
          <Grid.Column textAlign="right" width={8}>
            <Button secondary fluid onClick={() => onSubmit(true)}>
              UnPublish
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Modal.Content>
  </Modal>
);

UnPublishCompanyModel.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func,
};

export default UnPublishCompanyModel;
