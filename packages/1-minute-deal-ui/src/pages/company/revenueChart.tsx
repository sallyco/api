/* eslint-disable import/no-unresolved */
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
} from "recharts";
import { fetchAssetById } from "../../slices/assetsSlice";
import { useParams } from "react-router-dom";
import { RootState } from "../../rootReducer";

export default function ChartPage() {
  const dispatch = useDispatch();
  const { companyId } = useParams<{ companyId: string }>();
  const company = useSelector(
    (state: RootState) => state.assets.assetsById[companyId]
  );
  const revenuePreYear = company?.revenueHistory[0].replace(/\D+/g, "");
  const revenue2YearBack = company?.revenueHistory[1].replace(/\D+/g, "");
  const revenue3YearBack = company?.revenueHistory[2].replace(/\D+/g, "");

  useEffect(() => {
    if (!company || company.id !== companyId) {
      dispatch(fetchAssetById(companyId));
    }
  }, [dispatch, company, companyId]);

  const data = [
    { name: "Year1", Revenue: revenuePreYear },
    { name: "Year2", Revenue: revenue2YearBack },
    { name: "Year3", Revenue: revenue3YearBack },
  ];

  const DataFormater = (Revenue) => {
    return "$" + Revenue;
  };
  return (
    <BarChart width={250} height={150} data={data}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name">
        <Label value="Revenue" offset={0} position="insideBottom" />
      </XAxis>
      <YAxis tickFormatter={DataFormater} />
      <Tooltip />
      <Bar dataKey="Revenue" name="Revenue (Thousands)" fill="grey" />
    </BarChart>
  );
}
