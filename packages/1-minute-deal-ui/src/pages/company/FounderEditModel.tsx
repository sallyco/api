import React from "react";
import { Modal, Header, Divider } from "semantic-ui-react";
import PropTypes from "prop-types";
import { CompanyOverview } from "../../forms/CompanyProfileEdit/CompanyOverview";

const FounderEditModel = ({ isShowModal, company, self, onSubmit }) => (
  <Modal
    onClose={() => onSubmit(false)}
    open={isShowModal}
    size="small"
    closeIcon
  >
    <Header textAlign="center">Company Overview</Header>
    <Modal.Content>
      <Modal.Description>
        <CompanyOverview
          company={company}
          self={self}
          onSubmit={onSubmit}
          submitting={false}
        />
      </Modal.Description>
    </Modal.Content>
  </Modal>
);

FounderEditModel.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  company: PropTypes.object,
  self: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default FounderEditModel;
