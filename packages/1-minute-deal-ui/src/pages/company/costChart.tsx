/* eslint-disable import/no-unresolved */
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
} from "recharts";
import { useParams } from "react-router-dom";
import { fetchAssetById } from "../../slices/assetsSlice";
import { RootState } from "../../rootReducer";

export default function CostChartPage() {
  const dispatch = useDispatch();
  const { companyId } = useParams<{ companyId: string }>();
  const company = useSelector(
    (state: RootState) => state.assets.assetsById[companyId]
  );
  const costPreYear = company?.costHistory[0].replace(/\D+/g, "");
  const costs2YearBack = company?.costHistory[1].replace(/\D+/g, "");
  const costs3YearBack = company?.costHistory[2].replace(/\D+/g, "");

  useEffect(() => {
    if (!company || company.id !== companyId) {
      dispatch(fetchAssetById(companyId));
    }
  }, [dispatch, company, companyId]);

  const data = [
    { name: "Year1", Costs: costPreYear },
    { name: "Year2", Costs: costs2YearBack },
    { name: "Year3", Costs: costs3YearBack },
  ];
  const DataFormater = (Costs) => {
    return "$" + Costs;
  };

  return (
    <BarChart width={250} height={150} data={data}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name">
        <Label value="Costs" offset={0} position="insideBottom" />
      </XAxis>
      <YAxis tickFormatter={DataFormater} />
      <Tooltip />
      <Bar dataKey="Costs" name="Costs (Thousands)" fill="grey" />
    </BarChart>
  );
}
