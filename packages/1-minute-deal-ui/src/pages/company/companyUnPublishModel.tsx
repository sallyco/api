import React from "react";
import { Modal, Grid, Button, Header, Divider, Image } from "semantic-ui-react";
import PropTypes from "prop-types";
import published from "./../../assets/published.png";

interface Props {
  isShowModal: boolean;
  onSubmitUnpublish: (any) => void;
}

const CompanyUnPublishModel = ({ isShowModal, onSubmitUnpublish }: Props) => {
  return (
    <Modal
      onClose={() => onSubmitUnpublish(false)}
      open={isShowModal}
      size="tiny"
      closeIcon
    >
      <Header as="h1" textAlign="center">
        Company Published
      </Header>

      <Modal.Content>
        <Grid textAlign="center">
          <Grid.Column>
            <Image src={published.src} size="tiny" centered />
          </Grid.Column>
        </Grid>
        <Modal.Description>
          <Header as="h3" textAlign="center">
            Your company page is live!
          </Header>
          <Header as="h4" textAlign="center">
            Deal organizers will now be able to review your company and apply to
            be the lead investor.
          </Header>
          <Header as="h5" textAlign="center" disabled>
            You can unpublish your deal any time.
          </Header>
        </Modal.Description>
        <Divider hidden />
        <Button
          secondary
          fluid
          content="VIEW COMPANY"
          onClick={() => onSubmitUnpublish(true)}
        />
      </Modal.Content>
    </Modal>
  );
};

export default CompanyUnPublishModel;
