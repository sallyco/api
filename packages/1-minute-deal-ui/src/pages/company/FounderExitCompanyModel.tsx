import React from "react";
import { Modal, Header, Divider, Button } from "semantic-ui-react";
import PropTypes from "prop-types";

const FounderExitCompanyModel = ({ isShowModal, onSubmit }) => (
  <Modal
    onClose={() => onSubmit(false)}
    open={isShowModal}
    size="tiny"
    closeIcon
  >
    <Modal.Content>
      <p>
        Your company information will be saved and you can finish it at another
        time
      </p>
    </Modal.Content>
    <Modal.Actions>
      <Button
        type="button"
        color="teal"
        inverted
        circular
        onClick={() => onSubmit(false)}
      >
        Cancel
      </Button>
      <Button
        type="button"
        color="teal"
        circular
        onClick={() => onSubmit(true)}
      >
        View Dashboard
      </Button>
    </Modal.Actions>
  </Modal>
);

FounderExitCompanyModel.propTypes = {
  isShowModal: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func,
};

export default FounderExitCompanyModel;
