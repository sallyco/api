import React, { useContext } from "react";
import assetsImage from "../assets/images/assets.png";
import * as Yup from "yup";
import Log from "../tools/Log";

import {
  Button,
  Form,
  Divider,
  Header,
  Container,
  Image,
  List,
  Transition,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { Formik } from "formik";
import { FormField } from "../components/FormField";
import { Link, useLocation, useHistory } from "react-router-dom";

import { TenantContext } from "../contexts";

export const AssetChoicePage = () => {
  const tenantProfile = useContext(TenantContext);
  const query = new URLSearchParams(useLocation().search);
  let dealId = query.get("dealId") ?? undefined;
  let history = useHistory();

  const validation = Yup.object().shape({
    asset: Yup.string().required("Select an asset"),
  });

  async function onSubmit(data) {
    const url = !dealId
      ? `/assets/${data.asset}/create`
      : `/assets/${data.asset}/create?dealId=${dealId}`;
    Log.debug("url", url);
    history.push(url);
  }

  return (
    <>
      <Helmet title={`Select Asset Type - Assets | ${tenantProfile.name}`} />
      <Transition transitionOnMount animation={"fade up"}>
        <Container textAlign="center" className="mobile-container">
          <Divider hidden />
          <Image src={assetsImage.src} size="small" centered />
          <Divider hidden />
          <Header as="h2" inverted={tenantProfile.inverted} icon>
            Choose one of the following asset types:
          </Header>
          <List relaxed>
            <List.Item>
              <Formik
                initialValues={{}}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form onSubmit={props.handleSubmit}>
                    <FormField
                      id="asset"
                      name="asset"
                      component={Form.Select}
                      options={[
                        {
                          key: "companies",
                          value: "companies",
                          text: "Company",
                        },
                        // { key: "artwork", value: "artwork", text: "Artwork" },
                      ]}
                      placeholder="Select an Asset Type"
                    />
                    <Button secondary fluid type="submit" content="Select" />
                  </Form>
                )}
              </Formik>
            </List.Item>
          </List>
        </Container>
      </Transition>
    </>
  );
};
