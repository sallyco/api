import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { Link, useHistory, useParams } from "react-router-dom";
import { TenantContext } from "../contexts";
import * as Yup from "yup";
import {
  Icon,
  Button,
  Input,
  Segment,
  Divider,
  Container,
  Header,
  Card,
  Grid,
  List,
  Transition,
  Form,
  Loader,
  Modal,
  Message,
} from "semantic-ui-react";
import { DateInput } from "semantic-ui-calendar-react";
import NumberFormat from "react-number-format";
import { RootState } from "../rootReducer";

import { fetchDealById } from "../slices/dealsSlice";
import { fetchCloseById } from "../slices/closesSlice";
import { fetchSubscriptionsListByDeal } from "../slices/subscriptionsSlice";

import ShortHeader from "../components/ShortHeader";
import Log from "../tools/Log";
import { Deal } from "../api/dealsApi";
import moment from "moment";
import SubformDate, {
  defaultValues as DateDefaultValues,
  validationSchema as DateValidationSchema,
} from "../forms/subforms/SubformDate";
import { Formik } from "formik";
import { FormField, MaskedFormField } from "../forms/common/FormField";
import SubformCurrency from "../forms/subforms/SubformCurrency";

export const CloseFundPage = () => {
  const dispatch = useDispatch();
  const { dealId, closeId } = useParams<{ dealId: string; closeId: string }>();
  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const { isLoading: dealsLoading, error: dealsError } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);

  const { isLoading: closesLoading, error: closesError } = useSelector(
    (state: RootState) => state.closes
  );

  const close = useSelector(
    (state: RootState) => state.closes.closesById[closeId]
  );

  const subscriptions = useSelector((state: RootState) =>
    Object.values(state.subscriptions.subscriptionsById).filter(
      (subscription) => close.subscriptions.includes(subscription.id)
    )
  );
  useEffect(() => {
    dispatch(fetchSubscriptionsListByDeal(dealId));
  }, []);

  const sendDateLimit = (deal: Deal) => {
    const closeDate = moment.utc(deal.estimatedCloseDate);
    const todayCutoff = moment(14, "HH");
    const now = moment();
    const dateFormat = "YYYY-MM-DD";

    if (closeDate.isAfter(todayCutoff)) {
      return closeDate.format(dateFormat);
    }
    if (now.isAfter(todayCutoff)) {
      return now.add(1, "day").format(dateFormat);
    }
    return now.format(dateFormat);
  };

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    } else {
      setSendDate(sendDateLimit(deal));
    }
  }, [deal, dealId]);

  useEffect(() => {
    if (!close || close.id !== closeId) {
      dispatch(fetchCloseById(closeId));
    }
  }, [close, closeId]);

  //  const { setOpen, setMessage, modal } = ErrorModal();
  const [viewState, setViewState] = useState(0);
  const [sendDate, setSendDate] = useState(null);
  const [investorTotal, setInvestorTotal] = useState(0);

  async function handleClick() {
    history.push(`/deals/${dealId}/funding`);
  }

  return (
    <>
      <Helmet title={`Fund Deal - Deals | ${tenantProfile.name}`} />
      <ShortHeader title={deal?.name} loading={dealsLoading} />
      {dealsLoading || closesLoading ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <>
          <Container className="mobile-container">
            {deal && viewState === 0 && (
              <>
                <Divider hidden />
                <Header as="h3" inverted textAlign="center">
                  Sending funds to the portfolio company
                </Header>
                <Divider hidden />
                <Formik
                  onSubmit={() => {
                    setViewState(1);
                  }}
                  initialValues={{
                    ...DateDefaultValues,
                  }}
                  validationSchema={Yup.object().shape({
                    ...DateValidationSchema({ isRequired: true }),
                  })}
                >
                  {(props) => (
                    <Form onSubmit={props.handleSubmit}>
                      <div style={{ color: "black" }}>
                        <SubformDate
                          id={"date"}
                          label={"When would you like the money wired?"}
                          name={"date"}
                        />
                        {moment(props.values.date).isBefore(
                          sendDateLimit(deal)
                        ) && (
                          <Message color={"red"}>
                            Must be on or after {sendDateLimit(deal)}
                          </Message>
                        )}
                      </div>
                      <Divider hidden />
                      <Button
                        secondary
                        fluid
                        type={"submit"}
                        disabled={moment(props.values.date).isBefore(
                          sendDateLimit(deal)
                        )}
                      >
                        CONTINUE TO FUNDING
                      </Button>
                    </Form>
                  )}
                </Formik>
              </>
            )}
            {viewState === 1 && (
              <>
                <Transition.Group animation={"browse"} duration={300}>
                  <Divider hidden />
                  <Header as="h3" inverted textAlign="center">
                    Confirm portfolio company funding amount
                  </Header>
                  <Formik
                    onSubmit={() => setViewState(2)}
                    initialValues={{
                      amount: subscriptions.reduce(
                        (amt, sub) => amt + sub.amount,
                        0
                      ),
                    }}
                    validationSchema={Yup.object().shape({
                      amount: Yup.number()
                        .transform(function (value, originalvalue) {
                          return Number(originalvalue.replace(/\D+/g, ""));
                        })

                        .max(
                          subscriptions.reduce(
                            (amt, sub) => amt + sub.amount,
                            0
                          ),
                          "Must be less than close amount"
                        )
                        .required(),
                    })}
                  >
                    {(props) => (
                      <Form onSubmit={props.handleSubmit}>
                        <SubformCurrency
                          name={"amount"}
                          id={"amount"}
                          label={"Wire Amount"}
                          placeholder={subscriptions.reduce(
                            (amt, sub) => amt + sub.amount,
                            0
                          )}
                        />
                        <Divider hidden />
                        <Button secondary fluid type={"submit"}>
                          CONFIRM & SEND
                        </Button>
                      </Form>
                    )}
                  </Formik>
                </Transition.Group>
              </>
            )}
            {viewState === 2 && (
              <>
                <Transition.Group animation={"browse"} duration={300}>
                  <Modal open size="small">
                    <Modal.Content>
                      <Header as="h2" textAlign="center" color="grey">
                        Fund Transaction?
                      </Header>
                      <Header as="h2" icon textAlign="center">
                        <Icon color="green" name="dollar" />
                      </Header>
                      <Header as="h4" textAlign="center" color="grey">
                        You are about to wire{" "}
                        <span style={{ color: "green" }}>
                          <NumberFormat
                            value={subscriptions.reduce(
                              (amt, sub) => amt + sub.amount,
                              0
                            )}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"$"}
                          />
                        </span>{" "}
                        to the portfolio company on file,{" "}
                        <span style={{ color: "green" }}>
                          {deal.portfolioCompanyName}
                        </span>{" "}
                        with account ending in{" "}
                        <span style={{ color: "green" }}>XXXXX</span>
                      </Header>
                      <Header as="h4" textAlign="center" color="red">
                        Are you sure you want to send this wire?
                      </Header>
                      <List>
                        <List.Item>
                          <Button
                            as={Link}
                            to={`/deals/${dealId}`}
                            secondary
                            fluid
                          >
                            Send Funds
                          </Button>
                        </List.Item>
                        <List.Item>
                          <Button
                            basic
                            color="teal"
                            fluid
                            onClick={() => setViewState(1)}
                          >
                            Cancel
                          </Button>
                        </List.Item>
                      </List>
                    </Modal.Content>
                  </Modal>
                </Transition.Group>
              </>
            )}
          </Container>
        </>
      )}
    </>
  );
};
