import React, { useContext, useState, useEffect } from "react";
import { TenantContext } from "../contexts";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory, useParams } from "react-router-dom";
import RegisterForm from "../forms/RegisterForm";
import {
  Segment,
  Loader,
  List,
  Container,
  Transition,
} from "semantic-ui-react";

import { RootState } from "../rootReducer";
import { toast } from "react-toastify";
import { ToastError } from "../tools/ToastMessage";
import { fetchInviteById, acceptInviteById } from "../slices/invitesSlice";

import { TermsSidebar } from "../components/terms/TermsSidebar";
import { createUser, getUserEmailExist, verifyEmail } from "../api/usersApi";
import { sendEmail, sendVerifyAccountEmail } from "../api/emailsApi";

import { Helmet } from "react-helmet";

import Log from "../tools/Log";
import { getToken } from "../api/authApi";

export const Register = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  // TODO git rid of all this local state managment
  // create a slice to manage this forms UI state
  const [showTerms, setShowTerms] = useState(false);
  const [termsChecked, setTermsChecked] = useState(false);
  const [submitting, setSubmitting] = useState(false);

  let history = useHistory();
  const { inviteId } = useParams<{ inviteId: string }>();
  Log.debug("inviteId", inviteId);

  //TODO redirect to Login if email exists, prefile email field
  //This should be done in the store not in the url

  const { isLoading, error } = useSelector((state: RootState) => state.invites);

  const invite = useSelector((state: RootState) => {
    return !inviteId ? undefined : state.invites.invitesById[inviteId];
  });
  Log.debug("invite", invite);

  useEffect(() => {
    if (
      !invite &&
      inviteId !== undefined &&
      inviteId !== "founder" &&
      inviteId !== "dealer" &&
      inviteId !== "investor"
    ) {
      dispatch(fetchInviteById(inviteId));
    } else if (invite?.isDeleted) {
      history.push("/dashboard");
      toast.error("We are sorry, this deal is no longer available");
    }
  }, [invite, inviteId]);

  async function onSubmit(data) {
    Log.debug("data", data);
    setSubmitting(true);
    data.email = data.email.toLowerCase();

    if (inviteId === "founder") {
      data.role = "founder";
    } else if (inviteId === "dealer") {
      data.role = "dealer";
    } else if (invite) {
      if (
        invite.invitedTo.length &&
        invite.invitedTo[0].type === "user" &&
        invite.invitedTo[0].relation === "account-admin"
      ) {
        data.role = "account-admin";
        data.accountAdminId = invite.invitedTo[0].id;
      } else if (invite.invitedRole) {
        data.role = invite.invitedRole;
      } else {
        data.role = "investor";
      }
    } else if (!invite && inviteId === "investor") {
      data.role = "investor";
    } else {
      data.role = "investor";
    }

    try {
      const userExist: any = await getUserEmailExist(data.email);
      if (userExist.data.email) {
        setSubmitting(false);
        toast.warn(`Account for "${data.email}" Already Exists.`);
        //history.push("/");
      } else {
        /**
        if (invite && invite.invitedRole)
          data.role = invite.invitedRole;
        else
          data.role = 'organizer';
        **/
        Log.debug("invite", invite);
        Log.debug("formData", data);
        const userId = await createUser(data);
        Log.debug("createUser", true);
        Log.debug("userId", userId);

        if (invite) {
          await dispatch(acceptInviteById(invite.id, userId.id));
          Log.debug("invite", true);
          await verifyEmail({ email: data.email });
          Log.debug("outinvite", true);
          toast.success(`Account created`);
          // Since we already have a validated email and the data, we can log in directly
          await getToken(data.email, data.password);
          setSubmitting(false);
          if (data.role === "signer") {
            history.push("/dashboard/signing");
          } else {
            history.push("/dashboard/subscriptions");
          }
        } else {
          await sendVerifyAccountEmail(data.email, data.firstName);
          Log.debug("sendemail", true);

          toast.success(
            `Account created. An email confirmation message has been sent to ${data.email}`
          );
          setSubmitting(false);
          history.push("/");
        }
      }
    } catch (e) {
      setSubmitting(false);
      ToastError(e.response);
    }
  }

  // TODO need to figure out invites for exiting account
  //function hasAccount() {
  //  if (inviteId) {
  //    history.push(`/?inviteId=${inviteId}`);
  //  } else {
  //    history.push("/");
  //  }
  //}

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "pages-register" }}
        title={`Register | ${tenantProfile.name}`}
      />
      {isLoading ? (
        <Loader active inline="centered" size="massive" />
      ) : (
        <>
          <div style={{ display: showTerms ? "block" : "none" }}>
            <TermsSidebar
              setShowTerms={setShowTerms}
              setTermsChecked={setTermsChecked}
            />
          </div>

          <div style={{ display: showTerms ? "none" : "block" }}>
            <Container text className="margin-bottom-container">
              <RegisterForm
                title={"Create an Account"}
                invite={invite}
                onSubmit={onSubmit}
                setShowTerms={setShowTerms}
                submitting={submitting}
                termsChecked={termsChecked}
                setTermsChecked={setTermsChecked}
              />
              <Segment basic textAlign="center">
                <List>
                  <Link to={{ pathname: "/" }}>Already have an account?</Link>
                </List>
              </Segment>
            </Container>
          </div>
        </>
      )}
      ;
    </>
  );
};
