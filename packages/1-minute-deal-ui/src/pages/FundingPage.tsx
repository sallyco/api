import React, { useCallback, useContext, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useParams, useHistory, useLocation } from "react-router-dom";
import { TenantContext } from "../contexts";

import { RootState } from "../rootReducer";
import {
  fetchSubscriptionById,
  subscriptionACHTransfer,
  updateSubscriptionById,
} from "../slices/subscriptionsSlice";
import { fetchDealById } from "../slices/dealsSlice";

import {
  Divider,
  Header,
  Button,
  Container,
  Form,
  Loader,
  Transition,
  Modal,
} from "semantic-ui-react";

import ShortHeader from "../components/ShortHeader";
import BankingImage from "../assets/images/Banking";
import ProfileBankingSelect from "../forms/banking/ProfileBankingSelect";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import { insertNotification } from "../slices/notificationsSlice";
import { currencyFormat } from "../tools/helpers";
import { fetchSelf } from "../slices/usersSlice";
import { toast } from "react-toastify";
import { getAccountById } from "../api/accountsApi";
import { checkIfVaildPlaidLink, getUpdateLinkToken } from "../api/plaidApi";
import RepairPlaidLink from "../forms/banking/RepairPlaidLink";

export const FundingPage = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const bankingFeatureEnabled = useFeatureFlag("banking");

  const [confirmationModalOpen, setConfirmationModalOpen] = useState(false);
  const [repairPromptModalOpen, setRepairPromptModalOpen] = useState(false);
  const [openPlaidLink, setOpenPlaidLink] = useState(false);
  const [plaidLinkToken, setPlaidLinkToken] = useState<null | {
    linkToken: string;
    action: "INVALID_PUBLIC_TOKEN" | "ITEM_LOGIN_REQUIRED";
  }>(null);
  const query = new URLSearchParams(useLocation().search);
  const previousAmount = query.get("previousAmount") ?? 0;

  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  let history = useHistory();
  const {
    isLoading: subscriptionsLoading,
    isSubmitting: subscriptionsSubmitting,
    error: subscriptionsError,
  } = useSelector((state: RootState) => state.subscriptions);

  const { self } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });

  const [selectedBankAccount, setSelectedBankAccount] = useState(null);

  useEffect(() => {
    // Redux caching doesn't seem to ever have the transactions
    // (For raise amount calculations)
    // So we now always make a network call on the funding page
    // To include them
    dispatch(
      fetchSubscriptionById(subscriptionId, {
        include: ["transactions"],
      })
    );
  }, []);

  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [subscription, deal]);

  const amount = useCallback(() => {
    if (!subscription.transactions) {
      return subscription.amount;
    }
    let amount = subscription.amount;
    for (const transaction of subscription.transactions) {
      amount = amount - transaction.amount;
    }
    return amount;
  }, [subscription]);

  async function onSubmit(data) {
    try {
      // Dont send over the transactions
      const { transactions, ...updateSubscription } = subscription;
      await dispatch(
        updateSubscriptionById({
          ...updateSubscription,
          bankAccount: data,
        })
      );
      if (bankingFeatureEnabled) {
        const ach_transfer_result = await dispatch(
          subscriptionACHTransfer(updateSubscription)
        );
        await dispatch(
          insertNotification({
            tenantId: tenantProfile.id,
            userId: self.id,
            event: {
              type: "subscription",
              id: subscription.id,
            },
            category: "INVESTMENTS",
            message: `You have sent ${currencyFormat(
              subscription.amount
            )} for Deal ${deal.name}`,
            acknowledged: false,
          })
        );
        await dispatch(
          insertNotification({
            tenantId: tenantProfile.id,
            profileId: deal.profileId,
            event: {
              type: "deal",
              id: deal.id,
            },
            category: "DEAL_STATUS",
            message: `An Investor, ${self.firstName} ${
              self.lastName
            }, has ${currencyFormat(subscription.amount)} in transit for Deal ${
              deal.name
            }`,
            acknowledged: false,
          })
        );
        history.push(`/dashboard/subscriptions`);
      }

      history.push(`/dashboard/subscriptions`);
      return;
    } catch (err) {
      toast.error(
        `Transaction not initiated: \n${
          err?.response?.data?.error?.message ?? err.message
        }`
      );
    }
  }

  const checkIfValid = async (bankAccount): Promise<boolean> => {
    try {
      const accountDetail = await getAccountById(bankAccount.plaidAccountId);
      if (accountDetail) {
        const result = await checkIfVaildPlaidLink({
          accessToken: accountDetail.accessToken ?? "",
          subAccountId: bankAccount.plaidSubAccountId,
        });
        if (result?.data?.isError) {
          if (result?.data?.errorCode === "ITEM_LOGIN_REQUIRED") {
            const linkResponse = await getUpdateLinkToken({
              accessToken: accountDetail.accessToken ?? "",
            });
            setPlaidLinkToken({
              linkToken: linkResponse.data,
              action: "ITEM_LOGIN_REQUIRED",
            });
            return false;
          } else {
            setPlaidLinkToken(null);
            return false;
          }
        }
      }
      return true;
    } catch (e) {
      return false;
    }
  };

  const onRepairSuccess = async (data) => {
    setOpenPlaidLink(false);
  };

  return (
    <>
      <Helmet title={`Fund Investment | ${tenantProfile.name}`} />
      <ShortHeader title={deal?.name} loading={dealsLoading} />
      {dealsLoading || subscriptionsLoading ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <Transition>
          <Container textAlign={"center"}>
            <Divider hidden />
            <Header as="h3" inverted textAlign={"center"}>
              Add Banking Details to Fund Investment
              <div>
                <BankingImage width={200} />
              </div>
            </Header>
            <ProfileBankingSelect
              onSelect={async (bankAccount) => {
                setSelectedBankAccount(bankAccount);
                const isValid = await checkIfValid(bankAccount);
                if (isValid) {
                  setConfirmationModalOpen(true);
                } else {
                  setRepairPromptModalOpen(true);
                }
              }}
            />
            {selectedBankAccount && (
              <Modal
                onClose={() => setConfirmationModalOpen(false)}
                onOpen={() => setConfirmationModalOpen(true)}
                open={confirmationModalOpen}
              >
                <Modal.Header>Confirm Funds Transfer</Modal.Header>
                <Modal.Content>
                  <p>
                    {`By clicking the 'Confirm and Send' button below, you hereby authorize us to withdraw 
                        your committed investment amount of $${amount().toFixed(
                          2
                        )} from your ${selectedBankAccount.accountName}`}
                  </p>
                </Modal.Content>
                <Modal.Actions>
                  <Button
                    color="black"
                    onClick={() => setConfirmationModalOpen(false)}
                  >
                    Cancel
                  </Button>
                  <Button
                    content="Confirm and Send Funds"
                    labelPosition="right"
                    loading={subscriptionsSubmitting}
                    icon="checkmark"
                    onClick={async () => {
                      await onSubmit(selectedBankAccount);
                    }}
                    positive
                  />
                </Modal.Actions>
              </Modal>
            )}
            {plaidLinkToken && (
              <Modal
                onClose={() => setRepairPromptModalOpen(false)}
                onOpen={() => setRepairPromptModalOpen(true)}
                open={repairPromptModalOpen}
              >
                <Modal.Header>Repair Plaid Link</Modal.Header>
                <Modal.Content>
                  <p>
                    Plaid credentials not valid for selected account. Please
                    repair the Plaid link to continue.
                  </p>
                </Modal.Content>
                <Modal.Actions>
                  <Button
                    content="Repair Plaid link"
                    labelPosition="right"
                    loading={false}
                    icon="checkmark"
                    onClick={async () => {
                      setRepairPromptModalOpen(false);
                      setOpenPlaidLink(true);
                    }}
                    positive
                  />
                </Modal.Actions>
              </Modal>
            )}
            {openPlaidLink && (
              <RepairPlaidLink
                linkToken={plaidLinkToken.linkToken}
                action={plaidLinkToken.action}
                subscription={subscription}
                selectedBankAccount={selectedBankAccount}
                onRepairSuccess={onRepairSuccess}
              />
            )}
          </Container>
        </Transition>
      )}
    </>
  );
};
