import { Formik } from "formik";
import numeral from "numeral";
import React, { useEffect, useState } from "react";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Table,
} from "semantic-ui-react";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { FormField, MaskedFormField } from "../forms/common/FormField";
import { PopupModal } from "../components/common/PopupModal";
import { BankingBeneficiary, getBankingBeneficiaries } from "../api/bankingApi";
import { RootState } from "../rootReducer";
import { searchDeal } from "../slices/dealsSlice";
import {
  getEntityBankAccountDetails,
  postWireTransfer,
} from "../api/entitiesApi";
import BankingBeneficiaryTab from "./BankingBeneficiaryTab";

interface BankingWireTransferProps {
  inverted?: boolean;
  maxAmount?: number;
  entityId?: string;
  closeId?: string | undefined;
  amount?: number;
}

export default function BankingWireTransfersTab({
  inverted = false,
  maxAmount = null,
  entityId = "",
  closeId = undefined,
  amount = 0,
}: BankingWireTransferProps) {
  const history = useHistory();
  const dispatch = useDispatch();
  const [showTransferPopup, setShowTransferPopup] = useState(false);
  const [processing, setProcessing] = useState(false);
  const { currentPageDeals, dealsById } = useSelector(
    (state: RootState) => state.deals
  );
  const deals = currentPageDeals.map((dealId) => dealsById[dealId]);
  const [transferData, setTransferData] = useState({
    beneficiaryId: "",
    beneficiaryName: "",
    amount: 0,
    dealName: "",
    entityId: "",
    closeId: "",
    memo: "",
    scheduledForDate: "",
  });
  const [accountInformation, setAccountInformation] = useState<{
    currentBalance: number;
    availableBalance: number;
  } | null>();

  const [beneficiaries, setBeneficiaries] = useState<BankingBeneficiary[]>([]);
  const [showBeneficiaryForm, setShowBeneficiaryForm] = useState(false);

  useEffect(() => {
    getBankingBeneficiaries()
      .then((response) => {
        setBeneficiaries(response.data);
      })
      .catch(() => {
        toast.error("Could not retrieve Beneficiaries");
      });
  }, []);

  useEffect(() => {
    dispatch(
      searchDeal({
        page: 0,
        perPage: 200,
        search: [],
        sort: [
          {
            direction: "ASC",
            field: "name",
          },
        ],
        includes: [
          {
            relation: "entity",
            scope: {
              fields: ["name", "id", "dealId", "bankAccount"],
            },
          },
        ],
      })
    );
  }, []);

  let yupShape = {
    beneficiaryId: Yup.string().trim().required(),
    entityId: Yup.string().required(),
    amount: Yup.number()
      .transform(function (value, originalvalue) {
        if (typeof originalvalue === "number") {
          return originalvalue;
        }

        return Number(originalvalue.replace(/\D+/g, ""));
      })
      .required(),
    memo: Yup.string().required().max(140),
  };

  if (maxAmount > 0) {
    yupShape.amount = yupShape.amount.max(maxAmount);
  }

  const validation = Yup.object().shape(yupShape);

  async function onSubmit(data) {
    const dealName = deals.find((deal) => deal.entityId === data.entityId).name;
    const beneficiaryName = beneficiaries.find(
      (ben) => ben.id === data.beneficiaryId
    ).name;

    let amount: number = 0;
    if (typeof data.amount === "string") {
      amount = Number.parseInt(data.amount.replace(/\D/g, ""), 10);
    } else if (typeof data.amount === "number") {
      amount = data.amount;
    }

    const transferData = {
      beneficiaryId: data.beneficiaryId,
      beneficiaryName,
      amount,
      dealName,
      entityId: data.entityId,
      closeId: closeId,
      memo: data.memo,
      scheduledForDate: null,
    };

    setTransferData(transferData);
    setShowTransferPopup(true);
  }

  function fetchAccountInfo(selectedEntityId) {
    getEntityBankAccountDetails(selectedEntityId).then((accountInfo) => {
      setAccountInformation({
        currentBalance: accountInfo.currentBalance,
        availableBalance: accountInfo.availableBalance,
      });
    });
  }

  useEffect(() => {
    if (entityId) {
      fetchAccountInfo(entityId);
    }
  }, [entityId]);

  return (
    <>
      <Container className="margin-bottom-container text">
        <Header
          inverted={inverted}
          as="h2"
          content="Outbound Wire Transfer Form"
        />
        <Divider hidden />
        <Formik
          initialValues={{
            accountName: "",
            routingNumber: "",
            accountNumber: "",
            verifyAccountNumber: "",
            amount: amount ?? 0,
            entityId,
          }}
          onSubmit={onSubmit}
          validationSchema={validation}
          enableReinitialize
        >
          {(props) => (
            <>
              <Form
                inverted={inverted}
                className={inverted ? "" : "dark-labels"}
                onSubmit={props.handleSubmit}
              >
                <FormField
                  id="beneficiaryId"
                  name="beneficiaryId"
                  label="Send To Beneficiary"
                  component={Form.Select}
                  options={beneficiaries.map((beneficiary) => ({
                    value: beneficiary.id,
                    text: beneficiary.name,
                  }))}
                  placeholder="Select a Beneficiary"
                  required
                />
                <Button
                  fluid
                  type="button"
                  onClick={() => {
                    setShowBeneficiaryForm(true);
                  }}
                >
                  + Add Beneficiary
                </Button>

                <Divider hidden />
                <FormField
                  id="entityId"
                  name="entityId"
                  label="From Deal Account"
                  component={Form.Select}
                  options={deals.map((deal) => ({
                    value: deal.entityId,
                    text: `${deal.name}: ${
                      deal?.entity?.name && deal?.entity?.bankAccount
                        ? deal.entity.name
                        : "Pending banking information"
                    }`,
                    disabled: deal?.entity?.bankAccount ? false : true,
                  }))}
                  placeholder="Select a Deal Account"
                  required
                  customOnChange={(e, selectedEntityId) => {
                    setAccountInformation(null);
                    // Get the bank account information for the selected entity
                    fetchAccountInfo(selectedEntityId);
                  }}
                />
                {accountInformation && (
                  <Table
                    inverted={inverted}
                    basic="very"
                    celled
                    compact
                    unstackable
                  >
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell width={4}>Account Balance</Table.Cell>
                        <Table.Cell width={8}>
                          {numeral(accountInformation.currentBalance).format(
                            "$0,0"
                          )}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell width={4}>Available Balance</Table.Cell>
                        <Table.Cell width={8}>
                          {numeral(accountInformation.availableBalance).format(
                            "$0,0"
                          )}
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                )}
                <Divider hidden />
                <MaskedFormField
                  id="wireamount"
                  name={"amount"}
                  label="Amount"
                  required
                  thousandSeparator={true}
                  allowNegative={false}
                  prefix={"$"}
                  decimalScale={0}
                  type="tel"
                  placeholder="$500,000"
                />
                <FormField
                  name={"memo"}
                  id="memo"
                  placeholder={"Instructions"}
                  label={"Additional Instructions"}
                  required
                />
                <Divider horizontal />
                <Button
                  inverted={inverted}
                  disabled={!props.dirty || !props.isValid}
                  type="submit"
                  primary
                  fluid
                  content="Initiate Wire Transfer"
                />
              </Form>
              <PopupModal
                open={showBeneficiaryForm}
                size="large"
                heading="+ Add a Beneficiary"
                content={
                  <BankingBeneficiaryTab
                    onSuccessHandler={(newBeneficiary) => {
                      setBeneficiaries([...beneficiaries, newBeneficiary]);
                      props.setFieldValue("beneficiaryId", newBeneficiary.id);
                      setShowBeneficiaryForm(false);
                    }}
                  />
                }
                closecaption="Cancel"
                onClickClose={() => setShowBeneficiaryForm(false)}
              />
            </>
          )}
        </Formik>
      </Container>
      <PopupModal
        open={showTransferPopup}
        size="tiny"
        heading="Initiate Wire Transfer"
        content={
          <p>
            You are requesting a wire transfer of{" "}
            {numeral(transferData.amount).format("$0,0.00")} from{" "}
            {transferData.dealName} to {transferData.beneficiaryName}. Continue?
          </p>
        }
        okcaption="Confirm"
        closecaption="Cancel"
        processing={processing}
        onClickOk={async () => {
          setProcessing(true);

          const data = {
            amount: transferData.amount,
            beneficiaryId: transferData.beneficiaryId,
            instructions: transferData.memo,
            closeId: transferData.closeId,
          };

          if (transferData.scheduledForDate !== null) {
            data["scheduledForDate"] = transferData.scheduledForDate;
          }

          try {
            await postWireTransfer(transferData.entityId, data);
            setShowTransferPopup(false);
            setProcessing(false);
            toast.success("Wire transfer requested successfully");
            history.push(`/dashboard`);
          } catch (err) {
            const e: any = err;
            toast.error(
              e.data?.error?.message ??
                "The wire transfer request was unable to process: " + err,
              {
                autoClose: false,
              }
            );
            setProcessing(false);
          }
        }}
        onClickClose={() => setShowTransferPopup(false)}
      />
    </>
  );
}
