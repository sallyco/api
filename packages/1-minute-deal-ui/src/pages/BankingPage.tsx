import { Close, getCloseById } from "../api/closesApi";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory, useParams } from "react-router";
import { Segment, Container, Tab } from "semantic-ui-react";
import BankingWireTransfersTab from "../pages/BankingWireTransfersTab";
import BankingACHTransfersTab from "../pages/BankingACHTransfersTab";

import { useFeesStatementSWR } from "../swrConnector/deals";
import { FormControl, Select, MenuItem } from "@mui/material";
import BeneficiaryListTable from "../components/banking/BeneficiaryListTable";
import LinkedAccountsTable from "../components/banking/LinkedAccountsTable";
import { getLinkToken } from "../api/plaidApi";
import { useAccountRole } from "../contexts";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import TransactionLogTable from "../components/banking/TransactionLogTable";

export default function BankingPage() {
  const [toggle, setToggle] = useState("wire");
  const [plaidLinkToken, setPlaidLinkToken] = useState<null | string>(null);
  const { closeId } = useParams<{ closeId: string }>();
  const [close, setClose] = useState<Close>();
  const feesStatement: any = useFeesStatementSWR(close?.dealId);
  const isOrganizer = useAccountRole("organizer");

  const handleChange = (event) => {
    setToggle(event.target.value);
  };
  // Determine preselected values, based on deal that is closing
  useEffect(() => {
    if (closeId) {
      getCloseById(closeId).then((closeResponse) => {
        setClose(closeResponse);
      });
    }
  }, []);

  useEffect(() => {
    const linkToken = async () => {
      const linkResponse = await getLinkToken();
      setPlaidLinkToken(linkResponse.data);
    };
    if (!plaidLinkToken) {
      linkToken();
    }
  }, []);

  // get closing values and lock them
  const entityId = close?.entityId;
  const amount = feesStatement?.proceedsToSend ?? 0;

  let panes = [
    ...(isOrganizer
      ? [
          {
            menuItem: "TRANSFER",
            // eslint-disable-next-line react/display-name
            render: () => (
              <Tab.Pane inverted attached={false}>
                <Segment inverted>
                  <Container className="container text">
                    <FormControl
                      fullWidth
                      variant="outlined"
                      style={{ paddingBottom: "25px" }}
                    >
                      <Select value={toggle} onChange={handleChange}>
                        <MenuItem value={"wire"}>Wire Transfer</MenuItem>
                        <MenuItem value={"ach"}>ACH Transfer</MenuItem>
                      </Select>
                    </FormControl>
                  </Container>
                  {toggle === "ach" && (
                    <BankingACHTransfersTab
                      entityId={entityId}
                      closeId={closeId}
                      amount={amount}
                    />
                  )}
                  {toggle === "wire" && (
                    <BankingWireTransfersTab
                      entityId={entityId}
                      closeId={closeId}
                      amount={amount}
                    />
                  )}
                </Segment>
              </Tab.Pane>
            ),
          },
        ]
      : []),
    ...(isOrganizer
      ? [
          {
            menuItem: "BENEFICIARIES",
            // eslint-disable-next-line react/display-name
            render: () => (
              <Tab.Pane inverted attached={false}>
                <Segment inverted>
                  <Container>
                    <BeneficiaryListTable />
                  </Container>
                </Segment>
              </Tab.Pane>
            ),
          },
        ]
      : []),
    ...(isOrganizer
      ? [
          {
            menuItem: "TRANSACTION LOGS",
            // eslint-disable-next-line react/display-name
            render: () => (
              <Tab.Pane inverted attached={false}>
                <Segment inverted>
                  <Container>
                    <TransactionLogTable />
                  </Container>
                </Segment>
              </Tab.Pane>
            ),
          },
        ]
      : []),
  ];
  const linked_accounts = useFeatureFlag("linked_accounts");
  if (linked_accounts) {
    panes.push({
      menuItem: "LINKED ACCOUNTS",
      // eslint-disable-next-line react/display-name
      render: () => (
        <>
          {plaidLinkToken && (
            <Tab.Pane inverted attached={false}>
              <Segment inverted>
                <Container>
                  <LinkedAccountsTable plaidLinkToken={plaidLinkToken} />
                </Container>
              </Segment>
            </Tab.Pane>
          )}
        </>
      ),
    });
  }

  return (
    <>
      <Helmet title={`Banking`} />

      <Tab
        menu={{ secondary: true, pointing: true, className: "nav-bar" }}
        panes={panes}
      />
    </>
  );
}
