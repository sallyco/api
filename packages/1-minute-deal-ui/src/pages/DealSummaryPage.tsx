import React, {
  useState,
  useContext,
  useEffect,
  useCallback,
  useMemo,
} from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useParams, useHistory, Link } from "react-router-dom";
import DealDocumentsCard from "../components/deals/DealDocumentsCard";
import CompanyDocumentsCard from "../components/deals/CompanyDocumentsCard";
import { RootState } from "../rootReducer";
import { fetchSubscriptionById } from "../slices/subscriptionsSlice";
import { fetchEntityById } from "../slices/entitySlice";
import { fetchAssetById } from "../slices/assetsSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchProfileById } from "../slices/profilesSlice";
import { insertNotification } from "../slices/notificationsSlice";
import { getProfileName } from "../api/profileApiWrapper";
import { toast } from "react-toastify";
import {
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Button,
  Container,
  Transition,
  Loader,
  Card,
  Segment,
} from "semantic-ui-react";
import numeral from "numeral";

import Document from "../components/Document";
import ShortHeader from "../components/ShortHeader";
import NumberFormat from "react-number-format";
import moment from "moment";
import { TenantContext } from "../contexts";
import SendMessageModal from "../components/SendMessageModal";
import { Box, Typography } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import EntityBankingInformation from "../components/banks/EntityBankingInformation";
import CarryRecipientsCard from "../components/deals/CarryRecipientsCard";
import ManagementFeeCard from "../components/deals/ManagementFeeCard";
import ExpenseReserveCard from "../components/deals/ExpenseReserveCard";
import DealDetailsTable from "../components/deals/DealDetailsTable";
import SubscriptionButtons from "../components/subscriptions/SubscriptionButtons";
import SignersDetails from "../components/deals/SignersDetails";
import { API, useRequest } from "../api/swrApi";

import {
  FeatureFlag,
  useFeatureFlag,
} from "../components/featureflags/FeatureFlags";
import { fetchClosesListByDeal } from "../slices/closesSlice";
import { sendContactOrganizerEmail } from "../api/emailsApi";
import { getUserById } from "../api/usersApi";
import { Subscription } from "../api/subscriptionsApi";
import InvestorUploadsCard from "../components/deals/InvestorUploads";
import CapitalAccountStatementCard from "../components/deals/CapitalAccountStatementCard";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      "& img": {
        maxWidth: "100%",
      },
    },
  };
});

export const DealSummaryPage = () => {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();

  const classes = useStyles();

  let history = useHistory();
  const tenantProfile = useContext(TenantContext);
  const expenseReserveEnabled = useFeatureFlag("expense_reserve");
  const showInvestorOwnership = useFeatureFlag("investor_ownership");

  const [notificationSent, setNotificationSent] = useState(false);
  const [showMessageModal, setShowMessageModal] = useState(false);
  // const subscription = useSelector(
  //   (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  // );

  const { data: self } = useRequest(API.SELF);
  const { data: subscription } = useRequest<Subscription>(
    subscriptionId ? API.SUBSCRIPTION_BY_ID(subscriptionId) : null
  );
  const { data: deal } = useRequest(
    subscription?.dealId ? `/deals/` + subscription.dealId : null
  );
  const { data: entity } = useRequest(
    deal?.entityId ? API.ENTITY_BY_ID(deal.entityId) : null
  );

  const isPrimarySignatory = useCallback(() => {
    return subscription && self && subscription.ownerId === self.id;
  }, [subscription, self]);

  useEffect(() => {
    if (subscription) {
      dispatch(fetchSubscriptionById(subscription?.id));
    }
  }, [subscription]);

  // const { isLoading: subscriptionsLoading } = useSelector(
  //   (state: RootState) => state.subscriptions
  // );
  //
  // const { isLoading: entitiesLoading, error: entitiesListError } = useSelector(
  //   (state: RootState) => state.entities
  // );
  //
  // const { isLoading: dealsLoading, error: dealsListError } = useSelector(
  //   (state: RootState) => state.deals
  // );

  // const deal = useSelector((state: RootState) => {
  //   return !subscription
  //     ? undefined
  //     : state.deals.dealsById[subscription.dealId];
  // });

  const [closes, setCloses] = useState([]);

  const { closesById } = useSelector((state: RootState) => state.closes);
  useEffect(() => {
    if (deal) {
      setCloses(
        Object.values(closesById).filter((close) => close.dealId === deal.id)
      );
    }
  }, [closesById]);
  useEffect(() => {
    if (deal) {
      dispatch(fetchClosesListByDeal(deal.id));
    }
  }, [deal, dispatch]);

  // const entity = useSelector((state: RootState) => {
  //   return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  // });

  // ASSET
  const { isLoading: assetsLoading, error: assetsError } = useSelector(
    (state: RootState) => state.assets
  );
  const assets = useSelector((state: RootState) => {
    return !deal
      ? undefined
      : Object.values(state.assets.assetsById).filter(
          (asset) => asset.dealId === deal.id
        );
  });
  useEffect(() => {
    console.log(deal, entity, subscription, assetsLoading);
    if (deal && deal.assetIds && !assetsLoading && !assetsError) {
      deal.assetIds.forEach((assetId) => {
        if (assets.findIndex((asset) => asset.id === assetId) !== -1) {
          dispatch(fetchAssetById(assetId));
        }
      });
    }
  }, [dispatch, deal]);

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
    if (
      subscription &&
      self &&
      deal &&
      !notificationSent &&
      subscription.status === "INVITED"
    ) {
      dispatch(
        insertNotification({
          tenantId: tenantProfile.id,
          profileId: deal.profileId,
          event: {
            type: "deal",
            id: deal.id,
          },
          category: "DEAL_STATUS",
          message: `An Investor, ${self.firstName} ${self.lastName}, has opened their invitation email for deal ${deal.name}`,
          acknowledged: false,
        })
      );
      setNotificationSent(true);
    }
    if (subscription?.isDeleted) {
      history.push("/dashboard");
      toast.error("This Link is No Longer Valid");
    }
  }, [
    dispatch,
    subscription,
    subscriptionId,
    self,
    deal,
    history,
    notificationSent,
    tenantProfile,
  ]);

  useEffect(() => {
    if (entity && subscription) {
      if (
        entity.regulationType === "REGULATION_D_S" &&
        !subscription.profileId
      ) {
        history.push(
          `/profiles/create/subscriber?subscriptionId=${subscriptionId}`
        );
      }
    }
  }, [history, entity, subscription, subscriptionId]);

  // PROFILE
  //  const { isLoading: profilesLoading, error: profilessListError } = useSelector(
  //    (state: RootState) => state.profiles
  //  );

  const profilesListError = useSelector((state: RootState) => {
    return state.profiles.error;
  });

  const profile = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.profiles.profilesById[subscription.profileId];
  });
  useEffect(() => {
    if (
      subscription &&
      subscription.profileId &&
      !profile &&
      !profilesListError
    ) {
      dispatch(fetchProfileById(subscription.profileId));
    }
  }, [dispatch, subscription, profile, profilesListError]);

  // ORGANIZER PROFILE
  const organizer = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });
  useEffect(() => {
    if (deal && deal.profileId && !organizer && !profilesListError) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [dispatch, deal, organizer, profilesListError]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const shouldShowBanking = useCallback(() => {
    return subscription && entity && entity.bankAccount;
  }, [subscription, entity]);

  let iconRenderSwitch = (status) => {
    switch (status) {
      case "PENDING":
        return "file";
      case "COMMITTED":
        return "money";
      case "FUNDS_IN_TRANSIT-MANUAL":
        return "money";
      case "COMPLETED":
        return "check circle";
      default:
        return "check";
    }
  };

  let investmentRowSpanSwitch = (status) => {
    switch (status) {
      case "PENDING":
        return (
          <span>
            Click on Sign Agreement below to complete your investment for{" "}
            <NumberFormat
              value={subscription.amount}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />
          </span>
        );
      case "COMMITTED":
        return (
          <span>
            You&#39;re committed to this deal for{" "}
            <NumberFormat
              value={subscription.amount}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />
            . Click below to fund your investment
          </span>
        );
      case "FUNDS_IN_TRANSIT-MANUAL":
        return (
          <span>
            You have{" "}
            <NumberFormat
              value={subscription.amount}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />{" "}
            currently in transit for this deal
          </span>
        );
      case "COMPLETED":
        return (
          <span>
            You&#39;re invested in this deal for{" "}
            <NumberFormat
              value={subscription.amount}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
            />
          </span>
        );
      default:
        return "";
    }
  };

  const sendEmailToOrganizer = async (profile, messageContent) => {
    const user = await getUserById(deal["ownerId"]);

    await sendContactOrganizerEmail(
      user,
      {
        name: getProfileName(profile),
        email: profile.email,
        phone: profile.phone,
      },
      deal,
      messageContent
    );
  };

  const isLoading = useCallback(() => {
    return !!(!deal || !entity || !subscription || assetsLoading);
  }, [deal, entity, subscription, assetsLoading]);

  return (
    <>
      <Helmet
        bodyAttributes={{ class: "grey-background" }}
        title={`Deal Summary | ${tenantProfile.name}`}
      />
      <ShortHeader
        title={`${deal?.name ? deal.name + ` - ` : ""} Deal Overview`}
        hideBackArrow
      />
      {isLoading() ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <>
          <Grid celled="internally" className="ui secondary-dashboard-header">
            {!deal?.assetIds && (
              <>
                <Grid.Row columns={2}>
                  <Grid.Column textAlign="center">
                    <Header as="h5" inverted className="no-spacing">
                      Target Raise
                    </Header>
                    <Header as="h2" color="green" className="no-spacing">
                      <NumberFormat
                        value={deal?.targetRaiseAmount ?? 0}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Header>
                  </Grid.Column>
                  <Grid.Column textAlign="center">
                    <Header as="h5" inverted className="no-spacing">
                      Close Date
                    </Header>
                    <Header as="h2" inverted className="no-spacing">
                      {moment.utc(deal?.estimatedCloseDate).format("L")}
                    </Header>
                  </Grid.Column>
                </Grid.Row>
              </>
            )}
          </Grid>
          <Transition animation={"fade"}>
            <Container>
              <Box display={"flex"} flexDirection={"column"}>
                <Divider hidden />
                {/** THIS IS NOT COMPLETE - Disabled by Taylor **/}
                {/* eslint-disable-next-line no-constant-condition */}
                {/*{assets && assets.length > 0 && false ? (*/}
                {/*  assets.map((asset) => (*/}
                {/*    <>*/}
                {/*      <Grid.Row columns={2}>*/}
                {/*        <Grid.Column width={10}>*/}
                {/*          {asset && asset.images && (*/}
                {/*            <ImageFileGallery fileIds={asset.images} />*/}
                {/*          )}*/}
                {/*        </Grid.Column>*/}
                {/*        <Grid.Column width={6}>*/}
                {/*          <Header as="h2">*/}
                {/*            {asset?.name}*/}
                {/*            <Header.Subheader content={asset?.artist?.name} />*/}
                {/*          </Header>*/}
                {/*          <Divider hidden />*/}
                {/*          <Header as="h3">*/}
                {/*            <NumberFormat*/}
                {/*              value={asset?.fractionalOwnershipAmount}*/}
                {/*              displayType={"text"}*/}
                {/*              thousandSeparator={true}*/}
                {/*              prefix={"$"}*/}
                {/*            />*/}
                {/*            <Header.Subheader>*/}
                {/*              Fractional Ownership*/}
                {/*            </Header.Subheader>*/}
                {/*          </Header>*/}
                {/*          <SubscriptionButtons*/}
                {/*            subscriptionId={subscriptionId}*/}
                {/*          />*/}
                {/*        </Grid.Column>*/}
                {/*      </Grid.Row>*/}
                {/*      <Grid.Row columns={1}>*/}
                {/*        <Grid.Column>*/}
                {/*          <Header as="h2" content="About" />*/}
                {/*          {asset?.description}*/}
                {/*        </Grid.Column>*/}
                {/*      </Grid.Row>*/}
                {/*      <Grid.Row columns={1}>*/}
                {/*        <Grid.Column>*/}
                {/*          <Header as="h2" content="Details" />*/}
                {/*        </Grid.Column>*/}
                {/*      </Grid.Row>*/}
                {/*      <Grid.Row columns={2}>*/}
                {/*        <Grid.Column>*/}
                {/*          <Header*/}
                {/*            as="h4"*/}
                {/*            content="Artist Name"*/}
                {/*            subheader={asset?.artist?.name}*/}
                {/*            dividing*/}
                {/*          />*/}
                {/*          <Header*/}
                {/*            as="h4"*/}
                {/*            content="Artist Location"*/}
                {/*            subheader={asset?.artist?.location}*/}
                {/*            dividing*/}
                {/*          />*/}
                {/*          <Header as="h4" dividing>*/}
                {/*            Appraised Value*/}
                {/*            <Header.Subheader>*/}
                {/*              <NumberFormat*/}
                {/*                value={asset?.appraisedValue}*/}
                {/*                displayType={"text"}*/}
                {/*                thousandSeparator={true}*/}
                {/*                prefix={"$"}*/}
                {/*              />*/}
                {/*            </Header.Subheader>*/}
                {/*          </Header>*/}
                {/*        </Grid.Column>*/}
                {/*        <Grid.Column>*/}
                {/*          <Header*/}
                {/*            as="h4"*/}
                {/*            content="Size"*/}
                {/*            subheader={asset?.size}*/}
                {/*            dividing*/}
                {/*          />*/}
                {/*          <Header*/}
                {/*            as="h4"*/}
                {/*            content="Category"*/}
                {/*            subheader={asset?.category}*/}
                {/*            dividing*/}
                {/*          />*/}
                {/*          <Header as="h4" dividing>*/}
                {/*            Fractional Ownership Amount*/}
                {/*            <Header.Subheader>*/}
                {/*              <NumberFormat*/}
                {/*                value={asset?.fractionalOwnershipAmount}*/}
                {/*                displayType={"text"}*/}
                {/*                thousandSeparator={true}*/}
                {/*                prefix={"$"}*/}
                {/*              />*/}
                {/*            </Header.Subheader>*/}
                {/*          </Header>*/}
                {/*        </Grid.Column>*/}
                {/*      </Grid.Row>*/}
                {/*    </>*/}
                {/*  ))*/}
                {/*) : (*/}
                {/*  <>*/}
                <Card fluid>
                  <Segment>
                    <Grid>
                      <Grid.Row>
                        <Grid.Column align={"right"}>
                          <Button
                            onClick={() => {
                              setShowMessageModal(true);
                            }}
                          >
                            Contact Organizer
                          </Button>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row columns={1}>
                        <Grid.Column textAlign={"center"}>
                          <Typography
                            variant={deal?.marketing?.logo ? "h4" : "h5"}
                            style={{ fontWeight: "bold" }}
                            align="center"
                          >
                            {deal?.name}
                          </Typography>
                          <Typography variant={"h6"} color={"textSecondary"}>
                            {deal?.marketing?.tagline}
                          </Typography>
                        </Grid.Column>
                      </Grid.Row>
                      {subscription && subscription.amount && (
                        <Grid.Row color="green" columns={1}>
                          <Grid.Column textAlign="center">
                            <Header as="h3" icon inverted textAlign="center">
                              <Icon
                                name={iconRenderSwitch(subscription.status)}
                                size="tiny"
                              />
                              {investmentRowSpanSwitch(subscription.status)}
                              {showInvestorOwnership &&
                                subscription.status === "COMPLETED" &&
                                subscription.amount && (
                                  <Header.Subheader inverted textAlign="center">
                                    Estimated Ownership:{" "}
                                    {numeral(
                                      subscription.amount /
                                        deal.targetRaiseAmount
                                    ).format("0.0000%")}
                                  </Header.Subheader>
                                )}
                              {showInvestorOwnership &&
                                subscription.status === "CLOSED" &&
                                subscription.ownershipPercentageAtClose && (
                                  <Header.Subheader inverted textAlign="center">
                                    Ownership:{" "}
                                    {numeral(
                                      subscription.ownershipPercentageAtClose
                                    ).format("0.0000%")}
                                  </Header.Subheader>
                                )}
                            </Header>

                            <Header as="h5" inverted textAlign="center">
                              Investment profile used
                              <Header.Subheader>
                                {getProfileName(profile)} - invested as{" "}
                                {profile?.taxDetails.registrationType.toLowerCase()}
                              </Header.Subheader>
                            </Header>
                            {subscription.status !== "PENDING" && (
                              <Header as="h5" inverted textAlign="center">
                                You will be notified if there are investor
                                updates
                              </Header>
                            )}
                          </Grid.Column>
                        </Grid.Row>
                      )}
                      <Grid.Row centered>
                        <Grid.Column width={6}>
                          {subscriptionId && isPrimarySignatory() && (
                            <SubscriptionButtons
                              subscriptionId={subscriptionId}
                            />
                          )}
                        </Grid.Column>
                      </Grid.Row>
                      <FeatureFlag name={"multi_signer"}>
                        {subscription?.signers &&
                          subscription?.status !== "CANCELLED" && (
                            <Grid.Row columns={1} centered>
                              <Grid.Column width={10}>
                                <SignersDetails
                                  subscription={subscription}
                                  profile={profile}
                                />
                              </Grid.Column>
                            </Grid.Row>
                          )}
                      </FeatureFlag>
                    </Grid>
                  </Segment>
                </Card>
                {subscription?.status === "CLOSED" && (
                  <>
                    <CapitalAccountStatementCard
                      subscriptionId={subscription.id}
                    />
                  </>
                )}
                <Card fluid>
                  <Grid stackable padded centered>
                    <Grid.Row>
                      <Grid.Column floated="left" width={8}>
                        <Header
                          as="h2"
                          color="grey"
                          className="no-spacing"
                          style={{ textAlign: "left" }}
                        >
                          Deal Details
                        </Header>
                      </Grid.Column>
                      {deal.ownersDealUrl && (
                        <Grid.Column
                          floated="right"
                          width={8}
                          style={{ textAlign: "right" }}
                        >
                          <a
                            href={deal.ownersDealUrl}
                            style={{ display: "inline-block" }}
                          >
                            <Button>
                              Return to deal on {tenantProfile.name}
                            </Button>
                          </a>
                        </Grid.Column>
                      )}
                    </Grid.Row>
                    <Grid.Row columns={1}>
                      <Grid.Column>
                        <DealDetailsTable
                          deal={{ ...deal, ...{ closes } }}
                          entity={entity}
                        />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                      <Grid.Column>
                        {shouldShowBanking() && (
                          <>
                            <EntityBankingInformation
                              entity={entity}
                              investorProfile={profile}
                            />
                          </>
                        )}
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Card>
                <Card fluid>
                  <Grid celled={"internally"} divided={false}>
                    {deal && deal.description && (
                      <Grid.Row columns={1}>
                        <Grid.Column
                          style={{
                            textOverflow: "ellipsis",
                            overflow: "hidden",
                          }}
                        >
                          <div
                            className={classes.root}
                            dangerouslySetInnerHTML={{
                              __html: deal?.description,
                            }}
                          />
                        </Grid.Column>
                      </Grid.Row>
                    )}
                  </Grid>
                </Card>
                {deal?.id && (
                  <Container fluid>
                    <Card.Group
                      itemsPerRow={expenseReserveEnabled ? 3 : 2}
                      stackable
                    >
                      <CarryRecipientsCard dealId={deal.id} editable={false} />
                      <ManagementFeeCard dealId={deal.id} editable={false} />
                      <FeatureFlag name={"expense_reserve"}>
                        <ExpenseReserveCard dealId={deal.id} editable={false} />
                      </FeatureFlag>
                    </Card.Group>
                  </Container>
                )}
                {subscription?.id && (
                  <>
                    <InvestorUploadsCard
                      subscriptionId={subscription.id}
                      canUpload
                    />
                  </>
                )}
                {deal?.status !== "DRAFT" &&
                  !["CLOSED", "CANCELLED"].includes(subscription?.status) && (
                    <Grid.Row>
                      <Grid.Column stretched width={16}>
                        {deal?.files && deal?.files.length > 0 && (
                          <>
                            <DealDocumentsCard
                              dealId={deal.id}
                              title="Investor Documents"
                            />
                          </>
                        )}
                        {deal?.id && <CompanyDocumentsCard dealId={deal.id} />}
                        <Divider hidden />
                        <Header as="h2" dividing>
                          Fund Documents
                        </Header>
                        {!entity ? (
                          <Loader active inline="centered" />
                        ) : (
                          <Card.Group textAlign="center" centered>
                            <Document
                              fileId={
                                entity?.entityDocuments?.operatingAgreement
                              }
                              loadingTitle={"Operating Agreement"}
                            />
                            <Document
                              fileId={
                                entity?.entityDocuments
                                  ?.privatePlacementMemorandum
                              }
                              loadingTitle={"Private Placement Memorandum"}
                            />
                            <Document
                              fileId={
                                entity?.entityDocuments?.subscriptionAgreement
                              }
                              loadingTitle={"Subscription Agreement"}
                            />
                          </Card.Group>
                        )}
                      </Grid.Column>
                    </Grid.Row>
                  )}

                {!["INVITED", "PENDING", "CLOSED", "CANCELLED"].includes(
                  subscription?.status
                ) && (
                  <Grid.Row>
                    <Grid.Column stretched width={16}>
                      <Divider hidden />
                      <Header as="h2" dividing>
                        Investment Commitment Documents
                      </Header>
                      {!entity ? (
                        <Loader active inline="centered" />
                      ) : (
                        <Card.Group textAlign="center" centered>
                          {subscription?.files?.map((subscriptionFile) => (
                            <Document
                              key={subscriptionFile}
                              fileId={subscriptionFile}
                              loadingTitle={"Commitment Document"}
                            />
                          ))}
                        </Card.Group>
                      )}
                    </Grid.Column>
                  </Grid.Row>
                )}

                {subscription?.status === "CLOSED" && subscription.closeDoc && (
                  <Grid.Row>
                    <Grid.Column stretched width={16}>
                      <Header as="h2" dividing>
                        Close Documents
                      </Header>
                      {!entity ? (
                        <Loader active inline="centered" />
                      ) : (
                        <Card.Group textAlign="center" centered>
                          <Document
                            fileId={subscription?.closeDoc.toString()}
                            loadingTitle={"Close Document"}
                          />
                        </Card.Group>
                      )}
                    </Grid.Column>
                  </Grid.Row>
                )}
              </Box>
            </Container>
          </Transition>
        </>
      )}
      <SendMessageModal
        open={showMessageModal}
        size="large"
        heading="Contact Organizer"
        onClickClose={() => setShowMessageModal(false)}
        onClickSend={async (message) => {
          setShowMessageModal(false);
          const response = await dispatch(
            insertNotification({
              tenantId: tenantProfile.id,
              profileId: deal.profileId,
              event: {
                type: "deal",
                id: deal.id,
              },
              category: "GENERAL",
              message,
              acknowledged: false,
              sender: {
                profileId: subscription.profileId,
                name: getProfileName(profile),
                email: profile.email,
              },
            })
          );

          await sendEmailToOrganizer(profile, message);

          if (response !== null) {
            toast.success("Message Sent");
            await dispatch(
              insertNotification({
                tenantId: tenantProfile.id,
                profileId: subscription.profileId,
                event: {
                  type: "subscription",
                  id: subscription.id,
                },
                category: "GENERAL",
                message:
                  `You have sent a message to the organizer of deal ${deal.name}.\nMessage:\n` +
                  message,
                acknowledged: false,
              })
            );
          } else {
            toast.error("Message failed to send");
          }
        }}
        messageCaption="Message"
        messagePlaceholder={`I have a question about the deal, ${
          deal?.name ?? ""
        }`}
      />
    </>
  );
};
