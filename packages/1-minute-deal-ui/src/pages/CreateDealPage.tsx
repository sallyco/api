import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Sentry from "@sentry/react";
import { Helmet } from "react-helmet";
import { fetchCompanyById, updateCompanyById } from "../slices/companiesSlice";

import { toast } from "react-toastify";
import { RootState } from "../rootReducer";

import { Redirect, useHistory, useLocation } from "react-router-dom";

import { fetchProfileById } from "../slices/profilesSlice";
import { insertEntity } from "../slices/entitySlice";
import { insertDeal, updateDealById } from "../slices/dealsSlice";

import { Container, Divider, Transition, Loader } from "semantic-ui-react";
import CreateDealForm from "../forms/CreateDealForm";
import ShortHeader from "../components/ShortHeader";
import { getCurrentSeriesName, setNextSeriesName } from "../api/entitiesApi";

import { ToastError } from "../tools/ToastMessage";
import Log from "../tools/Log";
import { TenantContext } from "../contexts";

import { atLimit } from "../api/dealsApi";
import {
  fixAmountValue,
  MANAGEMENT_FEE_FREQUENCY,
} from "../forms/subforms/SubformManagementFee";

export const CreateDealPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const query = new URLSearchParams(useLocation().search);

  const [submitting, setSubmitting] = useState(false);
  const [entityName, setEntityName] = useState("");
  const [redirectToDealSummary, setRedidrectToDealSummary] = useState(false);

  //TODO there has to b a better way to handle this in TS
  let profileId = query.get("profileId") || null;
  const tenantProfile = useContext(TenantContext);

  Log.debug("profileId", profileId);

  const profile = useSelector(
    (state: RootState) => state.profiles.profilesById[profileId]
  );

  useEffect(() => {
    if (!profile || profile.id !== profileId) {
      dispatch(fetchProfileById(profileId ?? ""));
    }
  }, [dispatch, profile, profileId]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const masterEntity = useSelector((state: RootState) => {
    return !profile ||
      (!profile.masterEntityId && !tenantProfile.masterEntityId)
      ? undefined
      : state.companies.companiesById[
          profile.masterEntityId
            ? profile.masterEntityId
            : tenantProfile.masterEntityId
        ];
  });

  useEffect(() => {
    const getSeriesName = async () => {
      setEntityName(await getCurrentSeriesName());
    };
    if (profile) {
      if (masterEntity) {
        const pad = "000";
        setEntityName(
          `${masterEntity?.seriesPrefix ?? "Fund"} ${(
            pad + masterEntity.seriesNumber
          ).slice(-pad.length)}, a series of ${masterEntity.name}`
        );
      } else {
        if (
          !profile.masterEntityId &&
          !tenantProfile.masterEntityId &&
          !entityName
        ) {
          getSeriesName();
        }
      }
    }
  }, [profile, masterEntity]);

  useEffect(() => {
    if (profile && !masterEntity) {
      if (profile.masterEntityId) {
        dispatch(fetchCompanyById(profile.masterEntityId));
      } else if (tenantProfile.masterEntityId) {
        dispatch(fetchCompanyById(tenantProfile.masterEntityId));
      }
    }
  }, [dispatch, profile, tenantProfile]);

  useEffect(() => {
    // see if this user should be able to create a deal or not
    atLimit().then((results) => {
      if (results === true) {
        setRedidrectToDealSummary(true);
      }
    });
  }, []);

  if (profileId === null) {
    return <Redirect to="/profiles/create" />;
  }

  if (redirectToDealSummary) {
    return <Redirect to="/dashboard/deals?atLimit=true" />;
  }

  async function onSubmit(data) {
    Log.debug("data", data);
    setSubmitting(true);

    try {
      Log.debug("profile", profile);

      const dealData = {
        name: data.dealName,
        description: data.description,
        targetRaiseAmount: Number(data.targetRaiseAmount.replace(/\D+/g, "")),
        ...(data.previouslyRaisedAmount && {
          previouslyRaisedAmount: Number(
            data.previouslyRaisedAmount.replace(/\D+/g, "")
          ),
        }),
        minInvestmentAmount: Number(
          data.minInvestmentAmount.replace(/\D+/g, "")
        ),
        isPublic: data.isPublic,
        estimatedCloseDate: new Date(data.estimatedCloseDate),

        organizerCarryPercentage: Number(
          data.organizerCarryPercentage.replace("%", "")
        ),
        status: "DRAFT",
        requireQualifiedPurchaser: data?.requireQualifiedPurchaser ?? false,
      };

      Log.debug("dealData", dealData);

      let entityName: string;
      if (masterEntity) {
        const pad = "000";
        entityName = `${masterEntity?.seriesPrefix ?? "Fund"} ${(
          pad + masterEntity.seriesNumber
        ).slice(-pad.length)}, a series of ${masterEntity.name}`;
      } else {
        entityName = await getCurrentSeriesName();
      }

      const entityData = {
        name: entityName,
        entityType: "LIMITED_LIABILITY_COMPANY", //data.entityType,
        regulationType: data.regulationType
          ? data.regulationType
          : "REGULATION_D",
        regDExemption: data.regDExemption ?? "b",
        minInvestmentAmount: Number(
          data.minInvestmentAmount.replace(/\D+/g, "")
        ),
        countryOfFormation: "US",
        stateOfFormation: data.stateOfFormation,
      };

      if (data?.managementFee?.amount) {
        Object.assign(entityData, {
          ...entityData,
          managementFee: {
            ...data.managementFee,
            frequency: MANAGEMENT_FEE_FREQUENCY,
            amount: fixAmountValue(data.managementFee),
          },
        });
      }

      //TODO this must be cleaned up a lot
      const dealRes: any = await dispatch(insertDeal(dealData));
      Log.debug("dealRes", dealRes);

      const entityRes: any = await dispatch(
        insertEntity({ ...entityData, dealId: dealRes.id })
      );

      if (masterEntity) {
        const success = await dispatch(
          updateCompanyById({
            ...masterEntity,
            seriesNumber: masterEntity.seriesNumber + 1,
          })
        );
        if (success === null) {
          Sentry.captureMessage("seriesNumber failed to update", {
            extra: {
              seriesNumber: {
                from: masterEntity?.seriesNumber ?? "",
                to: masterEntity?.seriesNumber + 1,
              },
              masterEntity: {
                id: masterEntity.id,
                name: masterEntity.name,
              },
            },
          });
        }
      } else {
        await setNextSeriesName();
      }

      await dispatch(
        updateDealById({
          ...dealRes,
          entityId: entityRes.id,
          profileId: profile.id,
        })
      );

      setSubmitting(false);
      toast.success(`Deal "${dealRes.name}" Created`);

      history.push(`/deals/${dealRes.id}/review-docs?generate=true`);
    } catch (e) {
      Log.error("error", e);
      setSubmitting(false);
      if (e.response) {
        ToastError(e.response);
      }
    }
  }

  return (
    <>
      <Helmet title={`Create Deal | ${tenantProfile.name}`}></Helmet>
      <ShortHeader title={`Create Your Deal`} />
      <Divider hidden />
      <div>
        {!entityName ? (
          <Loader active inline="centered" size="massive" />
        ) : (
          <Container text className="margin-bottom-container">
            <CreateDealForm
              entityName={entityName}
              onSubmit={onSubmit}
              submitting={submitting}
              fieldSettings={{
                maxTargetRaiseAmount:
                  tenantProfile?.dealLimits?.perDealMaxTargetRaiseAmount ||
                  null,
              }}
            />
          </Container>
        )}
      </div>
    </>
  );
};
