/* eslint-disable react/display-name */
import React, { useContext } from "react";
import { Helmet } from "react-helmet";

import { OrganizerTab } from "../components/users/OrganizerTab";
import { ProfilesTab } from "../components/users/ProfilesTab";
import { AccountTab } from "../components/users/AccountTab";
import { NotificationsTab } from "../components/notifications/NotificationsTab";

import { Tab } from "semantic-ui-react";

import { AuthContext, TenantContext } from "../contexts";

export const SettingsPage = () => {
  const tenantProfile = useContext(TenantContext);
  const userProfile = useContext(AuthContext);

  const panes = [
    {
      menuItem: "ACCOUNT",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <AccountTab />
        </Tab.Pane>
      ),
    },
    ...(userProfile.resource_access.account.roles.includes("organizer")
      ? [
          {
            menuItem: "ORGANIZER",
            style: { color: "blue" },
            render: () => (
              <Tab.Pane inverted attached={false}>
                <OrganizerTab />
              </Tab.Pane>
            ),
          },
        ]
      : []),
    ...(userProfile.resource_access.account.roles.includes("investor")
      ? [
          {
            menuItem: "INVESTOR PROFILES",
            render: () => (
              <Tab.Pane inverted attached={false}>
                <ProfilesTab />
              </Tab.Pane>
            ),
          },
        ]
      : []),
    {
      menuItem: "NOTIFICATION HISTORY",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <NotificationsTab />
        </Tab.Pane>
      ),
    },
  ];

  return (
    <>
      <Helmet title={`Settings | ${tenantProfile.name}`}></Helmet>
      {/* {false ? (
        <Loader active inline="centered" size="massive" />
      ) : ( */}
      <>
        <Tab
          menu={{ secondary: true, pointing: true, className: "nav-bar" }}
          panes={panes}
        />
      </>
      {/* )} */}
    </>
  );
};
