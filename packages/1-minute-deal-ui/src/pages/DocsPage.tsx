/* eslint-disable react/display-name */
import React, { useContext } from "react";
import { Helmet } from "react-helmet";

import { MyDocumentsTab } from "../components/files/MyDocumentsTab";
import { ByDealTab } from "../components/files/ByDealTab";
import { TaxTab } from "../components/files/TaxTab";

import { Tab, Loader } from "semantic-ui-react";
import { TenantContext } from "../contexts";

export const DocsPage = () => {
  const tenantProfile = useContext(TenantContext);
  const panes = [
    {
      menuItem: "BY DEAL",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <ByDealTab />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "MY DOCUMENTS",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <MyDocumentsTab />
        </Tab.Pane>
      ),
    },
    {
      menuItem: "TAX",
      render: () => (
        <Tab.Pane inverted attached={false}>
          <TaxTab />
        </Tab.Pane>
      ),
    },
  ];

  return (
    <>
      <Helmet title={`Documents | ${tenantProfile.name}`} />
      <Tab
        menu={{
          secondary: true,
          pointing: true,
          fluid: true,
          className: "nav-bar",
        }}
        panes={panes}
      />
    </>
  );
};
