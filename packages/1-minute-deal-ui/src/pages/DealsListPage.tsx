import React, { useContext, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { Loader, Divider } from "semantic-ui-react";

import { RootState } from "../rootReducer";
import { DealsList } from "../components/deals/DealsList";
import { fetchDealsList, searchDeal } from "../slices/dealsSlice";
import { TenantContext } from "../contexts";
import { PopupModal } from "../components/common/PopupModal";

export const DealsListPage = () => {
  const query = new URLSearchParams(useLocation().search);
  const showModal = query.get("atLimit") ? true : false;
  const [promptForPayment, setPromptForPayment] = useState(showModal);

  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const { currentPageDeals, isInitialized, isLoading, dealsById } = useSelector(
    (state: RootState) => state.deals
  );

  const deals = currentPageDeals.map((dealId) => dealsById[dealId]);

  //  const showIssuesList = () => {
  //    dispatch(setCurrentDisplayType({ displayType: 'issues' }))
  //  }

  const { opType, opValue, opFilterValue } = useSelector(
    (state: RootState) => state.dealsListOpeartions
  );

  // useEffect(() => {
  //   dispatch(fetchDealsList());
  // }, [page, dispatch]);

  const [tempDeals, settempDeals] = useState(deals);

  useEffect(() => {
    switch (opType) {
      case "search":
        if (opValue) {
          const mainsearch = {
            search: [{ field: "name", value: opValue }],
          };
          dispatch(searchDeal(mainsearch));
        }
        return;
      case "filter":
        if (opFilterValue) {
          const mainsearch = {
            search: opFilterValue,
          };
          dispatch(searchDeal(mainsearch));
        }
        return;
      case "sort":
        if (opValue) {
          const splitdata = opValue.split("-");
          const mainsearch = {
            sort: [{ field: splitdata[0], direction: splitdata[1] }],
          };
          dispatch(searchDeal(mainsearch));
        }
        return;
      default:
        dispatch(fetchDealsList());
        return;
    }
  }, [opValue, opFilterValue]);

  return (
    <>
      <PopupModal
        open={promptForPayment}
        size="tiny"
        heading="Please Contact Us"
        content={
          <p>
            You have exceeded the number of SPVs permitted on your Account.
            Please contact your Sales Rep to increase your SPV limit
          </p>
        }
        okcaption="OK"
        onClickOk={() => setPromptForPayment(false)}
      />
      <Helmet title={`Deals - Dashboard | ${tenantProfile.name}`}></Helmet>
      {isLoading || !deals ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <DealsList deals={deals} />
      )}
    </>
  );
};
