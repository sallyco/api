/* eslint-disable react/display-name */
import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import NoCompanyOrganizer from "../components/companies/NoCompanyOrganizer";
import { TenantContext } from "../contexts";
import { Divider } from "semantic-ui-react";

export const CompanyOrganizerPage = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet title={`Organizers | ${tenantProfile.name}`} />
      <Divider hidden />
      <NoCompanyOrganizer />
    </>
  );
};
