/* eslint-disable react/display-name */
import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { Divider } from "semantic-ui-react";
import { TenantContext } from "../contexts";
import { CompanySettingsCard } from "../components/companies/CompanySettingsCard";

export const CompanySettingsPage = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet title={`Settings | ${tenantProfile.name}`} />
      <Divider hidden />
      <CompanySettingsCard />
    </>
  );
};
