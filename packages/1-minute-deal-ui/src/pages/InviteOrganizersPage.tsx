/* eslint-disable no-await-in-loop */
import React, { useState, useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Formik } from "formik";
import { FormField } from "../components/FormField";
import * as Yup from "yup";
import { TenantContext } from "../contexts";

import {
  Form,
  Button,
  Divider,
  Container,
  Header,
  Image,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import * as emailApi from "../api/emailsApi";
import { User, getUserByEmail, getSelf } from "../api/usersApi";
import { createSubscription } from "../api/subscriptionsApi";

import { Link, useHistory, useParams } from "react-router-dom";
import ShortHeader from "../components/ShortHeader";
import { toast } from "react-toastify";
import { RootState } from "../rootReducer";
import Log from "../tools/Log";

import { insertInvite } from "../slices/invitesSlice";
import { fetchDealById } from "../slices/dealsSlice";
import loadingDocsImage from "./../assets/images/invite-org.png";
import { fetchAssetById, updateAssetById } from "../slices/assetsSlice";
import { fetchSelf } from "../slices/usersSlice";

export const InviteOrganizersPage = () => {
  const dispatch = useDispatch();
  const { assetId } = useParams<{ assetId: string }>();
  let history = useHistory();
  const tenantProfile = useContext(TenantContext);
  const { self } = useSelector((state: RootState) => state.users);
  const [submitting, setSubmitting] = useState(false);

  const asset = useSelector(
    (state: RootState) => state.assets.assetsById[assetId]
  );

  useEffect(() => {
    if (!asset) {
      dispatch(fetchAssetById(assetId));
    }
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [dispatch, asset, self, assetId]);

  async function onSubmit(data) {
    Log.debug("data", data);
    Log.debug("self", self);
    setSubmitting(true);
    const invitees = data.email.trim().split(/[\s,]+/);

    await dispatch(
      updateAssetById({
        id: assetId,
        invitedOrganizers: invitees,
      })
    );

    for (const invite of invitees) {
      let hasEmail: User | boolean = false;
      try {
        hasEmail = await getUserByEmail(invite);
      } catch (e) {
        Log.debug("e", e);
        setSubmitting(false);
        if (e.statusCode === 404) {
          hasEmail = false;
        }
      }
      if (!hasEmail) {
        let inviteObject: any = await dispatch(
          insertInvite({
            email: invite,
            id: assetId,
            type: "company",
            relation: "organizer",
            role: "organizer,investor",
          })
        );
      }
      await emailApi.sendEmail({
        sender: self,
        to: invite,
        recipient: {
          firstName: hasEmail ? hasEmail.firstName : "",
          lastName: hasEmail ? hasEmail.lastName : "",
          email: invite,
        },
        company: asset,
        founder: { name: self?.firstName + " " + self?.lastName, ...self },
        subject: "Organizer Invitation",
        templateId: "inviteOrganizer",
        passLink: hasEmail
          ? window.location.origin.toString() + "/#/login"
          : window.location.origin.toString() + "/#/register",
        acceptLink: window.location.origin.toString() + `/#/assets/${assetId}`,
      });
    }

    toast.success(
      `${invitees.length} invitation${invitees.length > 1 ? "s" : ""} sent.`
    );
    history.push(`/assets/${assetId}`);
    setSubmitting(false);
  }

  const validation = Yup.object().shape({
    email: Yup.array()
      .transform(function (value, originalValue) {
        if (value !== null) {
          return value;
        }
        return originalValue ? originalValue.split(/[\s,]+/) : [];
      })
      .of(
        Yup.string()
          .trim()
          .email(({ value }) => `${value} is not a valid email `)
      ),
    //.required(),
  });

  return (
    <>
      <Helmet
        title={`Invite an Organizer - Deals | ${tenantProfile.name}`}
      ></Helmet>
      <ShortHeader title={"Invite an Organizer"} />
      <Divider hidden />
      <Container className="mobile-container">
        <Image src={loadingDocsImage.src} centered size="small" />

        <Header as="h3" inverted textAlign="center">
          Share with an organizer.
          <Divider hidden />
          <Header.Subheader>
            The organizer will be able to communicate and exchange files with
            you regarding due diligence.
          </Header.Subheader>
        </Header>
        <Formik
          initialValues={{}}
          validationSchema={validation}
          onSubmit={onSubmit}
        >
          {(props: any) => (
            <Form onSubmit={props.handleSubmit}>
              <FormField
                name="email"
                component={Form.Input}
                label="Organizer Email Address"
                placeholder="Organizer email address"
              />
              <Divider hidden />
              <Button fluid type="submit" positive loading={submitting}>
                Send Invite
              </Button>
              <Divider hidden />
              <Button secondary disabled fluid type="button">
                {/*as={Link} to={`/deals/${dealId}`}*/}
                View Your Deal
              </Button>
            </Form>
          )}
        </Formik>
      </Container>
    </>
  );
};
