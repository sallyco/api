/* eslint-disable no-await-in-loop */
import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Formik } from "formik";
import { FormField } from "../components/FormField";
import * as Yup from "yup";
import { TenantContext } from "../contexts";

import {
  Form,
  Button,
  Divider,
  Container,
  Header,
  Segment,
  List,
  Icon,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { User, getUserByEmail } from "../api/usersApi";
import {
  Subscription,
  createSubscription,
  getSubscriptionsByDeal,
} from "../api/subscriptionsApi";
import { fetchSelf } from "../slices/usersSlice";
import { insertNotification } from "../slices/notificationsSlice";

import { Link, useHistory, useParams } from "react-router-dom";
import ShortHeader from "../components/ShortHeader";
import { toast } from "react-toastify";
import { RootState } from "../rootReducer";
import Log from "../tools/Log";

import { insertInvite, fetchInvitesListByDeal } from "../slices/invitesSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchSubscriptionsListByDeal } from "../slices/subscriptionsSlice";
import {
  sendInviteInvestor,
  sendInviteInvestorExisting,
} from "../api/emailsApi";
import { PopupModal } from "../components/common/PopupModal";
import ContactListTable, {
  generateEmailFieldText,
  useContactsList,
  CONTACT_LIST_FLAG,
} from "../components/ContactListTable";
import { FeatureFlag } from "../components/featureflags/FeatureFlags";
import useSWR from "swr";
import { fetcher } from "../swrConnector";
import _ from "lodash";

const baseUrl = process.env.API_URL ?? process.env.NEXT_PUBLIC_API_URL;

export const InviteInvestorsPage = () => {
  const dispatch = useDispatch();
  const { dealId } = useParams<{ dealId: string }>();
  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const [submitting, setSubmitting] = useState(false);
  const [contactsOpen, setContactsOpen] = useState(false);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const { self } = useSelector((state: RootState) => state.users);
  const { contacts, selectedContacts, setSelectedRows } = useContactsList(self);

  const { data: dealData, error } = useSWR(
    () => baseUrl + `/deals/${dealId}`,
    fetcher
  );

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId, dispatch]);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [dispatch, self]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  async function onSubmit(data) {
    Log.debug("data", data);
    setSubmitting(true);
    let emails = data.email.trim().split(/[\s,]+/);
    const fromEmail = self?.email;

    if (!emails) return;

    emails = _.uniq(emails);

    const subs = await getSubscriptionsByDeal(dealId);

    await Promise.all(
      emails.map(async (email) => {
        if (!email) return;
        if (email === fromEmail) return;

        let hasEmail: User | boolean = false;
        try {
          hasEmail = await getUserByEmail(email);
        } catch (e) {
          Log.debug("e", e);
          if (e.statusCode === 404) {
            hasEmail = false;
          }
        }

        if (!hasEmail) {
          let invite: any = await dispatch(
            insertInvite({
              email: email,
              id: dealId,
              type: "deal",
              relation: "subscription",
              role: "investor",
            })
          );
          await sendInviteInvestor(
            invite.acceptableBy,
            fromEmail,
            dealData,
            invite
          );
        } else {
          var result = subs.data.find((obj) => {
            return obj.email === email;
          });
          Log.debug("result", result);

          let subscription: Subscription = result
            ? result
            : await createSubscription({
                email: hasEmail.username,
                name: `${hasEmail.firstName} ${hasEmail.lastName}`,
                ownerId: hasEmail.id,
                dealId: dealId,
                status: "INVITED",
                acquisitionMethod: "INVITATION",
              });

          await sendInviteInvestorExisting(
            hasEmail,
            fromEmail ?? "",
            dealData,
            subscription
          );

          await dispatch(
            insertNotification({
              tenantId: tenantProfile.id,
              userId: hasEmail.id,
              event: {
                type: "subscription",
                id: subscription.id,
              },
              category: "INVESTMENTS",
              message: `You have been invited to review a new deal ${dealData.name}`,
              acknowledged: false,
            })
          );
        }
      })
    );

    let invitesSent = emails.length;
    toast.success(
      `${invitesSent} invitation${invitesSent > 1 ? "s" : ""} sent.`
    );
    dispatch(fetchInvitesListByDeal(dealId));
    dispatch(fetchSubscriptionsListByDeal(dealId));
    setSubmitting(false);
    history.push(`/deals/${dealId}`);
  }

  const validation = Yup.object().shape({
    inviteSelf: Yup.boolean(),
    email: Yup.array()
      .transform(function (value, originalValue) {
        if (value !== null) {
          return value;
        }
        return originalValue ? originalValue.split(/[\s,]+/) : [];
      })
      .of(
        Yup.string()
          .trim()
          .email(({ value }) => `${value} is not a valid email `)
      )
      .when("inviteSelf", {
        is: false,
        then: Yup.array().required(),
      }),
  });

  return (
    <>
      <Helmet
        title={`Invite Investors - Deals | ${tenantProfile.name}`}
      ></Helmet>
      <ShortHeader title={"Invite Investors"} />
      <Divider hidden />
      <Container className="mobile-container">
        <Header as="h2" inverted textAlign="center">
          Invite your investors
        </Header>

        {/*
        {supported && (
          <Button
            onClick={getContacts}
            secondary
            className="invite-investor-button"
          >
            <Icon name="users"></Icon>
            IMPORT FROM CONTACTS
          </Button>
        )}
*/}

        <Segment basic inverted textAlign="center" className="no-spacing">
          Investors will get an invite to review your deal and it&apos;s
          documents.
        </Segment>

        <Segment basic inverted textAlign="left">
          <List>
            <List.Item>Example:</List.Item>
            <List.Item>johnsmith@email.com,</List.Item>
            <List.Item>mbrown@email.com</List.Item>
          </List>
        </Segment>

        <Formik
          initialValues={{
            inviteSelf: false,
            email: "",
          }}
          validationSchema={validation}
          onSubmit={onSubmit}
        >
          {(props) => {
            return (
              <>
                <Form onSubmit={props.handleSubmit}>
                  <FormField
                    name="email"
                    component={Form.TextArea}
                    label="Emails"
                    placeholder="Email Addresses"
                  />
                  <FeatureFlag name={CONTACT_LIST_FLAG}>
                    {Array.isArray(contacts) && contacts.length > 0 && (
                      <Button
                        fluid
                        onClick={() => {
                          setContactsOpen(true);
                        }}
                        secondary
                        className="invite-investor-button"
                        type="button"
                      >
                        <Icon name="users"></Icon>
                        Import From Contacts
                      </Button>
                    )}
                  </FeatureFlag>
                  <Divider hidden />
                  <Button secondary fluid type="submit" loading={submitting}>
                    Send Invites
                  </Button>
                </Form>
                <PopupModal
                  open={contactsOpen}
                  size="tiny"
                  heading="Contact List"
                  content={
                    <ContactListTable
                      contacts={contacts}
                      setSelectedRows={setSelectedRows}
                    />
                  }
                  closecaption="Cancel"
                  okcaption="Add"
                  onClickClose={() => setContactsOpen(false)}
                  onClickOk={() => {
                    props.setFieldValue(
                      "email",
                      generateEmailFieldText(
                        props.values.email,
                        selectedContacts
                      )
                    );
                    setContactsOpen(false);
                  }}
                />
              </>
            );
          }}
        </Formik>

        <Divider inverted />
        <Button as={Link} to={`/deals/${dealId}`} inverted fluid>
          Done - View Deal Status
        </Button>
      </Container>
    </>
  );
};
