import React, { useContext, useState } from "react";
import { Helmet } from "react-helmet";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { createKycAmlCheck } from "../api/profilesApi";
import {
  Button,
  Segment,
  Transition,
  Header,
  Image,
  Container,
  Divider,
} from "semantic-ui-react";
import kycaml from "../assets/images/kycaml.png";
import { ToastError } from "../tools/ToastMessage";
import { TenantContext } from "../contexts";

export const KycAmlReviewPage = () => {
  const [submitting, setSubmitting] = useState(false);
  let history = useHistory();
  const { profileId } = useParams<{ profileId: string }>();
  const query = new URLSearchParams(useLocation().search);
  const subscriptionId = query.get("subscriptionId") ?? "";
  const tenantProfile = useContext(TenantContext);

  async function handleClick() {
    try {
      setSubmitting(true);
      await createKycAmlCheck(profileId);
      toast.success(`KYC/AML Passed`);
      history.push(`/subscriptions/${subscriptionId}/set-amount`);
    } catch (e: any) {
      setSubmitting(false);
      if (e.response) {
        ToastError(e.response);
      }
    }
  }

  return (
    <>
      <Helmet
        title={`KYC/AML Review - Profiles | ${tenantProfile.name}`}
      ></Helmet>
      <Transition>
        <Container textAlign="center" className="mobile-container">
          <Divider />
          <Image src={kycaml.src} size="small" centered />
          <Header as="h2" inverted icon>
            KYC/AML Review
          </Header>
          <Segment
            inverted
            textAlign="left"
            size="small"
            className="no-padding"
          >
            Before moving forward with this investment, Glassboard Technology
            will need to complete its KYC/AML review.
          </Segment>
          <Segment
            inverted
            textAlign="left"
            size="small"
            className="no-padding"
          >
            Click PROCEED below to start this automated process. The review can
            take up to 2 business days and Glassboard Technology will reach out
            if more information is needed.
          </Segment>
          <Button onClick={handleClick} loading={submitting} secondary fluid>
            Proceed
          </Button>
        </Container>
      </Transition>
    </>
  );
};
