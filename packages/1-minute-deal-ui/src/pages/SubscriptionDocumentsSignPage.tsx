import React, {
  useContext,
  useEffect,
  useState,
  useRef,
  useCallback,
} from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useParams, useHistory, useLocation } from "react-router-dom";
import { RootState } from "../rootReducer";
import { DocumentHelpers } from "../tools/helpers";

import {
  fetchSubscriptionById,
  generateSubscriptionDocument,
  clearSigningDocuments,
  patchSubscriptionById,
} from "../slices/subscriptionsSlice";
import { fetchEntityById } from "../slices/entitySlice";
import { fetchProfileById } from "../slices/profilesSlice";
import { fetchDealById } from "../slices/dealsSlice";
import PdfDocument from "../components/PdfDocument";

import {
  Divider,
  Header,
  Button,
  Container,
  Segment,
  Loader,
} from "semantic-ui-react";

import { SignaturePad } from "@packages/gbt-ui";
import ShortHeader from "../components/ShortHeader";
import moment from "moment";

import Log from "../tools/Log";
import { TenantContext } from "../contexts";
import { insertNotification } from "../slices/notificationsSlice";
import { fetchSelf } from "../slices/usersSlice";
import { Profile } from "../api/profilesApi";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import { insertInvite } from "../slices/invitesSlice";
import { sendInviteSigner } from "../api/emailsApi";
import { getUserEmailExist } from "../api/usersApi";
import { API, useRequest } from "../api/swrApi";
import { updateSubscriptionSigners } from "../api/subscriptionsApi";

const QP_FEATURE_FLAG = "qualified_purchaser";

export const SubscriptionDocumentsSignPage = () => {
  const qualifiedPurchaserFeature = useFeatureFlag(QP_FEATURE_FLAG);
  const dispatch = useDispatch();
  // @ts-ignore
  const { subscriptionId } = useParams();
  const [isAllSubmitting, setIsAllSubmitting] = useState(false);
  const [sigStage, setSigStage] = useState("");
  const [subFile, setSubFile] = useState("");

  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const query = new URLSearchParams(useLocation().search);
  //TODO there has to b a better way to handle this in TS
  let isGenerate = query.get("generate") ?? false;
  let isRegenerate = query.get("regenerate") ?? false;
  let isAutoResign = query.get("autoresign") ?? false;
  let wiringBankName = query.get("wiringBankName") ?? "";
  let wiringBankInUS = query.get("wiringBankInUS") ?? false;
  let wiringBankSub = query.get("wiringBankSub") ?? false;
  Log.debug("isGenerate", isGenerate);
  const previousAmount = query.get("previousAmount") ?? 0;

  const [canAccept, setCanAccept] = useState(false);

  let __generated = useRef(false);

  const { data: self } = useRequest(API.SELF);

  // SUBSCRIPTION
  const { isLoading: subscriptionsLoading, currentSigningDocuments } =
    useSelector((state: RootState) => state.subscriptions);

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  // DEAL
  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });
  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [dispatch, subscription, deal]);

  // PROFILE
  //const { isLoading: profilesLoading } = useSelector(
  //  (state: RootState) => state.profiles
  //);

  const subscriptionProfile = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.profiles.profilesById[subscription.profileId];
  });

  useEffect(() => {
    if (subscription && !subscription.profileId) {
      history.replace(
        `/profiles/create/subscriber?subscriptionId=${subscription.id}`
      );
    } else if (subscription && !subscriptionProfile) {
      dispatch(fetchProfileById(subscription.profileId));
    }
  }, [dispatch, subscription, subscriptionProfile]);

  // ENTITY
  //
  //const { isLoading: entitiesLoading } = useSelector(
  //  (state: RootState) => state.entities
  //);

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });
  useEffect(() => {
    if (deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  const isPrimarySigner = useCallback(() => {
    return (
      self?.id && subscription?.ownerId && self.id === subscription.ownerId
    );
  }, [self, subscription]);

  useEffect(() => {
    async function createDocs() {
      dispatch(clearSigningDocuments());
      if (
        isGenerate &&
        !__generated.current &&
        subscription &&
        deal &&
        entity &&
        subscriptionProfile
      ) {
        __generated.current = true;
        const isRegS =
          entity.regulationType === "REGULATION_S" ||
          (entity.regulationType === "REGULATION_D_S" &&
            subscriptionProfile.address.country !== "United States of America");
        // This handles showing the second page for entity SA
        if (
          subscriptionProfile.taxDetails.registrationType === "ENTITY" ||
          subscriptionProfile.taxDetails.registrationType === "TRUST"
        ) {
          const id: any = await dispatch(
            generateSubscriptionDocument(subscriptionId, "sub")
          );
          setSubFile(id);
        } else {
          await dispatch(generateSubscriptionDocument(subscriptionId, "sub"));
        }

        if (subscriptionProfile.typeOfEntity !== "FOREIGN_ENTITY" && !isRegS) {
          if (await DocumentHelpers.shouldUseW9(subscriptionProfile)) {
            await dispatch(generateSubscriptionDocument(subscriptionId, "w9"));
          } else {
            if (subscriptionProfile.taxDetails.registrationType !== "TRUST") {
              await dispatch(
                generateSubscriptionDocument(subscriptionId, "w8ben")
              );
            }
          }
        }
        await dispatch(generateSubscriptionDocument(subscriptionId, "oa"));
      }
    }
    if (
      isGenerate &&
      !__generated.current &&
      subscription &&
      deal &&
      entity &&
      subscriptionProfile
    )
      if (isAutoResign === "true") {
        handleClick();
      } else {
        createDocs();
      }
  }, [
    dispatch,
    subscription,
    subscriptionId,
    subscriptionProfile,
    deal,
    entity,
    isGenerate,
    isAutoResign,
    __generated.current,
  ]);

  async function handleClick(sig?: string) {
    setIsAllSubmitting(true);
    __generated.current = true;

    const createdSig = isAutoResign === "true" ? subscription.signature : sig;
    Log.debug("signature", createdSig);

    if (subscription && deal && entity && subscriptionProfile) {
      setSigStage("Operating Agreement");
      let files = [];

      const file1: any = await dispatch(
        generateSubscriptionDocument(subscriptionId, "oa", createdSig)
      );
      files.push(file1.id);

      const isRegS =
        entity.regulationType === "REGULATION_S" ||
        (entity.regulationType === "REGULATION_D_S" &&
          subscriptionProfile.address.country !== "United States of America");
      setSigStage("Subscription Agreement");
      const file2: any = await dispatch(
        generateSubscriptionDocument(subscriptionId, "sub", createdSig)
      );
      files.push(file2.id);
      let file3: any;
      if (subscriptionProfile.typeOfEntity !== "FOREIGN_ENTITY" && !isRegS) {
        if (await DocumentHelpers.shouldUseW9(subscriptionProfile)) {
          setSigStage("W-9");
          file3 = await dispatch(
            generateSubscriptionDocument(subscriptionId, "w9", createdSig)
          );
        } else {
          setSigStage("W-8BEN");
          file3 = await dispatch(
            generateSubscriptionDocument(subscriptionId, "w8ben", createdSig)
          );
        }
        files.push(file3.id);
      }

      await dispatch(
        patchSubscriptionById(subscription.id, {
          ...(!isRegenerate ? { status: "COMMITTED" } : {}),
          isDocsSigned: true,
          files: files,
        })
      );
      await dispatch(
        insertNotification({
          tenantId: tenantProfile.id,
          userId: self.id,
          event: {
            type: "subscription",
            id: subscription.id,
          },
          category: "INVESTMENTS",
          message: `You have committed to investing in Deal ${deal.name}`,
          acknowledged: false,
        })
      );

      if (
        subscriptionProfile?.additionalSignatories &&
        subscriptionProfile?.additionalSignatories.length > 0
      ) {
        // Invite all additional Signers...
        const createInvites = [];
        const userExistsPromises = [];
        for (const signer of subscriptionProfile.additionalSignatories) {
          createInvites.push(
            dispatch(
              insertInvite({
                email: signer.email,
                id: self.id,
                type: "profile",
                relation: "individual",
                role: "signer",
              })
            )
          );

          userExistsPromises.push(getUserEmailExist(signer.email));
        }
        const invites = await Promise.all(createInvites);
        const exists: { data: { email: string } }[] = await Promise.all(
          userExistsPromises
        );

        if (subscription.signers && subscription.signers.length > 0) {
          await updateSubscriptionSigners(subscription.id);
        }

        const sendEmails = [];
        for (let i = 0; i < invites.length; i++) {
          const invite = invites[i];
          const isUser = !!exists[i].data.email;
          sendEmails.push(
            sendInviteSigner(
              invite.acceptableBy,
              self.email,
              deal,
              invite,
              isUser,
              subscription.id
            )
          );
        }
        await Promise.all(sendEmails);
      }

      setIsAllSubmitting(false);
    }

    let url = `/subscriptions/${subscriptionId}/funding-select${
      previousAmount > 0 ? `?previousAmount=${previousAmount}` : ""
    }`;
    if (isRegenerate) {
      url = `/subscriptions/${subscriptionId}`;
    }
    if (!isPrimarySigner()) {
      url = `/signing/${subscriptionId}`;
    }
    history.push(url);
  }

  return (
    <>
      <Helmet title={`Sign Documents | ${tenantProfile.name}`} />
      <ShortHeader title={deal?.name} loading={dealsLoading} hideBackArrow />
      {dealsLoading ||
      subscriptionsLoading ||
      isAllSubmitting ||
      currentSigningDocuments.length === 0 ? (
        <>
          <Divider hidden />
          <Loader active inverted inline="centered" size="massive" />
          <Segment basic inverted textAlign="center">
            {isAllSubmitting
              ? `${
                  isAutoResign === "true"
                    ? "Updating "
                    : "Applying Signature to "
                } ${sigStage}`
              : "Generating Signing Documents"}
          </Segment>
        </>
      ) : (
        <>
          <Divider hidden />
          <Container textAlign="center">
            <Header as="h4" textAlign="center" inverted>
              Draw your signature in the space below, it will be applied to all
              documents.
            </Header>
            <SignaturePad onSubmit={handleClick} id={"sig"} />
            <Divider />
            <Container className="wide-mobile-container" textAlign="center">
              {currentSigningDocuments.map((doc) => (
                <div key={doc.id}>
                  <PdfDocument
                    fileId={doc.id}
                    pageNumber={doc.id === subFile ? 2 : 1}
                  />
                  <Divider hidden />
                </div>
              ))}
            </Container>
          </Container>
        </>
      )}
    </>
  );
};
