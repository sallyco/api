import React, { useContext, useState } from "react";
import { TenantContext } from "../contexts";
import { Link, useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Segment, List, Container, Transition } from "semantic-ui-react";
import { getToken } from "../api/authApi";
import * as parseCookie from "../api/parseCookie";
import LoginForm from "../forms/LoginForm";
import BuildInfo from "../development/BuildInfo";
import { useDispatch } from "react-redux";
import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import { fetchSelf } from "../slices/usersSlice";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import AuthRedirect from "../components/AuthRedirect";
import { Unverified } from "../pages/UnverifiedPage";

export const Login = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [submitting, setSubmitting] = useState(false);
  const [credentialError, setCredentialError] = useState(false);
  const [showUnverified, setShowVerified] = useState(false);
  const [userEmail, setUserEmail] = useState("");
  const tenantProfile = useContext(TenantContext);

  const useNewLogin = useFeatureFlag("use_new_login");

  const { from } = props.location.state || "/dashboard/deals";

  async function onSubmit(data) {
    setSubmitting(true);
    Log.debug("login", "onSubmit");
    try {
      setUserEmail(data.email);
      await getToken(data.email, data.password);
      await dispatch(fetchSelf());
      setSubmitting(false);
      //Redirect to url
      const redirectPath = parseCookie.getToken("__postlogin-url");
      if (redirectPath) {
        document.cookie =
          "__postlogin-url= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
        history.push(decodeURIComponent(redirectPath));
      } else if (from && from.pathname !== "/logout") {
        history.push(from);
      } else {
        history.push("/dashboard");
      }
    } catch (e) {
      setSubmitting(false);
      if (e.response.status === 401) {
        if (
          e?.response?.data?.error?.message === "Account is not fully set up"
        ) {
          setShowVerified(true);
        } else {
          setCredentialError(true);
        }
      } else {
        ToastError(e.response);
      }
    }
  }

  return useNewLogin && !window.location.search.includes("code") ? (
    <AuthRedirect />
  ) : (
    <>
      <Helmet
        bodyAttributes={{ class: "pages-login" }}
        title={`Login | ${tenantProfile.name}`}
      />
      <Transition>
        <Container className="mobile-container">
          {showUnverified ? (
            <Unverified email={userEmail} />
          ) : (
            <>
              <LoginForm
                title={"Log In"}
                onSubmit={onSubmit}
                submitting={submitting}
                credentialError={credentialError}
              />
              <Segment basic inverted={true} textAlign="center">
                <List>
                  <List.Item>
                    {/*<Link to="/account-portal" data-testid={"register-link"}>*/}
                    {/*  Register for an account*/}
                    {/*</Link>*/}
                  </List.Item>
                  <List.Item>
                    <Link to="/forgot-credentials" data-testid={"forgot-link"}>
                      Forgot your password?
                    </Link>
                  </List.Item>
                </List>
              </Segment>
            </>
          )}
        </Container>
      </Transition>
      <BuildInfo />
    </>
  );
};
