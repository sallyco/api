import React, { useContext, useState, useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { Link, useParams, useLocation } from "react-router-dom";

import { toast } from "react-toastify";
import { RootState } from "../rootReducer";

import {
  fetchEntityById,
  generateEntityDocument,
  generateEntityEIN,
  patchEntityById,
} from "../slices/entitySlice";
import { fetchProfileById } from "../slices/profilesSlice";
import { uploadFileToServer } from "../slices/filesSlice";
import { fetchDealById, updateDealById } from "../slices/dealsSlice";
import { fetchCompanyById } from "../slices/companiesSlice";
import { TenantContext } from "../contexts";

import {
  Header,
  Card,
  Icon,
  Button,
  Divider,
  Container,
  Loader,
  Segment,
  List,
  Transition,
} from "semantic-ui-react";
import Document from "../components/Document";
import DocumentItem from "../components/files/DocumentItem";
import DocsSuccess from "../components/deals/DocsSuccess";
import ShortHeader from "../components/ShortHeader";
import Log from "../tools/Log";
import { SubformFileUpload } from "../forms/subforms/SubformFileUpload";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";

export const ReviewDocsPage = () => {
  const dispatch = useDispatch();
  const { dealId } = useParams<{ dealId: string }>();
  const fileInputRef: any = useRef();
  const tenantProfile = useContext(TenantContext);
  Log.debug("tenantProfile", tenantProfile);

  const bankingFeatureEnabled = useFeatureFlag("banking");

  const query = new URLSearchParams(useLocation().search);
  let isGenerate = query.get("generate") ?? false;
  let isRegenerate = query.get("regenerate") ?? false;
  Log.debug("isGenerate", isGenerate);

  const [generated, setGenerated] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  //const [uploadingFile, setUploadingFile] = useState(false);
  const [uploadedFiles, setUploadedFiles]: any = useState([]);

  const [oaId, setOaId] = useState("");
  const [ppmId, setPpmId] = useState("");
  const [subId, setSubId] = useState("");

  const [oaLoaded, setOaLoaded] = useState(false);
  const [ppmLoaded, setPpmLoaded] = useState(false);
  const [subLoaded, setSubLoaded] = useState(false);

  const [isSubmitting, setIsSubmitting] = useState(false);

  Log.debug("id", dealId);

  // DEAL
  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [dispatch, deal, dealId]);

  // ENIITY
  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });
  useEffect(() => {
    if (deal && (!entity || entity.dealId !== dealId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, dealId, entity]);

  // PROFILE
  const profile = useSelector((state: RootState) => {
    return !deal ? undefined : state.profiles.profilesById[deal.profileId];
  });
  useEffect(() => {
    if (deal && (!profile || profile.id !== deal.profileId)) {
      dispatch(fetchProfileById(deal.profileId));
    }
  }, [dispatch, profile, deal]);

  // MASTER
  const masterEntity = useSelector((state: RootState) => {
    return !profile ||
      (!profile.masterEntityId && !tenantProfile.masterEntityId)
      ? undefined
      : state.companies.companiesById[
          profile.masterEntityId
            ? profile.masterEntityId
            : tenantProfile.masterEntityId
        ];
  });
  const manager = useSelector((state: RootState) => {
    return !profile || (!profile.managerId && !tenantProfile.managerId)
      ? undefined
      : state.companies.companiesById[
          profile.managerId ? profile.managerId : tenantProfile.managerId
        ];
  });

  useEffect(() => {
    if (profile && !masterEntity) {
      if (profile.masterEntityId) {
        dispatch(fetchCompanyById(profile.masterEntityId));
      } else if (tenantProfile.masterEntityId) {
        dispatch(fetchCompanyById(tenantProfile.masterEntityId));
      }
    }
    if (profile && !manager) {
      if (profile.managerId) {
        dispatch(fetchCompanyById(profile.managerId));
      } else if (tenantProfile.managerId) {
        dispatch(fetchCompanyById(tenantProfile.managerId));
      }
    }
  }, [dispatch, profile, tenantProfile]);

  async function genDoc(setState, type) {
    const fileId: any = await dispatch(generateEntityDocument(entity.id, type));
    setState(fileId.id);
  }

  function formatTypeOfEntity(str) {
    str = str.split("_");
    for (let i = 0, x = str.length; i < x; i++) {
      str[i] = str[i][0].toUpperCase() + str[i].toLowerCase().substr(1);
    }
    return str.join(" ");
  }

  let calculateCarryPercentage = () => {
    let percentage = Number(deal?.organizerCarryPercentage ?? 0);
    if (
      Array.isArray(deal?.additionalCarryRecipients) &&
      deal?.additionalCarryRecipients?.length > 0
    ) {
      percentage += deal.additionalCarryRecipients.reduce(
        (accl, carry) => accl + (carry.carryPercentage ?? 0),
        0
      );
    }
    return percentage;
  };

  useEffect(() => {
    async function generateAll() {
      if (isGenerate && !generated && deal && entity && profile) {
        setGenerated(true);

        await genDoc(setOaId, "oa");
        await genDoc(setPpmId, "ppm");
        await genDoc(setSubId, "sub");
      }
    }
    if (!profile || !tenantProfile) return;
    if (
      (tenantProfile.masterEntityId || profile.masterEntityId) &&
      !masterEntity
    )
      return;
    if ((tenantProfile.managerId || profile.managerId) && !manager) return;

    generateAll();
  }, [
    dispatch,
    profile,
    deal,
    dealId,
    entity,
    isGenerate,
    generated,
    masterEntity,
    manager,
  ]);

  async function handleClick() {
    setIsSubmitting(true);

    await dispatch(
      patchEntityById(entity.id, {
        entityDocuments: {
          operatingAgreement: oaId,
          privatePlacementMemorandum: ppmId,
          subscriptionAgreement: subId,
        },
        ...(masterEntity && { masterEntityId: masterEntity.id }),
        ...(manager && { managerId: manager.id }),
      })
    );
    if (!isRegenerate) {
      await dispatch(generateEntityEIN(entity));
    }
    //bankingFeatureEnabled && (await dispatch(generateEntityBankAccount(entity)));
    await dispatch(
      updateDealById({
        ...deal,
        status: "OPEN",
      })
    );
    setShowSuccess(true);
    setIsSubmitting(false);
  }

  async function fileChange(e) {
    //setUploadingFiles(true);
    Log.debug("target ", e.target);
    Log.debug("filename ", e.target.value);
    Log.debug("file ", e.target.files);

    const file = e.target.files[0];
    // eslint-disable-next-line no-control-regex
    if (/[^\u0000-\u00ff]/.test(file.name)) {
      toast.error(
        `File name should not contain Unicode characters. '${file.name}' `
      );
      return;
    }

    const data = new FormData();
    await data.append("files", file);
    Log.debug("data ", data);
    const files: any = await dispatch(uploadFileToServer(data));
    Log.debug("files ", files);
    const fileIds = files.map((file) => file.id);
    Log.debug("filesIds ", fileIds);
  }

  return (
    <>
      <Helmet
        title={`Review Fund Documents - Create Deal | ${tenantProfile.name}`}
      ></Helmet>

      <ShortHeader title={"Review & Approve"} />
      <Divider hidden />
      <Transition animation={"fade up"}>
        <Container text className="margin-bottom-container">
          {showSuccess ? (
            <>
              <DocsSuccess />
              <Divider hidden />
              <Container className="mobile-container">
                <Button
                  as={Link}
                  fluid
                  to={`/deals/${dealId}/invite-investors`}
                  secondary
                  content="Continue"
                />
              </Container>
            </>
          ) : (
            <>
              <Header as="h2" inverted textAlign="center">
                Fund Documents
              </Header>

              <Divider hidden />
              {!entity ? (
                <Loader active inline="centered" />
              ) : (
                <>
                  <Card.Group itemsPerRow="three" centered stackable>
                    <Document
                      fileId={oaId}
                      loadingTitle={"Operating Agreement"}
                      setIsLoaded={setOaLoaded}
                    />
                    <Document
                      fileId={ppmId}
                      loadingTitle={"Private Placement Memorandum"}
                      setIsLoaded={setPpmLoaded}
                    />
                    <Document
                      fileId={subId}
                      loadingTitle={"Subscription Agreement"}
                      setIsLoaded={setSubLoaded}
                    />
                  </Card.Group>

                  <Divider hidden />

                  <Segment textAlign="center">
                    Review the above documents to ensure it reflects the terms
                    of your deal.
                  </Segment>
                  <Button
                    secondary
                    fluid
                    onClick={handleClick}
                    loading={isSubmitting}
                    {...((!oaId ||
                      !ppmId ||
                      !subId ||
                      !oaLoaded ||
                      !ppmLoaded ||
                      !subLoaded) && { disabled: true })}
                    content="Approve & Continue"
                  />

                  <Divider inverted />

                  {uploadedFiles.length > 0 && (
                    <Segment inverted textAlign="center">
                      <Transition.Group
                        as={List}
                        duration={200}
                        divided
                        verticalAlign="middle"
                        inverted
                        relaxed
                      >
                        {uploadedFiles.map((fileId) => (
                          <DocumentItem key={fileId} fileId={fileId} />
                        ))}
                      </Transition.Group>
                    </Segment>
                  )}

                  <SubformFileUpload
                    onSuccess={async (fileIds) => {
                      const upload = deal.files
                        ? [...deal.files, ...fileIds]
                        : fileIds;
                      Log.debug("newfiles", upload);

                      await dispatch(
                        updateDealById({
                          ...deal,
                          files: upload,
                        })
                      );

                      setUploadedFiles([...uploadedFiles, ...fileIds]);

                      toast.success(`File uploaded to deal`);
                    }}
                  >
                    {({ triggerFileUpload, uploading }) => (
                      <Button
                        inverted
                        fluid
                        loading={uploading}
                        onClick={triggerFileUpload}
                      >
                        <Icon name="file alternate outline" /> Upload Additional
                        Files
                      </Button>
                    )}
                  </SubformFileUpload>
                  <Segment
                    basic
                    inverted
                    textAlign="center"
                    className="no-padding"
                  >
                    It may be useful to upload a pitch deck and other supporting
                    documentation for your investors to review.
                  </Segment>
                </>
              )}
            </>
          )}
        </Container>
      </Transition>
    </>
  );
};
