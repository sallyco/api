import React, { useContext, useState, useEffect } from "react";
import org_image from "../assets/images/org-inv.png";
import founder_image from "../assets/images/founder.png";
import dealer_image from "../assets/images/dealer.png";
import {
  Divider,
  Header,
  Container,
  Image,
  Transition,
  Grid,
  Segment,
  Card,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { TenantContext } from "../contexts";
import { FeatureFlag } from "../components/featureflags/FeatureFlags";
import { Box } from "@mui/material";

export const AccountChoicePage = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet
        title={`Choose Account Type - Register | ${tenantProfile.name}`}
      />
      <Transition animation={"fade"}>
        <Container textAlign="center" className="mobile-container">
          <Divider hidden />
          <Image
            src={
              tenantProfile.inverted
                ? tenantProfile.assets.images.logoInverted
                : tenantProfile.assets.images.logo
            }
            size="medium"
            centered
          />
          <>
            <FeatureFlag name={"signup"}>
              <Header as="h2" inverted icon>
                Create An Account
              </Header>
              <Box>
                <Header size="small" inverted icon>
                  I will use Glassboard as a...
                </Header>
              </Box>
              <Grid textAlign="center">
                <Grid.Row>
                  <Grid.Column width={5}>
                    <Card>
                      <Image
                        src={org_image.src}
                        as={Link}
                        size="medium"
                        rounded
                        to={`/register`}
                      />
                    </Card>
                    <Segment inverted>Deal Organizer/ Investor</Segment>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <FeatureFlag name={"founder"}>
                      <Card>
                        <Image
                          src={founder_image.src}
                          as={Link}
                          size="medium"
                          rounded
                          to={`/register/founder`}
                        />
                      </Card>
                      <Segment inverted>Company Founder</Segment>
                    </FeatureFlag>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <Card>
                      <Image
                        src={dealer_image.src}
                        as={Link}
                        size="medium"
                        rounded
                        to={`/register/dealer`}
                      />
                    </Card>
                    <Segment inverted>Art dealer</Segment>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </FeatureFlag>
            <FeatureFlag name={"signup"} invertedState={true}>
              <Header size="small" inverted icon>
                Account signup is currently invite-only
              </Header>
              <Header size="small" inverted>
                Please Contact your Organizer or Platform Administrator for an
                account
              </Header>
            </FeatureFlag>
            <Divider hidden />
            <Link to="/">
              <Header inverted as="h3" style={{ textDecoration: "underline" }}>
                Already have an account?
              </Header>
            </Link>
          </>
        </Container>
      </Transition>
    </>
  );
};
