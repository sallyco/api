import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import assessmentImage from "../assets/images/kycaml.png";
import { API, useRequest } from "../api/swrApi";
import { updateAssetById } from "../slices/assetsSlice";
import { mutate } from "swr";
import _ from "lodash";

import {
  Button,
  Divider,
  Header,
  Loader,
  Image,
  List,
  Transition,
  Icon,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { Link, useHistory } from "react-router-dom";
import Log from "../tools/Log";
import { TenantContext } from "../contexts";
import { Box, Container, Paper } from "@mui/material";

import { applicationQuestionaire } from "../tools/questionaireSchemas";
import { Questionaire } from "@packages/gbt-ui";

export const FoundersAssessmentPage = () => {
  const dispatch = useDispatch();
  const [step, setStep] = useState(-1);

  const tenantProfile = useContext(TenantContext);

  const isLoading = false;

  const { data: companies, mutate } = useRequest(API.ASSETS);
  function customMerger(objValue, srcValue) {
    return {
      title: srcValue.title,
      response: objValue,
    };
  }

  async function onSubmit(formdata) {
    if (companies.data.length > 0) {
      //TODO (Andrew): this isn't really right, but needed for the Varia deadline
      const entryName = applicationQuestionaire[step].title;
      const answers = _.mergeWith(
        formdata,
        applicationQuestionaire[step].properties,
        customMerger
      );

      const data = {
        ...companies.data[0],
        foundersAssessment: {
          ...companies.data[0].foundersAssessment,
          [entryName]: answers,
        },
      };
      await dispatch(updateAssetById(data));
      await mutate(API.ADD_FILTER(API.ASSETS, { fields: ["id"] }));
      await mutate();
    }
    setStep(step + 1);
  }

  return (
    <>
      <Helmet
        title={`Select Profile Type - Profiles | ${tenantProfile.name}`}
      />

      {isLoading ? (
        <Container maxWidth="sm">
          <Box
            display={"flex"}
            flexDirection={"column"}
            alignItems={"center"}
            justifyContent={"center"}
            mt={2}
          >
            <Divider hidden />
            <Loader size="massive" inverted inline active />
          </Box>
        </Container>
      ) : step < 0 ? (
        <Transition transitionOnMount animation={"fade up"}>
          <Container maxWidth="sm">
            <Image src={assessmentImage.src} size="medium" centered />
            <Header as="p" inverted icon>
              Thank you for your interest in the SAGES 2022 Shark Tank
              competition, powered by Varia Ventures!
            </Header>
            <Paper>
              <Box p={2}>
                Please take the time to read through the previous page’s
                eligibility requirements prior to submitting your application.
                As part of the application, you will be required to submit a
                $175 application fee. Applications submitted without the fee
                will not be considered in this competition.Your application
                responses and materials submitted in the competition will be
                reviewed by the SAGES and Varia Ventures judging committees. All
                members of the judging committee conducting due diligence on
                your company and product will be required to sign a
                Non-Disclosure Agreement prior to their participation. Please
                carefully read the other terms and conditions of this
                competition before applying, available{" "}
                <a
                  href="https://www.sages2022.org/shark-tank-2022/"
                  style={{ color: "blue" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  here
                </a>
                . There are multiple stages of this competition, and we
                encourage you to take note of the timeline we have outlined on
                the previous page.All questions are required to be answered,
                however if you do not know an answer, it is okay to answer as
                such. If you have any remaining questions about the process,
                please contact{" "}
                <a href="mailto:nicolevh@sages.org" style={{ color: "blue" }}>
                  Nicole Von Husen
                </a>
              </Box>
            </Paper>
            <List relaxed>
              <List.Item>
                <Button onClick={() => setStep(0)} secondary fluid icon>
                  Proceed
                </Button>
              </List.Item>
            </List>
          </Container>
        </Transition>
      ) : step >= applicationQuestionaire.length ? (
        <Transition transitionOnMount animation={"fade up"}>
          <Container maxWidth="sm">
            <Image src={assessmentImage.src} size="medium" centered />
            <Paper>
              <Box p={2}>
                Please click here to submit your payment fee:{" "}
                <a
                  href="https://www.sages2022.org/shark-tank-application-fee/"
                  style={{ color: "blue" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  https://www.sages2022.org/shark-tank-application-fee/.
                </a>{" "}
                Your application will not be considered complete until payment
                is processed.
              </Box>
            </Paper>
            <List relaxed>
              <List.Item>
                <Button
                  as={Link}
                  to={`/dashboard/assets/companies`}
                  secondary
                  fluid
                  icon
                >
                  Return Home
                </Button>
              </List.Item>
            </List>
          </Container>
        </Transition>
      ) : (
        <Transition transitionOnMount animation={"fade up"}>
          <Container>
            <Paper elevation={3}>
              <Box p={3} pb={1}>
                <Questionaire
                  schema={applicationQuestionaire[step]}
                  submitAction={(formData) => onSubmit(formData)}
                />
              </Box>
            </Paper>
          </Container>
        </Transition>
      )}
    </>
  );
};
