import React from "react";
import { Divider, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Subscription } from "./../api/subscriptionsApi";
import { DB_STATUSES } from "./../tools/subscriptionStatuses";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../rootReducer";

export default function DealFundingButtons({
  subscription,
  dealStatus,
  handleMarkFundsSentManually,
}: {
  subscription: Subscription;
  dealStatus: string;
  handleMarkFundsSentManually: () => {};
}) {
  const isManuallySent =
    subscription.status === DB_STATUSES.FUNDS_IN_TRANSIT_MANUAL;

  const { isSubmitting } = useSelector(
    (state: RootState) => state.subscriptions
  );

  return (
    <>
      {isManuallySent && (
        <p>
          You have indicated that you will send funds manually off-platform for
          this investment. You may still add a funding source by clicking below
        </p>
      )}
      <Button
        fluid
        color="green"
        as={Link}
        to={`/subscriptions/${subscription.id}/funding-select`}
        disabled={dealStatus === "DRAFT"}
        content="Add Funding Source"
      />
      <Divider hidden fitted />
      {!isManuallySent && (
        <Button
          fluid
          primary
          loading={isSubmitting}
          onClick={handleMarkFundsSentManually}
          disabled={dealStatus === "DRAFT"}
          content="I have sent the Funds"
        />
      )}
    </>
  );
}
