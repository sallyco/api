import React, { useContext, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useLocation, useHistory, Prompt } from "react-router-dom";
import { Helmet } from "react-helmet";
import { RootState } from "../rootReducer";
import { Container, Divider, Transition } from "semantic-ui-react";
import ShortHeader from "../components/ShortHeader";
import IndividualProfileForm from "../forms/profiles/organizer/IndividualProfileForm";
import EntityProfileForm from "../forms/profiles/organizer/EntityProfileForm";
import SubscriberProfile from "../components/profiles/SubscriberProfile";
import { fetchSubscriptionById } from "../slices/subscriptionsSlice";
import Log from "../tools/Log";
import { TenantContext } from "../contexts";
import { fetchSelf } from "../slices/usersSlice";
import { useProfileSubmit } from "../forms/profiles/profileHooks";
import FounderProfile from "../components/profiles/FounderProfile";

export const CreateProfilePage = () => {
  let history = useHistory();
  Log.debug("history", history);
  const dispatch = useDispatch();
  const { type } = useParams<{ type: string }>();
  const tenantProfile = useContext(TenantContext);

  const query = new URLSearchParams(useLocation().search);
  const subscriptionId = query.get("subscriptionId") ?? "";
  const redirect = query.get("redirect") ?? "";

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );
  const { self } = useSelector((state: RootState) => state.users);
  const { onSubmit } = useProfileSubmit({ type, redirectTo: redirect });

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  useEffect(() => {
    if (
      (!subscription || subscription.id !== subscriptionId) &&
      subscriptionId
    ) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // const { isSubmitting } = useSelector((state: RootState) => state.profiles);

  const [isSubmitting, setIsSubmitting] = useState(false);

  Log.debug("type", type);

  const typeToTitleMap = {
    individual: "Organizer Profile",
    subscriber: "Investor Profile",
    founder: "Founder Profile",
    entity: "Organizer Profile",
  };

  return (
    <>
      <Helmet
        title={`Create ${typeToTitleMap[type]} - Profiles | ${tenantProfile.name}`}
      />
      <ShortHeader title={`Create your ${typeToTitleMap[type]}`} />
      {/*
      <Prompt
        message={
          "Your Profile has not been created yet. Leaving now will require you to fill out the form again."
        }
        when={!isSubmitting}
      />
*/}
      <Divider hidden />
      <Transition animation="fade left">
        <Container text className="margin-bottom-container">
          {type === "entity" && (
            <EntityProfileForm
              onSubmit={async (e) => {
                setIsSubmitting(true);
                await onSubmit(e);
                setIsSubmitting(false);
              }}
              submitting={isSubmitting}
              useAccountDefaultValues={true}
            />
          )}
          {type === "individual" && (
            <IndividualProfileForm
              onSubmit={async (e) => {
                setIsSubmitting(true);
                await onSubmit(e);
                setIsSubmitting(false);
              }}
              submitting={isSubmitting}
            />
          )}
          {type === "subscriber" && (
            <SubscriberProfile
              onSubmit={async (e) => {
                setIsSubmitting(true);
                await onSubmit(e);
                setIsSubmitting(false);
              }}
              submitting={isSubmitting}
            />
          )}
          {type === "founder" && (
            <FounderProfile
              onSubmit={async (e) => {
                setIsSubmitting(true);
                await onSubmit(e);
                setIsSubmitting(false);
              }}
              submitting={isSubmitting}
            />
          )}
        </Container>
      </Transition>
    </>
  );
};
