import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { CompanyList } from "../components/companies/CompanyList";
import { TenantContext } from "../contexts";
import { API, useRequest } from "../api/swrApi";

export const CompanyListPage = () => {
  const tenantProfile = useContext(TenantContext);

  const {
    data: companies,
    error,
    isValidating,
  } = useRequest(
    API.ADD_FILTER(API.ASSETS, {
      fields: ["id"],
    })
  );

  return (
    <>
      <Helmet title={`Companies - Dashboard | ${tenantProfile.name}`} />
      {!isValidating && (
        <>
          {!error && companies?.data ? (
            <CompanyList companies={companies.data} />
          ) : (
            <></>
          )}
        </>
      )}
    </>
  );
};
