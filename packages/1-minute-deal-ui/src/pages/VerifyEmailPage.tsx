import React, { useEffect, useState } from "react";
import { verifyEmail } from "../api/usersApi";
import { toast } from "react-toastify";
import { useHistory, useParams } from "react-router-dom";
import { Loader, Button } from "semantic-ui-react";
import Log from "../tools/Log";

export const VerifyEmail = () => {
  const { email } = useParams<{ email: string }>();
  var [loading, setLoading] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (email) {
      setLoading(true);
      verifyEmail({ email: email })
        .then(() => {
          setLoading(false);
          toast.success("Your email has been verified");
          history.push("/");
        })
        .catch((err) => {
          setLoading(false);
          toast.error("An error has occurred when verifying your email", {
            autoClose: false,
            onClose: () => {
              history.push("/");
            },
          });
        });
    }
  }, []);

  return (
    <div
      style={{
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      data-testid={"verify-email"}
    >
      {loading ? (
        <Loader
          active
          inline="centered"
          inverted
          size="massive"
          content={"Verifying your Email"}
          data-testid={"verify-email-loader"}
        />
      ) : (
        <Button
          secondary
          content={"Return to Login Page"}
          data-testid={"verify-email-button"}
          onClick={() => {
            window.location.replace("/#/logout");
          }}
        />
      )}
    </div>
  );
};
