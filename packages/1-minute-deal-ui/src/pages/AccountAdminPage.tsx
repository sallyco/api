import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import {
  Loader,
  Divider,
  Table,
  Transition,
  Container,
  Button,
} from "semantic-ui-react";
import { RootState } from "../rootReducer";
import { TenantContext } from "../contexts";
import { fetchSelf, fetchUserById, fetchUsersList } from "../slices/usersSlice";
import { impersonateUser } from "../api/usersApi";

export const AccountAdminPage = () => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const { isLoading, error, usersById, self } = useSelector(
    (state: RootState) => state.users
  );

  useEffect(() => {
    if (!isLoading && !error && !usersById) {
      dispatch(fetchUsersList());
    }
  }, [error, dispatch, usersById, isLoading]);

  useEffect(() => {
    if (!isLoading && !error && !self) {
      dispatch(fetchSelf());
    }
  }, [error, dispatch, self, isLoading]);

  useEffect(() => {
    if (
      !isLoading &&
      !error &&
      self?.attributes?.accountAdminId &&
      !usersById[self.attributes.accountAdminId]
    ) {
      dispatch(fetchUserById(self.attributes.accountAdminId));
    }
  });

  async function startImpersonateSession(userId) {
    await impersonateUser(tenantProfile.id, userId);
  }

  return (
    <>
      <Helmet
        title={`Account Administration - Dashboard | ${tenantProfile.name}`}
      ></Helmet>
      {isLoading || !usersById ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <Transition>
          <Container className="mobile-container">
            <Table
              celled
              size="small"
              selectable
              striped
              compact="very"
              unstackable
            >
              <Table.Body>
                {Object.values(usersById).map((user) => (
                  <Table.Row key={user.id}>
                    <Table.Cell width={"13"}>
                      {user.firstName} {user.lastName}
                    </Table.Cell>
                    <Table.Cell width={"3"}>
                      <Button
                        primary
                        compact
                        content="Impersonate"
                        onClick={() => startImpersonateSession(user.id)}
                      />
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </Container>
        </Transition>
      )}
    </>
  );
};
