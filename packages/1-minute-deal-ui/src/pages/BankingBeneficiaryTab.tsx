import { Formik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { Button, Container, Divider, Form, Header } from "semantic-ui-react";
import { toast } from "react-toastify";
import { Alert } from "@mui/material";

import { FormField } from "../forms/common/FormField";
import SubformAddress, {
  validationSchema,
  defaultValues,
} from "../forms/subforms/SubformAddress";
import SubformPhone, {
  validationSchema as phoneValidation,
} from "../forms/subforms/SubformPhone";
import {
  BankingBeneficiary,
  BankingBeneficiaryRequest,
  postBankingBeneficiary,
} from "../api/bankingApi";
import { getCodeByStateName } from "../tools/stateISO3166-2";

export default function BankingBeneficiaryTab({
  onSuccessHandler = (beneficiary: BankingBeneficiary) => {},
  onErrorHandler = () => {},
}) {
  const history = useHistory();
  const [failureMessage, setFailureMessage] = useState<null | string>();
  const [submitting, setSubmitting] = useState(false);
  const validation = Yup.object().shape({
    nameOnAccount: Yup.string().trim().required(),
    routingNumber: Yup.string()
      .length(9, "Must be a 9 digit routing number")
      .test("Digits", "Only numbers are allowed", (value) =>
        /^\d+$/.test(value)
      )
      .required(),
    accountNumber: Yup.string().required(),
    verifyAccountNumber: Yup.string()
      .oneOf([Yup.ref("accountNumber")], "Account numbers must match")
      .required(),
    bankName: Yup.string().required(),
    bankAddress: Yup.object().shape({
      ...validationSchema(),
      address1: Yup.string()
        .min(2)
        .required("Address Line 1 is Required")
        .max(35, "Address is limited to 35 characters"),
    }),
    name: Yup.string().required(),
    accountAddress: Yup.object().shape({
      ...validationSchema(),
      address1: Yup.string()
        .min(2)
        .required("Address Line 1 is Required")
        .max(35, "Address is limited to 35 characters"),
    }),
    email: Yup.string().email().required(),
    ...phoneValidation({ isRequired: false }),
  });

  async function onSubmit(data) {
    const { bankAddress, accountAddress, fixedCountry, bankName } = data;

    const submitData: BankingBeneficiaryRequest = {
      ...data,
      bankAddress: {
        street_1: bankAddress?.address1,
        street_2: bankAddress?.address2,
        city: bankAddress?.city,
        postal_code: bankAddress?.postalCode,
        region: getCodeByStateName(bankAddress?.state),
        country: fixedCountry ? "US" : bankAddress?.country,
      },
      addressOnAccount: {
        street_1: accountAddress?.address1,
        street_2: accountAddress?.address2,
        city: accountAddress?.city,
        postal_code: accountAddress?.postalCode,
        region: getCodeByStateName(accountAddress?.state),
        country: fixedCountry ? "US" : accountAddress?.country,
      },
    };

    try {
      setSubmitting(true);
      const result = await postBankingBeneficiary(submitData);
      setSubmitting(false);
      toast.success("Beneficiary was created");
      onSuccessHandler(result);
    } catch (err) {
      const e: any = err;
      setSubmitting(false);
      setFailureMessage(
        `Beneficiary was not saved: \n${
          e?.response?.data?.error?.message ?? e.message
        }`
      );
      onErrorHandler();
    }
  }

  return (
    <>
      <Container className="text">
        <Divider hidden />
        <Formik
          initialValues={{
            name: "",
            bankName: "",
            nameOnAccount: "",
            routingNumber: "",
            accountNumber: "",
            verifyAccountNumber: "",
            bankAddress: defaultValues,
            accountAddress: defaultValues,
            fixedCountry: true,
          }}
          onSubmit={onSubmit}
          validationSchema={validation}
        >
          {(props) => (
            <Form onSubmit={props.handleSubmit} className="dark-labels">
              <FormField
                name="name"
                component={Form.Input}
                label="Beneficiary Name"
                placeholder="Name"
                required
              />
              <FormField
                name="email"
                component={Form.Input}
                label="Recipient Email"
                placeholder="beneficiary@email.com"
                required
              />
              <SubformPhone />

              <Divider hidden />
              <Header content="Bank Information" />
              <FormField
                name="bankName"
                component={Form.Input}
                label="Bank Name"
                placeholder="My Bank"
                required
              />
              <FormField
                name={"routingNumber"}
                placeholder={"Routing Number"}
                label="Routing Number"
                required
              />
              <SubformAddress
                namespace="bankAddress"
                fieldsRequired={true}
                {...props}
              />

              <Divider hidden />
              <Header content="Account Information" />
              <FormField
                name="nameOnAccount"
                component={Form.Input}
                label="Name on Account"
                placeholder="Account Name"
                required
              />
              <FormField
                name={"accountNumber"}
                placeholder={"Account Number"}
                label="Account Number"
                required
              />
              <FormField
                name={"verifyAccountNumber"}
                placeholder={"Verify Account Number"}
                required
              />
              <SubformAddress
                namespace="accountAddress"
                fieldsRequired={true}
                {...props}
              />
              {failureMessage && (
                <>
                  <Alert
                    severity={"error"}
                    onClose={() => setFailureMessage(null)}
                  >
                    {failureMessage}
                  </Alert>
                  <Divider hidden />
                </>
              )}
              <Button
                loading={submitting}
                disabled={submitting}
                type="submit"
                primary
                fluid
                content="Add Beneficiary"
              />
            </Form>
          )}
        </Formik>
      </Container>
    </>
  );
}
