/* eslint-disable react/display-name */
import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import NoCompnayDoc from "../components/companies/NoCompnayDoc";
import { Divider } from "semantic-ui-react";
import { TenantContext } from "../contexts";

export const CompanyDocPage = () => {
  const tenantProfile = useContext(TenantContext);

  return (
    <>
      <Helmet title={`Documents | ${tenantProfile.name}`} />
      <Divider hidden />
      <NoCompnayDoc />
    </>
  );
};
