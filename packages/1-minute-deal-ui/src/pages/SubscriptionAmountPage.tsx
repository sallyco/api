import React, { useContext, useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams, useLocation } from "react-router-dom";
import { currencyFormat } from "../tools/helpers";
import { Helmet } from "react-helmet";

import { RootState } from "../rootReducer";
import * as Yup from "yup";
import numeral from "numeral";

import Log from "../tools/Log";

import {
  fetchSubscriptionById,
  updateSubscriptionById,
} from "../slices/subscriptionsSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { insertNotification } from "../slices/notificationsSlice";
import { fetchSelf } from "../slices/usersSlice";

import {
  Header,
  Message,
  Image,
  Button,
  Divider,
  Loader,
  Container,
  Segment,
  Form,
  Transition,
  Icon,
  Popup,
  Label,
  Grid,
} from "semantic-ui-react";
import ShortHeader from "../components/ShortHeader";
import { Formik } from "formik";
import money from "../assets/images/money.png";
import { TenantContext } from "../contexts";
import SubformCurrency from "../forms/subforms/SubformCurrency";
import { FormField } from "../forms/common/FormField";
import { fetchEntityById } from "../slices/entitySlice";
import { useSubscriptionMeta } from "../hooks/subscriptionHooks";

export const SubscriptionAmountPage = () => {
  const dispatch = useDispatch();
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const [overMax, setOverMax] = useState(false);
  const tenantProfile = useContext(TenantContext);
  const { meta, dealIsAvailable } = useSubscriptionMeta(subscriptionId);
  const query = new URLSearchParams(useLocation().search);
  const [minInvestmentAmount, setMinInvestmentAmount] = useState(0);
  const amount = query.get("amount") ?? "";

  let history = useHistory();

  const { isLoading: subscriptionsLoading, isSubmitting } = useSelector(
    (state: RootState) => state.subscriptions
  );

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const { self } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  useEffect(() => {
    if (subscription && entity) {
      setMinInvestmentAmount(
        subscription?.amount && subscription.transactionIds
          ? subscription.amount
          : entity?.minInvestmentAmount
      );
    }
  }, [subscription, entity]);

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !subscription.profileId) {
      history.replace(
        `/profiles/create/subscriber?subscriptionId=${subscription.id}`
      );
    } else {
      if (subscription && !deal) {
        dispatch(fetchDealById(subscription.dealId));
      }
      if (subscription && deal && !entity) {
        dispatch(fetchEntityById(deal.entityId));
      }
    }
  }, [dispatch, subscription, deal, entity]);

  async function onSubmit(data) {
    Log.debug("data", data);

    const newAmount = Number(data.amount.replace(/\D+/g, ""));
    const previousAmount = subscription?.amount ?? 0;
    await dispatch(
      updateSubscriptionById({
        ...subscription,
        amount: newAmount,
        fundingInfo: {
          bankName: data.wiringBankName,
          isCustomerOfBank: data.wiringBankSub,
          isFATFBank: data.wiringBankInUS,
        },
      })
    );

    await dispatch(
      insertNotification({
        tenantId: tenantProfile.id,
        profileId: deal.profileId,
        event: {
          type: "deal",
          id: deal.id,
        },
        category: "DEAL_STATUS",
        message: `An Investor, ${self.firstName} ${
          self.lastName
        }, has committed ${currencyFormat(data.amount)} to Deal ${deal.name}`,
        acknowledged: false,
      })
    );
    history.push(
      `/subscriptions/${subscriptionId}/sign-documents?generate=true&wiringBankName=${
        data.wiringBankName
      }&wiringBankInUS=${data.wiringBankInUS}&wiringBankSub=${
        data.wiringBankSub
      }&autoresign=${
        Object.prototype.hasOwnProperty.call(subscription, "signature") &&
        subscription.signature !== "" &&
        subscription.signature !== null &&
        subscription.signature !== undefined
      }${
        subscription?.amount && subscription.amount > 0
          ? `&previousAmount=${previousAmount}`
          : ""
      }`
    );
  }

  const yupShape = {
    amount: Yup.number()
      .transform(function (value, originalvalue) {
        return Number(originalvalue.replace(/\D+/g, ""));
      })
      .min(
        minInvestmentAmount,
        `Subscription amount must be at least ${numeral(
          minInvestmentAmount
        ).format("$0,0.00")}`
      )
      .required("Investment Amount is Required"),
    wiringBankName: Yup.string().trim().required("Bank Name is Required"),
    variaLoan: Yup.bool().oneOf(
      [true],
      "You must answer yes to this question."
    ),
  };

  if (dealIsAvailable && meta?.maxCommitmentAmount > 0) {
    yupShape.amount = yupShape.amount.max(
      meta.maxCommitmentAmount + (subscription?.amount ?? 0),
      `The deal is almost full! Only ${numeral(
        meta.maxCommitmentAmount + (subscription?.amount ?? 0)
      ).format("$0,0.00")} is left for subscriptions to ${deal?.name}.`
    );
  }

  const validation = Yup.object().shape(yupShape);

  const handleChange = (data: any) => {
    if (deal?.targetRaiseAmount) {
      setOverMax(data.amount > deal.targetRaiseAmount);
    }
  };

  return (
    <>
      <Helmet title={`Amount - Subscriptions | ${tenantProfile.name}`}></Helmet>

      <ShortHeader title={deal?.name} loading={dealsLoading} />
      {dealsLoading || subscriptionsLoading ? (
        <>
          <Divider hidden />
          <Loader active inverted inline="centered" size="massive" />
        </>
      ) : (
        <>
          <Transition>
            <Container textAlign="left" className="mobile-container">
              <Divider hidden />
              <Image src={money.src} size="small" centered />
              <Header as="h3" textAlign="center" inverted icon>
                Enter a subscription amount
              </Header>
              <Formik
                initialValues={{
                  amount: amount ?? "",
                  variaLoan: tenantProfile.id === "varia" ? false : true,
                }}
                validationSchema={validation}
                onSubmit={onSubmit}
              >
                {(props) => (
                  <Form onSubmit={props.handleSubmit}>
                    <SubformCurrency
                      name={"amount"}
                      id={"amount"}
                      label={"Subscription Amount"}
                      onChange={(e) => handleChange}
                      placeholder={numeral(minInvestmentAmount).format("$0,0")}
                    />
                    <FormField
                      name={"wiringBankName"}
                      id={"wiringBankName"}
                      label={
                        "Name of the bank from which you intend to send the funds from"
                      }
                      placeholder={"Name of Bank"}
                      required
                    />
                    <Grid stretched relaxed>
                      <Grid.Column width={11} stretched>
                        <FormField
                          inline
                          name={"wiringBankInUS"}
                          id={"wiringBankInUS"}
                          label={
                            'Is the Bank located in the United States or another "FATF COUNTRY"?'
                          }
                          component={Form.Checkbox}
                        />
                      </Grid.Column>
                      <Grid.Column textAlign={"right"}>
                        <Popup
                          trigger={<Icon color="grey" name="question circle" />}
                          content="The current list of countries that are members of the Financial Action Task Force on Money Laundering
(each an “FATF Country”) are: Argentina, Australia, Austria, Belgium, Brazil, Canada, China, Denmark, Finland,
France, Germany, Greece, Hong Kong, Iceland, India, Ireland, Italy, Japan, Korea, Luxembourg, Malaysia, Mexico,
Kingdom of the Netherlands, New Zealand, Norway, Portugal, Russian Federation, Singapore, South Africa, Spain,
Sweden, Switzerland, Turkey, United Kingdom, and the United States. The list of FATF Countries may be
expanded to include future FATF members and FATF compliant countries, as appropriate."
                          position="right center"
                        />
                      </Grid.Column>
                    </Grid>
                    <Divider hidden />
                    <FormField
                      name={"wiringBankSub"}
                      id={"wiringBankSub"}
                      label={
                        "If checked yes above, are you a customer of the Bank?"
                      }
                      component={Form.Checkbox}
                    />
                    {tenantProfile.id === "varia" && (
                      <FormField
                        name={"variaLoan"}
                        id={"variaLoan"}
                        label={
                          "I did not receive a loan from the company, or anyone associated with the company, to obtain an investment interest in the company"
                        }
                        component={Form.Checkbox}
                      />
                    )}
                    <Segment inverted textAlign="center" size="small">
                      {subscription && entity && !subscription.amount && (
                        <>
                          The minimum investment is{" "}
                          {numeral(entity?.minInvestmentAmount).format(
                            "$0,0.00"
                          )}
                        </>
                      )}
                      {subscription && subscription.amount && (
                        <>
                          You are currently committed for{" "}
                          {numeral(subscription.amount).format("$0,0.00")}
                          {props.values.amount &&
                            Number(props.values.amount.replace(/\D+/g, "")) !==
                              subscription.amount && (
                              <>
                                , continue to change the amount to{" "}
                                {numeral(props.values.amount).format("$0,0.00")}
                              </>
                            )}
                        </>
                      )}
                    </Segment>
                    <Transition.Group animation={"fade"}>
                      {overMax && (
                        <Message negative compact size="small">
                          Your committed investment amount exceeds the
                          investment available on the deal so you entire amount
                          may not be able to be accepted
                        </Message>
                      )}
                    </Transition.Group>
                    <Button
                      secondary
                      type="submit"
                      disabled={!props.dirty || !props.isValid}
                      loading={isSubmitting}
                      fluid
                    >
                      {subscription &&
                      Object.prototype.hasOwnProperty.call(
                        subscription,
                        "signature"
                      ) &&
                      subscription.signature !== "" &&
                      subscription.signature !== null &&
                      subscription.signature !== undefined
                        ? "Update Documents"
                        : "Review & Sign Docs"}
                    </Button>
                  </Form>
                )}
              </Formik>
            </Container>
          </Transition>
        </>
      )}
    </>
  );
};
