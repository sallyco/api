import React from "react";
import { SubscriptionsList } from "../components/subscriptions/SubscriptionsList";
import { API, useRequest } from "../api/swrApi";
import { Box } from "@mui/material";

export const SigningSummaryPage = () => {
  const { data: subscriptions } = useRequest(API.SIGNING_REQUESTS);

  return (
    <>
      <Box m={2}>
        {subscriptions && (
          <SubscriptionsList
            subscriptions={subscriptions.data}
            context={"signer"}
          />
        )}
      </Box>
    </>
  );
};
