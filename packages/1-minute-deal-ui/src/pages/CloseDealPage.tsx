import React, { useContext, useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { useHistory, useParams } from "react-router-dom";
import { TenantContext } from "../contexts";
import * as Yup from "yup";
import { currencyFormat } from "../tools/helpers";
import {
  SignaturePad,
  ContentLoader,
  StatementTable,
  AuditListBackend,
} from "@packages/gbt-ui";
import { useFeesStatementSWR, useBankBalanceSWR } from "../swrConnector/deals";

import {
  Form,
  Icon,
  Loader,
  Button,
  Divider,
  Container,
  Table,
  Header,
  Segment,
  Card,
  Grid,
  Transition,
  Modal,
  Input,
} from "semantic-ui-react";

import { Formik } from "formik";
import { FormField } from "../components/FormField";
import { toast } from "react-toastify";
import { RootState } from "../rootReducer";
import {
  fetchSubscriptionsListByDeal,
  updateSubscriptionById,
} from "../slices/subscriptionsSlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchClosesList, insertClose } from "../slices/closesSlice";
import { insertNotification } from "../slices/notificationsSlice";

import NumberFormat from "react-number-format";

import ShortHeader from "../components/ShortHeader";
import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import { Subscription } from "../api/subscriptionsApi";
import SignatureForm, {
  validationSchema as SignatureFormValidation,
} from "../forms/signatures/SignatureForm";
import FeesStatement from "../components/deals/FeesStatement";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";
import { isSeriesEntityOfGlassboardMasterII } from "../tools/closeHelpers";
import { fetchEntityById } from "../slices/entitySlice";
import { API, doChange } from "../api/swrApi";
import { CircularProgress, Paper, Typography } from "@mui/material";

export function CloseDealPage() {
  const showFeeStatementsFeature = useFeatureFlag("show_fee_statements");
  const outboundWiresAreEnabled = useFeatureFlag("outbound_wires");
  const dispatch = useDispatch();
  const { dealId } = useParams<{ dealId: string }>();
  let history = useHistory();
  const [showEdit, setShowEdit] = useState(false);
  const [showRemove, setShowRemove] = useState(false);
  const [selectedSub, setSelectedSub]: any = useState({});
  const [closingSubscriptions, setClosingSubscriptions] = useState<
    Subscription[]
  >([]);
  const [canAccept, setCanAccept] = useState(false);

  const tenantProfile = useContext(TenantContext);

  //const { isLoading: dealsLoading, error: dealsError } = useSelector(
  //  (state: RootState) => state.deals
  //);

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const { data: feesStatement, isValidating } = useFeesStatementSWR(dealId);
  const { data: bankBalance, isValidating: bankBalanceLoading } =
    useBankBalanceSWR(deal?.entityId);

  const {
    subscriptionsByDealId,
    // subscriptionsById,
    isLoading: subscriptionsLoading,
    isSubmitting: subscriptionsSubmitting,
    error: subscriptionsError,
  } = useSelector((state: RootState) => state.subscriptions);

  const { closesById } = useSelector((state: RootState) => state.closes);
  useEffect(() => {
    dispatch(fetchClosesList());
  }, []);

  const subscriptions = Object.values(subscriptionsByDealId[dealId] ?? {})
    .map((subscription) => {
      return {
        ...subscription,
        closes: Object.values(closesById).filter((el) =>
          el?.subscriptions?.includes(subscription?.id)
        ),
      };
    })
    .filter((subscription) =>
      ["COMPLETED", "COMPLETED_FUNDS_RECEIVED"].includes(subscription.status)
    );

  const { isSubmitting } = useSelector((state: RootState) => state.closes);

  const investorTotal = useCallback(() => {
    return closingSubscriptions.reduce((sum, key) => sum + (key.amount | 0), 0);
  }, [closingSubscriptions, subscriptions]);

  useEffect(() => {
    if (!deal || deal.id !== dealId) {
      dispatch(fetchDealById(dealId));
    }
  }, [deal, dealId]);

  useEffect(() => {
    if (deal && (!entity || entity.id !== deal.entityId)) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [deal, dispatch, entity]);

  useEffect(() => {
    if (dealId && !(dealId in subscriptionsByDealId) && !subscriptionsError)
      dispatch(fetchSubscriptionsListByDeal(dealId));
  }, [dealId, dispatch]);

  useEffect(() => {
    const subscriptionsToClose = subscriptions.filter(
      (subscription) =>
        ["COMPLETED", "COMPLETED_FUNDS_RECEIVED"].includes(
          subscription.status
        ) && !subscription?.closes?.length
    );
    setClosingSubscriptions(subscriptionsToClose);
  }, [subscriptionsByDealId, closesById]);

  const [showInvestors, setShowInvestors] = useState(false);
  const [showFinal, setShowFinal] = useState(false);
  const [showFees, setShowFees] = useState(false);
  const [closeAuditData, setCloseAuditData] = useState();

  useEffect(() => {
    if (closingSubscriptions) {
      doChange(
        API.CLOSE_AUDIT,
        {
          subscriptionIds: closingSubscriptions.map((sub) => sub.id),
          dealId: dealId,
        },
        "POST"
      ).then((data) => {
        setCloseAuditData(data);
      });
    }
  }, [closingSubscriptions]);

  useEffect(() => {
    if (!showFeeStatementsFeature) {
      // Flow should be:
      // 1. Statement Page
      // 2. Sign Page
      // 3. Link to Wire Page directly
      setShowInvestors(true);
    } else {
      // Flow without Statement Page active should be
      // 1. Select Investors
      // 2. Sign Page
      setShowFees(true);
    }
  }, [showFeeStatementsFeature]);

  async function handleClick(sig?: string) {
    const closeReq = {
      dealId: dealId,
      entityId: deal.entityId,
      amount: investorTotal(),
    };

    try {
      const closeResult: any = await dispatch(
        insertClose(
          dealId,
          sig,
          feesStatement,
          closingSubscriptions.map((sub) => sub.id)
        )
      );
      closingSubscriptions.map((sub) =>
        dispatch(
          insertNotification({
            tenantId: tenantProfile.id,
            profileId: sub.profileId,
            event: {
              type: "subscription",
              id: sub.id,
            },
            category: "INVESTMENTS",
            message: `Your Deal, ${deal.name}, has now closed`,
            acknowledged: false,
          })
        )
      );
      Log.debug("close", closeResult);
      toast.success(`Close Processing`);

      if (showFeeStatementsFeature && outboundWiresAreEnabled) {
        // Populates fields in the wire screen based on statement values
        history.push(`/dashboard/organizer-banking/${closeResult.id}`);
      } else if (outboundWiresAreEnabled) {
        // Brings user to the blank wire screen to do the wire
        history.push(`/dashboard/organizer-banking`);
      } else {
        // Go back to the deal page (historical behavior)
        history.push(`/deals/${deal.id}`);
      }
    } catch (e) {
      const err: any = e;
      if (err?.response) {
        ToastError(err.response);
      }
    }
  }

  return (
    <>
      <Helmet title={`Close Deal - Deals | ${tenantProfile.name}`} />
      <Modal open={showEdit} onClose={() => setShowEdit(false)} closeIcon>
        <Modal.Content>
          <Header as="h3" icon textAlign="center">
            <Icon name="pencil" circular color="green" />
            Edit {selectedSub.name}&#39;s subscription amount
          </Header>
          <Divider hidden />
          <Formik
            initialValues={{ subscriptionAmount: selectedSub.amount }}
            onSubmit={async (data) => {
              Log.debug("data", data);
              await dispatch(
                updateSubscriptionById({
                  ...selectedSub,
                  amount: Number(data.subscriptionAmount),
                })
              );
              dispatch(
                insertNotification({
                  tenantId: tenantProfile.id,
                  profileId: selectedSub.profileId,
                  event: {
                    type: "subscriptionAmount",
                    id: selectedSub.id,
                  },
                  category: "INVESTMENTS",
                  message: `Your investment amount has changed from ${currencyFormat(
                    selectedSub.amount
                  )} to ${currencyFormat(data.subscriptionAmount)} on Deal ${
                    deal.name
                  } by the Organizer. Please re-sign your documents.`,
                  acknowledged: false,
                })
              );
              setShowEdit(false);
            }}
          >
            {(props) => (
              <Form className="dark-labels" onSubmit={props.handleSubmit}>
                <FormField
                  component={Form.Input}
                  name="subscriptionAmount"
                  label="Subscription Amount"
                  icon="dollar"
                  focus
                  type="number"
                />
                <Button
                  secondary
                  fluid
                  type="submit"
                  loading={subscriptionsSubmitting}
                  content="Update Amount"
                />
              </Form>
            )}
          </Formik>
        </Modal.Content>
      </Modal>

      <ShortHeader title={"Close Deal"} />
      <Divider hidden />

      {!showFeeStatementsFeature && showInvestors && (
        <Container textAlign="center">
          <Header as="h3" inverted textAlign="center">
            The investors listed below are ready to be closed
          </Header>
          <Segment basic inverted textAlign="center" className="no-spacing">
            You can choose which investors are included in this close by
            selecting them below
          </Segment>
          <Divider hidden />
          <Transition animation={"fade up"}>
            <Card fluid>
              {!deal || !subscriptions ? (
                <>
                  <Divider hidden />
                  <Loader active inline="centered" size="massive" />
                </>
              ) : (
                <>
                  <Card.Content>
                    <Card.Description>
                      <Grid columns="equal">
                        <Grid.Column>Investments</Grid.Column>
                      </Grid>
                    </Card.Description>
                  </Card.Content>
                  <Card.Content>
                    <Table style={{ color: "black" }} unstackable>
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>
                            <Input
                              type={"checkbox"}
                              checked={
                                subscriptions.length ===
                                closingSubscriptions.length
                              }
                              onClick={() => {
                                if (
                                  closingSubscriptions.length <
                                  subscriptions.length
                                ) {
                                  setClosingSubscriptions(subscriptions);
                                } else if (
                                  closingSubscriptions.length ===
                                  subscriptions.length
                                ) {
                                  setClosingSubscriptions([]);
                                }
                              }}
                            />
                          </Table.HeaderCell>
                          <Table.HeaderCell>Investor Name</Table.HeaderCell>
                          <Table.HeaderCell textAlign="right">
                            Amount
                          </Table.HeaderCell>
                          <Table.HeaderCell />
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        {subscriptions
                          .filter(
                            (subscription) =>
                              subscription.status === "COMPLETED"
                          )
                          .map((subscription, index) => (
                            <Table.Row key={subscription.id}>
                              <Table.Cell>
                                <Input
                                  type={"checkbox"}
                                  checked={closingSubscriptions
                                    ?.map((sub) => sub.id)
                                    .includes(subscription.id)}
                                  onClick={() => {
                                    if (
                                      closingSubscriptions
                                        ?.map((sub) => sub.id)
                                        .includes(subscription.id)
                                    ) {
                                      setClosingSubscriptions(
                                        closingSubscriptions.filter((sub) => {
                                          return subscription.id !== sub.id;
                                        })
                                      );
                                    } else {
                                      setClosingSubscriptions([
                                        ...closingSubscriptions,
                                        subscription,
                                      ]);
                                    }
                                  }}
                                />
                              </Table.Cell>
                              <Table.Cell>{subscription.name}</Table.Cell>
                              <Table.Cell textAlign="right">
                                <NumberFormat
                                  style={{ color: "green" }}
                                  value={subscription.amount}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                  prefix={"$"}
                                />
                              </Table.Cell>
                              <Table.Cell textAlign="right"></Table.Cell>
                            </Table.Row>
                          ))}
                      </Table.Body>
                    </Table>
                  </Card.Content>
                  <Card.Content>
                    <Card.Header>
                      <Grid columns="equal">
                        <Grid.Column width={10}>Total being closed</Grid.Column>
                        <Grid.Column textAlign="right">
                          <NumberFormat
                            style={{ color: "green" }}
                            value={investorTotal()}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"$"}
                          />
                        </Grid.Column>
                      </Grid>
                    </Card.Header>
                  </Card.Content>
                  <Card.Content extra>
                    <Button
                      disabled={closingSubscriptions.length < 1}
                      secondary
                      fluid
                      onClick={() => {
                        closingSubscriptions.map((sub) => {
                          dispatch(
                            insertNotification({
                              tenantId: tenantProfile.id,
                              profileId: sub.profileId,
                              event: {
                                type: "subscription",
                                id: sub.id,
                              },
                              category: "INVESTMENTS",
                              message: `Your Deal, ${deal.name}, is now in final close`,
                              acknowledged: false,
                            })
                          );
                        });

                        setShowFinal(true);
                        setShowInvestors(false);
                      }}
                    >
                      Close Into Deal
                    </Button>
                  </Card.Content>
                </>
              )}
            </Card>
          </Transition>
        </Container>
      )}

      {showFees && showFeeStatementsFeature && entity && (
        <Container textAlign="center">
          <Paper>
            {closeAuditData ? (
              <>
                <Typography variant={"h6"}>Deal Audit Results</Typography>
                <AuditListBackend
                  auditData={closeAuditData}
                  setCanApprove={setCanAccept}
                />
              </>
            ) : (
              <>
                <Typography variant={"h6"}>Auditing Your Deal...</Typography>
                <CircularProgress />
              </>
            )}

            {!feesStatement ? (
              <div style={{ color: "white" }}>
                <ContentLoader
                  message="Generating closing statement, please wait..."
                  size={64}
                  timeoutMilliseconds={30000}
                />
              </div>
            ) : (
              <StatementTable
                statementData={feesStatement}
                loading={isValidating}
              />
            )}
            <Divider hidden />

            <Grid textAlign="center">
              <Grid.Column mobile={16} tablet={8} computer={8}>
                <Button
                  secondary
                  fluid
                  disabled={!canAccept}
                  onClick={() => {
                    setShowFinal(true);
                    setShowFees(false);
                  }}
                >
                  {isSeriesEntityOfGlassboardMasterII(entity)
                    ? "Proceed with Close"
                    : "Proceed to Signature Page"}
                </Button>
              </Grid.Column>
            </Grid>
          </Paper>
        </Container>
      )}

      {showFinal && (
        <>
          <Grid textAlign={"center"}>
            <Grid.Column mobile={1} tablet={2} computer={3} />
            <Grid.Column mobile={14} tablet={12} computer={10}>
              <Header as="h2" icon inverted textAlign="center">
                <Icon name="warning circle" />
                You are closing {closingSubscriptions.length} investors into the
                &apos;{deal.name}&apos; deal
              </Header>

              <Header as="h2" color="green" textAlign="center">
                totaling{" "}
                {
                  <NumberFormat
                    value={investorTotal()}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"$"}
                  />
                }
              </Header>
              <SignaturePad onSubmit={handleClick} id={"sig"} />
              <Divider />
              <Button
                inverted
                fluid
                onClick={() => {
                  if (showFeeStatementsFeature) {
                    setShowFees(true);
                  } else {
                    setShowInvestors(true);
                  }
                  setShowFinal(false);
                }}
                content={
                  showFeeStatementsFeature
                    ? "Go Back To Closing Statement"
                    : "Go Back To Investors"
                }
              />
            </Grid.Column>
            <Grid.Column mobile={1} tablet={2} computer={3} />
          </Grid>
        </>
      )}
    </>
  );
}
