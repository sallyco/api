import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { RootState } from "../rootReducer";
import { Loader, Divider } from "semantic-ui-react";
import { ArtworkList } from "../components/artwork/ArtworkList";
import { fetchAssetsList } from "../slices/assetsSlice";
import { TenantContext } from "../contexts";

interface CLProps {
  page: number;
  perPage: number;
  totalCount: number;
}

// TODO get pagination working
export const ArtworkListPage = ({
  page = 1,
  perPage = 10,
  totalCount = 0,
}: CLProps) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);

  const { currentPageAssets, isLoading, error, assetsById } = useSelector(
    (state: RootState) => state.assets
  );

  const assets = currentPageAssets.map((assetId) => assetsById[assetId]);

  useEffect(() => {
    dispatch(fetchAssetsList());
  }, [tenantProfile]);

  return (
    <>
      <Helmet title={`Artwork - Dashboard | ${tenantProfile.name}`}></Helmet>
      {isLoading || !assets ? (
        <>
          <Divider hidden />
          <Loader active inline="centered" size="massive" />
        </>
      ) : (
        <ArtworkList assets={assets} />
      )}
    </>
  );
};
