import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import NumberFormat from "react-number-format";
import BankingImage from "../assets/images/Banking";
import ShortHeader from "../components/ShortHeader";
import { fetchDealById } from "../slices/dealsSlice";
import {
  Icon,
  Segment,
  Button,
  Divider,
  Header,
  Container,
  List,
  Transition,
} from "semantic-ui-react";
import { Helmet } from "react-helmet";
import { Link, useParams, useLocation } from "react-router-dom";
import { RootState } from "../rootReducer";
import { fetchSubscriptionById } from "../slices/subscriptionsSlice";
import { fetchSelf } from "../slices/usersSlice";
import { TenantContext } from "../contexts";
import { Subscription } from "../api/subscriptionsApi";

const VIEW_BANKING_DETAILS_TXT = "View Banking Details";
// TODO: better way to handle default tenant info?
const DEFAULT_FUNDING_LIMIT = 100000;
export const SelectFundingPage = (props) => {
  const dispatch = useDispatch();
  const tenantProfile = useContext(TenantContext);
  const FUNDING_LIMIT =
    tenantProfile?.bankingLimits?.ach?.debit?.transaction ??
    DEFAULT_FUNDING_LIMIT;
  const query = new URLSearchParams(useLocation().search);
  const { subscriptionId } = useParams<{ subscriptionId: string }>();
  const previousAmount = Number(query.get("previousAmount")) ?? 0;

  const { isLoading: dealsLoading } = useSelector(
    (state: RootState) => state.deals
  );

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });

  const { self } = useSelector((state: RootState) => state.users);

  useEffect(() => {
    if (!self) {
      dispatch(fetchSelf());
    }
  }, [self]);

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [dispatch, subscription, deal]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const isOverFundingLimit = (subscription: Subscription) => {
    if (previousAmount > 0) {
      return subscription?.amount - previousAmount > FUNDING_LIMIT;
    }
    return subscription?.amount > FUNDING_LIMIT;
  };

  return (
    <>
      <Helmet title={`Select Funding Source | ${tenantProfile.name}`} />
      <ShortHeader title={deal?.name} loading={dealsLoading} hideBackArrow />
      <Transition animation={"fade"}>
        <Container textAlign="center" className="mobile-container">
          <Header as="h2" inverted icon>
            Select a Funding Method
            <div>
              <BankingImage width={200} />
            </div>
          </Header>
          <Segment basic inverted>
            {!isOverFundingLimit(subscription) && (
              <>
                If you want to transfer funds, you can add your bank account
                using our partner Plaid. Plaid is a service that easily and
                securely connects financial accounts. <br />
                <br />
                If you prefer to transfer your funds outside the platform, click
                &quot;{VIEW_BANKING_DETAILS_TXT}&quot; below to view the
                required details.
              </>
            )}
            {isOverFundingLimit(subscription) && (
              <>
                Your investment amount of &nbsp;
                <NumberFormat
                  value={subscription.amount}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"$"}
                />
                &nbsp; exceeds the maximum{" "}
                <NumberFormat
                  value={FUNDING_LIMIT}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"$"}
                />{" "}
                that can be processed through this platform. You will need to
                wire your funds off-platform, directly from your financial
                institution. <br />
              </>
            )}
          </Segment>
          <List relaxed>
            {!isOverFundingLimit(subscription) && (
              <>
                <Segment basic inverted>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={"https://plaid.com/what-is-plaid/"}
                  >
                    {" "}
                    Learn more about Plaid <Icon name="external" />{" "}
                  </a>
                </Segment>
                <List.Item>
                  <>
                    <Button
                      as={Link}
                      to={`/subscriptions/${subscriptionId}/funding${
                        previousAmount > 0
                          ? `?previousAmount=${previousAmount}`
                          : ""
                      }`}
                      secondary
                      fluid
                      content="Continue with PLAID"
                    />
                  </>
                </List.Item>
                <Divider
                  horizontal
                  inverted={tenantProfile.inverted}
                  content="Or"
                />
              </>
            )}
            <List.Item>
              <Button
                as={Link}
                to={`/subscriptions/${subscriptionId}/bank_account`}
                secondary
                fluid
                content={VIEW_BANKING_DETAILS_TXT}
              />
            </List.Item>
          </List>
        </Container>
      </Transition>
    </>
  );
};
