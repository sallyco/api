import React, { useContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Helmet } from "react-helmet";
import { Link, useLocation, useParams } from "react-router-dom";
import BankingImage from "../assets/images/Banking";

import { RootState } from "../rootReducer";

import { fetchSubscriptionById } from "../slices/subscriptionsSlice";
import { fetchEntityById } from "../slices/entitySlice";
import { fetchDealById } from "../slices/dealsSlice";
import { fetchProfileById } from "../slices/profilesSlice";

import {
  Divider,
  Header,
  Button,
  Container,
  Segment,
  Transition,
  List,
  Message,
  Grid,
} from "semantic-ui-react";
import ShortHeader from "../components/ShortHeader";
import { TenantContext } from "../contexts";
import BankInfoList from "../components/banks/BankInfoList";
import { getProfileName } from "../api/profileApiWrapper";
import {
  getErrorMessagesByType,
  ERROR_TYPES,
} from "../plaid/plaidErrorStrings";
import EntityBankingInformation from "../components/banks/EntityBankingInformation";

export const BankAccountPage = () => {
  const dispatch = useDispatch();
  const { subscriptionId, displayReason } = useParams<{
    subscriptionId: string;
    displayReason: string;
  }>();
  const tenantProfile = useContext(TenantContext);

  const subscription = useSelector(
    (state: RootState) => state.subscriptions.subscriptionsById[subscriptionId]
  );

  const deal = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.deals.dealsById[subscription.dealId];
  });

  const entity = useSelector((state: RootState) => {
    return !deal ? undefined : state.entities.entitiesById[deal.entityId];
  });

  const query = new URLSearchParams(useLocation().search);
  const error = query.get("error") ?? false;
  const errorType = query.get("errorType") ?? "";

  useEffect(() => {
    if (!subscription || subscription.id !== subscriptionId) {
      dispatch(fetchSubscriptionById(subscriptionId));
    }
  }, [dispatch, subscription, subscriptionId]);

  useEffect(() => {
    if (subscription && !deal) {
      dispatch(fetchDealById(subscription.dealId));
    }
  }, [dispatch, subscription, deal]);

  const profilesListError = useSelector((state: RootState) => {
    return state.profiles.error;
  });
  const profile = useSelector((state: RootState) => {
    return !subscription
      ? undefined
      : state.profiles.profilesById[subscription.profileId];
  });
  useEffect(() => {
    if (
      subscription &&
      subscription.profileId &&
      !profile &&
      !profilesListError
    ) {
      dispatch(fetchProfileById(subscription.profileId));
    }
  }, [dispatch, subscription, profile]);

  useEffect(() => {
    if (deal && !entity) {
      dispatch(fetchEntityById(deal.entityId));
    }
  }, [dispatch, deal, entity]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <Helmet title={`Bank Account Details | ${tenantProfile.name}`} />
      <ShortHeader title="Bank Account Details" hideBackArrow />
      <Transition animation={"fade"}>
        <Container textAlign="center">
          <Header as="h2" icon>
            <div>
              <BankingImage width={200} />
            </div>
          </Header>
          {displayReason === "transaction_failed" && (
            <Segment inverted basic>
              <Message color={"yellow"}>
                <Message.Header>
                  Our banking provider is currently unable to process
                  transactions .
                </Message.Header>
                <Message.Content>
                  <p>
                    Your transaction attempt has been cancelled. No funds have
                    been withdrawn from your account.
                  </p>
                  <p>
                    Please use the details below for an off-platform transaction
                    to fund this deal.
                  </p>
                </Message.Content>
              </Message>
            </Segment>
          )}
          {error && (
            <>
              <Segment basic inverted>
                We were unable to connect your bank account with Plaid. You may
                re-attempt to connect with Plaid.
              </Segment>
              {errorType === ERROR_TYPES.MFA_NOT_SUPPORTED &&
                getErrorMessagesByType(errorType).map((message, index) => {
                  return (
                    <Segment basic inverted key={index}>
                      {message}
                    </Segment>
                  );
                })}
              <Button
                as={Link}
                to={`/subscriptions/${subscriptionId}/funding`}
                secondary
                content="Continue with PLAID"
              />
              <br />
              <br />
              <Divider
                horizontal
                inverted={tenantProfile.inverted}
                content="Or"
              />
            </>
          )}
          <Segment inverted basic>
            Below are the fund&apos;s bank account details for an off-platform
            transaction.
          </Segment>
          {entity?.bankAccount ? (
            <Segment
              vertical
              inverted
              className="no-padding"
              compact
              textAlign="left"
              style={{ margin: "auto" }}
            >
              <EntityBankingInformation
                entity={entity}
                investorProfile={profile}
              />
            </Segment>
          ) : (
            <>
              <Segment
                vertical
                inverted
                className="no-padding"
                compact
                textAlign="left"
                style={{ margin: "auto" }}
              >
                The organizer hasn&apos;t added banking details yet, please
                contact the organizer
              </Segment>
            </>
          )}
          <Grid inverted centered>
            <Segment compact inverted basic>
              <Divider />
              <Button
                as={Link}
                to={`/subscriptions/${subscriptionId}`}
                secondary
                fluid
                content="Return to Deal Summary"
              />
            </Segment>
          </Grid>
        </Container>
      </Transition>
    </>
  );
};
