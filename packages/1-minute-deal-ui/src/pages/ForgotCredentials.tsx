import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { requsetPassword } from "../api/usersApi";
import { Segment, List, Container, Transition } from "semantic-ui-react";
import ForgotCredentialsForm from "../forms/ForgotCredentialsForm";

import { sendEmail, sendResetPasswordEmail } from "../api/emailsApi";
import { toast } from "react-toastify";

import Log from "../tools/Log";
import { ToastError } from "../tools/ToastMessage";
import { TenantContext } from "../contexts";

export const ForgotCredentials = () => {
  const [submitting, setSubmitting] = useState(false);

  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  async function handleSubmit(data) {
    setSubmitting(true);
    Log.debug("data", data);
    try {
      const token = await requsetPassword(data);
      await sendResetPasswordEmail(data.email, token.data);
      toast.success("If an account exists, you will receive an email.");
      history.push("/");
    } catch (e) {
      setSubmitting(false);
      ToastError(e.response);
      Log.error("error", e);
    }
  }

  return (
    <>
      <Helmet title={`Recover Password | ${tenantProfile.name}`}></Helmet>
      <Transition>
        <Container className="mobile-container">
          <ForgotCredentialsForm
            title={"Recover Password"}
            onSubmit={handleSubmit}
            submitting={submitting}
          />

          <Segment basic textAlign="center">
            <List>
              <List.Item>
                <Link to="/">Back to sign in</Link>
              </List.Item>
            </List>
          </Segment>
        </Container>
      </Transition>
    </>
  );
};
