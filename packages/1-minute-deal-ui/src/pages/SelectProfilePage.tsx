import React, { useContext, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import entityImage from "../assets/images/Entity.png";
import { getProfileName } from "../api/profileApiWrapper";

import * as Yup from "yup";
import {
  Form,
  Button,
  Divider,
  Header,
  Loader,
  Image,
  List,
  Transition,
} from "semantic-ui-react";
import { Formik } from "formik";
import { FormField } from "../components/FormField";
import { Helmet } from "react-helmet";
import { Link, useHistory, useLocation } from "react-router-dom";
import Log from "../tools/Log";
import { RootState } from "../rootReducer";
import { fetchProfilesList } from "../slices/profilesSlice";
import { TenantContext } from "../contexts";
import ProfileCard from "../components/profiles/ProfileCard";
import { Box, Container, Grid } from "@mui/material";
import ParallelMarkets from "../components/accreditation/ParallelMarkets";

export const SelectProfilePage = () => {
  const query = new URLSearchParams(useLocation().search);
  const redirect = query.get("redirect") ?? "";
  const redirectQuery = redirect ? `?redirect=${redirect}` : "";
  const initialShowNew = Boolean(redirect);

  const dispatch = useDispatch();
  const [showNew, setShowNew] = useState(initialShowNew);
  let history = useHistory();
  const tenantProfile = useContext(TenantContext);

  const { currentPageProfiles, isLoading, profilesById } = useSelector(
    (state: RootState) => state.profiles
  );

  const profiles = currentPageProfiles.map(
    (profileId) => profilesById[profileId]
  );

  useEffect(() => {
    dispatch(fetchProfilesList());
  }, [dispatch]);

  const validation = Yup.object().shape({
    profile: Yup.string().required("Select a profile is required"),
  });

  async function onSubmit(data) {
    Log.debug("data", data);
    history.push(`/deals/create?profileId=${data.profile}`);
  }

  return (
    <>
      <Helmet
        title={`Select Profile Type - Profiles | ${tenantProfile.name}`}
      />
      <Image src={entityImage.src} size="medium" centered />

      {isLoading ? (
        <Container maxWidth="sm">
          <Box
            display={"flex"}
            flexDirection={"column"}
            alignItems={"center"}
            justifyContent={"center"}
          >
            <Divider hidden />
            <Loader size="massive" inverted inline active />
          </Box>
        </Container>
      ) : (
        <>
          {showNew || (profiles && profiles.length === 0) ? (
            <Transition transitionOnMount animation={"fade up"}>
              <Container maxWidth="sm">
                <Header as="h2" inverted={tenantProfile.inverted} icon>
                  Do you operate your Deal Organizer business as an Individual
                  or an Entity?
                </Header>
                <List relaxed>
                  <List.Item>
                    <Button
                      as={Link}
                      to={`/profiles/create/individual${redirectQuery}`}
                      secondary
                      fluid
                      content="Individual"
                    />
                  </List.Item>
                  <Divider
                    horizontal
                    inverted={tenantProfile.inverted}
                    content="Or"
                  />
                  <List.Item>
                    <Button
                      as={Link}
                      to={`/profiles/create/entity${redirectQuery}`}
                      secondary
                      fluid
                    >
                      Entity
                    </Button>
                  </List.Item>
                </List>
              </Container>
            </Transition>
          ) : (
            <Transition transitionOnMount animation={"fade up"}>
              <Container>
                <Grid
                  container
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="center"
                  spacing={2}
                  style={{
                    textAlign: "center",
                  }}
                >
                  <Grid item xs>
                    <Header as="h2" inverted={tenantProfile.inverted} icon>
                      Select existing profile or create a new profile?
                    </Header>
                    <List relaxed>
                      <List.Item>
                        <Formik
                          initialValues={{
                            profile: null,
                          }}
                          validationSchema={validation}
                          onSubmit={onSubmit}
                        >
                          {(props) => (
                            <Form onSubmit={props.handleSubmit}>
                              <FormField
                                id="profile"
                                name="profile"
                                component={Form.Select}
                                options={profiles
                                  .filter(
                                    (profile) =>
                                      profile.profileType === "ORGANIZER"
                                  )
                                  .map((profile) => ({
                                    value: profile.id,
                                    text: getProfileName(profile),
                                  }))}
                                placeholder="Select a Profile"
                              />
                              {props?.values?.profile && (
                                <ProfileCard
                                  profileId={props.values.profile}
                                  showAccreditation={false}
                                />
                              )}
                              <Button
                                fluid
                                secondary
                                type="submit"
                                content="Select"
                              />
                            </Form>
                          )}
                        </Formik>
                      </List.Item>
                      <Divider horizontal inverted={tenantProfile.inverted}>
                        Or
                      </Divider>
                      <List.Item>
                        <Button
                          fluid
                          secondary
                          content="Create New"
                          onClick={() => setShowNew(true)}
                        />
                      </List.Item>
                    </List>
                  </Grid>
                </Grid>
              </Container>
            </Transition>
          )}
        </>
      )}
    </>
  );
};
