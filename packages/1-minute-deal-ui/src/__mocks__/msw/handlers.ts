import { rest } from "msw";
import { API } from "../../api/swrApi";

export const handlers = [
  rest.get(API.FULL_PATH(API.NOTIFICATIONS), (req, res, ctx) => {
    return res(
      ctx.json({
        data: [],
      })
    );
  }),
  rest.get(
    API.FULL_PATH(API.NOTIFICATION_BY_ID(":notificationId")),
    (req, res, ctx) => {
      const { notificationId } = req.params;
      return res(
        ctx.json({
          id: notificationId,
        })
      );
    }
  ),
  rest.get(
    API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
    (req, res, ctx) => {
      const { subscriptionId } = req.params;
      return res(
        ctx.json({
          id: subscriptionId,
          dealId: "test",
          ownerId: "test",
        })
      );
    }
  ),
  rest.put(
    API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
    (req, res, ctx) => {
      const { subscriptionId } = req.params;
      return res(
        ctx.json({
          id: subscriptionId,
          dealId: "test",
        })
      );
    }
  ),
  rest.patch(
    API.FULL_PATH(API.SUBSCRIPTION_BY_ID(":subscriptionId")),
    (req, res, ctx) => {
      const { subscriptionId } = req.params;
      return res(
        ctx.json({
          id: subscriptionId,
          dealId: "test",
        })
      );
    }
  ),
  rest.get(API.FULL_PATH(API.SUBSCRIPTIONS), (req, res, ctx) => {
    return res(
      ctx.json({
        data: [
          {
            id: "test",
            dealId: "test",
            ownerId: "test",
          },
        ],
      })
    );
  }),
  rest.get(API.FULL_PATH(API.ACCREDITATIONS), (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: "test",
        },
      ])
    );
  }),
  rest.get(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
    const { dealId } = req.params;
    return res(
      ctx.json({
        id: dealId,
        entityId: "test",
      })
    );
  }),
  rest.patch(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
    const { dealId } = req.params;
    return res(
      ctx.json({
        id: dealId,
        entityId: "test",
      })
    );
  }),
  rest.put(API.FULL_PATH(API.DEAL_BY_ID(":dealId")), (req, res, ctx) => {
    const { dealId } = req.params;
    return res(
      ctx.json({
        id: dealId,
      })
    );
  }),
  rest.get(API.FULL_PATH(API.ENTITY_BY_ID(":entityId")), (req, res, ctx) => {
    const { entityId } = req.params;
    return res(
      ctx.json({
        id: entityId,
        entityId: "test",
      })
    );
  }),
  rest.patch(API.FULL_PATH(API.ENTITY_BY_ID(":entityId")), (req, res, ctx) => {
    const { entityId } = req.params;
    return res(
      ctx.json({
        id: entityId,
        entityId: "test",
      })
    );
  }),
  rest.get(
    API.FULL_PATH(API.ENTITY_DOCUMENT(":entityId", ":documentId")),
    (req, res, ctx) => {
      const { entityId, documentId } = req.params;
      return res(
        ctx.json({
          id: entityId,
        })
      );
    }
  ),
  rest.post(API.FULL_PATH(API.PROFILES), (req, res, context) => {
    return res(
      context.json({
        countryOfFormation: "United States of America",
        createdAt: "2021-09-06T09:25:43.825Z",
        dateOfBirth: "2000-01-01",
        displayName: "Sample Individual 2",
        email: "t@t.t",
        id: "test",
        investorStatus: [3],
        isUSBased: true,
        name: "Test Investor",
        ownerId: "38551bae-bc41-4f20-bcf3-88aa8a755739",
        phone: "+00 000 000 0000",
        profileType: "INVESTOR",
        purchaserStatus: [],
        stateOfFormation: "Utah",
        taxDetails: {
          registrationType: "INDIVIDUAL",
          taxIdentification: { type: "ssn", value: "111-11-1111" },
        },
        tenantId: "gbt",
        updatedAt: "2021-09-06T09:25:43.825Z",
      })
    );
  }),
  rest.get(API.FULL_PATH(API.PROFILES), (req, res, context) => {
    return res(
      context.json({
        data: [
          {
            countryOfFormation: "United States of America",
            createdAt: "2021-09-06T09:25:43.825Z",
            dateOfBirth: "2000-01-01",
            displayName: "Sample Individual 2",
            email: "t@t.t",
            id: "test",
            investorStatus: [3],
            isUSBased: true,
            name: "Test Investor",
            ownerId: "38551bae-bc41-4f20-bcf3-88aa8a755739",
            phone: "+00 000 000 0000",
            profileType: "INVESTOR",
            purchaserStatus: [],
            stateOfFormation: "Utah",
            taxDetails: {
              registrationType: "INDIVIDUAL",
              taxIdentification: { type: "ssn", value: "111-11-1111" },
            },
            tenantId: "gbt",
            updatedAt: "2021-09-06T09:25:43.825Z",
          },
        ],
      })
    );
  }),
  rest.get(
    API.FULL_PATH(API.PROFILE_BY_ID(":profileId")),
    (req, res, context) => {
      const { profileId } = req.params;
      return res(
        context.json({
          id: profileId,
        })
      );
    }
  ),
  rest.get(API.FULL_PATH(API.PROFILES_FOR_DEAL("*")), (req, res, context) => {
    return res(
      context.json({
        data: [],
      })
    );
  }),
  rest.post(
    API.FULL_PATH(API.PROFILE_CREATE_KYCAML_APPLICANT("test")),
    (req, res, context) => {
      return res(
        context.json({
          id: "test",
        })
      );
    }
  ),
  rest.patch(API.FULL_PATH(API.ASSET_BY_ID("*")), (req, res, context) => {
    return res(
      context.json({
        id: "test",
      })
    );
  }),
  rest.patch(API.FULL_PATH(API.CLOSES_BY_ID("*")), (req, res, context) => {
    return res(
      context.json({
        id: "test",
      })
    );
  }),
  rest.get(API.FULL_PATH(API.SELF), (req, res, context) => {
    return res(
      context.json({
        id: "test",
        email: "test@test.com",
      })
    );
  }),
  rest.get(
    API.FULL_PATH(API.INVESTOR_PROFILES_OF_USER),
    (req, res, context) => {
      return res(context.json([]));
    }
  ),
  rest.get(API.FULL_PATH(API.PING), (req, res, context) => {
    return res(context.json({}));
  }),
  rest.post(API.FULL_PATH(API.FILES_UPLOAD), (req, res, context) => {
    return res(context.json([{ id: "test" }]));
  }),
  rest.post(API.FULL_PATH(API.USERS_VERIFY_EMAIL), (req, res, context) => {
    return res(context.json({}));
  }),
  rest.post(
    API.FULL_PATH(API.DECISIONS(":decisionId")),
    (req, res, context) => {
      return res(context.json({ shouldUseW9: { value: true } }));
    }
  ),
  rest.get(API.FULL_PATH(API.CLOSES), (req, res, context) => {
    return res(context.json([]));
  }),
  rest.get(
    API.FULL_PATH(API.FEES_BY_DEAL_ID(":dealId")),
    (req, res, context) => {
      return res(context.json({}));
    }
  ),
];
