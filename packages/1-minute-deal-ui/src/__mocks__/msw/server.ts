import { setupServer } from "msw/node";
import { handlers } from "./handlers";
import { cleanup } from "@testing-library/react";
import { cache } from "swr";
require("dotenv").config();
// This configures a request mocking server with the given request handlers.
const server = setupServer(...handlers);
export { server };

beforeAll(() => {
  server.listen();
  // Used for debugging:
  // server.events.on('request:start', (req) => {
  //   console.log(req.method, req.url.href)
  // })
  // server.events.on('response:mocked', (res, reqId) => {
  //   const responseText = res.body
  //   console.log('sent a mocked response', reqId, responseText)
  // })
});
// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(async () => {
  server.resetHandlers(...handlers);
  // Since this is setup, we'll teardown rendering between tests to prevent SWR from sticking around
  await cleanup();
  cache.clear();
});
// Clean up after the tests are finished.
afterAll(() => server.close());
