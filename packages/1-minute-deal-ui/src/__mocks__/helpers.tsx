import React, { JSXElementConstructor, ReactElement, ReactNode } from "react";
import { TenantContext } from "../contexts";
import { SWRConfig } from "swr";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import {
  ThemeProvider,
  Theme,
  StyledEngineProvider,
  adaptV4Theme,
} from "@mui/material";
import { ReactComponentLike } from "prop-types";
import { render } from "@testing-library/react";
import { createTheme } from "@mui/material/styles";

export const defaultTestTenant = {
  id: "gbt",
  inverted: true,
  managerId: null,
  masterEntityId: null,
  maxAllowedDaysToClose: 0,
  name: "Glassboard Technology",
  url: "https://glassboardtech.com",
  assets: {
    colors: {
      primaryColor: "#1D3557",
      secondaryColor: "#457B9D",
    },
    images: {
      logo: null,
      logoInverted: null,
    },
  },
  settings: {
    emails: {
      fromAddress: "",
    },
  },
  bankingLimits: {},
  realm: {
    id: "gbt",
    realm: "gbt",
    verifyEmail: true,
  },
  billingContact: "Test Billing Contact",
};

export const makeHookWrapper = (history, store) =>
  function testWrapper({ children }: { children?: ReactNode }) {
    return (
      <TenantContext.Provider value={defaultTestTenant}>
        <SWRConfig value={{ dedupingInterval: 0 }}>
          <Provider store={store}>
            <Router history={history}>
              <StyledEngineProvider injectFirst>
                <ThemeProvider theme={createTheme(adaptV4Theme({}))}>
                  {children}
                </ThemeProvider>
              </StyledEngineProvider>
            </Router>
          </Provider>
        </SWRConfig>
      </TenantContext.Provider>
    );
  };

export const makeWrapper = (history, store, component, props = {}) => {
  return (
    <TenantContext.Provider value={defaultTestTenant}>
      <SWRConfig value={{ dedupingInterval: 0 }}>
        <Provider store={store}>
          <Router history={history}>
            <StyledEngineProvider injectFirst>
              <ThemeProvider theme={createTheme(adaptV4Theme({}))}>
                {React.cloneElement(component, props)}
              </ThemeProvider>
            </StyledEngineProvider>
          </Router>
        </Provider>
      </SWRConfig>
    </TenantContext.Provider>
  );
};

const TestComponentWrapper = ({ store, history, ...props }): JSX.Element => {
  return (
    <>
      <TenantContext.Provider value={props.tenant ?? defaultTestTenant}>
        <SWRConfig value={{ dedupingInterval: 0 }}>
          <Provider store={store}>
            <Router history={history}>
              <StyledEngineProvider injectFirst>
                <ThemeProvider theme={createTheme(adaptV4Theme({}))}>
                  {props.children}
                </ThemeProvider>
              </StyledEngineProvider>
            </Router>
          </Provider>
        </SWRConfig>
      </TenantContext.Provider>
    </>
  );
};
export { TestComponentWrapper };
