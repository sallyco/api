import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../rootReducer";

import { fetchDealById } from "../slices/dealsSlice";
import { fetchCloseById } from "../slices/closesSlice";
import { fetchSubscriptionsListByDeal } from "../slices/subscriptionsSlice";

export default function useCloseDealMeta(dealId: string, closeId: string) {
  const dispatch = useDispatch();

  const deal = useSelector((state: RootState) => state.deals.dealsById[dealId]);
  const close = useSelector(
    (state: RootState) => state.closes.closesById[closeId]
  );
  const subscriptions = useSelector((state: RootState) =>
    Object.values(state.subscriptions.subscriptionsById).filter(
      (subscription) => close.subscriptions.includes(subscription.id)
    )
  );

  useEffect(() => {
    dispatch(fetchDealById(dealId));
    dispatch(fetchCloseById(closeId));
    dispatch(fetchSubscriptionsListByDeal(dealId));
  }, []);

  const maxAmount =
    subscriptions?.length > 0
      ? subscriptions.reduce((amt, sub) => amt + sub.amount, 0)
      : 0;

  const earliestDate = deal?.estimatedCloseDate ?? "";

  return { deal, close, subscriptions, maxAmount, earliestDate };
}
