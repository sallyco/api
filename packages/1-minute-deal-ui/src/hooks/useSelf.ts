import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchSelf, getSelfSuccess } from "../slices/usersSlice";
import { RootState } from "../rootReducer";
import { API, useRequest } from "../api/swrApi";

export default function useSelf() {
  const { data: self } = useRequest(API.SELF);
  const dispatch = useDispatch();

  useEffect(() => {
    if (self) {
      dispatch(getSelfSuccess(self));
    }
  }, [self]);

  return self;
}
