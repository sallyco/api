import { useEffect, useState } from "react";
import { useFeatureFlag } from "../components/featureflags/FeatureFlags";

import {
  getSubscriptionDealMeta,
  SubscriptionsDealMeta,
} from "../api/subscriptionsApi";

export function useSubscriptionMeta(subscriptionId) {
  const investorLimitFeature = useFeatureFlag("max_investors_per_deal");
  const [meta, setMeta] = useState<SubscriptionsDealMeta>();
  const [dealIsAvailable, setDealIsAvailable] = useState(true);

  useEffect(() => {
    if (!meta && investorLimitFeature) {
      getSubscriptionDealMeta(subscriptionId).then((result) => {
        setMeta(result.meta);
      });
    }

    if (meta && investorLimitFeature) {
      setDealIsAvailable(
        meta?.includedInDeal ||
          (!meta?.investorLimitReached && !(meta?.maxCommitmentAmount < 0))
      );
    }
  }, [meta, investorLimitFeature]);

  return { meta, dealIsAvailable };
}
