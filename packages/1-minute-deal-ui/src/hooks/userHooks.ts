import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../rootReducer";
import { fetchSelf } from "../slices/usersSlice";

export function useLoggedInUser() {
  const loggedInUser = useSelector((state: RootState) => state.users.self);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!loggedInUser) {
      dispatch(fetchSelf());
    }
  }, [dispatch, loggedInUser]);

  return loggedInUser;
}
