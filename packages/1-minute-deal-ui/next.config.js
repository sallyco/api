module.exports = {
  async rewrites() {
    return [
      {
        source: '/:path*',
        destination: '/',
      },
    ];
  },
  experimental: {
    css: false,
  }
};
