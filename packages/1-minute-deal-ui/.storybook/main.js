module.exports = {
  stories: ['../stories/**/*.stories.tsx'],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-controls',
    'storybook-addon-material-ui',
    '@storybook/addon-essentials',
  ],
  webpackFinal: async (config) => {
    // do mutation to the config

    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      // @ts-ignore
      loader: require.resolve('babel-loader'),
      options: {
        presets: [['react-app', {flow: false, typescript: true}]],
      },
    });
    config.resolve.extensions.push('.ts', '.tsx');

    return config;
  },
  typescript: {
    check: false,
    checkOptions: {},
    reactDocgen: 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      shouldExtractLiteralValuesFromEnum: true,
      propFilter: (prop) =>
        prop.parent ? !/node_modules/.test(prop.parent.fileName) : true,
    },
  },
};
